﻿# The-Loop-Story &nbsp; <sup><sub><sup>v0.1.1</sup></sub></sup>

Read the story in English and Deutsch on

### &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://normai.gitlab.io/the-loop-story">normai.gitlab.io/the-loop-story</a>

1.&nbsp;[Goto](#id20230617o0534)
     2.&nbsp;[While](#id20230617o0536)
     3.&nbsp;[Do-While](#id20230617o0538)
     4.&nbsp;[For](#id20230617o0542)
     5.&nbsp;[For-Each-Struct](#id20230617o0544)
     6.&nbsp;[For-Each-Method](#id20230617o0546)
     7.&nbsp;[Recursion](#id20230617o0548)

This story explains the various ways of programming a loop. It takes
you from the basement phrasing `goto` through the process of ever
higher abstracting to the penthouse `ForEach`-method.

## <a name="id20230617o0534"></a> 1. Goto

Before *structured programming* existed, in arachaic languages like Assembler
 or BASIC, there were no dedicated loop keywords. You had to roll a loop
 yourself with a conditional `goto`.

In today higher abstracting languages, the `goto` is banned, or at least
 defused, e.g. constraining it into it's code block, e.g. in C-Sharp.
 <sup><sub>Todo: Check C-Sharp syntax.</sub></sup>

Bottom line: **The conditional Goto provides the brute primitive means for creating
 a loop**, when you have no structured syntax available.

See code in
 [`./codes/c/c1goto.c`](./codes/c/c1goto.c)

By the way. Remember the *Switch-Case* control structure in modern languages.
 Do you wonder about the syntax with the colon, and the need to insert
 a `break` after each single case? This are the syntactic remnants of the
 former Goto.

## <a name="id20230617o0536"></a> 2. While

The While-Loop is the mother of all loop types, which have evolved then in
 the structured programming languages. `if`, `while` and function calls
 are the (three) pillars, which the structured programming is built upon,
 which spare us from the impenetrably spaghetti code we got with
 unstructured programming.

Bottom line. **The While keyword abstracts away the dangerous arbitrary jumps
 and provides a confident structure instead**.

See code in
 [`./codes/jv/j2while.java`](./codes/jv/j2while.java)

## <a name="id20230617o0538"></a> 3. Do-While

Do-While is the While loop's ugly sister, it checks the loop condition
 at the end of the loop body, thus making sure the loop body runs
 at least once, before the loop can be ended.

 For my taste, it brings more syntactic learning burden, than it is useful in
 practice. E.g. the thrifty Python does not offer Do-While at all.

Bottom line. The Do-While keywords may be considered as a **legacy burden**
 from the time, when the While was brand new, and the language designers
 had not much experience yet about what is really necessary,
 and what not *(just my personal assessment)*.

See code in
 [`./codes/jv/j3dowhile.java`](./codes/jv/j3dowhile.java)

## <a name="id20230617o0542"></a> 4. For

The typical While loop consists of three crucial elements: 1. Some kind of
 counter is initialized above the While keyword, 2. on the While line, the
 loop condition is stated and 3. inside the loop somehow the counter must
 be changed, so the loop will eventually be finished.

Those three elements may well be distanced from each other. To understand
 the logic of a loop, your eyes have to go up and down, and you need to bring multiple
 lines into relation. At worst you even have to scroll up and down, so
 you cannot see that three places in one single view.

Here comes the For loop for rescue. It combines above described three
 scattered elements into one single clearly arranged line.

Rule of thumb. Take the For loop whenever it is clear, how often the loop
 runs, before it starts.

Bottom line: **The For loop abstracts away the scattered nature of the basic
 While loop** by condensing them into one single clearly arranged line.

See code in
 [`./codes/jv/j4for.java`](./codes/jv/j4for.java)

## <a name="id20230617o0544"></a> 5. For-Each Control Structure

Often, you will use the For loop to iterate over some container object
 to do something with each of it's elements. You do this first by asking
 how many elements the container has, to create the For loop, and inside
 the loop you access each element by it's index.

Just numbers of elements, and  the index itself, is of no interest at all
 in many cases. All you want, is to access all elements, no matter how many,
 and with which index. Here comes the For-Each loop, which allows you to
 just work this way.

The For-Each loop today exists in all commonly used programming languages,
 though the exact syntax varies greatly.

In principal, the For-Each syntax shall be available for all container
 objects, which *implement the iterable interface*. Means, that type
 internally provide some pointer to an alement and provide some method
 like *next* to forward this pointer from one to the next element.

Bottom line. **The For-Each abstracts away the need to count the elements
 and to access them via their index.**

See code in
 [`./codes/jv/j5foreachstruct.java`](./codes/jv/j5foreachstruct.java)

## <a name="id20230617o0546"></a> 6. ForEach Method

This is no more a loop on code level. On code level, this is a pure method
 call, in fact one with a function as a parameter. The method is provided
 by the container object, and the function parameter is what formerly
 was inside the loop body.

The former control structure is transformed into a method call. A method
 call syntactically takes fewer lines as a control structure.

This now involves the *functional programming paradigm* in so far, as we
 have a function passed as a parameter.

The function to be passed can be implemented in the following styles:
- Write a dedicated fully named function and pass the name of it (not in Java)
- Write an anonymous function inside the parameter parenthesis (not in Java)
- Write a lambda expression inside the parameter parenthesis (since Java 8)

Bottom line. **The For-Each method abstracts away the control structure
 syntax and condenses it into a simple method.**

See codes in
 - [`./codes/jv/j6foreachmethod.java`](./codes/jv/j6foreachmethod.java) (only with lambda)
 - [`./codes/js/js6foreachmethod.js`](./codes/js/js6foreachmethod.js) with [`./codes/js/js6foreachmethod.html`](./codes/js/js6foreachmethod.html) shows all three flavours


## <a name="id20230617o0548"></a> 7. Recursion

Recursion is actually not a loop but a special case of function calling.
 A function calls itself again and again, until some end condition is met.

- It is a loop in so far, as the same code is executed again and again.
- But it is **not** a loop, in so far, as which each call a new stack
  frame is allocated, and the local variables are distinct individuals
  each time.

With respect to the kind of tasks it solves, where ever you solve a task
 with a loop, you can as well use recursion, and vice versa. But of course
 for each task, the one or the other technique will suite better.

See code in
 [`./codes/jv/j7recursion.java`](./codes/jv/j7recursion.java)

Have fun

---

<sup><sub>[file 20231103°1721, project 20231103°1711] ⬞Ω</sub></sup>
