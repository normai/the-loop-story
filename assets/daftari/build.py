﻿#!/usr/bin/env python
# file         : 20190407°0521 daftari/build.py
# version      : • 20210810°1121 Script is callable multiple ways
# summary      : Build Daftari by minifying and combining files
# license      : MIT License
# copyright    : © 2019 - 2023 Norbert C. Maier
# requires     : • Python 3.6 or higher path set • Java path set • Google Closure Compiler
# encoding     : UTF-8-with-BOM
# see          : Python link collection which was shifted to page 20190403°0411
#                 downtown.trilo.de/svn/daftaridev/trunk/straypages/strayfour.html

"""
   This minifies and combines the Daftari JavaScript modules
"""

import os, os.path, pathlib, subprocess, sys

sBinGoCloCom = 'G:/work/gipsydrive/app.composer/trunk/bin/goclocom/closure-compiler-v20210406.jar'

print('* Daftari build *')
print('[Dgb] __file__ = ' + __file__)

# After https://stackoverflow.com/questions/3718657/how-do-you-properly-determine-the-current-script-directory [ref 20210810°1112]
import inspect
sFilnam = inspect.getframeinfo(inspect.currentframe()).filename
sThisScriptDir = os.path.dirname(os.path.abspath(sFilnam))
print('[Dgb] xPath = ' + sThisScriptDir)

# Goto folder where this script resides
##os.chdir(os.path.dirname(__file__))                                  # Does not work with commandline 'python.exe build.py'
os.chdir(sThisScriptDir)
print('[Dgb] CWD = ' + os.getcwd())

# Work off third-party build scripts
os.system("python.exe ./jslibs/dropdown/build.py silent")              # Planned to be minified-only like CanvasGear etc.
###os.system("python.exe ./jslibs/modpopdialog/build.py silent")       # Planned to be minified-only like CanvasGear etc.

print(' * Daftari Builder continues ..')

# Get time of target file
timLast = 0                                                            # For below comparisons — not yet used
p = pathlib.Path('./jsi/daftari.min.js')
if p.is_file() :
   timLast = os.path.getmtime('./jsi/daftari.min.js') 

aFils2Combi = [ './jsi/dafutils.js'                                    # Mandatory to be combined
               , './jsi/blogs.js'
                , './jsi/dafcanary.js'
                 , './jsi/dafdash.js'
                 , './jsi/dafdispatch.js'
                 , './jsi/daflingos.js'
                 , './jsi/dafmenu.js'
                 , './jsi/dafsitmap.js'
                 , './jsi/daftarigo.js'
                 , './jsi/gallery-js-autoflick.js'
                 , './jsi/gallery-js-xbtooltip.js'
                  , './jslibs/tigra_tree_menu/tree.js'
                   , './jsi/daftari.js'                                # The collector file must be the last one — Why?!
                    ]

## Is any of the source files younger than the target file?
bBuild = False
for sFile in aFils2Combi :
   if timLast < os.path.getmtime(sFile) :
      bBuild = True

# Assemble commandline
sCmd = 'java.exe -jar ' + sBinGoCloCom
for sFile in aFils2Combi :
   sCmd += ' ' + sFile
sCmd += ' ' + '--js_output_file' + ' ' + './jsi/daftari.min.js' \
      + ' ' + '--create_source_map' + ' ' + './jsi/daftari.min.js.map' \
       + ' ' + '--formatting PRETTY_PRINT --charset UTF-8'
# For testing directly on the console, here is the created command (asof 20210427°1121) :
# • java.exe -jar G:/work/gipsydrive/app.composer/trunk/bin/goclocom/closure-compiler-v20210406.jar ./jsi/dafutils.js ./jsi/blogs.js ./jsi/dafcanary.js ./jsi/dafdash.js ./jsi/dafdispatch.js ./jsi/daflingos.js ./jsi/dafmenu.js ./jsi/dafsitmap.js ./jsi/daftarigo.js ./jsi/dafterm.js ./jsi/gallery-js-autoflick.js ./jsi/gallery-js-xbtooltip.js ./jslibs/tigra_tree_menu/tree.js ./jsi/daftari.js --js_output_file ./jsi/daftari.min.js --create_source_map ./jsi/daftari.min.js.map --formatting PRETTY_PRINT --charset UTF-8

# Provide function to see all console output immediately — Does not work as expected, does it?
# After ref 20210427°1132 posting by Thang on 2015-May-03'06:00
def run_command(command) :
   p = subprocess.Popen( command
                        , stdout=subprocess.PIPE
                         , stderr=subprocess.STDOUT
                          )
   return iter(p.stdout.readline, b'')

# Build only if any file was new
if bBuild == True :
   print(' - Daftari building ...')
   for line in run_command(sCmd) :
      print(line)
else :
   print(' - Daftari is up-to-date')

# Bye
input('Daftari Builder done. Press Enter to exit.')

# ======================================================
# Versions :
#  • 20210810°1121 Script is callable multiple ways now, e.g.
#    ◦ X:\work\daftaridev\trunk\daftari>build.py
#    ◦ X:\work\daftaridev\trunk\daftari>python.exe build.py
#    ◦ X:\work\daftaridev>trunk\daftari\build.py
#    ◦ X:\work\daftaridev>python.exe trunk\daftari\build.py
#  • 20210518°1221 No more calling CanvasGear, PurpleTerms and SlideGear build.py
#  • 20210427°1111 Simplify, first attempt, probably with deficiencies
#  • 20210416°1631 Set compilation level 'ADVANCED' — Too many errors!
#  • 20201212°1551 Refactor • First combine, then minify • Create map files
# ======================================================
