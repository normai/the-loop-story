﻿<img src="./jsi/imgs23/20100828o1621.blocconotes.v1.x0128y0128.gif" align="right" width="128" height="128" data-dims="x500y621" style="margin-left:1.7em;" alt="Logo 'Keyboard Kendo'">

# Daftari &nbsp; <sup><sub><sup>*v0.7.6.g*</sup></sub></sup>

Slogan : Static multilingual webpage editor in JS/PHP

Status : Useful on localhost so far

Installation : Copy the folder to your website and open daftari/index.html in your browser
(Well, this is the objective, in practice it may not yet be so easy.)

Usage : See the
[User Manual](https://downtown.trilo.de/svn/daftaridev/trunk/daftari/docs/help.html)
and inspect the HTML code of those pages

License : [GNU AGPL v3](https://downtown.trilo.de/svn/daftaridev/trunk/daftari/license.txt)

Copyright : © 2011 - 2023 Norbert C. Maier

Norbert, 2023-Mar-08 &nbsp; <del>2022-Feb-23</del> <del>2021-May-22</del>

<sup><sub><sup>[project 20100817°1234] ⬞Ω</sup></sub></sup><!-- folder 20140828°0321 file 20140828°0322 -->
