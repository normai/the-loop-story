/*!
 *  id : file 20110805°1401 daftari/jsi/dafdispatch.js
 *  summary : This module (mainly) processes the dispatcher calls after the page loaded
 *  license : GNU AGPL v3
 *  copyright : © 2011 - 2023 Norbert C. Maier
 *  authors : ncm
 *  callers : mainly • func 20110813°2221 Daf.Core.daftari
 */

// Options for JSHint [seq 20190423°0421`04]
/* jshint laxbreak : true, laxcomma : true */
/* globals blogfadein, Cvgr, Daf, displayCookies, jQuery, Sldgr, Trekta */ // DAF_PAGENODES DAF_TREE_TPL

/**
 *  This namespace constitutes the Daftari root namespace
 *
 *  @id 20170923°0421`03
 *  @note See topic 20170923°0441 'javascript namespace organisation'
 *  @c_o_n_s_t — Namespace
 */
Daf = window.Daf || {};

/**
 *  Announce namespace
 *
 *  @id 20190106°0311`12
 *  @c_o_n_s_t — Namespace
 */
Trekta = window.Trekta || {};

/**
 *  This namespace DafDipat ..
 *
 *  @id 20180322°0511
 *  @see ref 20150317°0941 'stackoverflow → creating list of objects'
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Dspat =
{
   /**
    *  This class provides a Feature object
    *
    *  @id 20150411°0521
    *  @status See chg 20190417°0453 'retire class 20180322°0511 Fiture'
    *  @note 20190315°0723 : The feature seems to be too bloated, dismantle it.
    *  @note The property prefix 'Fi' is not nice, but it allows better plain text search.
    *  @constructor —
    */
   RtFiture : function()
   {
      'use strict';
      this.FiIdent = "";                                                // Daftari internal feature identifier. Proforma, possibly not used yet
      this.FiCreate = false;                                            // Tells whether the feature shall be created if it does not yet exist
      this.FiHtAttrib = "";                                             // Target HTML attribute class or id, where the FiHtId shall be searched
      this.FiHtClass = "";                                              // HTML element class name (some features use class, others use the id) [new 20160618°0451]
      this.FiHtId = "";                                                 // Search string to locate the feature in a page
      this.FiHtTag = "";                                                // HTML tag, where the feature shall be searched on the page, e.g. DIV or P
      this.FiSumary = "";                                               // Short statement to be shown as help or tooltip
      this.FiValuet = "";                                               // HTML block generated after above other fields, then used e.g. as innerHTML
   }

   /**
    *  This function builds and injects the Daftari main menu plus other things
    *
    * @id 20190417°0441
    *  @callers : Only • daftari()
    *  @return {undefined} —
    */
   , process_1_MainMenu : function ()
   {
      'use strict';

      // () Prologue [line 20190417°0443]
      var s1 = '';

      // (1) Find the place to insert the DaftariNotebookIcon [algo 20110813°2223]
      // (1.x) Search page element [seq 20210518°1632] 'DaftariMainMenuIcon_20120827o0314'
      var eIconTarget = document.getElementById(Daf.Const.sKey_DAFTARI_NOTEBOOK_ICON_4);

      // (1.4) Automatically find location for the icon [seq 20190416°0625]
      // If nothing was found so far, use the first suited paragraph as fallback
      // See issue 20190416°0623 'Find location to put the main menu icon'
      // Provisory shutdown with chg 20210518°1641. If user wants no icon, so what.
      if (Daf.Const.b_Toggle_FALSE) {
         if (! eIconTarget) {
            // The icon must *not* be inserted into the CakeCrumbs bag [seq 20190416°0627]
            var eCake = document.getElementsByClassName(Daf.Dspat.Config.sFiturIdent_Cakecrumbs_FiHtClass)[0];
            var esParas = document.getElementsByTagName('P');
            for (var iNdx = 0; iNdx < esParas.length; iNdx++) {
               eIconTarget = document.getElementsByTagName('P')[iNdx];
               if ( typeof eCake !== 'undefined' ) {
                  if ( eCake.contains(esParas[iNdx]) ) {
                     continue;
                  }
               }
               eIconTarget = esParas[iNdx];
               break;
            }
         }
      }

      // () Assemble the Page Menu icon HTML [seq 20190422°0621]
      var sIcon2 = '';
      if ( Daf.Start.isLocalhost() || Daf.Const.b_IncludeExperimentalDropdownMenu ) {
         sIcon2 =  '<img'
                 + ' id="mimg"'
                 + ' onclick="Daf.Mnu.Tfd.dropMenu1Load(\'divTfddmMainMenu\', this)"'
                 + ' src="' + Trekta.Utils.s_DaftariBaseFolderRel + '/'
                             + Daf.Const.sFILE_DAFTARI_ICON_NOTEBOOK_MAIN + '"' // 'imgs23/20100814o021302.logo-edit-small-v015.png'
                 + ' onmouseover="this.style.cursor=\'pointer\';'
                 +                ' Daftari.XBT(this, {id:\'xbtAbout\'});"'
                 + ' alt="Daftari Page Menue 2"'
                 + '>'
                  ;
      }

      // () Inject the image (if a location for it was assigned) [line 20110813°2226]
      if (eIconTarget) {                                                // Condition newly necessary with chg 20210518°1641
         eIconTarget.innerHTML = eIconTarget.innerHTML + sIcon2;
         eIconTarget.className = Daf.Const.sKey_DAFTARI_NOTEBOOK_ICON_4;
      }

      // () Notification [seq 20110813°2232]
      s1 = '[Checkpoint 20110813°2225] Notebook icon link was built'
           + "\n &nbsp; • Trekta.Utils.s_DaftariBaseFolderRel = '" + Trekta.Utils.s_DaftariBaseFolderRel + "'"
            + '\n &nbsp; • Daf.Const.sFILE_DAFTARI_ICON_NOTEBOOK_MAIN = ' + Daf.Const.sFILE_DAFTARI_ICON_NOTEBOOK_MAIN
             ;
      Trekta.Utils.outDbgMsg(s1);

      // () Inject hidden bulk area [seq 20110813°2224]
      // Summary : Inject the hidden bulk area (the Daftari menues, forms, etc.)
      // Note : It works, but shouldn't it be better done *after* the document is loaded?
      // Note : Remember issue 20150223°1721 'assigning eBody.innerHTML destroys canvasgear'
      var eBody = document.getElementsByTagName('BODY')[0];

      // A frameset has no body [seq 20210518°0911] Quick'n'dirty exit
      // E.g. reference page 20070814°1921 with TreeView switched on
      if (typeof eBody === 'undefined') {
         return;
      }

      var eDivAjaxDebugArea = document.createElement('DIV');
      eDivAjaxDebugArea.innerHTML = ''
         + ' <form action="" method="post" style="visibility:hidden;">'
         + '  <textarea id="textarea_intermediate" name="cmd_post_textarea" rows="25"'
         +       ' cols="' + Daf.Mnu.Vars.iSet_AjaxDebugAreaWidth + '" wrap="on"'
         +       ' style="border:2px solid Orange; border-radius:0.9em;"'
         +     '>'
         +     'This is a temporary debug area.'
         +   '</textarea>'
         + ' </form>'
          ;
      eBody.appendChild(eDivAjaxDebugArea);                             // [line 20150223°1731]

      // () Inject the 'about' tooltip content [seq 20110813°2233]
      var eDiv1 = document.createElement('DIV');
      eDiv1.className = 'xbtooltip';
      eDiv1.id = 'xbtAbout';
      eDiv1.style.display = 'none';
      eDiv1.innerHTML = '<span style="color:gray; text-align:center;">'
                       + '<big><b>Daftari<br />' + Daf.Const.s_VERSION_NUMBER
                        + '</b></big>'
                         + '</span>'
                          ;
      eBody.appendChild(eDiv1);

      // (x) Inject Lingos Selection Area [algo 20120902°1721]
      // Summary : Find the target element to insert the feature
      // Note : The algo is pretty the same as algo 20110813°2223 above.
      // Note : General remark about injecting elements: It's always the choice
      //    between either (1) Make a string and then manipulate e.innerHTML or
      //    e.value or (2) Make element by document.createElement() and append
      //    that. I haven't figured out exactly when to prefere what. Compare
      //    issue 20120508°0921 'IE fails to write innerHTML'. [note 20120902°1723]

      // (x.1) Find element with search string one.
      // Former alternatives "if (!eLingos ) {eLingos = document.getElementById(..); }" deleted with chg 20210518°1611
      var eLingos = document.getElementById(Daf.Const.sKey_LINGOS_SELECTION_AREA_3);  // 'LingoSelectionArea_20120901o2023'

      // () Lingos target element found? Work in strict mode
      //  No more fallback — No target element, no Lingos [chg 20210519°0921]
      if (! eLingos) {
         return;                                                        // Strict mode — No target element, no Lingos [chg 20210519°0921`12]
      }

      // () Retrieve visible lang select block plus invisible tooltip texts [seq 20110813°2234]
      var aBlocks = [];
      aBlocks = Daf.Lngo.wafaili_lingos();
      var sLingos = aBlocks[0];
      var sXbts = aBlocks[1];

      // () Apply the invisible tooltip texts [seq 20110813°2235]
      var eDivLingos = document.createElement('DIV');
      eDivLingos.className = 'xbtooltip';
      eDivLingos.innerHTML = sXbts;
      eBody.appendChild(eDivLingos);

      // () Insert Lingos HTML fragment [seq 20120915°1233]
      // Note. This intermediate div is in fact superfluous. We can insert the
      //  HTML fragment directly into the marker div as well [chg 20210518°1621]
      eLingos.innerHTML = sLingos;
   }

   /**
    *  This function works off one feature from the Features List
    *
    *  @id 20150411°0511
    *  @status Developing
    *  @callers Only • func 20110813°2221 Daf.Core.daftari
    *  @return {undefined} —
    */
   , process_blog : function()
   {
      'use strict';

      if (document.getElementById('id20140625o1651_BlogFadeIn') === null) {
         return;
      }

      // Define helper function [seq 20190411°0511]
      // This replaces an anonymous function to help NetBeans populate its Navigator pane
      // Remember issue 20190411°0221 'NetBeans Navigator pane stays empty'
      var fNonAnonCallback_blogfadein = function()
      {
         //'use strict';                                                // Unnecessary directive
         blogfadein('');
      };

      // () Pull-behind blog.js [seq 20180509°0311]
      // See howto 20181229°1943 'summary on pullbehind'
      // See ref 20180509°0252 'jQuery API → jQuery.getScript' — this was
      //   used to load blogs.js before the introduction of serial-pull.
      // note : blogs.js depends on jQuery in seq 20140829°1333 'blog block into div'
      // See summary 20190404°0645 'the pull-behind proceedings'

      // () Prepare serial-pull blogs.js [seq 20190404°0711]
      //  Using an alternative style also understood by IE11
      // Remember issue 20190411°0721 'IE11 rejects bracketed dynamic properties'
      // Process issue 20190404°0721 'why is jquery loading icon unnecessarily'
      // Remember : 'job_2' .. 'job_5' are serial, 'job_6' is final
      var sLoc = Trekta.Utils.s_DaftariBaseFolderAbs;
      var oJobs = {};

      // Do pull-behind only if not cooked [workaround 20210427°1221 'Handle Blogs module']
      // See issue 20210427°1223 — Settle workaround 20210427°1221
      if ( ! Trekta.Utils.bUseMinified ) {
         Object.defineProperty ( oJobs, 'job_2'
                             , { value : sLoc + '/jsi/blogs.js'
                              ,  writable : true
                               , enumerable : true
                                , configurable : true
                                 });
      }

      // Set blog ignition
      Object.defineProperty ( oJobs, Daf.Core.sPullJob_No6              // not verbatim '6' but 'last' job
                             , { value : fNonAnonCallback_blogfadein
                              ,  writable : true
                               , enumerable : true
                                , configurable : true
                                 });

      // () Do the pull
      Trekta.Utils.pullScriptBehind ( sLoc + '/jslibs/jquery/jquery.js'  // 'job_1' // [chg 20200806°1911`01]
                                     , Daf.Core.pullBehind_onLoad
                                      , Daf.Core.pullBehind_onError
                                       , oJobs
                                        );
   }

   /**
    *  This ignits the cake baking process
    *
    *  @id 20190418°0111
    *  @note chg 20190418°0331 'all ajax asynchronous, no more synchronous'
    *  @note See issue 20200103°0252 'Chrome → AJAX fails with file protocol'
    *  @callers Only • func 20110813°2221 Daf.Core.daftari
    *  @return {undefined} —
    */
   , process_cake1ignit : function()
   {
      'use strict';

      // Read token translations [seq 20161109°2044]
      // Remember issue 20200103°0252 'Chrome → AJAX fails with file protocol'
      // An alternative might be ref 20150516°0612 dean.edwards.name/weblog/2006/04/easy-xml/
      var sTgtDir = Daf.Dspat.Config.SitemapFolderAbsolute;

      // Read token translations from JS file [seq 20200103°0313]
      Trekta.Utils.pullScriptBehind ( sTgtDir + "/sitmaplangs.js"
                                     , Daf.Dspat.process_cake2build     // callback if loaded
                                      , function() { Daf.Dspat.process_cake2build(null); }  // callback if failed
                                       , null                           // oJobs
                                        );

   }

   /**
    *  This function processes the newly made CakeCrumbsBag by • putting it
    *   into the page and • possibly sending it via Ajax to make it static
    *
    *  @id 20190413°0741
    *  @callers Only • callback from 20190418°0111Daf.Dspat.process_cake1ignit
    *  @param {string} sReadSitmapLangs — The content of the file read by Daf.Dspat.process_cake1ignit()
    *  @return {undefined} —
    */
   , process_cake2build : function(sReadSitmapLangs)
   {
      'use strict';

      // Process sitmaplangs file [seq 20190418°0117]
      // Remember issue 20200103°0252 'Chrome → AJAX fails with file protocol'
      // The condition helps against curious "Sitemap wrong" issue [condition 20200105°0515]
      if (sReadSitmapLangs !== null && sReadSitmapLangs !== '') {
         Daf.Lngo.dCacheDataReadFile = Daf.Sitmp.oSitlangs;
      }

      // [line 20190413°0743]
      var sCakeCrmbsNew = Daf.Sitmp.workoff_Cake_0_go();

      // (.) Feature 20190413°0711 'bake static cakecrumbs' [seq 20190413°0747]
      var bReplace = null;
      if ( Daf.Vars.bToggle_BakeStaticCakecrumbs ) {                    // true while normal operation
         bReplace = Daf.Dspat.process_cake3bakeStatic(sCakeCrmbsNew);
      }

      // (x) Retrieve target element [seq 20160618°0251]
      // browsercompatibility : IE8 will not show the CakeCrumbs
      var eleTarget = document.getElementsByClassName(Daf.Dspat.Config.sFiturIdent_Cakecrumbs_FiHtClass)[0];  // 'DafFurnitureCakecrumbs_Bag'

      // Update cakecrumbs bag [line 20190415°0543]
      if ( bReplace && (typeof eleTarget !== 'undefined') ) {
         
         // Here finally materializes the feature [line 20111222°1653]
         eleTarget.innerHTML = sCakeCrmbsNew;

         // Refresh Lingos [seq 20190416°0451]
         var sLng = Trekta.Utils.getCookie(Daf.Lngo.sCookieNameLanguage);
         Daf.Lngo.langSplit(sLng);
      }

      // Complete the FOUC fighting experiment [seq 20190413°0123]
      if ( Daf.Vars.bToggle_FoucFighter ) {
         document.body.style.display = 'block';                         // Just a guess. To know exactly, look the browsers default stylesheet
      }
   }

   /**
    *  This function bakes static cakecrumbs
    *
    *  @id 20190413°0713
    *  @callers Only • func 20190413°0741 Daf.Dspat.process_cake2build
    *  @param {string} sCakeCrmbsNew — The newly built CakeCrumbsBag
    *  @return {boolean} — Flag telling whether cakecrumbs were replaced or not
    */
   , process_cake3bakeStatic : function(sCakeCrmbsNew)
   {
      'use strict';

      // Are there cakecrumbs on this page? [seq 20190421°0131]
      var eCake = document.getElementsByClassName(Daf.Dspat.Config.sFiturIdent_Cakecrumbs_FiHtClass)[0];  // 'DafFurnitureCakecrumbs_Bag'
      if ( typeof eCake === 'undefined' ) {
         // No cakecrumbs, nothing to bake
         return false;
      }

      // (.1) No wafailiskip in the baken crumbs [line 20190415°0541]
      sCakeCrmbsNew = sCakeCrmbsNew.replace(/ data-wafailiskip=\"yes\"/g, '');

      // (.2) Manipulate fragment from innerHTML [seq 20190415°0521]
      //  Workaround issue 20190415°0511 'quirks with innerHTML'
      //  Remove self-closing slash, which appears on the br and img tags
      var sCompareNew = sCakeCrmbsNew.replace(/ \/\>/g, '>');
      var sCompareOld = Daf.Lngo.sSaveStaticCakecrumbs.replace(/<div><\/div>/g, '');  // introduced for page 20150314°0511 strayone.html

      // (.3) Cope with different linebreaks [seq 20190415°0523]
      //  Just strip all newline chars to ease below string comparison
      sCompareOld = sCompareOld.replace(/[\r\n]/g, '');
      sCompareNew = sCompareNew.replace(/[\r\n]/g, '');

      // (.4) Allow for closing tag indent [seq 20201129°1421] e.g. "\n <div>\n"
      // notes : • The closing tag is in the line behind the inspected tag.
      //  • This workaround goes together with PHP variable 20201129°1417 usage.
      //  • I just could not do the PHP replacement exactly over the linebreak.
      sCompareOld = sCompareOld.trimEnd();                              // Does not work with IE, but for IE static-baking is switched off below anyway
      sCompareNew = sCompareNew.trimEnd();

      // Switch off static-baking with some browsers [seq 20190417°0213]
      // See issue 20190417°0211 'Edge/IE10/FiFo are messing innerHTML'
      // note : So far only Chrome and Opera seem to remain for static-baking
      if ( // Trekta.Utils.bIs_Browser_Edge ||                          // Experiment 20201129°1413 allow Edge for static-baking
            Trekta.Utils.bIs_Browser_Explorer
          || Trekta.Utils.bIs_Browser_Firefox
           ) {
         return false;
      }

      // (.4) Are the static cakecrumbs uptodate? [seq 20190413°0753]
      var bReplace = (sCompareOld !== sCompareNew);
      if (bReplace) {

         // (4.1) Build target url [seq 20190413°0753] (after 20120830°0453)
         // Compare e.g. 20120830°0451 Daf.Mnu.Edt.EditFinishTransmit
         var sTarget = Trekta.Utils.s_DaftariBaseFolderRel + '/php'
                      + '/' + 'Go.php'
                       + '?cmd=' + 'cakecrumbsbag'
                        + '&targetid=' + Daf.Dspat.Config.sFiturIdent_Cakecrumbs_FiHtClass // "DafFurnitureCakecrumbs_Bag"
                         + '&targetfile=' + Trekta.Utils.getFileNameFull()  // E.g. 'http://localhost/trekta/demomatrix/trunk/pages/gen/intro.html' [issue 20210426°0911 fails with https]
                          ;

         // (4.2) Finally do the wanted job
         // todo : Make an async request with some feedback gimmick success/fail.
         //    Supplement Daf.Core.Ajax.MakeRequest with flexible callback [todo 20190413°0755]
         Daf.Core.Ajax.MakeRequest(sTarget, sCakeCrmbsNew);
      }

      return bReplace;
   }

   /**
    *  This function processes possible CanvasGear elements
    *
    *  @id 20190404°0911
    *  @see todo 20190404°0923 'unify feature pull-behind'
    *  @see todo 20190404°0921 'finetune canvasgear detection'
    *  @see issue 20190404°0925 'canvasgear delayed with pull-behind'
    *  @callers Only • Daf.Core.daftari
    *  @return {undefined} —
    */
   , process_canvasgear : function()
   {
      'use strict';

      // () Do CanvasGear elemens exist on page? [line 20190404°0913 (after 20140815°0941)]
      // todo : Finetune the CanvasGear detection. Just looking for canvas tags
      //    is too bold, there could be ordinary canvases. Look for the 'data-cvgr'
      //    attribute. [todo 20190404°0921 'finetune canvasgear detection'
      // note : This elements list is thrown away, and if canvasgear.js is
      //    loaded then, it will create the same list again. This looks not so
      //    elegant. But since fadeinfiles.js shall also work as a standalone
      //    script, the list must be created there anyway. So the only option
      //    were, if we find an earmark cheaper than a class.
      var canvases = document.getElementsByTagName("canvas");           // Todo : use Daf.Dspat.sEarmark_

      // Ready? [line 20190404°0914]
      if ( canvases.length < 1) {
         return;
      }

      // Determine script name [seq 20210427°1231]
      var sScriptCvg = 'canvasgear.combi.js';

      // Simple pull [seq 20190404°0915]
      var sLoc = Trekta.Utils.s_DaftariBaseFolderAbs;
      Trekta.Utils.pullScriptBehind ( sLoc + '/jslibs/canvasgear/' + sScriptCvg  //
                                     , ( function() { Cvgr.startCanvasGear(); } )  //
                                      , null                            //
                                       , null                           //
                                        );
   }

   /**
    *  This function processes the possibly wanted ~~Daf.Dash script
    *
    *  @id 20190403°0521
    *  @callers : Only • Daf.Core.daftari()
    *  @return {undefined} —
    */
   , process_dashboard : function()
   {
      'use strict';

      // Process self-closing feature [seq 20161009°0131]
      // note : Using name tag as earmark may be no so good idea, because
      //    the name tag seems related with the id tag [note 20190424°0411]
      var eBeacon = document.getElementsByName(Daf.Dspat.sEarmark_Dashboard)[0];  // 'formPinLoginWindow'
      if (typeof eBeacon === 'undefined') {
         return;
      }

      // Notification [seq 20161009°0133]
      var s = "[Dbg 20161009°0741] Feature Daf.Dash.IeOnBlur.InitiateSelfClosing detected.";
      Trekta.Utils.outDbgMsg(s);

      // () Guarantee wanted script [seq 20190403°0523]
      var sLocation = Trekta.Utils.s_DaftariBaseFolderAbs;
      var sScriptToLoad = sLocation + '/jsi/dafdash.js';
      Trekta.Utils.pullScriptBehind ( sScriptToLoad                     //
                                     , function() { Daf.Dash.IeOnBlur.initiateSelfClosing(); }  // The wanted feature
                                      , null                            //
                                       , null                           //
                                        );
   }

   /**
    *  Process the cookie display feature ..
    *
    *  This calls the function displayCookies() if it exists in the page.
    *  Function displayCookies exists only in page daftari/panels/tools.html.
    *
    *  @id ~20190403°0531
    *  @callers : Only • func 20110813°2221 Daf.Core.daftari()
    *  @return {undefined} —
    */
   , process_displayCookies : function()
   {
      'use strict';

      var s1 = "[Flowpoint 20170901°1222] Cookies — ";
      try {
         displayCookies();                                              // Exists in tools.html
         s1 += "Function displayCookies was called.";
      }
      catch (ex) {
         // This happens on all pages except tools.html, does it?!
         s1 += "Page has no displayCookies-function.";
      }
      Trekta.Utils.outDbgMsg(s1);
   }

   /**
    *  This function processes possible FadeInFiles elements
    *
    *  @id 20190404°0821
    *  @note Consider todo 20190404°0923 'unify feature pull-behind'
    *  @callers Only • Daf.Core.daftari
    *  @return {undefined} —
    */
   , process_fadeinfiles : function()
   {
      'use strict';

      // () Do FadeInFiles elements exist on page? [line 20190404°0823] old flavour
      // This line is redundant with fadeinfiles.js line 20141117°1234
      // Remember issue 20190404°0824 'redundant checking for FadeInFiles earmarks'
      var els = document.getElementsByClassName(Daf.Dspat.sEarmark_FadeInFiles_Class);  // 'fadein'

      // Check for FadeInFiles elements in the page [line 20190406°0221] new flavour
      // This line is redundant with fadeinfiles.js line 20190406°0112
      var nodelist = document.querySelectorAll('[data-fadein]');

      // Ready? [line 20190404°0825]
      if (( els.length < 1) && ( nodelist.length < 1)) {
         return;
      }

      // Activate script [seq 20190404°0827]
      var sScript = 'fadeinfiles.combi.js';
      var sLocation = Trekta.Utils.s_DaftariBaseFolderAbs + '/jslibs/fadeinfiles/';
      Trekta.Utils.pullScriptBehind ( sLocation + sScript
                                     , ( function() { Trekta.Fadin.fadeInFiles(); } )
                                      , null
                                       , null
                                        );
   }

   /**
    *  This function processes possible Fancytree elements
    *
    *  @id 20190403°0131
    *  @note Process todo 20190403°0131 'Finetune chain-pull-behind'
    *  @callers Only • daftari.js Daf.Core.daftari()
    *  @return {undefined} —
    */
   , process_fancytree : function()
   {
      'use strict';

      // () Do any Fancytree elements exist on page? [lin 20190403°0132]
      var elsFancy = document.querySelectorAll('[' + Daf.Dspat.sEarmark_Fancytree + ']');  // 'data-fancytree'

      // () There are no Fancytree elements on board [lin 20190403°0133]
      if ( elsFancy.length < 1) {
         return;
      }

      // () Prepare pull-behind calls [seq 20190403°0134]
      var sLocation = Trekta.Utils.s_DaftariBaseFolderAbs;
      var sScript1ToLoad = sLocation + '/jslibs/jquery/jquery.js';      // [chg 20200806°1911`02]
      var sScript3ToLoad = sLocation + '/jslibs/fancytree/dist/jquery.fancytree-all-deps.js';  // [chg 20200806°1911`04]

      // [line 20190403°0135]
      var oJob = {
         'fancytree' : elsFancy
         // , 'job2' : sScript2ToLoad                                   // [chg 20200806°1911`05]
         , 'job3' : sScript3ToLoad
      };

      // () Perform chain-pull-behind [line 20190403°0621]
      // Remember issue 20190403°0611 'Fancytree pull-behind loading sequence'
      Trekta.Utils.pullScriptBehind ( sScript1ToLoad
                                     , Daf.Dspat.process_fancytree_onLoad
                                      , Daf.Dspat.process_fancytree_onError
                                       , oJob
                                        );
   }

   /**
    *  This event handler reacts if Fancytree (or other library) failed loading
    *
    *  @id 20190403°0141
    *  @note Process todo 20190404°0131 'Finetune chain-pull-behind'
    *  @callers : Only • browser via pull-behind
    *  @param {string} sScript — ..
    *  @param {Object} oJob — ..
    *  @return {undefined} —
    */
   , process_fancytree_onError : function(sScript, oJob)
   {
      'use strict';
      console.error('[Error 20190403°0141] Failed to load "' + sScript + '" ' + oJob);
   }

   /**
    *  This event handler reacts if Fancytree (or other library) is loaded
    *
    *  @id 20190403°0151
    *  @note Process todo 20190403°0131 'Finetune chain-pull-behind'
    *  @callers : Only • browser via pull-behind
    *  @param {string} sScript — The script to load, as pull-behind has got it
    *  @param {object/anytype} xJob — Something forwarded from the original pull-behind caller
    *  @param {type} bWasAlreadyLoaded — Flag telling whether script was already loaded
    *            or is newly loaded. Not used yet, but I want provide this information
    *  @return {undefined} —
    */
   , process_fancytree_onLoad : function ( sScript
                                          , xJob
                                           , bWasAlreadyLoaded
                                            )
   {
      'use strict';

      // Prologue [seq 20190403°0152]
      var sJobType = typeof xJob;
      var bAlert_OnLoadHandler = false;
      if (bAlert_OnLoadHandler) {
         alert ( 'This is callbackOnload' + '\n\nscript = ' + sScript
                + '\nxJob = ' + xJob + '\ntypeof job = ' + sJobType
                 + '\nbWasAlreadyLoaded = ' + bWasAlreadyLoaded
                  );
      }

      // It might be Fancytree [seq 20190403°0153]
      if ( sJobType === 'object' ) {

         // Is it job 2 from fancytree-pulling [seq 20190403°0623]
         if (( 'job2' in xJob ) || ( 'job3' in xJob )) {

            var sJob = xJob['job2'] ? 'job2' : 'job3';

            // Process the job object [seq 20190403°0625]
            var sScriptToLoad = xJob[sJob];
            delete xJob[sJob];

            // () Perform batch-pull-behind [line 20190403°0627]
            Trekta.Utils.pullScriptBehind ( sScriptToLoad
                                           , Daf.Dspat.process_fancytree_onLoad
                                            , Daf.Dspat.process_fancytree_onError
                                             , xJob
                                              );
            // Ready
            return;
         }

         // Is it Fancytree? [seq 20190403°0154]
         if ( 'fancytree' in xJob ) {

            // Loop over the Fancytree elements [seq 20190403°0155]
            var elsFancy = xJob['fancytree'];
            for (var i = 0; i < elsFancy.length; i++) {

               // Process each element [seq 20190403°0156]
               // Remember note 20190403°0627 'Daftari jQuery usage'
               var sAttri = elsFancy[i].getAttribute('id');
               var e = jQuery('#' + sAttri);

               // Here you need a jQuery object, not a HTML DOM element
               //  Remember issue 20190403°0231 'error "fancytree is not a function"'
               e.fancytree({minExpandLevel : 0});

               // Add connector lines [line 20200806°1921]
               jQuery(".fancytree-container").addClass("fancytree-connectors");
            }
         }
      }
      // [seq 20190403°0551]
      else if ( xJob === 'jquery.js') {
         //alert ("[Dbg 0190403°0551] Loaded " + xJob);
      }
      // [seq 20190403°0552]
      else if ( xJob === 'jquery-ui.custom.js') {
         //alert ("[Dbg 0190403°0551] Loaded " + xJob);
         var xDbg = xDbg;
      }
   }

};                                                                      // End namespace Daf.Dspat


/**
 *  This function just prints a demo
 *
 *  @id 20190417°0523
 *  @see chg 20190417°0453 'retire class 20180322°0511 Daf.Dspat.Fiture'
 *  @callers : Only • func 20110813°2221 Daf.Core.daftari
 *  @return {undefined} —
 */
Daf.Dspat.process_rtFitursDemo = function ()
{
   'use strict';

   // [condi 20190417°0525]
   if (Daf.Const.b_Toggle_FALSE) {
      return;
   }

   // (W.2) Fill features array [seq 20150411°0551]
   Daf.Dspat.rtFituresFill();

   // Loop over the features list [seq 20150411°0621]
   var s45 = '';
   for (var i = 0; i < Daf.Dspat.RtFiturs.length ; i++) {

      var fiture = Daf.Dspat.RtFiturs[i];

      // Debug — accumulate message [seq 20150411°0513]
      var a43 = [];
      
      // Leading message header
      if ( Daf.Dspat.sDbgMsg567Accu === '') {
         a43 = [[ 'FiIdent' , 27 ] , [ 'FiCreate' , 9 ] , [ 'FiHtAttrib' , 7 ]
              , [ 'FiHtId' , 21 ] , [ 'FiHtTag'  , 10 ] , [ 'FiSumary'  , 11 ]
               ];
         s45 = '&nbsp; ' +  Daf.Utlis.tabAlign(a43, ' ◦ ');
         Daf.Dspat.sDbgMsg567Accu = "[Dbg 20150411°0632 ⇘⇖] Demonstrate func Daf.Utlis.tabAlign with retired feature list" + '\n' + s45;
      }
      
      // The regular message line
      a43 = [[ fiture.FiIdent , 27 ] , [ fiture.FiCreate.toString() , 9 ]
           , [ fiture.FiHtAttrib , 7 ] , [ fiture.FiHtId , 21 ] , [ fiture.FiHtTag , 10 ]
            , [ ( fiture.FiSumary.length < 19 ? fiture.FiSumary : fiture.FiSumary.substring(0, 17) + '...' )  , 11 ]
             ];
      s45 = '\n &nbsp; ' +  Daf.Utlis.tabAlign(a43, ' • ');
      Daf.Dspat.sDbgMsg567Accu += s45;
   }

   // Output possibly accumulated message 20150411°0632
   if ( Daf.Dspat.sDbgMsg567Accu !== '') {
      Trekta.Utils.outDbgMsg(Daf.Dspat.sDbgMsg567Accu);
   }
};

/**
 *  This function processes the slideshow
 *
 *  @id 20190417°0531
 *  @callers : Only • func 20110813°2221 Daf.Core.daftari
 *  @return {undefined} —
 */
Daf.Dspat.process_slideshow = function ()
{
   'use strict';

   // Is any slideshow on the page? [seq 20160623°0251]
   var eExists = document.getElementsByClassName("Furniture_SlideGear");
   if ( eExists.length < 1 ) {
      return;
   }

   // Pull-behind slideshow [seq 20160623°0253] experimental but seems to work
   if (! Trekta.Utils.isScriptAlreadyLoaded('/slidegear.min.js') ) {
      var sJsFile = Trekta.Utils.bUseMinified ? '/jsi/daftari.min.js' : '/jsi/daftari.js';
      var sLocation = Trekta.Utils.retrieveDafBaseFolderAbs(sJsFile);
      var sScriptToLoad = sLocation + '/jslibs/slidegear/slidegear.min.js'; 
      Trekta.Utils.pullScriptBehind ( sScriptToLoad
                                     , function() { Sldgr.Func.startup(); }
                                      , null
                                       , null
                                        );
   }
};

/**
 *  This function retrieves the cornerstone pathes
 *
 *  @id 20150411°0651
 *  @callers : Only • Daf.Core.daftari()
 *  @return {undefined} —
 */
Daf.Dspat.provideCornerstonePathes = function()
{
   'use strict';
   var s1 = '';

   // (X) Set sitemap base folder as target prefix [seq 20160621°0231]
   // (X.1) determinate site base folder path e.g. "file:///G:/work/daftaridev/trunk/daftari"
   var sPath = Trekta.Utils.retrieveDafBaseFolderAbs('/sitmapdaf.js');
   Daf.Dspat.Config.SitemapFolderAbsolute = sPath;

   // Get path to this page [seq 20180310°1211]
   // note : The function possibly supplements a missing page name 'index.html'
   Daf.Dspat.Config.PathToThisPageFile = Trekta.Utils.getFileNameFull();

   // Notification
   if ( Daf.Const.b_Toggle_TRUE ) {
      var a51 = [[ ' • Daf.Dspat.Config.SitemapFolderAbsolute' , 43 ] , [ '= ' + Daf.Dspat.Config.SitemapFolderAbsolute , 9 ]];  // ✱
      var a52 = [[ ' • Daf.Dspat.Config.PathToThisPageFile' , 43 ] , [ '= ' + Daf.Dspat.Config.PathToThisPageFile , 9 ]];  // ✱
      var a53 = [[ ' • Trekta.Utils.s_DaftariBaseFolderAbs' , 43 ] , [ '= ' + Trekta.Utils.s_DaftariBaseFolderAbs , 9 ]];
      var a54 = [[ ' • Daf.Dspat.Config.SelfPageUrlRelative' , 43 ] , [ '= ' + Daf.Dspat.Config.SelfPageUrlRelative , 9 ]];
      var s51 = Daf.Utlis.tabAlign(a51, ' ');
      var s52 = Daf.Utlis.tabAlign(a52, ' ');
      var s53 = Daf.Utlis.tabAlign(a53, ' ');
      var s54 = Daf.Utlis.tabAlign(a54, ' ');
      s1 = "[Checkpoint 20150318°0907] provideCornerstonePathes"
          + "\n &nbsp; " + s51
          + "\n &nbsp; " + s52
          + "\n &nbsp; " + s53
          + "\n &nbsp; " + s54
           ;
      Trekta.Utils.outDbgMsg(s1);
   }
};                                                                      // Func provideCornerstonePathes end

// Chg 20190417°0453`02 'retire Fiture class' (now Daf.Dspat.RtFiture)
/**
 *  This function fills the array Daf.Dspat.RtFiturs with the features to be work off
 *
 *  @id 20150411°0641
 *  @see chg 20190417°0453 'retire class 20180322°0511 Daf.Dspat.Fiture'
 *  @callers •
 *  @return {undefined} —
 */
Daf.Dspat.rtFituresFill = function()
{
   'use strict';

   // (1) Standard output facility — Index 0 — Dismantled
   var f = new Daf.Dspat.RtFiture();
   f.FiIdent = '';                                                      // "20150321°0231"; // Feature id
   f.FiHtId = "id20150321o0231_StandardOutputDiv";                      // Trekta.Utils.sFurniture_OutputArea_Id is not yet available if minified?
   f.FiSumary = "Standard output facility";                             // Feature help string, e.g. for tooltip
   Daf.Dspat.RtFiturs.push(f);

   // (2) Cakecrumbs — Index 1
   f = new Daf.Dspat.RtFiture();
   f.FiIdent = Daf.Dspat.Config.sFiturIdent_Cakecrumbs_FiHtClass;       // "DafFurnitureCakecrumbs_Bag"
   f.FiCreate = false;
   f.FiHtAttrib = "id";                                                 // Probably not used, to be removed
   f.FiHtClass = Daf.Dspat.Config.sFiturIdent_Cakecrumbs_FiHtClass;     // "DafFurnitureCakecrumbs_Bag"
   f.FiHtId = "";
   f.FiHtTag = "";
   f.FiSumary = "This feature provides a crumbs sitemap, which is generated from sitemap.js.";
   f.FiValuet = "";
   Daf.Dspat.RtFiturs.push(f);

   // (3) Lingos — Index 2
   f = new Daf.Dspat.RtFiture();
   f.FiIdent = "20160609°0452.lingosflagsarea";
   f.FiCreate = false;
   f.FiHtAttrib = "id";                                                 // This info may be redundant since we have separate fields for id and class now [note 20160618°0453]
   f.FiHtClass = "";
   f.FiHtId = "LingoSelectionArea_20120901o2023";                       // Superfluous — At least, I noticed no error while it was wrong [Chg 20210518°1611`01]
   f.FiHtTag = "";
   f.FiSumary = "The lingo flags area";
   f.FiValuet = "";
   Daf.Dspat.RtFiturs.push(f);

   // (5) TopTitleBox — Index 3
   f = new Daf.Dspat.RtFiture();
   f.FiIdent = "20160609°0454";                                         // "topTitleBox";
   f.FiCreate = false;
   f.FiHtAttrib = "class";
   f.FiHtClass = "";
   f.FiHtId = "topTitleBox";
   f.FiHtTag = "";
   f.FiSumary = "This is the top title box";
   f.FiValuet = "";
   Daf.Dspat.RtFiturs.push(f);

   // (6) TopTitleCrumbs — Index 4
   f = new Daf.Dspat.RtFiture();
   f.FiIdent = "20160609°0455";                                         // "toptitlecrumbs";
   f.FiCreate = false;
   f.FiHtAttrib = "class";
   f.FiHtClass = "";
   f.FiHtId = "toptitlecrumbs";
   f.FiHtTag = "";
   f.FiSumary = "This are the top title crumbs";
   f.FiValuet = "";
   Daf.Dspat.RtFiturs.push(f);

   // (4) BlogFadeIn — Index 5
   f = new Daf.Dspat.RtFiture();
   f.FiIdent = "id20140625o1651_BlogFadeIn";
   f.FiCreate = false;
   f.FiHtAttrib = "id";
   f.FiHtClass = "";
   f.FiHtId = "id20140625o1651_BlogFadeIn";
   f.FiHtTag = "";
   f.FiSumary = "Into this div, some blog article can be faded-in";
   f.FiValuet = "";
   Daf.Dspat.RtFiturs.push(f);

   // (7) CanvasGear [feature 20170302°0411] — Index 6
   f = new Daf.Dspat.RtFiture();
   f.FiIdent = "20170302°0411°CanvasGear";                              // Nothing depends on this
   f.FiCreate = false;
   f.FiHtAttrib = "";
   f.FiHtClass = "";
   f.FiHtId = "";
   f.FiHtTag = "canvas";
   f.FiSumary = "This calls CanvasGear";
   f.FiValuet = "";
   Daf.Dspat.RtFiturs.push(f);
};

/**
 *  This array provides the global feature list
 *
 *  @id 20150411°0531
 *  @status Dismantled
 *  @see chg 20190417°0453 'retire class 20180322°0511 Daf.Dspat.Fiture'
 *  @type {Array} —
 */
Daf.Dspat.RtFiturs = [];

/**
 *  This property ...
 *
 *  @id 20190413°0331
 *  @type {string} —
 */
Daf.Dspat.sDbgMsg567Accu = '';

/**
 *  This constant property defines the dashboard form earmark
 *
 *  @id ~20190413°0335
 *  @type {string} —
 */
Daf.Dspat.sEarmark_Dashboard = 'formPinLoginWindow';

/**
 *  This constant property defines the FadeInFiles earmark
 *
 *  @id 20190404°0813
 *  @type {string} —
 */
Daf.Dspat.sEarmark_FadeInFiles_Class = 'fadein';

/**
 *  This constant property defines the Fancytree earmark
 *
 *  @id 20190403°0513
 *  @type {string} —
 */
Daf.Dspat.sEarmark_Fancytree = 'data-fancytree';


/**
 *  This namespace ..
 *
 *  @id 20180322°0521
 *  @c_o_n_s_t — Namespace
 */
Daf.Dspat.Config = {};

/**
 *  This variable stores the absolute path to this page, as given in
 *  the browser address line, possibly supplemented by 'index.html',
 *  value e.g. "http://localhost/daftaridev/index.html"
 *
 *  This value is set in function ..
 *  It is wanted for ...
 *
 *  @id 20170924°0221
 *  @callers Only function 20160417°1011 workoff_Cake_0_go
 *  @type {string} —
 */
Daf.Dspat.Config.PathToThisPageFile = '';

/**
 *  This variable shall store the path of the current page
 *   as relative to the sitemap root ..
 *
 *  This value is set in function 20160417°1011 workoff_Cake_0_go.
 *  It is wanted later for ...
 *
 *  @id 20170923°0511
 *  @callers
 *  @type {string} —
 */
Daf.Dspat.Config.SelfPageUrlRelative = '';

/**
 *  This variable tells the absolute url to the folder where sitmapdaf.js resides
 *
 *  This path is retrieved from the script tag as written in the header
 *  of the HTML page, e.g. :
 *   • "http://localhost/daftaridev/trunk/"
 *   • "http://localhost/daftaridev/trunk/daftari/docs/"
 *
 *  This value is set in function 20150411°0641 provideCornerstonePathes,
 *   and it is wanted later for ...
 *
 *  @id 20170924°0231
 *  @callers
 *  @type {string} —
 */
Daf.Dspat.Config.SitemapFolderAbsolute = '';

/**
 *  This consititues the earmark for the cakecrumbs bag in the page
 *
 *  @id const 20160610°0311
 *  @type {string} —
 */
Daf.Dspat.Config.sFiturIdent_Cakecrumbs_FiHtClass = "DafFurnitureCakecrumbs_Bag";

// eof
