/*!
 *  id          : 20110512°1311 daftari/jsi/gallery-js-autoflick.js
 *  summary     : This file hosts the autoflick feature functions
 *  license     : GNU AGPL v3
 *  copyright   : © 2011 - 2023 Norbert C. Maier
 *  authors     : ncm
 *  encoding    : UTF-8-without-BOM
 *  callers     :
 *
 */

// Options for JSHint [seq 20190423°0421`12]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf, Trekta */

/*
   todo 20190420°0345 'use general autoflick for gallery as well'
   matter : This is autoflick for page 20110509°1331 docs/gallery56album.html
      Abandon this file in favour of namespace Daf.Utlis.Flick if possible
   note : This may be not so easy, because in gallery56album.html, this are
      buttons, referring to some kind of database, which does not yet exist.
   location : File 20110512°1311 gallery-js-autoflick.js
   status : open
*/

/**
 *  Announce namespace
 *
 *  @id 20190106°0311`17
 *  @c_o_n_s_t — Namespace
 */
Trekta = window.Trekta || {};

/**
 *  Announce namespace
 *
 *  @id 20170923°0421`09
 *  @c_o_n_s_t — Namespace
 */
Daf = window.Daf || {};

/**
 *  This function constitutes the autoflick feature entry point
 *
 *  @id 20110510°2141`37
 *  @callers • page 20110509°1331 gallery56album.html via windowOnloadDaisychain()
 *  @return {undefined} —
 */
function autoflickAvail()
{
    'use strict';

    // Retrieve status [line 20110510°2142`37]
    var sValue = Trekta.Utils.getCookie('flicker');

    // Initial request with cookie not yet set [seq 20110510°2143`37]
    if (sValue === '') {
        sValue = 'STOP';
        Trekta.Utils.setCookie('flicker', sValue, 2);                   // Days
    }

    // Show status indicator [line 20110510°2144`37]
    document.getElementById("flickstatus").innerHTML = sValue;

    // Schedule the wanted action [seq 20110510°2145`37]
    switch (sValue) {
        case 'FAST' : tim = setTimeout("autoflickAction();", 500); break;  // 100
        case 'SLOW' : tim = setTimeout("autoflickAction();", 2500); break;  // 3600
        default : break;
    }
}

/**
 *  This function finally fetch the next page
 *
 *  @id 20110512°0121`37
 *  @note
 *  @callers • The main functions's scheduler
 *  @return {undefined} —
 */
function autoflickAction()
{
    'use strict';
    $e = document.getElementById('clickthis');
    location.href = $e;
}

/**
 *  This function starts flicking
 *
 *  @id 20110510°2146`37
 *  @note
 *  @callers • The menu link's onclick property
 *  @param {string} sSpeed — The speed to be set
 *  @return {undefined} —
 */
function flickerRun(sSpeed)
{
    'use strict';

    Trekta.Utils.setCookie('flicker', sSpeed, 2);
    document.getElementById("flickstatus").innerHTML = sSpeed;

    // Refresh page [line 20110510°2155`37] why?
    Daf.Utlis.refreshPage();
}

/**
 *  This function stops autoflicking
 *
 *  @id 20110510°2147`37
 *  @callers • The menu link's onclick property
 *  @return {undefined} —
 */
function flickerStop()
{
    'use strict';
    Trekta.Utils.setCookie('flicker', 'STOP', 2);

    // Not sure whether this is correct [note 20190423°0645]
    document.getElementById("flickstatus").innerHTML = 'STOP';
}

// eof
