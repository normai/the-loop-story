﻿/*!
 *  This module hosts some helper functions for the Daftari dashboard pages.
 *
 *  This module is pulled-behind in func 20190403°0521 process_dashboard
 *  if the earmark name="checkboxPinLoginWindow" is detected. This are the
 *  pages • login.html • settings.html • tools.html
 *
 *  id : 20120826°1711 daftari/jsi/dafdash.js
 *  license : GNU AGPL v3
 *  copyright : © 2011 - 2023 Norbert C. Maier
 *  authors : ncm
 *  status : Applicable
 *  callers :
 *  note :
 */

// Options for JSHint [seq 20190423°0421`03]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf, Trekta */

/**
 *  Announce namespace
 *
 *  @id 20190106°0311`11
 *  @const — Namespace
 */
Trekta = window.Trekta || {};

/**
 *  This namespace constitutes the Daftari root namespace
 *
 *  @id 20170923°0421`02
 *  @callers
 *  @const — Namespace
 */
Daf = window.Daf || {};

/**
 *  This namespace hosts the functions of file dafdash.js
 *
 *  @id 20150319°0421
 *  @const — Namespace
 */
Daf.Dash = Daf.Dash || {};

// Provisory flags for this module only, mostly not yet used, not connected to cookies
Daf.Dash.bDebug_initiateSelfClosing = true;                             // [prop 20150319°0424] false // true
Daf.Dash.bDebug_closeThisWindow = true;                                 // [prop 20150319°0425] false // true
Daf.Dash.bDebug_getPagePinCookie = true;                                // [prop 20150319°0426] false // true
Daf.Dash.bDebug_processBrowserSetting = true;                           // [prop 20150319°0427] false // true
Daf.Dash.bDebug_initializeSettingCheckboxes = true;                     // [prop 20150319°0428] false // true

Daf.Dash.sFlag_checkbox_inlineedit = 'checkbox_inlineedit';             // [prop 20150319°0434]
Daf.Dash.sFlag_checkbox_yellowdebugpane = 'checkbox_yellowdebugpane';   // [prop 20150319°0435]

// ? chg 20190412°0153 'eliminate debug_BootAnalyseScriptStatus sequences'
// status : Currently inactive. Possibly to be recycled with its color gimmick on the settings page
// //Daf.Dash.sFlag_debug_BootAnalyseScriptStatus = 'debug_BootAnalyseScriptStatus';  // [prop 20150319°0436]// ELIMINATED with chg 20200103°0111

Daf.Dash.sFlag_debug_DialogBeforeTransmit = 'debug_DialogBeforeTransmit';  // [prop 20150319°0437]
Daf.Dash.sFlag_debug_DialogBeforeEdit = 'debug_DialogBeforeEdit';       // [prop 20150319°0441]
Daf.Dash.sFlag_debug_OnloadLngSplit = 'debug_OnloadLngSplit';           // [prop 20150319°0443] triggers an alert info

/**
 *  This array provides flags to be set in settings.html.
 *
 *  @id 20150411°0541
 *  @todo 20160416°0531 : Proposal. Generate this array automatically from
 *      the HTML of settings.html. No, in the contrary, do it the other way,
 *      generate HTML from the values here.
 *  @note Remember 20190404°0657 cookies quickfix. When aCheckboxes2 were declared
 *      inside the namespace, the cookies were broken. Obviously, if we have Daf.Dash
 *      already defined, a second definition with the '||' style does no more help.
 *      So here we need the properties defined in the script-level-style.
 *  @callers • settings.html::Daf.Dash.processBrowserSetting() not really used, may be discarded?
 *  @type {Array} —
 */
Daf.Dash.aCheckboxes2 = [
                            [ 'checkbox_inlineedit'            , true  ]
                          , [ 'checkbox_yellowdebugpane'       , false ]
                          , [ 'debug_DialogBeforeTransmit'     , false ]
                          , [ 'debug_DialogBeforeEdit'         , false ]
                          , [ 'debug_OnloadLngSplit'           , false ]
                          , [ 'debug_wafaili_begin'            , false ]
                          , [ 'reducePageLoadFlash'            , false ]
                        ];

/**
 *  This function creates HTML fragment to fill in a dedicated div.
 *
 *  @id 20120828°1811
 *  @see ref 20150320°0741 'stackoverflow → get all cookies from browser'
 *  @status Developing
 *  @callers ..
 *  @return {string} — The created HTML fragment to display all cookies as defined in Daf.Dash.aCheckboxes2.
 */
Daf.Dash.initializeSettingCheckboxes = function()
{
    'use strict';

    // Loop with the old code
    for (var i = 0; i < Daf.Dash.aCheckboxes2.length; i += 1) {
        var els = document.getElementsByName( Daf.Dash.aCheckboxes2[i][0] );
        if (els.length > 0) {
            els[0].checked = Trekta.Utils.getCookieBool(Daf.Dash.aCheckboxes2[i][0], Daf.Dash.aCheckboxes2[i][1]);
        }
    }

    // Loop to generate HTML string [seq 20150319°0411] seems just startin a feature, but not made ready
    var sHtmlFrag = '';
    for (i = 0; i < Daf.Dash.aCheckboxes2.length; i += 1) {
        sHtmlFrag += "\n<p id=\"id20150319o0331\">";
        sHtmlFrag += ' <span id="id20150319o0332"><label id="id20120609o1944">jsiLangSplit</label></span>';
        sHtmlFrag += ' <span id="id20150319o0333"> : <input type="checkbox" name="checkbox_inlineedit" value="" onclick="Daf.Dash.processBrowserSetting()"></span>';
        sHtmlFrag += "\n</p>";
    }
    return sHtmlFrag;
};

/**
 *  This function shall output the list of cookies in the browser.
 *
 *  @id 20150323°0241
 *  @see ref 20150320°0741 'stackoverflow → get all cookies from browser'
 *  @status Developing
 *  @callers • //settings.html::body::onload()
 *  @return {undefined} —
 */
Daf.Dash.listCookiesAll = function()
{
    'use strict';

    var sMsgDbg = "[Dbg 20150323°0251] Run function Daf.Dash.listCookiesAll().";
    alert(sMsgDbg);

    var cokis = Daf.Dash.listCookiesAll_getCookies();

    if (Trekta.Const.b_Toggle_FALSE) {
        Trekta.Utils.outDbgMsg(sMsgDbg);
    }

    alert(sMsgDbg);
};

/**
 *  This function delivers the list of cookies.
 *
 *  @id 20150324°0111
 *  @see ref 20150320°0741 'stackoverflow → get all cookies from browser'
 *  @status Experimental
 *  @return {undefined} —
 */
Daf.Dash.listCookiesAll_getCookies = function() {
    'use strict';
    var pairs = document.cookie.split(";");
    var cookies = {};
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split("=");
        // note 20190423°0641`02 : unescape is deprecated, I boldly replace it by decodeURI
        //cookies[pair[0]] = unescape(pair[1]);
        cookies[pair[0]] = decodeURI(pair[1]);
    }
    return cookies;
};

/**
 *  This function processes the onclick event in an input field of type checkbox.
 *   For each entry in the checkboxes array, it retrieves the element and sets the cookie.
 *
 *  @id 20120828°1801
 *  @callers Only •
 *  @return {undefined} —
 */
Daf.Dash.processBrowserSetting = function()
{
    'use strict';

    // Progogue [seq 20120828°1802]
    var sMsgDbg = "This is function 20120828°1801 Daf.Dash.processBrowserSetting.";
    sMsgDbg += "\n Checkboxes :";

    // Loop over the checkboxes array [seq 20120828°1803]
    for (var i = 0; i < Daf.Dash.aCheckboxes2.length; i += 1) {

        // Process one checkbox [seq 20120828°1804]
        sMsgDbg += "\n - " + Daf.Dash.aCheckboxes2[i][0];
        var els = document.getElementsByName(Daf.Dash.aCheckboxes2[i][0]);

        // [condition 20120828°1805]
        if (els.length > 0) {
            Trekta.Utils.setCookie(Daf.Dash.aCheckboxes2[i][0], els[0].checked, 9);  // [line 20120828°1806]
        }
    }
};

/**
 *  This function shall fire during form view, if user changes checkbox
 *   value. It sets cookie 'pinLoginWindow' to the new value.
 *
 *  @id 20120826°1712
 *  @callers Only •
 *  @return {undefined} —
 */
Daf.Dash.setPinLoginWindow = function()
{
    'use strict';

    // Prologue [line 20120826°1713]
    var bCheckbox = document.formPinLoginWindow.checkboxPinLoginWindow.checked;

    // [seq 20120826°1715]
    Trekta.Utils.setCookie(Daf.Const.sCookieName_pinLoginWindow, bCheckbox, 1);  // 'pinLoginWindow'
};

/**
 *  This namespace holds the 'pin window' feature, which is only one function,
 *    the other functions are for workaround for issue 20120826°1731 'IE onblur bug'
 *
 *  @id 20180322°0411
 *  @see todo 20190411°0541 'dismantle workaround issue 20120826°1731 IE onblur bug'
 *  @callers
 *  @const — Namespace
 */
Daf.Dash.IeOnBlur = window.Daf.Dash.IeOnBlur || {};

/**
 *  This variable ...
 *
 *  @id 20120827°1504
 *  @see : todo 20190411°0541 'dismantle workaround issue 20120826°1731 IE onblur bug'
 *  @type {Object|Node|null} —
 */
Daf.Dash.IeOnBlur.eActiveElement = null;

/**
 *  This variable ...
 *
 *  @id 20120827°1505
 *  @see : todo 20190411°0541 'dismantle workaround issue 20120826°1731 IE onblur bug'
 *  @type {boolean} —
 */
Daf.Dash.IeOnBlur.bIsMSIE = false;

/**
 *  This variable ... is this useful?
 *
 *  @id 20120827°1503
 *  @see : todo 20190411°0541 'dismantle workaround issue 20120826°1731 IE onblur bug'
 *  @type {string} —
 */
Daf.Dash.IeOnBlur.sCookieName = '';

/**
 *  This function assigns the onblur event the method to close the window.
 *   It has to be called by all pages with the "checkboxPinLoginWindow checkbox
 *
 *  @id 20120826°1732
 *  @see todo 20190411°0541 'dismantle workaround issue 20120826°1731 IE onblur bug'
 *  @callers • The body onload event of all dashboard pages
 *  @return {undefined} —
 */
Daf.Dash.IeOnBlur.initiateSelfClosing = function()
{
    'use strict';

    // Hook closing function appropriately
    if (Trekta.Utils.bIs_Browser_Explorer) {
        Daf.Dash.IeOnBlur.eActiveElement = document.activeElement;
        document.onfocusout = Daf.Dash.IeOnBlur.closeThisWindow;
        Daf.Dash.IeOnBlur.bIsMSIE = true;
    }
    else {
        window.onblur = Daf.Dash.IeOnBlur.closeThisWindow;
    }

    // Set pin checkbox accordingly
    Daf.Dash.IeOnBlur.getPagePinCookie();
};

/**
 *  This function closes the Dashboard window.
 *
 *  @id 20120826°1733
 *  @note You activate this function by navigating into the browser address bar.
 *  @see todo 20190411°0541 'dismantle workaround issue 20120826°1731 IE onblur bug'
 *  @callers
 *  @return {undefined} —
 */
Daf.Dash.IeOnBlur.closeThisWindow = function()
{
    'use strict';

    // Possibly shy away from below closing mechanism [seq 20120826°1735]
    if ( Trekta.Utils.getCookieBool(Daf.Const.sCookieName_pinLoginWindow, null)) {
        return;
    }

    // The dashboard closing mechanism
    if (window.opener !== null) {
        if ( Daf.Dash.IeOnBlur.bIsMSIE
            && ( Daf.Dash.IeOnBlur.eActiveElement !== document.activeElement )
             ) {
            Daf.Dash.IeOnBlur.eActiveElement = document.activeElement;
        }
        else {
            window.close();
        }
    }
};

/**
 *  This function determines the form and sets it's pin checkbox
 *
 *  @id 20120826°1734
 *  @callers • ~~help.html::initiateSelfClosing() • login.html • settings.html • tools.html
 *  @return {undefined} —
 */
Daf.Dash.IeOnBlur.getPagePinCookie = function()
{
    'use strict';

    Daf.Dash.IeOnBlur.sCookieName = Daf.Const.sCookieName_pinLoginWindow;  // 'pinLoginWindow';
    var bChecked = Trekta.Utils.getCookieBool(Daf.Dash.IeOnBlur.sCookieName, false);

    // Paranoia 20161009°0821
    try {
        // Payload [line 20120826°1741]
        document.formPinLoginWindow.checkboxPinLoginWindow.checked = bChecked;
    }
    catch (ex) {
        // This shall never happen. The feature dispatcher shall call this
        //  function here only after feature 'initiateSelfClosing' was detected
        //  on the page. If dispatcher calls superfluously, this exception is seen.
        var s = "[Event 20161009°0822] This page has no 'checkboxPinLoginWindow'."
               + "\nException = \"" + ex + "\""
                ;
        Trekta.Utils.outDbgMsg(s);
    }

    // Forward sequence - don't mess the function here with more code [line 20120826°1743]
    Daf.Dash.initializeSettingCheckboxes();

    return;
};

// eof
