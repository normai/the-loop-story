/*!
 *  This module provides the main and element dropdown menu,
 *   plus various global utility functions.
 *
 *  id          : 20150318°0551 daftari/jsi/dafmenu.js
 *  license     : GNU AGPL v3
 *  copyright   : © 2011 - 2023 Norbert C. Maier
 *  authors     : ncm
 *  status      : developing
 *  see         : ref 20190227°1357 'demosjs → register onload events with jQuery'
 *  callers     : daftari.js, settings.html
 *
 */

// Apply some JSLint directives [seq 20190423°0311]
// See ref 20190423°0334 'ESLint'
// See ref 20190423°0333 'JSHint'
// See ref 20190423°0332 'Douglas Crockford → JSLint'
/*j_s_lint devel, for, long, single*/
// • Assume in development (allow todo, alert, confirm, console, prompt) • Tolerate
//  for statement • Tolerate long source lines • Tolerate single quote strings
/*jslint browser: true*/ // avoid 'windows' being complained?
/*global Daf, DafCanary, Trekta */

// Options for JSHint [seq 20190423°0421`06]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf */

/**
 *  Announce namespace
 *
 *  @id 20190106°0311`14
 *  @c_o_n_s_t — Namespace
 */
Trekta = window.Trekta || {};

/**
 *  Announce Daftari root namespace
 *
 *  @id 20170923°0421`05
 *  @note With "Daf = window.Daf || {};", iIn NetBeans navigator two namespaces would show up: Daf plus window.Daf
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf = Daf || {};

/**
 *  This namespace shall collect what is behind the Daftari Notebook Icon
 *
 *  @id 20190411°0411
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu = Daf.Mnu || {};

/**
 *  This namespace Daf.Mnu.Edt shall collect members around the edit feature
 *
 *  @id 20170902°1111
 *  @status new
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.Edt = Daf.Mnu.Edt || {};

/**
 *  This eventhandler finishes edit mode from inline edit
 *
 *  @id 20120827°0542
 *  @status nice
 *  @callers Event butt2.onclick
 *  @return {undefined} —
 */
Daf.Mnu.Edt.EditFinishCancel = function()
{
    "use strict";
    Daf.Vars.sGlobalParamCloseMode = 'cancel';
    Daf.Mnu.Edt.EditFinishFromInline();
};

/**
 *  This function restores the element if that was changed.
 *
 *  @id 20120828°2121
 *  @status nice
 *  @callers
 *  @param {string} sTransmit — The HTML fragment to transmit (?)
 *  @return {undefined} —
 */
Daf.Mnu.Edt.EditFinishFromDialog = function(sTransmit)
{
    "use strict";
    Daf.Mnu.VrsEd.eGlobTarget.innerHTML = sTransmit + Daf.Mnu.VrsEd.sEditPostfix;
    Daf.Mnu.Edt.EditFinishTransmit(sTransmit, '');

    Daf.Vars.bGlobalPageStatusEditingIsOngoing = false;
};

/**
 *  This function restores the page from edit box and send edited block via ajax to server
 *
 *  @id 20110812°1814
 *  @see ref 20110811°2023 'Quirksmode → Edit text'
 *  @todo 20120905°1601 : Merge algorithm in seq 20110816°1243 with the identical one in edit.html
 *  @callers The button's press event
 *  @return {undefined} —
 */
Daf.Mnu.Edt.EditFinishFromInline = function()
{
    "use strict";

    // Prologue [seq 20110816°1242]
    var sTransmit = '';
    var eRestore = document.createElement(Daf.Mnu.VrsPrm.sActualNodeType);  // Global var sActualNodeType abused as parameter, e.g. 'P'

    //>>>>>>>>>>>>>>>>>>>>
    // Get the Edit Array aEdited [seq 20110816°1243]
    //  Build result string form labels and textareas
    // note : See todo 20120905°1601 'Merge algos'
    var oReturn = [];
    var sDbg = 'Debug 20120905°1602'
              + Daf.Const.sFILE_PAGE_EDIT                               // '/panels/edit.html'
               + '::terminate() output = '
                ;
    var i = 0;                                                          // JSLint: does not want the var inside the for loop header [note 20190423°0313]    
    for ( i = 0; i < Daf.Mnu.VrsEd.eGlobLabels.length; i += 1 ) {
        var aPair = [];                                                 // Create new array // JSLint: Do not declare variables in a loop, but we need it here // [fix 20200106°0123]    
        aPair.push(Daf.Mnu.VrsEd.eGlobLabels[i].innerHTML);
        aPair.push(Daf.Mnu.VrsEd.aTextareas[i].value);
        oReturn.push(aPair);                                            // Attention, this pushes a reference, not a value    
        sDbg += '\n-------------------------------\n' + oReturn[i];
    }
    if ( Trekta.Utils.bShow_Debug_Dialogs ) {
        alert(sDbg);
    }
    //>>>>>>>>>>>>>>>>>>>>

    // [seq 20110816°1244]
    sTransmit = Daf.Mnu.Edt.ElementEditCatch_reassemble(oReturn);

    // Restore page, either original or edited version [seq 20110816°1245]
    if ( Daf.Vars.sGlobalParamCloseMode === 'edit' )  {                 // [line 20120819°1721]
        eRestore.innerHTML = sTransmit + Daf.Mnu.VrsEd.sEditPostfix;    // Daf.Mnu.VrsEd.sEditPostfix is a global var abused as parameter
    }
    else {
        eRestore.innerHTML = Daf.Mnu.VrsPrm.sOriginalText + Daf.Mnu.VrsEd.sEditPostfix;
    }

    // Remove the edit control and render restored page element [line 20110816°1246]
    Daf.Mnu.VrsEd.eGlobEditControl.parentNode.replaceChild(eRestore, Daf.Mnu.VrsEd.eGlobEditControl);

    // [seq 20110816°1247]
    // Transfer the edited text to the intermediate textarea which
    // communicates with PHP. For now, we want to have the existing
    // feature separated from the developing feature. Therefore, we now
    // have two buttons(?), which may merge to one if everything works.
    var el = document.getElementById('textarea_intermediate');
    if (el) {
        // Attention : In IE, the transmitted text must NOT contain pointed
        // brackets, otherwise, the field stays unchanged. In Firefox pointed
        // brackets are allowed. In IE, the script aborts in the assignment
        // line with pointed brackets. Compare issue 20110821°1121
        // 'IE textarea.innerHTML susceptibility' jsi-ajax.js

        if ( Daf.Vars.sGlobalParamCloseMode === 'edit' ) {
            Daf.Mnu.Edt.EditFinishTransmit(sTransmit, '');
        }

        var el2 = el.parentNode;                                        // Navigate to the so far invisible form tag
        el2.style.visibility = 'visible';
    }

    // [line 20110816°1248]
    Daf.Vars.bGlobalPageStatusEditingIsOngoing = false;
};

/**
 *  This function ...
 *
 *  @id 20120827°0541
 *  @todo Possibly configure this function as an event ignited by button.
 *      This function is a workaround for not yet knowing how to set the
 *      onclick event programmatically with a parameterized function. The
 *      solution is given in qna 20190413°0541 'set parameterized events'.
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Edt.EditFinishSave = function()
{
    "use strict";
    Daf.Vars.sGlobalParamCloseMode = 'edit';
    Daf.Mnu.Edt.EditFinishFromInline();
};

/**
 *  This function finally posts the new content
 *
 *  @id 20120830°0451
 *  @status nice
 *  @callers • EditFinishFromDialog • EditFinishFromInline • execElementInsert
 *  @param {string} sTransmit — This is the content
 *  @param {string} sMode — Optional, a = insert after, b = insert before
 *  @return {undefined} —
 */
Daf.Mnu.Edt.EditFinishTransmit = function(sTransmit, sMode)
{
    "use strict";

    var sTagId = null;
    if (! Daf.Mnu.VrsEd.bClickedEditPointHooverBox) {
        sTagId = Daf.Mnu.VrsPrm.oSelf.id;                               // E.g. 'mimg.P.7'
    } else {
        // Mimic old style with complete notebook icon id value like 'mimg.P.7'
        var s = Daf.Mnu.VrsPrm.oSelf.getAttribute('data-editpoint');    // E.g. 'P.7'
        sTagId = 'mimg.' + s;
    }

    // (.1) Build target url [seq 20120830°0453]
    var sTarget = Trekta.Utils.s_DaftariBaseFolderRel + '/php'
                 + '/Go.php'
                  + '?cmd=' + 'edit'                                    // Added 20190205°0553
                   + '&tagid=' + sTagId // Daf.Mnu.VrsPrm.oSelf.id
                    + '&file=' + Trekta.Utils.getFileNameFull()
                     + '&mode=' + sMode
                      ;

    // (.2) Optional debug dialog [seq 20120830°0455]
    if ( Trekta.Utils.getCookieBool('debug_DialogBeforeTransmit', null) ) {
        var s = 'tag = ' + Daf.Mnu.VrsPrm.oSelf.id
               + '<br />target = ' + Trekta.Utils.getFileNameFull()
                + '<br />content : ' + Daf.Utlis.convertPointedBracketsToHtml(sTransmit)
                 ;
        s = Daf.Utlis.convertLinebreaksToHtml(s);
        s = Daf.Utlis.showDialogContinueBreak_DISCARD(s, 'Transmit this change?');
        if (s !== 'continue') {
            return;
        }
    }

    // (.3) Finally do the wanted job
    Daf.Core.Ajax.MakeRequest(sTarget, sTransmit);
};

/**
 *  This function ...
 *
 *  @id 20110812°1813
 *  @status nice
 *  @see ref 20110811°2023 'Quirksmode → Edit text'
 *  @note As parameter we don't need the event but the direct object
 *  @note Remember issue 20110818°1531 'innerHTML messes linebreaks'
 *  @callers jsi-bootstrap.js::execElementEdit()
 *  @return {undefined} —
 */
Daf.Mnu.Edt.ElementEditCatch = function()
{
    "use strict";

    // Paranoia [seq 20110812°1821]
    if ( ! document.getElementById || ( ! document.createElement) ) {
        var s = '(Fatal 20170902°1421)' + '\n' + 'Your browser does'
               + ' not support functions getElementById and createElement'
                ;
        alert(s);
        Daf.Vars.bGlobalPageStatusEditingIsOngoing = false;
        return;
    }

    // Provide multilingual array [sequence 20120827°0631]
    var aEdit = [];                                                     // Consists of label/content pairs
    aEdit = Daf.Mnu.Edt.ElementEditCatch_splitMulti();

    // [condi 20110812°1822]
    if ( Daf.Mnu.Edt.b_UseInlineEditing )
    {
        // Edit inline [line 20110812°1823]
        var eParentNode = Daf.Mnu.VrsEd.eGlobTarget.parentNode;         // E.g. <div class="chapter">..</div>

        // Provide frame element [line 20110812°1824]
        Daf.Mnu.VrsEd.eGlobEditControl = document.createElement('DIV');

        // Build the edit control according aEdit [seq 20120903°1711]
        // todo 20120905°1603: Possibly merge analogous sequence in edit.html::function 20120827°1742.
        var i = 0;
        for (i = 0; i < aEdit.length; i += 1)
        {
            // Build box for one language [seq 20110812°1825]
            Daf.Mnu.VrsEd.eGlobLabels.push(document.createElement('P'));
            Daf.Mnu.VrsEd.eGlobLabels[i].style.margin = '0';
            Daf.Mnu.VrsEd.eGlobLabels[i].innerHTML = aEdit[i][0];
            Daf.Mnu.VrsEd.eGlobEditControl.appendChild(Daf.Mnu.VrsEd.eGlobLabels[i]);

            Daf.Mnu.VrsEd.aTextareas.push(document.createElement('TEXTAREA'));
            Daf.Mnu.VrsEd.aTextareas[i].cols = 64;
            Daf.Mnu.VrsEd.aTextareas[i].rows = (18 / aEdit.length);
            if (Daf.Mnu.VrsEd.aTextareas[i].rows < 3) {
                Daf.Mnu.VrsEd.aTextareas[i].rows = 3;                   // Set minimum height per area
            }
            Daf.Mnu.VrsEd.aTextareas[i].value = aEdit[i][1];
            Daf.Mnu.VrsEd.eGlobEditControl.appendChild(Daf.Mnu.VrsEd.aTextareas[i]);
        }

        // Service [line 20110812°1826]
        // todo 20120905°1956 : Not tested yet. Explicitly test it.
        Daf.Mnu.VrsEd.aTextareas[0].focus();

        // Attach functionality [seq 20190403°0732]
        // We would prefere to place this lines inside func 20190403°0731, but
        //  the onclick event can only be attached after the function is known
        Daf.Mnu.Vars.button1Ready.onclick = Daf.Mnu.Edt.EditFinishSave;
        Daf.Mnu.Vars.button2Cancel.onclick = Daf.Mnu.Edt.EditFinishCancel;

        // [seq 20110812°1827]
        Daf.Mnu.VrsEd.eGlobEditControl.appendChild(Daf.Mnu.Vars.button1Ready);
        Daf.Mnu.VrsEd.eGlobEditControl.appendChild(Daf.Mnu.Vars.button2Cancel);

        // [line 20110812°1828]
        eParentNode.replaceChild(Daf.Mnu.VrsEd.eGlobEditControl, Daf.Mnu.VrsEd.eGlobTarget);

        // As opposed to editing via modal dialog, we are finished here
        // and get not returned somehow the result of the edit. It's the
        // button press event which continues and can see any edit result.
        // It continues with EditFinishFromInline(). [note 20110812°1829]
    }
    else {
        // Call the edit modal dialog [line 20110812°1831]
        var aResult = Daf.Utlis.modalDialogEdit_REPLACETHIS(aEdit);

        // Escape? [seq 20110812°1832]
        if (aResult.length < 1) {
            Daf.Vars.bGlobalPageStatusEditingIsOngoing = false;
            return;
        }

        // [line 20110812°1833]
        var sTransmit = Daf.Mnu.Edt.ElementEditCatch_reassemble(aResult);

        // [seq 20110812°1834]
        var sRet = Daf.Mnu.Edt.EditFinishFromDialog(sTransmit);
        if ( ! sRet ) {                                                 // Is this really wanted?
            sRet = '';
        }
    }

    // line  [seq 20110812°1835]
    Daf.Vars.bGlobalPageStatusEditingIsOngoing = false;
};

/**
 *  This function re-assembles HTML text from the edit array.
 *
 *  @id 20120830°1751
 *  @status nice
 *  @callers Only ElementEditCatch()
 *  @param {Object} aEdited — The edited object ...
 *  @return {undefined} —
 */
Daf.Mnu.Edt.ElementEditCatch_reassemble = function(aEdited)
{
    "use strict";

    var iLan = 0;
    var s4 = '';
    var sEle = '';
    var sHtml = '';
    var i = 0;
    for (i = 0; i < aEdited.length; i += 1) {
        // Is language label a language or something else ('text')?
        s4 = Daf.Mnu.Edt.ExtractLangFromLangclass(aEdited[i][0]);
        iLan = Daf.Utlis.getArrayIndexOf(Daf.Const.aLang_Codes, s4);
        if (iLan < 0) {
            sEle = Daf.Const.sTb_1Cr + Daf.Const.sTb_InTxt + aEdited[i][1];
        }
        else {
            sEle = Daf.Const.sTb_1Cr + Daf.Const.sTb_InSpn + '<span class="' + aEdited[i][0] + '">'
                  + Daf.Const.sTb_1Cr + Daf.Const.sTb_InTxt + aEdited[i][1]
                   + Daf.Const.sTb_1Cr + Daf.Const.sTb_InSpn + '</span>'
                    ;
        }
        sHtml += sEle;
    }
    sHtml += Daf.Const.sTb_1Cr;
    return sHtml;
};

/**
 *  This function breaks edit element into it's language and non-language components.
 *
 *  @id 20120830°1731
 *  @status nice
 *  @note No parameters, the global var Daf.Mnu.VrsEd.eGlobTarget is used as input
 *  @callers •
 *  @return {Array} — The edit array
 */
Daf.Mnu.Edt.ElementEditCatch_splitMulti = function()
{
    "use strict";

    // Prologue [seq 20120830°1732]
    var a = [];
    var aElements = [];
    var aLang = [];
    var bNonLangBefore = true;
    var e = null;
    var eChilds = Daf.Mnu.VrsEd.eGlobTarget.childNodes;
    var sContent = '';
    var sMsg = '';
    var sNonLangText = '';

    // Loop over all childnodes [seq 20120830°1733]
    var i = 0;
    for (i = 0; i < eChilds.length; i += 1) {

        e = eChilds[i];

        // Condition for edit facility [seq 20120830°1734]
        //  • Editing in modal dialogbox, skip last element is the localmenu icon,
        //  • Editing inline, the localmenu icon was already removed by caller
        if ( ! Daf.Mnu.Edt.b_UseInlineEditing ) {
            if (i === (eChilds.length - 1)) {

                // Paranoia [seq 20120830°1735]
                // note : This condition seems useless. [note 20190423°0513] //// And what about 'localTfddm'?
                if ((e.tagName !== 'img') || (e.attributes.name !== 'localmenu')) {
                    sMsg = "[Error 20120830°1736] Program flow error ..";  // This must never happen
                    alert(sMsg);
                }

                // Remember for later re-appending [line 20121005°1531]
                // Check — This is not used anymore, isn't it
                Daf.Mnu.VrsEd.sGlobLocalMenuIcon = e.outerHTML;

                continue;
            }
        }

        // [line 20120830°1737]
        aLang = Daf.Mnu.Edt.ElementEditCatch_splitMulti_detectLangSpan(e);

        // Is it a language node? [seq 20120830°1738]
        if (aLang.length > 0) {

            // Append the pre-nonlanguage text [seq 20120830°1741]
            // note : Below is an identical sequence
            if (bNonLangBefore) {
                if (sNonLangText !== '') {
                    a = [];
                    a.push('text');
                    a.push(sNonLangText);
                    aElements.push(a);
                    sNonLangText = '';
                }
            }

            // [seq 20120830°1742]
            bNonLangBefore = false;
            aElements.push(aLang);
            continue;
        }

        // Process non-language node [seq 20120830°1743]
        sContent = Daf.Mnu.Edt.Extract_node_content(e, false);

        // Skip empty nodes [seq 20120830°1744]
        sContent = Daf.Utlis.trimmB(sContent);
        if (sContent === '') {
            continue;
        }

        // Accumulate non-lang-text [seq 20120830°1745]
        if (sNonLangText !== '') {
            sNonLangText += ' ';
        }
    }

    // Append the post-nonlanguage text [seq 20120830°1746]
    // note : Above is an identical sequence
    if (sNonLangText !== '') {
        var a2 = [];
        a2.push('text');
        a2.push(sNonLangText);
        aElements.push(a2);
    }

    return aElements;
};

/**
 *  This function determines whether element is a language span.
 *
 *  @id 20120923°2241
 *  @status nice
 *  @callers Only ElementEditCatch_splitMulti()
 *  @param {Element} e — The element to investigate
 *  @return {undefined} —
 */
Daf.Mnu.Edt.ElementEditCatch_splitMulti_detectLangSpan = function(e)
{
    "use strict";
    var aRet = [];

    // Is it a span?
    if (e.nodeName !== 'SPAN') {
        return aRet;
    }

    // Is it a language span?
    var sClass = e.className;
    var s2 = sClass.substr(0, 4);
    if (s2 !== 'lang') {
        return aRet;
    }

    var sContent = Daf.Mnu.Edt.Extract_node_content(e, true);

    // Remove possible hidden markers (different in IE)
    if (sContent.length > 9) {
        if (sContent.substr(0, 5) === '<!-- ') {
            var s = sContent.substr(sContent.length - 4, 4);
            if (s === ' -->') {
                sContent = sContent.substr(4, sContent.length - 9);
                sContent = Daf.Utlis.trimmB(sContent);
            }
        }
    }

    // Collect the items
    aRet.push(sClass);
    aRet.push(sContent);

    return aRet;
};

/**
 *  This function extracts language code from language class, e.g. 'eng' from 'langeng'.
 *
 *  @id 20120923°2301
 *  @status nice
 *  @todo Call this from all places extracting the lang code from lang class.
 *  @callers ElementEditCatch_reassemble()
 *  @param {string} sLangclass — The language class string, e.g. 'langeng', 'langger'
 *  @return {undefined} —
 */
Daf.Mnu.Edt.ExtractLangFromLangclass = function(sLangclass)
{
    "use strict";

    var sLangcode = '';
    if (sLangclass.length === 7) {
        if (sLangclass.substr(0, 4) === 'lang') {
             sLangcode = sLangclass.substr(4, 3);
        }
    }
    return sLangcode;
};

/**
 *  This function provides a helper function to extract
 *   the content from a node. It extracts either innerHTML
 *   or outerHTML, depending on flag.
 *
 *  @id 20120923°2251
 *  @status nice
 *  @callers ElementEditCatch_splitMulti(), ElementEditCatch_splitMulti_detectLangSpan
 *  @param {Element} e — ...
 *  @param {boolean} bInner — ...
 *  @return {undefined} —
 */
Daf.Mnu.Edt.Extract_node_content = function(e, bInner)
{
    "use strict";

    var sContent = '';
    if ( ! e.innerHTML ) {
        if (e.nodeName === '#text') {
            // It's final text
            if ( Trekta.Utils.bIs_Browser_Explorer ) {                  // IE
                sContent = e.data;
            }
            else {
                sContent = e.textContent;                               // e.nodeName '#text'
            }
        }
        else {
            // It's a tag, e.g. <br />
            sContent = e.outerHTML;
            sContent = Daf.Const.sTb_1Cr +  sContent;                   // Provisory HTML cosmetics
        }
    }
    else {
        if (bInner) {
            sContent = e.innerHTML;
        }
        else {
            sContent = e.outerHTML;
        }
    }
    sContent = Daf.Utlis.trimmB(sContent);
    return sContent;
};

/**
 *  Flag which edit facility to use, inline or dialogbox
 *
 *  @id 20190407°0343
 *  @type {boolean|null}
 */
Daf.Mnu.Edt.b_UseInlineEditing = null;

/**
 *  This nested namespace Daf.Mnu.Handl shall collect event handler
 *
 *  @id 20170902°1021
 *  @status new
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.Handl = {};

/**
 *  This function is called if the user clicks the icon for an editable element.
 *
 *  This function does nothing so far. It only provides a debug alert.
 *  Which elements are involved? While switching to edit mode, the editable
 *  elements got this function as their element.onclick event assigned.
 *
 *  @id 20120901°0223
 *  @status nice
 *  @callers The browsers event system
 *  @return {undefined} —
 */
Daf.Mnu.Handl.editableOnclick = function()
{
    "use strict";
    var s = '';
    if ( Trekta.Utils.bShow_Debug_Dialogs ) {
        s = '(debug 20170902°0931)';
        s = s + '\nYou clicked an icon for an editable element.';
        alert(s);
    }

    // Experiment, use strange element property // [seq 20200410°0914]
    // See issue 20200410°0910 'is currentyl hoovered'
    // See issue 20210416°1711 'GoCloCom warning dangerous global this'
    if ( this.bIsCurrentlyHoovered ) {                                  // [marker 20210416°1633`24 GoCloCom] 'Dangerous use of the global this object'
         // Open Element Menu
        // See feature 20200410°0911 'element menu by clicking the hoover box'
        Daf.Mnu.VrsEd.bClickedEditPointHooverBox = true;
        Daf.Mnu.Tfd.dropMenu1Load('divTfddmLocalMenu', this);
    }
};

/**
 *  This function removes the border from the mouse over feature.
 *
 *  @id 20120901°0222
 *  @status nice
 *  @callers The browser
 *  @return {undefined} —
 */
Daf.Mnu.Handl.editableOnmouseout = function()
{
    "use strict";
    this.style.border = 'none';

    // Experiment using strange element property [line 20200410°0913]
    this.bIsCurrentlyHoovered = false;
};

/**
 *  This function paints a border on editable elements when mouse goes over.
 *
 *  @id 20120901°0221
 *  @status nice
 *  @callers The browser
 *  @return {undefined} —
 */
Daf.Mnu.Handl.editableOnmouseover = function()
{
    "use strict";
    this.style.border = 'solid silver 2px';

    // Experiment, attach strange property to element
    this.bIsCurrentlyHoovered = true;                                   // [line 20200410°0912]
};

/**
 *  This namespace shall collect and cleanup some old functions
 *
 *  @id 20170902°1021
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.Jsi = Daf.Mnu.Jsi || {};

// ~~~~~~~~~~~~~~~~~~~~~~ schnippel 20170901°1411 ~~~~~~~~~~~~~~~~~~~~~~
// id : area 20110812°1811 of former file jsi-quirkscms.js
// summary : This function edits one paragraph in a text area.
// reference : Idea by Peter-Paul Koch ref 20110811°2023 'Quirksmode → Edit text'
// note : Remember issue 20110812°2131 'attributes are lost after edit'
// note 20170901°1413 : After many changes, it is no more clear, what exactly
//     dates back to Peter-Paul Koch's code. So some general credit shall suffice.
//     It is mostly namespace 20170902°1021 Daf.Mnu.Jsi, where his code went.
// ~~~~~~~~~~~~~~~~~~~~~~ schnappel 20170901°1411 ~~~~~~~~~~~~~~~~~~~~~~

/**
 *  This function builds a debug message string, either as plaintext
 *    or as HTML fragment
 *
 *  @id 20111227°1212
 *  @status Nice
 *  @callers • func 20150323°0321 outDbgMsg_GuaranteeParentElement
 *  @param {boolean} bHtml — Flag to build either a plain string or an HTML string
 *  @return {undefined} —
 */
Daf.Mnu.Jsi.getJsiIntroDebugMessage = function(bHtml)
{
    "use strict";

    // Prologue [seq 20111227°1213]
    var sNL = ( bHtml ? '\n<br />' : '\n' );
    var sSp = ( bHtml ? '&nbsp;' : ' ' );

    // Build message [seq 20111227°1214]
    var sMsg = sNL
              + '------- (Debug message 20150210°0851) -------'
               + sNL + sSp + '- location.hash\t= '         + location.hash
               + sNL + sSp + '- location.host\t= '         + location.host
               + sNL + sSp + '- location.hostname\t= '     + location.hostname
               + sNL + sSp + '- location.pathname\t= '     + location.pathname
               + sNL + sSp + '- location.port\t= '         + location.port
               + sNL + sSp + '- location.protocol\t= '     + location.protocol
               + sNL + sSp + '- document.URL\t= '          + document.URL
               + sNL + sSp + '- document.title\t= '        + document.title
               + sNL + sSp + '- document.cookie\t= '       + document.cookie
               + sNL + sSp + '- document.referer\t= '      + document.referrer
                + sNL + '-----------------------------' + sNL
                 ;
    return sMsg;
};

// ################ bundle 20110810°1552 start ################
// See ref 20110810°1542 'javascript-array → simple javascript drop-down menu v2.0'

/**
 *  This function cancels to close the timer
 *
 *  @id 20110810°1753
 *  @status nice
 *  @callers •
 *  @return {undefined} —
 */
Daf.Mnu.Jsi.mcancelclosetime = function()
{
    "use strict";
    if (Daf.Mnu.VrsMop.closetimer) {
        window.clearTimeout(Daf.Mnu.VrsMop.closetimer);
        Daf.Mnu.VrsMop.closetimer = null;
    }
};

/**
 *  This function closes the displayed layer
 *
 *  @id 20110810°1751
 *  @status nice
 *  @note The function is used in parallel to also process the new TFDDM [note 20190423°0457`02]
 *  @note ~~ Close layer when clicking outside the menu 'document.onclick=mclose;'
 *  @callers •
 *  @return {undefined} —
 */
Daf.Mnu.Jsi.mclose = function()
{
    "use strict";

    if (Daf.Mnu.VrsMop.ddmenuitem) {
        Daf.Mnu.VrsMop.ddmenuitem.style.visibility = 'hidden';
    }

    // Abuse old function for new dropdown menu [line 20190423°0457]
    Daf.Mnu.Tfd.destroy1Tfddm();
};

/**
 *  This function goes to close the timer
 *
 *  @id 20110810°1752
 *  @status nice
 *  @note We want to detect when the DIV is left. But onmouseout fires not
 *     only if the DIV itself is left, but after every single HREF is left.
 *     We cannot directly detect if the DIV is left. Workaround: Ignit a timer
 *     to close the menu, but cancel it, if mouse stays inside another HREF,
 *     what is detected by mouseover.
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Jsi.mclosetime = function()
{
    "use strict";
    Daf.Mnu.VrsMop.closetimer = window.setTimeout(Daf.Mnu.Jsi.mclose, Daf.Mnu.VrsMop.timeout);  // timeout = 100
};

// marker 20200410°0821 'shutdown old local menu' (e.g. 100 lines deleted)

// ################ bundle 20110810°1552 stop ################

// marker 20200410°0821 'shutdown old local menu' (e.g. 100 lines deleted)

/**
 *  This namespace is collecting functions related to the Daftari Start Menu
 *
 *  @id 20170902°0941
 *  @callers ..
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.Meo = {};

/**
 *  This function retrieves the string representation of given node (the ~~outerHtml)
 *
 *  @id 20190423°0721
 *  @note This is the sequence outsourced from two functions
 *  @note Possibly this workaround was wanted in former days, when some browsers
 *     not yet had ~~outerHTML. Probably today this workaround is no more wanted.
 *  @see ref 20110811°1821 'stackoverflow → string representation of dom node'
 *  @callers • •
 *  @param eTarget {Element} The node of which to retrieve the string representation
 *  @returns {string} The wanted ~~outerHTML
 *  @return {undefined} —
 */
Daf.Mnu.Meo.egetOuterHtml = function(eTarget)
{
    "use strict";

    // Former .. [seq 20190423°0723]
    var elTmp = document.createElement('P');
    var oTmp = eTarget.cloneNode(true);                                 // Appending the original node would shift it away, so we make a copy of it
    elTmp.appendChild(oTmp);
    var sTmp = elTmp.innerHTML;                                         // The icon's img tag
   elTmp = null;                                                        // Dispose finished element

    return sTmp;
};

/**
 *  This function provides an event handler to edit one staticpoint.
 *
 *  @id 20110811°1921
 *  @callers This is called if you select the elementmenu 'Edit' item.
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execElementEdit = function()
{
    "use strict";

    // Already editing? (only wanted with inline editing) [seq 20110811°1931]
    if ( Daf.Vars.bGlobalPageStatusEditingIsOngoing ) {
        var s1 = 'You are already editing an element,\nyou cannot edit two elements.';
        alert(s1);
        return;
    }

    // Check : Set false also at end of editing plus any intermediate returns [line 20110811°1932]
    Daf.Vars.bGlobalPageStatusEditingIsOngoing = true;

    // Publish current target node [line 20110811°1933]
    //  Global variabel abused as parameter?!
    // Changes for feature 20200410°0911 'edit point hoover box click'
    if (! Daf.Mnu.VrsEd.bClickedEditPointHooverBox) {
        // Old style, the notebook icon was clicked
        Daf.Mnu.VrsEd.eGlobTarget = Daf.Mnu.VrsPrm.oSelf.parentNode;
    } else {
        // New style, the edit-point-hoover-box was clicked
        Daf.Mnu.VrsEd.eGlobTarget = Daf.Mnu.VrsPrm.oSelf;
    }

    //-------------------------------------------------------
    // get ~~~outerHTML [seq 20110811°1922`01]
    // note : Remember todo 20110811°1941 'Outsource partially redundant sequences to dedicated function'
    // note : Remember todo 20110811°1945 'Remove the artificially appended icon part'

    // Get string representation [line 20190423°0731] (former seq 20110811°1922`01)
    var sTmp = '';
    if (! Daf.Mnu.VrsEd.bClickedEditPointHooverBox) {
        // Old style
        sTmp = Daf.Mnu.Meo.egetOuterHtml(Daf.Mnu.VrsPrm.oSelf);
    } else {
        // New style
        var el = Daf.Mnu.VrsPrm.oSelf.lastChild;
        sTmp = Daf.Mnu.Meo.egetOuterHtml(el);                           // Experimental
    }

    // Calculate the wanted netto text of target node [seq 20110811°1934]
    // [note 20121005°1522] The complete sequence seems superfluous because
    //    editElementCatch_splitMulti() does not take Daf.Mnu.VrsPrm.sOriginalText
    //    anymore but uses Daf.Mnu.VrsEd.eGlobTarget to calculate the language parts
    Daf.Mnu.VrsPrm.sOriginalText = Daf.Mnu.VrsEd.eGlobTarget.innerHTML.substr
                                  ( 0
                                   , Daf.Mnu.VrsEd.eGlobTarget.innerHTML.length - sTmp.length
                                    );
    Daf.Mnu.VrsEd.sEditPostfix = Daf.Mnu.VrsEd.eGlobTarget.innerHTML.substr
                                ( Daf.Mnu.VrsEd.eGlobTarget.innerHTML.length - sTmp.length
                                 );
    // Now the following variables are ready to be used
    //  - Daf.Mnu.VrsPrm.sOriginalText : The wanted netto text - no, it's not diassembled into langs
    //  - Daf.Mnu.VrsEd.sEditPostfix : The edit icon to be possibly re-appended after editing
    //-------------------------------------------------------

    //  [seq 20110811°1935]
    Daf.Mnu.VrsPrm.sActualNodeType = Daf.Mnu.VrsEd.eGlobTarget.tagName;  // Set global var abused as parameter

    // Possibly remove the element menu icon [seq 20110811°1936]
    Daf.Mnu.Edt.b_UseInlineEditing = Trekta.Utils.getCookieBool('checkbox_inlineedit', true);
    if ( Daf.Mnu.Edt.b_UseInlineEditing ) {

        // Set eGlobTarget [seq 20110811°1937]
        // See issue 20120905°1614 'IE cannot assign innerHTML'
        var eFree = Daf.Mnu.VrsEd.eGlobTarget.cloneNode(true);
        eFree.innerHTML = Daf.Mnu.VrsPrm.sOriginalText;
        Daf.Mnu.VrsEd.eGlobTarget.parentNode.replaceChild(eFree, Daf.Mnu.VrsEd.eGlobTarget);
        Daf.Mnu.VrsEd.eGlobTarget = eFree;
        eFree = null;
    }

    // Breakpoint [seq 20111230°1932]
    if ( Trekta.Utils.getCookieBool('debug_DialogBeforeEdit', null) ) {
        var s2 = '[Breakpoint 5201] before calling ElementEditCatch'
                + '\n • eTarget.baseURI = '       + Daf.Mnu.VrsEd.eGlobTarget.baseURI
                 + '\n • eTarget.localName = '        + Daf.Mnu.VrsEd.eGlobTarget.localName
                 + '\n • eTarget.offsetTop = '        + Daf.Mnu.VrsEd.eGlobTarget.offsetTop
                 + '\n • eTarget.offsetWidth = '      + Daf.Mnu.VrsEd.eGlobTarget.offsetWidth
                 + '\n'
                 + '\n • eTgt.parent.baseURI = '      + Daf.Mnu.VrsEd.eGlobTarget.parentNode.baseURI
                 + '\n • eTgt.parent.localName = '    + Daf.Mnu.VrsEd.eGlobTarget.parentNode.localName
                 + '\n • eTgt.parent.offsetTop = '    + Daf.Mnu.VrsEd.eGlobTarget.parentNode.offsetTop
                 + '\n • eTgt.parent.offsetWidth = '  + Daf.Mnu.VrsEd.eGlobTarget.parentNode.offsetWidth
                 + '\n'
                 + '\n • eTarget.innerHTML = "'       + Trekta.Utils.sConfine(Daf.Mnu.VrsEd.eGlobTarget.innerHTML, 64) + '"' // Daf.Mnu.VrsPrm.sOriginalText
                  + '\n----------------------------------------'
                   ;
        s2 ='<small>' + s2 + '</small>' + '<br /><br />Continue?';
        s2 = Daf.Utlis.showDialogContinueBreak_DISCARD(s2, 'Debug info');
        if (s2 !== 'continue') {
            Daf.Vars.bGlobalPageStatusEditingIsOngoing = false;
            return;
        }
    }

    // [seq 20110811°1938]
    // note : Daf.Mnu.VrsEd.eGlobTarget is a copy (?!) of the node to be edited (p, li, td)
    // todo : This call seems superfluous, why not just continue with that code here
    Daf.Mnu.Edt.ElementEditCatch();
};

/**
 *  This function inserts an element on the page.
 *
 *  @id 20120905°1931
 *  @status nice
 *  @callers
 *  @param {string} sEle — The name of the element to be inserted [?]
 *  @param {string} sPos — The position of the element to be inserted [?]
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execElementInsert = function(sEle, sPos)
{
    "use strict";

    var eTarget = Daf.Mnu.VrsPrm.oSelf.parentNode;

    var eNew = document.createElement(sEle);
    if (sEle === 'UL') {
        var eNew2 = document.createElement('LI');
        eNew2.innerHTML = '[New list item]';
        eNew.appendChild(eNew2);
    }
    else {
        eNew.innerHTML = '[New ' + sEle + ']';
    }

    // Provide automatic id [line 20180322°0221]
    eNew.id = Daf.Utlis.getTimestampPrint();
    var sTransmit = eNew.outerHTML;
    if (sPos === 'b') {
        eTarget.parentNode.insertBefore(eNew, eTarget);
        sTransmit += Daf.Const.sTb_3Cr;                                 // Beautify HTML
    }
    else if (sPos === 'a') {
        eTarget.parentNode.insertBefore(eNew, eTarget.nextSibling);     // ?
      sTransmit = Daf.Const.sTb_3Cr + sTransmit;                        // Beautify HTML
    }
    else {
        alert('Error, unknown insert position: "' + sPos + '"');
        return;
    }

    // todo 20120905°1945 : Solve condition more elegant. E.g. set incoming
    //    string 'b' to the finally wanted one, so no translation is needed.
    //    Proposal: instead a string, use a flag for before/after so we can write
    //    • EditFinishTransmit(sTransmit, bPos = 'insertbefore' : 'insertafter');
    var s = (( sPos === 'b' )
           ? 'insertbefore'
            : 'insertafter'
             );
    Daf.Mnu.Edt.EditFinishTransmit(sTransmit, s);

    // Quick'n'dirty repaint
    Daf.Mnu.Meo.execPageSwitchEditmodeOff();
    Daf.Mnu.Meo.execPageSwitchEditmodeOn();
};

/**
 *  This function presents a dummy dialog box.
 *
 *  @id 20110811°1621
 *  @status nice
 *  @callers function execItem
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execElementItemAbout = function ()
{
    "use strict";
    var s = "This is the Element Menu's About Item.";
    alert(s);
};

/**
 *  This function displays page properties in an alert box.
 *
 *  @id 20110811°1321
 *  @status Nice
 *  @note A parameter 'this' would represent just the useless 'A' tag
 *     of the clicked menu item. So we use global 'Daf.Mnu.VrsPrm.oSelf',
 *     set by mopen(). Seek a more elegant solution.
 *  @callers Only • func 20110811°1311 execItem
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execElementShowProperties = function()
{
    "use strict";

    // Retrieve element properties [seq 20110811°1922´02]
    // note : Remember todo 20110811°1941 'Outsource partially redundant sequences to dedicated function'
    var eTgt = Daf.Mnu.VrsPrm.oSelf.parentNode;
    var elTmp = document.createElement('p');                            // Check — Rather use capital 'P'?
    var oTmp = Daf.Mnu.VrsPrm.oSelf.cloneNode(true);                    // Appending the original node would shift that away, so we make a copy of it
    elTmp.appendChild(oTmp);
    var sTmp = elTmp.innerHTML;
    elTmp = null;                                                       // Dispose finished element
    var sInner = eTgt.innerHTML.substring(0, eTgt.innerHTML.length - sTmp.length);  // Calculate netto text of target node

    // Assemble message
    var s = 'Target element:'
           + '\n\taf.Mnu.VrsPrm.oSelf.id\t\t= ' + Daf.Mnu.VrsPrm.oSelf.id
            + '\n\teTgt\t\t\t= '                + eTgt
             + '\n\teTgt.tagName\t\t= '         + eTgt.tagName
              + '\n\teTgt.name\t\t= '           + eTgt.name
               + '\n\teTgt.id\t\t\t= '          + eTgt.id
                + '\n\tinnerHTML\t= '           + sInner
                 ;
    alert(s);
};

/**
 *  This function dispatches the calls from the main menu and the element menues
 *   (element menu = local menu = context menu)
 *
 *  @id 20110811°1311
 *  @note The item strings prefixed 'pag' are coming from the main menu,
 *         those prefixed 'ele' are coming form an element menu.
 *  @callers
 *  @param {string} sItem — The function to be executed
 *  @param {string} sPara1 — Parameter one for that function
 *  @param {string} sPara2 — Parameter two for that function
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execItem = function(sItem, sPara1, sPara2)
{
    "use strict";

    Daf.Mnu.Jsi.mclose();
    switch (sItem) {
        case 'eleAbout'         : Daf.Mnu.Meo.execElementItemAbout()    ; break;  // Not active
        case 'eleEdit'          : Daf.Mnu.Meo.execElementEdit()         ; break;
        case 'eleInsert'        : Daf.Mnu.Meo.execElementInsert(sPara1, sPara2) ; break;
        case 'eleShowProps'     : Daf.Mnu.Meo.execElementShowProperties() ; break;
        case 'pagAbout'         : Daf.Mnu.Meo.execPageAbout()           ; break;
        case 'pagEditModeOnOff' : Daf.Mnu.Meo.execPageSwitchEditmode()  ; break;
        case 'pag0410Login'     : Daf.Mnu.Meo.execPage0400LoginWindow(Daf.Const.sFILE_PAGE_LOGIN) ; break;
        case 'pag04Settings'    : Daf.Mnu.Meo.execPage0400LoginWindow(Daf.Const.sFILE_PAGE_SETTINGS) ; break;
        case 'pag0430Tools'     : Daf.Mnu.Meo.execPage0400LoginWindow(Daf.Const.sFILE_PAGE_TOOLS) ; break;
        case 'pag0440Help'      : Daf.Mnu.Meo.execPage0400LoginWindow(Daf.Const.sFILE_PAGE_HELP) ; break;
        case 'pagViewSource'    : Daf.Mnu.Meo.execPageViewSource()      ; break;
        case 'pagViewProps'     : Daf.Mnu.Meo.execPageViewProperties()  ; break;
        case 'pgOpenPageTerminal' : Daf.Mnu.Meo.exe_PageTerminal_1_LoadFirst()  ; break;
        case 'pagDemoPshdp'     : Daf.Mnu.Meo.execPage07DemoPSHDP()     ; break;
        case 'pagDemoDomdoc'    : Daf.Mnu.Meo.execPage08DemoDomDoc()    ; break;
        case 'pagDemoDomdocGen' : Daf.Mnu.Meo.execPage09DemoDomdocGenerate(); break;
        case 'pagBrowserInfo'   : Daf.Mnu.Meo.execPage10ViewBrowserInfo() ; break;
        case 'pagOpenLocalEditor' : Daf.Mnu.Meo.execPage11OpenLocalEditor1Reqest() ; break;
        case 'pagAutoFlick'     : Daf.Mnu.Meo.execPage120AutoFlick()    ; break;
        default                 :
           alert('Something was wrong with the selected menu item.');
    }
};

/**
 *  This function opens the Login Window with the given page
 *
 *  @id 20120904°1641
 *  @callers Only • 20110811°1311 execItem
 *  @param {string} sPage — The URL to be opened e.g. "docs/help.html"
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPage0400LoginWindow = function(sPage)
{
    "use strict";

    // Prologue [seq 20120904°1643]
    var sFullpath = Trekta.Utils.s_DaftariBaseFolderRel + '/' + sPage;

    // Determine opening mode [seq 20190423°0711]
    var bOpenInNewTab = false;
    if (sPage === 'docs/help.html') {
        bOpenInNewTab = true;
    }

    // [condi 20190423°0713]
    var win = null;
    if (! bOpenInNewTab) {

        // Open page in new window [seq 20120904°1645]
        win = window.open
             ( sFullpath                                                // sPage
              , 'Loginbox'                                              // 'Zweitfenster'
               , 'width=640,height=640,left=160,top=160,scrollbars=yes'
                );
    }
    else {
        // Open page in new tab [seq 20190423°0715]
        //  This is done just for fun, should rather be somehow configurable
        //  See ref 20190423°0655 'stackoverflow → open url in new tab'
        win = window.open(sFullpath, '_blank');
   }
    win.focus();
};

/**
 *  This function sends command 'domdemo' via Ajax
 *
 *  @id 20110818°1211
 *  @status nice
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPage07DemoPSHDP = function()
{
    "use strict";
    var s = Trekta.Utils.s_DaftariBaseFolderRel
           + '/' + Daf.Const.s_DAFTARI_OFFSET_PHP_SCRIPTS
            + '/' + 'Go.php?cmd=simple-html-dom-demo&tagid=mimg.p.6&file='
             ;
    Daf.Core.Ajax.MakeRequest ( s                                       // sTargetUrl — The URL to request
                               , 'Hello..'                              // sTransmit — The payload to be sent
                                );
};

/**
 *  This function ...
 *
 *  @id 20110818°1212
 *  @status nice
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPage08DemoDomDoc = function()
{
    "use strict";
    var s = Trekta.Utils.s_DaftariBaseFolderRel
           + '/' + Daf.Const.s_DAFTARI_OFFSET_PHP_SCRIPTS
            + '/' + 'Go.php?cmd=jsi-demo-dom-doc'                       // [chg 20190209°0453]
             ;
    Daf.Core.Ajax.MakeRequest(s, 'Hello..');
};

/**
 *  This function ...
 *
 *  @id 20110818°1213
 *  @status nice
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPage09DemoDomdocGenerate = function()
{
    "use strict";
    var s = Trekta.Utils.s_DaftariBaseFolderRel
           + '/' + Daf.Const.s_DAFTARI_OFFSET_PHP_SCRIPTS
            + '/' + 'Go.php?cmd=jsi-demo-dom-gen'                       // [chg 20190209°0454]
             ;
    Daf.Core.Ajax.MakeRequest(s, 'Hello..');
};

/**
 *  This function presents a dialog with various browser information
 *
 *  @id 20110821°1241 execPage10ViewBrowserInfo
 *  @do Process todo 20190403°0641 'improve feature view-browser-info'
 *  @see Remember ref 20110821°1231 'w3schools → JavaScript Browser Detection'
 *  @callers Only • func 20110811°1311 execItem
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPage10ViewBrowserInfo = function()
{
    "use strict";

    // Build message [seq 20110821°1243]
    var sMsg = 'Miscellaneous browser infos:'
              + '\n'
               + '\n navigator.appCodeName\t= '   + navigator.appCodeName
               + '\n navigator.appName\t\t= '     + navigator.appName
               + '\n navigator.userAgent\t= '     + navigator.userAgent
               // Provisory faded out, just to see below messages, because alert box is too short
               /* */
               + '\n navigator.appVersion\t\t= '    + navigator.appVersion
               + '\n navigator.cookieEnabled\t\t= ' + navigator.cookieEnabled
               + '\n navigator.platform\t\t\t= '    + navigator.platform
               + '\n navigator.userAgent\t\t= '     + navigator.userAgent
               + '\n'
               + '\n Viewport width\t\t\t= ' + Daf.Utlis.getViewPortSize().iWidth
               + '\n Viewport height\t\t\t= ' + Daf.Utlis.getViewPortSize().iHeight
               + '\n'
               + '\n document.URL\t\t\t= ' + document.URL
               + '\n document.baseURI\t\t\t= ' + document.baseURI
               + '\n document.documentURI\t\t= ' + document.documentURI
               + '\n document.domain\t\t\t= ' + document.domain
               + '\n'
               + '\ndocument.location.host\t\t= ' + document.location.host
               + '\ndocument.location.hostname\t= ' + document.location.hostname
               + '\ndocument.location.href\t\t= ' + document.location.href
               + '\ndocument.location.pathname\t= ' + document.location.pathname
                /* */
                 + '\ndocument.location.protocol\t= ' + document.location.protocol
                  ;

    // Compatibility info [seq 20190403°0643]
    // note : Code shifted here from former seq 20111228°1931
    var sMsg2 = '\n[Dbg 20111228°1935] Feature Detect';
    sMsg2 += '\n' + ' • getElementById : ';
    sMsg2 += ( document.getElementById
              ? 'available'
               : 'N/A'
                );
    sMsg2 += '\n' + ' • createElement : ';
    sMsg2 += ( document.createElement
              ? 'available'
               : 'N/A'
                );
    sMsg2 += '\n' + ' • createTextNode : ';
    sMsg2 += ( document.createTextNode
              ? 'available'
               : 'N/A'
                );
    sMsg2 += '\n' + ' • Array.prototype.forEach : ';
    sMsg2 += ( Array.prototype.forEach                                  // [line 20190407°0223]
              ? 'available'
               : 'N/A'
                );
    sMsg2 += '\n' + ' • Array.prototype.indexOf : ';
    sMsg2 += ( Array.prototype.indexOf                                  // [line 20190407°0221]
              ? 'available'
               : 'N/A'
                );
    sMsg2 += '\n' + ' • document.documentElement.scrollLeft/scrollTop shall be available always';

    // display message [seq 20110821°1243]
    sMsg += '\n' + sMsg2;
    alert(sMsg);
};

/**
 *  This function, only on localhost, tries to open a text editor with the current page.
 *
 *  @id 20180512°0711
 *  @status under construction
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPage11OpenLocalEditor1Reqest = function()
{
    "use strict";

    // Build target url [line 20190417°0121] (compare seq 20120830°0453)
    var sUrl = Trekta.Utils.s_DaftariBaseFolderRel + '/php'
              + '/Go.php'
               + '?cmd=' + 'get-physical'
                + '&file=' + Trekta.Utils.getFileNameFull()
                 ;
    Trekta.Utils.ajax3Send ( 'POST'
                            , sUrl
                             , ''                                       // Payload
                              , Daf.Mnu.Meo.execPage11OpenLocalEditor2Conti  // onLoad callback
                               , null                                   // onError callback
                                );
};

/**
 *  This callback function continues after Ajax had responded
 *
 *  @id 20190417°0131
 *  @status under construction
 *  @callers • Ajax event
 *  @param {string} sResponse — The text coming from the server
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPage11OpenLocalEditor2Conti = function(sResponse)
{
    "use strict";

    // Get the wanted path to the user [line 20190417°0133]
    // See howto 20190417°0135 'get string into clipboard'
    sResponse = sResponse.replace(/\//g,"\\");
    var sMsg = '[Msg 20190417°0131] The page physical path'
              + '\n\n(Press Ctrl+C then Enter to copy it to clipboard)'
               ;
    prompt(sMsg, sResponse);
};

/**
 *  This function displays the About dialog.
 *
 *  @id 20110811°1251
 *  @note The Chrome dialog does not understand tabs
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPageAbout = function()
{
    "use strict";

    var sMsg = '***   This is Daftari ' + Daf.Const.s_VERSION_NUMBER
              + ' ' + Daf.Const.s_VERSION_ATTRIB + '   ***'
               + '\n'
               + '\nDaftari is a simple static multilingual online webpage editor.'
               + '\n'
               + '\nWhat?'
               + "\n - Simple : It has only primitive edit features."
               + "\n - Static : It saves directly to HTML, not to a database."
               + "\n - Multilingual : Edit multiple languages in one single dialog."
               + "\n - Online : You edit the pages in your browser."
               + '\n'
               + '\nHow?'
               + '\nSelect the \'Switch edit mode on\' menu item. It will equip'
               + '\nthe page elements with icons to edit one element at a'
               + '\ntime. After confirming your edit, you immediately'
               + '\nsee the result.'
               + '\n'
               + '\nPlease note: Saving your changes permanently is'
               + '\nanother question. Permanent saving may not be'
               + '\n available in your browser.'
               + '\n'
               + '\nLicense : GNU AGPL — GNU Affero Gerneral Public License v3'
               + '\n                 (http://www.gnu.org/licenses/agpl.html)'
               + '\nCopyright : Norbert C. Maier (http://www.trilo.de)'
               + '\nProject status : Applicable on small scale'
               + '\nSources : https://downtown.trilo.de/svn/daftaridev/trunk.'
               + '\n'
               + '\nTo learn more, select the \'Go Help\' menu item.'
               + '\n'
               + '\nHave fun.'
               + '\n'
                + '\n(' + Daf.Const.s_VERSION_TIMEST + ')'
                 + '\n'
                  ;
    alert(sMsg);
};

/**
 *  This function toggles the edit mode of the page.
 *
 *  @id 20120827°0511
 *  @status nice
 *  @callers Function execItem
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPageSwitchEditmode = function()
{
    "use strict";
    if ( Daf.Vars.bGlobalPageStatusEditModeIsOn ) {
        Daf.Mnu.Meo.execPageSwitchEditmodeOff();
    }
    else {
        Daf.Mnu.Meo.execPageSwitchEditmodeOn();
    }
};

/**
 *  This function toggles the edit mode off
 *
 *  @id 20110811°1231
 *  @note Remember issue 20180322°0321 'switch edit mode off'
 *  @status nice
 *  @callers Functions execPageSwitchEditmode, execElementInsert
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPageSwitchEditmodeOff = function()
{
    "use strict";
    // End edit mode [seq 20180322°0323] brute force workaround for issue 20180322°0321
    window.location.reload();
};

/**
 *  This function injects element icons for elements to be editable.
 *
 *   Now shall be implemented feature 20200410°0911 'element menu by
 *   clicking the hoover box'. Status: experimental, under construction.
 *
 *  @id 20110811°1221
 *  @note Remember issue 20110821°1221 'IE fails injecting TD tag icons'
 *  @note Remember issue 20120819°1731 'dynamic elements not visible from php'
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPageSwitchEditmodeOn = function()
{
    "use strict";

    var bPhysicalHtml = true;                                           // Do not declare in loop
    var bHikiUlinzi = false;                                            // Do not declare in loop
    var elCurr = null;
    var elsCurrTags = null;
    var i = 0;
    var i2 = 0;
    var iPhysicalElement = 0;                                           // Do not declare in loop
    var s4 = '';

    // () Define list of tags to be processed [seq 20110811°1222]
    // note : Compare fadeinfiles.js seq 20200413°1122 and keep in sync.
    // note : Provisory browser switch fighting issue 20110821°1221
    var aTags = [ 'P', 'TD', 'LI', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'PRE'];
    if ( Trekta.Utils.bIs_Browser_Explorer ) {
        aTags = [ 'P', 'LI', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'PRE'];
    }

    // Element counter [seq 20110811°1223]
    // Skip elements not visible on PHP back end (problem 20120819°1731)
    for (i = 0; i < aTags.length; i += 1) {

        i2 = 0;                                                         // ?
        elsCurrTags = document.getElementsByTagName(aTags[i]);

        // () Tag count [line 20110811°1224]
        // note : This is a dedicated counter for the elements which exist in the
        //  physical HTML file, as opposed to elements added dynamically by PHP.
        iPhysicalElement = 0;

        // Loop over one tag type [seq 20110811°1225]
        bPhysicalHtml = true;
        bHikiUlinzi = false;
        elCurr = elsCurrTags[i2];                                       // Do not forget the bottom twin [refactor 20161009°0111]
        i2 += 1;
        while ( elCurr ) {

            // Mark this element to be counted, but not necessarily armed with an element menu icon
            bPhysicalHtml = true;

            // Do not edit dynamically generated element [seq 20120819°1741]
            // See note 20121111°1521 'about data-wafailiskip'
            // note : Keep the algorithm in JsiEdit.php in sync.
            if (Daf.Utlis.util_hasAttribute(elCurr, 'data-wafailiskip')) {
                bPhysicalHtml = false;
            }

            // Read the element protection attribute [seq 20140307°0431]
            // note : 'hikiulinzi' shall signal 'this element is protected'.
            // note : This attribute solves issue 20140306°1311 'edit hits wrong cell'.
            if (Daf.Utlis.util_hasAttribute(elCurr, 'data-hikiulinzi')) {
                bHikiUlinzi = true;
            }

            // [line 20110811°1226]
            // note : Workaround for issue 20120828°1851 'IE claims 'unknown error''
            s4 = '';
            try {

                // Mark editable element [seq 20110811°1227]
                if (bPhysicalHtml) {
                    if (! bHikiUlinzi) {

                        // Add TFDDM [seq 20190423°0511]
                        elCurr.innerHTML = elCurr.innerHTML
                                          + '<img'
                                           + ' id="mimg' + '.' + aTags[i] + '.' + iPhysicalElement + '"'
                                           + ' onclick="Daf.Mnu.Tfd.dropMenu1Load(\'divTfddmLocalMenu\', this); Daf.Mnu.VrsEd.bClickedEditPointHooverBox = false;"'
                                           + ' src=' + '"' + Trekta.Utils.s_DaftariBaseFolderRel + '/'
                                           + Daf.Const.sFILE_DAFTARI_ICON_NOTEBOOK_NARROW  + '"'  // '/jsi/imgs23/20100814o0214.logo-edit-small-narrow.png'
                                           + ' width="12" height="16"'
                                           + ' alt="Daftari Element New Menue"'
                                           + ' align="right"'           // PhpStorm warning 'Obsolete attribute', I leave it anyway [issue 20220220°1017]
                                            + '>'
                                             ;

                        // Attach editing attribute [seq 20200410°0921]
                        // Inserted for feature 20200410°0911 'element menu by clicking the hoover box'
                        // This shall replace above
                        //  ' id="mimg' + '.' + aTags[i] + '.' + iPhysicalElement + '"'
                        elCurr.setAttribute('data-editpoint', aTags[i] + '.' + iPhysicalElement);

                        // Attach event handlers [seq 20110811°1228]
                        elCurr.onmouseover = Daf.Mnu.Handl.editableOnmouseover;
                        elCurr.onmouseout = Daf.Mnu.Handl.editableOnmouseout;
                        elCurr.onclick = Daf.Mnu.Handl.editableOnclick;
                    }
                    iPhysicalElement++;
                }
            }
            catch (exc) {
                // Fallback [seq 20110811°1229]
                s4 = 'Exception = ' + exc;
                Daf.Utlis.outputError(s4);
            }

            // Do bottom twin increment [refactor 20161009°0111`02]
            elCurr = elsCurrTags[i2];
            i2 += 1;
        }
    }

    // Globally announce changed page state [line 20110811°1230]
    Daf.Vars.bGlobalPageStatusEditModeIsOn = true;
};

/**
 *  This function executes the ViewPageProps menu item.
 *
 *  @id 20110811°1241
 *  @status nice
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPageViewProperties = function()
{
    "use strict";

    // Prologue [seq 20110811°1242]
    var aEles = [];
    var aCounts = [];
    var sMsg = 'Page element count:';
    var allTags = document.getElementsByTagName('*');
    var i = 0;
    var iTotal = 0;

    // Feature detect [seq 20110811°1243]
    // E.g. IE has no elements.indexOf (see issue 20120902°1221)
    if ( ! aEles.indexOf ) {
        var s = 'Sorry, the page properties cannot be shown with this browser.'
               + "\nThis browser does not support property 'array.indexOf'."
                + '\n(issue 20110903°0221 IE).'
                 ;
        alert(s);
        return;
    }

    // Loop over all elements [seq 20110811°1244]
    var iEl = 0;
    var el = allTags[i];                                                // Do not forget the bottom twin [refactor 20190407°0611]
    i += 1;
    while ( el ) {
        // Provisory sequence [seq 20110811°1245]
        // See issue 20120902°1221 'array.indexof browsercompatibility'
        // See ref 20120902°1202 'stackoverflow → javascript array contains'
        iEl = aEles.indexOf(el.tagName);
        if (iEl >= 0) {
            aCounts[iEl]++;
        }
        else {
            aEles.push(el.tagName);
            aCounts.push(1);
        }
        iTotal++;
        el = allTags[i];                                                // The bottom twin [refactor 20190407°0611`02]
        i += 1;
    }

    // Get elements sorted [seq 20110811°1246]
    // note : aHlp = aEles works but then sorting aHlp will affect aEles (aHlp seems to be a pointer to aEles)
    var aHlp = [];
    for (var sKey in aEles) {
        if (aEles.hasOwnProperty(sKey)) {
            aHlp.push(aEles[Number.parseInt(sKey, 10)]);                // GoCloCom: index key must be number [chg 20210416°1722] Introduce Number.parseInt()
        }
    }
    aHlp.sort();

    // Build output [seq 20110811°1247]
    for (i = 0; i < aHlp.length; i += 1) {
        // Provisory — Not all browsers have e.indexof (see issue 20120902°1221)
        //  see ref 20120902°1202 'stackoverflow → javascript array contains'
        iEl = aEles.indexOf(aHlp[i]);
        sMsg += '\n ' + aEles[iEl] + ' : ' + aCounts[iEl];
    }

    // [seq 20110811°1248]
    sMsg += '\ntotal : ' + iTotal;
    alert(sMsg);
};

/**
 *  This function presents the pages source in an alert box.
 *
 *  @id 20110812°2121
 *  @status nice
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Meo.execPageViewSource = function()
{
    "use strict";

    // Feature detect, IE does not support property getElementsByClassName
    var eShow = null;
    if (document.getElementsByClassName) {
        eShow = document.getElementsByClassName('chapterapps')[0];      // [todo 20160617°0231] Eliminate hardcoded CSS style
    }
    if ( ! eShow ) {
        eShow = document.getElementsByTagName('BODY')[0];
    }
    alert(eShow.innerHTML);
};

/**
 *  This function tests the ModPopDialog functionality.
 *   Probe 1 — Simplest box with only the ok button
 *
 *  @id 20190426°0131
 *  @callers • func 20110811°1311 Daf.Mnu.Meo.execItem
 *  ////@param {number} iType (Integer) — The test to be run (1, 2 ,3, 4, 5)
 *  @return {undefined} —
 */
////Daf.Mnu.Meo.exec_pgTestModPop_1 = function(iType)
Daf.Mnu.Meo.Exec_ProbeModPop_10 = function() {

    "use strict";
    //// var sBaseUrl = Trekta.Utils.s_DaftariBaseFolderAbs;            // [debugmark 20200103°0412`04]

    // (.1) Provide callback for the pull-behind
    let fnCbk = function() {
        Mpd.ShowAlert("Hallo ModPopDialog", null);
    };
    // (.2) Pull-behind
    let sLib = Trekta.Utils.s_DaftariBaseFolderAbs + '/jslibs/modpopdialog/modpopdialog.min.js';
    Trekta.Utils.pullScriptBehind ( sLib                                // Module to pull
                                   , fnCbk                              // Callback after successful pull
                                    , null                              //
                                     , null                             //
                                      );
};

/**
 *  This function executes dopdown menu item 'Probe ModPopDialog 2'
 *
 *  @id 20190426°0152
 *  @callers •
 *  @return {undefined} —
 */
Daf.Mnu.Meo.Exec_ProbeModPop_20 = function() {
    "use strict";


    // issue 20220309°0921 'ModpopDialog disappears late'
    // matter : Interestingly, the ModPopDialog disappears only after below alert
    //    is closed. I'd prefer, that first the dialog closes, then the alert comes.
    // status : Open

    // todo 20220309°0911 'outDbgMsg in two flavours'
    // do : Provide outDbgMsg() in two flavours, the one as is now, to
    //    print debug messages to the PaleYellow debug box. The other
    //    to print alerts to the top of the page, e.g. as is wanted here.
    // location : First use in 20220308°1221
    // status : Open


    // (.) Demo 1 Simple Message [seq 20220308°1221]
    // (.1) Provide callback for the pull-behind
    // See issue 20220309°0921 'ModpopDialog disappears late'
    let cbkShowPrompt = function(sInput) {
         alert("Your input was : " + sInput);
         Trekta.Utils.outDbgMsg("Your input was : " + sInput);          // See tdo 20220309°0911 'outDbgMsg() in two flavours'
    };
    // (.2) Provide callback for the pull-behind
    let cbkPullBehind = function() {
        Mpd.ShowPrompt("Please input some text", cbkShowPrompt);
    };
    // (.3) Pull-behind
    let sLib23 = Trekta.Utils.s_DaftariBaseFolderAbs + '/jslibs/modpopdialog/modpopdialog.min.js';
    Trekta.Utils.pullScriptBehind ( sLib23                              // Module to pull
                                   , cbkPullBehind                      // Callback after successful pull
                                    , null                              //
                                     , null                             //
                                      );
};

/**
 *  This function executes dopdown menu item 'Probe ModPopDialog 3'
 *
 *  @id 20190426°0153
 *  @callers •
 *  @return {undefined} —
 */
Daf.Mnu.Meo.Exec_ProbeModPop_30 = function() {
    "use strict";

    // This shall mimic ..
    let a13 = [];
    a13.push('ModPopDialg Probe 3');                                    // iDlgOptNdx_btn_title = 0
    a13.push('Probe 3 is using form dialog.html.');                     // iDlgOptNdx_btn_message = 1
    a13.push(true);                                                     // iDlgOptNdx_btn_yes = 2
    a13.push(true);                                                     // iDlgOptNdx_btn_no = 3
    a13.push(true);                                                     // iDlgOptNdx_btn_cancel = 4
    a13.push(false);                                                    // iDlgOptNdx_btn_continue = 5
    a13.push(false);                                                    // iDlgOptNdx_btn_break = 6

    // () [seq 20220312°0911] Possibly just a dummy
    let fnCbk132 = function(x) {
        alert('This is fnCbk132 [seq 20220312°0911].' + x)
    };

    // () Provide callback for the pull-behind [seq 20190422°0635`11]   // [debugmark 20200103°0412`05]
    let fnCbk131 = function() {

        //// Daf.Utlis.ShowModPop_continue ( sUrl13
        ////                                , a13                       //// aArgs
        ////                                 , ''                       //// sOpts
        ////                                  , fnCbk132                //// cbkFinal
        ////                                   );

        let sUrl13 = Trekta.Utils.s_DaftariBaseFolderAbs + Daf.Const.sFILE_PAGE_DIALOG;  // '/panels/dialog.html'
        Mpd.ShowModalDialog ( 'ModPopDialog Probe 3'                    // The title bar text
                             , 'Press one of the offered buttons'       // The message text
                              , sUrl13                                  // Either a fields object or a custom form URL
                               , 500                                    // Width in pixel
                                , 600                                   // Height in pixel
                                 , ['Yes', 'No', 'Perhaps', 'Cancel']   // The buttons to show
                                  , fnCbk132                            // The callback function
                                   );                                   // Returns nothing

    };

    // () Guarantee library
    let sLib13 = Trekta.Utils.s_DaftariBaseFolderAbs + '/jslibs/modpopdialog/modpopdialog.min.js';
    Trekta.Utils.pullScriptBehind ( sLib13                              // Module to pull
                                   , fnCbk131                           // Function to call then
                                    , null                              //
                                     , null                             //
                                      );
};

/**
 *  This function executes dopdown menu item 'Probe ModPopDialog 4'
 *
 *  @id 20190426°0154
 *  @callers •
 *  @return {undefined} —
 */
Daf.Mnu.Meo.Exec_ProbeModPop_40 = function() {
    "use strict";

    // This shall mimic line 20110812°1831 'call the edit modal dialog'
    let a = [];
    a.push(["langeng", "This is the Daftari developers playground and documentation area."]);
    a.push(["langfre", "Ceci est le terrain de jeu et de la documentation zone Daftari."]);
    a.push(["langger", "Dies ist der Daftari Spielplatz und Dokumentationsbereich."]);
    a.push(["langrus", "Это площадка и документация площадь Дафтари."]);
    a.push(["langspa", "Este es el patio de recreo y documentación área Daftari."]);
    a.push(["langswa fontSmall fontFat", "Huu ndio uwanja wa kucheza wa Daftari na eneo la nyaraka."]);

    Daf.Utlis.ShowModPop_ELIM ( Trekta.Utils.s_DaftariBaseFolderAbs + '/panels/edit.html'
                               , a
                                , ''                                    //// {}
                                 , Daf.Utlis.exec_pgTestModPop_2conti
                                  );
};

/**
 *  This function executes dopdown menu item 'Probe ModPopDialog 5'
 *
 *  @id 20190426°0155
 *  @see ref 20190429°0342 'stackoverflow → Open URL in new tab'
 *  @callers •
 *  @return {undefined} —
 */
Daf.Mnu.Meo.Exec_ProbeModPop_50 = function() {
    "use strict";

    let sPage = 'https://www.trekta.biz/svn/modpopdev/trunk/modpopdialog/docs/demos.html';
    window.open(sPage, '_blank');
};


// ~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+
// note 20220220°1111 'Func definition inside vs. outside namespace block'
// Implement functions not inside the namespace like above, but as standalone,
// so they carry the namespace in their definition. This makes text search easier.

/**
 *  This function opens the PageTerminal (part 1 of 3)
 *
 *  @id 20220220°1031
 *  @status Works
 *  @callers Only func 20110811°1311 Daf.Mnu.Meo.execItem()
 *  @return {undefined} —
 */
Daf.Mnu.Meo.exe_PageTerminal_1_LoadFirst = function()
{
    "use strict";

    let sScpt = Trekta.Utils.s_DaftariBaseFolderAbs + '/jslibs/terminaljs/purpleterms.min.js';
    Trekta.Utils.pullScriptBehind ( sScpt                               // The script to load
                                   , Daf.Mnu.Meo.exe_PageTerminal_2_LoadNext  // Function to execute on success
                                    , Daf.Core.pullBehind_onError       // Function to execute on fail — Fail case not yet tested from here. Todo: Test it.
                                     , null                             // oJobs
                                      );
};

/**
 *  This function opens the PageTerminal (part 2 of 3)
 *
 *  @id 20220220°1051
 *  @status works
 *  @note 20220220°1051 'About pull-behind-chaining alternative technique'
 *     The two scripts could be loaded using the oJobs mechanism, see daftari.js
 *     seqence 20190404°0125. Only that technique is pretty convoluted, so here
 *     I present a more simple-minded alternative for pull-behind chaining.
 *  @callers Only func 20220220°1031 exe_PageTerminal_1_LoadFirst
 *  @return {undefined} —
 */
Daf.Mnu.Meo.exe_PageTerminal_2_LoadNext = function() {
    "use strict";

    let sScpt = Trekta.Utils.s_DaftariBaseFolderAbs + '/jsi/dafterm.js';
    Trekta.Utils.pullScriptBehind ( sScpt                               // The script to load
                                   , Daf.Mnu.Meo.exe_PageTerminal_3_DoTheJob  // Function to execute on success
                                    , Daf.Core.pullBehind_onError       // Function to execute on fail — Fail case not yet tested from here. Todo: Test it.
                                     , null                             // oJobs
                                      );                                //
};

/**
 *  This function opens the PageTerminal (part 2 of 3)
 *
 *  @id 20220220°1033
 *  @status
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Meo.exe_PageTerminal_3_DoTheJob = function()
{
    "use strict";

    // [line 20220221°0711] First use of ES6 Template Literals
    // note : Snippet copied here from tools.html
    // see : attacomsian.com/blog/javascript-multiline-strings [ref 20220221°0722]
    // see : attacomsian.com/blog/javascript-template-literals [ref 20220221°0724]
    const sSnippet = `
        <div style="border:2px solid SkyBlue; margin-top:1.1em; margin-bottom:0.4em;"
                id="Furniture_20190205o0211_Terminal"></div>
        <p style="display:table-cell; min-width:7.1em; background-color:LightBlue;
                      padding:0 0.7em 0 0.7em; border-radius:0.4em;"
                        id="Is_PHP_Available">
            <!-- Signal content with undecided predetermination [line 20220221°0831]
                note : Here was the former canary element line 20210509°1021´02.
                 See former custom.js seq 20210509°1041 'Determine PHP availability'
            -->
            Is PHP available?
        </p>
    `

    // Setup container
    let elBox = document.createElement('div');                          // First use of ES6 'let' [line 20220220°1041 cornerstone]
    elBox.style.border = "2px dotted Pink";
    elBox.style.borderRadius = "0.7em";
    elBox.style.padding = '0.2em 0.2em 0.2em 0.7em';
    elBox.innerHTML = sSnippet;
    let elH1 = document.getElementsByTagName('h1')[0];
    let elParent = elH1.parentNode;
    elParent.insertBefore(elBox, elH1);

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
    // [seq 20220221°0731]
    // Seqence copied here from func 20190205°0131 Daf.Term.execute

    // Determine PHP availability [seq 20220221°0741] (formerly 20210509°1041`02)
    Daf.Term.isPhpAvailable = null;
/*
    // Determine and show PHP availability [seq 20210509°1041`02]
    // === This does no more work here with the dynamically created element ===
    let el = document.getElementById('Is_PHP_Available');
    let sInner = el.innerHTML;
    if (sInner.indexOf('PHP is available') >= 0) {
        Daf.Term.isPhpAvailable = true;
        el.style.backgroundColor = 'Lime';
    } else {
        Daf.Term.isPhpAvailable = false;
        el.innerHTML = '&nbsp;PHP is not available&nbsp;';
        el.style.backgroundColor = 'Tomato';
    }
*/
    let sUrl = '';                                                      // Why does an empty URL work?!
    // let cbk142Load = function () { alert("Debug 20220221°0742: Loaded"); };  // Debug
    // let cbk142Fail = function () { alert("Debug 20220221°0743: Failed"); };  // Debug
    let cbk142Load = function () { Daf.Mnu.Meo.exe_PageTerminal_4_PhpAvail(true); };
    let cbk142Fail = function () { Daf.Mnu.Meo.exe_PageTerminal_4_PhpAvail(false); };
    Trekta.Utils.ajax3Send ( 'GET' // sMethod                           //
                            , sUrl                                      //
                             , null // sPayload                         //
                              , cbk142Load                              //
                               , cbk142Fail                             //
                                );

    // Create terminal [seq 20190205°0133] after terminaljs/test.html
    Daf.Term.t21 = new Terminal('Terminal_One');
    Daf.Term.t21.setHeight("250px");
    Daf.Term.t21.setWidth('650px');
    Daf.Term.t21.setBackgroundColor('blue');
    Daf.Term.t21.blinkingCursor(false);
    let eDiv = document.getElementById("Furniture_20190205o0211_Terminal");
    eDiv.appendChild(Daf.Term.t21.html);

    // Launch [seq 20190205°0135]
    Daf.Term.t21.print('This is the proof-of-concept PageTerminal.');
    Daf.Term.t21.input(Daf.Term.sHelp, Daf.Term.inputz);
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~

};
// ~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+

/**
 *  This function sets the PHP availability signal (part 2 of 3)
 *
 *  @id 20220221°0841
 *  @status Works
 *  @callers Only func 20220220°1033 exe_PageTerminal_3_DoTheJob
 *  @param {boolean} bPhpAvail — Flag whether PHP is available or not
 *  @return {undefined} —
 */
Daf.Mnu.Meo.exe_PageTerminal_4_PhpAvail = function(bPhpAvail) {

    "use strict";

    // Set PHP availability indicator
    Daf.Term.isPhpAvailable = bPhpAvail;
    let elPara = document.getElementById("Is_PHP_Available");
    if (Daf.Term.isPhpAvailable) {
        elPara.style.backgroundColor = 'Lime';
        elPara.innerHTML = 'PHP is available';
    }
    else {
        elPara.style.backgroundColor = 'Tomato';
        elPara.innerHTML = 'PHP is not available';
    }
}

/**
 *  This namespace wraps the function 'Position' by Matt Kruse
 *
 *  @id 20161009°0911
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.Pos = Daf.Mnu.Pos || { };

// ~~~~~~~~~~~~~~~~~~ schnippel 20170901°1351 ~~~~~~~~~~~~~~~~~~
/**
 *  This function creates an object to deal with positions
 *
 *  @id 20110810°1726
 *  @note The original function header (see ref 20110810°1725) :
 *      ----------------------------------------------------
 *     Copyright © 2005-2009 Matt Kruse (javascripttoolbox.com)
 * 
 *     Dual licensed under the MIT and GPL licenses.
 *     This basically means you can use this code however you want for
 *     free, but don't claim to have written it yourself!
 *     Donations always accepted: http://www.JavascriptToolbox.com/donate/
 * 
 *     Please do not link to the .js files on javascripttoolbox.com from
 *     your site. Copy the files locally to your server instead.
 *     ----------------------------------------------------
 *  @status Nice
 *  @callers
 *  @return {undefined} —
 */
Daf.Mnu.Pos.Position = ( function()
{
    "use strict";

    // ===== Resolve a string identifier to an object =====
    // [func 20110810°1726]
    Daf.Mnu.Pos.resolveObject = function (s)                           // [line 20161009°0835]
    {
        if (document.getElementById && document.getElementById(s) !== null) {
            return document.getElementById(s);
        }
        else if (document.all && document.all[s] !== null) {
            return document.all[s];
        }
        else if ( document.anchors
                 && document.anchors.length
                  && document.anchors.length > 0
                   && document.anchors[0].x
                    ) {
            var i = 0;
            for (i = 0; i < document.anchors.length; i += 1) {
                if (document.anchors[i].name === s) {
                    return document.anchors[i];
                }
            }
        }

        // note 20160503°0552 : Next line KomodoIde "Strict warning - function
        //    Daf.Mnu.Pos.resolveObject does not always return a value". This
        //    is why we experimentally supplement "return null;".
        return null;                                                    // Just get rid of KomodoIde warning - empirical, not yet validated
    };

    // Declare ~inner class [line 20161009°0837]
    Daf.Mnu.Pos.pos = {};

    // ===== Set the position of this object =====
    Daf.Mnu.Pos.pos.set = function(o, left,top) {
        if (typeof(o) === 'string') {
            o = Daf.Mnu.Pos.resolveObject(o);
        }
        if (o === null || !o.style) {
            return false;
        }

        // If the second parameter is an object, it is assumed to be the result of getPosition()
        if ( typeof (left) === 'object' ) {
            var pos = left;
            left = pos.left;
            top = pos.top;
        }

        o.style.left = left + 'px';
        o.style.top = top + 'px';
        return true;
    };

    // ===== Retrieve the position and size of an object =====
    Daf.Mnu.Pos.pos.get = function(o) {

        // If a string is passed in instead of an object ref, resolve it
        if (typeof(o) === 'string') {
            o = Daf.Mnu.Pos.resolveObject(o);
        }

        if (o === null) {
            return null;
        }

        var left = 0;
        var top = 0;
        var width = 0;
        var height = 0;
        //var parentNode = null;                                        // Obviusly unused
        var offsetParent = null;

        offsetParent = o.offsetParent;
        var originalObject = o;
        var el = o;                                                     // 'el' will be nodes as we walk up, 'o' will be saved for offsetParent references
        while (el.parentNode !== null) {
            el = el.parentNode;

            if (el.offsetParent !== null) {

                // Calculate scroll offset [seq 20161009°0833]
                // Remember issue 20110810°1738 'Opera Daf.Mnu.Pos.Position scroll issues'
                if (el.scrollTop && el.scrollTop > 0) {
                    top -= el.scrollTop;
                }
                if (el.scrollLeft && el.scrollLeft > 0) {
                    left -= el.scrollLeft;
                }
            }

            // If this node is also the offsetParent, add on the offsets and reset to the new offsetParent
            if (el === offsetParent) {
                left += o.offsetLeft;
                if (el.clientLeft && el.nodeName !== 'TABLE') {
                    left += el.clientLeft;
                }
                top += o.offsetTop;
                if (el.clientTop && el.nodeName !== 'TABLE') {
                    top += el.clientTop;
                }
                o = el;
                if (o.offsetParent === null) {
                    if (o.offsetLeft) {
                        left += o.offsetLeft;
                    }
                    if (o.offsetTop) {
                        top += o.offsetTop;
                    }
                }
                offsetParent = o.offsetParent;
            }
        }

        if (originalObject.offsetWidth) {
            width = originalObject.offsetWidth;
        }
        if (originalObject.offsetHeight) {
            height = originalObject.offsetHeight;
        }

        return { 'left':left, 'top':top, 'width':width, 'height':height };
    };

    // ===== Retrieve the position of an object's center point =====
    Daf.Mnu.Pos.pos.getCenter = function(o) {
        var c = this.get(o);
        if (c === null)
        {
            return null;
        }
        c.left = c.left + (c.width / 2);
        c.top = c.top + (c.height / 2);
        return c;
    };

    return Daf.Mnu.Pos.pos;
}());
// ~~~~~~~~~~~~~~~~~~ schnappel 20170901°1351 ~~~~~~~~~~~~~~~~~~


/**
 *  This namespace shall process the Touch Friendly Dropdown Menu
 *
 *  @id 20190422°0611
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.Tfd = Daf.Mnu.Tfd || {};

/**
 *  This function destroys the dropdown menu
 *
 *  @id 20190423°0431
 *  @callers Only • via event ..
 *  @return {undefined} —
 */
Daf.Mnu.Tfd.destroy1Tfddm = function()
{
    "use strict";

    // Paraonioa advisably [seq 20110811°1313]
    var el4 = document.getElementById('divTfddmMainMenu')
             || document.getElementById('divTfddmLocalMenu')            // [fix 20200410°0811]
              ;

    // Do not knock out the old menu
    // Check 20200410°0821 When shutdown old menu — Convert to a fatal error
    if (typeof el4 === 'undefined' || el4 === null) {
        return;
    }

    // Do the job [seq 20110811°1315]
    var el5 = el4.parentNode;
    el5.removeChild(el4);
};

/**
 *  This function ..
 *
 *  @id 20190422°0631
 *  @callers • click event in page menu icon from 20190417°0441 process_1_MainMenu
 *      • click event in element menu icon from 20110811°1221 execPageSwitchEditmodeOn
 *  @param {string} sPara — Element ID • 'divTfddmMainMenu' or • 'divTfddmLocalMenu'
 *  @param {Object} oThis — 'this' from element igniting the call
 *  @return {undefined} —
 */
Daf.Mnu.Tfd.dropMenu1Load = function(sPara, oThis)
{
    "use strict";

    // Pull CSS [seq 20190422°0633]
    Daf.Core.pullCss ( Trekta.Utils.s_DaftariBaseFolderAbs
                      , 'jslibs/dropdown/dropdown2.css'
                       );
    Daf.Core.pullCss ( Trekta.Utils.s_DaftariBaseFolderAbs
                      , 'jslibs/dropdown/demonstration2.css'
                       );

    var sJs = Trekta.Utils.bUseMinified ? 'dropdown2.min.js' : 'dropdown2.js';

    // Simple pull [seq 20190422°0635]
    Trekta.Utils.pullScriptBehind ( Trekta.Utils.s_DaftariBaseFolderAbs
                                   + '/jslibs/dropdown/' + sJs
                                    , function() { Daf.Mnu.Tfd.dropMenu2Act(sPara, oThis); }
                                     , null
                                      , null
                                       );
};

/**
 *  This function continues func Daf.Mnu.Tfd.dropMenu1Load
 *
 *  @id 20190422°0711
 *  note : This has replaced func 20110810°1741 Daf.Mnu.Jsi.mopen and its belongings
 *  @callers Callback originating in • func 20190422°0631 Daf.Mnu.Tfd.dropMenu1Load
 *  @param {string} sTgtId — Either • element ID 'divTfddmMainMenu'
 *            or • element ID 'divTfddmLocalMenu'
 *  @param oThis {Object} 'this' from element igniting the call
 *  @return {undefined} —
 */
Daf.Mnu.Tfd.dropMenu2Act = function(sTgtId, oThis)
{
    "use strict";

    // Import wanted global namespaces [seq 20190423°0341]
    // This fixes issue 20190423°0321 'namespaces undefined'
    var Daf = window.Daf;
    var Dropdown = window.Dropdown;                                     // [marker 20210416°1633`53] GoCloCom "WARNING - JSC_INEXISTENT_PROPERTY Property Dropdown never defined on Window" — But it does exist indeed!

    // Retrieve position [seq 20190422°0721]
    var oPos1 = null;
    oPos1 = Daf.Mnu.Pos.Position.getCenter(oThis);

    // Provide the menu div [line 20190422°0723]
    var sHt = '';
    switch (sTgtId) {
        case 'divTfddmMainMenu' :
            sHt = Daf.Mnu.Tfd.getTfddmMainHtml();
            break;
        case 'divTfddmLocalMenu' :
            sHt = Daf.Mnu.Tfd.getTfddmLocalHtml();
            Daf.Mnu.VrsPrm.oSelf = oThis;
            break;
        default :
            alert('Program flow error 20190422°0724.');
            return;
    }

    // Provide the dropdownmenu [seq 20190423°0223]
    // note : As opposed to seq 20110813°2224 'inject hidden bulk area',
    //         we now provide the HTML dynamically, not in stock.
    var eBody = document.getElementsByTagName('BODY')[0];
    var eDiv = document.createElement('DIV');
    eDiv.innerHTML = sHt;
    eBody.appendChild(eDiv);

    // Provide dynamic menu adjustion [seq 20120827°0511]
    if (sTgtId === 'divTfddmMainMenu') {
        var el34 = document.getElementById('pageMenu2Item_switchEditMode');
        if ( Daf.Vars.bGlobalPageStatusEditModeIsOn ) {
            el34.innerHTML = 'Switch edit mode <b>off</b>';
        }
        else {
            el34.innerHTML = 'Switch edit mode <b>on</b>';
        }
    }

    // Calculate and apply the popup direction [seq 20150313°1841]
    // note : The rules are this. If the notebook icon is in the upper left
    //    half of viewport, the menu shall drop to the down right, if notebook
    //    is in the lower right halve, the menu shall drop to the upper left.
    // note : Such rule is also wanted for XBT (see seq 20150313°2011)
    var vp = Daf.Utlis.getViewPortSize();
    var oPos2 = Daf.Mnu.Pos.Position.getCenter(oThis);                  // Provisory — Just to get such object since we have no plain constructor
    if ( oPos1.left > (vp.iWidth / 2 )) {
        oPos2.left = oPos2.left - Daf.Mnu.Pos.Position.get(sTgtId).width;
    }
    if ( oPos1.top > (vp.iHeight / 2)) {
        oPos2.top = oPos2.top - Daf.Mnu.Pos.Position.get(sTgtId).height;
    }
    Daf.Mnu.Pos.Position.set(sTgtId, oPos2);

    // Remove the About tooltip which may overlap the menu box [seq 20150313°1851]
    // Check — Perhaps with using the new TFDDM this sequence can be
    //     removed, or the matter can be solved with z-index setting.
    var el33 = document.getElementById('xbtAbout');
    if (el33) {
        el33.style.display = "none";
    }

    // Apply the JavaScript to a menu by passing an ID [line 20190422°0725]
    Dropdown.initialise();
    Dropdown.applyTo(sTgtId);
};

/**
 *  This function provides the HTML for the local dropdown menu
 *
 *  @id 20190423°0631
 *  @note This shall replace func ~~20110810°1551 Daf.Mnu.Jsi.get_sMenuesHtmlBlock
 *  @callers Only • func 20190422°0711 dropMenu2Act
 *  @return {string} —
 */
Daf.Mnu.Tfd.getTfddmLocalHtml = function()
{
    "use strict";

    // Build Tfddm Local Menu [seq 20190423°0633]
    var sHt = '<div style="position:absolute;"'
             +     ' onmouseover="Daf.Mnu.Jsi.mcancelclosetime();"'
             +     ' onmouseout="Daf.Mnu.Jsi.mclosetime();"'
             +     ' onclick="Daf.Mnu.Jsi.mclose();"'
             +     ' dir="rtl" id="divTfddmLocalMenu">'                 // divTfddmLocalMenu
             + ' <ul class="dropdown dropdownVertical" id="ulTfddmLocalMenu">'
             + '  <li>'
             + '   <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleEdit\')"><b>Edit element</b></a>'
             + '  </li>'
             + '  <li>'
             + '   <span><b>Insert</b></span>'
             + '   <ul>'
             + '    <li>'
             + '     <span>&lt;p&gt;</span>'
             + '     <ul>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'P\',\'b\');">before</a>'
             + '      </li>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'P\',\'a\');">after</a>'
             + '      </li>'
             + '     </ul>'
             + '    </li>'
             + '    <li>'
             + '     <span>&lt;ul&gt;</span>'
             + '     <ul>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'UL\',\'b\');">before</a>'
             + '      </li>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'UL\',\'a\');">after</a>'
             + '      </li>'
             + '     </ul>'
             + '    </li>'
             + '    <li>'
             + '     <span>&lt;li&gt;</span>'
             + '     <ul>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'LI\',\'b\');">before</a>'
             + '      </li>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'LI\',\'a\');">after</a>'
             + '      </li>'
             + '     </ul>'
             + '    </li>'
             + '    <li>'
             + '     <span>&lt;h2&gt;</span>'
             + '     <ul>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'H2\',\'b\');">before</a>'
             + '      </li>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'H2\',\'a\');">after</a>'
             + '      </li>'
             + '     </ul>'
             + '    </li>'
             + '    <li>'
             + '     <span>&lt;h3&gt;</span>'
             + '     <ul>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'H3\',\'b\');">before</a>'
             + '      </li>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'H3\',\'a\');">after</a>'
             + '      </li>'
             + '     </ul>'
             + '    </li>'
             + '    <li>'
             + '     <span>&lt;pre&gt;</span>'
             + '     <ul>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'PRE\',\'b\');">before</a>'
             + '      </li>'
             + '      <li>'
             + '       <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleInsert\',\'PRE\',\'a\');">after</a>'
             + '      </li>'
             + '     </ul>'
             + '    </li>'
             + '   </ul>'
             + '  </li>'
             + '  <li>'
             + '   <a href="#" onclick="Daf.Mnu.Meo.execItem(\'pagEditModeOnOff\');">Switch edit mode off</a>'
             + '  </li>'
             + '  <li>'
             + '   <a href="#" onclick="Daf.Mnu.Meo.execItem(\'eleShowProps\');">View element properties</a>'
             + '  </li>'
             + '</div>'
              ;

    return sHt;
};

/**
 *  This function provides the HTML for the Daftari main dropdown menu.
 *   This shall replace func 20110810°1551 Daf.Mnu.Jsi.get_sMenuesHtmlBlock.
 *
 *  @id 20190423°0211
 *  @callers Only • func 20190422°0711 dropMenu2Act
 *  @return {string} —
 */
Daf.Mnu.Tfd.getTfddmMainHtml = function()
{
    "use strict";

    // Build TFDDM Main Menu HTML block [seq 20190423°0221]
    var sHt = '<div style="position:absolute;"'
             + '    onmouseover="Daf.Mnu.Jsi.mcancelclosetime();"'
             + '    onmouseout="Daf.Mnu.Jsi.mclosetime();"'
             + '    onclick="Daf.Mnu.Jsi.mclose();"'
             + '    dir="rtl" id="divTfddmMainMenu">'
             + ' <ul class="dropdown dropdownVertical" id="ulTfddmMainMenu">'
             + '  <li>'
             + '   <a href="javascript:void(0);" id="pageMenu2Item_switchEditMode"'
             + '       onclick="Daf.Mnu.Meo.execItem(\'pagEditModeOnOff\');">'
             + '    Switch edit mode on/off'
             + '   </a>'
             + '  </li>'
             + '  <li>'
             + '   <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pagAbout\');">'
             + '    <b>About</b>'
             + '   </a>'
             + '  </li>'
             + '  <li>'
             + '   <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pag0410Login\');">'
             + '    Go Login'
             + '   </a>'
             + '  </li>'
             + '  <li>'
             + '   <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pag04Settings\');">'
             + '    Go Settings'
             + '   </a>'
             + '  </li>'
             + '  <li>'
             + '   <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pag0430Tools\');">'
             + '    Go Tools'
             + '   </a>'
             + '  </li>'
             + '  <li>'
             + '   <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pag0440Help\');">'
             + '    Go Help'
             + '   </a>'
             + '  </li>'
             + '  <li>'
             + '   <span><a href="#">Page infos</a></span>'
             + '   <ul>'
             + '    <li>'
             + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pagViewSource\');">'
             + '      <small>View page source</small>'
             + '     </a>'
             + '    </li>'
             + '    <li>'
             + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pagViewProps\');">'
             + '      <small>Show page properties</small>'
             + '     </a>'
             + '    </li>'
             + '    <li>'
             + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pagBrowserInfo\');">'
             + '      <small>Browser Info</small>'
             + '     </a>'
             + '    </li>'
             + '   </ul>'
             + '  </li>'
              ;

    // Finegrain menu items switching [seq 20220221°0851]
    let bItem_DemosAndTests = true;
    let bItem_Demos_PageTerminal = true;
    let bItem_Demos_SimpleHtmlDomParser = Daf.Start.isLocalhost();
    let bItem_Demos_DomDocumentUsage = Daf.Start.isLocalhost();
    let bItem_Demos_DomDocumentGeneration = Daf.Start.isLocalhost();
    let bItem_GetLocalPagePath = Daf.Start.isLocalhost();

    if (bItem_DemosAndTests) {
        sHt += '  <li>'
             + '   <span><a href="#">Demos/Tests</a></span>'
              + '   <ul>'
               ;

        // Menu item opening the PageTerminal [seq 20220220°1021]
        if (bItem_Demos_PageTerminal) {  // todo 20220220°1021`01 : Replace 'true' by some concrete flag, after such exists
            sHt += '   <li>'
                 + '    <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pgOpenPageTerminal\');">'
                 + '     <i>Open PageTerminal</i>'
                 + '    </a>'
                 + '   </li>'
                  ;
      }

        if (bItem_Demos_SimpleHtmlDomParser) {
            sHt += '     <li>'
                 + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pagDemoPshdp\');">'
                 + '      <i>Simple HTML DOM Parser</i>'
                 + '     </a>'
                 + '    </li>'
                  ;
        }

        if (bItem_Demos_DomDocumentUsage) {
            sHt += '    <li>'
                 + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pagDemoDomdoc\');">'
                 + '      <i>DOMDocument Usage</i>'
                 + '     </a>'
                 + '    </li>'
                  ;
        }

        if (bItem_Demos_DomDocumentGeneration) {
            sHt += '    <li>'
                 + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pagDemoDomdocGen\');">'
                 + '      <i>DOMDocument Generation</i>'
                 + '     </a>'
                 + '    </li>'
                  ;
        }

        sHt += '   </ul>'
             + '  </li>'
              ;
    }

    if (bItem_GetLocalPagePath) {
        sHt += '  <li>'
             + '   <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.execItem(\'pagOpenLocalEditor\');">'
             + '    <i>Get local page path</i>'
             + '   </a>'
             + '  </li>'
              ;
    }

    sHt += '  <li>'
         + '   <span><a href="#">autoflick</a></span>'                  // [chg 20201114°0231] 'parameterize autoflick'
         + '   <ul dir="ltr">'                                          // Neutralize dir="rtl" from high above [chg 20201114°0231]
         + '    <li>'
         + '     <a href="javascript:void(0);" onclick="Daf.Utlis.Flick.flickerRun(500)">0.5 seconds</a>'
         + '    </li>'
         + '    <li>'
         + '     <a href="javascript:void(0);" onclick="Daf.Utlis.Flick.flickerRun(1000)">1 second</a>'
         + '    </li>'
         + '    <li>'
         + '     <a href="javascript:void(0);" onclick="Daf.Utlis.Flick.flickerRun(2000)">2 seconds</a>'
         + '    </li>'
         + '    <li>'
         + '     <a href="javascript:void(0);" onclick="Daf.Utlis.Flick.flickerRun(4000)">4 seconds</a>'
         + '    </li>'
         + '   </ul>'
         + '  </li>'
         + '  <li>'
         + '   <span><a href="#">'
         + '    <code><small>Some Probes</small></code>'
         + '   </a></span>'
         + '   <ul>'
         + '    <li>'
         + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.Exec_ProbeModPop_10();">'
         + '      Test ShowModal 1 Plain'                               // [mark 20200103°0412`02]
         + '     </a>'
         + '    </li>'
         + '    <li>'
         + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.Exec_ProbeModPop_20();">'
         + '      Test ShowModal 2 Confirm'
         + '     </a>'
         + '    </li>'
         + '    <li>'
         + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.Exec_ProbeModPop_30();">'
         + '      Test ShowModal 3 dialog.html'
         + '     </a>'
         + '    </li>'
         + '    <li>'
         + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.Exec_ProbeModPop_40();">'
         + '      Test ShowModal 4 edit.html'
         + '     </a>'
         + '    </li>'
         + '    <li>'
         + '     <a href="javascript:void(0);" onclick="Daf.Mnu.Meo.Exec_ProbeModPop_50();">'
         + '      Open ModPopDialog Demo Page'
         + '     </a>'
         + '    </li>'
         + '    <li>'
         + '     <span><a href="#">Probe Dropdown Menu</a></span>'
         + '     <ul>'
         + '      <li>'
         + '       <a href="#">Subsub 3.1</a>'
         + '      </li>'
         + '      <li>'
         + '       <a href="#">Subsub 3.2</a>'
         + '      </li>'
         + '      <li>'
         + '       <span><a href="#">Subsub 3.3</a></span>'
         + '       <ul>'
         + '        <li>'
         + '         <a href="#">Subsubsub 3.3.1</a>'
         + '        </li>'
         + '        <li>'
         + '         <a href="#">Subsubsub 3.3.2</a>'
         + '        </li>'
         + '        <li>'
         + '         <a href="#">Subsubsub 3.3.3</a>'
         + '        </li>'
         + '       </ul>'
         + '      </li>'
         + '     </ul>'
         + '    </li>'
         + '   </ul>'
         + '  </li>'
         + ' </ul>'
         + '</div>'
          ;

    return sHt;
};

/**
 *  This namespace holds variables
 *
 *  @id 20190411°0421
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.Vars = Daf.Mnu.Vars || {};

/**
 *  This defines the width of the Ajax Debug Area in characters
 *
 *  @id 20190414°0711
 *  @type {number} — Width of the debug area
 */
Daf.Mnu.Vars.iSet_AjaxDebugAreaWidth = 112;                             // 127; // 112

/**
 *  This property ..
 *
 *  @id 20120819°1721
 *  @type {Node|null} — The Ready button used with the inline edit mode
 */
Daf.Mnu.Vars.button1Ready = null;

/**
 *  This property ..
 *
 *  @id 20120819°1722
 *  @type {Node|null} — The Cancel button used with the inline edit mode
 */
Daf.Mnu.Vars.button2Cancel = null;

/**
 *  This namespace holds variables
 *
 *  @id 20190411°0423
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.VrsPrm = Daf.Mnu.VrPrm || {};

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// Variables part one for the edit feature [seq 20110811°1211]

/**
 *  The clicked pagemenu/elementmenu icon as parameter from the icon click to subsequent functions
 *   (Global variable abused as parameter, how ugly!)
 *
 *  @id 20110811°1212
 *  @type {Object|null}
 */
Daf.Mnu.VrsPrm.oSelf = null;

/**
 *  Store 'P' or 'TD' so far, para from .. to eps-quirkscms.js
 *   (Global variable abused as parameter, how ugly!)
 *
 *  @id 20110811°1213
 *  @type {string|null}
 */
Daf.Mnu.VrsPrm.sActualNodeType = null;

/**
 *  Preserve original text to possibly be restored
 *   (Global variable abused as parameter, how ugly!)
 *
 *  @id 20110811°1214
 *  @type {string}
 */
Daf.Mnu.VrsPrm.sOriginalText = '';
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

/**
 *  This namespace holds variables
 *
 *  @id 20190411°0425
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.VrsEd = Daf.Mnu.VrEd || {};

// + + + + + + + + + + + + + + + + + + + + + + + + + + +
// variables part two for the edit feature [seq 20110811°1215]

/**
 *  This element
 *
 *  @id 20120903°1921
 *  @type {Object|null}
 */
Daf.Mnu.VrsEd.eGlobEditControl = null;

/**
 *  This property ...
 *   Defined as global property to survive from ElementEditCatch() to EditFinish*()
 *
 *  @id 20110811°1216
 *  @type {Array}
 */
Daf.Mnu.VrsEd.aTextareas = [];

/**
 *  This property .. 
 *
 *  @id 20110811°1217
 *  @type {Object|null}
 */
Daf.Mnu.VrsEd.eGlobLabels = [];

/**
 *  This property .. 
 *
 *  @id 20120829°1941
 *  @type {Object|null}
 */
Daf.Mnu.VrsEd.eGlobTarget = null;

/**
 *  Not used ?!
 *
 *  @id 20121005°1531
 *  @type {string}
 */
Daf.Mnu.VrsEd.sGlobLocalMenuIcon = '';

/**
 *  This property .. 
 *
 *  @id 20110811°1218
 *  @type {string}
 */
Daf.Mnu.VrsEd.sEditPostfix = '';

/**
 *  Helper while having old and new click in parallel, eliminate if old style is eliminated
 *
 *  @id 20200410°0925
 *  @type {boolean|null}
 */
Daf.Mnu.VrsEd.bClickedEditPointHooverBox = null;
// + + + + + + + + + + + + + + + + + + + + + + + + + + +

/**
 *  This namespace holds variables
 *
 *  @id 20190411°0427
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Mnu.VrsMop = Daf.Mnu.VrMop || {};


// % % % % % % % % % % % % % % % % % % % % % % % % % % %
// globals for mopen() [seq 20110811°1251]

/**
 *  Global for mopen ..
 *
 *  @id 20110811°1252
 *  @type {}
 */
Daf.Mnu.VrsMop.timeout = 100;                                           // 500;

/**
 *  Global for mopen ..
 *
 *  @id 20110811°1253
 *  @type {}
 */
Daf.Mnu.VrsMop.closetimer = 0;

/**
 *  Global for mopen ..
 *
 *  @id 20110811°1254
 *  @type {}
 */
Daf.Mnu.VrsMop.ddmenuitem = 0;
// % % % % % % % % % % % % % % % % % % % % % % % % % % %


/* *
 *  This anonymous self-executing function creates two buttons
 *
 *  @id 20190403°0731
 *  @return {undefined} —
 */
( function() {
    "use strict";

    // Process button one [seq 20120819°1723]
    Daf.Mnu.Vars.button1Ready = document.createElement('BUTTON');
    Daf.Mnu.Vars.button1Ready.appendChild(document.createTextNode('Ready'));

    // Process button two [seq 20120819°1724]
    Daf.Mnu.Vars.button2Cancel = document.createElement('BUTTON');
    Daf.Mnu.Vars.button2Cancel.appendChild(document.createTextNode('Cancel'));

}());


/**
 *  This namespace holds some early variables
 *
 *  @id 20160502°0411
 *  @c_o_n_s_t — Namespace
 */
Daf.Start =
{
   /**
    *  This namespace holds quick debug flags and the like
    *
    *  @id 20160610°0411
    *  @see summary 20190416°0611 'on namespace usage'
    *  @callers
    *  @c_o_n_s_t — Namespace
    */
    Flags : {

        // Local debug flags (shall be ordered chronologically)
        onMouseOver_1_Hello : false                                     // Toggle true false
        , onMouseOver_2_More : true // false                            // Toggle true false
        , onMousOver_3_Alert : false // true                            //
        , jqueryHello : true                                            //

        // Debug alert flags
        , bALERT_00200_jqueryDocumentReady : false                      // Toggle true false
        , bALERT_00400_pullScriptsBehind_PART_1 : false                 // Not used // 'Debug 3' IF SET TRUE, SCRIPTS WILL LOAD, IF SET FALSE, SCRIPTS WILL FAIL TO LOAD. HOW CAN THIS BE?! LOOKS LIKE A TIMING PROBLEM?
        , bALERT_00500_jsibootstrap_PART_2 : false  // 1x               // Show result of carnary pings
        , bALERT_00500_jsibootstrap_PART_2_2 : false // 1x              // Show result of carnary pings SECOND TIMES - CURIOUSLY, IF THE FIRST DIDN'T SHOW THE SCRIPTS, THE SECOND DOES (see issue 20120101°2001 'timing problem')

        // Array of flags, cookie list [var 20150322°0411]
        // note 20160621°0431 : This var seems not used so far
        , Flags : []

        // Style elements [var 20150322°0421]
        // note 20160621°0432 : This var seems used not much so far
        , Elemant : []

        // Cache standard output messages [var 20150516°0451]
        // note : This is wanted as long as page is not yet available
        , InitialMessageCache : []

        /**
         *  This class defines one flag object
         *
         *  @id 20150322°0331
         */
        , Flag : function () {
           "use strict";
           this.Cookinam = "";
           this.Selfnome = "";
           this.Usertext = "";
           this.Valuet = "";
        }
    }                                                                   // End namespace Daf.Start.DbgFlags

    /**
     *  This namespace Daf.Start.Funcs holds .. functions
     *
     *  @id 20160622°0111
     *  @callers
     *  @c_o_n_s_t — Namespace
     */
    , Funcs : {

        /**
         *  This class defines one div element to insert in the page
         *
         *  @id 20150322°0341
         *  @status Not used yet
         *  @callers • none so far
         *  @constructor —
         *  @return {undefined} —
         */
        Divio : function () {
            "use strict";
            this.Family = "";                                           // 'id' or 'class'
            this.Selfnome = "";                                         //
            this.Usertext = "";                                         //
            this.Valuet = "";                                           // The element string to append
        }

        /**
         *  This function constitutes an event handler ...
         *   Prototype usage in moonwalk.html#id20160622o0121
         *
         *  @id 20150318°0651
         *  @note Curiously, this eventhandler is only called on the first time
         *      entering the element, but no more on successvie enterings. Why?
         *  @ref ref 20150318°0543 'stackoverflow → get id of element that fired event'
         *  @param {Element} eThis — The caller element
         *  @return {undefined} —
         */
        , onMousOverEvant : function (eThis)
        {
            "use strict";

            var sInner = eThis.innerHTML;
            var sId = eThis.id;
            if ( Daf.Start.Flags.onMousOver_3_Alert )
            {
                var s = "[dbg 20150318°0632] Hello Daf.Start.Funcs.onMousOverEvant()"
                       + "\n\n inner = " + sInner
                        + "\n id = " + sId
                         ;
                alert(s);
            }
        }
    } // end nested namespace Daf.Start.Funcs

    /**
     *  This function tests func 20110821°0121 pull-behind feature
     *
     *  @id 20160503°0141
     *  @see ref 20160503°0121 'stackoverflow → using HTML button to call JS function'
     *  @ref : See howto 20181229°1943 'summary on pullbehind'
     *  @callers • Button id20160503o0132 • Button id20160503o0134 in page /docs/testing.html
     *  @return {undefined} —
     */
    , callCanarySqueak : function ()
    {
        "use strict";

        Daf.Vars.iTimeOfStartLoading = new Date();

        if ( ! Trekta.Utils.bUseMinified ) {                            //
            var s = Trekta.Utils.s_DaftariBaseFolderAbs;                //
            var sScriptToLoad = s + '/jsi/dafcanary.js';                // Hardcoded
            Trekta.Utils.pullScriptBehind ( sScriptToLoad
                                           , function () { DafCanary.squeak(); }
                                            , null
                                             , null
                                              );
                                   }
        return;
    }

    /**
     *  This function tells whether we are on localhost or online
     *
     *  @id 20180514°0121
     *  @status new
     *  @todo 20180514°0122 : Also we should discern, whether the page is viewed via file protocol or not
     *  @see ref 20180514°0132 'stackoverflow → difference window.location.host and window.location.hostname'
     *  @callers ..
     *  @return {boolean} — The wanted flag
     */
    , isLocalhost : function ()
    {
        "use strict";

        var bLocalhost = false;
        if ( window.location.hostname === 'localhost' )
        {
            bLocalhost = true;
        }
        return bLocalhost;
    }

};                                                                      // End namespace Daf.Start

/* eof */
