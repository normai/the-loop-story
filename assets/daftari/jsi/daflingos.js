/*!
 *  file : 20110512°1331 daftari/jsi/daflingos.js
 *  summary : This file provides the Lingos feature (multilingual)
 *  encoding : UTF-8-without-BOM
 *  license : GNU AGPL v3
 *  copyright : © 2011 - 2023 Norbert C. Maier
 *  authors :
 *  see : ref 20110512°1721 'ISO 639.2 → Language codes Standard'
 *  callers : • daftari.js::daftari()
 */

// Options for JSHint [seq 20190423°0421`05]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf, Trekta */

/**
 *  Announce namespace
 *
 *  @id 20190106°0311`13
 *  @c_o_n_s_t — Namespace
 */
Trekta = window.Trekta || {};

/**
 *  Announce Daftari root namespace
 *
 *  @id 20170923°0421`04
 *  @c_o_n_s_t — Namespace
 */
Daf = window.Daf || {};

/**
 *  This namespace defines the Lingos constants
 *
 *  @id 20150316°0911
 *  @todo Also fetch the other members from top-level into namespace here.
 *  @c_o_n_s_t — Namespace
 */
Daf.Lngo = {};

/**
 *  Retrieve translations for given token and language set
 *
 *  @id 20161109°2011
 *  @callers So far only • func 20160417°1241 Daf.Sitmp.workoff_Cake_4_assemble
 *  @param {string} sCrumbText — The phrase token
 *  @param {Array} aLangsWanted — The wanted languages
 *  @return {Array} — The wanted translations
 */
Daf.Lngo.getLangsFromCrumb = function(sCrumbText, aLangsWanted)
{
    'use strict';
    var aTerms = [];

    // Detect token id [line 20161109°2032]
    // If it does not contain '#' return the text as given
    if (sCrumbText.indexOf("#") < 0) {
        return [sCrumbText];
    }
    // The given crumb text is a lingo token and can be translated

    // Guarantee one wanted language [seq 20190416°0531]
    if (aLangsWanted.length < 1) {
        aLangsWanted[0] = 'eng';
    }

    // Possibly use cache [seq 20190404°0611]
    // See story 20190404°0541 'before and after implementing sitmaplangs.js cache'
    if ( Daf.Lngo.dCacheDataReadFile === null ) {
        // fatal
        aTerms[0] = sCrumbText;
    }
    else {
        // New algo [seq 20190416°0331]
        for (var i7 = 0; i7 < aLangsWanted.length; i7++) {

            /*
            Issue 20210518°1111 'Phrase with octothorp'
            Situation : Page 20200314°0511 scripts.html sCrumbTest = 'C#'
            Browser throws "Uncaught TypeError: Cannot read property 'eng' of undefined"
            Reference : https://dmitripavlutin.com/check-if-object-has-property-javascript/ [ref 20210518°1122]
            Do : Find out whether Daf.Lngo.dCacheDataReadFile[sCrumbText] has a [aLangsWanted[i7]]
            Note : The issues slogan may be misleading, because the issue is not the octothorp but the undefined property.
            Status : Solved empirically by seq 20210518°1131
            */

            // Paranoia [seq 20210518°1131] Catch case of unavailable wanted language
            // Should never happen, but does e.g. if sitmaplangs.js is not correct.
            // Remember issue 20210518°1111 What if phrase itself has octothorp?
            // if (! sCrumbText in Daf.Lngo.dCacheDataReadFile) { // Why does this fail to decide correctly?
            if (Daf.Lngo.dCacheDataReadFile[sCrumbText] === undefined) {
                aTerms[i7] = sCrumbText;
                return aTerms;
            }

            aTerms[i7] = Daf.Lngo.dCacheDataReadFile[sCrumbText][aLangsWanted[i7]];
        }
    }

    return aTerms;
};

/**
 *  Find the languages present on this page except in cakecrumbs bag
 *
 *  @id 20160618°0321
 *  @callers • 20120902°1721 Daf.Lngo.wafaili_lingos • 20160417°1241 Daf.Sitmp.workoff_Cake_4_assemble
 *  @return {Array} — The languages found on this page (not those in the cakecrumbs bag)
 */
Daf.Lngo.getLangsOnPage = function()
{
    'use strict';

    // Prologue [seq 20190416°0312]
    var elesSpan = document.getElementsByTagName('span');
    var aSpans = Array.prototype.slice.call( elesSpan );                // convert HTMLCollection to array

    // Clean array from spans inside the cake [seq 20190416°0313]
    var eCake = document.getElementsByClassName(Daf.Dspat.Config.sFiturIdent_Cakecrumbs_FiHtClass)[0];  // 'DafFurnitureCakecrumbs_Bag'
    for (var i7 = aSpans.length - 1; i7 >= 0; i7--) {
        var ele = aSpans[i7];
        if ( typeof eCake !== 'undefined' ) {
            if ( eCake.contains(ele) ) {
                aSpans.splice(i7, 1);
            }
        }
    }

    // Extract Lingos [seq 20190416°0314]
    var aLangs = [];
    for (i7 = 0; i7 < aSpans.length; i7++) {

        // Convenience [seq 20190416°0315]
        var sCls = aSpans[i7].className;

        // Skip classnames not starting with 'lang' [seq 20190416°0316]
        if ( sCls.match(/^lang.+/) === null ) {
            continue;
        }

        // Discard any second classes [line 20190416°0317]
        sCls = sCls.split(' ')[0];

        // Reduce classname to lingo name [line 20190416°0323]
        sCls = sCls.substr(4);

        // Maintain wanted list [seq 20190416°0318]
        if (aLangs.indexOf(sCls) < 0) {
            aLangs.push(sCls);
        }
    }

    // Debug [seq 20190416°0319]
    if (Daf.Const.b_Toggle_TRUE) {
        var sDbg = '[Debug 20190416°0323] Lingos found on this page :'
                  + ' • ' + (aLangs + '')                               // convert array to comma separated string
                   ;
        Trekta.Utils.outDbgMsg(sDbg);
    }

    // Sort array by codes [seq 20190416°0541]
    // See feature 20190416°0543 'unsteady lingos'
    var aLngsSorted = [];
    for (var i23 = 0; i23 < Daf.Const.aLang_Codes.length; i23++) {
        if ( aLangs.indexOf(Daf.Const.aLang_Codes[i23]) > -1) {
            aLngsSorted.push(Daf.Const.aLang_Codes[i23]);
        }
    }

    return aLngsSorted;
};

/**
 *  This function actualizes the Lingos on the page
 *
 *  @id 20190416°0411 [former 20110512°1334]
 *  @callers Only • func 20110512°1332 Daf.Lngo.onloadLangSplit
 *  @param {string} sLang — The language to be set
 *  @return {undefined} —
 */
Daf.Lngo.langSplit = function(sLang)
{
    'use strict';
    var mt = null;                                                      // For match

    // Save physical HTML for bake-static-cakecrumbs comparison [seq 20190416°0453]
    var eCake = document.getElementsByClassName(Daf.Dspat.Config.sFiturIdent_Cakecrumbs_FiHtClass)[0];  // 'DafFurnitureCakecrumbs_Bag'
    if (typeof eCake !== 'undefined' ) {
        Daf.Lngo.sSaveStaticCakecrumbs = eCake.innerHTML;
    }

    // Get all spans [line 20190416°0414]
    var esPgSpans = document.getElementsByTagName('span');

    // Make list of all parent elements containing Lingo spans [seq 20190416°0417]
    var aParents = [];
    for (var i3 = 0; i3 < esPgSpans.length; i3++) {
        var elSpan = esPgSpans[i3];
        //  [seq 20190416°0419]
        if ( elSpan.className.match(/^lang.+/) !== null ) {
            //  [seq 20190416°0421]
            if ( aParents.indexOf(elSpan.parentElement ) < 0 ) {
                aParents.push(elSpan.parentElement);
            }
        }
    }

    // Loop over all elements which contain lingo spans [seq 20190416°0423]
    for (var i4 = 0; i4 < aParents.length; i4++) {

        // Get list of local span elements [seq 20190416°0425]
        var esSpans = aParents[i4].getElementsByTagName('span');

        // Clean from non-lang spans [seq 20190416°0427]
        var aSpans = Array.prototype.slice.call( esSpans );             // convert HTMLCollection to array
        for (var i41 = aSpans.length - 1; i41 >= 0 ; i41--) {
            mt = aSpans[i41].className.match(/^lang.+/);
            if (mt === null) {
                aSpans.splice(i41, 1);
            }
        }

        // Debug [seq 20190416°0631] moonwalk.html
        if ( aParents[i4].id === 'id20111231o0221' ) {
            var xBrkpt = xBrkpt;
        }

        // Detect possible span blocks [seq 20190416°0429]
        var iLngNdx = 99;
        var aBlock = [];
        for (var i5 = 0; i5 < aSpans.length; i5++) {
            var eSpn = aSpans[i5];
            var sClsnm = eSpn.className.substr(4, 3);
            var i23 = Daf.Const.aLang_Codes.indexOf(sClsnm);
            // New block?
            if (i23 < iLngNdx) {
                aBlock.push([]);
            }
            aBlock[aBlock.length - 1].push(eSpn);
            iLngNdx = i23;
        }

        // Process block [seq 20190416°0433]
        for (var i8 = 0; i8 < aBlock.length; i8++) {

            // Preparation in case wanted lang is not available [seq 20190416°0435]
            var eFallbkSpan = aBlock[i8][0];

            // Switch all lingo spans hidden except current language [seq 20190416°0437]
            var bFound = false;
            var rex = new RegExp('^lang' + sLang + '.*');
            for (var i9 = 0; i9 < aBlock[i8].length; i9++) {
                var eSpan = aBlock[i8][i9];
                var sCls = eSpan.className;
                var aClasses = sCls.split(' ');                         // Make additional classes available [seq 20190416°0633]
                mt = sCls.match(rex);
                if ( mt !== null ) {
                    // [seq 20190416°0439]
                    if ( aClasses.indexOf('invisible') < 0 ) {          // Do not overwrite css class 'invisible' [condi 20190416°0635]
                        eSpan.style.display = 'inline';
                    }
                    //eSpan.style.color = Daf.Lngo.aLangDefs[iLngNdx][Daf.Lngo.sPropColor];  // 'color'
                    bFound = true;
                }
                else {

                    // [condi 20190416°0441]
                    if (sLang === 'all') {

                        // [seq 20190416°0443]
                        var iLangNdx = Daf.Const.aLang_Codes.indexOf(eSpan.className.substr(4, 3));
                        if ( aClasses.indexOf('invisible') < 0 ) {      // Do not overwrite css class 'invisible' (like above) [condi 20190416°0631]
                            eSpan.style.display = 'inline';
                        }

                        // Provisory ease for occasional undefined element [seq 20190416°0523]
                        // Clear away issue 20190416°0521 'lang span class must be more tolerant'
                        try {
                            eSpan.style.color = Daf.Lngo.aLangDefs[iLangNdx][Daf.Lngo.sPropColor];  // 'color'
                        } catch (ex) {
                            // This did happen with spans with additional classes e.g. 'invisible'
                            alert('Holla 20190416°0521 ..');
                        }

                        // No more fallback wanted
                        bFound = true;
                    }
                    else {
                        // [seq 20190416°0445]
                        eSpan.style.display = 'none';
                    }
                }
            }

            // Provide fallback [seq 20190416°0447]
            if (! bFound) {
                eFallbkSpan.style.display = 'inline';
                eFallbkSpan.style.color = 'Silver';
            }
        }
    }

    return;                                                             // Breakpoint
};

/**
 *  This function constitutes the langsplit event handler after page load
 *
 *  @id 20110512°1332
 *  @callers : daftari.js at seq 20150316°0931
 *  @param {boolean|null} bForce — Flag to allow reentrance, e.g. from blogs.js::loadReady
 *  @return {undefined} —
 */
Daf.Lngo.onloadLangSplit = function(bForce)
{
    'use strict';

    // Allow or deny reentrance [line 20190416°0131]
    if ( bForce ) {
        Daf.Lngo.bStatus_LangsplitIsStarted = false;
    }

    // Prohibit reentrance [seq 20150210°1431]
    if ( Daf.Lngo.bStatus_LangsplitIsStarted ) {
        var s = '[RedErrorPane 20150210°1432]'
                 + ' Daf.Lngo.onloadLangSplit is called double'
                  + ' (perhaps from body element).'
                    ;
        Trekta.Utils.outDbgMsg(s);

        // Do not execute this function again
        return;
    }
    Daf.Lngo.bStatus_LangsplitIsStarted = true;                         // Reentrance flag

    // Determine wanted language [line 20161109°2021] (compare line 20161109°2022)
    var sCookieValueLanguage = Trekta.Utils.getCookie(Daf.Lngo.sCookieNameLanguage);

    // Retrieve cookie as boolean [seq 20120913°1902 cookie]
    var b1 = Trekta.Utils.getCookieBool ( Daf.Lngo.sCookieName_debug_OnloadLngSplit, null);
    if ( b1 )
    {
        var s1 = '[Dbg 20120913°1903] Daf.Lngo.onloadLangSplit()'
                + '\n Trekta.Utils.getCookieBool() : '
                  + '\n - cookie name = ' + Daf.Lngo.sCookieNameLanguage
                   + '\n - cookie value = ' + sCookieValueLanguage
                    ;
        alert(s1);
    }

    // () Try reading default lingo from the HTML page [seq 20150316°0921]
    if ( (! sCookieValueLanguage) || sCookieValueLanguage === '')       // Add null comparison [fix 20210416°1521]
    {
        // Determine language without cookie [feature 20210416°1524]
        // This fixes issue 20210416°1311 'Default Lingo fail'
        // Simplify reading! [todo 20210517°1111`23]
        if ( typeof DAF_SETTINGS !== 'undefined' && typeof DAF_SETTINGS.default_lang !== 'undefined' ) {
            sCookieValueLanguage = DAF_SETTINGS[Daf.Lngo.sIni_LangDefault_Sitmapdaf];
        }
        else {
            sCookieValueLanguage = Daf.Lngo.sIni_LangDefault_Fallback;
        }
        Trekta.Utils.setCookie(Daf.Lngo.sCookieNameLanguage, sCookieValueLanguage, 2);
    }

    // Do the job [line 20190416°0351]
    Daf.Lngo.langSplit(sCookieValueLanguage);

    // Set language selection indicators [seq 20120831°1331]
    // note : Prerequisite is that the language selection area must already
    //    have been rendered, because in this sequence here, only existing
    //    flags are dimmed or brightend respectively.
    for (var i = 0; i < Daf.Const.aLang_Codes.length; i++)
    {
        var e2 = document.getElementById('langflax_' + Daf.Const.aLang_Codes[i]);
        if (e2)
        {
            if ( Daf.Const.aLang_Codes[i] === sCookieValueLanguage )
            {
                e2.src = Trekta.Utils.s_DaftariBaseFolderRel + '/' + Daf.Const.sFOLDER_FLAGS + Daf.Lngo.aLangDefs[i][Daf.Lngo.sPropFlagOn];  // 'flagon'

            }
            else
            {
                e2.src = Trekta.Utils.s_DaftariBaseFolderRel + '/' + Daf.Const.sFOLDER_FLAGS + Daf.Lngo.aLangDefs[i][Daf.Lngo.sPropFlagOff];  // 'flagoff'
            }

            // [seq 20150405°1811]
            e2.style.border = Daf.Lngo.sFlagsStyleBorder1;              // 'none';
            e2.style.marginLeft = Daf.Lngo.sFlagsStyleMarginHorizontal;  // '3px';
            e2.style.marginRight = Daf.Lngo.sFlagsStyleMarginHorizontal;  // '3px';
            e2.style.marginBottom = Daf.Lngo.sFlagsStyleMarginVertical;  // '1px';
            e2.style.marginTop = Daf.Lngo.sFlagsStyleMarginVertical;    // '1px';
            e2.style.width = Daf.Lngo.sFlagsStyleWidth;                 // "27px";
            e2.style.height = Daf.Lngo.sFlagsStyleHeight;               // "18px";
        }
    }
};

/**
 *  This function sets the language selection cookie
 *
 *  @id 20110512°1333
 *  @callers • *.html::href::onclick()
 *  @param sCookieValueLanguage {string} The language cookie name, possibly superfluous.
 *  @return {undefined} —
 */
Daf.Lngo.setLanguage = function(sCookieValueLanguage)
{
    'use strict';

    // Do the job
    Trekta.Utils.setCookie(Daf.Lngo.sCookieNameLanguage, sCookieValueLanguage, 2);

    // Refresh page
    Daf.Utlis.refreshPage();
};

/**
 *  This function injects the Lingo Selection Area
 *
 *  @id 20120902°1721
 *  @callers • Daf.Core.daftari()
 *  @return {undefined} —
 */
Daf.Lngo.wafaili_lingos = function() {

  'use strict';

    // Prologue
    var sLingos = '';
    var sXbts = '';
    var aResult = [];
    aResult[0] = sLingos;
    aResult[1] = sXbts;

    var aLangs = Daf.Lngo.getLangsOnPage();

    // No languages found?
    if (aLangs.length < 1) {
        return aResult;
    }

    // Start lingos paragraph
    sLingos = '<p style="margin-top:0px; margin-bottom:0px; text-align:right;'
             + ' color:gray;" data-wafailiskip="yes">Select language:'
              ;
    for (var i2 = 0; i2 < aLangs.length; i2++) {

        // Retrieve lang index
        var iLangIndex = Daf.Utlis.getArrayIndexOf(Daf.Const.aLang_Codes, aLangs[i2]);

        // Build html for flag
        sLingos += Daf.Const.sTb_1Cr
                 + '<span onclick=Daf.Lngo.setLanguage("' + aLangs[i2] + '")>'
                 + '<img id="langflax_' + aLangs[i2] + '"'
                 + ' src="' + Trekta.Utils.s_DaftariBaseFolderRel + '/' + Daf.Const.sFOLDER_FLAGS + Daf.Lngo.aLangDefs[iLangIndex][Daf.Lngo.sPropFlagOn] + '"'
                 + ' width="21" height="14" alt="Flag of ..."'
                 + ' style="border:solid gray 1px;"'
                 + ' onmouseover="this.style.cursor=\'pointer\';'
                 + ' Daftari.XBT(this, {id:\'xbtLangflag_' + aLangs[i2] + '\'});"'
                 + ' />'
                 + '</span>'
                  ;

        sXbts += Daf.Const.sTb_1Cr
               + '<div'
               + ' id="xbtLangflag_' + aLangs[i2] + '" class="xbtooltip"'
               + ' style="display:none; color:darkmagenta;">'
               + '<big><big><b>' + Daf.Lngo.aLangDefs[iLangIndex][Daf.Lngo.sPropName1] + '</b></big></big>'
               + '</div>'
                ;
    }

    //--------------------------
    // todo 20120903°0121 : Sequence from above is provisory doubled,
    //    by outsourcing as function re-merge this with above sequence.
    var sLangsi = 'all';

    var aLangDefsiName = 'All Languages';
    sLingos += Daf.Const.sTb_1Cr
             + '<span onclick=Daf.Lngo.setLanguage("' + 'all' + '")>'
              + '<img id="langflax_' + sLangsi + '"'
              + ' src="' + Trekta.Utils.s_DaftariBaseFolderRel
              + '/' + Daf.Const.sFOLDER_FLAGS + Daf.Const.aLang_DefsiFlagon + '"' // e.g. '20120831o1321.flag-of-all.v0.x0021y0014.png'
              + ' width="21" height="14" alt="Flag of ..."'
              + ' style="border:solid gray 1px;"'
              + ' onmouseover="this.style.cursor=\'pointer\';'
              + ' Daftari.XBT(this, {id:\'xbtLangflag_' + sLangsi + '\'});"'
              + ' />'
               + '</span>'
                ;
    sXbts += Daf.Const.sTb_1Cr
           + '<div'
            + ' id="xbtLangflag_' + sLangsi + '" class="xbtooltip"'
             + ' style="display:none; color:darkmagenta;">'
              + '<big><big><b>' + aLangDefsiName + '</b></big></big>'
               + '</div>'
                ;
    //--------------------------

    sLingos += '</p>';
    aResult[0] = sLingos;
    aResult[1] = sXbts;

    return aResult;
};

// = = = = = = = = = = = = = = = = = = = = = = = = = =
// Define the Lingos constants [seq 20120831°1311]

// Provide programmatic property names [seq 20190407°0622]
// Remember howto 20190407°0621 'Refactor property names writing style'
Daf.Lngo.sPropCode = 'code';
Daf.Lngo.sPropColor = 'color';
Daf.Lngo.sPropFlagOff = 'flagoff';
Daf.Lngo.sPropFlagOn = 'flagon';
Daf.Lngo.sPropFontFam = 'fontfam';
Daf.Lngo.sPropName1 = 'name';
Daf.Lngo.sPropName2 = 'name2';

// Keep this array in sync with const 20140605°1517 aLangCodes array in daftari.js
Daf.Lngo.aLangDefs = [];

Daf.Lngo.aLangDefs[0] = {};
Daf.Lngo.aLangDefs[0][Daf.Lngo.sPropName1]   = 'English';
Daf.Lngo.aLangDefs[0][Daf.Lngo.sPropName2]   = 'English';
Daf.Lngo.aLangDefs[0][Daf.Lngo.sPropCode]    = 'eng';
Daf.Lngo.aLangDefs[0][Daf.Lngo.sPropFlagOff] = '20090905o1902.flag-of-britain.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[0][Daf.Lngo.sPropFlagOn]  = '20090905o190202.flag-of-britain-gray.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[0][Daf.Lngo.sPropColor]   = 'Green';
Daf.Lngo.aLangDefs[0][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[1] = {};
Daf.Lngo.aLangDefs[1][Daf.Lngo.sPropName1]   = 'Arabic';
Daf.Lngo.aLangDefs[1][Daf.Lngo.sPropName2]   = '(Arabic)';
Daf.Lngo.aLangDefs[1][Daf.Lngo.sPropCode]    = 'ara';
Daf.Lngo.aLangDefs[1][Daf.Lngo.sPropFlagOff] = '20120902o1412.arabic-language-flag.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[1][Daf.Lngo.sPropFlagOn]  = '20120902o141202.arabic-language-flag-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[1][Daf.Lngo.sPropColor]   = 'Orange';                // 'DarkTurquoise';
Daf.Lngo.aLangDefs[1][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam2;

Daf.Lngo.aLangDefs[2] = {};
Daf.Lngo.aLangDefs[2][Daf.Lngo.sPropName1]   = 'Chinese';
Daf.Lngo.aLangDefs[2][Daf.Lngo.sPropName2]   = '(Chinese)';
Daf.Lngo.aLangDefs[2][Daf.Lngo.sPropCode]    = 'chi';
Daf.Lngo.aLangDefs[2][Daf.Lngo.sPropFlagOff] = '20120902o1416.flag-of-china.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[2][Daf.Lngo.sPropFlagOn]  = '20120902o141602.flag-of-china-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[2][Daf.Lngo.sPropColor]   = 'Tomato';
Daf.Lngo.aLangDefs[2][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[3] = {};
Daf.Lngo.aLangDefs[3][Daf.Lngo.sPropName1]   = 'Czech';
Daf.Lngo.aLangDefs[3][Daf.Lngo.sPropName2]   = '(Czech)';
Daf.Lngo.aLangDefs[3][Daf.Lngo.sPropCode]    = 'cze';
Daf.Lngo.aLangDefs[3][Daf.Lngo.sPropFlagOff] = '20090905o1912.flag-of-czechrepublic.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[3][Daf.Lngo.sPropFlagOn]  = '20090905o191202.flag-of-czechrepublic-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[3][Daf.Lngo.sPropColor]   = 'CadetBlue';             // MediumViolet Red RoyalBlue
Daf.Lngo.aLangDefs[3][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[4] = {};
Daf.Lngo.aLangDefs[4][Daf.Lngo.sPropName1]   = 'Danish';
Daf.Lngo.aLangDefs[4][Daf.Lngo.sPropName2]   = '(Danish)';
Daf.Lngo.aLangDefs[4][Daf.Lngo.sPropCode]    = 'dan';
Daf.Lngo.aLangDefs[4][Daf.Lngo.sPropFlagOff] = '20090905o1922.flag-of-denmark.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[4][Daf.Lngo.sPropFlagOn]  = '20090905o192202.flag-of-denmark-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[4][Daf.Lngo.sPropColor]   = 'SpringGreen';
Daf.Lngo.aLangDefs[4][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[5] = {};
Daf.Lngo.aLangDefs[5][Daf.Lngo.sPropName1]   = 'Finnish';
Daf.Lngo.aLangDefs[5][Daf.Lngo.sPropName2]   = '(Finnish)';
Daf.Lngo.aLangDefs[5][Daf.Lngo.sPropCode]    = 'fin';
Daf.Lngo.aLangDefs[5][Daf.Lngo.sPropFlagOff] = '20090905o1932.flag-of-finland.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[5][Daf.Lngo.sPropFlagOn]  = '20090905o193202.flag-of-finland-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[5][Daf.Lngo.sPropColor]   = 'RosyBrown';
Daf.Lngo.aLangDefs[5][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[6] = {};
Daf.Lngo.aLangDefs[6][Daf.Lngo.sPropName1]   = 'French';
Daf.Lngo.aLangDefs[6][Daf.Lngo.sPropName2]   = 'Français';              // 'français';
Daf.Lngo.aLangDefs[6][Daf.Lngo.sPropCode]    = 'fre';
Daf.Lngo.aLangDefs[6][Daf.Lngo.sPropFlagOff] = '20090905o1942.flag-of-france.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[6][Daf.Lngo.sPropFlagOn]  = '20090905o194202.flag-of-france-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[6][Daf.Lngo.sPropColor]   = 'Red';
Daf.Lngo.aLangDefs[6][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam2;

Daf.Lngo.aLangDefs[7] = {};
Daf.Lngo.aLangDefs[7][Daf.Lngo.sPropName1]   = 'German';
Daf.Lngo.aLangDefs[7][Daf.Lngo.sPropName2]   = 'Deutsch';
Daf.Lngo.aLangDefs[7][Daf.Lngo.sPropCode]    = 'ger';
Daf.Lngo.aLangDefs[7][Daf.Lngo.sPropFlagOff] = '20090905o1952.flag-of-germany.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[7][Daf.Lngo.sPropFlagOn]  = '20090905o195202.flag-of-germany-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[7][Daf.Lngo.sPropColor]   = 'Blue';
Daf.Lngo.aLangDefs[7][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[8] = {};
Daf.Lngo.aLangDefs[8][Daf.Lngo.sPropName1]   = 'Greek';
Daf.Lngo.aLangDefs[8][Daf.Lngo.sPropName2]   = '(Greek)';
Daf.Lngo.aLangDefs[8][Daf.Lngo.sPropCode]    = 'gre';
Daf.Lngo.aLangDefs[8][Daf.Lngo.sPropFlagOff] = '20120902o1418.flag-of-greece.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[8][Daf.Lngo.sPropFlagOn]  = '20120902o141802.flag-of-greece-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[8][Daf.Lngo.sPropColor]   = 'MediumAquamarine';
Daf.Lngo.aLangDefs[8][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[9] = {};
Daf.Lngo.aLangDefs[9][Daf.Lngo.sPropName1]   = 'Hindi';
Daf.Lngo.aLangDefs[9][Daf.Lngo.sPropName2]   = '(Hindi)';
Daf.Lngo.aLangDefs[9][Daf.Lngo.sPropCode]    = 'hin';
Daf.Lngo.aLangDefs[9][Daf.Lngo.sPropFlagOff] = '20120902o1420.flag-of-india.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[9][Daf.Lngo.sPropFlagOn]  = '20120902o142002.flag-of-india-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[9][Daf.Lngo.sPropColor]   = 'CornflowerBlue';
Daf.Lngo.aLangDefs[9][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[10] = {};
Daf.Lngo.aLangDefs[10][Daf.Lngo.sPropName1]   = 'Italian';
Daf.Lngo.aLangDefs[10][Daf.Lngo.sPropName2]   = '(Italian)';
Daf.Lngo.aLangDefs[10][Daf.Lngo.sPropCode]    = 'ita';
Daf.Lngo.aLangDefs[10][Daf.Lngo.sPropFlagOff] = '20090905o2002.flag-of-italy.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[10][Daf.Lngo.sPropFlagOn]  = '20090905o200202.flag-of-italy-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[10][Daf.Lngo.sPropColor]   = 'Firebrick';
Daf.Lngo.aLangDefs[10][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[11] = {};
Daf.Lngo.aLangDefs[11][Daf.Lngo.sPropName1]   = 'Japanese';
Daf.Lngo.aLangDefs[11][Daf.Lngo.sPropName2]   = '(Japanese)';
Daf.Lngo.aLangDefs[11][Daf.Lngo.sPropCode]    = 'jpn';
Daf.Lngo.aLangDefs[11][Daf.Lngo.sPropFlagOff] = '20120902o1422.flag-of-japan.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[11][Daf.Lngo.sPropFlagOn]  = '20120902o142202.flag-of-japan-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[11][Daf.Lngo.sPropColor]   = 'RosyBrown';
Daf.Lngo.aLangDefs[11][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[12] = {};
Daf.Lngo.aLangDefs[12][Daf.Lngo.sPropName1]   = 'Nepali';
Daf.Lngo.aLangDefs[12][Daf.Lngo.sPropName2]   = '(Nepali)';
Daf.Lngo.aLangDefs[12][Daf.Lngo.sPropCode]    = 'nep';
Daf.Lngo.aLangDefs[12][Daf.Lngo.sPropFlagOff] = '20120902o1424.flag-of-nepal.v0.x0011y0014.png';
Daf.Lngo.aLangDefs[12][Daf.Lngo.sPropFlagOn]  = '20120902o142402.flag-of-nepal-gray.x0011y0014.png';
Daf.Lngo.aLangDefs[12][Daf.Lngo.sPropColor]   = 'Crimson';
Daf.Lngo.aLangDefs[12][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[13] = {};
Daf.Lngo.aLangDefs[13][Daf.Lngo.sPropName1]   = 'Polish';
Daf.Lngo.aLangDefs[13][Daf.Lngo.sPropName2]   = '(Polish)';
Daf.Lngo.aLangDefs[13][Daf.Lngo.sPropCode]    = 'pol';
Daf.Lngo.aLangDefs[13][Daf.Lngo.sPropFlagOff] = '20090905o2012.flag-of-poland.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[13][Daf.Lngo.sPropFlagOn]  = '20090905o201202.flag-of-poland-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[13][Daf.Lngo.sPropColor]   = 'RoyalBlue';
Daf.Lngo.aLangDefs[13][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[14] = {};
Daf.Lngo.aLangDefs[14][Daf.Lngo.sPropName1]   = 'Russian';
Daf.Lngo.aLangDefs[14][Daf.Lngo.sPropName2]   = '(Russian)';
Daf.Lngo.aLangDefs[14][Daf.Lngo.sPropCode]    = 'rus';
Daf.Lngo.aLangDefs[14][Daf.Lngo.sPropFlagOff] = '20090908o1541.flag-of-russia.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[14][Daf.Lngo.sPropFlagOn]  = '20090908o154102.flag-of-russia-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[14][Daf.Lngo.sPropColor]   = 'Magenta';              // 'Orange';
Daf.Lngo.aLangDefs[14][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[15] = {};
Daf.Lngo.aLangDefs[15][Daf.Lngo.sPropName1]   = 'Spanish';
Daf.Lngo.aLangDefs[15][Daf.Lngo.sPropName2]   = '(Spanish)';
Daf.Lngo.aLangDefs[15][Daf.Lngo.sPropCode]    = 'spa';
Daf.Lngo.aLangDefs[15][Daf.Lngo.sPropFlagOff] = '20090905o2022.flag-of-spain.v0.x0021y0014.png';  // Compare 20120902°1426 spain2
Daf.Lngo.aLangDefs[15][Daf.Lngo.sPropFlagOn]  = '20090905o202202.flag-of-spain-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[15][Daf.Lngo.sPropColor]   = 'indianred';
Daf.Lngo.aLangDefs[15][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[16] = {};
Daf.Lngo.aLangDefs[16][Daf.Lngo.sPropName1]   = 'Swahili';
Daf.Lngo.aLangDefs[16][Daf.Lngo.sPropName2]   = '(Swahili)';
Daf.Lngo.aLangDefs[16][Daf.Lngo.sPropCode]    = 'swa';
Daf.Lngo.aLangDefs[16][Daf.Lngo.sPropFlagOff] = '20120902o1426.flag-of-tanzania.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[16][Daf.Lngo.sPropFlagOn]  = '20120902o142602.flag-of-tanzania-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[16][Daf.Lngo.sPropColor]   = 'DarkTurquoise';        // 'Aquamarine';
Daf.Lngo.aLangDefs[16][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;

Daf.Lngo.aLangDefs[17] = {};
Daf.Lngo.aLangDefs[17][Daf.Lngo.sPropName1]   = 'Turkish';
Daf.Lngo.aLangDefs[17][Daf.Lngo.sPropName2]   = '(Turkish)';
Daf.Lngo.aLangDefs[17][Daf.Lngo.sPropCode]    = 'tur';
Daf.Lngo.aLangDefs[17][Daf.Lngo.sPropFlagOff] = '20120902o1428.flag-of-turkey.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[17][Daf.Lngo.sPropFlagOn]  = '20120902o142802.flag-of-turkey-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[17][Daf.Lngo.sPropColor]   = 'Cyan';
Daf.Lngo.aLangDefs[17][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam2;

Daf.Lngo.aLangDefs[18] = {};
Daf.Lngo.aLangDefs[18][Daf.Lngo.sPropName1]   = 'All';
Daf.Lngo.aLangDefs[18][Daf.Lngo.sPropName2]   = '(All)';
Daf.Lngo.aLangDefs[18][Daf.Lngo.sPropCode]    = 'all';
Daf.Lngo.aLangDefs[18][Daf.Lngo.sPropFlagOff] = '20120831o1321.flag-of-all.v0.x0021y0014.png';
Daf.Lngo.aLangDefs[18][Daf.Lngo.sPropFlagOn]  = '20120831o132102.flag-of-all-gray.x0021y0014.png';
Daf.Lngo.aLangDefs[18][Daf.Lngo.sPropColor]   = '';
Daf.Lngo.aLangDefs[18][Daf.Lngo.sPropFontFam] = Daf.Lngo.sFontFam1;
// = = = = = = = = = = = = = = = = = = = = = = = = = =


/**
 *  Reentrance flag
 *
 *  @id prop 20150210°1421
 *  @type {boolean} —
 */
Daf.Lngo.bStatus_LangsplitIsStarted = false;


/**
 *  Caching flag
 *
 *  @id prop 20190404°0551
 *  @type {Object|null} —
 */
Daf.Lngo.dCacheDataReadFile = null;

/**
 *  Cache cookie value to save cookie readings
 *
 *  @id prop 20190416°0121
 *  @type {string} —
 */
Daf.Lngo.sCacheSelectedLanguage = '';

Daf.Lngo.sCookieNameLanguage = 'language';                              // [prop 20150316°0913]
Daf.Lngo.sCookieName_debug_OnloadLngSplit = 'debug_OnloadLngSplit';     // [prop 20150316°0927]
Daf.Lngo.sFlagsStyleBorder1 = 'none';                                   // [prop 20150316°0914]
Daf.Lngo.sFlagsStyleBorder2 = 'solid white 2px';                        // [prop 20150316°0915]
Daf.Lngo.sFlagsStyleMarginHorizontal = '3px';                           // [prop 20150316°0916]
Daf.Lngo.sFlagsStyleMarginVertical = '1px';                             // [prop 20150316°0917]
Daf.Lngo.sFlagsStyleHeight = '18px';                                    // [prop 20150316°0918]
Daf.Lngo.sFlagsStyleWidth = '27px';                                     // [prop 20150316°0921]

// See ref 20120902°1213 'w3 → tips → font families'
Daf.Lngo.sFontFam1 = 'arial,helvetica,sans-serif';                      // [prop 20150316°0924]
Daf.Lngo.sFontFam2 = 'Times New Roman, serif';                          // not used so far [prop 20150316°0925]

Daf.Lngo.sIni_LangDefault_Fallback = 'eng';                             // [prop 20150316°0926]
Daf.Lngo.sIni_LangDefault_Sitmapdaf = 'default_lang';                   // [prop 20210416°1523] used in sitmapdaf.js for feature 20210416°1524 'Determine language without cookie'

Daf.Lngo.sSaveStaticCakecrumbs = '';                                    // [prop 20190416°0455]

// eof
