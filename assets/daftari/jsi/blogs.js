/*!
 *  This module shall provide the blog-fade-in feature
 *
 *  @id          : 20140625°1711 daftari/jsi/blogs.js
 *  @license     : GNU AGPL v3
 *  @copyright   : © 2014 - 2023 Norbert C. Maier
 *  @authors     : Norbert C. Maier
 *  @status      : embryonic
 *  @requires    : jquery.js
 *  @see         : ref 20140627°1111 'stackoverflow → load div from one page and show in other'
 *  @see         : ref 20140627°1213 'jQuery API → jQuery.load'
 *  @see         : todo 20180509°0351 'merge blog-fade-in and fade-in-file'
 *  @see         : issue 20190311°1531 'blog page image read error'
 *  @callers     :
 */

// Options for JSHint [seq 20190423°0421]
/* jshint laxbreak : true, laxcomma : true */
/* globals Cvgr, Daf, jQuery */

/**
 *  Announce the Daftari root namespace
 *
 *  @id 20170923°0421`11
 *  @const — Namespace
 */
Daf = window.Daf || {};

/**
 *  Announce CanvasGear namespace
 *
 *  @id 20180618°0621`21
 *  @const — Namespace
 */
Cvgr = window.Cvgr || {};

/**
 *  This function fades-in one blog article into the target page
 *
 *  @id 20140625°1721
 *  @status Works proof-of-concept
 *  chain : howto 20181115°0551 'read from XML file'
 *  @required jQuery (for parseXML and load)
 *  @callers Only • dafdispatch.js via pullScriptBehind
 *  @param {string} sTarget — The ID of the item to fade-in, e.g. "./blogitems/20120915o1621.html #id20120915o1632".
 *  @return {undefined}
 */
function blogfadein(sTarget)
{
    'use strict';

    //debugger;

    // Select a default blog [seq 20180509°0321]
    // See todo 20180509°0338 'make blog feature configurable'
    // note : In sTarget, behind the blank comes a jQuery selector to specify the element to be loaded
    if (sTarget === '')
    {
        // [seq 20181115°0533]
        // summary : Read default blog from target page script snippet
        // note : This was the solution for issue 20180509°0338 'make blog feature'
        //     configurable (task 1)' — from the target page, a variable is set.
        // proposal : Store the blog article selection in a cookie, so if the user
        //     leaves the page or switches language, she will get the same article again.
        // prototypes :
        //    • page 20180619°0411 daftari/docs/blogs.html
        if (typeof Daf.Blog !== 'undefined') {
            sTarget = Daf.Blog.s_Default_Entry;
        }
    }

    // () Load blog block into div [line 20140829°1333]
    // note : class="Blog12FadeIn" is target div e.g. in ~~daftari/docsmanual/index.html
    // See issue 20190404°0721 'why is jquery loading icon unnecessarily'
    jQuery('#id20140625o1651_BlogFadeIn').load(sTarget, loadReady);
}

/**
 *  This function is called each time after one blog article was loaded
 *
 *  @id 20140625°1751
 *  @status provisory — The link manipulation must be generalized somehow
 *  @see ref 20140625°1811 'quirksmode → js strings'
 *  @see ref 20140625°1812 'stackoverflow → replace occurrences in string'
 *  @callers : blogfadein()
 *  @return {undefined}
 */
function loadReady()
{
    'use strict';

    // () Adjust links inside fade-in block [seq 20180509°0331]
    // note : Since the blog pages are in a subdir, not in the root dir, the
    //     links in it have to be manipulated. This is quick'n'dirty, a solid
    //     algorithm is still wanted. This lines may not suffice for long.
    // note 20160619°0111 : Compare solution for similar problem in dafdispatch.js::Breadcrumbs.
    // See todo 20180509°0341 'adjust relative links in faded-in block'
    var sDiv = '';
    sDiv = document.getElementById('id20140625o1651_BlogFadeIn').innerHTML;  // index.html target div
    sDiv = sDiv.split("./../img/").join("./img/");                      // No more subdir
    sDiv = sDiv.split("./../img1/").join("./img1/");                    // No more subdir
    sDiv = sDiv.split("./../img2/").join("./img2/");                    // No more subdir
    sDiv = sDiv.split("./../img3/").join("./img3/");                    // No more subdir
    sDiv = sDiv.split("./../imgs26/").join("./imgs26/");                // Daftari manual [line 20150815°1421 new]
    sDiv = sDiv.split("./../stack/").join("./stack/");                  // No more subdir
    sDiv = sDiv.split("./../stack1/").join("./stack1/");                // No more subdir
    sDiv = sDiv.split("./../stack2/").join("./stack2/");                // [line 20150815°1422 new]
    document.all.id20140625o1651_BlogFadeIn.innerHTML = sDiv;           // Replace without subdirs

    // () Refresh language split [seq 20150815°0621]
    // note : It were nice to process only the affected div, not the complete page.
    Daf.Lngo.onloadLangSplit(true);                                     // true = force

    // Workaround 20150815°1431 re-ignit CanvasGear
    // The faded-in blog does not show CanvasGear, so just try to ignit it again.
    if (typeof Cvgr !== 'undefined') {
        Cvgr.startCanvasGear();
    }
}
