/*!
 *  id : 20170306°0511 (formerly 20111222°1921) daftari/jsi/dafsitmap.js
 *  summary : This module provides the CakeCrumbs
 *  license : GNU AGPL v3
 *  copyright : © 2011 - 2023 Norbert C. Maier
 *  authors : ncm
 *  status : Developing
 *  callers : • onload
 */

// Options for JSHint [seq 20190423°0421`07]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf, DAF_PAGENODES, Trekta */

/**
 *  Announce namespace
 *
 *  @id 20190106°0311`15
 *  @c_o_n_s_t — Namespace
 */
Trekta = window.Trekta || {};

/**
 *  This namespace constitutes the Daftari root namespace
 *
 *  @id 20170923°0421`06
 *  @c_o_n_s_t — Namespace
 */
Daf = Daf || {};

/**
 *  This namespace shall provide the CakeCrumb and TreeView features.
 *
 *  @id 20150317°0911
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Sitmp = Daf.Sitmp || {};

/**
 *  This constructor function (or class) defines one node from sitemap
 *
 *  @id 20160417°1021
 *  @callers • 20160417°1131 workoff_Cake_1_buildTree
 *  @param {string} sCrumbText — Crumb text
 *  @param {string} sTargetUrl — Crumb URL
 *  @param {Object|Node|null} nParentPage (Daf.Sitmp.CakeNode4) — Parent page node or null
 *  @constructor
 *  @return {undefined} —
 */
Daf.Sitmp.CakeNode4 = function(sCrumbText, sTargetUrl, nParentPage)
{
    'use strict';

    this.ChildNodes = [];                                               // Will be filled later with type Daf.Sitmp.CakeNode4
    this.CrumbText = sCrumbText;                                        // HTLM fragment
    this.IsSelfPage = false;                                            // Flag telling whether the given node is the current page
    this.IsSelfParent = false;                                          //
    this.ParentPage = nParentPage;                                      // Type Daf.Sitmp.CakeNode4
    this.TargetUrl1AsInSitmap = sTargetUrl;                             // As is read from sitmapdef.js (field 20160620°0431)

    this.PageNumber = 0;                                                // [prop 20160618°0131] To be assigned at runtime
    this.PageNeighbourLeft = "";                                        // [prop 20160618°0132] To be assigned at runtime
    this.PageNeighbourRight = "";                                       // [prop 20160618°0133] To be assigned at runtime
    this.RelPath_FromCrumbPage_ToSitmapFolder = "";                     // [prop 20180310°1252] This finally solves issue 20180307°0201 'wrong navigation arrows'
};

/**
 *  This class provides one page node.
 *
 *  @id 20160416°0741
 *  @status developing
 *  @usage When building breadcrumbs
 *  @note This properties represent the tokens used in 'sitmapdaf.xml'
 *  @constructor —
 *  @return {undefined} —
 */
Daf.Sitmp.PageNode = function()
{
    'use strict';
    this.pgCrumb = "";                                                  // 'crumb' e.g. 'Intro'
    this.pgFileName = "";                                               // 'filenam' e.g. './Thr_0100.html'
    this.pgParent = "";                                                 // 'parent' e.g. './index.html'
    this.pgText = "";                                                   // E.g. 'Introduction'
};                                                                      // End class PageNode

/**
 *  This function retrieves the relative path from current page to site root folder
 *
 *  @id 20180310°1241
 *  @status
 *  @note This algorithm solved issue 20180307°0201 'wrong next-page-arrow'
 *  @important File sitmapdaf.js has the condition, that all entries have to
 *      be prefixed with the identical path down to base folder. To know this
 *      path, it suffices to read the very first node and skip the others. The
 *      sitmapdaf.js may contain absolute paths, relative pathes must begin with '.'
 *  @callers Only • func 20160417°1131 Daf.Sitmp.workoff_Cake_1_buildTree
 *  @param {string} sUrlAsInSitmapdaf The URL as read from sitmapdaf.js, e.g. "./../index.html"
 *  @return {string|undefined} — The wanted relative path from current page to site root
 */
Daf.Sitmp.getRelPathFromPageToSiteRoot = function(sUrlAsInSitmapdaf)
{
    'use strict';
    var s7 = '';

    // () If given path is not a relative one, leave URL as is [seq 20170924°0421]
    if (sUrlAsInSitmapdaf.substr(0,1) !== '.')
    {
        return s7;
    }

    // () Count steps down from sitmapdaf to siteroot [seq 20170924°0422]
    //  This is wanted to build the absolute path to siteroot
    var a = sUrlAsInSitmapdaf.split('/');
    var iStepsDown = 0;

    // Count steps down [seq 20190407°0322] (not IE8)
    a.forEach( function(s7) {
        if (s7 === '..') {
            iStepsDown++;
        }
    });

    Daf.Sitmp.iDownSteps = iStepsDown;                                  // [line 20190129°0913]
    // Now the number of steps down to root is known

    // () Build absolute path to site root folder [seq 20170924°0423]
    s7 = Daf.Dspat.Config.SitemapFolderAbsolute;
    a = s7.split('/');
    for (var i2 = 0 ; i2 < iStepsDown; i2++) {
        a.pop();                                                        // Remove one more level
    }
    s7 = a.join('/');
    Daf.Sitmp.Config.sAbsolutePath_ToSiteBase = s7;

    // () Build relative path from siteroot to sitmapdaf [seq 20170924°0424]
    var o = Daf.Utlis.get_ThreeFromTwoByDir ( Daf.Dspat.Config.SitemapFolderAbsolute
                                             , Daf.Sitmp.Config.sAbsolutePath_ToSiteBase
                                              );
    var sRelPathFromSiterootToSitemapdaf = o.sRestOne;                  // E.g 'help'

    // () Find relative path from current page to site root [seq 20170924°0425]
    //  Comparing the absolute path to current page with the siteroot folder
    //   yields the number of steps down from current to siteroot
    var o2 = Daf.Utlis.get_ThreeFromTwoByDir ( Daf.Dspat.Config.PathToThisPageFile
                                              , Daf.Sitmp.Config.sAbsolutePath_ToSiteBase
                                               );

    // () Determine steps down from current page to siteroot [seq 20170924°0426]
    s7 = o2.sRestOne;                                                   // E.g. "docs/sub1test/vault.html"
    var a2 = s7.split('/');
    iStepsDown = a2.length - 1;

    // () Build relative path from current page to site root folder [seq 20170924°0427]
    s7 = '.';
    for (var i3 = 0 ; i3 < iStepsDown; i3++)
    {
        s7 += '/..';
    }
    Daf.Sitmp.Config.sRelPath_FromPage_ToSiteBase = s7;                 // E.g. "./../.."

    // () Build relative path from current page to sitmapdaf folder [seq 20170924°0428]
    Daf.Sitmp.Config.sRelPath_FromPage_ToSitemap = Daf.Sitmp.Config.sRelPath_FromPage_ToSiteBase
                                                  + '/' + sRelPathFromSiterootToSitemapdaf
                                                   ;

    // Calculate this pages path as given in the sitemap [line 20180315°1021]
    // This is later used to detect the self page in the crumbs tree.
    Daf.Dspat.Config.SelfPageUrlRelative = Daf.Sitmp.Config.sRelPath_FromPage_ToSiteBase
                                          + "/" + Trekta.Utils.getFilenamePlain()
                                           ;

    // () Debug output
    if ( Daf.Const.b_Toggle_TRUE ) {
        s7 = "[Dbg 20180310°1251] Relative path from current page to sitmapdaf"
          + " = '" + Daf.Sitmp.Config.sRelPath_FromPage_ToSitemap + "'"
            ;
        Trekta.Utils.outDbgMsg(s7);
    }

    // () Set ready flag
    Daf.Sitmp.iFlag_RelPathToSitmapKnown = 1;

    return;
};

/**
 *  This function works off the Cakecrumbs feature
 *
 *   The breadcrumbs are built in the following steps
 *    (1) Akquire page self filename
 *    (2) ..
 *    (3) Build string with HTML code
 *    (4) ..
 *
 *  @id func 20160417°1011
 *  @note For code processing the tree view array, see file 20160416°1121 tree.js
 *  @callers Only • func 20190413°0741 Daf.Dspat.process_cake2build
 *  @return {string} — The wanted cakecrumbs HTML block to be inserted
 */
Daf.Sitmp.workoff_Cake_0_go = function()
{
    'use strict';

    // (X) Acquire the HTML block
    // (X.1) Preface
    var i = 0;
    var s = '';

    // (X.1.1) Create root node
    var nRoot = null;                                                   // CakeCrumb4 [var 20160620°0131]

    // (X.1.2) Guarantee sitemapdaf [seq 20160417°1012]
    if ( typeof DAF_PAGENODES === 'undefined' ) {
        s += '<p>Sitemap not available.' + '</p>';
        return s;
    }

    // (X.2) Prepare input
    //  If not some root node was already created above (e.g. from sitmapdaf.xml),
    //  then use DAF_PAGENODES, which are linked from file 'sitmapdaf.js' as a JS
    //  object tree. After this has run, Daf.Sitmp.iDownSteps will be known.
    if (nRoot === null) {
        nRoot = Daf.Sitmp.workoff_Cake_1_buildTree(DAF_PAGENODES[0], null);
    }
    // Now inside nRoot you find a tree of Daf.Sitmp.CakeNode4 nodes

    // (X.3) Process the neuralgic turnaround arrows [seq 20190420°0613]
    //  Now after the complete tree was build, we have the both URLs available
    // (X.3.1) Supplement previous-arrow for first page [line 20190420°0614]
    nRoot.PageNeighbourLeft = Daf.Sitmp.Config.sRememberLastPageUrl;
    // (X.3.2) supplement next-arrow for last page [line 20190420°0615]
    Daf.Sitmp.Config.crumbPreviousPageNode.PageNeighbourRight = Daf.Sitmp.Config.sRememberFirstPageUrl;

    // Debug — Possibly accumulated message
    if ( Daf.Sitmp.sDbgMsgAccu !== '' ) {
        Trekta.Utils.outDbgMsg(Daf.Sitmp.sDbgMsgAccu);
    }

    //--------------------------------------------------
    // (X.4) Calculate the triangle folder [seq 20170924°0431]
    // Algo : From sitemap folder, go down the number of down-steps.
    // note : Seq 20170924°0431 and 20170924°0451 are paired.
    // note : Is this sequence obsolete since issue 20180307°0201 'wrong navigation arrows' seems solved?!

    // (X.4.1) Disassemble sitmap path
    var a = Daf.Dspat.Config.SitemapFolderAbsolute.split('/');          // [line 20190419°0124]

    // (X.4.2) Remove the mandatory trailing slash
    var sTest = a[a.length - 1];
    if (sTest === '') {
        a.pop();
    }

    // (X.4.3) Remove right folder after number of down steps (possibly zero)
    for (i = 0; i < Daf.Sitmp.iDownSteps; i++) {
        a.pop();
    }

    // (X.4.4) Reassemble fragments to the wanted triangle folder
    var sWebrootTriangleFolder = '';
    for (i = 0; i < a.length ; i++) {
        sWebrootTriangleFolder += a[i] + '/';
    }
    if (Daf.Const.b_Toggle_TRUE) {
        s = '[Debug 20170924°0441] sWebrootTriangleFolder = "' + sWebrootTriangleFolder + '"';
        Trekta.Utils.outDbgMsg(s);
    }
    // Now sWebrootTriangleFolder is determined — Really?
    //----------------------------------------------------

    // (A) Calculate self as to compare with self from sitmapdaf.js
    // (A.1) From two strings calculate three strings [seq 20170902°0551]
    // ref : note 20170923°0532 'debug self page finding 2'
    // See todo 20180315°1012 'purge algo Daf.Utlis.get_ThreeFromTwoByDir'
    var o = Daf.Utlis.get_ThreeFromTwoByDir ( Daf.Dspat.Config.PathToThisPageFile  // E.g. "http://localhost/daftari/docs/help.html"
                                             , sWebrootTriangleFolder   // E.g. "http://localhost/daftari/"
                                              );

    // (A.3) Calculate sitemap comparison path
    // () Akquire page self filename [seq 20170924°0451 algo] (compare seq 20150516°0641)
    // note : Seq 20170924°0431 and 20170924°0451 are paired
    var sRel = '.';
    var sSelfPageUrl = o.sRestOne;                                      // E.g. "docs/help.html"
    for (i = 0; i < Daf.Sitmp.iDownSteps; i++) {
        sSelfPageUrl = '../' + sSelfPageUrl;
        sRel += '/..';
    }
    sSelfPageUrl = './' + sSelfPageUrl;

    Daf.Dspat.Config.SelfPageUrlRelative = sSelfPageUrl;                // E.g. "./../manual/index.html"

    // Status notification
    //  Here pageNode.TargetUrl1AsInSitmap is not yet available, see below seq 20170902°0543.
    if ( Daf.Const.b_Toggle_TRUE )
    {
        var a51 = [[ ' • Daf.Dspat.Config.PathToThisPageFile' , 41 ] , [ '= ' + Daf.Dspat.Config.PathToThisPageFile , 9 ]];
        var a52 = [[ ' • Daf.Dspat.Config.SelfPageUrlRelative' , 41 ] , [ '= ' + Daf.Dspat.Config.SelfPageUrlRelative , 9 ]];
        var a53 = [[ ' ◦ sWebrootTriangleFolder' , 41 ] , [ '= ' + sWebrootTriangleFolder , 9 ]];
        var s51 = Daf.Utlis.tabAlign(a51, ' ');
        var s52 = Daf.Utlis.tabAlign(a52, ' ');
        var s53 = Daf.Utlis.tabAlign(a53, ' ');
        s = "[Status 20170902°0453] workoff_Cake_0_go"
           + "\n &nbsp;" + s51
           + "\n &nbsp;" + s52
           + "\n &nbsp;" + s53
            ;
        Trekta.Utils.outDbgMsg(s);
    }

    // (X.2.2) Traverse the tree to analyse it for self path
    Daf.Sitmp.workoff_Cake_2_traverse(nRoot);

    // (X.2.3) Traverse again to build the crumbs array
    //  Here the initial outside call of that recursive function.
    Daf.Sitmp.workoff_Cake_3_traverse_again(nRoot, 1, Daf.Sitmp.iFLAG_START);

    // (X.2.4) Traverse again for debug purposes
    // Switched off 20180315°1011 after issue was solved. Possibly delete permanently.
    if ( Trekta.Utils.bShow_Debug_Dialogs )
    {
        var sMsg432 = "[Debug 20180307°0212] list : • node — right neighbour";
        sMsg432 = Daf.Sitmp.workoff_Cake_3_1_traverse_debug(nRoot, sMsg432);
        Trekta.Utils.outDbgMsg(sMsg432);
    }

    // (X.3) Build wanted HTML fragment (call seq 20160417°1241)
    var sHtmlFrag = Daf.Sitmp.workoff_Cake_4_assemble();

    // (X.4) Ready
    return sHtmlFrag;
};

/**
 *  This recursive function builds the internal tree object of Daf.Sitmp.CakeNode4 nodes
 *
 *  @id 20160417°1131
 *  @ref 20160419°1311 article 'JavaScript Array: slice vs splice'
 *  @callers Only • func 20160417°1011 Daf.Sitmp.workoff_Cake_0_go • then self
 *  @param {Array} aTreeNode (string[]) — The original sitmapdaf.js treenode
 *  @param {Object|Node|null} nParent (Daf.Sitmp.CakeNode4) — The parent node or null
 *  @returns {Object|Node} (Daf.Sitmp.CakeNode4) — The newly created node. The initial
 *      caller gets the root node with the the rest of the tree inside.
 *  @return {Object} —
 */
Daf.Sitmp.workoff_Cake_1_buildTree = function(aTreeNode, nParent)
{
    'use strict';
    var s77 = "";

    // (D.1) Create this one current new node object [line 20160417°1133]
    var nThis = new Daf.Sitmp.CakeNode4
               ( aTreeNode[Daf.Sitmp.iTvNodeNdxCrumb]                   // Be nThis.CrumbText
                , aTreeNode[Daf.Sitmp.iTvNodeNdxUrl]                    // Be nThis.TargetUrl1AsInSitmap
                 , nParent                                              // Be nThis.ParentPage
                  );

    // () Determine relative path from current page to sitmapdaf folder [seq 20180310°1221]
    if (Daf.Sitmp.iFlag_RelPathToSitmapKnown < 1)
    {
        s77 = nThis.TargetUrl1AsInSitmap;
        Daf.Sitmp.getRelPathFromPageToSiteRoot(s77);
    }

    // () Workaround for possible double slashes [seq 20180509°0211]
    // This solves issue 20180307°0201 'wrong navigation arrow urls'
    // See todo 20180322°0132 'make workaround superfluous'
    // note 20180509°0212 : Test this sequence with a sitmapdaf.js
    //     with no folder depth e.g. • http://localhost
    s77 = Daf.Sitmp.Config.sRelPath_FromPage_ToSitemap;                 // E.g. "./../trunk/"
    if (s77.substring(s77.length - 1) !== '/') {                        // Guarantee trailing slash
        s77 += "/";
    }

    // () Repace any '/./' by '/' [seq 20190411°0633] just non-functional cosmetics
    s77 += nThis.TargetUrl1AsInSitmap;
    s77 = s77.replace(/\/\.\//, '/');
    nThis.RelPath_FromCrumbPage_ToSitmapFolder = s77;

    // (D.1.2) Debug notification [seq 20160417°1135]
    if ( Daf.Sitmp.sDbgMsgAccu === '' ) {
        Trekta.Utils.outDbgMsg("[Dbg 20170902°0542 ⇘] Start accumulating msg 20170902°0543 ...");
        Daf.Sitmp.sDbgMsgAccu = "[Dbg 20170902°0543 ⇖] Pathes from crumb targets to sitmap folder";
    }
    if ( Daf.Const.b_Toggle_TRUE ) {
        // Short but ugly [seq 20160417°1137]
        var a = nThis.TargetUrl1AsInSitmap.split('/');
        s77 = a[a.length - 1];
        s77 = s77.substr(0, s77.length - 5);
        Daf.Sitmp.sDbgMsgAccu += ', ' + s77;
    }
    else {
        // nice but long [line 20160417°1139]
        Daf.Sitmp.sDbgMsgAccu += '\n &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; — '
                               + nThis.RelPath_FromCrumbPage_ToSitmapFolder + "'"
                                ;
    }

    // (E) Process arrows box values
    // (E.1) Set page number [seq 20160618°0221]
    Daf.Sitmp.Config.iPageCounter += 1;
    nThis.PageNumber = Daf.Sitmp.Config.iPageCounter;

    // (E.2) Set arrow left URL for current node [line 20160417°1141]
    nThis.PageNeighbourLeft = Daf.Sitmp.Config.sRememberLastPageUrl;

    // (E.3) Remember arrow left URL for next round [line 20160417°1143]
    Daf.Sitmp.Config.sRememberLastPageUrl = nThis.RelPath_FromCrumbPage_ToSitmapFolder;

    // (E.4) Set target URL for arrow right [line 20160417°1145]
    if (Daf.Sitmp.Config.crumbPreviousPageNode)
    {
        // [line 20180307°0411]
        Daf.Sitmp.Config.crumbPreviousPageNode.PageNeighbourRight = nThis.RelPath_FromCrumbPage_ToSitmapFolder;
    }
    else {
        // Remember the right-arrow URL for later [line 20190420°0611]
        //  We are in the root node now, the memory variable
        //  Daf.Sitmp.Config.crumbPreviousPageNode.PageNeighbourRight is
        //  not assigned, because there was no previous node yet, so store
        //  the value until the tree is finished, and assign it afterward
        Daf.Sitmp.Config.sRememberFirstPageUrl = nThis.RelPath_FromCrumbPage_ToSitmapFolder;
    }
    Daf.Sitmp.Config.crumbPreviousPageNode = nThis;

    // (F) Process chapter tree [line 20160417°1147]
    // (F.1) add this new node to it's parent's childs list
    if (nParent) {
        nParent.ChildNodes.push(nThis);                                 // [marker 20210416°1633`56 GoCloCom] WARNING - [JSC_INEXISTENT_PROPERTY] Property ChildNodes never defined on Node
    }

    // (F.2) Extract the child nodes array [line 20160417°1149]
    var aChildNodes = aTreeNode.slice(Daf.Sitmp.iTvNodeNdxFirstChild, aTreeNode.length);  // 2

    // (F.3) Recursively iterate over child nodes [line 20160417°1151]
    if (aChildNodes.length > 0) {
        for (var i = 0; i < aChildNodes.length; i++) {
             Daf.Sitmp.workoff_Cake_1_buildTree(aChildNodes[i], nThis);
        }
    }

    // The initial caller gets the root node, which carries the complete tree
    return nThis;                                                       // One Daf.Sitmp.CakeNode4 node object
};

/**
 *  This function finds the self page in the page nodes tree.
 *
 *  @id 20160417°1031
 *  @callers Only • Daf.Sitmp.workoff_Cake_0_go • then recursively self
 *  @param {Object|Node|null} pageNode (Daf.Sitmp.CakeNode4) — On initial call root node, on recursive calls child nodes
 *  @return {undefined} —
 */
Daf.Sitmp.workoff_Cake_2_traverse = function(pageNode)
{
    'use strict';

    /*
    rule 20190419°0151 'sitmapdaf.js must always tell the page file name'
    text : The pathes in sitmapdaf.js must always tell the
       pagename, e.g. './index.html', not just the folder like '.' or './'.
       This is wanted so the page recognizes itself by a simple algorithm.
    location : 20160417°1031 Daf.Sitmp.workoff_Cake_2_traverse
    */

    // () Recognize self page [condi 20160624°0311]
    // Obey rule 20190419°0151 'sitmapdaf.js must always tell the page file name'
    if ( pageNode.TargetUrl1AsInSitmap === Daf.Dspat.Config.SelfPageUrlRelative )
    {
        // Set flag 'self page' [line 20160624°0313]
        pageNode.IsSelfPage = true;

        // Go recursively down and mark parent nodes as such [line 20160624°0315]
        Daf.Sitmp.workoff_Cake_21_traverse_down(pageNode);
    }

    // Loop recursive over children [line 20160417°1033]
    for (var i = 0; i < pageNode.ChildNodes.length; i++) {
        Daf.Sitmp.workoff_Cake_2_traverse(pageNode.ChildNodes[i]);
    }

    return;
};

/**
 *  This function recursively traverses down from the current
 *   page and marks all parent and grandparents pages of this node.
 *   This markers are used when building the cakecrumbs to format them.
 *
 *  @id 20160417°1041
 *  @callers Only • Daf.Sitmp.workoff_Cake_2_traverse • then recursively self
 *  @param pageNode {Daf.Sitmp.CakeNode4} The self page node in the
 *          initial call, the ancesters in the recursive calls
 *  @return {undefined} —
 */
Daf.Sitmp.workoff_Cake_21_traverse_down = function(pageNode)
{
    'use strict';

    // (1) If the frontpage is self page [condi 20160417°1043]
    //  The frontpage is recognized as it has no parent page.
    if (pageNode.ParentPage === null) {
        return;
    }

    // (2) Mark the parent page as parent of the current one [line 20160417°1045]
    //  This function was called from the node of the current page to show,
    //  so this page's parent is the parent of the current page (self).
    pageNode.ParentPage.IsSelfParent = true;

    // (3) Possibly recurse [condi 20160417°1047]
    if (pageNode.ParentPage.ParentPage !== null) {
        Daf.Sitmp.workoff_Cake_21_traverse_down(pageNode.ParentPage);
    }

    return;
};

/**
 *  This function recurses over the tree and builds the cakecrumbs array
 *
 *  @id 20160417°1051
 *  @status applicable
 *  @callers
 *  @param {Object|Node} pageNode (Daf.Sitmp.CakeNode4) — The node from which to traverse up
 *  @param {number} iLevel (Integer) — The line number of the CakeCrumb
 *  @param {number|null} iFlag (Integer) — Control flag telling the status of the operation
 *  @return {undefined} —
 */
Daf.Sitmp.workoff_Cake_3_traverse_again = function(pageNode, iLevel, iFlag)
{
    'use strict';

    // Last round [seq 20160417°1052]
    if (iFlag === Daf.Sitmp.iFLAG_SUBSELF) {
        iFlag = Daf.Sitmp.iFLAG_FINISH;
    }

    // Guarantee crumbs level [seq 20160417°1053]
    if (Daf.Sitmp.aCRUMBS.length < iLevel) {
        Daf.Sitmp.aCRUMBS.push([]);
    }

    // Enroll the given node as crumb [line 20160417°1054]
    Daf.Sitmp.aCRUMBS[iLevel - 1].push(pageNode);

    // Process node [seq 20160417°1055]
    //  There must be only one parent per level
    if (pageNode.IsSelfParent) {
        iLevel++;
    }
    else if (pageNode.IsSelfPage) {
        iLevel++;
        iFlag = Daf.Sitmp.iFLAG_SUBSELF;
    }
    else {
        iFlag = Daf.Sitmp.iFLAG_FINISH;
    }

    // If not reached end [seq 20160417°1056]
    if (iFlag !== Daf.Sitmp.iFLAG_FINISH) {
        // Loop over child nodes
        for (var i = 0; i < pageNode.ChildNodes.length; i++) {
            // Recurse
            Daf.Sitmp.workoff_Cake_3_traverse_again ( pageNode.ChildNodes[i]
                                                     , iLevel
                                                      , iFlag
                                                       );
        }
    }

    return;
};

/**
 *  This function recurses over the tree again for debugging a former
 *   issue. It is called only with debug flag switched on. Keep the code,
 *   it contains nice • tree traversing and • string formatting sequences.
 *
 *  @id 20180307°0211
 *  @note : Proposal — Try to convert Daftari base path to relative path
 *  @status nice listing but bulky
 *  @callers Only • func 20160417°1011 workoff_Cake_0_go • then self
 *  @param {Daf.Sitmp.CakeNode4} pageNode — The node from which to traverse up
 *  @param {string} sMsg432 — The message to be built
 *  @return {undefined} —
 */
Daf.Sitmp.workoff_Cake_3_1_traverse_debug = function(pageNode, sMsg432)
{
    'use strict';

    // Create padding [seq 20190413°0311] prototype seqence
    //  Compare stackoverflow.com/questions/202605/repeat-string-javascript
    var i54 = pageNode.TargetUrl1AsInSitmap.length;
    i54 = (i54 < 33) ? (33 - i54) : 0;
    var a = [];
    a.length = i54;
    var s55 = a.join('&nbsp;');

    // Message 20180307°0212
    sMsg432 += "\n&nbsp; • '" + pageNode.TargetUrl1AsInSitmap + "'"
             + s55 + " — '" + pageNode.PageNeighbourRight + "'"
              ;

    // Loop recursively over child nodes
    for (var i = 0; i < pageNode.ChildNodes.length; i++) {
        sMsg432 = Daf.Sitmp.workoff_Cake_3_1_traverse_debug ( pageNode.ChildNodes[i], sMsg432 );
    }

    return sMsg432;
};

/**
 *  This function builds the HTML fragment from the cakecrumbs array
 *
 *  @id 20160417°1241
 *  @callers Only • workoff_Cake_0_go
 *  @return {undefined} —
 */
Daf.Sitmp.workoff_Cake_4_assemble = function()
{
    'use strict';
    var s4 = '';

    // () Precondition [seq 20160417°1243]
    if (! Daf.Sitmp.aCRUMBS) {
        s4 = '<p>[Sorry no cakecrumbs.]</p>';
        return s4;
    }

    // () Current environment [seq 20160417°1244]
    var sCake = '';                                                     // The growing HTML fragment [var 20160620°0321]
    var crumbCurrent = null;                                            // Type CakeNode4 volatile [var 20160620°0332]
    var crumbThisPage = null;                                           // [var 20160620°0351]
    var sCrumbClass = '';

    // [line 20190416°0321]
    var aLangsOnPage = Daf.Lngo.getLangsOnPage();

    // (X.3.2) Process the crumbs array [seq 20160417°1245]
    //  Iterate over the hierarchy levels
    for (var i1 = 0; i1 < Daf.Sitmp.aCRUMBS.length; i1++) {

        // (X.3.2.1) Create new paragraph only on levels above zero [seq 20160417°1246]
        //  Cosmetically print levels one and two on into single paragraphe, because
        //  level one has only one node, typically named index.html.
        if (i1 !== 1) {

            // Original line for normal operation [line 20170903°0221]
            // See note 20121111°1521 'about data-wafailiskip'
            sCake += Daf.Const.sTb_1Cr + '   <p class="Daf_TopNav12Line" data-wafailiskip="yes">';  // "topNaviLine" [chg 20210415°0912]
        }

        // (X.3.2.2) Level prelude always except for root page [seq 20160417°1247]
        if (i1 > 0) {
            sCake += ' →';
        }

        // (X.3.2.3) iIterate over the crumbs of the current level [seq 20160417°1251]
        for (var i2 = 0; i2 < Daf.Sitmp.aCRUMBS[i1].length; i2++) {

            // Prologue [seq 20160417°1252]
            crumbCurrent = Daf.Sitmp.aCRUMBS[i1][i2];
            var sCrumbFrag = '';

            // (.) Assemble one crumb [seq 20190416°0341]
            var sCrumbText = '';
            var aTerms = Daf.Lngo.getLangsFromCrumb(crumbCurrent.CrumbText, aLangsOnPage);  // [line 20161109°2031]
            if (aTerms.length < 2) {
                // (.1) If there is only one language, we need no lingo spans [line 20190416°0343]
                //  More precisely, it is probably no Lingo at all, but a plaintext
                //  crumb, but Daf.Lngo.getLangsFromCrumb returns an array anyway.
                sCrumbText += aTerms[0];
            }
            else {
                // (.2) Build crumbtext with lingo spans [line 20190416°0345]
                for (var i7 = 0; i7 < aTerms.length; i7++) {
                    sCrumbText += '<span class="lang' + aLangsOnPage[i7] + '">' + aTerms[i7] + '</span>';
                }
            }

            // Determine crumb class [seq 20160417°1253]
            if (crumbCurrent.IsSelfPage) {
                crumbThisPage = crumbCurrent;
                sCrumbClass = 'Daf_TopNav12LinkSelf';                   // 'topNaviLinkSelf' [chg 20210415°0912]
            }
            else if (crumbCurrent.IsSelfParent) {
                sCrumbClass = 'Daf_TopNav12LinkParent';                 // 'topNaviLinkParent' [chg 20210415°0912]
            }
            else {
                sCrumbClass = 'Daf_TopNav12LinkActive';                 //  'topNaviLinkActive' [chg 20210415°0912]
            }

            // () Determine current link target [seq 20160621°0241]
            // There are two types of targets. (1) relative targets, they begin
            //  with './' and (2) absolute targets typically pointing outside.
            if (crumbCurrent.TargetUrl1AsInSitmap.substr(0, 2) === './') {
                // [seq 20160417°1254]
                s4 = crumbCurrent.RelPath_FromCrumbPage_ToSitmapFolder;  // [line 20160621°0242]
                sCrumbFrag = Daf.Const.sTb_1Cr + '               <a href="' + s4
                            + '" class="' + sCrumbClass
                             + '">' + sCrumbText + '</a>'
                              ;
            }
            else {
                // [seq 20160417°1255]
                s4 = crumbCurrent.TargetUrl1AsInSitmap;
                sCrumbFrag = Daf.Const.sTb_1Cr + '               <a href="' + s4
                            + '" class="' + sCrumbClass
                             + ' extern">' + sCrumbText + '</a>'
                              ;
            }
            sCrumbFrag = ' ' + sCrumbFrag;

            // Build cake
            sCake += ' ' + sCrumbFrag;
        }

        // (X.3.2.4) Cosmetically print the first two levels in one line [seq 20160417°1256]
        if (i1 !== 0) {
            sCake += Daf.Const.sTb_1Cr + '    </p>';
        }
    }

    // Paranoia [seq 20160621°0151]
    s4 = "[20160621°0151 workoff_Cake_4_assemble]";
    if ( (typeof crumbThisPage === 'undefined') || (crumbThisPage === null) ) {
        var sDummytext = "Sitemap wrong";
        if ( Trekta.Utils.bShow_Debug_Dialogs ) {
            s4 += ' ' + sDummytext;
            Trekta.Utils.outDbgMsg(s4);
        }
        return sDummytext;
    }
    else {
        s4 += " Sitemap seems fine";
        Trekta.Utils.outDbgMsg(s4);
    }

    // (X.3.3) Build wanted return value [seq 20160617°0321 new style]
    // (X.3.3.x) Provide values [seq 20160620°0221]
    var sArrowTarget_PageParent = null;
    if (crumbThisPage.ParentPage) {
        sArrowTarget_PageParent = crumbThisPage.ParentPage.RelPath_FromCrumbPage_ToSitmapFolder;
    }

    // () Provide trivial values [seq 20160417°1257]
    var sArrowImg_Right = Trekta.Utils.s_DaftariBaseFolderRel + "/jsi/imgs23/20081114o1722.navi-arrow-right.png.gif";  // [var 20160620°0243]
    var sArrowImg_Up = Trekta.Utils.s_DaftariBaseFolderRel + "/jsi/imgs23/20081114o1727.navi-arrow-up-wide.gif";  // [var 20160620°0242]
    var sArrowImg_UpNone = Trekta.Utils.s_DaftariBaseFolderRel + "/jsi/imgs23/20081114o1724.navi-arrow-up-none.gif";  // [var 20160620°0244]
    var sArrowImg_Left = Trekta.Utils.s_DaftariBaseFolderRel + "/jsi/imgs23/20081114o1721.navi-arrow-left.png.gif";  // [var 20160620°0241]

    // (X.3.3.1) Build navigation arrows block [seq 20160618°0128]
    //  Remember closed issue 20190420°0331 'complains about onmouseenter in div'. Now
    //  the formerly hardcoded onmouseenter attribute is replaced by runtime attaching.
    var sHtml = '';
    sHtml += Daf.Const.sTb_1Cr + '  <div class="' + Daf.Sitmp.Config.sCAKE_ARROWS_DIV_CLASSNAME + '"'  // 'DafCakecrumbs_ArrowsDiv'
                                + ' style=\"float:right;\"'
                                 + '>'
                                  ;
    sHtml += Daf.Const.sTb_1Cr + "               Page " + crumbThisPage.PageNumber + " of " + Daf.Sitmp.Config.iPageCounter;  // "Page x of y"
    sHtml += Daf.Const.sTb_1Cr + "        <br>";
    sHtml += Daf.Const.sTb_1Cr + "               <a href=\"" + crumbThisPage.PageNeighbourLeft + "\""
                                + " id=\"autoflick_backward\">" // ident 20190420°0253 'flick_backward'
                                 + "<img src=\"" + sArrowImg_Left + "\" width=\"48\" height=\"24\" alt=\"Goto previous page\">"
                                  + "</a>"
                                   ;
    if (sArrowTarget_PageParent) {
        sHtml += Daf.Const.sTb_1Cr + "               <a href=\"" + sArrowTarget_PageParent + "\"><img src=\"" + sArrowImg_Up + "\" width=\"32\" height=\"24\" alt=\"Goto parent page\"></a>";
    }
    else {
        sHtml += Daf.Const.sTb_1Cr + "               <img src=\"" + sArrowImg_UpNone + "\" width=\"32\" height=\"24\" alt=\"Goto parent page\">";
    }
    sHtml += Daf.Const.sTb_1Cr + "               <a href=\"" + crumbThisPage.PageNeighbourRight + "\""
                                + " id=\"autoflick_forward\">" // ident 20190420°0252 'flick_forward'
                                 + "<img src=\"" + sArrowImg_Right + "\" width=\"48\" height=\"24\" alt=\"Goto next page\">"
                                  + "</a>"
                                   ;

    sHtml += '<p style="margin:0 0 0 0; background-color:Yellow; font-size:small; display:none;" id="autoflick_status">autoflick = 12345</p>';

    // [condi 20160620°0252]
    if (Daf.Sitmp.Config.bFlag_AppendDaftariMainMenuIcon) {
        // () Anchor for Daftari Main Menu notebook icon [seq 20160620°0251]
        // Somehow make the anchor for the Daftari Main Menu icon.
        //  The image serves as dummy until a functioning implementation.
        sHtml += Daf.Const.sTb_1Cr + "        <br>"
               + Daf.Const.sTb_1Cr + "               <img src=\"" + Trekta.Utils.s_DaftariBaseFolderRel + "/"
                + Daf.Const.sFILE_DAFTARI_ICON_NOTEBOOK_MAIN // "jsi/imgs23/20100814o021302.logo-edit-small-v015.png"
                 + "\" width=\"16\" height=\"16\" align=\"right\" style=\"margin-right:0.7em;\" alt=\"Daftari Main Menu notebook icon\">"
                  ;
    }

    // (X.3.3.2) Assemble block [seq 20160417°1258]
    sHtml += Daf.Const.sTb_1Cr + "  </div>";
    sHtml += Daf.Const.sTb_1Cr + '  ' + Daf.Sitmp.Config.sCAKECRUMBS_DIV_TAG + sCake + Daf.Const.sTb_1Cr + '  </div>';

    return sHtml;
};

// Helper variables for function 20160417°1051 workoff_Cake_3_traverse_again
Daf.Sitmp.aCRUMBS = [];                                                 // [var 20160417°1111]
Daf.Sitmp.iFLAG_START = 0;                                              // [var 20160417°1121]
Daf.Sitmp.iFLAG_SUBSELF = 1;                                            // [var 20160417°1122]

/**
 *  This variable stores the number of steps to go down to reach the triangle
 *   folder. It is wanted to calculate the triangle folder from the sitmap folder.
 *
 *  @todo Eliminate this variable [todo 20170924°0419]
 *  @id 20170924°0418
 *  @type {number} —
 */
Daf.Sitmp.iDownSteps = 0;

/**
 *  This flag tells whether it is the last round with cakecrumbs or not.
 *
 *  @type {number} —
 */
Daf.Sitmp.iFLAG_FINISH = 2;                                             // [var 20160417°1123]

// Provisory flag [var 20180310°1233]
Daf.Sitmp.iFlag_RelPathToSitmapKnown = 0;

// Indices for the nodes array from 'sitmapdaf.xml'
Daf.Sitmp.iTvNodeNdxCrumb = 0;                                          // [var 20160417°0941]
Daf.Sitmp.iTvNodeNdxFirstChild = 2;                                     // [var 20160417°0943] adjust this, if below elements are added
Daf.Sitmp.iTvNodeNdxUrl = 1;                                            // [var 20160417°0942]

Daf.Sitmp.sDbgMsgAccu = '';                                             // [var 20190413°0321]

/**
 *  This namespace Daf.Sitmp.Config holds constants
 *   and memory for the Daftari JS modules.
 *
 *  @id 20160618°0141
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Sitmp.Config = {};

/**
 *  This tells, whether in the cakecrumb's arrow box, append the notebook icon or not
 *
 *  @id flag 20160620°0251
 *  @type {boolean} — Settings value
 */
Daf.Sitmp.Config.bFlag_AppendDaftariMainMenuIcon = false;

/**
 *  This tells the current page number while building the tree
 *
 *  @id var 20160618°0211
 *  @type {number} — Intermediate memory
 */
Daf.Sitmp.Config.iPageCounter = 0;

/**
 *  This variable tells the absolute path to site root folder
 *
 *  @id 20180310°1233
 *  @type {string} —
 */
Daf.Sitmp.Config.sAbsolutePath_ToSiteBase = '';

/**
 *  This constant ..
 *
 *  @id const 20150517°0312
 *  @type {string} —
 */
Daf.Sitmp.Config.sCAKECRUMBS_DIV_TAG = '<div class="id20160618o0231_DafSitmapCakecrums_CrumbsDiv">';

/**
 *  This constant ..
 *
 *  @id const 20150517°0312
 *  @type {string} —
 */
Daf.Sitmp.Config.sCAKE_ARROWS_DIV_CLASSNAME = 'DafCakecrumbs_ArrowsDiv';

/**
 *  This variable tells the relative path from current page to site root
 *   folder. It is calculated from the pathes given in sitmapdaf.js
 *
 *  @id 20180310°1231
 *  @callers •
 *  @type {string} —
 */
Daf.Sitmp.Config.sRelPath_FromPage_ToSiteBase = '';

/**
 *  This variable tells the relative path from current page to the sitemap
 *   folder. It is calculated after having read the sitmapdaf.js nodes.
 *   This path can be prepended the immediate URLs from sitmapdaf.js
 *
 *  @id 20180310°1232
 *  @callers •
 *  @type {string} —
 */
Daf.Sitmp.Config.sRelPath_FromPage_ToSitemap = '';

/**
 *  This remembers the first page URL while building the tree
 *
 *  @id var 20190420°0612
 *  @type {string} — Intermediate memory
 */
Daf.Sitmp.Config.sRememberFirstPageUrl = '';

/**
 *  This remembers the last page while building the tree
 *
 *  @id var 20160620°0141
 *  @type {string} — Intermediate memory
 */
Daf.Sitmp.Config.sRememberLastPageUrl = '';

/**
 *  This volatile memory provides a reference to fill the left navigation arrow URL
 *
 *  @id var 20160620°0411
 *  @type {Object|null} (CakeNode4) — Intermediate memory
 */
Daf.Sitmp.Config.crumbPreviousPageNode = null;                          // [marker 20210416°1633`57 GoCloCom]

// eof
