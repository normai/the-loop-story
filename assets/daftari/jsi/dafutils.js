﻿/*!
 *  Daftari — Simplistic static multilingual webpage edit helper
 *
 *  File       : 20160427°0411 daftari/jsi/dafutils.js (or daftari.min.js if compiled)
 *  Summary    : This module provides various general basic utility functions
 *  Version    : v0.7.6.k
 *  License    : AGPL v3 (GNU Affero General Public License v3)
 *  Copyright  : © 2011 - 2023 Norbert C. Maier
 */
/*
 *  note       : In the Minification, dafutils.js is on top in the combined
 *                file, so the global version header has to be duplicated here.
 *
 *  encoding   : UTF-8-with-BOM
 *  status     : Applicable
 *  todo       : Successively take over general functions from other modules. [todo 20160612°0331]
 *  note       : See todo 20160622°0213 'supplement use-strict directive'
 *  callers    : Many
 *  note       : Goto ✂
 */

// Options for JSHint [seq 20190423°0421`12]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf, Trekta */

/**
 *  Define namespace satisfies the NetBeans syntax checker
 *
 *  @id 20190428°0751
 *  @type {type}
 */
var Mpd = Mpd || {};

/**
 *  Announce namespace
 *
 *  @id 20190106°0311`16
 *  @see issue 20210416°1632 'referenced before declaration'
 *  @c_o_n_s_t — Namespace
 */
var Trekta = Trekta || {};                                              // [marker 20210416°1633`01]

/**
 *  This namespace constitutes the Daftari root namespace
 *
 *  @id 20170923°0421`08
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
var Daf = Daf || {};                                                    // [marker 20210416°1633`02]

/**
 *  This namespace stores some Daftari specific utilities, possibly used early
 *
 *  @id 20170924°0621
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Utlis = Daf.Utlis || {};

/**
 *  This function converts newline chars to HTML break tags
 *
 *  @id 20120830°1521
 *  @note Compare func 20201203°1445 sConfine(), they are of similar categoty
 *  @callers • dialog.html::initiate()
 *  @param {string} sMsg — The HTML fragment to be supplemented with linebreak tags
 *  @return {string} — The input string but filtered for linebreaks
 */
Daf.Utlis.convertLinebreaksToHtml = function(sMsg)
{
    'use strict';

    /*
    todo 20120830°1522 'prefere regex over split´n´loop"
    task : A regex were more elegant than array/split/loop.
            Find out the wanted syntax. Below line does not work:
           - s = s.replace(/\n/, '<br />');
    status : open
    note :
    */

    // Compare dialog.html line 20190424°0727
    var a = sMsg.split('\n');
    var sRet = '';
    for (var i = 0; i < a.length; i++) {
        if (i > 0) {
            sRet += '\n    <br />';
        }
        sRet += a[i];
    }
    return sRet;
};

/**
 *  This function converts pointed brackets to the HTML entities
 *
 *  @id 20121111°1741
 *  @ref
 *  @callers EditFinishTransmit()
 *  @note Remember todo 20121111°1742 'use general encoding algorithm'
 *  @param {string} s — The string to be processed ...
 *  @return {undefined} —
 */
Daf.Utlis.convertPointedBracketsToHtml = function(s) {
    'use strict';
    s = s.replace(/>/g,'&gt;');
    s = s.replace(/</g,'&lt;');
    return s;
};

/*
 *  This function formats a number by a given number of decimal ciphers
 *
 *  @id : 20141118°0711
 *  @ref de.selfhtml.org/javascript/objekte/number.htm [ref 20140926°1443]
 *  @ref www.mredkj.com/javascript/numberFormat.html [ref 20140926°1442
 *  @ref stackoverflow.com/questions/5731193/how-to-format-numbers-using-javascript [ref 20140926°1441
 *  @note See JavaScript string method toFixed() and toPrecision()
 *  @callers None so far
 *  @param {number} iDecimals — The number of decimal digits wanted
 *  @return {undefined} —
 */
Daf.Utlis.formatNumber_NOTYETUSED = function(iDecimals)
{
    'use strict';

    iDecimals = iDecimals.toFixed(2) + '';
    var x = iDecimals.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
};

/**
 *  This function retrieves the index of an array element.
 *  This replaces getIndexOf() which is not available in all browsers.
 *  This replacement seems superfluous since old browsers are no more supported.
 *
 *  @id 20120902°1222
 *  @see issue 20120902°1221 'array.indexof browsercompatibility'
 *  @param {Array} aHaystack — The array to be searched
 *  @param {string} sNeedle — The string bo search for
 *  @return {undefined} —
 */
Daf.Utlis.getArrayIndexOf = function(aHaystack, sNeedle)
{
    'use strict';
    var iIndex = -1;
    for (var i = 0; i < aHaystack.length; i++) {
        if (aHaystack[i] === sNeedle) {
            iIndex = i;
            break;
        }
    }
    return iIndex;
};

/**
 *  This function returns the first element of the given class.
 *   (?) The given target node may typically be 'window.document'.
 *
 *  @id 20160617°0241
 *  @callers :
 *  @param {string} sClassName — The name of the wanted class, mandatory.
 *  @param {Element|null} eTargetNode — Optional element where to
 *          search for class. If no element is given, window.document is used.
 *  @return {Object|Node|null} —
 */
Daf.Utlis.getElamentByClassName = function(sClassName, eTargetNode)
{
    'use strict';
    var eWanted = null;

    // () If eTargetNode is null, assign it window.document [seq 20160620°0421]
    //  This happens, if the function is called with only the first param,
    //   not the second. This is default behaviour.
    if (! eTargetNode) {
        eTargetNode = document.querySelector('body');
    }

    // Pipeline to underlying function
    var es = Daf.Utlis.getElamentsByClassName(sClassName, eTargetNode);

    // Extract the wanted first class element
    if (es.length > 0) {
        eWanted = es[0];
    }

    // The first found element with the given class name
    return eWanted;
};

/**
 *  This function delivers an array of elements by class name
 *
 *  This function may be discarded after chg 20180619°0115 'discontinue IE8 support'
 *
 *  @id 20120902°1733
 *  @callers execPage10ViewBrowserInfo(),
 *  @note Remember chg 20180619°0115 'discontinue IE8 support'
 *  @note Remember issue 20140704°1051 'getElementsByClassName not supported by IE8'
 *  @param {string } sClassname — The class name to search for
 *  @param {Element} eNode — The node where to start the search
 *  @return {Array} — The found elements
 */
Daf.Utlis.getElamentsByClassName = function (sClassname, eNode) {

    'use strict';

    // (x) Browser feature detect, use native implementation if available
    if (eNode.getElementsByClassName) {

        // (x.1) Get the elements directly [seq 20120902°1734]
        // (x.1.1) Use native browser implementation
        var eles = eNode.getElementsByClassName(sClassname);

        // Ready
        return eles;
    }
    else
    {
        // (x.2) Workaround for IE8 and the like [seq 20120902°1735]
        // todo 20190209°0834 : Eliminate this sequence, there are only
        //    IE8 and Firefox 4 without getElementsByClassName().
        return (function (searchClass, eNode) {
            if ( eNode === null ) {
                eNode = document;
            }
            var classElements = [],
                    els = eNode.getElementsByTagName('*'),
                    elsLen = els.length,
                    pattern = new RegExp('(^|\\s)' + searchClass + "(\\s|$)"), i, j;
             for (i = 0, j = 0; i < elsLen; i++) {
                if ( pattern.test(els[i].className) ) {
                    classElements[j] = els[i];
                    j++;
                }
            }
            return classElements;
        })(sClassname, eNode);
    }
};

/**
 *  This function delivers the common left part of two paths plus the two rests
 *
 *  @id 20180307°0631 Daf.Utlis.get_ThreeFromTwoByDir
 *  @see todo 20180315°1012 'purge algo Daf.Utlis.get_ThreeFromTwoByDir'
 *  @note Compare func 20170925°0411 JsiEdit::calculateTriangle
 *  @note Remember issue 20170915°0201 'infinit loop in getStrings_ThreeFromTwo'
 *  @note Remember issue 20180307°0621 'algo must be dir by dir'
 *  @callers : • Func 20160417°1011 dafdispatch.js::workoff_Cake_1_buildTree,
 *              and • func 20160417°1011 workoff_Cake_0_go
 *  @param {string} sProbeOne — The page path as appears in the browser "http://localhost/daftaridev/index.html", possibly supplemented with 'index.html'
 *  @param {string} sProbeTwo — The path to the folder where sitmapdaf.js resides, e.g. "http://localhost/daftaridev/trunk/"
 *  @return {Object} — Object containing the wanted three strings
 */
Daf.Utlis.get_ThreeFromTwoByDir = function(sProbeOne, sProbeTwo)
{
    'use strict';

    // Remove possible trailing slashes
    sProbeOne = Daf.Utlis.trimmRChar(sProbeOne, '/');
    sProbeTwo = Daf.Utlis.trimmRChar(sProbeTwo, '/');

    // Convert input strings to arrays
    var aProbeOne = sProbeOne.split('/');
    var aProbeTwo = sProbeTwo.split('/');

    // Prepare wanted result arrays
    var sBasePart = '';
    var aTailOne = [];
    var aTailTwo = [];

    // Prepare intermediate values
    var sOne = '';
    var sTwo = '';

    // Determine the smaller element count
    var iLenProbeSmall = (aProbeOne.length < aProbeTwo.length)
                        ? aProbeOne.length
                         : aProbeTwo.length
                          ;

    // Do the work
    var iNdx = 0;
    while (true)
    {
        sOne = aProbeOne.slice(0, iNdx + 1).join('/');
        sTwo = aProbeTwo.slice(0, iNdx + 1).join('/');

        if (iNdx >= iLenProbeSmall)
        {
            aTailOne = aProbeOne.slice(iNdx);
            aTailTwo = aProbeTwo.slice(iNdx);
            break;
        }
        if (sOne !== sTwo)                                              // Check JavaScript string comparison, it is not that trivial
        {
            aTailOne = aProbeOne.slice(iNdx, aProbeOne.length);
            aTailTwo = aProbeTwo.slice(iNdx, aProbeTwo.length);
            break;
        }
        sBasePart = sOne;
        iNdx++;
    }

    // Convert result arrays back to strings
    var sTailOne = aTailOne.join('/');
    var sTailTwo = aTailTwo.join('/');

    // Create return object
    var oPathfork = {
        sCommon : sBasePart
        , sRestOne : sTailOne
        , sRestTwo : sTailTwo
    };

    return oPathfork;
};

/**
 *  This function shall provide a human readable timestamp. Currently it does
 *   so only in the format for HTML element IDs, * e.g. 'id20201218o173108'
 *
 *  @id 20180322°0231
 *  @ref stackoverflow.com/questions/5416920/timestamp-to-human-readable-format [ref 20180322°0241]
 *  @callers only func 20120905°1931 execElementInsert
 *  @return {string} —
 */
Daf.Utlis.getTimestampPrint = function()
{
    'use strict';

    const sNewElementIdPrefix = 'i';                                    // Added 20201218°1711 (>=IE11)

    var date = new Date();

    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;

    var s = sNewElementIdPrefix + date.getFullYear() + month + day + "o" + hour + min + sec;

    return s;
};

/**
 *  This function retrieves the browsers window size
 *
 *  @id 20150313°1831
 *  @callers : execPage10ViewBrowserInfo()
 *  @return {Object} —
 */
Daf.Utlis.getViewPortSize = function()
{
    'use strict';

    var Viewport = {
        iWidth : 0
        , iHeight : 0
    };

    // [seq 20150313°1832]
    // See ref 20150313°1821 'stackoverflow → get browser viewport dimensions'
    // note : This is the part which has to be finetuned for possible
    //     compatibility issues, notably for for mobile devices.
    if ( (typeof window.innerWidth !== 'undefined')
        && (typeof window.innerHeight !== 'undefined')
         ) {
        Viewport.iWidth = window.innerWidth;
        Viewport.iHeight = window.innerHeight;
    }
    else {
        Viewport.iWidth = document.documentElement.clientWidth;
        Viewport.iHeight = document.documentElement.clientHeight;
    }

    return Viewport;
};

/**
 *  This function shows a modal dialog to edit
 *
 *  @id 20120827°1713
 *  @callers Only • 20110812°1813 Daf.Mnu.Edt.ElementEditCatch
 *  @param {Array} aInput — Array of arrays of strings with the languages plus original text(s)
 *  @return {Array} — Array of arrays or strings with the lanugages plus the edited text(s)
 */
Daf.Utlis.modalDialogEdit_REPLACETHIS = function(aInput)
{
    'use strict';

    var oRet = aInput;                                                  // Provide return value for unchanged element

    // Feature detect [seq 20180303°1141] prototype 'test for existence of function'
    // See ref 20150210°1412 'stackoverflow → check if function exists (volatile)'
    // note : Compare seq 20180303°1141 20180322°0311
    // note : Bad news — Opera does pass the test into showModalDialog,
    //         but does not support it then, and leaves oRet being ~empty.
    if ( typeof showModalDialog !== 'undefined'
        && typeof showModalDialog === 'function'
         )
    {
        // [seq 20120827°1714]
        // note : See todo 20180106°1117 'replace func showModalDialog'
        var iWidth = 560;
        var iHeight = 560;
        oRet = window.showModalDialog
              ( Trekta.Utils.s_DaftariBaseFolderRel + '/' + Daf.Const.sFILE_PAGE_EDIT  // 'panels/edit.html'
               , aInput
                , 'center:yes'
                 + ';dialogHeight:' + iHeight + 'px'                    // Length/units
                  + ';dialogLeft:200'                                   // Integer, overrides center
                   + ';dialogTop:200'                                   // Integer, overrides center
                    + ';dialogWidth:' + iWidth + 'px'                   // Length/units
                     + ';edge:raised'                                   // 'raised' or 'sunken'
                      + ';help:no'
                       + ';resizable:yes'
                        + ';status:yes'
                         );

        // Empiric workaround for Opera [seq 20120827°1715]
        // note : I also tried it with types "Array" and "object"
        if (typeof oRet === "string")
        {
            var s = "Your browser seems to be Opera,"
                   + Daf.Const.sTb_1Cr + "the Edit dialog does not work here."
                    + Daf.Const.sTb_2Cr + "Please try e.g. IE."
                     + Daf.Const.sTb_2Cr + "(message 20120827°1715)"
                      + " Opera = " + (Trekta.Utils.bIs_Browser_Opera ? 'true' : 'false')
                       ;
            alert(s);
            Daf.Mnu.Meo.execPageSwitchEditmodeOff();                    // Reset page
        }
    }
    else
    {
        // Build alternative for showModalDialog [seq 20180303°1151]
        var s2 = 'Sorry, this modern browser does not show the wanted edit dialog.'
                + Daf.Const.sTb_1Cr + 'Try using IE.'
                 + Daf.Const.sTb_2Cr + '[message 20180303°1152]'
                  ;
        alert(s2);
        Daf.Mnu.Meo.execPageSwitchEditmodeOff();                        // Reset page
    }

    return oRet;
};

/**
 *  This function shall provide a central output facility.
 *
 *  @id 20120828°1911
 *  @status check — not applicable as is?
 *  todo : Make this function work as expected
 *  @callers • 20110811°1221 Daf.Mnu.Meo.execPageSwitchEditmodeOn
 *  @param {string} sMsg — The text to be output
 *  @return {undefined} —
 */
Daf.Utlis.outputError = function(sMsg)
{
  'use strict';

    // [seq 20120828°1912]
    if ( Trekta.Utils.bShow_Debug_Dialogs )                             // Probably falsse // 20200410°0842
    {
        // Bold try 20150516°0221 to fix mysterious script abortion

        // [seq 20120828°1913]
        var bodi = document.getElementsByTagName('body')[0];
        // 'p' versus 'P' — see issue 20120508°0921 'IE fails to set innerHTML of paragraph'
        var eDiv = document.createElement('div');
        var s = '<p style="background-color:orange;">'
               + "*** Aloha Error Output ***"
                + '<br />' + sMsg
                 + '</p>'
                  ;
        eDiv.innerHTML = s;

        // See finished issue 20120828°1851 'IE8 claims 'unknown error' [seq 20120828°1914]
        try {
            bodi.appendChild(eDiv);
        }
        catch (excpt) {
            if (Daf.Const.b_Toggle_TRUE) {
                var sDbg = "[Err 20150516°0211] message = '" + excpt.message + "'.";
                alert(sDbg);
            }
        }
    }
};

/**
 *  This function loads the page newly
 *
 *  @id 20190416°0111
 *  @callers • 20110512°1333 Daf.Lngo.setLanguage  • 20110510°2146 Daf.Utlis.Flick.flickerRun
 *      • 20110510°2147 Daf.Utlis.Flick.flickerStop  • 20110510°2146`37 gallery-js-autoflick.js::flickerRun
 *  @return {undefined} —
 */
Daf.Utlis.refreshPage = function()
{
    'use strict';

    // Refresh page [seq 20110510°2155]
    var sProto = window.location.protocol;
    var sHost = location.host;
    var sPath = window.location.pathname;
    var sUrl = sProto + '//' + sHost + sPath;
    location.href = sUrl;
};

/**
 *  This function returns the path to the given script .. using indexOf
 *
 *  @id 20160501°1611
 *  @status working
 *  @callers •
 *  @param sCanary {string} The script path tail to search for in the script tags
 *  @return {string} — The URL of the folder where the canary script resides
 */
Daf.Utlis.retrieveDafBaseFolderRel = function (sCanary)
{
    'use strict';
    var s23 = '';

    // Debug [seq 20190412°0251]
    s23 = '[Dbg 20190412°0251] retrieveDafBaseFolderRel (appears sometime double, sometimes not?)'
         + '\n &nbsp; • bUseMinified = "' + Trekta.Utils.bUseMinified + '"'
          + '\n &nbsp; • sCanary (1/2) = "' + sCanary + '"'
           ;

    // () Get the script tags list
    var scripts = document.getElementsByTagName('script');

    // () Find the canary script tag
    var script = null;
    for (var i1 = 0; i1 < scripts.length; i1++) {
        if (scripts[i1].src.indexOf(sCanary) > 0) {
            script = scripts[i1];
            break;
        }
    }

    // Paranoia
    if (script === null) {
        var s24 = '[Error 20160501°1631] Fatal.\nScript tag not found: "' + sCanary + '"';
        alert(s24);
        return '';
    }

    // (.2) Get the script tag's literal path [algo 20111225°1251]
    var sPathToCanary = '';
    for (var i2 = 0; i2 < script.attributes.length; i2++) {
        if ( script.attributes[i2].name === 'src' ) {
            sPathToCanary = script.attributes[i2].value;
            break;
        }
    }

    // Reduce from canary script path to folder only path [seq 20190316°0131]
    var sWantedPath = sPathToCanary.substring ( 0 , ( sPathToCanary.length - sCanary.length ) );

    // Output message
    s23 += '\n &nbsp; • sCanary (2/2) = "' + sCanary + '"';
    s23 += '\n &nbsp; • sWantedPath = "' + sWantedPath + '"';
    Trekta.Utils.outDbgMsg(s23);

    return sWantedPath;
};

/**
 *  This function shows an alert box on top of the page
 *
 *  @id 20160622°0241
 *  @see ref 20160916°1221 'stackoverflow → change element class with js'
 *  @note The tricky thing here is, that we want it work from two different
 *     times. (1) The HTML body does not yet exist, so we have to pipeline it
 *     to the onload event. (2) The body is available to directly work on it.
 *  @todo 20160622°0312 : For now, this function 20160622°0241 does process
 *     multiple alerts only from situations when the body is already loaded
 *     (see the respective test buttons). But multiple alerts coming from
 *     runtime points before that, will probably fail to work correctly!
 *     Test and fix that situation.
 *  @callers : Only • two links in testing.html
 *  @param {string} sMessage — The message to be output
 *  @return {undefined} —
 */
Daf.Utlis.showAlertBox = function(sMessage)
{
    'use strict';

    // Cache message - this is not the final way (see todo 20160622°0312).
    if ( Daf.Utlis.sCacheAlertBoxMessage === null ) {
        Daf.Utlis.sCacheAlertBoxMessage = sMessage;
    }

    // Prepare location to insert
    // hm .. the body does not exist yet!
    var eBody = document.getElementsByTagName('body')[0];
    if (! eBody) {
        // The body is not yet available, try again later
        //  This will not work correctly multiple times. See todo 20160622°0312.
        Trekta.Utils.windowOnloadDaisychain(Daf.Utlis.Funcs.showAlertBox);
        return;
    }
    var eTarget = eBody.firstChild;                                     // E.g. '<TextNode textContent="\n\n">'

    // Search an even better place for the box
    // Targeting the body just guarantees a fallback. If possible,
    //  we like to position the box a few elements below.
    var eChapterPane = document.getElementsByClassName("ChapterFaceNormal")[0];
    if (eChapterPane) {
        eBody = eChapterPane;
        eTarget = eChapterPane.firstChild;
    }

    // Process message
    // See issue 20120508°0921 'IE fails to set innerHTML of paragraph'
    var sMsg = Daf.Utlis.convertLinebreaksToHtml(Daf.Utlis.sCacheAlertBoxMessage);

    // Prepare element to insert
    // See issue 20120508°0921 'IE fails to set innerHTML of paragraph'
    var eDiv = document.createElement('div');
    eDiv.className = "DafFurnitureShowAlertBox";
    eDiv.innerHTML = sMsg;

    // Insert element
    eBody.insertBefore(eDiv, eTarget);

    // Clear cache
    Daf.Utlis.sCacheAlertBoxMessage = null;

    // Ready
    return;
};

/**
 *  This function shows a dialogbox to let the user select Continue/Break
 *
 *  @id 20120829°1922
 *  @callers (1) func 20120830°0451 EditFinishTransmit 'Transmit this change?',
 *     (2) func 20110811°1921 execElementEdit 'Debug dialog before editing element'
 *  @param sMessage {string} — The text inside the dialog box
 *  @param sTitle {string} — The optional title of the dialog box
 *  @return {string} — The answer from the button the user pressed
 */
Daf.Utlis.showDialogContinueBreak_DISCARD = function(sMessage, sTitle)
{
    'use strict';

    sTitle = (sTitle === '') ? 'Continue?' : sTitle;
    var a = [];
    a.push(sTitle);                                                     // [0] Title
    a.push(sMessage);                                                   // [1] Message
    a.push(false);                                                      // [2] Botton yes
    a.push(false);                                                      // [3] Button no
    a.push(false);                                                      // [4] Button cancel
    a.push(true);                                                       // [5] Button continue
    a.push(true);                                                       // [6] Button break
    var s = Daf.Utlis.showDialogbox_DISCARD(a);
    return s;
};

/**
 *  This function shows a dialogbox
 *
 *  @id func 20120829°1911
 *  @todo Merge funcs 20120829°1911 showDialogbox and 20120827°1713 modalDialogEdit
 *  @see todo 20180106°1117 'replace showModalDialog'
 *  @note Matrix         +  Chr 63   Edge 42  IE-11    FF-66    Opera-58
 *     - showModalDialog :  no       no       yes      no       no
 *     - dialog element  :  yes      no       no       optin    yes
 *  note 20190424°0341 : In polyfill 20190424°0221, line 20190424°0227 fails.
 *     A bold attempt to fix it with "showModalDialog.caller = 'hello';" does
 *     not work, property is read-only. The usage were a bit tricky, see the
 *     documentation. I decide to go for the GoogleChrome dialog-polyfill.
 *  @callers • showDialogContinueBreak_DISCARD • showDialogYesNoCancel
 *  @param aContent {Array} The array with the elements to show
 *  @return {string} — Answer string 'yes', 'no' or 'cancel'
 */
Daf.Utlis.showDialogbox_DISCARD = function(aContent)
{
    'use strict';

    // Prologue [seq 20190424°0625]
    var sRet = 'cancel';                                                // Pessimistic predetermination

    // Provide settings [seq 20190424°0627]
    var iWidth = 480;
    var iHeight = 480;
    var sUrl = Trekta.Utils.s_DaftariBaseFolderRel + '/' + Daf.Const.sFILE_PAGE_DIALOG;  // '/panels/dialog.html'
    var sOpts = 'center:yes;'                                           // Does not work as expected
               + ' dialogHeight:' + iHeight + 'px;'
                //+ ' dialogLeft:200;'
                 //+ ' dialogTop:200;'
                  + ' dialogWidth:' + iWidth + 'px;'
                   + ' edge:raised;'
                    + ' help:no;'
                     + ' resizable:yes;'
                      + ' status:no;'                                   // Yes
                       ;

    // Feature detect [seq 20180322°0311] (compare prototype 20180303°1141)
    if ( typeof showModalDialog !== 'undefined'
        && typeof showModalDialog === 'function'
         )
    {
        // Use IE native showModalDialog [seq 20120829°1913]
        sRet = window.showModalDialog(sUrl, aContent, sOpts);
        return sRet;
    }

    // Test dialog element availablility [seq 20190424°0615]
    // See ref 20190424o0613 'javascriptkit → test support for element'
    var eCanary = document.createElement("dialog");
    var bDialogAvail = (eCanary.showModal) ? true : false;
    if (bDialogAvail) {

        // [line 20190424°0617]
        if ( Trekta.Utils.bToggle_FALSE ) {
            sRet = Daf.Utlis.showDialogbx_continue_DISCARD(sUrl, aContent, sOpts);
        } else {
            sRet = window.showModalDialog(sUrl, aContent, sOpts);
        }

        return sRet;
    }

    // Here come browsers with nor native showModalDialog nor native
    //  dialog element. This are mostly Edge and Safari.
    // todo : Load googlechrome dialog-polyfill from download 20190424°0421

    // Escape with user notification [seq 20120829°1915]
    var s = "Sorry, this browser does not show the Edit dialogbox."
           + Daf.Const.sTb_1Cr + "Please try e.g. Internet Explorer or Chrome."
            + Daf.Const.sTb_2Cr + "[dialog 20180322°0312]"
             ;
    alert(s);

    // Reset page [line 20120829°1917]
    Daf.Mnu.Meo.execPageSwitchEditmodeOff();

    return sRet;                                                        // Check - Use the same string, which the dialog uses for 'cancel'
};

/**
 *  This function shows a dialogbox
 *
 *  @id func 20190424°0621
 *  @see ref 20190425°0222 'javascript.info → async/await'
 *  @note Proceeding e.g.: Write first after func 20190424°0225 far below,
 *      then refine after ref 20190424°0431 'agektmr → dialog element demo'
 *  @callers • func 20120829°1911 Daf.Utlis.showDialogbox_DISCARD
 *  @param sUrl {string} The URL for the dialog page
 *  @param aArgs {Array} The array with the elements to show
 *  @param sOpts {string} The options
 *  @return {string|Promise} — Answer string 'yes', 'no' or 'cancel' [marker 20210416°1633`04 GoCloCom]
 */
Daf.Utlis.showDialogbx_continue_DISCARD = function(sUrl, aArgs, sOpts)
{
    'use strict';

    // Debug [seq 20190424°0623]
    if (Daf.Const.b_Toggle_FALSE) {
        var n4 = 0;
        var s4 = 'Here shall come the dialog element.\n';
        // Remember issue 20190424°0721 'IE11 complains arrow style'
        aArgs.forEach( function (x) {
            n4 += 1;
            var s5 = x.toString();
            s5 = (s5.length > 24) ? s5.substr(0, 23) : s5;
            s4 += '\n' + '(' + n4 + ') ' + s5;
        });
        alert(s4);
    }

    // [seq 20190424°0631]
    var dialog = document.body.appendChild(document.createElement('dialog'));
    dialog.setAttribute('style', sOpts.replace(/dialog/gi, ''));
    dialog.innerHTML = '<a href="#" id="dialog-close"'
                      + ' style="position: absolute; top: 0; right: 5px; font-size: 20px;'
                       + ' color: #000; text-decoration: none; outline: none;'
                        + '">&times;</a>'
                         + '<iframe id="dialog-body" src="' + sUrl + '"'
                          + ' style="border: 0; width: 100%; height: 100%;">'
                           + '</iframe>'
                            ;
    document.getElementById('dialog-body').contentWindow.dialogArguments = aArgs;
    document.getElementById('dialog-close').addEventListener('click', function(e) {
        e.preventDefault();
        dialog.close();
    });
    dialog.showModal();

    // [seq 20190424°0633]
    var promise = new Promise(function(resolve, reject) {
         dialog.addEventListener('close', function() {
              var returnValue = document.getElementById('dialog-body').contentWindow.returnValue;
              document.body.removeChild(dialog);
              resolve(returnValue);
         });
    });

    /// let value = await promise;
    var value = promise;

    return value;
};

// NOT USED YET?
/**
 *  This function shall show a modal dialog. It shall replace func 20120829°1911 showDialogbox
 *
 *  @id func 20190426°0111
 *  @see ref 20190425°0412 'codeproject → js modal popup window'
 *  @see ref 20190425°0312 'Chirp Internet → simple modal feedback form'
 *  @callers • 20190426°0131 Exec_ProbeModPop_x0 //// exec_pgTestModPop_1
 *  @param {string} sUrl — The URL for a possible dialog page or null for message only
 *  @param {Array} aArgs — The array with the elements to show
 *  @param {string} sOpts — The options
 *  @param {Function} cbkFinal — The callback if everything is done
 *  @return {undefined} — Answer string 'yes', 'no' or 'cancel' [marker 20210416°1633`05 GoCloCom]
 */
Daf.Utlis.ShowModPop_ELIM = function(sUrl, aArgs, sOpts, cbkFinal) {

    'use strict';

    // () Provide callback for the pull-behind [seq 20190422°0635`11]   // [debugmark 20200103°0412`05]
    let fCallback = function() { Daf.Utlis.ShowModPop_continue ( sUrl
                                                                , aArgs
                                                                 , sOpts
                                                                  , cbkFinal
                                                                   );
                                   };

    // () Guarantee library
    let sLib = 'modpopdialog.min.js';
    Trekta.Utils.pullScriptBehind ( Trekta.Utils.s_DaftariBaseFolderAbs
                                   + '/jslibs/modpopdialog/' + sLib     // Module to pull
                                    , fCallback                         // Function to call then
                                     , null                             //
                                      , null                            //
                                       );
};

/**
 *  This function does the modal dialog after the script has loaded
 *
 *  @id func 20190426°0121
 *  @callers • via callback
 *  @param {string} sUrl — The URL for the dialog page
 *  @param {Array} aArgs — The array with the elements to show
 *  @param {string} sOpts — The options
 *  @param {Function} cbkFinal — The callback if everything is done
 *  @return {string|undefined} — Answer string 'yes', 'no' or 'cancel'  // [marker 20210416°1633`07 GoCloCom]
 */
Daf.Utlis.ShowModPop_continue = function(sUrl, aArgs, sOpts, cbkFinal)
{
    'use strict';
    let s4 = '';

    //// // Code after demo file 'Parent.htm' [seq 20190426°0122]
    //// let modalWin = Mpd.ShowModalDialog();

    // [condi 20190426°0123]
    if (sUrl === '' && aArgs[Daf.Utlis.iDlgOptNdx_btn_yes] === false) {  // Provisory signal
        // Plain dialog [seq 20190426°0124]
        Mpd.ShowModalDialog ( 'User Information'                        // Title // XYZ
                             , 'This is ModPop simplest flavour.'       // Message
                              , 333 // 200                              // Height
                               , 333 // 400                             // Width
                                );
    }
    else if (sUrl === '') {
        // Dialog with confirmation [seq 20190426°0125]
        s4 = 'This is confirmation window using Javascript';
        modalWin.ShowConfirmationMessage ( s4
                                          , 200
                                           , 400
                                            , 'User Confirmation'
                                             , null
                                              , 'Agree'
                                            //, Action1
                                               , function() { alert("Action One"); }
                                                , 'Disagree'
                                              //, Action2
                                                 , function() { alert("Action Two"); }
                                                  );
    }
    else {

        // Dialog with the external page [seq 20190426°0126]
        let callbackFunctionArray = [ function() { alert('Dialog answer:\n\nEnrollNow'); }
                                   , function() { alert('Dialog answer:\n\nEnrollLater'); }
                                    ];

        // [line 20190427°0211]
        Mpd.aTRANSFER_ARGS_PARAM = aArgs;                               // [marker 20210416°1633`09 GoCloCom JSC_TYPE_MISMATCH]
/*
        modalWin.ShowURL ( sUrl                                         // './demochild1.html'
                          , 320
                           , 470
                            , 'User Enrollment'
                             , null
                              , callbackFunctionArray
                               , true                                   // Show maximize button
                               );
*/
    }

};

/**
 *  This function builds a table-row-like formatted string
 *
 *  @id 20190413°0411
 *  @todo Nice feature, sorrily currently not applied. Make a demo [todo 20190417°0455]
 *  @todo If overage is too big, it has to be reducted over several iterations
 *  @callers So far only • ...
 *  @param aIn {Array} Array of arrays with strings and wanted lengthes
 *  @param sDeli {string} Delimiter
 *  @return {string} — The resulting string
 */
Daf.Utlis.tabAlign = function(aIn, sDeli)
{
    'use strict';
    var sRet = '';
    var iOver = 0;

    // Loop over the elements, each a text/length pair
    // JSHint says "The body of a for in should be wrapped in an if statement
    //  to filter unwanted properties from the prototype" [note 20190423°0643]
    for (var i23 in aIn) {

        // Convenience
        var sText = aIn[i23][0];
        var iSpec = aIn[i23][1];

        // Process overage
        if (iOver > 0) {
            iSpec = iSpec - iOver;
            iSpec = iSpec > 0 ? iSpec : 0;
            iOver = 0;                                                  // This is too bold, could need several iterations
        }

        // Calculate padding [seq 20190413°0311 (after 20190413°0311)]
        var iPad = 0;
        var iDif = iSpec - sText.length;
        if ( iDif > 0 ) {
            iPad = iDif;
        }
        else {
            iOver = iDif * (-1) + 1;
        }

        // Append padding
        var a = [];
        a.length = iPad;
        var s55 = sText + a.join('&nbsp;');
        sRet += sDeli + s55;
    }

    return sRet;
};

/**
 *  This function trims both left and right whitespace from a string
 *
 *  @id 20120901°1201 trimmB
 *  @check Has JavaScript really no native trim function/method? [todo 20120901°1205]
 *  @see ref 20140704°0834 'stackoverflow → trim string in javascript'
 *  @callers ElementEditCatch_splitMulti()
 *  @param {string} sTx — The string to be trimmed
 *  @return {string} — The wanted trimmed string
 */
Daf.Utlis.trimmB = function(sTx) {
    'use strict';
    sTx = Daf.Utlis.trimmL(sTx);
    sTx = Daf.Utlis.trimmR(sTx);
    return sTx;
};

/**
 *  This function trims left trailing whitespace from a string
 *
 *  @id 20120831°1841
 *  @callers Daf.Utlis.trimmB()
 *  @param sTx {string} — The string to be trimmed
 *  @return {string} — The wanted trimmed string
 */
Daf.Utlis.trimmL = function(sTx) {
    'use strict';
    if ( ! sTx ) {
        return '';
    }
    if (sTx === '') {
        return sTx;
    }
    var s = sTx.substr(0, 1);
    while ((s === ' ') || (s === '\n') || (s === '\r') || (s === '\t')) {
        if ((sTx === ' ') || (sTx === '\n') || (sTx === '\r') || (sTx === '\t')) {
            sTx = '';
            break;
        }
        else {
            sTx = sTx.substr(1, sTx.length - 1);
            s = sTx.substr(0, 1);
        }
    }
    return sTx;
};

/**
 *  This function trims right trailing whitespace from a string
 *
 *  @id 20120901°1202
 *  @note Func superfluous, see ref 20190404°0951 'MDN → String​.prototype​.trim (since IE9)'
 *  @callers Daf.Utlis.trimmB(sTx)
 *  @param sTx {string} — The string to be trimmed
 *  @return {string} — The wanted trimmed string
 */
Daf.Utlis.trimmR = function (sTx) {
    'use strict';
    if ( ! sTx ) {
        return '';
    }
    if (sTx === '') {
        return sTx;
    }
    var s = sTx.substr(sTx.length - 1, 1);
    while ((s === ' ') || (s === '\n') || (s === '\r') || (s === '\t')) {
        if ((sTx === ' ') || (sTx === '\n') || (sTx === '\r') || (sTx === '\t')) {
            sTx = '';
            break;
        }
      else {
            sTx = sTx.substr(0, sTx.length - 1);
            s = sTx.substr(sTx.length - 1, 1);
        }
    }
    return sTx;
};

/**
 *  This function trims right trailing given character(s) from a string
 *
 *  @id 20120901°1202
 *  @note Func superfluous, see ref 20190404°0951 'MDN → String​.prototype​.trim (since IE9)'
 *  @callers ...
 *  @param {string} sTx — The string to be trimmed
 *  @param {string} sChar — The 1-char-string to taken off
 *  @return {string} — The wanted trimmed string
 */
Daf.Utlis.trimmRChar = function (sTx, sChar)
{
    'use strict';
    if ( ! sTx ) {
        return '';
    }
    if (sTx === '') {
        return sTx;
    }
    var s = sTx.substr(sTx.length - 1, 1);
    while (s === sChar) {
        if (sTx === sChar) {
            sTx = '';
            break;
        }
      else {
            sTx = sTx.substr(0, sTx.length - 1);
            s = sTx.substr(sTx.length - 1, 1);
        }
    }
    return sTx;
};

/**
 *  This function tells whether a given element has a given attribute
 *
 *  @id 20120905°1501
 *  @status nice
 *  @see ref 20120905°1502 'selfhtml → hasAttribute für IE mit prototype' about double negation
 *  @note The wanted attribute must not be empty to be detected.
 *  @usage e.g. in the Lingo Selection Area : data-wafailiskip="yes"
 *  @callers • execPageSwitchEditmodeOn • ..
 *  @param {Element} eEle — The element to be inspected
 *  @param {string} sAtt — The attribute to be looked for
 *  @return {boolean} — The wanted answer yes or not
 */
Daf.Utlis.util_hasAttribute = function(eEle, sAtt)
{
  'use strict';
    var b = false;
    b = ( !! eEle.getAttribute(sAtt) );
    return b;
};

/**
 *  This var stores the messages as long the output area is not yet available
 *
 *  @id 20160622°0311
 *  @type {string|null} —
 */
Daf.Utlis.sCacheAlertBoxMessage = null;

/**
 *  This namespace hosts some constants
 *
 *  @id 20190425°0131
 *  @callers •
 *  @c_o_n_s_t — Namespace
 */
Daf.Utlis.Cst = Daf.Utlis.Cst || {};

// Define some indices [seq 20190425°0141]
// Keep in sync with seq 20190425°0151 in dialog.html
Daf.Utlis.iDlgOptNdx_btn_title = 0;                                     // [var 20190425°0142]
Daf.Utlis.iDlgOptNdx_btn_message = 1;                                   // [var 20190425°0143]
Daf.Utlis.iDlgOptNdx_btn_yes = 2;                                       // [var 20190425°0144]
Daf.Utlis.iDlgOptNdx_btn_no = 3;                                        // [var 20190425°0145]
Daf.Utlis.iDlgOptNdx_btn_cancel = 4;                                    // [var 20190425°0146]
Daf.Utlis.iDlgOptNdx_btn_continue = 5;                                  // [var 20190425°0147]
Daf.Utlis.iDlgOptNdx_btn_break = 6;                                     // [var 20190425°0148]

/**
 *  This namespace hosts the autoflick feature
 *
 *  @id 20190420°0231
 *  @note Remember todo 20190420°0345 'use general autoflick for gallery as well'
 *  @note Code shifted here from former file 20110512°1311 gallery-js-autoflick.js
 *  @callers •
 *  @c_o_n_s_t — Namespace
 */
Daf.Utlis.Flick = Daf.Utlis.Flick || {};

/**
 *  This function constitutes the autoflick feature entry point
 *
 *  @id 20110510°2141
 *  @callers • func 20110813°2221 Daf.Core.daftari
 *  @return {undefined} —
 */
Daf.Utlis.Flick.autoflickAvail = function()
{
    'use strict';

    // Is the arrows div on the page? [seq 20190421°0141]
    var elArrowsDiv = document.getElementsByClassName(Daf.Sitmp.Config.sCAKE_ARROWS_DIV_CLASSNAME)[0];
    if (typeof elArrowsDiv !== 'undefined') {

        // Supplement eventhandler [seq 20190421°0143]
        // This replaces the formerly hardcoded onmouseenter in func
        //  20160417°1241 Daf.Sitmp.workoff_Cake_4_assemble, seq 20160618°0128
        elArrowsDiv.onmouseenter = Daf.Utlis.Flick.flickerStop;
    }
    else {
        // Hm .. not sure whether this is really appropriate.
        // The flicking would probably also work without the arrows div,
        // but we had no means to stop it then. Isnt it?
        return;
    }

    // Retrieve status [line 20110510°2142]
    var iMillisec = Trekta.Utils.getCookieInt(Daf.Const.sCookieName_autoflick, 0);

    // Schedule the wanted action [seq 20110510°2143]
    if (iMillisec > 0) {
        // Show status [line 20110510°2144]
        var el = document.getElementById("autoflick_status");
        el.innerHTML = 'autoflick ' + iMillisec.toString() + ' ms';
        el.style.display = 'block';                                     // block, inline

        // Ignit flicking [line 20110510°2145]
        Daf.Utlis.Flick.tim = setTimeout(Daf.Utlis.Flick.autoflickAction, iMillisec);
    }
};

/**
 *  This function flicks the page
 *
 *  @id 20110512°0121
 *  @note
 *  @callers • The main functions's scheduler
 *  @return {undefined} —
 */
Daf.Utlis.Flick.autoflickAction = function()
{
    'use strict';

    // Retrieve URL [seq 20110512°0123]
    var el = document.getElementById('autoflick_forward');
    var sUrl = el.href;

    // Goto URL [seq 20110512°0125]
    if (Daf.Const.b_Toggle_TRUE) {
        location.href = sUrl;
    }
    else {
        // This works as well
        location.href = el;
    }
};

/**
 *  This function starts flicking
 *
 *  @id 20110510°2146
 *  @callers •
 *  @param {number} iMillisec (Integer) — Timespan to the next flick
 *  @return {undefined} —
 */
Daf.Utlis.Flick.flickerRun = function(iMillisec)
{
    'use strict';

    // Put signal [line 20110510°2155]
    Trekta.Utils.setCookie(Daf.Const.sCookieName_autoflick, iMillisec.toString(), 7);

    // rRefresh page to start timer [line 20110510°2156]
    Daf.Utlis.refreshPage();
};

/**
 *  This function stops autoflicking
 *
 *  @id 20110510°2147
 *  @callers •
 *  @return {undefined} —
 */
Daf.Utlis.Flick.flickerStop = function()
{
    'use strict';

    // React only if autoflick is active [line 20190420°0341]
    if ( Trekta.Utils.getCookieInt(Daf.Const.sCookieName_autoflick, 0) < 1 ) {
        return;
    }

    // Reset signal [line 20110510°2148]
    Trekta.Utils.setCookie(Daf.Const.sCookieName_autoflick, '0', 7);

    // Refresh hide status area [line 20190420°0343]
    Daf.Utlis.refreshPage();
};

/**
 *  The timer for flicking
 *
 *  @id 20190420°0321
 *  @type {number|null}
 */
Daf.Utlis.Flick.tim = null;

// eof
