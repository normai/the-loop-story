﻿/*!
 *  This module shall pull-behind the rest of Daftari
 *
 *  id : 20181106°0611 daftari/jsi/daftarigo.js
 *  license : GNU AGPL v3
 *  copyright : © 2018 - 2023 Norbert C. Maier
 *  authors : ncm
 *  status : Under construction
 *  callers : none yet(?)
 *
 */

// Options for JSHint [seq 20190423°0421`09]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf */

/**
 *  This namespace constitutes the Daftari root namespace
 *
 *  @id 20170923°0421`07
 *  @callers
 *  @const — Namespace
 */
Daf = window.Daf || {};

/**
 *  This namespace shall host the earliest functions
 *
 *  @id 20181106°0631
 *  @callers
 *  @const — Namespace
 */
Daf.Go = Daf.Go || {};

/**
 *  This namespace hosts earliest variables and constants
 *
 *  @id 20181106°0641
 *  @callers
 *  @const — Namespace
 */
Daf.Go.Vars = Daf.Go.Vars || {};

/**
 *  This variable stores some dummy string
 *
 *  @id 20181106°0642
 *  @callers
 *  @type {string} —
 */
Daf.Go.Vars.sDummyWelcome = "[Debug 20181106°0643] Hello Daf.Go.Vars namespace.";

/**
 *  This function performs some dummy action
 *
 *  @id 20181106°0643
 *  @callers
 *  @return {undefined} —
 */
Daf.Go.dummyFunction = function()
{
   'use strict';
   // alert('Dummy function 20181106°0643 :\n' + Daf.Go.Vars.sDummyWelcome);
};

// Do some script level call [seq 20181106°0651]
Daf.Go.dummyFunction();
