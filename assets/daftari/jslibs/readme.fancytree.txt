﻿
   *** fancytree ***

   This is Fancytree v2.36.1  with jQuery v3.5.1

   This was downloaded from https://github.com/mar10/fancytree/releases
    as fancytree-2.36.1.zip [download 20200805°1311 file 20200805°1313]

   This is a slimmed down version with the minimum files. For the full folder
   see https://downtown.trilo.de/svn/daftaridev/trunk/z.shunting/fancytree-2.36.1

   sources : https://github.com/mar10/fancytree/

   demo : http://wwwendt.de/tech/fancytree/demo/index.html

   license : MIT style

   ————————————————————————————————————————————————————————————
   Project log :

   download 20200805°1311 fancytree v2.36.1

   download 20190227°1331 fancytree v2.30.1

   download 20161004°1711 fancytree v2.19.0

   folder 20161010°0211
   url : https://downtown.trilo.de/svn/daftaridev/trunk/htdocs/daftari/jslibs/fancytree
   title : Fancytree
   producthome : https://github.com/mar10/fancytree/
      and http://wwwendt.de/tech/fancytree/demo/index.html
   license : MIT style
   copyright :
   authors :
   note :
   ◦

   ———————————————————————
   [file 20161010°0212] ◦Ω
