/*
 This script dynamically displays text files inside an HTML page

 Version v0.3.3.b
 License BSD 3-Clause License
 Copyright © 2014 - 2023 Norbert C. Maier
*/
var $jscomp = $jscomp || {};
$jscomp.scope = {};
$jscomp.createTemplateTagFirstArg = function(c) {
  return c.raw = c;
};
$jscomp.createTemplateTagFirstArgWithRaw = function(c, d) {
  c.raw = d;
  return c;
};
$jscomp.arrayIteratorImpl = function(c) {
  var d = 0;
  return function() {
    return d < c.length ? {done:!1, value:c[d++], } : {done:!0};
  };
};
$jscomp.arrayIterator = function(c) {
  return {next:$jscomp.arrayIteratorImpl(c)};
};
$jscomp.makeIterator = function(c) {
  var d = "undefined" != typeof Symbol && Symbol.iterator && c[Symbol.iterator];
  return d ? d.call(c) : $jscomp.arrayIterator(c);
};
$jscomp.arrayFromIterator = function(c) {
  for (var d, f = []; !(d = c.next()).done;) {
    f.push(d.value);
  }
  return f;
};
$jscomp.arrayFromIterable = function(c) {
  return c instanceof Array ? c : $jscomp.arrayFromIterator($jscomp.makeIterator(c));
};
$jscomp.ASSUME_ES5 = !1;
$jscomp.ASSUME_NO_NATIVE_MAP = !1;
$jscomp.ASSUME_NO_NATIVE_SET = !1;
$jscomp.SIMPLE_FROUND_POLYFILL = !1;
$jscomp.ISOLATE_POLYFILLS = !1;
$jscomp.FORCE_POLYFILL_PROMISE = !1;
$jscomp.FORCE_POLYFILL_PROMISE_WHEN_NO_UNHANDLED_REJECTION = !1;
$jscomp.objectCreate = $jscomp.ASSUME_ES5 || "function" == typeof Object.create ? Object.create : function(c) {
  var d = function() {
  };
  d.prototype = c;
  return new d;
};
$jscomp.defineProperty = $jscomp.ASSUME_ES5 || "function" == typeof Object.defineProperties ? Object.defineProperty : function(c, d, f) {
  if (c == Array.prototype || c == Object.prototype) {
    return c;
  }
  c[d] = f.value;
  return c;
};
$jscomp.getGlobal = function(c) {
  c = ["object" == typeof globalThis && globalThis, c, "object" == typeof window && window, "object" == typeof self && self, "object" == typeof global && global, ];
  for (var d = 0; d < c.length; ++d) {
    var f = c[d];
    if (f && f.Math == Math) {
      return f;
    }
  }
  throw Error("Cannot find global object");
};
$jscomp.global = $jscomp.getGlobal(this);
$jscomp.IS_SYMBOL_NATIVE = "function" === typeof Symbol && "symbol" === typeof Symbol("x");
$jscomp.TRUST_ES6_POLYFILLS = !$jscomp.ISOLATE_POLYFILLS || $jscomp.IS_SYMBOL_NATIVE;
$jscomp.polyfills = {};
$jscomp.propertyToPolyfillSymbol = {};
$jscomp.POLYFILL_PREFIX = "$jscp$";
var $jscomp$lookupPolyfilledValue = function(c, d) {
  var f = $jscomp.propertyToPolyfillSymbol[d];
  if (null == f) {
    return c[d];
  }
  f = c[f];
  return void 0 !== f ? f : c[d];
};
$jscomp.polyfill = function(c, d, f, b) {
  d && ($jscomp.ISOLATE_POLYFILLS ? $jscomp.polyfillIsolated(c, d, f, b) : $jscomp.polyfillUnisolated(c, d, f, b));
};
$jscomp.polyfillUnisolated = function(c, d, f, b) {
  f = $jscomp.global;
  c = c.split(".");
  for (b = 0; b < c.length - 1; b++) {
    var h = c[b];
    if (!(h in f)) {
      return;
    }
    f = f[h];
  }
  c = c[c.length - 1];
  b = f[c];
  d = d(b);
  d != b && null != d && $jscomp.defineProperty(f, c, {configurable:!0, writable:!0, value:d});
};
$jscomp.polyfillIsolated = function(c, d, f, b) {
  var h = c.split(".");
  c = 1 === h.length;
  b = h[0];
  b = !c && b in $jscomp.polyfills ? $jscomp.polyfills : $jscomp.global;
  for (var k = 0; k < h.length - 1; k++) {
    var u = h[k];
    if (!(u in b)) {
      return;
    }
    b = b[u];
  }
  h = h[h.length - 1];
  f = $jscomp.IS_SYMBOL_NATIVE && "es6" === f ? b[h] : null;
  d = d(f);
  null != d && (c ? $jscomp.defineProperty($jscomp.polyfills, h, {configurable:!0, writable:!0, value:d}) : d !== f && (void 0 === $jscomp.propertyToPolyfillSymbol[h] && ($jscomp.propertyToPolyfillSymbol[h] = $jscomp.IS_SYMBOL_NATIVE ? $jscomp.global.Symbol(h) : $jscomp.POLYFILL_PREFIX + h), $jscomp.defineProperty(b, $jscomp.propertyToPolyfillSymbol[h], {configurable:!0, writable:!0, value:d})));
};
$jscomp.getConstructImplementation = function() {
  function c() {
    function f() {
    }
    new f;
    Reflect.construct(f, [], function() {
    });
    return new f instanceof f;
  }
  if ($jscomp.TRUST_ES6_POLYFILLS && "undefined" != typeof Reflect && Reflect.construct) {
    if (c()) {
      return Reflect.construct;
    }
    var d = Reflect.construct;
    return function(f, b, h) {
      f = d(f, b);
      h && Reflect.setPrototypeOf(f, h.prototype);
      return f;
    };
  }
  return function(f, b, h) {
    void 0 === h && (h = f);
    h = $jscomp.objectCreate(h.prototype || Object.prototype);
    return Function.prototype.apply.call(f, h, b) || h;
  };
};
$jscomp.construct = {valueOf:$jscomp.getConstructImplementation}.valueOf();
$jscomp.underscoreProtoCanBeSet = function() {
  var c = {a:!0}, d = {};
  try {
    return d.__proto__ = c, d.a;
  } catch (f) {
  }
  return !1;
};
$jscomp.setPrototypeOf = $jscomp.TRUST_ES6_POLYFILLS && "function" == typeof Object.setPrototypeOf ? Object.setPrototypeOf : $jscomp.underscoreProtoCanBeSet() ? function(c, d) {
  c.__proto__ = d;
  if (c.__proto__ !== d) {
    throw new TypeError(c + " is not extensible");
  }
  return c;
} : null;
$jscomp.inherits = function(c, d) {
  c.prototype = $jscomp.objectCreate(d.prototype);
  c.prototype.constructor = c;
  if ($jscomp.setPrototypeOf) {
    var f = $jscomp.setPrototypeOf;
    f(c, d);
  } else {
    for (f in d) {
      if ("prototype" != f) {
        if (Object.defineProperties) {
          var b = Object.getOwnPropertyDescriptor(d, f);
          b && Object.defineProperty(c, f, b);
        } else {
          c[f] = d[f];
        }
      }
    }
  }
  c.superClass_ = d.prototype;
};
$jscomp.polyfill("Reflect", function(c) {
  return c ? c : {};
}, "es6", "es3");
$jscomp.polyfill("Reflect.construct", function(c) {
  return $jscomp.construct;
}, "es6", "es3");
$jscomp.polyfill("Reflect.setPrototypeOf", function(c) {
  if (c) {
    return c;
  }
  if ($jscomp.setPrototypeOf) {
    var d = $jscomp.setPrototypeOf;
    return function(f, b) {
      try {
        return d(f, b), !0;
      } catch (h) {
        return !1;
      }
    };
  }
  return null;
}, "es6", "es5");
$jscomp.polyfill("Number.parseInt", function(c) {
  return c || parseInt;
}, "es6", "es3");
$jscomp.checkStringArgs = function(c, d, f) {
  if (null == c) {
    throw new TypeError("The 'this' value for String.prototype." + f + " must not be null or undefined");
  }
  if (d instanceof RegExp) {
    throw new TypeError("First argument to String.prototype." + f + " must not be a regular expression");
  }
  return c + "";
};
$jscomp.polyfill("String.prototype.repeat", function(c) {
  return c ? c : function(d) {
    var f = $jscomp.checkStringArgs(this, null, "repeat");
    if (0 > d || 1342177279 < d) {
      throw new RangeError("Invalid count value");
    }
    d |= 0;
    for (var b = ""; d;) {
      if (d & 1 && (b += f), d >>>= 1) {
        f += f;
      }
    }
    return b;
  };
}, "es6", "es3");
$jscomp.stringPadding = function(c, d) {
  c = void 0 !== c ? String(c) : " ";
  return 0 < d && c ? c.repeat(Math.ceil(d / c.length)).substring(0, d) : "";
};
$jscomp.polyfill("String.prototype.padEnd", function(c) {
  return c ? c : function(d, f) {
    var b = $jscomp.checkStringArgs(this, null, "padStart");
    return b + $jscomp.stringPadding(f, d - b.length);
  };
}, "es8", "es3");
$jscomp.checkEs6ConformanceViaProxy = function() {
  try {
    var c = {}, d = Object.create(new $jscomp.global.Proxy(c, {get:function(f, b, h) {
      return f == c && "q" == b && h == d;
    }}));
    return !0 === d.q;
  } catch (f) {
    return !1;
  }
};
$jscomp.USE_PROXY_FOR_ES6_CONFORMANCE_CHECKS = !1;
$jscomp.ES6_CONFORMANCE = $jscomp.USE_PROXY_FOR_ES6_CONFORMANCE_CHECKS && $jscomp.checkEs6ConformanceViaProxy();
$jscomp.initSymbol = function() {
};
$jscomp.polyfill("Symbol", function(c) {
  if (c) {
    return c;
  }
  var d = function(h, k) {
    this.$jscomp$symbol$id_ = h;
    $jscomp.defineProperty(this, "description", {configurable:!0, writable:!0, value:k});
  };
  d.prototype.toString = function() {
    return this.$jscomp$symbol$id_;
  };
  var f = 0, b = function(h) {
    if (this instanceof b) {
      throw new TypeError("Symbol is not a constructor");
    }
    return new d("jscomp_symbol_" + (h || "") + "_" + f++, h);
  };
  return b;
}, "es6", "es3");
$jscomp.polyfill("Symbol.iterator", function(c) {
  if (c) {
    return c;
  }
  c = Symbol("Symbol.iterator");
  for (var d = "Array Int8Array Uint8Array Uint8ClampedArray Int16Array Uint16Array Int32Array Uint32Array Float32Array Float64Array".split(" "), f = 0; f < d.length; f++) {
    var b = $jscomp.global[d[f]];
    "function" === typeof b && "function" != typeof b.prototype[c] && $jscomp.defineProperty(b.prototype, c, {configurable:!0, writable:!0, value:function() {
      return $jscomp.iteratorPrototype($jscomp.arrayIteratorImpl(this));
    }});
  }
  return c;
}, "es6", "es3");
$jscomp.iteratorPrototype = function(c) {
  c = {next:c};
  c[Symbol.iterator] = function() {
    return this;
  };
  return c;
};
$jscomp.owns = function(c, d) {
  return Object.prototype.hasOwnProperty.call(c, d);
};
$jscomp.polyfill("WeakMap", function(c) {
  function d() {
    if (!c || !Object.seal) {
      return !1;
    }
    try {
      var r = Object.seal({}), w = Object.seal({}), B = new c([[r, 2], [w, 3]]);
      if (2 != B.get(r) || 3 != B.get(w)) {
        return !1;
      }
      B.delete(r);
      B.set(w, 4);
      return !B.has(r) && 4 == B.get(w);
    } catch (P) {
      return !1;
    }
  }
  function f() {
  }
  function b(r) {
    var w = typeof r;
    return "object" === w && null !== r || "function" === w;
  }
  function h(r) {
    if (!$jscomp.owns(r, u)) {
      var w = new f;
      $jscomp.defineProperty(r, u, {value:w});
    }
  }
  function k(r) {
    if (!$jscomp.ISOLATE_POLYFILLS) {
      var w = Object[r];
      w && (Object[r] = function(B) {
        if (B instanceof f) {
          return B;
        }
        Object.isExtensible(B) && h(B);
        return w(B);
      });
    }
  }
  if ($jscomp.USE_PROXY_FOR_ES6_CONFORMANCE_CHECKS) {
    if (c && $jscomp.ES6_CONFORMANCE) {
      return c;
    }
  } else {
    if (d()) {
      return c;
    }
  }
  var u = "$jscomp_hidden_" + Math.random();
  k("freeze");
  k("preventExtensions");
  k("seal");
  var x = 0, t = function(r) {
    this.id_ = (x += Math.random() + 1).toString();
    if (r) {
      r = $jscomp.makeIterator(r);
      for (var w; !(w = r.next()).done;) {
        w = w.value, this.set(w[0], w[1]);
      }
    }
  };
  t.prototype.set = function(r, w) {
    if (!b(r)) {
      throw Error("Invalid WeakMap key");
    }
    h(r);
    if (!$jscomp.owns(r, u)) {
      throw Error("WeakMap key fail: " + r);
    }
    r[u][this.id_] = w;
    return this;
  };
  t.prototype.get = function(r) {
    return b(r) && $jscomp.owns(r, u) ? r[u][this.id_] : void 0;
  };
  t.prototype.has = function(r) {
    return b(r) && $jscomp.owns(r, u) && $jscomp.owns(r[u], this.id_);
  };
  t.prototype.delete = function(r) {
    return b(r) && $jscomp.owns(r, u) && $jscomp.owns(r[u], this.id_) ? delete r[u][this.id_] : !1;
  };
  return t;
}, "es6", "es3");
$jscomp.MapEntry = function() {
};
$jscomp.polyfill("Map", function(c) {
  function d() {
    if ($jscomp.ASSUME_NO_NATIVE_MAP || !c || "function" != typeof c || !c.prototype.entries || "function" != typeof Object.seal) {
      return !1;
    }
    try {
      var t = Object.seal({x:4}), r = new c($jscomp.makeIterator([[t, "s"]]));
      if ("s" != r.get(t) || 1 != r.size || r.get({x:4}) || r.set({x:4}, "t") != r || 2 != r.size) {
        return !1;
      }
      var w = r.entries(), B = w.next();
      if (B.done || B.value[0] != t || "s" != B.value[1]) {
        return !1;
      }
      B = w.next();
      return B.done || 4 != B.value[0].x || "t" != B.value[1] || !w.next().done ? !1 : !0;
    } catch (P) {
      return !1;
    }
  }
  if ($jscomp.USE_PROXY_FOR_ES6_CONFORMANCE_CHECKS) {
    if (c && $jscomp.ES6_CONFORMANCE) {
      return c;
    }
  } else {
    if (d()) {
      return c;
    }
  }
  var f = new WeakMap, b = function(t) {
    this.data_ = {};
    this.head_ = u();
    this.size = 0;
    if (t) {
      t = $jscomp.makeIterator(t);
      for (var r; !(r = t.next()).done;) {
        r = r.value, this.set(r[0], r[1]);
      }
    }
  };
  b.prototype.set = function(t, r) {
    t = 0 === t ? 0 : t;
    var w = h(this, t);
    w.list || (w.list = this.data_[w.id] = []);
    w.entry ? w.entry.value = r : (w.entry = {next:this.head_, previous:this.head_.previous, head:this.head_, key:t, value:r, }, w.list.push(w.entry), this.head_.previous.next = w.entry, this.head_.previous = w.entry, this.size++);
    return this;
  };
  b.prototype.delete = function(t) {
    t = h(this, t);
    return t.entry && t.list ? (t.list.splice(t.index, 1), t.list.length || delete this.data_[t.id], t.entry.previous.next = t.entry.next, t.entry.next.previous = t.entry.previous, t.entry.head = null, this.size--, !0) : !1;
  };
  b.prototype.clear = function() {
    this.data_ = {};
    this.head_ = this.head_.previous = u();
    this.size = 0;
  };
  b.prototype.has = function(t) {
    return !!h(this, t).entry;
  };
  b.prototype.get = function(t) {
    return (t = h(this, t).entry) && t.value;
  };
  b.prototype.entries = function() {
    return k(this, function(t) {
      return [t.key, t.value];
    });
  };
  b.prototype.keys = function() {
    return k(this, function(t) {
      return t.key;
    });
  };
  b.prototype.values = function() {
    return k(this, function(t) {
      return t.value;
    });
  };
  b.prototype.forEach = function(t, r) {
    for (var w = this.entries(), B; !(B = w.next()).done;) {
      B = B.value, t.call(r, B[1], B[0], this);
    }
  };
  b.prototype[Symbol.iterator] = b.prototype.entries;
  var h = function(t, r) {
    var w = r && typeof r;
    "object" == w || "function" == w ? f.has(r) ? w = f.get(r) : (w = "" + ++x, f.set(r, w)) : w = "p_" + r;
    var B = t.data_[w];
    if (B && $jscomp.owns(t.data_, w)) {
      for (t = 0; t < B.length; t++) {
        var P = B[t];
        if (r !== r && P.key !== P.key || r === P.key) {
          return {id:w, list:B, index:t, entry:P};
        }
      }
    }
    return {id:w, list:B, index:-1, entry:void 0};
  }, k = function(t, r) {
    var w = t.head_;
    return $jscomp.iteratorPrototype(function() {
      if (w) {
        for (; w.head != t.head_;) {
          w = w.previous;
        }
        for (; w.next != w.head;) {
          return w = w.next, {done:!1, value:r(w)};
        }
        w = null;
      }
      return {done:!0, value:void 0};
    });
  }, u = function() {
    var t = {};
    return t.previous = t.next = t.head = t;
  }, x = 0;
  return b;
}, "es6", "es3");
$jscomp.polyfill("Set", function(c) {
  function d() {
    if ($jscomp.ASSUME_NO_NATIVE_SET || !c || "function" != typeof c || !c.prototype.entries || "function" != typeof Object.seal) {
      return !1;
    }
    try {
      var b = Object.seal({x:4}), h = new c($jscomp.makeIterator([b]));
      if (!h.has(b) || 1 != h.size || h.add(b) != h || 1 != h.size || h.add({x:4}) != h || 2 != h.size) {
        return !1;
      }
      var k = h.entries(), u = k.next();
      if (u.done || u.value[0] != b || u.value[1] != b) {
        return !1;
      }
      u = k.next();
      return u.done || u.value[0] == b || 4 != u.value[0].x || u.value[1] != u.value[0] ? !1 : k.next().done;
    } catch (x) {
      return !1;
    }
  }
  if ($jscomp.USE_PROXY_FOR_ES6_CONFORMANCE_CHECKS) {
    if (c && $jscomp.ES6_CONFORMANCE) {
      return c;
    }
  } else {
    if (d()) {
      return c;
    }
  }
  var f = function(b) {
    this.map_ = new Map;
    if (b) {
      b = $jscomp.makeIterator(b);
      for (var h; !(h = b.next()).done;) {
        this.add(h.value);
      }
    }
    this.size = this.map_.size;
  };
  f.prototype.add = function(b) {
    b = 0 === b ? 0 : b;
    this.map_.set(b, b);
    this.size = this.map_.size;
    return this;
  };
  f.prototype.delete = function(b) {
    b = this.map_.delete(b);
    this.size = this.map_.size;
    return b;
  };
  f.prototype.clear = function() {
    this.map_.clear();
    this.size = 0;
  };
  f.prototype.has = function(b) {
    return this.map_.has(b);
  };
  f.prototype.entries = function() {
    return this.map_.entries();
  };
  f.prototype.values = function() {
    return this.map_.values();
  };
  f.prototype.keys = f.prototype.values;
  f.prototype[Symbol.iterator] = f.prototype.values;
  f.prototype.forEach = function(b, h) {
    var k = this;
    this.map_.forEach(function(u) {
      return b.call(h, u, u, k);
    });
  };
  return f;
}, "es6", "es3");
$jscomp.assign = $jscomp.TRUST_ES6_POLYFILLS && "function" == typeof Object.assign ? Object.assign : function(c, d) {
  for (var f = 1; f < arguments.length; f++) {
    var b = arguments[f];
    if (b) {
      for (var h in b) {
        $jscomp.owns(b, h) && (c[h] = b[h]);
      }
    }
  }
  return c;
};
$jscomp.polyfill("Object.assign", function(c) {
  return c || $jscomp.assign;
}, "es6", "es3");
$jscomp.iteratorFromArray = function(c, d) {
  c instanceof String && (c += "");
  var f = 0, b = !1, h = {next:function() {
    if (!b && f < c.length) {
      var k = f++;
      return {value:d(k, c[k]), done:!1};
    }
    b = !0;
    return {done:!0, value:void 0};
  }};
  h[Symbol.iterator] = function() {
    return h;
  };
  return h;
};
$jscomp.polyfill("Array.prototype.keys", function(c) {
  return c ? c : function() {
    return $jscomp.iteratorFromArray(this, function(d) {
      return d;
    });
  };
}, "es6", "es3");
$jscomp.polyfill("Object.is", function(c) {
  return c ? c : function(d, f) {
    return d === f ? 0 !== d || 1 / d === 1 / f : d !== d && f !== f;
  };
}, "es6", "es3");
$jscomp.polyfill("Array.prototype.includes", function(c) {
  return c ? c : function(d, f) {
    var b = this;
    b instanceof String && (b = String(b));
    var h = b.length;
    f = f || 0;
    for (0 > f && (f = Math.max(f + h, 0)); f < h; f++) {
      var k = b[f];
      if (k === d || Object.is(k, d)) {
        return !0;
      }
    }
    return !1;
  };
}, "es7", "es3");
$jscomp.polyfill("String.prototype.includes", function(c) {
  return c ? c : function(d, f) {
    return -1 !== $jscomp.checkStringArgs(this, d, "includes").indexOf(d, f || 0);
  };
}, "es6", "es3");
$jscomp.findInternal = function(c, d, f) {
  c instanceof String && (c = String(c));
  for (var b = c.length, h = 0; h < b; h++) {
    var k = c[h];
    if (d.call(f, k, h, c)) {
      return {i:h, v:k};
    }
  }
  return {i:-1, v:void 0};
};
$jscomp.polyfill("Array.prototype.findIndex", function(c) {
  return c ? c : function(d, f) {
    return $jscomp.findInternal(this, d, f).i;
  };
}, "es6", "es3");
$jscomp.polyfill("String.prototype.startsWith", function(c) {
  return c ? c : function(d, f) {
    var b = $jscomp.checkStringArgs(this, d, "startsWith");
    d += "";
    var h = b.length, k = d.length;
    f = Math.max(0, Math.min(f | 0, b.length));
    for (var u = 0; u < k && f < h;) {
      if (b[f++] != d[u++]) {
        return !1;
      }
    }
    return u >= k;
  };
}, "es6", "es3");
$jscomp.polyfill("Array.prototype.find", function(c) {
  return c ? c : function(d, f) {
    return $jscomp.findInternal(this, d, f).v;
  };
}, "es6", "es3");
var Trekta = Trekta || {};
Trekta.Fadin = Trekta.Fadin || {};
Trekta.Fadin.Vars = Trekta.Fadin.Vars || {};
Trekta.Fadin.Vars = {aFifoHili:[], aFifoShdo:[], bConsole_FadeIn:!1, bDbg_20141118o0611_Gugg:!1, bDbg_20141117o0931_Curious_Elements:!1, bDbg_20141118o0921_Target_Element:!1, bDbg_20210515o1423_Relative_Path:!0, bDebugFadin030Each:!1, bOutpuzDebug:!1, iMaxCharPerLine:127, iReadFileNo:0, s:""};
Trekta.Fadin.Vars.converter = null;
Trekta.Fadin.Vars.eOutBox = null;
Trekta.Fadin.Vars.eOutCount = 0;
Trekta.Fadin.breakLongLine = function(c, d) {
  for (var f = ""; c.length > d;) {
    var b = c.substring(0, d), h = b.lastIndexOf(" "), k = b.lastIndexOf("\n");
    -1 !== k && (h = k);
    -1 === h && (h = d);
    f += b.substring(0, h) + "\n";
    Trekta.Utils.bIs_Browser_Explorer && (f += "<br />");
    c = c.substring(h + 1);
  }
  return f + c;
};
Trekta.Fadin.breakLongLines = function(c) {
  var d = "";
  c = c.split(/\n/);
  for (var f = 0; f < c.length; f++) {
    var b = Trekta.Fadin.breakLongLine(c[f], Trekta.Fadin.Vars.iMaxCharPerLine);
    d += b + "\n";
    Trekta.Utils.bIs_Browser_Explorer && (d += "<br />");
  }
  return d;
};
Trekta.Fadin.confirmAction = function(c) {
  return confirm(c);
};
Trekta.Fadin.convertMarkdown2Html = function(c) {
  null === Trekta.Fadin.Vars.converter && (Trekta.Fadin.Vars.converter = new showdown.Converter, Trekta.Fadin.Vars.converter.setOption("tables", !0));
  return Trekta.Fadin.Vars.converter.makeHtml(c);
};
Trekta.Fadin.fadeInFiles = function() {
  "function" === typeof setFadeInFilesMaxLineLength && (Trekta.Fadin.Vars.iMaxCharPerLine = setFadeInFilesMaxLineLength());
  for (var c = document.getElementsByClassName("fadein"), d = 0; d < c.length; d++) {
    var f = c.item(d);
    f.hasAttribute("data-fadein") || ("TR" === f.tagName ? f.setAttribute("data-fadein", "pre") : f.setAttribute("data-fadein", ""));
  }
  c = document.querySelectorAll("[data-fadein]");
  d = c.length;
  Trekta.Fadin.Vars.s = "To switch this debug messages off, set Trekta.Fad.Vars.bOutpuzDebug = false;\n[Debug 20140704°0827] Function FadeInFiles is starting.\n - Fadein max line length = " + Trekta.Fadin.Vars.iMaxCharPerLine + ".\n - Fadein number of elements = " + d + ".\n[" + (new Date).toString() + "]";
  Trekta.Fadin.outputz(Trekta.Fadin.Vars.s);
  if (1 > d) {
    Trekta.Fadin.outputz("FadeInFiles has nothing to do.");
  } else {
    if (Trekta.Utils.bShow_Debug_Dialogs) {
      f = [].slice.call(c);
      var b = "[Experiment 20141117°1341] [].slice.call() : ";
      for (h in f) {
        f.hasOwnProperty(h) && (b += " " + h);
      }
      Trekta.Fadin.outputz(b);
    }
    if (Trekta.Fadin.Vars.bDbg_20141117o0931_Curious_Elements) {
      var h = "[Debug 20141117°0931] FadeInFiles found curious initial elements list.\n      List length = " + (d + ". The forEach loop yields :");
      f = 1;
      for (var k in c) {
        if (c.hasOwnProperty(k)) {
          if (2 > f || f >= d) {
            h += "\n " + f + "  " + k.toString() + " = " + k.name;
          }
          h += ".";
          f++;
        }
      }
      Trekta.Fadin.outputz(h);
    }
    for (k = 0; k < d; k++) {
      h = c.item(k);
      f = c[k].getElementsByTagName("a")[0].getAttribute("href");
      b = c[k].getAttribute("data-fadein");
      var u = Trekta.Utils.CmdlinParser.parse(b);
      if (Trekta.Fadin.Vars.bDbg_20141118o0611_Gugg) {
        var x = h.parentNode, t = "[Gugg 20141118°0611] Target and parent nodes available.";
        t += "\n - Element " + (k + 1) + " of " + d + " :";
        t += "\n - commandline = '" + b + "'";
        t += "\n - target id = '" + h.id + "'";
        t += "\n - target tag = '" + h.tagName + "'";
        t += "\n - target class = '" + h.className + "'";
        t += "\n - target innerHTML = '" + h.innerHTML + "'";
        t += "\n - parent id = '" + x.id + "'";
        t += "\n - parent tag = '" + x.tagName + "'";
        t += "\n - parent class = '" + x.className + "'";
        Trekta.Fadin.outputz(t);
      }
      "pre" in u && (h = c[k].getElementsByTagName("pre")[0]);
      u["file-to-load"] = f;
      u["target-element"] = h;
      Trekta.Fadin.fillInFile_1_loadLibs(h, f, u);
    }
  }
};
Trekta.Fadin.fillInFile_1_loadLibs = function(c, d, f) {
  var b = "";
  "highlight-type" in f || (f["highlight-type"] = "");
  if ("" === f["highlight-type"]) {
    var h = /(?:\.([^.]+))?$/;
    if (Trekta.Utils.bToggle_FALSE) {
      var k = h.exec("file.name.with.dots.txt")[1], u = h.exec("file.txt")[1], x = h.exec("file")[1], t = h.exec("")[1], r = h.exec(null)[1], w = h.exec(void 0)[1];
      alert("Dbg 20190407°0112]" + k + " " + u + " " + x + " " + t + " " + r + " " + w + " ");
    }
    h = h.exec(d)[1];
    "bas" === h && (h = "basic");
    -1 < Trekta.Fadin.Vars.aHiliTypes.indexOf(h) && (b = h, f["highlight-type"] = b);
  }
  h = !1;
  f.markdown = !1;
  -1 !== d.indexOf(".md", d.length - 3) && (h = !0, f.markdown = !0);
  if ("undefined" === typeof hljs && "" !== b) {
    Trekta.Fadin.Vars.bConsole_FadeIn && console.log('[Dbg 20190407°0132] Load highlight.pack.js for "' + d + '"'), Trekta.Fadin.Vars.aFifoHili.push([d, c]), c = Trekta.Utils.retrieveDafBaseFolderAbs("/fadeinfiles.js"), Trekta.Utils.pullScriptBehind(c + "./highlight/highlight.pack.js", function() {
      Trekta.Fadin.fillInFile_2_libsLoaded(Trekta.Fadin.Vars.aFifoHili, f);
    }, null, null);
  } else {
    if (h && "undefined" === typeof showdown) {
      Trekta.Fadin.Vars.bConsole_FadeIn && console.log('[Dbg 20190405°0254] Load showdown.js for "' + d + '"');
      Trekta.Fadin.Vars.aFifoShdo.push([d, c]);
      c = Trekta.Utils.retrieveDafBaseFolderAbs("/fadeinfiles.js");
      d = Trekta.Utils.retrieveDafBaseFolderAbs("/fadeinfiles.combi.js");
      var B = ("" !== c ? c : d) + "/showdown/showdown.min.js";
      Trekta.Utils.pullScriptBehind(B, function() {
        Trekta.Fadin.fillInFile_2_libsLoaded(Trekta.Fadin.Vars.aFifoShdo, f);
      }, function() {
        alert("Failed to load " + B);
      }, null);
    } else {
      Trekta.Fadin.fillInFile_3_continue(d, c, f);
    }
  }
};
Trekta.Fadin.fillInFile_2_libsLoaded = function(c, d) {
  for (var f; 0 < c.length;) {
    f = c.shift(), Trekta.Fadin.fillInFile_3_continue(f[0], f[1], d);
  }
};
Trekta.Fadin.fillInFile_3_continue = function(c, d, f) {
  var b = null;
  try {
    Trekta.Fadin.Vars.iReadFileNo += 1, b = Trekta.Fadin.Vars.iReadFileNo, Trekta.Fadin.Vars.bConsole_FadeIn && console.log("[Dbg 20190405°0255] Reading file no " + b + ' "' + c + '"'), Trekta.Utils.readTextFile2(c, function(h) {
      Trekta.Fadin.fillInFile_4_finish(d, f, b, h, c);
    });
  } catch (h) {
    Trekta.Fadin.fillInFile_4_finish(d, !1, b, h.message, c);
  }
};
Trekta.Fadin.fillInFile_4_finish = function(c, d, f, b, h) {
  Trekta.Fadin.Vars.bConsole_FadeIn && console.log("[Dbg 20190405°0256] Continue file no " + f);
  !0 === d.markdown ? (b = Trekta.Fadin.convertMarkdown2Html(b), b = Trekta.Fadin.mdFileRelativeLinks(h, b)) : (b = Trekta.Utils.htmlEscape(b), b = Trekta.Fadin.breakLongLines(b));
  if ("undefined" === typeof c) {
    Trekta.Fadin.outputz("[Error 20141118°0851] Fatal — Target element undefined.");
  } else {
    var k = c.parentNode, u = c.nextSibling;
    "undefined" === typeof k && alert("[Error 20141118°0852] Fatal — Parent element undefined.");
    var x = k.innerHTML;
    64 < x.length && (x = x.substr(0, 64) + "...");
    if (Trekta.Fadin.Vars.bDbg_20141118o0921_Target_Element) {
      var t = "Target element :\nclassName = '" + (k.className + "'");
      t += "\nid = '" + k.id + "'";
      t += "\ntagName = '" + k.tagName + "'";
      Trekta.Fadin.outputz("[Msg 20141118°0921] " + (t + ("\ninnerHTML : " + x)));
    }
    !0 === d.markdown ? (x = document.createElement("div"), x.className = "FadeinMarkDown", x.style.border = "1.0px solid LightSlateGray", x.style.borderRadius = "0.7em", x.style.padding = "0 0.7em 0 0.7em") : (x = document.createElement("pre"), x.className = "CodeSmall");
    2 > b.length && (b = '<span style="font-size:small;">⚡ Sorry, JavaScript cannot fade-in file <a href="' + h + '">' + h + '</a></span> <span style="font-size:xx-small;">(file ' + f + ')</span>.<br><span style="font-size:x-small;">Problem: Some browsers refuse AJAX requests via <b>file protocol</b> (e.g. <code>file:///D:/page.html</code>).<br>Solution: View this page via a server (e.g. <code>http://localhost/page.html</code>).</span> <span style="font-size:xx-small;">[Error 20141118°0853]</span>', 
    x.style.lineHeight = "1.2em", x.style.padding = "0.4em 0 0 0.4em");
    x.setAttribute("id", "oha");
    x.innerHTML = b;
    x.setAttribute("data-wafailiskip", "yes");
    f = "P TD LI H1 H2 H3 H4 H5 H6 PRE".split(" ");
    for (h = 0; h < f.length; h++) {
      t = x.getElementsByTagName(f[h]);
      for (var r = 0; r < t.length; r++) {
        t[r].setAttribute("data-wafailiskip", "yes");
      }
    }
    "PRE" === c.tagName ? (c.classList.add("CodeSmall"), c.innerHTML = b, c.setAttribute("data-wafailiskip", "yes")) : "undefined" === typeof k ? c.appendChild(x) : (k.insertBefore(x, u), c = x);
    "" !== d["highlight-type"] && (hljs.configure({languages:[d["highlight-type"]]}), hljs.highlightElement(c));
  }
};
Trekta.Fadin.mdFileRelativeLinks = function(c, d) {
  var f = c.match(/^.\/(.*)\/.*/);
  f = null === f ? "" : f[f.length - 1];
  Trekta.Fadin.Vars.bDbg_20210515o1423_Relative_Path && Trekta.Fadin.outputz('[msg 20210515°1423`01`] Relation "' + c + '" — "' + f + '"');
  c = /<a href=".\/|<img src=".\//g;
  "" !== f && (d = d.replaceAll(c, "$&" + f + "/"));
  return d;
};
Trekta.Fadin.outputz = function(c) {
  if (Trekta.Fadin.Vars.bOutpuzDebug) {
    "undefined" === typeof c && (c = "<i>Test2 Einfügung</i>");
    if (null === Trekta.Fadin.Vars.eOutBox) {
      var d = null, f = document.getElementsByClassName("footerReferer");
      0 < f.length && (d = f[0]);
      Trekta.Fadin.Vars.eOutBox = document.createElement("div");
      Trekta.Fadin.Vars.eOutBox.style.cssText = "border:2px solid Red; display:table;";
      null === d ? document.body.appendChild(Trekta.Fadin.Vars.eOutBox) : d.parentNode.insertBefore(Trekta.Fadin.Vars.eOutBox, d.nextSibling);
    }
    d = document.createElement("pre");
    d.className = "codeTiny";
    d.style.backgroundColor = "PapayaWhip";
    d.style.margin = "0.6em";
    Trekta.Fadin.Vars.eOutCount++;
    c = document.createTextNode("(" + Trekta.Fadin.Vars.eOutCount + ") " + c);
    d.appendChild(c);
    Trekta.Fadin.Vars.eOutBox.appendChild(d);
  }
};
Trekta.Fadin.Vars.aHiliTypes = "Apache;apache;apacheconf;Bash;bash;sh;zsh;Basic;basic;C#;cs;csharp;C;c;h;C++;cpp;hpp;cc;hh;c++;h++;cxx;hxx;CSS;css;CoffeeScript;coffeescript;coffee;cson;iced;DOS;dos;bat;cmd;Diff;diff;patch;Erlang;erlang;erl;go;go;golang;HTML, XML;xml;html;xhtml;rss;atom;xjb;xsd;xsl;plist;svg;HTTP;http;https;Ini, TOML;ini;toml;JSON;json;Java;java;jsp;JavaScript;javascript;js;jsx;Kotlin;kotlin;kt;Less;less;Lisp;lisp;Lua;lua;Makefile;makefile;mk;mak;make;Nginx;nginx;nginxconf;Objective C;objectivec;mm;objc;obj-c;obj-c++;objective-c++;PHP;php;php3;php4;php5;php6;php7;php8;Perl;perl;pl;pm;Plaintext;plaintext;txt;text;PowerShell;powershell;ps;ps1;Properties;properties;Python;python;py;gyp;Python REPL;python-repl;pycon;R;r;Ruby;ruby;rb;gemspec;podspec;thor;irb;Rust;rust;rs;SCSS;scss;SQL;sql;Scheme;scheme;Shell;shell;console;Swift;swift;TypeScript;typescript;ts;VB.Net;vbnet;vb;x86 Assembly;x86asm;YAML;yml;yaml".split(";");
Trekta = Trekta || {};
Trekta.Utils = Trekta.Utils || {getCookie:function(c) {
  var d, f = "";
  if (0 < document.cookie.length) {
    var b = document.cookie.split(";");
    for (d = 0; d < b.length; d++) {
      var h = b[d].substr(0, b[d].indexOf("="));
      var k = b[d].substr(b[d].indexOf("=") + 1);
      h = h.replace(/^\s+|\s+$/g, "");
      if (h === c) {
        f = decodeURI(k);
        break;
      }
    }
  } else {
    f = localStorage.getItem(c);
  }
  return f;
}, getCookieBool:function(c, d) {
  d = d || !1;
  c = Trekta.Utils.getCookie(c);
  return "true" === c ? !0 : "false" === c ? !1 : d;
}, getCookieInt:function(c, d) {
  d = d || 0;
  c = Trekta.Utils.getCookie(c);
  "" !== c && (d = parseInt(c, 10));
  return d;
}, getFileNameFull:function() {
  var c = document.location.href;
  c = c.substring(0, -1 === c.indexOf("?") ? c.length : c.indexOf("?"));
  c = c.substring(0, -1 === c.indexOf("#") ? c.length : c.indexOf("#"));
  -1 !== c.indexOf("/", c.length - 1) && (c += "index.html");
  return c;
}, getFilenamePlain:function() {
  var c = Trekta.Utils.getFileNameFull();
  c = c.split("/");
  return c = c[c.length - 1];
}, htmlEscape:function(c) {
  c = c.replace(/</g, "&lt;");
  return c = c.replace(/>/g, "&gt;");
}, isScriptAlreadyLoaded:function(c) {
  c = c.replace(/\./g, "\\.");
  c = new RegExp(c + "$", "");
  var d = document.getElementsByTagName("SCRIPT");
  if (d && 0 < d.length) {
    for (var f in d) {
      var b = Number.parseInt(f, 10);
      if (d[b] && d[b].src.match(c)) {
        return !0;
      }
    }
  }
  return !1;
}, outDbgMsg:function(c) {
  if (Trekta.Utils.getCookieBool("checkbox_yellowdebugpane", null)) {
    if ("complete" !== document.readyState) {
      Trekta.Utils.InitialMessageCache.push(c);
    } else {
      Trekta.Utils.outDbgMsg_GuaranteeParentElement();
      Date.now || (Date.now = function() {
        return (new Date).getTime();
      });
      var d = Math.floor(Date.now() / 1000);
      d = d.toString();
      if (0 < Trekta.Utils.InitialMessageCache.length) {
        for (var f = "", b = Trekta.Utils.InitialMessageCache.length - 1; 0 <= b; b--) {
          "" !== f && (f = "\n\n" + f), f = Trekta.Utils.InitialMessageCache[b] + f;
        }
        Trekta.Utils.InitialMessageCache.length = 0;
        c = '<div style="background-color:LightGreen; margin:1.1em; padding:0.7em;">' + f + "</div>\n\n" + c;
      }
      c = c.replace(/\n/g, "<br />");
      d = '<p><span style="font-size:72%;">' + (d + "</span> ") + c + "</p>";
      document.getElementById(Daf.Dspat.Config.sFurniture_OutputArea_Id).insertAdjacentHTML("beforeend", "\n" + d);
    }
  }
}, outDbgMsg_GuaranteeParentElement:function() {
  var c = Daf.Dspat.Config.sFurniture_OutputArea_Id, d = document.getElementById(c);
  if (!d) {
    d = '<div id="' + c + '" class="dafBoxDebugOutput"><p>[Msg 20150325°0211] Loading dafmenu.js (1/x).\n<br />&nbsp; Here comes the yellow Standard Output Box. Some page values are :' + Daf.Mnu.Jsi.getJsiIntroDebugMessage(!0) + "</p></div>";
    var f = document.getElementsByTagName("body")[0], b = document.createElement("div");
    b.innerHTML = d;
    f.appendChild(b);
    d = document.getElementById(c);
  }
  return d;
}, pullScriptBehind:function(c, d, f, b) {
  if (0 <= Trekta.Utils.aPulled.indexOf(c)) {
    d(c, b, !0);
  } else {
    var h = document.getElementsByTagName("head")[0], k = document.createElement("script");
    k.type = "text/javascript";
    k.src = c;
    if ("undefined" !== typeof d) {
      var u = function() {
        d(c, b, !1);
      };
      k.onload = function() {
        Trekta.Utils.pullScript_onload(c, u);
      };
    }
    f = f || null;
    null !== f && (k.onerror = function() {
      f(c, b);
    });
    h.appendChild(k);
    return !0;
  }
}, pullScript_onload:function(c, d) {
  Trekta.Utils.aPulled.push(c);
  d();
}, readTextFile2:function(c, d, f) {
  Trekta.Utils.ajax3Send("GET", c, "", d, f);
}, ajax3Send:function(c, d, f, b, h) {
  b = "undefined" === typeof b ? null : b;
  h = "undefined" === typeof h ? null : h;
  var k = new XMLHttpRequest;
  k.open(c, d, !0);
  k.onreadystatechange = function() {
    if (0 !== k.readyState && 1 !== k.readyState && 2 !== k.readyState && 3 !== k.readyState && 4 === k.readyState) {
      var u = !1;
      switch(k.status) {
        case 0:
          u = !0;
          break;
        case 200:
          u = !0;
      }
      u ? b(k.responseText) : h(k.responseText);
    }
  };
  try {
    k.send(null);
  } catch (u) {
    c = "<b>Sorry, some feature on this page does not work.</b>\nFile <tt>" + d + "</tt> ~~could not be read.\nYour browser said: <tt>" + u.message + "</tt>.", c = Trekta.Utils.bIs_Browser_Chrome && "file:" === location.protocol ? c + "\nYour browser seems to be Chrome, and this does not ~~read files via file protocol.\nThere are two <b>solutions</b>: (1) Use a different browser, e.g. Firefox or IE\nor (2) view this page from <tt>localhost</tt> with a HTTP server." : Trekta.Utils.bIs_Browser_Firefox && 
    "file:" === location.protocol ? c + "\nYour browser seems to be <b>Firefox</b>, and this does not ~~read files\nwith a path going below the current directory via file protocol.\nThere are two <b>solutions</b>: (1) Use a different browser, e.g. Chrome or IE\nor (2)  view this page from <tt>localhost</tt> with a HTTP server." : c + ("\n [info 20160622°0131] Failed sending request " + d + "."), b(c);
  }
}, retrieveDafBaseFolderAbs:function(c) {
  var d = c.replace(/\./g, "\\.") + "$";
  c = new RegExp(d, "");
  d = new RegExp("(.*)" + d, "");
  var f = "", b = document.getElementsByTagName("SCRIPT");
  if (b && 0 < b.length) {
    for (var h in b) {
      var k = Number.parseInt(h, 10);
      b[k] && b[k].src.match(c) && (f = b[k].src.replace(d, "$1"));
    }
  }
  return f;
}, sConfine:function(c, d) {
  var f = "";
  c.length > d && (f = c.substring(0, d / 2) + " … " + c.substring(c.length - d / 2));
  f = f.split("\n").join("☼");
  f = f.split("<").join("&lt;");
  return f = f.split(">").join("&gt;");
}, setCookie:function(c, d, f) {
  var b = new Date;
  b.setDate(b.getDate() + f);
  f = encodeURI(d) + (null === f ? "" : "; Expires = " + b.toUTCString());
  document.cookie = c + "=" + f + "; path=/";
  1 > document.cookie.length && localStorage.setItem(c, d);
}, windowOnloadDaisychain:function(c) {
  if (window.onload) {
    var d = window.onload;
    window.onload = function() {
      d(null);
      c();
    };
  } else {
    window.onload = function() {
      c();
    };
  }
}, InitialMessageCache:[], aPulled:[], bIs_Browser_Chrome:navigator.userAgent.match(/Chrome/) ? !0 : !1, bIs_Browser_Edge:navigator.userAgent.match(/Edge/) ? !0 : !1, bIs_Browser_Explorer:navigator.appName.match(/Explorer/) || window.msCrypto ? !0 : !1, bIs_Browser_Firefox:navigator.userAgent.match(/Firefox/) ? !0 : !1, bIs_Browser_Opera:navigator.userAgent.match(/(Opera)|(OPR)/) ? !0 : !1, bShow_Debug_Dialogs:!1, bToggle_FALSE:!1, bToggle_TRUE:!0, bUseMinified:!1, sFurniture_OutputArea_Id:"i20150321o0231_StandardOutputDiv", 
s_DaftariBaseFolderAbs:"", s_DaftariBaseFolderRel:""};
Trekta.Utils.CmdlinParser = function() {
  Trekta.Utils.parse = function(c) {
    "undefined" === typeof c && (c = "");
    for (var d = [], f = "", b = "", h = 0; h < c.length; h++) {
      var k = c.charAt(h);
      " " === k && "" === f ? "" !== b && (d.push(b), b = "") : "=" === k && "" === f ? ("" !== b && (d.push(b), b = ""), d.push("=")) : "'" === k || '"' === k ? f = "" === f ? k : "" : b += k;
    }
    d.push(b);
    c = [];
    for (b = 0; b < d.length; b++) {
      "" !== d[b] && (f = d[b], c[f] = "<n/a>", "=" === d[b + 1] && (c[f] = d[b + 2], b++, b++));
    }
    return c;
  };
  return {parse:Trekta.Utils.parse};
}();
Trekta.Utils.windowOnloadDaisychain(Trekta.Fadin.fadeInFiles);
/*
 showdown v 1.9.1 - 02-11-2019 */
(function() {
  function c(a) {
    var g = {omitExtraWLInCodeBlocks:{defaultValue:!1, describe:"Omit the default extra whiteline added to code blocks", type:"boolean"}, noHeaderId:{defaultValue:!1, describe:"Turn on/off generated header id", type:"boolean"}, prefixHeaderId:{defaultValue:!1, describe:"Add a prefix to the generated header ids. Passing a string will prefix that string to the header id. Setting to true will add a generic 'section-' prefix", type:"string"}, rawPrefixHeaderId:{defaultValue:!1, describe:'Setting this option to true will prevent showdown from modifying the prefix. This might result in malformed IDs (if, for instance, the " char is used in the prefix)', 
    type:"boolean"}, ghCompatibleHeaderId:{defaultValue:!1, describe:"Generate header ids compatible with github style (spaces are replaced with dashes, a bunch of non alphanumeric chars are removed)", type:"boolean"}, rawHeaderId:{defaultValue:!1, describe:"Remove only spaces, ' and \" from generated header ids (including prefixes), replacing them with dashes (-). WARNING: This might result in malformed ids", type:"boolean"}, headerLevelStart:{defaultValue:!1, describe:"The header blocks level start", 
    type:"integer"}, parseImgDimensions:{defaultValue:!1, describe:"Turn on/off image dimension parsing", type:"boolean"}, simplifiedAutoLink:{defaultValue:!1, describe:"Turn on/off GFM autolink style", type:"boolean"}, excludeTrailingPunctuationFromURLs:{defaultValue:!1, describe:"Excludes trailing punctuation from links generated with autoLinking", type:"boolean"}, literalMidWordUnderscores:{defaultValue:!1, describe:"Parse midword underscores as literal underscores", type:"boolean"}, literalMidWordAsterisks:{defaultValue:!1, 
    describe:"Parse midword asterisks as literal asterisks", type:"boolean"}, strikethrough:{defaultValue:!1, describe:"Turn on/off strikethrough support", type:"boolean"}, tables:{defaultValue:!1, describe:"Turn on/off tables support", type:"boolean"}, tablesHeaderId:{defaultValue:!1, describe:"Add an id to table headers", type:"boolean"}, ghCodeBlocks:{defaultValue:!0, describe:"Turn on/off GFM fenced code blocks support", type:"boolean"}, tasklists:{defaultValue:!1, describe:"Turn on/off GFM tasklist support", 
    type:"boolean"}, smoothLivePreview:{defaultValue:!1, describe:"Prevents weird effects in live previews due to incomplete input", type:"boolean"}, smartIndentationFix:{defaultValue:!1, description:"Tries to smartly fix indentation in es6 strings", type:"boolean"}, disableForced4SpacesIndentedSublists:{defaultValue:!1, description:"Disables the requirement of indenting nested sublists by 4 spaces", type:"boolean"}, simpleLineBreaks:{defaultValue:!1, description:"Parses simple line breaks as <br> (GFM Style)", 
    type:"boolean"}, requireSpaceBeforeHeadingText:{defaultValue:!1, description:"Makes adding a space between `#` and the header text mandatory (GFM Style)", type:"boolean"}, ghMentions:{defaultValue:!1, description:"Enables github @mentions", type:"boolean"}, ghMentionsLink:{defaultValue:"https://github.com/{u}", description:"Changes the link generated by @mentions. Only applies if ghMentions option is enabled.", type:"string"}, encodeEmails:{defaultValue:!0, description:"Encode e-mail addresses through the use of Character Entities, transforming ASCII e-mail addresses into its equivalent decimal entities", 
    type:"boolean"}, openLinksInNewWindow:{defaultValue:!1, description:"Open all links in new windows", type:"boolean"}, backslashEscapesHTMLTags:{defaultValue:!1, description:"Support for HTML Tag escaping. ex: <div>foo</div>", type:"boolean"}, emoji:{defaultValue:!1, description:"Enable emoji support. Ex: `this is a :smile: emoji`", type:"boolean"}, underline:{defaultValue:!1, description:"Enable support for underline. Syntax is double or triple underscores: `__underline word__`. With this option enabled, underscores no longer parses into `<em>` and `<strong>`", 
    type:"boolean"}, completeHTMLDocument:{defaultValue:!1, description:"Outputs a complete html document, including `<html>`, `<head>` and `<body>` tags", type:"boolean"}, metadata:{defaultValue:!1, description:"Enable support for document metadata (defined at the top of the document between `«««` and `»»»` or between `---` and `---`).", type:"boolean"}, splitAdjacentBlockquotes:{defaultValue:!1, description:"Split adjacent blockquote blocks", type:"boolean"}};
    if (!1 === a) {
      return JSON.parse(JSON.stringify(g));
    }
    a = {};
    for (var e in g) {
      g.hasOwnProperty(e) && (a[e] = g[e].defaultValue);
    }
    return a;
  }
  function d(a, g) {
    g = g ? "Error in " + g + " extension->" : "Error in unnamed extension";
    var e = {valid:!0, error:""};
    b.helper.isArray(a) || (a = [a]);
    for (var l = 0; l < a.length; ++l) {
      var m = g + " sub-extension " + l + ": ", v = a[l];
      if ("object" !== typeof v) {
        e.valid = !1;
        e.error = m + "must be an object, but " + typeof v + " given";
        break;
      }
      if (!b.helper.isString(v.type)) {
        e.valid = !1;
        e.error = m + 'property "type" must be a string, but ' + typeof v.type + " given";
        break;
      }
      var n = v.type = v.type.toLowerCase();
      "language" === n && (n = v.type = "lang");
      "html" === n && (n = v.type = "output");
      if ("lang" !== n && "output" !== n && "listener" !== n) {
        e.valid = !1;
        e.error = m + "type " + n + ' is not recognized. Valid values: "lang/language", "output/html" or "listener"';
        break;
      }
      if ("listener" === n) {
        if (b.helper.isUndefined(v.listeners)) {
          e.valid = !1;
          e.error = m + '. Extensions of type "listener" must have a property called "listeners"';
          break;
        }
      } else {
        if (b.helper.isUndefined(v.filter) && b.helper.isUndefined(v.regex)) {
          e.valid = !1;
          e.error = m + n + ' extensions must define either a "regex" property or a "filter" method';
          break;
        }
      }
      if (v.listeners) {
        if ("object" !== typeof v.listeners) {
          e.valid = !1;
          e.error = m + '"listeners" property must be an object but ' + typeof v.listeners + " given";
          break;
        }
        for (var y in v.listeners) {
          if (v.listeners.hasOwnProperty(y) && "function" !== typeof v.listeners[y]) {
            return e.valid = !1, e.error = m + '"listeners" property must be an hash of [event name]: [callback]. listeners.' + y + " must be a function but " + typeof v.listeners[y] + " given", e;
          }
        }
      }
      if (v.filter) {
        if ("function" !== typeof v.filter) {
          e.valid = !1;
          e.error = m + '"filter" must be a function, but ' + typeof v.filter + " given";
          break;
        }
      } else {
        if (v.regex) {
          b.helper.isString(v.regex) && (v.regex = new RegExp(v.regex, "g"));
          if (!(v.regex instanceof RegExp)) {
            e.valid = !1;
            e.error = m + '"regex" property must either be a string or a RegExp object, but ' + typeof v.regex + " given";
            break;
          }
          if (b.helper.isUndefined(v.replace)) {
            e.valid = !1;
            e.error = m + '"regex" extensions must implement a replace string or function';
            break;
          }
        }
      }
    }
    return e;
  }
  function f(a, g) {
    return "¨E" + g.charCodeAt(0) + "E";
  }
  var b = {}, h = {}, k = {}, u = c(!0), x = "vanilla", t = {github:{omitExtraWLInCodeBlocks:!0, simplifiedAutoLink:!0, excludeTrailingPunctuationFromURLs:!0, literalMidWordUnderscores:!0, strikethrough:!0, tables:!0, tablesHeaderId:!0, ghCodeBlocks:!0, tasklists:!0, disableForced4SpacesIndentedSublists:!0, simpleLineBreaks:!0, requireSpaceBeforeHeadingText:!0, ghCompatibleHeaderId:!0, ghMentions:!0, backslashEscapesHTMLTags:!0, emoji:!0, splitAdjacentBlockquotes:!0}, original:{noHeaderId:!0, ghCodeBlocks:!1}, 
  ghost:{omitExtraWLInCodeBlocks:!0, parseImgDimensions:!0, simplifiedAutoLink:!0, excludeTrailingPunctuationFromURLs:!0, literalMidWordUnderscores:!0, strikethrough:!0, tables:!0, tablesHeaderId:!0, ghCodeBlocks:!0, tasklists:!0, smoothLivePreview:!0, simpleLineBreaks:!0, requireSpaceBeforeHeadingText:!0, ghMentions:!1, encodeEmails:!0}, vanilla:c(!0), allOn:function() {
    var a = c(!0), g = {}, e;
    for (e in a) {
      a.hasOwnProperty(e) && (g[e] = !0);
    }
    return g;
  }()};
  b.helper = {};
  b.extensions = {};
  b.setOption = function(a, g) {
    u[a] = g;
    return this;
  };
  b.getOption = function(a) {
    return u[a];
  };
  b.getOptions = function() {
    return u;
  };
  b.resetOptions = function() {
    u = c(!0);
  };
  b.setFlavor = function(a) {
    if (!t.hasOwnProperty(a)) {
      throw Error(a + " flavor was not found");
    }
    b.resetOptions();
    var g = t[a];
    x = a;
    for (var e in g) {
      g.hasOwnProperty(e) && (u[e] = g[e]);
    }
  };
  b.getFlavor = function() {
    return x;
  };
  b.getFlavorOptions = function(a) {
    if (t.hasOwnProperty(a)) {
      return t[a];
    }
  };
  b.getDefaultOptions = function(a) {
    return c(a);
  };
  b.subParser = function(a, g) {
    if (b.helper.isString(a)) {
      if ("undefined" !== typeof g) {
        h[a] = g;
      } else {
        if (h.hasOwnProperty(a)) {
          return h[a];
        }
        throw Error("SubParser named " + a + " not registered!");
      }
    }
  };
  b.extension = function(a, g) {
    if (!b.helper.isString(a)) {
      throw Error("Extension 'name' must be a string");
    }
    a = b.helper.stdExtName(a);
    if (b.helper.isUndefined(g)) {
      if (!k.hasOwnProperty(a)) {
        throw Error("Extension named " + a + " is not registered!");
      }
      return k[a];
    }
    "function" === typeof g && (g = g());
    b.helper.isArray(g) || (g = [g]);
    var e = d(g, a);
    if (e.valid) {
      k[a] = g;
    } else {
      throw Error(e.error);
    }
  };
  b.getAllExtensions = function() {
    return k;
  };
  b.removeExtension = function(a) {
    delete k[a];
  };
  b.resetExtensions = function() {
    k = {};
  };
  b.validateExtension = function(a) {
    a = d(a, null);
    return a.valid ? !0 : (console.warn(a.error), !1);
  };
  b.hasOwnProperty("helper") || (b.helper = {});
  b.helper.isString = function(a) {
    return "string" === typeof a || a instanceof String;
  };
  b.helper.isFunction = function(a) {
    var g = {};
    return a && "[object Function]" === g.toString.call(a);
  };
  b.helper.isArray = function(a) {
    return Array.isArray(a);
  };
  b.helper.isUndefined = function(a) {
    return "undefined" === typeof a;
  };
  b.helper.forEach = function(a, g) {
    if (b.helper.isUndefined(a)) {
      throw Error("obj param is required");
    }
    if (b.helper.isUndefined(g)) {
      throw Error("callback param is required");
    }
    if (!b.helper.isFunction(g)) {
      throw Error("callback param must be a function/closure");
    }
    if ("function" === typeof a.forEach) {
      a.forEach(g);
    } else {
      if (b.helper.isArray(a)) {
        for (var e = 0; e < a.length; e++) {
          g(a[e], e, a);
        }
      } else {
        if ("object" === typeof a) {
          for (e in a) {
            a.hasOwnProperty(e) && g(a[e], e, a);
          }
        } else {
          throw Error("obj does not seem to be an array or an iterable object");
        }
      }
    }
  };
  b.helper.stdExtName = function(a) {
    return a.replace(/[_?*+\/\\.^-]/g, "").replace(/\s/g, "").toLowerCase();
  };
  b.helper.escapeCharactersCallback = f;
  b.helper.escapeCharacters = function(a, g, e) {
    g = "([" + g.replace(/([\[\]\\])/g, "\\$1") + "])";
    e && (g = "\\\\" + g);
    return a = a.replace(new RegExp(g, "g"), f);
  };
  b.helper.unescapeHTMLEntities = function(a) {
    return a.replace(/&quot;/g, '"').replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&");
  };
  var r = function(a, g, e, l) {
    var m = l || "";
    l = -1 < m.indexOf("g");
    e = new RegExp(g + "|" + e, "g" + m.replace(/g/g, ""));
    g = new RegExp(g, m.replace(/g/g, ""));
    m = [];
    var v, n;
    do {
      for (v = 0; n = e.exec(a);) {
        if (g.test(n[0])) {
          if (!v++) {
            var y = e.lastIndex;
            var D = y - n[0].length;
          }
        } else {
          if (v && !--v) {
            var C = n.index + n[0].length;
            m.push({left:{start:D, end:y}, match:{start:y, end:n.index}, right:{start:n.index, end:C}, wholeMatch:{start:D, end:C}});
            if (!l) {
              return m;
            }
          }
        }
      }
    } while (v && (e.lastIndex = y));
    return m;
  };
  b.helper.matchRecursiveRegExp = function(a, g, e, l) {
    g = r(a, g, e, l);
    e = [];
    for (l = 0; l < g.length; ++l) {
      e.push([a.slice(g[l].wholeMatch.start, g[l].wholeMatch.end), a.slice(g[l].match.start, g[l].match.end), a.slice(g[l].left.start, g[l].left.end), a.slice(g[l].right.start, g[l].right.end)]);
    }
    return e;
  };
  b.helper.replaceRecursiveRegExp = function(a, g, e, l, m) {
    if (!b.helper.isFunction(g)) {
      var v = g;
      g = function() {
        return v;
      };
    }
    e = r(a, e, l, m);
    m = a;
    l = e.length;
    if (0 < l) {
      m = [];
      0 !== e[0].wholeMatch.start && m.push(a.slice(0, e[0].wholeMatch.start));
      for (var n = 0; n < l; ++n) {
        m.push(g(a.slice(e[n].wholeMatch.start, e[n].wholeMatch.end), a.slice(e[n].match.start, e[n].match.end), a.slice(e[n].left.start, e[n].left.end), a.slice(e[n].right.start, e[n].right.end))), n < l - 1 && m.push(a.slice(e[n].wholeMatch.end, e[n + 1].wholeMatch.start));
      }
      e[l - 1].wholeMatch.end < a.length && m.push(a.slice(e[l - 1].wholeMatch.end));
      m = m.join("");
    }
    return m;
  };
  b.helper.regexIndexOf = function(a, g, e) {
    if (!b.helper.isString(a)) {
      throw "InvalidArgumentError: first parameter of showdown.helper.regexIndexOf function must be a string";
    }
    if (!1 === g instanceof RegExp) {
      throw "InvalidArgumentError: second parameter of showdown.helper.regexIndexOf function must be an instance of RegExp";
    }
    a = a.substring(e || 0).search(g);
    return 0 <= a ? a + (e || 0) : a;
  };
  b.helper.splitAtIndex = function(a, g) {
    if (!b.helper.isString(a)) {
      throw "InvalidArgumentError: first parameter of showdown.helper.regexIndexOf function must be a string";
    }
    return [a.substring(0, g), a.substring(g)];
  };
  b.helper.encodeEmailAddress = function(a) {
    var g = [function(e) {
      return "&#" + e.charCodeAt(0) + ";";
    }, function(e) {
      return "&#x" + e.charCodeAt(0).toString(16) + ";";
    }, function(e) {
      return e;
    }];
    return a = a.replace(/./g, function(e) {
      if ("@" === e) {
        e = g[Math.floor(2 * Math.random())](e);
      } else {
        var l = Math.random();
        e = 0.9 < l ? g[2](e) : 0.45 < l ? g[1](e) : g[0](e);
      }
      return e;
    });
  };
  b.helper.padEnd = function(a, g, e) {
    g >>= 0;
    e = String(e || " ");
    if (a.length > g) {
      return String(a);
    }
    g -= a.length;
    g > e.length && (e += e.repeat(g / e.length));
    return String(a) + e.slice(0, g);
  };
  "undefined" === typeof console && (console = {warn:function(a) {
    alert(a);
  }, log:function(a) {
    alert(a);
  }, error:function(a) {
    throw a;
  }});
  b.helper.regexes = {asteriskDashAndColon:/([*_:~])/g};
  b.helper.emojis = {"+1":"\ud83d\udc4d", "-1":"\ud83d\udc4e", 100:"\ud83d\udcaf", 1234:"\ud83d\udd22", "1st_place_medal":"\ud83e\udd47", "2nd_place_medal":"\ud83e\udd48", "3rd_place_medal":"\ud83e\udd49", "8ball":"\ud83c\udfb1", a:"\ud83c\udd70️", ab:"\ud83c\udd8e", abc:"\ud83d\udd24", abcd:"\ud83d\udd21", accept:"\ud83c\ude51", aerial_tramway:"\ud83d\udea1", airplane:"✈️", alarm_clock:"⏰", alembic:"⚗️", alien:"\ud83d\udc7d", ambulance:"\ud83d\ude91", amphora:"\ud83c\udffa", anchor:"⚓️", angel:"\ud83d\udc7c", 
  anger:"\ud83d\udca2", angry:"\ud83d\ude20", anguished:"\ud83d\ude27", ant:"\ud83d\udc1c", apple:"\ud83c\udf4e", aquarius:"♒️", aries:"♈️", arrow_backward:"◀️", arrow_double_down:"⏬", arrow_double_up:"⏫", arrow_down:"⬇️", arrow_down_small:"\ud83d\udd3d", arrow_forward:"▶️", arrow_heading_down:"⤵️", arrow_heading_up:"⤴️", arrow_left:"⬅️", arrow_lower_left:"↙️", arrow_lower_right:"↘️", arrow_right:"➡️", arrow_right_hook:"↪️", arrow_up:"⬆️", arrow_up_down:"↕️", arrow_up_small:"\ud83d\udd3c", arrow_upper_left:"↖️", 
  arrow_upper_right:"↗️", arrows_clockwise:"\ud83d\udd03", arrows_counterclockwise:"\ud83d\udd04", art:"\ud83c\udfa8", articulated_lorry:"\ud83d\ude9b", artificial_satellite:"\ud83d\udef0", astonished:"\ud83d\ude32", athletic_shoe:"\ud83d\udc5f", atm:"\ud83c\udfe7", atom_symbol:"⚛️", avocado:"\ud83e\udd51", b:"\ud83c\udd71️", baby:"\ud83d\udc76", baby_bottle:"\ud83c\udf7c", baby_chick:"\ud83d\udc24", baby_symbol:"\ud83d\udebc", back:"\ud83d\udd19", bacon:"\ud83e\udd53", badminton:"\ud83c\udff8", 
  baggage_claim:"\ud83d\udec4", baguette_bread:"\ud83e\udd56", balance_scale:"⚖️", balloon:"\ud83c\udf88", ballot_box:"\ud83d\uddf3", ballot_box_with_check:"☑️", bamboo:"\ud83c\udf8d", banana:"\ud83c\udf4c", bangbang:"‼️", bank:"\ud83c\udfe6", bar_chart:"\ud83d\udcca", barber:"\ud83d\udc88", baseball:"⚾️", basketball:"\ud83c\udfc0", basketball_man:"⛹️", basketball_woman:"⛹️&zwj;♀️", bat:"\ud83e\udd87", bath:"\ud83d\udec0", bathtub:"\ud83d\udec1", battery:"\ud83d\udd0b", beach_umbrella:"\ud83c\udfd6", 
  bear:"\ud83d\udc3b", bed:"\ud83d\udecf", bee:"\ud83d\udc1d", beer:"\ud83c\udf7a", beers:"\ud83c\udf7b", beetle:"\ud83d\udc1e", beginner:"\ud83d\udd30", bell:"\ud83d\udd14", bellhop_bell:"\ud83d\udece", bento:"\ud83c\udf71", biking_man:"\ud83d\udeb4", bike:"\ud83d\udeb2", biking_woman:"\ud83d\udeb4&zwj;♀️", bikini:"\ud83d\udc59", biohazard:"☣️", bird:"\ud83d\udc26", birthday:"\ud83c\udf82", black_circle:"⚫️", black_flag:"\ud83c\udff4", black_heart:"\ud83d\udda4", black_joker:"\ud83c\udccf", black_large_square:"⬛️", 
  black_medium_small_square:"◾️", black_medium_square:"◼️", black_nib:"✒️", black_small_square:"▪️", black_square_button:"\ud83d\udd32", blonde_man:"\ud83d\udc71", blonde_woman:"\ud83d\udc71&zwj;♀️", blossom:"\ud83c\udf3c", blowfish:"\ud83d\udc21", blue_book:"\ud83d\udcd8", blue_car:"\ud83d\ude99", blue_heart:"\ud83d\udc99", blush:"\ud83d\ude0a", boar:"\ud83d\udc17", boat:"⛵️", bomb:"\ud83d\udca3", book:"\ud83d\udcd6", bookmark:"\ud83d\udd16", bookmark_tabs:"\ud83d\udcd1", books:"\ud83d\udcda", boom:"\ud83d\udca5", 
  boot:"\ud83d\udc62", bouquet:"\ud83d\udc90", bowing_man:"\ud83d\ude47", bow_and_arrow:"\ud83c\udff9", bowing_woman:"\ud83d\ude47&zwj;♀️", bowling:"\ud83c\udfb3", boxing_glove:"\ud83e\udd4a", boy:"\ud83d\udc66", bread:"\ud83c\udf5e", bride_with_veil:"\ud83d\udc70", bridge_at_night:"\ud83c\udf09", briefcase:"\ud83d\udcbc", broken_heart:"\ud83d\udc94", bug:"\ud83d\udc1b", building_construction:"\ud83c\udfd7", bulb:"\ud83d\udca1", bullettrain_front:"\ud83d\ude85", bullettrain_side:"\ud83d\ude84", burrito:"\ud83c\udf2f", 
  bus:"\ud83d\ude8c", business_suit_levitating:"\ud83d\udd74", busstop:"\ud83d\ude8f", bust_in_silhouette:"\ud83d\udc64", busts_in_silhouette:"\ud83d\udc65", butterfly:"\ud83e\udd8b", cactus:"\ud83c\udf35", cake:"\ud83c\udf70", calendar:"\ud83d\udcc6", call_me_hand:"\ud83e\udd19", calling:"\ud83d\udcf2", camel:"\ud83d\udc2b", camera:"\ud83d\udcf7", camera_flash:"\ud83d\udcf8", camping:"\ud83c\udfd5", cancer:"♋️", candle:"\ud83d\udd6f", candy:"\ud83c\udf6c", canoe:"\ud83d\udef6", capital_abcd:"\ud83d\udd20", 
  capricorn:"♑️", car:"\ud83d\ude97", card_file_box:"\ud83d\uddc3", card_index:"\ud83d\udcc7", card_index_dividers:"\ud83d\uddc2", carousel_horse:"\ud83c\udfa0", carrot:"\ud83e\udd55", cat:"\ud83d\udc31", cat2:"\ud83d\udc08", cd:"\ud83d\udcbf", chains:"⛓", champagne:"\ud83c\udf7e", chart:"\ud83d\udcb9", chart_with_downwards_trend:"\ud83d\udcc9", chart_with_upwards_trend:"\ud83d\udcc8", checkered_flag:"\ud83c\udfc1", cheese:"\ud83e\uddc0", cherries:"\ud83c\udf52", cherry_blossom:"\ud83c\udf38", chestnut:"\ud83c\udf30", 
  chicken:"\ud83d\udc14", children_crossing:"\ud83d\udeb8", chipmunk:"\ud83d\udc3f", chocolate_bar:"\ud83c\udf6b", christmas_tree:"\ud83c\udf84", church:"⛪️", cinema:"\ud83c\udfa6", circus_tent:"\ud83c\udfaa", city_sunrise:"\ud83c\udf07", city_sunset:"\ud83c\udf06", cityscape:"\ud83c\udfd9", cl:"\ud83c\udd91", clamp:"\ud83d\udddc", clap:"\ud83d\udc4f", clapper:"\ud83c\udfac", classical_building:"\ud83c\udfdb", clinking_glasses:"\ud83e\udd42", clipboard:"\ud83d\udccb", clock1:"\ud83d\udd50", clock10:"\ud83d\udd59", 
  clock1030:"\ud83d\udd65", clock11:"\ud83d\udd5a", clock1130:"\ud83d\udd66", clock12:"\ud83d\udd5b", clock1230:"\ud83d\udd67", clock130:"\ud83d\udd5c", clock2:"\ud83d\udd51", clock230:"\ud83d\udd5d", clock3:"\ud83d\udd52", clock330:"\ud83d\udd5e", clock4:"\ud83d\udd53", clock430:"\ud83d\udd5f", clock5:"\ud83d\udd54", clock530:"\ud83d\udd60", clock6:"\ud83d\udd55", clock630:"\ud83d\udd61", clock7:"\ud83d\udd56", clock730:"\ud83d\udd62", clock8:"\ud83d\udd57", clock830:"\ud83d\udd63", clock9:"\ud83d\udd58", 
  clock930:"\ud83d\udd64", closed_book:"\ud83d\udcd5", closed_lock_with_key:"\ud83d\udd10", closed_umbrella:"\ud83c\udf02", cloud:"☁️", cloud_with_lightning:"\ud83c\udf29", cloud_with_lightning_and_rain:"⛈", cloud_with_rain:"\ud83c\udf27", cloud_with_snow:"\ud83c\udf28", clown_face:"\ud83e\udd21", clubs:"♣️", cocktail:"\ud83c\udf78", coffee:"☕️", coffin:"⚰️", cold_sweat:"\ud83d\ude30", comet:"☄️", computer:"\ud83d\udcbb", computer_mouse:"\ud83d\uddb1", confetti_ball:"\ud83c\udf8a", confounded:"\ud83d\ude16", 
  confused:"\ud83d\ude15", congratulations:"㊗️", construction:"\ud83d\udea7", construction_worker_man:"\ud83d\udc77", construction_worker_woman:"\ud83d\udc77&zwj;♀️", control_knobs:"\ud83c\udf9b", convenience_store:"\ud83c\udfea", cookie:"\ud83c\udf6a", cool:"\ud83c\udd92", policeman:"\ud83d\udc6e", copyright:"©️", corn:"\ud83c\udf3d", couch_and_lamp:"\ud83d\udecb", couple:"\ud83d\udc6b", couple_with_heart_woman_man:"\ud83d\udc91", couple_with_heart_man_man:"\ud83d\udc68&zwj;❤️&zwj;\ud83d\udc68", 
  couple_with_heart_woman_woman:"\ud83d\udc69&zwj;❤️&zwj;\ud83d\udc69", couplekiss_man_man:"\ud83d\udc68&zwj;❤️&zwj;\ud83d\udc8b&zwj;\ud83d\udc68", couplekiss_man_woman:"\ud83d\udc8f", couplekiss_woman_woman:"\ud83d\udc69&zwj;❤️&zwj;\ud83d\udc8b&zwj;\ud83d\udc69", cow:"\ud83d\udc2e", cow2:"\ud83d\udc04", cowboy_hat_face:"\ud83e\udd20", crab:"\ud83e\udd80", crayon:"\ud83d\udd8d", credit_card:"\ud83d\udcb3", crescent_moon:"\ud83c\udf19", cricket:"\ud83c\udfcf", crocodile:"\ud83d\udc0a", croissant:"\ud83e\udd50", 
  crossed_fingers:"\ud83e\udd1e", crossed_flags:"\ud83c\udf8c", crossed_swords:"⚔️", crown:"\ud83d\udc51", cry:"\ud83d\ude22", crying_cat_face:"\ud83d\ude3f", crystal_ball:"\ud83d\udd2e", cucumber:"\ud83e\udd52", cupid:"\ud83d\udc98", curly_loop:"➰", currency_exchange:"\ud83d\udcb1", curry:"\ud83c\udf5b", custard:"\ud83c\udf6e", customs:"\ud83d\udec3", cyclone:"\ud83c\udf00", dagger:"\ud83d\udde1", dancer:"\ud83d\udc83", dancing_women:"\ud83d\udc6f", dancing_men:"\ud83d\udc6f&zwj;♂️", dango:"\ud83c\udf61", 
  dark_sunglasses:"\ud83d\udd76", dart:"\ud83c\udfaf", dash:"\ud83d\udca8", date:"\ud83d\udcc5", deciduous_tree:"\ud83c\udf33", deer:"\ud83e\udd8c", department_store:"\ud83c\udfec", derelict_house:"\ud83c\udfda", desert:"\ud83c\udfdc", desert_island:"\ud83c\udfdd", desktop_computer:"\ud83d\udda5", male_detective:"\ud83d\udd75️", diamond_shape_with_a_dot_inside:"\ud83d\udca0", diamonds:"♦️", disappointed:"\ud83d\ude1e", disappointed_relieved:"\ud83d\ude25", dizzy:"\ud83d\udcab", dizzy_face:"\ud83d\ude35", 
  do_not_litter:"\ud83d\udeaf", dog:"\ud83d\udc36", dog2:"\ud83d\udc15", dollar:"\ud83d\udcb5", dolls:"\ud83c\udf8e", dolphin:"\ud83d\udc2c", door:"\ud83d\udeaa", doughnut:"\ud83c\udf69", dove:"\ud83d\udd4a", dragon:"\ud83d\udc09", dragon_face:"\ud83d\udc32", dress:"\ud83d\udc57", dromedary_camel:"\ud83d\udc2a", drooling_face:"\ud83e\udd24", droplet:"\ud83d\udca7", drum:"\ud83e\udd41", duck:"\ud83e\udd86", dvd:"\ud83d\udcc0", "e-mail":"\ud83d\udce7", eagle:"\ud83e\udd85", ear:"\ud83d\udc42", ear_of_rice:"\ud83c\udf3e", 
  earth_africa:"\ud83c\udf0d", earth_americas:"\ud83c\udf0e", earth_asia:"\ud83c\udf0f", egg:"\ud83e\udd5a", eggplant:"\ud83c\udf46", eight_pointed_black_star:"✴️", eight_spoked_asterisk:"✳️", electric_plug:"\ud83d\udd0c", elephant:"\ud83d\udc18", email:"✉️", end:"\ud83d\udd1a", envelope_with_arrow:"\ud83d\udce9", euro:"\ud83d\udcb6", european_castle:"\ud83c\udff0", european_post_office:"\ud83c\udfe4", evergreen_tree:"\ud83c\udf32", exclamation:"❗️", expressionless:"\ud83d\ude11", eye:"\ud83d\udc41", 
  eye_speech_bubble:"\ud83d\udc41&zwj;\ud83d\udde8", eyeglasses:"\ud83d\udc53", eyes:"\ud83d\udc40", face_with_head_bandage:"\ud83e\udd15", face_with_thermometer:"\ud83e\udd12", fist_oncoming:"\ud83d\udc4a", factory:"\ud83c\udfed", fallen_leaf:"\ud83c\udf42", family_man_woman_boy:"\ud83d\udc6a", family_man_boy:"\ud83d\udc68&zwj;\ud83d\udc66", family_man_boy_boy:"\ud83d\udc68&zwj;\ud83d\udc66&zwj;\ud83d\udc66", family_man_girl:"\ud83d\udc68&zwj;\ud83d\udc67", family_man_girl_boy:"\ud83d\udc68&zwj;\ud83d\udc67&zwj;\ud83d\udc66", 
  family_man_girl_girl:"\ud83d\udc68&zwj;\ud83d\udc67&zwj;\ud83d\udc67", family_man_man_boy:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc66", family_man_man_boy_boy:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc66&zwj;\ud83d\udc66", family_man_man_girl:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc67", family_man_man_girl_boy:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc67&zwj;\ud83d\udc66", family_man_man_girl_girl:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc67&zwj;\ud83d\udc67", family_man_woman_boy_boy:"\ud83d\udc68&zwj;\ud83d\udc69&zwj;\ud83d\udc66&zwj;\ud83d\udc66", 
  family_man_woman_girl:"\ud83d\udc68&zwj;\ud83d\udc69&zwj;\ud83d\udc67", family_man_woman_girl_boy:"\ud83d\udc68&zwj;\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc66", family_man_woman_girl_girl:"\ud83d\udc68&zwj;\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc67", family_woman_boy:"\ud83d\udc69&zwj;\ud83d\udc66", family_woman_boy_boy:"\ud83d\udc69&zwj;\ud83d\udc66&zwj;\ud83d\udc66", family_woman_girl:"\ud83d\udc69&zwj;\ud83d\udc67", family_woman_girl_boy:"\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc66", 
  family_woman_girl_girl:"\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc67", family_woman_woman_boy:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc66", family_woman_woman_boy_boy:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc66&zwj;\ud83d\udc66", family_woman_woman_girl:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc67", family_woman_woman_girl_boy:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc66", family_woman_woman_girl_girl:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc67", 
  fast_forward:"⏩", fax:"\ud83d\udce0", fearful:"\ud83d\ude28", feet:"\ud83d\udc3e", female_detective:"\ud83d\udd75️&zwj;♀️", ferris_wheel:"\ud83c\udfa1", ferry:"⛴", field_hockey:"\ud83c\udfd1", file_cabinet:"\ud83d\uddc4", file_folder:"\ud83d\udcc1", film_projector:"\ud83d\udcfd", film_strip:"\ud83c\udf9e", fire:"\ud83d\udd25", fire_engine:"\ud83d\ude92", fireworks:"\ud83c\udf86", first_quarter_moon:"\ud83c\udf13", first_quarter_moon_with_face:"\ud83c\udf1b", fish:"\ud83d\udc1f", fish_cake:"\ud83c\udf65", 
  fishing_pole_and_fish:"\ud83c\udfa3", fist_raised:"✊", fist_left:"\ud83e\udd1b", fist_right:"\ud83e\udd1c", flags:"\ud83c\udf8f", flashlight:"\ud83d\udd26", fleur_de_lis:"⚜️", flight_arrival:"\ud83d\udeec", flight_departure:"\ud83d\udeeb", floppy_disk:"\ud83d\udcbe", flower_playing_cards:"\ud83c\udfb4", flushed:"\ud83d\ude33", fog:"\ud83c\udf2b", foggy:"\ud83c\udf01", football:"\ud83c\udfc8", footprints:"\ud83d\udc63", fork_and_knife:"\ud83c\udf74", fountain:"⛲️", fountain_pen:"\ud83d\udd8b", four_leaf_clover:"\ud83c\udf40", 
  fox_face:"\ud83e\udd8a", framed_picture:"\ud83d\uddbc", free:"\ud83c\udd93", fried_egg:"\ud83c\udf73", fried_shrimp:"\ud83c\udf64", fries:"\ud83c\udf5f", frog:"\ud83d\udc38", frowning:"\ud83d\ude26", frowning_face:"☹️", frowning_man:"\ud83d\ude4d&zwj;♂️", frowning_woman:"\ud83d\ude4d", middle_finger:"\ud83d\udd95", fuelpump:"⛽️", full_moon:"\ud83c\udf15", full_moon_with_face:"\ud83c\udf1d", funeral_urn:"⚱️", game_die:"\ud83c\udfb2", gear:"⚙️", gem:"\ud83d\udc8e", gemini:"♊️", ghost:"\ud83d\udc7b", 
  gift:"\ud83c\udf81", gift_heart:"\ud83d\udc9d", girl:"\ud83d\udc67", globe_with_meridians:"\ud83c\udf10", goal_net:"\ud83e\udd45", goat:"\ud83d\udc10", golf:"⛳️", golfing_man:"\ud83c\udfcc️", golfing_woman:"\ud83c\udfcc️&zwj;♀️", gorilla:"\ud83e\udd8d", grapes:"\ud83c\udf47", green_apple:"\ud83c\udf4f", green_book:"\ud83d\udcd7", green_heart:"\ud83d\udc9a", green_salad:"\ud83e\udd57", grey_exclamation:"❕", grey_question:"❔", grimacing:"\ud83d\ude2c", grin:"\ud83d\ude01", grinning:"\ud83d\ude00", 
  guardsman:"\ud83d\udc82", guardswoman:"\ud83d\udc82&zwj;♀️", guitar:"\ud83c\udfb8", gun:"\ud83d\udd2b", haircut_woman:"\ud83d\udc87", haircut_man:"\ud83d\udc87&zwj;♂️", hamburger:"\ud83c\udf54", hammer:"\ud83d\udd28", hammer_and_pick:"⚒", hammer_and_wrench:"\ud83d\udee0", hamster:"\ud83d\udc39", hand:"✋", handbag:"\ud83d\udc5c", handshake:"\ud83e\udd1d", hankey:"\ud83d\udca9", hatched_chick:"\ud83d\udc25", hatching_chick:"\ud83d\udc23", headphones:"\ud83c\udfa7", hear_no_evil:"\ud83d\ude49", heart:"❤️", 
  heart_decoration:"\ud83d\udc9f", heart_eyes:"\ud83d\ude0d", heart_eyes_cat:"\ud83d\ude3b", heartbeat:"\ud83d\udc93", heartpulse:"\ud83d\udc97", hearts:"♥️", heavy_check_mark:"✔️", heavy_division_sign:"➗", heavy_dollar_sign:"\ud83d\udcb2", heavy_heart_exclamation:"❣️", heavy_minus_sign:"➖", heavy_multiplication_x:"✖️", heavy_plus_sign:"➕", helicopter:"\ud83d\ude81", herb:"\ud83c\udf3f", hibiscus:"\ud83c\udf3a", high_brightness:"\ud83d\udd06", high_heel:"\ud83d\udc60", hocho:"\ud83d\udd2a", hole:"\ud83d\udd73", 
  honey_pot:"\ud83c\udf6f", horse:"\ud83d\udc34", horse_racing:"\ud83c\udfc7", hospital:"\ud83c\udfe5", hot_pepper:"\ud83c\udf36", hotdog:"\ud83c\udf2d", hotel:"\ud83c\udfe8", hotsprings:"♨️", hourglass:"⌛️", hourglass_flowing_sand:"⏳", house:"\ud83c\udfe0", house_with_garden:"\ud83c\udfe1", houses:"\ud83c\udfd8", hugs:"\ud83e\udd17", hushed:"\ud83d\ude2f", ice_cream:"\ud83c\udf68", ice_hockey:"\ud83c\udfd2", ice_skate:"⛸", icecream:"\ud83c\udf66", id:"\ud83c\udd94", ideograph_advantage:"\ud83c\ude50", 
  imp:"\ud83d\udc7f", inbox_tray:"\ud83d\udce5", incoming_envelope:"\ud83d\udce8", tipping_hand_woman:"\ud83d\udc81", information_source:"ℹ️", innocent:"\ud83d\ude07", interrobang:"⁉️", iphone:"\ud83d\udcf1", izakaya_lantern:"\ud83c\udfee", jack_o_lantern:"\ud83c\udf83", japan:"\ud83d\uddfe", japanese_castle:"\ud83c\udfef", japanese_goblin:"\ud83d\udc7a", japanese_ogre:"\ud83d\udc79", jeans:"\ud83d\udc56", joy:"\ud83d\ude02", joy_cat:"\ud83d\ude39", joystick:"\ud83d\udd79", kaaba:"\ud83d\udd4b", 
  key:"\ud83d\udd11", keyboard:"⌨️", keycap_ten:"\ud83d\udd1f", kick_scooter:"\ud83d\udef4", kimono:"\ud83d\udc58", kiss:"\ud83d\udc8b", kissing:"\ud83d\ude17", kissing_cat:"\ud83d\ude3d", kissing_closed_eyes:"\ud83d\ude1a", kissing_heart:"\ud83d\ude18", kissing_smiling_eyes:"\ud83d\ude19", kiwi_fruit:"\ud83e\udd5d", koala:"\ud83d\udc28", koko:"\ud83c\ude01", label:"\ud83c\udff7", large_blue_circle:"\ud83d\udd35", large_blue_diamond:"\ud83d\udd37", large_orange_diamond:"\ud83d\udd36", last_quarter_moon:"\ud83c\udf17", 
  last_quarter_moon_with_face:"\ud83c\udf1c", latin_cross:"✝️", laughing:"\ud83d\ude06", leaves:"\ud83c\udf43", ledger:"\ud83d\udcd2", left_luggage:"\ud83d\udec5", left_right_arrow:"↔️", leftwards_arrow_with_hook:"↩️", lemon:"\ud83c\udf4b", leo:"♌️", leopard:"\ud83d\udc06", level_slider:"\ud83c\udf9a", libra:"♎️", light_rail:"\ud83d\ude88", link:"\ud83d\udd17", lion:"\ud83e\udd81", lips:"\ud83d\udc44", lipstick:"\ud83d\udc84", lizard:"\ud83e\udd8e", lock:"\ud83d\udd12", lock_with_ink_pen:"\ud83d\udd0f", 
  lollipop:"\ud83c\udf6d", loop:"➿", loud_sound:"\ud83d\udd0a", loudspeaker:"\ud83d\udce2", love_hotel:"\ud83c\udfe9", love_letter:"\ud83d\udc8c", low_brightness:"\ud83d\udd05", lying_face:"\ud83e\udd25", m:"Ⓜ️", mag:"\ud83d\udd0d", mag_right:"\ud83d\udd0e", mahjong:"\ud83c\udc04️", mailbox:"\ud83d\udceb", mailbox_closed:"\ud83d\udcea", mailbox_with_mail:"\ud83d\udcec", mailbox_with_no_mail:"\ud83d\udced", man:"\ud83d\udc68", man_artist:"\ud83d\udc68&zwj;\ud83c\udfa8", man_astronaut:"\ud83d\udc68&zwj;\ud83d\ude80", 
  man_cartwheeling:"\ud83e\udd38&zwj;♂️", man_cook:"\ud83d\udc68&zwj;\ud83c\udf73", man_dancing:"\ud83d\udd7a", man_facepalming:"\ud83e\udd26&zwj;♂️", man_factory_worker:"\ud83d\udc68&zwj;\ud83c\udfed", man_farmer:"\ud83d\udc68&zwj;\ud83c\udf3e", man_firefighter:"\ud83d\udc68&zwj;\ud83d\ude92", man_health_worker:"\ud83d\udc68&zwj;⚕️", man_in_tuxedo:"\ud83e\udd35", man_judge:"\ud83d\udc68&zwj;⚖️", man_juggling:"\ud83e\udd39&zwj;♂️", man_mechanic:"\ud83d\udc68&zwj;\ud83d\udd27", man_office_worker:"\ud83d\udc68&zwj;\ud83d\udcbc", 
  man_pilot:"\ud83d\udc68&zwj;✈️", man_playing_handball:"\ud83e\udd3e&zwj;♂️", man_playing_water_polo:"\ud83e\udd3d&zwj;♂️", man_scientist:"\ud83d\udc68&zwj;\ud83d\udd2c", man_shrugging:"\ud83e\udd37&zwj;♂️", man_singer:"\ud83d\udc68&zwj;\ud83c\udfa4", man_student:"\ud83d\udc68&zwj;\ud83c\udf93", man_teacher:"\ud83d\udc68&zwj;\ud83c\udfeb", man_technologist:"\ud83d\udc68&zwj;\ud83d\udcbb", man_with_gua_pi_mao:"\ud83d\udc72", man_with_turban:"\ud83d\udc73", tangerine:"\ud83c\udf4a", mans_shoe:"\ud83d\udc5e", 
  mantelpiece_clock:"\ud83d\udd70", maple_leaf:"\ud83c\udf41", martial_arts_uniform:"\ud83e\udd4b", mask:"\ud83d\ude37", massage_woman:"\ud83d\udc86", massage_man:"\ud83d\udc86&zwj;♂️", meat_on_bone:"\ud83c\udf56", medal_military:"\ud83c\udf96", medal_sports:"\ud83c\udfc5", mega:"\ud83d\udce3", melon:"\ud83c\udf48", memo:"\ud83d\udcdd", men_wrestling:"\ud83e\udd3c&zwj;♂️", menorah:"\ud83d\udd4e", mens:"\ud83d\udeb9", metal:"\ud83e\udd18", metro:"\ud83d\ude87", microphone:"\ud83c\udfa4", microscope:"\ud83d\udd2c", 
  milk_glass:"\ud83e\udd5b", milky_way:"\ud83c\udf0c", minibus:"\ud83d\ude90", minidisc:"\ud83d\udcbd", mobile_phone_off:"\ud83d\udcf4", money_mouth_face:"\ud83e\udd11", money_with_wings:"\ud83d\udcb8", moneybag:"\ud83d\udcb0", monkey:"\ud83d\udc12", monkey_face:"\ud83d\udc35", monorail:"\ud83d\ude9d", moon:"\ud83c\udf14", mortar_board:"\ud83c\udf93", mosque:"\ud83d\udd4c", motor_boat:"\ud83d\udee5", motor_scooter:"\ud83d\udef5", motorcycle:"\ud83c\udfcd", motorway:"\ud83d\udee3", mount_fuji:"\ud83d\uddfb", 
  mountain:"⛰", mountain_biking_man:"\ud83d\udeb5", mountain_biking_woman:"\ud83d\udeb5&zwj;♀️", mountain_cableway:"\ud83d\udea0", mountain_railway:"\ud83d\ude9e", mountain_snow:"\ud83c\udfd4", mouse:"\ud83d\udc2d", mouse2:"\ud83d\udc01", movie_camera:"\ud83c\udfa5", moyai:"\ud83d\uddff", mrs_claus:"\ud83e\udd36", muscle:"\ud83d\udcaa", mushroom:"\ud83c\udf44", musical_keyboard:"\ud83c\udfb9", musical_note:"\ud83c\udfb5", musical_score:"\ud83c\udfbc", mute:"\ud83d\udd07", nail_care:"\ud83d\udc85", 
  name_badge:"\ud83d\udcdb", national_park:"\ud83c\udfde", nauseated_face:"\ud83e\udd22", necktie:"\ud83d\udc54", negative_squared_cross_mark:"❎", nerd_face:"\ud83e\udd13", neutral_face:"\ud83d\ude10", "new":"\ud83c\udd95", new_moon:"\ud83c\udf11", new_moon_with_face:"\ud83c\udf1a", newspaper:"\ud83d\udcf0", newspaper_roll:"\ud83d\uddde", next_track_button:"⏭", ng:"\ud83c\udd96", no_good_man:"\ud83d\ude45&zwj;♂️", no_good_woman:"\ud83d\ude45", night_with_stars:"\ud83c\udf03", no_bell:"\ud83d\udd15", 
  no_bicycles:"\ud83d\udeb3", no_entry:"⛔️", no_entry_sign:"\ud83d\udeab", no_mobile_phones:"\ud83d\udcf5", no_mouth:"\ud83d\ude36", no_pedestrians:"\ud83d\udeb7", no_smoking:"\ud83d\udead", "non-potable_water":"\ud83d\udeb1", nose:"\ud83d\udc43", notebook:"\ud83d\udcd3", notebook_with_decorative_cover:"\ud83d\udcd4", notes:"\ud83c\udfb6", nut_and_bolt:"\ud83d\udd29", o:"⭕️", o2:"\ud83c\udd7e️", ocean:"\ud83c\udf0a", octopus:"\ud83d\udc19", oden:"\ud83c\udf62", office:"\ud83c\udfe2", oil_drum:"\ud83d\udee2", 
  ok:"\ud83c\udd97", ok_hand:"\ud83d\udc4c", ok_man:"\ud83d\ude46&zwj;♂️", ok_woman:"\ud83d\ude46", old_key:"\ud83d\udddd", older_man:"\ud83d\udc74", older_woman:"\ud83d\udc75", om:"\ud83d\udd49", on:"\ud83d\udd1b", oncoming_automobile:"\ud83d\ude98", oncoming_bus:"\ud83d\ude8d", oncoming_police_car:"\ud83d\ude94", oncoming_taxi:"\ud83d\ude96", open_file_folder:"\ud83d\udcc2", open_hands:"\ud83d\udc50", open_mouth:"\ud83d\ude2e", open_umbrella:"☂️", ophiuchus:"⛎", orange_book:"\ud83d\udcd9", orthodox_cross:"☦️", 
  outbox_tray:"\ud83d\udce4", owl:"\ud83e\udd89", ox:"\ud83d\udc02", "package":"\ud83d\udce6", page_facing_up:"\ud83d\udcc4", page_with_curl:"\ud83d\udcc3", pager:"\ud83d\udcdf", paintbrush:"\ud83d\udd8c", palm_tree:"\ud83c\udf34", pancakes:"\ud83e\udd5e", panda_face:"\ud83d\udc3c", paperclip:"\ud83d\udcce", paperclips:"\ud83d\udd87", parasol_on_ground:"⛱", parking:"\ud83c\udd7f️", part_alternation_mark:"〽️", partly_sunny:"⛅️", passenger_ship:"\ud83d\udef3", passport_control:"\ud83d\udec2", pause_button:"⏸", 
  peace_symbol:"☮️", peach:"\ud83c\udf51", peanuts:"\ud83e\udd5c", pear:"\ud83c\udf50", pen:"\ud83d\udd8a", pencil2:"✏️", penguin:"\ud83d\udc27", pensive:"\ud83d\ude14", performing_arts:"\ud83c\udfad", persevere:"\ud83d\ude23", person_fencing:"\ud83e\udd3a", pouting_woman:"\ud83d\ude4e", phone:"☎️", pick:"⛏", pig:"\ud83d\udc37", pig2:"\ud83d\udc16", pig_nose:"\ud83d\udc3d", pill:"\ud83d\udc8a", pineapple:"\ud83c\udf4d", ping_pong:"\ud83c\udfd3", pisces:"♓️", pizza:"\ud83c\udf55", place_of_worship:"\ud83d\uded0", 
  plate_with_cutlery:"\ud83c\udf7d", play_or_pause_button:"⏯", point_down:"\ud83d\udc47", point_left:"\ud83d\udc48", point_right:"\ud83d\udc49", point_up:"☝️", point_up_2:"\ud83d\udc46", police_car:"\ud83d\ude93", policewoman:"\ud83d\udc6e&zwj;♀️", poodle:"\ud83d\udc29", popcorn:"\ud83c\udf7f", post_office:"\ud83c\udfe3", postal_horn:"\ud83d\udcef", postbox:"\ud83d\udcee", potable_water:"\ud83d\udeb0", potato:"\ud83e\udd54", pouch:"\ud83d\udc5d", poultry_leg:"\ud83c\udf57", pound:"\ud83d\udcb7", 
  rage:"\ud83d\ude21", pouting_cat:"\ud83d\ude3e", pouting_man:"\ud83d\ude4e&zwj;♂️", pray:"\ud83d\ude4f", prayer_beads:"\ud83d\udcff", pregnant_woman:"\ud83e\udd30", previous_track_button:"⏮", prince:"\ud83e\udd34", princess:"\ud83d\udc78", printer:"\ud83d\udda8", purple_heart:"\ud83d\udc9c", purse:"\ud83d\udc5b", pushpin:"\ud83d\udccc", put_litter_in_its_place:"\ud83d\udeae", question:"❓", rabbit:"\ud83d\udc30", rabbit2:"\ud83d\udc07", racehorse:"\ud83d\udc0e", racing_car:"\ud83c\udfce", radio:"\ud83d\udcfb", 
  radio_button:"\ud83d\udd18", radioactive:"☢️", railway_car:"\ud83d\ude83", railway_track:"\ud83d\udee4", rainbow:"\ud83c\udf08", rainbow_flag:"\ud83c\udff3️&zwj;\ud83c\udf08", raised_back_of_hand:"\ud83e\udd1a", raised_hand_with_fingers_splayed:"\ud83d\udd90", raised_hands:"\ud83d\ude4c", raising_hand_woman:"\ud83d\ude4b", raising_hand_man:"\ud83d\ude4b&zwj;♂️", ram:"\ud83d\udc0f", ramen:"\ud83c\udf5c", rat:"\ud83d\udc00", record_button:"⏺", recycle:"♻️", red_circle:"\ud83d\udd34", registered:"®️", 
  relaxed:"☺️", relieved:"\ud83d\ude0c", reminder_ribbon:"\ud83c\udf97", repeat:"\ud83d\udd01", repeat_one:"\ud83d\udd02", rescue_worker_helmet:"⛑", restroom:"\ud83d\udebb", revolving_hearts:"\ud83d\udc9e", rewind:"⏪", rhinoceros:"\ud83e\udd8f", ribbon:"\ud83c\udf80", rice:"\ud83c\udf5a", rice_ball:"\ud83c\udf59", rice_cracker:"\ud83c\udf58", rice_scene:"\ud83c\udf91", right_anger_bubble:"\ud83d\uddef", ring:"\ud83d\udc8d", robot:"\ud83e\udd16", rocket:"\ud83d\ude80", rofl:"\ud83e\udd23", roll_eyes:"\ud83d\ude44", 
  roller_coaster:"\ud83c\udfa2", rooster:"\ud83d\udc13", rose:"\ud83c\udf39", rosette:"\ud83c\udff5", rotating_light:"\ud83d\udea8", round_pushpin:"\ud83d\udccd", rowing_man:"\ud83d\udea3", rowing_woman:"\ud83d\udea3&zwj;♀️", rugby_football:"\ud83c\udfc9", running_man:"\ud83c\udfc3", running_shirt_with_sash:"\ud83c\udfbd", running_woman:"\ud83c\udfc3&zwj;♀️", sa:"\ud83c\ude02️", sagittarius:"♐️", sake:"\ud83c\udf76", sandal:"\ud83d\udc61", santa:"\ud83c\udf85", satellite:"\ud83d\udce1", saxophone:"\ud83c\udfb7", 
  school:"\ud83c\udfeb", school_satchel:"\ud83c\udf92", scissors:"✂️", scorpion:"\ud83e\udd82", scorpius:"♏️", scream:"\ud83d\ude31", scream_cat:"\ud83d\ude40", scroll:"\ud83d\udcdc", seat:"\ud83d\udcba", secret:"㊙️", see_no_evil:"\ud83d\ude48", seedling:"\ud83c\udf31", selfie:"\ud83e\udd33", shallow_pan_of_food:"\ud83e\udd58", shamrock:"☘️", shark:"\ud83e\udd88", shaved_ice:"\ud83c\udf67", sheep:"\ud83d\udc11", shell:"\ud83d\udc1a", shield:"\ud83d\udee1", shinto_shrine:"⛩", ship:"\ud83d\udea2", 
  shirt:"\ud83d\udc55", shopping:"\ud83d\udecd", shopping_cart:"\ud83d\uded2", shower:"\ud83d\udebf", shrimp:"\ud83e\udd90", signal_strength:"\ud83d\udcf6", six_pointed_star:"\ud83d\udd2f", ski:"\ud83c\udfbf", skier:"⛷", skull:"\ud83d\udc80", skull_and_crossbones:"☠️", sleeping:"\ud83d\ude34", sleeping_bed:"\ud83d\udecc", sleepy:"\ud83d\ude2a", slightly_frowning_face:"\ud83d\ude41", slightly_smiling_face:"\ud83d\ude42", slot_machine:"\ud83c\udfb0", small_airplane:"\ud83d\udee9", small_blue_diamond:"\ud83d\udd39", 
  small_orange_diamond:"\ud83d\udd38", small_red_triangle:"\ud83d\udd3a", small_red_triangle_down:"\ud83d\udd3b", smile:"\ud83d\ude04", smile_cat:"\ud83d\ude38", smiley:"\ud83d\ude03", smiley_cat:"\ud83d\ude3a", smiling_imp:"\ud83d\ude08", smirk:"\ud83d\ude0f", smirk_cat:"\ud83d\ude3c", smoking:"\ud83d\udeac", snail:"\ud83d\udc0c", snake:"\ud83d\udc0d", sneezing_face:"\ud83e\udd27", snowboarder:"\ud83c\udfc2", snowflake:"❄️", snowman:"⛄️", snowman_with_snow:"☃️", sob:"\ud83d\ude2d", soccer:"⚽️", 
  soon:"\ud83d\udd1c", sos:"\ud83c\udd98", sound:"\ud83d\udd09", space_invader:"\ud83d\udc7e", spades:"♠️", spaghetti:"\ud83c\udf5d", sparkle:"❇️", sparkler:"\ud83c\udf87", sparkles:"✨", sparkling_heart:"\ud83d\udc96", speak_no_evil:"\ud83d\ude4a", speaker:"\ud83d\udd08", speaking_head:"\ud83d\udde3", speech_balloon:"\ud83d\udcac", speedboat:"\ud83d\udea4", spider:"\ud83d\udd77", spider_web:"\ud83d\udd78", spiral_calendar:"\ud83d\uddd3", spiral_notepad:"\ud83d\uddd2", spoon:"\ud83e\udd44", squid:"\ud83e\udd91", 
  stadium:"\ud83c\udfdf", star:"⭐️", star2:"\ud83c\udf1f", star_and_crescent:"☪️", star_of_david:"✡️", stars:"\ud83c\udf20", station:"\ud83d\ude89", statue_of_liberty:"\ud83d\uddfd", steam_locomotive:"\ud83d\ude82", stew:"\ud83c\udf72", stop_button:"⏹", stop_sign:"\ud83d\uded1", stopwatch:"⏱", straight_ruler:"\ud83d\udccf", strawberry:"\ud83c\udf53", stuck_out_tongue:"\ud83d\ude1b", stuck_out_tongue_closed_eyes:"\ud83d\ude1d", stuck_out_tongue_winking_eye:"\ud83d\ude1c", studio_microphone:"\ud83c\udf99", 
  stuffed_flatbread:"\ud83e\udd59", sun_behind_large_cloud:"\ud83c\udf25", sun_behind_rain_cloud:"\ud83c\udf26", sun_behind_small_cloud:"\ud83c\udf24", sun_with_face:"\ud83c\udf1e", sunflower:"\ud83c\udf3b", sunglasses:"\ud83d\ude0e", sunny:"☀️", sunrise:"\ud83c\udf05", sunrise_over_mountains:"\ud83c\udf04", surfing_man:"\ud83c\udfc4", surfing_woman:"\ud83c\udfc4&zwj;♀️", sushi:"\ud83c\udf63", suspension_railway:"\ud83d\ude9f", sweat:"\ud83d\ude13", sweat_drops:"\ud83d\udca6", sweat_smile:"\ud83d\ude05", 
  sweet_potato:"\ud83c\udf60", swimming_man:"\ud83c\udfca", swimming_woman:"\ud83c\udfca&zwj;♀️", symbols:"\ud83d\udd23", synagogue:"\ud83d\udd4d", syringe:"\ud83d\udc89", taco:"\ud83c\udf2e", tada:"\ud83c\udf89", tanabata_tree:"\ud83c\udf8b", taurus:"♉️", taxi:"\ud83d\ude95", tea:"\ud83c\udf75", telephone_receiver:"\ud83d\udcde", telescope:"\ud83d\udd2d", tennis:"\ud83c\udfbe", tent:"⛺️", thermometer:"\ud83c\udf21", thinking:"\ud83e\udd14", thought_balloon:"\ud83d\udcad", ticket:"\ud83c\udfab", 
  tickets:"\ud83c\udf9f", tiger:"\ud83d\udc2f", tiger2:"\ud83d\udc05", timer_clock:"⏲", tipping_hand_man:"\ud83d\udc81&zwj;♂️", tired_face:"\ud83d\ude2b", tm:"™️", toilet:"\ud83d\udebd", tokyo_tower:"\ud83d\uddfc", tomato:"\ud83c\udf45", tongue:"\ud83d\udc45", top:"\ud83d\udd1d", tophat:"\ud83c\udfa9", tornado:"\ud83c\udf2a", trackball:"\ud83d\uddb2", tractor:"\ud83d\ude9c", traffic_light:"\ud83d\udea5", train:"\ud83d\ude8b", train2:"\ud83d\ude86", tram:"\ud83d\ude8a", triangular_flag_on_post:"\ud83d\udea9", 
  triangular_ruler:"\ud83d\udcd0", trident:"\ud83d\udd31", triumph:"\ud83d\ude24", trolleybus:"\ud83d\ude8e", trophy:"\ud83c\udfc6", tropical_drink:"\ud83c\udf79", tropical_fish:"\ud83d\udc20", truck:"\ud83d\ude9a", trumpet:"\ud83c\udfba", tulip:"\ud83c\udf37", tumbler_glass:"\ud83e\udd43", turkey:"\ud83e\udd83", turtle:"\ud83d\udc22", tv:"\ud83d\udcfa", twisted_rightwards_arrows:"\ud83d\udd00", two_hearts:"\ud83d\udc95", two_men_holding_hands:"\ud83d\udc6c", two_women_holding_hands:"\ud83d\udc6d", 
  u5272:"\ud83c\ude39", u5408:"\ud83c\ude34", u55b6:"\ud83c\ude3a", u6307:"\ud83c\ude2f️", u6708:"\ud83c\ude37️", u6709:"\ud83c\ude36", u6e80:"\ud83c\ude35", u7121:"\ud83c\ude1a️", u7533:"\ud83c\ude38", u7981:"\ud83c\ude32", u7a7a:"\ud83c\ude33", umbrella:"☔️", unamused:"\ud83d\ude12", underage:"\ud83d\udd1e", unicorn:"\ud83e\udd84", unlock:"\ud83d\udd13", up:"\ud83c\udd99", upside_down_face:"\ud83d\ude43", v:"✌️", vertical_traffic_light:"\ud83d\udea6", vhs:"\ud83d\udcfc", vibration_mode:"\ud83d\udcf3", 
  video_camera:"\ud83d\udcf9", video_game:"\ud83c\udfae", violin:"\ud83c\udfbb", virgo:"♍️", volcano:"\ud83c\udf0b", volleyball:"\ud83c\udfd0", vs:"\ud83c\udd9a", vulcan_salute:"\ud83d\udd96", walking_man:"\ud83d\udeb6", walking_woman:"\ud83d\udeb6&zwj;♀️", waning_crescent_moon:"\ud83c\udf18", waning_gibbous_moon:"\ud83c\udf16", warning:"⚠️", wastebasket:"\ud83d\uddd1", watch:"⌚️", water_buffalo:"\ud83d\udc03", watermelon:"\ud83c\udf49", wave:"\ud83d\udc4b", wavy_dash:"〰️", waxing_crescent_moon:"\ud83c\udf12", 
  wc:"\ud83d\udebe", weary:"\ud83d\ude29", wedding:"\ud83d\udc92", weight_lifting_man:"\ud83c\udfcb️", weight_lifting_woman:"\ud83c\udfcb️&zwj;♀️", whale:"\ud83d\udc33", whale2:"\ud83d\udc0b", wheel_of_dharma:"☸️", wheelchair:"♿️", white_check_mark:"✅", white_circle:"⚪️", white_flag:"\ud83c\udff3️", white_flower:"\ud83d\udcae", white_large_square:"⬜️", white_medium_small_square:"◽️", white_medium_square:"◻️", white_small_square:"▫️", white_square_button:"\ud83d\udd33", wilted_flower:"\ud83e\udd40", 
  wind_chime:"\ud83c\udf90", wind_face:"\ud83c\udf2c", wine_glass:"\ud83c\udf77", wink:"\ud83d\ude09", wolf:"\ud83d\udc3a", woman:"\ud83d\udc69", woman_artist:"\ud83d\udc69&zwj;\ud83c\udfa8", woman_astronaut:"\ud83d\udc69&zwj;\ud83d\ude80", woman_cartwheeling:"\ud83e\udd38&zwj;♀️", woman_cook:"\ud83d\udc69&zwj;\ud83c\udf73", woman_facepalming:"\ud83e\udd26&zwj;♀️", woman_factory_worker:"\ud83d\udc69&zwj;\ud83c\udfed", woman_farmer:"\ud83d\udc69&zwj;\ud83c\udf3e", woman_firefighter:"\ud83d\udc69&zwj;\ud83d\ude92", 
  woman_health_worker:"\ud83d\udc69&zwj;⚕️", woman_judge:"\ud83d\udc69&zwj;⚖️", woman_juggling:"\ud83e\udd39&zwj;♀️", woman_mechanic:"\ud83d\udc69&zwj;\ud83d\udd27", woman_office_worker:"\ud83d\udc69&zwj;\ud83d\udcbc", woman_pilot:"\ud83d\udc69&zwj;✈️", woman_playing_handball:"\ud83e\udd3e&zwj;♀️", woman_playing_water_polo:"\ud83e\udd3d&zwj;♀️", woman_scientist:"\ud83d\udc69&zwj;\ud83d\udd2c", woman_shrugging:"\ud83e\udd37&zwj;♀️", woman_singer:"\ud83d\udc69&zwj;\ud83c\udfa4", woman_student:"\ud83d\udc69&zwj;\ud83c\udf93", 
  woman_teacher:"\ud83d\udc69&zwj;\ud83c\udfeb", woman_technologist:"\ud83d\udc69&zwj;\ud83d\udcbb", woman_with_turban:"\ud83d\udc73&zwj;♀️", womans_clothes:"\ud83d\udc5a", womans_hat:"\ud83d\udc52", women_wrestling:"\ud83e\udd3c&zwj;♀️", womens:"\ud83d\udeba", world_map:"\ud83d\uddfa", worried:"\ud83d\ude1f", wrench:"\ud83d\udd27", writing_hand:"✍️", x:"❌", yellow_heart:"\ud83d\udc9b", yen:"\ud83d\udcb4", yin_yang:"☯️", yum:"\ud83d\ude0b", zap:"⚡️", zipper_mouth_face:"\ud83e\udd10", zzz:"\ud83d\udca4", 
  octocat:'<img alt=":octocat:" height="20" width="20" align="absmiddle" src="https://assets-cdn.github.com/images/icons/emoji/octocat.png">', showdown:"<span style=\"font-family: 'Anonymous Pro', monospace; text-decoration: underline; text-decoration-style: dashed; text-decoration-color: #3e8b8a;text-underline-position: under;\">S</span>"};
  b.Converter = function(a) {
    function g(p, z) {
      z = z || null;
      if (b.helper.isString(p)) {
        z = p = b.helper.stdExtName(p);
        if (b.extensions[p]) {
          console.warn("DEPRECATION WARNING: " + p + " is an old extension that uses a deprecated loading method.Please inform the developer that the extension should be updated!");
          var I = b.extensions[p];
          "function" === typeof I && (I = I(new b.Converter));
          b.helper.isArray(I) || (I = [I]);
          p = d(I, p);
          if (!p.valid) {
            throw Error(p.error);
          }
          for (p = 0; p < I.length; ++p) {
            switch(I[p].type) {
              case "lang":
                v.push(I[p]);
                break;
              case "output":
                n.push(I[p]);
                break;
              default:
                throw Error("Extension loader error: Type unrecognized!!!");
            }
          }
          return;
        }
        if (b.helper.isUndefined(k[p])) {
          throw Error('Extension "' + p + '" could not be loaded. It was either not found or is not a valid extension.');
        }
        p = k[p];
      }
      "function" === typeof p && (p = p());
      b.helper.isArray(p) || (p = [p]);
      z = d(p, z);
      if (!z.valid) {
        throw Error(z.error);
      }
      for (z = 0; z < p.length; ++z) {
        switch(p[z].type) {
          case "lang":
            v.push(p[z]);
            break;
          case "output":
            n.push(p[z]);
        }
        if (p[z].hasOwnProperty("listeners")) {
          for (I in p[z].listeners) {
            p[z].listeners.hasOwnProperty(I) && e(I, p[z].listeners[I]);
          }
        }
      }
    }
    function e(p, z) {
      if (!b.helper.isString(p)) {
        throw Error("Invalid argument in converter.listen() method: name must be a string, but " + typeof p + " given");
      }
      if ("function" !== typeof z) {
        throw Error("Invalid argument in converter.listen() method: callback must be a function, but " + typeof z + " given");
      }
      y.hasOwnProperty(p) || (y[p] = []);
      y[p].push(z);
    }
    function l(p) {
      var z = p.match(/^\s*/)[0].length;
      return p.replace(new RegExp("^\\s{0," + z + "}", "gm"), "");
    }
    var m = {}, v = [], n = [], y = {}, D = x, C = {parsed:{}, raw:"", format:""};
    (function() {
      a = a || {};
      for (var p in u) {
        u.hasOwnProperty(p) && (m[p] = u[p]);
      }
      if ("object" === typeof a) {
        for (var z in a) {
          a.hasOwnProperty(z) && (m[z] = a[z]);
        }
      } else {
        throw Error("Converter expects the passed parameter to be an object, but " + typeof a + " was passed instead.");
      }
      m.extensions && b.helper.forEach(m.extensions, g);
    })();
    this._dispatch = function(p, z, I, W) {
      if (y.hasOwnProperty(p)) {
        for (var ba = 0; ba < y[p].length; ++ba) {
          var M = y[p][ba](p, z, this, I, W);
          M && "undefined" !== typeof M && (z = M);
        }
      }
      return z;
    };
    this.listen = function(p, z) {
      e(p, z);
      return this;
    };
    this.makeHtml = function(p) {
      if (!p) {
        return p;
      }
      var z = {gHtmlBlocks:[], gHtmlMdBlocks:[], gHtmlSpans:[], gUrls:{}, gTitles:{}, gDimensions:{}, gListLevel:0, hashLinkCounts:{}, langExtensions:v, outputModifiers:n, converter:this, ghCodeBlocks:[], metadata:{parsed:{}, raw:"", format:""}};
      p = p.replace(/¨/g, "¨T");
      p = p.replace(/\$/g, "¨D");
      p = p.replace(/\r\n/g, "\n");
      p = p.replace(/\r/g, "\n");
      p = p.replace(/\u00A0/g, "&nbsp;");
      m.smartIndentationFix && (p = l(p));
      p = "\n\n" + p + "\n\n";
      p = b.subParser("detab")(p, m, z);
      p = p.replace(/^[ \t]+$/mg, "");
      b.helper.forEach(v, function(I) {
        p = b.subParser("runExtension")(I, p, m, z);
      });
      p = b.subParser("metadata")(p, m, z);
      p = b.subParser("hashPreCodeTags")(p, m, z);
      p = b.subParser("githubCodeBlocks")(p, m, z);
      p = b.subParser("hashHTMLBlocks")(p, m, z);
      p = b.subParser("hashCodeTags")(p, m, z);
      p = b.subParser("stripLinkDefinitions")(p, m, z);
      p = b.subParser("blockGamut")(p, m, z);
      p = b.subParser("unhashHTMLSpans")(p, m, z);
      p = b.subParser("unescapeSpecialChars")(p, m, z);
      p = p.replace(/¨D/g, "$$");
      p = p.replace(/¨T/g, "¨");
      p = b.subParser("completeHTMLDocument")(p, m, z);
      b.helper.forEach(n, function(I) {
        p = b.subParser("runExtension")(I, p, m, z);
      });
      C = z.metadata;
      return p;
    };
    this.makeMarkdown = this.makeMd = function(p, z) {
      function I(M) {
        for (var ea = 0; ea < M.childNodes.length; ++ea) {
          var q = M.childNodes[ea];
          3 === q.nodeType ? /\S/.test(q.nodeValue) ? (q.nodeValue = q.nodeValue.split("\n").join(" "), q.nodeValue = q.nodeValue.replace(/(\s)+/g, "$1")) : (M.removeChild(q), --ea) : 1 === q.nodeType && I(q);
        }
      }
      p = p.replace(/\r\n/g, "\n");
      p = p.replace(/\r/g, "\n");
      p = p.replace(/>[ \t]+</, ">¨NBSP;<");
      if (!z) {
        if (window && window.document) {
          z = window.document;
        } else {
          throw Error("HTMLParser is undefined. If in a webworker or nodejs environment, you need to provide a WHATWG DOM and HTML such as JSDOM");
        }
      }
      z = z.createElement("div");
      z.innerHTML = p;
      p = {preList:function(M) {
        M = M.querySelectorAll("pre");
        for (var ea = [], q = 0; q < M.length; ++q) {
          if (1 === M[q].childElementCount && "code" === M[q].firstChild.tagName.toLowerCase()) {
            var E = M[q].firstChild.innerHTML.trim(), J = M[q].firstChild.getAttribute("data-language") || "";
            if ("" === J) {
              for (var N = M[q].firstChild.className.split(" "), O = 0; O < N.length; ++O) {
                var K = N[O].match(/^language-(.+)$/);
                if (null !== K) {
                  J = K[1];
                  break;
                }
              }
            }
            E = b.helper.unescapeHTMLEntities(E);
            ea.push(E);
            M[q].outerHTML = '<precode language="' + J + '" precodenum="' + q.toString() + '"></precode>';
          } else {
            ea.push(M[q].innerHTML), M[q].innerHTML = "", M[q].setAttribute("prenum", q.toString());
          }
        }
        return ea;
      }(z)};
      I(z);
      z = z.childNodes;
      for (var W = "", ba = 0; ba < z.length; ba++) {
        W += b.subParser("makeMarkdown.node")(z[ba], p);
      }
      return W;
    };
    this.setOption = function(p, z) {
      m[p] = z;
    };
    this.getOption = function(p) {
      return m[p];
    };
    this.getOptions = function() {
      return m;
    };
    this.addExtension = function(p, z) {
      g(p, z || null);
    };
    this.useExtension = function(p) {
      g(p);
    };
    this.setFlavor = function(p) {
      if (!t.hasOwnProperty(p)) {
        throw Error(p + " flavor was not found");
      }
      var z = t[p];
      D = p;
      for (var I in z) {
        z.hasOwnProperty(I) && (m[I] = z[I]);
      }
    };
    this.getFlavor = function() {
      return D;
    };
    this.removeExtension = function(p) {
      b.helper.isArray(p) || (p = [p]);
      for (var z = 0; z < p.length; ++z) {
        for (var I = p[z], W = 0; W < v.length; ++W) {
          v[W] === I && v[W].splice(W, 1);
        }
        for (; 0 < n.length; ++W) {
          n[0] === I && n[0].splice(W, 1);
        }
      }
    };
    this.getAllExtensions = function() {
      return {language:v, output:n};
    };
    this.getMetadata = function(p) {
      return p ? C.raw : C.parsed;
    };
    this.getMetadataFormat = function() {
      return C.format;
    };
    this._setMetadataPair = function(p, z) {
      C.parsed[p] = z;
    };
    this._setMetadataFormat = function(p) {
      C.format = p;
    };
    this._setMetadataRaw = function(p) {
      C.raw = p;
    };
  };
  b.subParser("anchors", function(a, g, e) {
    a = e.converter._dispatch("anchors.before", a, g, e);
    var l = function(m, v, n, y, D, C, p) {
      b.helper.isUndefined(p) && (p = "");
      n = n.toLowerCase();
      if (-1 < m.search(/\(<?\s*>? ?(['"].*['"])?\)$/m)) {
        y = "";
      } else {
        if (!y) {
          n || (n = v.toLowerCase().replace(/ ?\n/g, " "));
          if (b.helper.isUndefined(e.gUrls[n])) {
            return m;
          }
          y = e.gUrls[n];
          b.helper.isUndefined(e.gTitles[n]) || (p = e.gTitles[n]);
        }
      }
      y = y.replace(b.helper.regexes.asteriskDashAndColon, b.helper.escapeCharactersCallback);
      m = '<a href="' + y + '"';
      "" !== p && null !== p && (p = p.replace(/"/g, "&quot;"), p = p.replace(b.helper.regexes.asteriskDashAndColon, b.helper.escapeCharactersCallback), m += ' title="' + p + '"');
      g.openLinksInNewWindow && !/^#/.test(y) && (m += ' rel="noopener noreferrer" target="¨E95Eblank"');
      return m + (">" + v + "</a>");
    };
    a = a.replace(/\[((?:\[[^\]]*]|[^\[\]])*)] ?(?:\n *)?\[(.*?)]()()()()/g, l);
    a = a.replace(/\[((?:\[[^\]]*]|[^\[\]])*)]()[ \t]*\([ \t]?<([^>]*)>(?:[ \t]*((["'])([^"]*?)\5))?[ \t]?\)/g, l);
    a = a.replace(/\[((?:\[[^\]]*]|[^\[\]])*)]()[ \t]*\([ \t]?<?([\S]+?(?:\([\S]*?\)[\S]*?)?)>?(?:[ \t]*((["'])([^"]*?)\5))?[ \t]?\)/g, l);
    a = a.replace(/\[([^\[\]]+)]()()()()()/g, l);
    g.ghMentions && (a = a.replace(/(^|\s)(\\)?(@([a-z\d]+(?:[a-z\d.-]+?[a-z\d]+)*))/gmi, function(m, v, n, y, D) {
      if ("\\" === n) {
        return v + y;
      }
      if (!b.helper.isString(g.ghMentionsLink)) {
        throw Error("ghMentionsLink option must be a string");
      }
      m = g.ghMentionsLink.replace(/\{u}/g, D);
      n = "";
      g.openLinksInNewWindow && (n = ' rel="noopener noreferrer" target="¨E95Eblank"');
      return v + '<a href="' + m + '"' + n + ">" + y + "</a>";
    }));
    return a = e.converter._dispatch("anchors.after", a, g, e);
  });
  var w = /([*~_]+|\b)(((https?|ftp|dict):\/\/|www\.)[^'">\s]+?\.[^'">\s]+?)()(\1)?(?=\s|$)(?!["<>])/gi, B = /([*~_]+|\b)(((https?|ftp|dict):\/\/|www\.)[^'">\s]+\.[^'">\s]+?)([.!?,()\[\]])?(\1)?(?=\s|$)(?!["<>])/gi, P = /()<(((https?|ftp|dict):\/\/|www\.)[^'">\s]+)()>()/gi, ca = /(^|\s)(?:mailto:)?([A-Za-z0-9!#$%&'*+-/=?^_`{|}~.]+@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]+)(?=$|\s)/gmi, R = /<()(?:mailto:)?([-.\w]+@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]+)>/gi, Y = function(a) {
    return function(g, e, l, m, v, n, y) {
      g = l = l.replace(b.helper.regexes.asteriskDashAndColon, b.helper.escapeCharactersCallback);
      v = m = "";
      e = e || "";
      y = y || "";
      /^www\./i.test(l) && (l = l.replace(/^www\./i, "http://www."));
      a.excludeTrailingPunctuationFromURLs && n && (m = n);
      a.openLinksInNewWindow && (v = ' rel="noopener noreferrer" target="¨E95Eblank"');
      return e + '<a href="' + l + '"' + v + ">" + g + "</a>" + m + y;
    };
  }, X = function(a, g) {
    return function(e, l, m) {
      e = "mailto:";
      l = l || "";
      m = b.subParser("unescapeSpecialChars")(m, a, g);
      a.encodeEmails ? (e = b.helper.encodeEmailAddress(e + m), m = b.helper.encodeEmailAddress(m)) : e += m;
      return l + '<a href="' + e + '">' + m + "</a>";
    };
  };
  b.subParser("autoLinks", function(a, g, e) {
    a = e.converter._dispatch("autoLinks.before", a, g, e);
    a = a.replace(P, Y(g));
    a = a.replace(R, X(g, e));
    return a = e.converter._dispatch("autoLinks.after", a, g, e);
  });
  b.subParser("simplifiedAutoLinks", function(a, g, e) {
    if (!g.simplifiedAutoLink) {
      return a;
    }
    a = e.converter._dispatch("simplifiedAutoLinks.before", a, g, e);
    a = g.excludeTrailingPunctuationFromURLs ? a.replace(B, Y(g)) : a.replace(w, Y(g));
    a = a.replace(ca, X(g, e));
    return a = e.converter._dispatch("simplifiedAutoLinks.after", a, g, e);
  });
  b.subParser("blockGamut", function(a, g, e) {
    a = e.converter._dispatch("blockGamut.before", a, g, e);
    a = b.subParser("blockQuotes")(a, g, e);
    a = b.subParser("headers")(a, g, e);
    a = b.subParser("horizontalRule")(a, g, e);
    a = b.subParser("lists")(a, g, e);
    a = b.subParser("codeBlocks")(a, g, e);
    a = b.subParser("tables")(a, g, e);
    a = b.subParser("hashHTMLBlocks")(a, g, e);
    a = b.subParser("paragraphs")(a, g, e);
    return a = e.converter._dispatch("blockGamut.after", a, g, e);
  });
  b.subParser("blockQuotes", function(a, g, e) {
    a = e.converter._dispatch("blockQuotes.before", a, g, e);
    var l = /(^ {0,3}>[ \t]?.+\n(.+\n)*\n*)+/gm;
    g.splitAdjacentBlockquotes && (l = /^ {0,3}>[\s\S]*?(?:\n\n)/gm);
    a = (a + "\n\n").replace(l, function(m) {
      m = m.replace(/^[ \t]*>[ \t]?/gm, "");
      m = m.replace(/¨0/g, "");
      m = m.replace(/^[ \t]+$/gm, "");
      m = b.subParser("githubCodeBlocks")(m, g, e);
      m = b.subParser("blockGamut")(m, g, e);
      m = m.replace(/(^|\n)/g, "$1  ");
      m = m.replace(/(\s*<pre>[^\r]+?<\/pre>)/gm, function(v, n) {
        v = n.replace(/^  /mg, "¨0");
        return v = v.replace(/¨0/g, "");
      });
      return b.subParser("hashBlock")("<blockquote>\n" + m + "\n</blockquote>", g, e);
    });
    return a = e.converter._dispatch("blockQuotes.after", a, g, e);
  });
  b.subParser("codeBlocks", function(a, g, e) {
    a = e.converter._dispatch("codeBlocks.before", a, g, e);
    a = (a + "¨0").replace(/(?:\n\n|^)((?:(?:[ ]{4}|\t).*\n+)+)(\n*[ ]{0,3}[^ \t\n]|(?=¨0))/g, function(l, m, v) {
      l = m;
      m = "\n";
      l = b.subParser("outdent")(l, g, e);
      l = b.subParser("encodeCode")(l, g, e);
      l = b.subParser("detab")(l, g, e);
      l = l.replace(/^\n+/g, "");
      l = l.replace(/\n+$/g, "");
      g.omitExtraWLInCodeBlocks && (m = "");
      l = "<pre><code>" + l + m + "</code></pre>";
      return b.subParser("hashBlock")(l, g, e) + v;
    });
    a = a.replace(/¨0/, "");
    return a = e.converter._dispatch("codeBlocks.after", a, g, e);
  });
  b.subParser("codeSpans", function(a, g, e) {
    a = e.converter._dispatch("codeSpans.before", a, g, e);
    "undefined" === typeof a && (a = "");
    a = a.replace(/(^|[^\\])(`+)([^\r]*?[^`])\2(?!`)/gm, function(l, m, v, n) {
      l = n.replace(/^([ \t]*)/g, "");
      l = l.replace(/[ \t]*$/g, "");
      l = b.subParser("encodeCode")(l, g, e);
      l = m + "<code>" + l + "</code>";
      return l = b.subParser("hashHTMLSpans")(l, g, e);
    });
    return a = e.converter._dispatch("codeSpans.after", a, g, e);
  });
  b.subParser("completeHTMLDocument", function(a, g, e) {
    if (!g.completeHTMLDocument) {
      return a;
    }
    a = e.converter._dispatch("completeHTMLDocument.before", a, g, e);
    var l = "html", m = "<!DOCTYPE HTML>\n", v = "", n = '<meta charset="utf-8">\n', y = "", D = "";
    "undefined" !== typeof e.metadata.parsed.doctype && (m = "<!DOCTYPE " + e.metadata.parsed.doctype + ">\n", l = e.metadata.parsed.doctype.toString().toLowerCase(), "html" === l || "html5" === l) && (n = '<meta charset="utf-8">');
    for (var C in e.metadata.parsed) {
      if (e.metadata.parsed.hasOwnProperty(C)) {
        switch(C.toLowerCase()) {
          case "doctype":
            break;
          case "title":
            v = "<title>" + e.metadata.parsed.title + "</title>\n";
            break;
          case "charset":
            n = "html" === l || "html5" === l ? '<meta charset="' + e.metadata.parsed.charset + '">\n' : '<meta name="charset" content="' + e.metadata.parsed.charset + '">\n';
            break;
          case "language":
          case "lang":
            y = ' lang="' + e.metadata.parsed[C] + '"';
            D += '<meta name="' + C + '" content="' + e.metadata.parsed[C] + '">\n';
            break;
          default:
            D += '<meta name="' + C + '" content="' + e.metadata.parsed[C] + '">\n';
        }
      }
    }
    a = m + "<html" + y + ">\n<head>\n" + v + n + D + "</head>\n<body>\n" + a.trim() + "\n</body>\n</html>";
    return a = e.converter._dispatch("completeHTMLDocument.after", a, g, e);
  });
  b.subParser("detab", function(a, g, e) {
    a = e.converter._dispatch("detab.before", a, g, e);
    a = a.replace(/\t(?=\t)/g, "    ");
    a = a.replace(/\t/g, "¨A¨B");
    a = a.replace(/¨B(.+?)¨A/g, function(l, m) {
      l = m;
      m = 4 - l.length % 4;
      for (var v = 0; v < m; v++) {
        l += " ";
      }
      return l;
    });
    a = a.replace(/¨A/g, "    ");
    a = a.replace(/¨B/g, "");
    return a = e.converter._dispatch("detab.after", a, g, e);
  });
  b.subParser("ellipsis", function(a, g, e) {
    a = e.converter._dispatch("ellipsis.before", a, g, e);
    a = a.replace(/\.\.\./g, "…");
    return a = e.converter._dispatch("ellipsis.after", a, g, e);
  });
  b.subParser("emoji", function(a, g, e) {
    if (!g.emoji) {
      return a;
    }
    a = e.converter._dispatch("emoji.before", a, g, e);
    a = a.replace(/:([\S]+?):/g, function(l, m) {
      return b.helper.emojis.hasOwnProperty(m) ? b.helper.emojis[m] : l;
    });
    return a = e.converter._dispatch("emoji.after", a, g, e);
  });
  b.subParser("encodeAmpsAndAngles", function(a, g, e) {
    a = e.converter._dispatch("encodeAmpsAndAngles.before", a, g, e);
    a = a.replace(/&(?!#?[xX]?(?:[0-9a-fA-F]+|\w+);)/g, "&amp;");
    a = a.replace(/<(?![a-z\/?$!])/gi, "&lt;");
    a = a.replace(/</g, "&lt;");
    a = a.replace(/>/g, "&gt;");
    return a = e.converter._dispatch("encodeAmpsAndAngles.after", a, g, e);
  });
  b.subParser("encodeBackslashEscapes", function(a, g, e) {
    a = e.converter._dispatch("encodeBackslashEscapes.before", a, g, e);
    a = a.replace(/\\(\\)/g, b.helper.escapeCharactersCallback);
    a = a.replace(/\\([`*_{}\[\]()>#+.!~=|-])/g, b.helper.escapeCharactersCallback);
    return a = e.converter._dispatch("encodeBackslashEscapes.after", a, g, e);
  });
  b.subParser("encodeCode", function(a, g, e) {
    a = e.converter._dispatch("encodeCode.before", a, g, e);
    a = a.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/([*_{}\[\]\\=~-])/g, b.helper.escapeCharactersCallback);
    return a = e.converter._dispatch("encodeCode.after", a, g, e);
  });
  b.subParser("escapeSpecialCharsWithinTagAttributes", function(a, g, e) {
    a = e.converter._dispatch("escapeSpecialCharsWithinTagAttributes.before", a, g, e);
    a = a.replace(/<\/?[a-z\d_:-]+(?:[\s]+[\s\S]+?)?>/gi, function(l) {
      return l.replace(/(.)<\/?code>(?=.)/g, "$1`").replace(/([\\`*_~=|])/g, b.helper.escapeCharactersCallback);
    });
    a = a.replace(/<!(--(?:(?:[^>-]|-[^>])(?:[^-]|-[^-])*)--)>/gi, function(l) {
      return l.replace(/([\\`*_~=|])/g, b.helper.escapeCharactersCallback);
    });
    return a = e.converter._dispatch("escapeSpecialCharsWithinTagAttributes.after", a, g, e);
  });
  b.subParser("githubCodeBlocks", function(a, g, e) {
    if (!g.ghCodeBlocks) {
      return a;
    }
    a = e.converter._dispatch("githubCodeBlocks.before", a, g, e);
    a = (a + "¨0").replace(/(?:^|\n)(?: {0,3})(```+|~~~+)(?: *)([^\s`~]*)\n([\s\S]*?)\n(?: {0,3})\1/g, function(l, m, v, n) {
      m = g.omitExtraWLInCodeBlocks ? "" : "\n";
      n = b.subParser("encodeCode")(n, g, e);
      n = b.subParser("detab")(n, g, e);
      n = n.replace(/^\n+/g, "");
      n = n.replace(/\n+$/g, "");
      n = "<pre><code" + (v ? ' class="' + v + " language-" + v + '"' : "") + ">" + n + m + "</code></pre>";
      n = b.subParser("hashBlock")(n, g, e);
      return "\n\n¨G" + (e.ghCodeBlocks.push({text:l, codeblock:n}) - 1) + "G\n\n";
    });
    a = a.replace(/¨0/, "");
    return e.converter._dispatch("githubCodeBlocks.after", a, g, e);
  });
  b.subParser("hashBlock", function(a, g, e) {
    a = e.converter._dispatch("hashBlock.before", a, g, e);
    a = a.replace(/(^\n+|\n+$)/g, "");
    a = "\n\n¨K" + (e.gHtmlBlocks.push(a) - 1) + "K\n\n";
    return a = e.converter._dispatch("hashBlock.after", a, g, e);
  });
  b.subParser("hashCodeTags", function(a, g, e) {
    a = e.converter._dispatch("hashCodeTags.before", a, g, e);
    a = b.helper.replaceRecursiveRegExp(a, function(l, m, v, n) {
      l = v + b.subParser("encodeCode")(m, g, e) + n;
      return "¨C" + (e.gHtmlSpans.push(l) - 1) + "C";
    }, "<code\\b[^>]*>", "</code>", "gim");
    return a = e.converter._dispatch("hashCodeTags.after", a, g, e);
  });
  b.subParser("hashElement", function(a, g, e) {
    return function(l, m) {
      l = m.replace(/\n\n/g, "\n");
      l = l.replace(/^\n/, "");
      l = l.replace(/\n+$/g, "");
      return l = "\n\n¨K" + (e.gHtmlBlocks.push(l) - 1) + "K\n\n";
    };
  });
  b.subParser("hashHTMLBlocks", function(a, g, e) {
    a = e.converter._dispatch("hashHTMLBlocks.before", a, g, e);
    var l = "pre div h1 h2 h3 h4 h5 h6 blockquote table dl ol ul script noscript form fieldset iframe math style section header footer nav article aside address audio canvas figure hgroup output video p".split(" "), m = function(z, I, W, ba) {
      -1 !== W.search(/\bmarkdown\b/) && (z = W + e.converter.makeHtml(I) + ba);
      return "\n\n¨K" + (e.gHtmlBlocks.push(z) - 1) + "K\n\n";
    };
    g.backslashEscapesHTMLTags && (a = a.replace(/\\<(\/?[^>]+?)>/g, function(z, I) {
      return "&lt;" + I + "&gt;";
    }));
    for (var v = 0; v < l.length; ++v) {
      for (var n, y = new RegExp("^ {0,3}(<" + l[v] + "\\b[^>]*>)", "im"), D = "<" + l[v] + "\\b[^>]*>", C = "</" + l[v] + ">"; -1 !== (n = b.helper.regexIndexOf(a, y));) {
        n = b.helper.splitAtIndex(a, n);
        var p = b.helper.replaceRecursiveRegExp(n[1], m, D, C, "im");
        if (p === n[1]) {
          break;
        }
        a = n[0].concat(p);
      }
    }
    a = a.replace(/(\n {0,3}(<(hr)\b([^<>])*?\/?>)[ \t]*(?=\n{2,}))/g, b.subParser("hashElement")(a, g, e));
    a = b.helper.replaceRecursiveRegExp(a, function(z) {
      return "\n\n¨K" + (e.gHtmlBlocks.push(z) - 1) + "K\n\n";
    }, "^ {0,3}\x3c!--", "--\x3e", "gm");
    a = a.replace(/(?:\n\n)( {0,3}(?:<([?%])[^\r]*?\2>)[ \t]*(?=\n{2,}))/g, b.subParser("hashElement")(a, g, e));
    return a = e.converter._dispatch("hashHTMLBlocks.after", a, g, e);
  });
  b.subParser("hashHTMLSpans", function(a, g, e) {
    function l(m) {
      return "¨C" + (e.gHtmlSpans.push(m) - 1) + "C";
    }
    a = e.converter._dispatch("hashHTMLSpans.before", a, g, e);
    a = a.replace(/<[^>]+?\/>/gi, function(m) {
      return l(m);
    });
    a = a.replace(/<([^>]+?)>[\s\S]*?<\/\1>/g, function(m) {
      return l(m);
    });
    a = a.replace(/<([^>]+?)\s[^>]+?>[\s\S]*?<\/\1>/g, function(m) {
      return l(m);
    });
    a = a.replace(/<[^>]+?>/gi, function(m) {
      return l(m);
    });
    return a = e.converter._dispatch("hashHTMLSpans.after", a, g, e);
  });
  b.subParser("unhashHTMLSpans", function(a, g, e) {
    a = e.converter._dispatch("unhashHTMLSpans.before", a, g, e);
    for (var l = 0; l < e.gHtmlSpans.length; ++l) {
      for (var m = e.gHtmlSpans[l], v = 0; /¨C(\d+)C/.test(m);) {
        var n = RegExp.$1;
        m = m.replace("¨C" + n + "C", e.gHtmlSpans[n]);
        if (10 === v) {
          console.error("maximum nesting of 10 spans reached!!!");
          break;
        }
        ++v;
      }
      a = a.replace("¨C" + l + "C", m);
    }
    return a = e.converter._dispatch("unhashHTMLSpans.after", a, g, e);
  });
  b.subParser("hashPreCodeTags", function(a, g, e) {
    a = e.converter._dispatch("hashPreCodeTags.before", a, g, e);
    a = b.helper.replaceRecursiveRegExp(a, function(l, m, v, n) {
      m = v + b.subParser("encodeCode")(m, g, e) + n;
      return "\n\n¨G" + (e.ghCodeBlocks.push({text:l, codeblock:m}) - 1) + "G\n\n";
    }, "^ {0,3}<pre\\b[^>]*>\\s*<code\\b[^>]*>", "^ {0,3}</code>\\s*</pre>", "gim");
    return a = e.converter._dispatch("hashPreCodeTags.after", a, g, e);
  });
  b.subParser("headers", function(a, g, e) {
    function l(n) {
      var y;
      g.customizedHeaderId && (y = n.match(/\{([^{]+?)}\s*$/)) && y[1] && (n = y[1]);
      y = b.helper.isString(g.prefixHeaderId) ? g.prefixHeaderId : !0 === g.prefixHeaderId ? "section-" : "";
      g.rawPrefixHeaderId || (n = y + n);
      n = g.ghCompatibleHeaderId ? n.replace(/ /g, "-").replace(/&amp;/g, "").replace(/¨T/g, "").replace(/¨D/g, "").replace(/[&+$,\/:;=?@"#{}|^¨~\[\]`\\*)(%.!'<>]/g, "").toLowerCase() : g.rawHeaderId ? n.replace(/ /g, "-").replace(/&amp;/g, "&").replace(/¨T/g, "¨").replace(/¨D/g, "$").replace(/["']/g, "-").toLowerCase() : n.replace(/[^\w]/g, "").toLowerCase();
      g.rawPrefixHeaderId && (n = y + n);
      e.hashLinkCounts[n] ? n = n + "-" + e.hashLinkCounts[n]++ : e.hashLinkCounts[n] = 1;
      return n;
    }
    a = e.converter._dispatch("headers.before", a, g, e);
    var m = isNaN(parseInt(g.headerLevelStart)) ? 1 : parseInt(g.headerLevelStart), v = g.smoothLivePreview ? /^(.+)[ \t]*\n-{2,}[ \t]*\n+/gm : /^(.+)[ \t]*\n-+[ \t]*\n+/gm;
    a = a.replace(g.smoothLivePreview ? /^(.+)[ \t]*\n={2,}[ \t]*\n+/gm : /^(.+)[ \t]*\n=+[ \t]*\n+/gm, function(n, y) {
      n = b.subParser("spanGamut")(y, g, e);
      y = g.noHeaderId ? "" : ' id="' + l(y) + '"';
      y = "<h" + m + y + ">" + n + "</h" + m + ">";
      return b.subParser("hashBlock")(y, g, e);
    });
    a = a.replace(v, function(n, y) {
      n = b.subParser("spanGamut")(y, g, e);
      y = g.noHeaderId ? "" : ' id="' + l(y) + '"';
      var D = m + 1;
      n = "<h" + D + y + ">" + n + "</h" + D + ">";
      return b.subParser("hashBlock")(n, g, e);
    });
    a = a.replace(g.requireSpaceBeforeHeadingText ? /^(#{1,6})[ \t]+(.+?)[ \t]*#*\n+/gm : /^(#{1,6})[ \t]*(.+?)[ \t]*#*\n+/gm, function(n, y, D) {
      n = D;
      g.customizedHeaderId && (n = D.replace(/\s?\{([^{]+?)}\s*$/, ""));
      n = b.subParser("spanGamut")(n, g, e);
      D = g.noHeaderId ? "" : ' id="' + l(D) + '"';
      y = m - 1 + y.length;
      y = "<h" + y + D + ">" + n + "</h" + y + ">";
      return b.subParser("hashBlock")(y, g, e);
    });
    return a = e.converter._dispatch("headers.after", a, g, e);
  });
  b.subParser("horizontalRule", function(a, g, e) {
    a = e.converter._dispatch("horizontalRule.before", a, g, e);
    var l = b.subParser("hashBlock")("<hr />", g, e);
    a = a.replace(/^ {0,2}( ?-){3,}[ \t]*$/gm, l);
    a = a.replace(/^ {0,2}( ?\*){3,}[ \t]*$/gm, l);
    a = a.replace(/^ {0,2}( ?_){3,}[ \t]*$/gm, l);
    return a = e.converter._dispatch("horizontalRule.after", a, g, e);
  });
  b.subParser("images", function(a, g, e) {
    function l(m, v, n, y, D, C, p, z) {
      p = e.gUrls;
      var I = e.gTitles, W = e.gDimensions;
      n = n.toLowerCase();
      z || (z = "");
      if (-1 < m.search(/\(<?\s*>? ?(['"].*['"])?\)$/m)) {
        y = "";
      } else {
        if ("" === y || null === y) {
          if ("" === n || null === n) {
            n = v.toLowerCase().replace(/ ?\n/g, " ");
          }
          if (b.helper.isUndefined(p[n])) {
            return m;
          }
          y = p[n];
          b.helper.isUndefined(I[n]) || (z = I[n]);
          b.helper.isUndefined(W[n]) || (D = W[n].width, C = W[n].height);
        }
      }
      v = v.replace(/"/g, "&quot;").replace(b.helper.regexes.asteriskDashAndColon, b.helper.escapeCharactersCallback);
      y = y.replace(b.helper.regexes.asteriskDashAndColon, b.helper.escapeCharactersCallback);
      m = '<img src="' + y + '" alt="' + v + '"';
      z && b.helper.isString(z) && (z = z.replace(/"/g, "&quot;").replace(b.helper.regexes.asteriskDashAndColon, b.helper.escapeCharactersCallback), m += ' title="' + z + '"');
      D && C && (m = m + (' width="' + ("*" === D ? "auto" : D) + '" height="') + (("*" === C ? "auto" : C) + '"'));
      return m + " />";
    }
    a = e.converter._dispatch("images.before", a, g, e);
    a = a.replace(/!\[([^\]]*?)] ?(?:\n *)?\[([\s\S]*?)]()()()()()/g, l);
    a = a.replace(/!\[([^\]]*?)][ \t]*()\([ \t]?<?(data:.+?\/.+?;base64,[A-Za-z0-9+/=\n]+?)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*(?:(["'])([^"]*?)\6)?[ \t]?\)/g, function(m, v, n, y, D, C, p, z) {
      y = y.replace(/\s/g, "");
      return l(m, v, n, y, D, C, p, z);
    });
    a = a.replace(/!\[([^\]]*?)][ \t]*()\([ \t]?<([^>]*)>(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*(?:(?:(["'])([^"]*?)\6))?[ \t]?\)/g, l);
    a = a.replace(/!\[([^\]]*?)][ \t]*()\([ \t]?<?([\S]+?(?:\([\S]*?\)[\S]*?)?)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*(?:(["'])([^"]*?)\6)?[ \t]?\)/g, l);
    a = a.replace(/!\[([^\[\]]+)]()()()()()/g, l);
    return a = e.converter._dispatch("images.after", a, g, e);
  });
  b.subParser("italicsAndBold", function(a, g, e) {
    a = e.converter._dispatch("italicsAndBold.before", a, g, e);
    g.literalMidWordUnderscores ? (a = a.replace(/\b___(\S[\s\S]*?)___\b/g, function(l, m) {
      return "<strong><em>" + m + "</em></strong>";
    }), a = a.replace(/\b__(\S[\s\S]*?)__\b/g, function(l, m) {
      return "<strong>" + m + "</strong>";
    }), a = a.replace(/\b_(\S[\s\S]*?)_\b/g, function(l, m) {
      return "<em>" + m + "</em>";
    })) : (a = a.replace(/___(\S[\s\S]*?)___/g, function(l, m) {
      return /\S$/.test(m) ? "<strong><em>" + m + "</em></strong>" : l;
    }), a = a.replace(/__(\S[\s\S]*?)__/g, function(l, m) {
      return /\S$/.test(m) ? "<strong>" + m + "</strong>" : l;
    }), a = a.replace(/_([^\s_][\s\S]*?)_/g, function(l, m) {
      return /\S$/.test(m) ? "<em>" + m + "</em>" : l;
    }));
    g.literalMidWordAsterisks ? (a = a.replace(/([^*]|^)\B\*\*\*(\S[\s\S]*?)\*\*\*\B(?!\*)/g, function(l, m, v) {
      return m + "<strong><em>" + v + "</em></strong>";
    }), a = a.replace(/([^*]|^)\B\*\*(\S[\s\S]*?)\*\*\B(?!\*)/g, function(l, m, v) {
      return m + "<strong>" + v + "</strong>";
    }), a = a.replace(/([^*]|^)\B\*(\S[\s\S]*?)\*\B(?!\*)/g, function(l, m, v) {
      return m + "<em>" + v + "</em>";
    })) : (a = a.replace(/\*\*\*(\S[\s\S]*?)\*\*\*/g, function(l, m) {
      return /\S$/.test(m) ? "<strong><em>" + m + "</em></strong>" : l;
    }), a = a.replace(/\*\*(\S[\s\S]*?)\*\*/g, function(l, m) {
      return /\S$/.test(m) ? "<strong>" + m + "</strong>" : l;
    }), a = a.replace(/\*([^\s*][\s\S]*?)\*/g, function(l, m) {
      return /\S$/.test(m) ? "<em>" + m + "</em>" : l;
    }));
    return a = e.converter._dispatch("italicsAndBold.after", a, g, e);
  });
  b.subParser("lists", function(a, g, e) {
    function l(n, y) {
      e.gListLevel++;
      n = n.replace(/\n{2,}$/, "\n");
      n += "¨0";
      var D = /(\n)?(^ {0,3})([*+-]|\d+[.])[ \t]+((\[(x|X| )?])?[ \t]*[^\r]+?(\n{1,2}))(?=\n*(¨0| {0,3}([*+-]|\d+[.])[ \t]+))/gm, C = /\n[ \t]*\n(?!¨0)/.test(n);
      g.disableForced4SpacesIndentedSublists && (D = /(\n)?(^ {0,3})([*+-]|\d+[.])[ \t]+((\[(x|X| )?])?[ \t]*[^\r]+?(\n{1,2}))(?=\n*(¨0|\2([*+-]|\d+[.])[ \t]+))/gm);
      n = n.replace(D, function(p, z, I, W, ba, M, ea) {
        ea = ea && "" !== ea.trim();
        p = b.subParser("outdent")(ba, g, e);
        I = "";
        M && g.tasklists && (I = ' class="task-list-item" style="list-style-type: none;"', p = p.replace(/^[ \t]*\[(x|X| )?]/m, function() {
          var q = '<input type="checkbox" disabled style="margin: 0px 0.35em 0.25em -1.6em; vertical-align: middle;"';
          ea && (q += " checked");
          return q + ">";
        }));
        p = p.replace(/^([-*+]|\d\.)[ \t]+[\S\n ]*/g, function(q) {
          return "¨A" + q;
        });
        z || -1 < p.search(/\n{2,}/) ? (p = b.subParser("githubCodeBlocks")(p, g, e), p = b.subParser("blockGamut")(p, g, e)) : (p = b.subParser("lists")(p, g, e), p = p.replace(/\n$/, ""), p = b.subParser("hashHTMLBlocks")(p, g, e), p = p.replace(/\n\n+/g, "\n\n"), p = C ? b.subParser("paragraphs")(p, g, e) : b.subParser("spanGamut")(p, g, e));
        p = p.replace("¨A", "");
        return "<li" + I + ">" + p + "</li>\n";
      });
      n = n.replace(/¨0/g, "");
      e.gListLevel--;
      y && (n = n.replace(/\s+$/, ""));
      return n;
    }
    function m(n, y) {
      return "ol" === y && (n = n.match(/^ *(\d+)\./)) && "1" !== n[1] ? ' start="' + n[1] + '"' : "";
    }
    function v(n, y, D) {
      var C = g.disableForced4SpacesIndentedSublists ? /^ ?\d+\.[ \t]/gm : /^ {0,3}\d+\.[ \t]/gm, p = g.disableForced4SpacesIndentedSublists ? /^ ?[*+-][ \t]/gm : /^ {0,3}[*+-][ \t]/gm, z = "ul" === y ? C : p, I = "";
      if (-1 !== n.search(z)) {
        (function ea(M) {
          var q = M.search(z), E = m(n, y);
          -1 !== q ? (I += "\n\n<" + y + E + ">\n" + l(M.slice(0, q), !!D) + "</" + y + ">\n", y = "ul" === y ? "ol" : "ul", z = "ul" === y ? C : p, ea(M.slice(q))) : I += "\n\n<" + y + E + ">\n" + l(M, !!D) + "</" + y + ">\n";
        })(n);
      } else {
        var W = m(n, y);
        I = "\n\n<" + y + W + ">\n" + l(n, !!D) + "</" + y + ">\n";
      }
      return I;
    }
    a = e.converter._dispatch("lists.before", a, g, e);
    a += "¨0";
    a = e.gListLevel ? a.replace(/^(( {0,3}([*+-]|\d+[.])[ \t]+)[^\r]+?(¨0|\n{2,}(?=\S)(?![ \t]*(?:[*+-]|\d+[.])[ \t]+)))/gm, function(n, y, D) {
      n = -1 < D.search(/[*+-]/g) ? "ul" : "ol";
      return v(y, n, !0);
    }) : a.replace(/(\n\n|^\n?)(( {0,3}([*+-]|\d+[.])[ \t]+)[^\r]+?(¨0|\n{2,}(?=\S)(?![ \t]*(?:[*+-]|\d+[.])[ \t]+)))/gm, function(n, y, D, C) {
      n = -1 < C.search(/[*+-]/g) ? "ul" : "ol";
      return v(D, n, !1);
    });
    a = a.replace(/¨0/, "");
    return a = e.converter._dispatch("lists.after", a, g, e);
  });
  b.subParser("metadata", function(a, g, e) {
    function l(m) {
      e.metadata.raw = m;
      m = m.replace(/&/g, "&amp;").replace(/"/g, "&quot;");
      m = m.replace(/\n {4}/g, " ");
      m.replace(/^([\S ]+): +([\s\S]+?)$/gm, function(v, n, y) {
        e.metadata.parsed[n] = y;
        return "";
      });
    }
    if (!g.metadata) {
      return a;
    }
    a = e.converter._dispatch("metadata.before", a, g, e);
    a = a.replace(/^\s*«««+(\S*?)\n([\s\S]+?)\n»»»+\n/, function(m, v, n) {
      l(n);
      return "¨M";
    });
    a = a.replace(/^\s*---+(\S*?)\n([\s\S]+?)\n---+\n/, function(m, v, n) {
      v && (e.metadata.format = v);
      l(n);
      return "¨M";
    });
    a = a.replace(/¨M/g, "");
    return a = e.converter._dispatch("metadata.after", a, g, e);
  });
  b.subParser("outdent", function(a, g, e) {
    a = e.converter._dispatch("outdent.before", a, g, e);
    a = a.replace(/^(\t|[ ]{1,4})/gm, "¨0");
    a = a.replace(/¨0/g, "");
    return a = e.converter._dispatch("outdent.after", a, g, e);
  });
  b.subParser("paragraphs", function(a, g, e) {
    a = e.converter._dispatch("paragraphs.before", a, g, e);
    a = a.replace(/^\n+/g, "");
    a = a.replace(/\n+$/g, "");
    var l = a.split(/\n{2,}/g);
    a = [];
    for (var m = l.length, v = 0; v < m; v++) {
      var n = l[v];
      0 <= n.search(/¨(K|G)(\d+)\1/g) ? a.push(n) : 0 <= n.search(/\S/) && (n = b.subParser("spanGamut")(n, g, e), n = n.replace(/^([ \t]*)/g, "<p>"), n += "</p>", a.push(n));
    }
    m = a.length;
    for (v = 0; v < m; v++) {
      l = a[v];
      for (n = !1; /¨(K|G)(\d+)\1/.test(l);) {
        var y = RegExp.$2;
        y = "K" === RegExp.$1 ? e.gHtmlBlocks[y] : n ? b.subParser("encodeCode")(e.ghCodeBlocks[y].text, g, e) : e.ghCodeBlocks[y].codeblock;
        y = y.replace(/\$/g, "$$$$");
        l = l.replace(/(\n\n)?¨(K|G)\d+\2(\n\n)?/, y);
        /^<pre\b[^>]*>\s*<code\b[^>]*>/.test(l) && (n = !0);
      }
      a[v] = l;
    }
    a = a.join("\n");
    a = a.replace(/^\n+/g, "");
    a = a.replace(/\n+$/g, "");
    return e.converter._dispatch("paragraphs.after", a, g, e);
  });
  b.subParser("runExtension", function(a, g, e, l) {
    a.filter ? g = a.filter(g, l.converter, e) : a.regex && (e = a.regex, e instanceof RegExp || (e = new RegExp(e, "g")), g = g.replace(e, a.replace));
    return g;
  });
  b.subParser("spanGamut", function(a, g, e) {
    a = e.converter._dispatch("spanGamut.before", a, g, e);
    a = b.subParser("codeSpans")(a, g, e);
    a = b.subParser("escapeSpecialCharsWithinTagAttributes")(a, g, e);
    a = b.subParser("encodeBackslashEscapes")(a, g, e);
    a = b.subParser("images")(a, g, e);
    a = b.subParser("anchors")(a, g, e);
    a = b.subParser("autoLinks")(a, g, e);
    a = b.subParser("simplifiedAutoLinks")(a, g, e);
    a = b.subParser("emoji")(a, g, e);
    a = b.subParser("underline")(a, g, e);
    a = b.subParser("italicsAndBold")(a, g, e);
    a = b.subParser("strikethrough")(a, g, e);
    a = b.subParser("ellipsis")(a, g, e);
    a = b.subParser("hashHTMLSpans")(a, g, e);
    a = b.subParser("encodeAmpsAndAngles")(a, g, e);
    g.simpleLineBreaks ? /\n\n¨K/.test(a) || (a = a.replace(/\n+/g, "<br />\n")) : a = a.replace(/  +\n/g, "<br />\n");
    return a = e.converter._dispatch("spanGamut.after", a, g, e);
  });
  b.subParser("strikethrough", function(a, g, e) {
    g.strikethrough && (a = e.converter._dispatch("strikethrough.before", a, g, e), a = a.replace(/(?:~){2}([\s\S]+?)(?:~){2}/g, function(l, m) {
      l = m;
      g.simplifiedAutoLink && (l = b.subParser("simplifiedAutoLinks")(l, g, e));
      return "<del>" + l + "</del>";
    }), a = e.converter._dispatch("strikethrough.after", a, g, e));
    return a;
  });
  b.subParser("stripLinkDefinitions", function(a, g, e) {
    var l = function(m, v, n, y, D, C, p) {
      v = v.toLowerCase();
      n.match(/^data:.+?\/.+?;base64,/) ? e.gUrls[v] = n.replace(/\s/g, "") : e.gUrls[v] = b.subParser("encodeAmpsAndAngles")(n, g, e);
      if (C) {
        return C + p;
      }
      p && (e.gTitles[v] = p.replace(/"|'/g, "&quot;"));
      g.parseImgDimensions && y && D && (e.gDimensions[v] = {width:y, height:D});
      return "";
    };
    a = (a + "¨0").replace(/^ {0,3}\[(.+)]:[ \t]*\n?[ \t]*<?(data:.+?\/.+?;base64,[A-Za-z0-9+/=\n]+?)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*\n?[ \t]*(?:(\n*)["|'(](.+?)["|')][ \t]*)?(?:\n\n|(?=¨0)|(?=\n\[))/gm, l);
    a = a.replace(/^ {0,3}\[(.+)]:[ \t]*\n?[ \t]*<?([^>\s]+)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*\n?[ \t]*(?:(\n*)["|'(](.+?)["|')][ \t]*)?(?:\n+|(?=¨0))/gm, l);
    return a = a.replace(/¨0/, "");
  });
  b.subParser("tables", function(a, g, e) {
    function l(D) {
      return /^:[ \t]*--*$/.test(D) ? ' style="text-align:left;"' : /^--*[ \t]*:[ \t]*$/.test(D) ? ' style="text-align:right;"' : /^:[ \t]*--*[ \t]*:$/.test(D) ? ' style="text-align:center;"' : "";
    }
    function m(D, C) {
      var p = "";
      D = D.trim();
      if (g.tablesHeaderId || g.tableHeaderId) {
        p = ' id="' + D.replace(/ /g, "_").toLowerCase() + '"';
      }
      D = b.subParser("spanGamut")(D, g, e);
      return "<th" + p + C + ">" + D + "</th>\n";
    }
    function v(D, C) {
      D = b.subParser("spanGamut")(D, g, e);
      return "<td" + C + ">" + D + "</td>\n";
    }
    function n(D, C) {
      for (var p = "<table>\n<thead>\n<tr>\n", z = D.length, I = 0; I < z; ++I) {
        p += D[I];
      }
      p += "</tr>\n</thead>\n<tbody>\n";
      for (I = 0; I < C.length; ++I) {
        p += "<tr>\n";
        for (D = 0; D < z; ++D) {
          p += C[I][D];
        }
        p += "</tr>\n";
      }
      return p + "</tbody>\n</table>\n";
    }
    function y(D) {
      var C, p = D.split("\n");
      for (C = 0; C < p.length; ++C) {
        /^ {0,3}\|/.test(p[C]) && (p[C] = p[C].replace(/^ {0,3}\|/, "")), /\|[ \t]*$/.test(p[C]) && (p[C] = p[C].replace(/\|[ \t]*$/, "")), p[C] = b.subParser("codeSpans")(p[C], g, e);
      }
      var z = p[0].split("|").map(function(q) {
        return q.trim();
      }), I = p[1].split("|").map(function(q) {
        return q.trim();
      }), W = [], ba = [], M = [], ea = [];
      p.shift();
      p.shift();
      for (C = 0; C < p.length; ++C) {
        "" !== p[C].trim() && W.push(p[C].split("|").map(function(q) {
          return q.trim();
        }));
      }
      if (z.length < I.length) {
        return D;
      }
      for (C = 0; C < I.length; ++C) {
        M.push(l(I[C]));
      }
      for (C = 0; C < z.length; ++C) {
        b.helper.isUndefined(M[C]) && (M[C] = ""), ba.push(m(z[C], M[C]));
      }
      for (C = 0; C < W.length; ++C) {
        D = [];
        for (p = 0; p < ba.length; ++p) {
          b.helper.isUndefined(W[C][p]), D.push(v(W[C][p], M[p]));
        }
        ea.push(D);
      }
      return n(ba, ea);
    }
    if (!g.tables) {
      return a;
    }
    a = e.converter._dispatch("tables.before", a, g, e);
    a = a.replace(/\\(\|)/g, b.helper.escapeCharactersCallback);
    a = a.replace(/^ {0,3}\|?.+\|.+\n {0,3}\|?[ \t]*:?[ \t]*(?:[-=]){2,}[ \t]*:?[ \t]*\|[ \t]*:?[ \t]*(?:[-=]){2,}[\s\S]+?(?:\n\n|¨0)/gm, y);
    a = a.replace(/^ {0,3}\|.+\|[ \t]*\n {0,3}\|[ \t]*:?[ \t]*(?:[-=]){2,}[ \t]*:?[ \t]*\|[ \t]*\n( {0,3}\|.+\|[ \t]*\n)*(?:\n|¨0)/gm, y);
    return a = e.converter._dispatch("tables.after", a, g, e);
  });
  b.subParser("underline", function(a, g, e) {
    if (!g.underline) {
      return a;
    }
    a = e.converter._dispatch("underline.before", a, g, e);
    g.literalMidWordUnderscores ? (a = a.replace(/\b___(\S[\s\S]*?)___\b/g, function(l, m) {
      return "<u>" + m + "</u>";
    }), a = a.replace(/\b__(\S[\s\S]*?)__\b/g, function(l, m) {
      return "<u>" + m + "</u>";
    })) : (a = a.replace(/___(\S[\s\S]*?)___/g, function(l, m) {
      return /\S$/.test(m) ? "<u>" + m + "</u>" : l;
    }), a = a.replace(/__(\S[\s\S]*?)__/g, function(l, m) {
      return /\S$/.test(m) ? "<u>" + m + "</u>" : l;
    }));
    a = a.replace(/(_)/g, b.helper.escapeCharactersCallback);
    return a = e.converter._dispatch("underline.after", a, g, e);
  });
  b.subParser("unescapeSpecialChars", function(a, g, e) {
    a = e.converter._dispatch("unescapeSpecialChars.before", a, g, e);
    a = a.replace(/¨E(\d+)E/g, function(l, m) {
      l = parseInt(m);
      return String.fromCharCode(l);
    });
    return a = e.converter._dispatch("unescapeSpecialChars.after", a, g, e);
  });
  b.subParser("makeMarkdown.blockquote", function(a, g) {
    var e = "";
    if (a.hasChildNodes()) {
      a = a.childNodes;
      for (var l = a.length, m = 0; m < l; ++m) {
        var v = b.subParser("makeMarkdown.node")(a[m], g);
        "" !== v && (e += v);
      }
    }
    e = e.trim();
    return e = "> " + e.split("\n").join("\n> ");
  });
  b.subParser("makeMarkdown.codeBlock", function(a, g) {
    var e = a.getAttribute("language");
    a = a.getAttribute("precodenum");
    return "```" + e + "\n" + g.preList[a] + "\n```";
  });
  b.subParser("makeMarkdown.codeSpan", function(a) {
    return "`" + a.innerHTML + "`";
  });
  b.subParser("makeMarkdown.emphasis", function(a, g) {
    var e = "";
    if (a.hasChildNodes()) {
      e += "*";
      a = a.childNodes;
      for (var l = a.length, m = 0; m < l; ++m) {
        e += b.subParser("makeMarkdown.node")(a[m], g);
      }
      e += "*";
    }
    return e;
  });
  b.subParser("makeMarkdown.header", function(a, g, e) {
    var l = Array(e + 1).join("#");
    e = "";
    if (a.hasChildNodes()) {
      e = l + " ";
      a = a.childNodes;
      l = a.length;
      for (var m = 0; m < l; ++m) {
        e += b.subParser("makeMarkdown.node")(a[m], g);
      }
    }
    return e;
  });
  b.subParser("makeMarkdown.hr", function() {
    return "---";
  });
  b.subParser("makeMarkdown.image", function(a) {
    var g = "";
    a.hasAttribute("src") && (g += "![" + a.getAttribute("alt") + "](", g += "<" + a.getAttribute("src") + ">", a.hasAttribute("width") && a.hasAttribute("height") && (g += " =" + a.getAttribute("width") + "x" + a.getAttribute("height")), a.hasAttribute("title") && (g += ' "' + a.getAttribute("title") + '"'), g += ")");
    return g;
  });
  b.subParser("makeMarkdown.links", function(a, g) {
    var e = "";
    if (a.hasChildNodes() && a.hasAttribute("href")) {
      var l = a.childNodes, m = l.length;
      e = "[";
      for (var v = 0; v < m; ++v) {
        e += b.subParser("makeMarkdown.node")(l[v], g);
      }
      e = e + "](<" + (a.getAttribute("href") + ">");
      a.hasAttribute("title") && (e += ' "' + a.getAttribute("title") + '"');
      e += ")";
    }
    return e;
  });
  b.subParser("makeMarkdown.list", function(a, g, e) {
    var l = "";
    if (!a.hasChildNodes()) {
      return "";
    }
    var m = a.childNodes, v = m.length;
    a = a.getAttribute("start") || 1;
    for (var n = 0; n < v; ++n) {
      "undefined" !== typeof m[n].tagName && "li" === m[n].tagName.toLowerCase() && (l += ("ol" === e ? a.toString() + ". " : "- ") + b.subParser("makeMarkdown.listItem")(m[n], g), ++a);
    }
    return (l + "\n\x3c!-- --\x3e\n").trim();
  });
  b.subParser("makeMarkdown.listItem", function(a, g) {
    var e = "";
    a = a.childNodes;
    for (var l = a.length, m = 0; m < l; ++m) {
      e += b.subParser("makeMarkdown.node")(a[m], g);
    }
    return e = /\n$/.test(e) ? e.split("\n").join("\n    ").replace(/^ {4}$/gm, "").replace(/\n\n+/g, "\n\n") : e + "\n";
  });
  b.subParser("makeMarkdown.node", function(a, g, e) {
    e = e || !1;
    var l = "";
    if (3 === a.nodeType) {
      return b.subParser("makeMarkdown.txt")(a, g);
    }
    if (8 === a.nodeType) {
      return "\x3c!--" + a.data + "--\x3e\n\n";
    }
    if (1 !== a.nodeType) {
      return "";
    }
    switch(a.tagName.toLowerCase()) {
      case "h1":
        e || (l = b.subParser("makeMarkdown.header")(a, g, 1) + "\n\n");
        break;
      case "h2":
        e || (l = b.subParser("makeMarkdown.header")(a, g, 2) + "\n\n");
        break;
      case "h3":
        e || (l = b.subParser("makeMarkdown.header")(a, g, 3) + "\n\n");
        break;
      case "h4":
        e || (l = b.subParser("makeMarkdown.header")(a, g, 4) + "\n\n");
        break;
      case "h5":
        e || (l = b.subParser("makeMarkdown.header")(a, g, 5) + "\n\n");
        break;
      case "h6":
        e || (l = b.subParser("makeMarkdown.header")(a, g, 6) + "\n\n");
        break;
      case "p":
        e || (l = b.subParser("makeMarkdown.paragraph")(a, g) + "\n\n");
        break;
      case "blockquote":
        e || (l = b.subParser("makeMarkdown.blockquote")(a, g) + "\n\n");
        break;
      case "hr":
        e || (l = b.subParser("makeMarkdown.hr")(a, g) + "\n\n");
        break;
      case "ol":
        e || (l = b.subParser("makeMarkdown.list")(a, g, "ol") + "\n\n");
        break;
      case "ul":
        e || (l = b.subParser("makeMarkdown.list")(a, g, "ul") + "\n\n");
        break;
      case "precode":
        e || (l = b.subParser("makeMarkdown.codeBlock")(a, g) + "\n\n");
        break;
      case "pre":
        e || (l = b.subParser("makeMarkdown.pre")(a, g) + "\n\n");
        break;
      case "table":
        e || (l = b.subParser("makeMarkdown.table")(a, g) + "\n\n");
        break;
      case "code":
        l = b.subParser("makeMarkdown.codeSpan")(a, g);
        break;
      case "em":
      case "i":
        l = b.subParser("makeMarkdown.emphasis")(a, g);
        break;
      case "strong":
      case "b":
        l = b.subParser("makeMarkdown.strong")(a, g);
        break;
      case "del":
        l = b.subParser("makeMarkdown.strikethrough")(a, g);
        break;
      case "a":
        l = b.subParser("makeMarkdown.links")(a, g);
        break;
      case "img":
        l = b.subParser("makeMarkdown.image")(a, g);
        break;
      default:
        l = a.outerHTML + "\n\n";
    }
    return l;
  });
  b.subParser("makeMarkdown.paragraph", function(a, g) {
    var e = "";
    if (a.hasChildNodes()) {
      a = a.childNodes;
      for (var l = a.length, m = 0; m < l; ++m) {
        e += b.subParser("makeMarkdown.node")(a[m], g);
      }
    }
    return e = e.trim();
  });
  b.subParser("makeMarkdown.pre", function(a, g) {
    a = a.getAttribute("prenum");
    return "<pre>" + g.preList[a] + "</pre>";
  });
  b.subParser("makeMarkdown.strikethrough", function(a, g) {
    var e = "";
    if (a.hasChildNodes()) {
      e += "~~";
      a = a.childNodes;
      for (var l = a.length, m = 0; m < l; ++m) {
        e += b.subParser("makeMarkdown.node")(a[m], g);
      }
      e += "~~";
    }
    return e;
  });
  b.subParser("makeMarkdown.strong", function(a, g) {
    var e = "";
    if (a.hasChildNodes()) {
      e += "**";
      a = a.childNodes;
      for (var l = a.length, m = 0; m < l; ++m) {
        e += b.subParser("makeMarkdown.node")(a[m], g);
      }
      e += "**";
    }
    return e;
  });
  b.subParser("makeMarkdown.table", function(a, g) {
    var e = "", l = [[], []], m = a.querySelectorAll("thead>tr>th"), v = a.querySelectorAll("tbody>tr");
    for (a = 0; a < m.length; ++a) {
      var n = b.subParser("makeMarkdown.tableCell")(m[a], g);
      var y = "---";
      if (m[a].hasAttribute("style")) {
        switch(m[a].getAttribute("style").toLowerCase().replace(/\s/g, "")) {
          case "text-align:left;":
            y = ":---";
            break;
          case "text-align:right;":
            y = "---:";
            break;
          case "text-align:center;":
            y = ":---:";
        }
      }
      l[0][a] = n.trim();
      l[1][a] = y;
    }
    for (a = 0; a < v.length; ++a) {
      y = l.push([]) - 1;
      var D = v[a].getElementsByTagName("td");
      for (n = 0; n < m.length; ++n) {
        var C = " ";
        "undefined" !== typeof D[n] && (C = b.subParser("makeMarkdown.tableCell")(D[n], g));
        l[y].push(C);
      }
    }
    g = 3;
    for (a = 0; a < l.length; ++a) {
      for (n = 0; n < l[a].length; ++n) {
        m = l[a][n].length, m > g && (g = m);
      }
    }
    for (a = 0; a < l.length; ++a) {
      for (n = 0; n < l[a].length; ++n) {
        1 === a ? ":" === l[a][n].slice(-1) ? l[a][n] = b.helper.padEnd(l[a][n].slice(-1), g - 1, "-") + ":" : l[a][n] = b.helper.padEnd(l[a][n], g, "-") : l[a][n] = b.helper.padEnd(l[a][n], g);
      }
      e += "| " + l[a].join(" | ") + " |\n";
    }
    return e.trim();
  });
  b.subParser("makeMarkdown.tableCell", function(a, g) {
    var e = "";
    if (!a.hasChildNodes()) {
      return "";
    }
    a = a.childNodes;
    for (var l = a.length, m = 0; m < l; ++m) {
      e += b.subParser("makeMarkdown.node")(a[m], g, !0);
    }
    return e.trim();
  });
  b.subParser("makeMarkdown.txt", function(a) {
    a = a.nodeValue;
    a = a.replace(/ +/g, " ");
    a = a.replace(/¨NBSP;/g, " ");
    a = b.helper.unescapeHTMLEntities(a);
    a = a.replace(/([*_~|`])/g, "\\$1");
    a = a.replace(/^(\s*)>/g, "\\$1>");
    a = a.replace(/^#/gm, "\\#");
    a = a.replace(/^(\s*)([-=]{3,})(\s*)$/, "$1\\$2$3");
    a = a.replace(/^( {0,3}\d+)\./gm, "$1\\.");
    a = a.replace(/^( {0,3})([+-])/gm, "$1\\$2");
    a = a.replace(/]([\s]*)\(/g, "\\]$1\\(");
    return a = a.replace(/^ {0,3}\[([\S \t]*?)]:/gm, "\\[$1]:");
  });
  "function" === typeof define && define.amd ? define(function() {
    return b;
  }) : "undefined" !== typeof module && module.exports ? module.exports = b : this.showdown = b;
}).call(this);
var hljs = function() {
  function c(q) {
    return q instanceof Map ? q.clear = q.delete = q.set = function() {
      throw Error("map is read-only");
    } : q instanceof Set && (q.add = q.clear = q.delete = function() {
      throw Error("set is read-only");
    }), Object.freeze(q), Object.getOwnPropertyNames(q).forEach(function(E) {
      E = q[E];
      "object" != typeof E || Object.isFrozen(E) || c(E);
    }), q;
  }
  function d(q) {
    return q.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#x27;");
  }
  function f(q, E) {
    for (var J = [], N = 1; N < arguments.length; ++N) {
      J[N - 1] = arguments[N];
    }
    var O = Object.create(null), K;
    for (K in q) {
      O[K] = q[K];
    }
    return J.forEach(function(A) {
      for (var H in A) {
        O[H] = A[H];
      }
    }), O;
  }
  function b(q) {
    return q ? "string" == typeof q ? q : q.source : null;
  }
  function h(q, E) {
    "." === q.input[q.index - 1] && E.ignoreMatch();
  }
  function k(q, E) {
    E && q.beginKeywords && (q.begin = "\\b(" + q.beginKeywords.split(" ").join("|") + ")(?!\\.)(?=\\b|\\s)", q.__beforeBegin = h, q.keywords = q.keywords || q.beginKeywords, delete q.beginKeywords, void 0 === q.relevance && (q.relevance = 0));
  }
  function u(q, E) {
    Array.isArray(q.illegal) && (q.illegal = function(J) {
      for (var N = [], O = 0; O < arguments.length; ++O) {
        N[O - 0] = arguments[O];
      }
      return "(" + N.map(function(K) {
        return b(K);
      }).join("|") + ")";
    }.apply(null, $jscomp.arrayFromIterable(q.illegal)));
  }
  function x(q, E) {
    if (q.match) {
      if (q.begin || q.end) {
        throw Error("begin & end are not supported with match");
      }
      q.begin = q.match;
      delete q.match;
    }
  }
  function t(q, E) {
    void 0 === q.relevance && (q.relevance = 1);
  }
  function r(q, E, J) {
    function N(K, A) {
      E && (A = A.map(function(H) {
        return H.toLowerCase();
      }));
      A.forEach(function(H) {
        var fa = H.split("|");
        H = fa[0];
        var Z = fa[1];
        fa = Z ? Number(Z) : z.includes(fa[0].toLowerCase()) ? 0 : 1;
        O[H] = [K, fa];
      });
    }
    J = void 0 === J ? "keyword" : J;
    var O = {};
    return "string" == typeof q ? N(J, q.split(" ")) : Array.isArray(q) ? N(J, q) : Object.keys(q).forEach(function(K) {
      Object.assign(O, r(q[K], E, K));
    }), O;
  }
  function w(q, E) {
    function J(K, A) {
      return RegExp(b(K), "m" + (q.case_insensitive ? "i" : "") + (A ? "g" : ""));
    }
    var N = function() {
      this.matchIndexes = {};
      this.regexes = [];
      this.matchAt = 1;
      this.position = 0;
    };
    N.prototype.addRule = function(K, A) {
      A.position = this.position++;
      this.matchIndexes[this.matchAt] = A;
      this.regexes.push([A, K]);
      this.matchAt += RegExp(K.toString() + "|").exec("").length - 1 + 1;
    };
    N.prototype.compile = function() {
      0 === this.regexes.length && (this.exec = function() {
        return null;
      });
      var K = this.regexes.map(function(A) {
        return A[1];
      });
      this.matcherRe = J(function(A, H) {
        H = void 0 === H ? "|" : H;
        var fa = 0;
        return A.map(function(Z) {
          var aa = fa += 1;
          Z = b(Z);
          for (var T = ""; 0 < Z.length;) {
            var ja = g.exec(Z);
            if (!ja) {
              T += Z;
              break;
            }
            T += Z.substring(0, ja.index);
            Z = Z.substring(ja.index + ja[0].length);
            "\\" === ja[0][0] && ja[1] ? T += "\\" + (Number(ja[1]) + aa) : (T += ja[0], "(" === ja[0] && fa++);
          }
          return T;
        }).map(function(Z) {
          return "(" + Z + ")";
        }).join(H);
      }(K), !0);
      this.lastIndex = 0;
    };
    N.prototype.exec = function(K) {
      this.matcherRe.lastIndex = this.lastIndex;
      K = this.matcherRe.exec(K);
      if (!K) {
        return null;
      }
      var A = K.findIndex(function(fa, Z) {
        return 0 < Z && void 0 !== fa;
      }), H = this.matchIndexes[A];
      return K.splice(0, A), Object.assign(K, H);
    };
    var O = function() {
      this.rules = [];
      this.multiRegexes = [];
      this.regexIndex = this.lastIndex = this.count = 0;
    };
    O.prototype.getMatcher = function(K) {
      if (this.multiRegexes[K]) {
        return this.multiRegexes[K];
      }
      var A = new N;
      return this.rules.slice(K).forEach(function(H) {
        var fa = $jscomp.makeIterator(H);
        H = fa.next().value;
        fa = fa.next().value;
        return A.addRule(H, fa);
      }), A.compile(), this.multiRegexes[K] = A, A;
    };
    O.prototype.resumingScanAtSamePosition = function() {
      return 0 !== this.regexIndex;
    };
    O.prototype.considerAll = function() {
      this.regexIndex = 0;
    };
    O.prototype.addRule = function(K, A) {
      this.rules.push([K, A]);
      "begin" === A.type && this.count++;
    };
    O.prototype.exec = function(K) {
      var A = this.getMatcher(this.regexIndex);
      A.lastIndex = this.lastIndex;
      A = A.exec(K);
      !this.resumingScanAtSamePosition() || A && A.index === this.lastIndex || (A = this.getMatcher(0), A.lastIndex = this.lastIndex + 1, A = A.exec(K));
      return A && (this.regexIndex += A.position + 1, this.regexIndex === this.count && this.considerAll()), A;
    };
    if (q.compilerExtensions || (q.compilerExtensions = []), q.contains && q.contains.includes("self")) {
      throw Error("ERR: contains `self` is not supported at the top-level of a language.  See documentation.");
    }
    return q.classNameAliases = f(q.classNameAliases || {}), function fa(A, H) {
      if (A.isCompiled) {
        return A;
      }
      [x].forEach(function(aa) {
        return aa(A, H);
      });
      q.compilerExtensions.forEach(function(aa) {
        return aa(A, H);
      });
      A.__beforeBegin = null;
      [k, u, t].forEach(function(aa) {
        return aa(A, H);
      });
      A.isCompiled = !0;
      var Z = null;
      if ("object" == typeof A.keywords && (Z = A.keywords.$pattern, delete A.keywords.$pattern), A.keywords && (A.keywords = r(A.keywords, q.case_insensitive)), A.lexemes && Z) {
        throw Error("ERR: Prefer `keywords.$pattern` to `mode.lexemes`, BOTH are not allowed. (see mode reference) ");
      }
      return Z = Z || A.lexemes || /\w+/, A.keywordPatternRe = J(Z, !0), H && (A.begin || (A.begin = /\B|\b/), A.beginRe = J(A.begin), A.endSameAsBegin && (A.end = A.begin), A.end || A.endsWithParent || (A.end = /\B|\b/), A.end && (A.endRe = J(A.end)), A.terminatorEnd = b(A.end) || "", A.endsWithParent && H.terminatorEnd && (A.terminatorEnd += (A.end ? "|" : "") + H.terminatorEnd)), A.illegal && (A.illegalRe = J(A.illegal)), A.contains || (A.contains = []), A.contains = [].concat.apply([], $jscomp.arrayFromIterable(A.contains.map(function(aa) {
        return function(T) {
          return T.variants && !T.cachedVariants && (T.cachedVariants = T.variants.map(function(ja) {
            return f(T, {variants:null}, ja);
          })), T.cachedVariants ? T.cachedVariants : B(T) ? f(T, {starts:T.starts ? f(T.starts) : null}) : Object.isFrozen(T) ? f(T) : T;
        }("self" === aa ? A : aa);
      }))), A.contains.forEach(function(aa) {
        fa(aa, A);
      }), A.starts && fa(A.starts, H), A.matcher = function(aa) {
        var T = new O;
        return aa.contains.forEach(function(ja) {
          return T.addRule(ja.begin, {rule:ja, type:"begin"});
        }), aa.terminatorEnd && T.addRule(aa.terminatorEnd, {type:"end"}), aa.illegal && T.addRule(aa.illegal, {type:"illegal"}), T;
      }(A), A;
    }(q);
  }
  function B(q) {
    return !!q && (q.endsWithParent || B(q.starts));
  }
  function P(q) {
    var E = {props:["language", "code", "autodetect"], data:function() {
      return {detectedLanguage:"", unknownLanguage:!1};
    }, computed:{className:function() {
      return this.unknownLanguage ? "" : "hljs " + this.detectedLanguage;
    }, highlighted:function() {
      if (!this.autoDetect && !q.getLanguage(this.language)) {
        return console.warn('The language "' + this.language + '" you specified could not be found.'), this.unknownLanguage = !0, d(this.code);
      }
      var J = {};
      return this.autoDetect ? (J = q.highlightAuto(this.code), this.detectedLanguage = J.language) : (J = q.highlight(this.language, this.code, this.ignoreIllegals), this.detectedLanguage = this.language), J.value;
    }, autoDetect:function() {
      return !(this.language && (J = this.autodetect, !J && "" !== J));
      var J;
    }, ignoreIllegals:function() {
      return !0;
    }}, render:function(J) {
      return J("pre", {}, [J("code", {class:this.className, domProps:{innerHTML:this.highlighted}})]);
    }};
    return {Component:E, VuePlugin:{install:function(J) {
      J.component("highlightjs", E);
    }}};
  }
  function ca(q) {
    var E = [];
    return function K(N, O) {
      for (N = N.firstChild; N; N = N.nextSibling) {
        3 === N.nodeType ? O += N.nodeValue.length : 1 === N.nodeType && (E.push({event:"start", offset:O, node:N}), O = K(N, O), N.nodeName.toLowerCase().match(/br|hr|img|input/) || E.push({event:"stop", offset:O, node:N}));
      }
      return O;
    }(q, 0), E;
  }
  c.default = c;
  var R = function(q) {
    void 0 === q.data && (q.data = {});
    this.data = q.data;
    this.isMatchIgnored = !1;
  };
  R.prototype.ignoreMatch = function() {
    this.isMatchIgnored = !0;
  };
  var Y = function(q, E) {
    this.buffer = "";
    this.classPrefix = E.classPrefix;
    q.walk(this);
  };
  Y.prototype.addText = function(q) {
    this.buffer += d(q);
  };
  Y.prototype.openNode = function(q) {
    if (q.kind) {
      var E = q.kind;
      q.sublanguage || (E = "" + this.classPrefix + E);
      this.span(E);
    }
  };
  Y.prototype.closeNode = function(q) {
    q.kind && (this.buffer += "</span>");
  };
  Y.prototype.value = function() {
    return this.buffer;
  };
  Y.prototype.span = function(q) {
    this.buffer += '<span class="' + q + '">';
  };
  var X = function() {
    this.rootNode = {children:[]};
    this.stack = [this.rootNode];
  };
  X.prototype.add = function(q) {
    this.top.children.push(q);
  };
  X.prototype.openNode = function(q) {
    q = {kind:q, children:[]};
    this.add(q);
    this.stack.push(q);
  };
  X.prototype.closeNode = function() {
    if (1 < this.stack.length) {
      return this.stack.pop();
    }
  };
  X.prototype.closeAllNodes = function() {
    for (; this.closeNode();) {
    }
  };
  X.prototype.toJSON = function() {
    return JSON.stringify(this.rootNode, null, 4);
  };
  X.prototype.walk = function(q) {
    return this.constructor._walk(q, this.rootNode);
  };
  X._walk = function(q, E) {
    var J = this;
    return "string" == typeof E ? q.addText(E) : E.children && (q.openNode(E), E.children.forEach(function(N) {
      return J._walk(q, N);
    }), q.closeNode(E)), q;
  };
  X._collapse = function(q) {
    "string" != typeof q && q.children && (q.children.every(function(E) {
      return "string" == typeof E;
    }) ? q.children = [q.children.join("")] : q.children.forEach(function(E) {
      X._collapse(E);
    }));
  };
  $jscomp.global.Object.defineProperties(X.prototype, {top:{configurable:!0, enumerable:!0, get:function() {
    return this.stack[this.stack.length - 1];
  }}, root:{configurable:!0, enumerable:!0, get:function() {
    return this.rootNode;
  }}});
  var a = function(q) {
    var E = X.call(this) || this;
    E.options = q;
    return E;
  };
  $jscomp.inherits(a, X);
  a.prototype.addKeyword = function(q, E) {
    "" !== q && (this.openNode(E), this.addText(q), this.closeNode());
  };
  a.prototype.addText = function(q) {
    "" !== q && this.add(q);
  };
  a.prototype.addSublanguage = function(q, E) {
    q = q.root;
    q.kind = E;
    q.sublanguage = !0;
    this.add(q);
  };
  a.prototype.toHTML = function() {
    return (new Y(this, this.options)).value();
  };
  a.prototype.finalize = function() {
    return !0;
  };
  var g = /\[(?:[^\\\]]|\\.)*\]|\(\??|\\([1-9][0-9]*)|\\./, e = {begin:"\\\\[\\s\\S]", relevance:0}, l = {className:"string", begin:"'", end:"'", illegal:"\\n", contains:[e]}, m = {className:"string", begin:'"', end:'"', illegal:"\\n", contains:[e]}, v = {begin:/\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|they|like|more)\b/}, n = function(q, E, J) {
    J = void 0 === J ? {} : J;
    q = f({className:"comment", begin:q, end:E, contains:[]}, J);
    return q.contains.push(v), q.contains.push({className:"doctag", begin:"(?:TODO|FIXME|NOTE|BUG|OPTIMIZE|HACK|XXX):", relevance:0}), q;
  }, y = n("//", "$"), D = n("/\\*", "\\*/"), C = n("#", "$"), p = Object.freeze({__proto__:null, MATCH_NOTHING_RE:/\b\B/, IDENT_RE:"[a-zA-Z]\\w*", UNDERSCORE_IDENT_RE:"[a-zA-Z_]\\w*", NUMBER_RE:"\\b\\d+(\\.\\d+)?", C_NUMBER_RE:"(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)", BINARY_NUMBER_RE:"\\b(0b[01]+)", RE_STARTERS_RE:"!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|-|-=|/=|/|:|;|<<|<<=|<=|<|===|==|=|>>>=|>>=|>=|>>>|>>|>|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~", SHEBANG:function(q) {
    q = void 0 === q ? {} : q;
    var E = /^#![ ]*\//;
    return q.binary && (q.begin = function(J) {
      for (var N = [], O = 0; O < arguments.length; ++O) {
        N[O - 0] = arguments[O];
      }
      return N.map(function(K) {
        return b(K);
      }).join("");
    }(E, /.*\b/, q.binary, /\b.*/)), f({className:"meta", begin:E, end:/$/, relevance:0, "on:begin":function(J, N) {
      0 !== J.index && N.ignoreMatch();
    }}, q);
  }, BACKSLASH_ESCAPE:e, APOS_STRING_MODE:l, QUOTE_STRING_MODE:m, PHRASAL_WORDS_MODE:v, COMMENT:n, C_LINE_COMMENT_MODE:y, C_BLOCK_COMMENT_MODE:D, HASH_COMMENT_MODE:C, NUMBER_MODE:{className:"number", begin:"\\b\\d+(\\.\\d+)?", relevance:0}, C_NUMBER_MODE:{className:"number", begin:"(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)", relevance:0}, BINARY_NUMBER_MODE:{className:"number", begin:"\\b(0b[01]+)", relevance:0}, CSS_NUMBER_MODE:{className:"number", begin:"\\b\\d+(\\.\\d+)?(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?", 
  relevance:0}, REGEXP_MODE:{begin:/(?=\/[^/\n]*\/)/, contains:[{className:"regexp", begin:/\//, end:/\/[gimuy]*/, illegal:/\n/, contains:[e, {begin:/\[/, end:/\]/, relevance:0, contains:[e]}]}]}, TITLE_MODE:{className:"title", begin:"[a-zA-Z]\\w*", relevance:0}, UNDERSCORE_TITLE_MODE:{className:"title", begin:"[a-zA-Z_]\\w*", relevance:0}, METHOD_GUARD:{begin:"\\.\\s*[a-zA-Z_]\\w*", relevance:0}, END_SAME_AS_BEGIN:function(q) {
    return Object.assign(q, {"on:begin":function(E, J) {
      J.data._beginMatch = E[1];
    }, "on:end":function(E, J) {
      J.data._beginMatch !== E[1] && J.ignoreMatch();
    }});
  }}), z = "of and for in not or if then parent list value".split(" "), I = {"after:highlightElement":function(q) {
    var E = q.result, J = q.text;
    q = ca(q.el);
    if (q.length) {
      var N = document.createElement("div");
      N.innerHTML = E.value;
      E.value = function(O, K, A) {
        function H() {
          return O.length && K.length ? O[0].offset !== K[0].offset ? O[0].offset < K[0].offset ? O : K : "start" === K[0].event ? O : K : O.length ? O : K;
        }
        function fa(qa) {
          ja += "<" + qa.nodeName.toLowerCase() + [].map.call(qa.attributes, function(ka) {
            return " " + ka.nodeName + '="' + d(ka.value) + '"';
          }).join("") + ">";
        }
        function Z(qa) {
          ja += "</" + qa.nodeName.toLowerCase() + ">";
        }
        function aa(qa) {
          ("start" === qa.event ? fa : Z)(qa.node);
        }
        for (var T = 0, ja = "", ra = []; O.length || K.length;) {
          var oa = H();
          if (ja += d(A.substring(T, oa[0].offset)), T = oa[0].offset, oa === O) {
            ra.reverse().forEach(Z);
            do {
              aa(oa.splice(0, 1)[0]), oa = H();
            } while (oa === O && oa.length && oa[0].offset === T);
            ra.reverse().forEach(fa);
          } else {
            "start" === oa[0].event ? ra.push(oa[0].node) : ra.pop(), aa(oa.splice(0, 1)[0]);
          }
        }
        return ja + d(A.substr(T));
      }(q, ca(N), J);
    }
  }}, W = {}, ba = function(q, E) {
    for (var J = [], N = 1; N < arguments.length; ++N) {
      J[N - 1] = arguments[N];
    }
    console.log.apply(console, ["WARN: " + q].concat($jscomp.arrayFromIterable(J)));
  }, M = function(q, E) {
    W[q + "/" + E] || (console.log("Deprecated as of " + q + ". " + E), W[q + "/" + E] = !0);
  }, ea = Symbol("nomatch");
  return function(q) {
    function E(F, L, Q, U) {
      var ia = "", ma = "";
      "object" == typeof L ? (ia = F, Q = L.ignoreIllegals, ma = L.language, U = void 0) : (M("10.7.0", "highlight(lang, code, ...args) has been deprecated."), M("10.7.0", "Please use highlight(code, options) instead.\nhttps://github.com/highlightjs/highlight.js/issues/2277"), ma = F, ia = L);
      F = {code:ia, language:ma};
      Z("before:highlight", F);
      Q = F.result ? F.result : J(F.language, F.code, Q, U);
      return Q.code = F.code, Z("after:highlight", Q), Q;
    }
    function J(F, L, Q, U) {
      function ia() {
        null != V.subLanguage ? function() {
          if ("" !== la) {
            if ("string" == typeof V.subLanguage) {
              if (!aa[V.subLanguage]) {
                return void na.addText(la);
              }
              var G = J(V.subLanguage, la, !0, Ea[V.subLanguage]);
              Ea[V.subLanguage] = G.top;
            } else {
              G = N(la, V.subLanguage.length ? V.subLanguage : null);
            }
            0 < V.relevance && (wa += G.relevance);
            na.addSublanguage(G.emitter, G.language);
          }
        }() : function() {
          if (!V.keywords) {
            return void na.addText(la);
          }
          var G = 0;
          V.keywordPatternRe.lastIndex = 0;
          for (var S = V.keywordPatternRe.exec(la), ha = ""; S;) {
            ha += la.substring(G, S.index);
            G = V;
            var da = S;
            da = ta.case_insensitive ? da[0].toLowerCase() : da[0];
            (G = Object.prototype.hasOwnProperty.call(G.keywords, da) && G.keywords[da]) ? (da = $jscomp.makeIterator(G), G = da.next().value, da = da.next().value, (na.addText(ha), ha = "", wa += da, G.startsWith("_")) ? ha += S[0] : na.addKeyword(S[0], ta.classNameAliases[G] || G)) : ha += S[0];
            G = V.keywordPatternRe.lastIndex;
            S = V.keywordPatternRe.exec(la);
          }
          ha += la.substr(G);
          na.addText(ha);
        }();
        la = "";
      }
      function ma(G) {
        return G.className && na.openNode(ta.classNameAliases[G.className] || G.className), V = Object.create(G, {parent:{value:V}}), V;
      }
      function sa(G, S, ha) {
        var da;
        if (da = (da = (da = G.endRe) && da.exec(ha)) && 0 === da.index) {
          if (G["on:end"]) {
            var pa = new R(G);
            G["on:end"](S, pa);
            pa.isMatchIgnored && (da = !1);
          }
          if (da) {
            for (; G.endsParent && G.parent;) {
              G = G.parent;
            }
            return G;
          }
        }
        if (G.endsWithParent) {
          return sa(G.parent, S, ha);
        }
      }
      function ua(G, S) {
        var ha = S && S[0];
        if (la += G, null == ha) {
          return ia(), 0;
        }
        if ("begin" === xa.type && "end" === S.type && xa.index === S.index && "" === ha) {
          if (la += L.slice(S.index, S.index + 1), !ra) {
            throw S = Error("0 width match regex"), S.languageName = F, S.badRule = xa.rule, S;
          }
          return 1;
        }
        if (xa = S, "begin" === S.type) {
          a: {
            ha = S[0];
            G = S.rule;
            for (var da = new R(G), pa = $jscomp.makeIterator([G.__beforeBegin, G["on:begin"]]), va = pa.next(); !va.done; va = pa.next()) {
              if ((va = va.value) && (va(S, da), da.isMatchIgnored)) {
                S = 0 === V.matcher.regexIndex ? (la += ha[0], 1) : (za = !0, 0);
                break a;
              }
            }
            S = (G && G.endSameAsBegin && (G.endRe = RegExp(ha.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&"), "m")), G.skip ? la += ha : (G.excludeBegin && (la += ha), ia(), G.returnBegin || G.excludeBegin || (la = ha)), ma(G), G.returnBegin ? 0 : ha.length);
          }
          return S;
        }
        if ("illegal" === S.type && !Q) {
          throw S = Error('Illegal lexeme "' + ha + '" for mode "' + (V.className || "<unnamed>") + '"'), S.mode = V, S;
        }
        if ("end" === S.type) {
          G = S[0];
          da = L.substr(S.index);
          if (da = sa(V, S, da)) {
            pa = V;
            pa.skip ? la += G : (pa.returnEnd || pa.excludeEnd || (la += G), ia(), pa.excludeEnd && (la = G));
            do {
              V.className && na.closeNode(), V.skip || V.subLanguage || (wa += V.relevance), V = V.parent;
            } while (V !== da.parent);
            G = (da.starts && (da.endSameAsBegin && (da.starts.endRe = da.endRe), ma(da.starts)), pa.returnEnd ? 0 : G.length);
          } else {
            G = ea;
          }
          if (G !== ea) {
            return G;
          }
        }
        if ("illegal" === S.type && "" === ha) {
          return 1;
        }
        if (1e5 < Aa && Aa > 3 * S.index) {
          throw Error("potential infinite loop, way more iterations than matches");
        }
        return la += ha, ha.length;
      }
      var xa = {}, ta = A(F);
      if (!ta) {
        throw console.error("Could not find the language '{}', did you forget to load/include a language module?".replace("{}", F)), Error('Unknown language: "' + F + '"');
      }
      var Ga = w(ta, {plugins:ja}), Ba = "", V = U || Ga, Ea = {}, na = new ka.__emitter(ka);
      (function() {
        for (var G = [], S = V; S !== ta; S = S.parent) {
          S.className && G.unshift(S.className);
        }
        G.forEach(function(ha) {
          return na.openNode(ha);
        });
      })();
      var la = "", wa = 0, Aa = U = 0, za = !1;
      try {
        for (V.matcher.considerAll();;) {
          Aa++;
          za ? za = !1 : V.matcher.considerAll();
          V.matcher.lastIndex = U;
          var ya = V.matcher.exec(L);
          if (!ya) {
            break;
          }
          var Ha = ua(L.substring(U, ya.index), ya);
          U = ya.index + Ha;
        }
        return ua(L.substr(U)), na.closeAllNodes(), na.finalize(), Ba = na.toHTML(), {relevance:Math.floor(wa), value:Ba, language:F, illegal:!1, emitter:na, top:V};
      } catch (G) {
        if (G.message && G.message.includes("Illegal")) {
          return {illegal:!0, illegalBy:{msg:G.message, context:L.slice(U - 100, U + 100), mode:G.mode}, sofar:Ba, relevance:0, value:d(L), emitter:na};
        }
        if (ra) {
          return {illegal:!1, relevance:0, value:d(L), emitter:na, language:F, top:V, errorRaised:G};
        }
        throw G;
      }
    }
    function N(F, L) {
      L = L || ka.languages || Object.keys(aa);
      var Q = function(U) {
        var ia = {relevance:0, emitter:new ka.__emitter(ka), value:d(U), illegal:!1, top:qa};
        return ia.emitter.addText(U), ia;
      }(F);
      L = L.filter(A).filter(fa).map(function(U) {
        return J(U, F, !1);
      });
      L.unshift(Q);
      Q = L.sort(function(U, ia) {
        if (U.relevance !== ia.relevance) {
          return ia.relevance - U.relevance;
        }
        if (U.language && ia.language) {
          if (A(U.language).supersetOf === ia.language) {
            return 1;
          }
          if (A(ia.language).supersetOf === U.language) {
            return -1;
          }
        }
        return 0;
      });
      L = $jscomp.makeIterator(Q);
      Q = L.next().value;
      L = L.next().value;
      return Q.second_best = L, Q;
    }
    function O(F) {
      var L = function(ia) {
        var ma = ia.className + " ";
        ma += ia.parentNode ? ia.parentNode.className : "";
        var sa = ka.languageDetectRe.exec(ma);
        return sa ? (ma = A(sa[1]), ma || (ba("Could not find the language '{}', did you forget to load/include a language module?".replace("{}", sa[1])), ba("Falling back to no-highlight mode for this block.", ia)), ma ? sa[1] : "no-highlight") : ma.split(/\s+/).find(function(ua) {
          return ka.noHighlightRe.test(ua) || A(ua);
        });
      }(F);
      if (!ka.noHighlightRe.test(L)) {
        Z("before:highlightElement", {el:F, language:L});
        var Q = F.textContent, U = L ? E(Q, {language:L, ignoreIllegals:!0}) : N(Q);
        Z("after:highlightElement", {el:F, result:U, text:Q});
        F.innerHTML = U.value;
        (function(ia, ma, sa) {
          ma = ma ? T[ma] : sa;
          ia.classList.add("hljs");
          ma && ia.classList.add(ma);
        })(F, L, U.language);
        F.result = {language:U.language, re:U.relevance, relavance:U.relevance};
        U.second_best && (F.second_best = {language:U.second_best.language, re:U.second_best.relevance, relavance:U.second_best.relevance});
      }
    }
    function K() {
      "loading" !== document.readyState ? document.querySelectorAll("pre code").forEach(O) : Ca = !0;
    }
    function A(F) {
      return F = (F || "").toLowerCase(), aa[F] || aa[T[F]];
    }
    function H(F, L) {
      var Q = L.languageName;
      "string" == typeof F && (F = [F]);
      F.forEach(function(U) {
        T[U.toLowerCase()] = Q;
      });
    }
    function fa(F) {
      return (F = A(F)) && !F.disableAutodetect;
    }
    function Z(F, L) {
      ja.forEach(function(Q) {
        Q[F] && Q[F](L);
      });
    }
    var aa = Object.create(null), T = Object.create(null), ja = [], ra = !0, oa = /(^(<[^>]+>|\t|)+|\n)/gm, qa = {disableAutodetect:!0, name:"Plain text", contains:[]}, ka = {noHighlightRe:/^(no-?highlight)$/i, languageDetectRe:/\blang(?:uage)?-([\w-]+)\b/i, classPrefix:"hljs-", tabReplace:null, useBR:!1, languages:null, __emitter:a}, Ia = /^(<[^>]+>|\t)+/gm, Da = function() {
      Da.called || (Da.called = !0, M("10.6.0", "initHighlighting() is deprecated.  Use highlightAll() instead."), document.querySelectorAll("pre code").forEach(O));
    }, Ca = !1;
    "undefined" != typeof window && window.addEventListener && window.addEventListener("DOMContentLoaded", function() {
      Ca && K();
    }, !1);
    Object.assign(q, {highlight:E, highlightAuto:N, highlightAll:K, fixMarkup:function(F) {
      return M("10.2.0", "fixMarkup will be removed entirely in v11.0"), M("10.2.0", "Please see https://github.com/highlightjs/highlight.js/issues/2534"), L = F, ka.tabReplace || ka.useBR ? L.replace(oa, function(Q) {
        return "\n" === Q ? ka.useBR ? "<br>" : Q : ka.tabReplace ? Q.replace(/\t/g, ka.tabReplace) : Q;
      }) : L;
      var L;
    }, highlightElement:O, highlightBlock:function(F) {
      return M("10.7.0", "highlightBlock will be removed entirely in v12.0"), M("10.7.0", "Please use highlightElement now."), O(F);
    }, configure:function(F) {
      F.useBR && (M("10.3.0", "'useBR' will be removed entirely in v11.0"), M("10.3.0", "Please see https://github.com/highlightjs/highlight.js/issues/2559"));
      ka = f(ka, F);
    }, initHighlighting:Da, initHighlightingOnLoad:function() {
      M("10.6.0", "initHighlightingOnLoad() is deprecated.  Use highlightAll() instead.");
      Ca = !0;
    }, registerLanguage:function(F, L) {
      var Q = null;
      try {
        Q = L(q);
      } catch (U) {
        console.error("Language definition for '{}' could not be registered.".replace("{}", F));
        if (!ra) {
          throw U;
        }
        console.error(U);
        Q = qa;
      }
      Q.name || (Q.name = F);
      aa[F] = Q;
      Q.rawDefinition = L.bind(null, q);
      Q.aliases && H(Q.aliases, {languageName:F});
    }, unregisterLanguage:function(F) {
      delete aa[F];
      for (var L = $jscomp.makeIterator(Object.keys(T)), Q = L.next(); !Q.done; Q = L.next()) {
        Q = Q.value, T[Q] === F && delete T[Q];
      }
    }, listLanguages:function() {
      return Object.keys(aa);
    }, getLanguage:A, registerAliases:H, requireLanguage:function(F) {
      M("10.4.0", "requireLanguage will be removed entirely in v11.");
      M("10.4.0", "Please see https://github.com/highlightjs/highlight.js/pull/2844");
      var L = A(F);
      if (L) {
        return L;
      }
      throw Error("The '{}' language is required, but not loaded.".replace("{}", F));
    }, autoDetection:fa, inherit:f, addPlugin:function(F) {
      (function(L) {
        L["before:highlightBlock"] && !L["before:highlightElement"] && (L["before:highlightElement"] = function(Q) {
          L["before:highlightBlock"](Object.assign({block:Q.el}, Q));
        });
        L["after:highlightBlock"] && !L["after:highlightElement"] && (L["after:highlightElement"] = function(Q) {
          L["after:highlightBlock"](Object.assign({block:Q.el}, Q));
        });
      })(F);
      ja.push(F);
    }, vuePlugin:P(q).VuePlugin});
    q.debugMode = function() {
      ra = !1;
    };
    q.safeMode = function() {
      ra = !0;
    };
    q.versionString = "10.7.2";
    for (var Fa in p) {
      "object" == typeof p[Fa] && c(p[Fa]);
    }
    return Object.assign(q, p), q.addPlugin({"before:highlightElement":function(F) {
      F = F.el;
      ka.useBR && (F.innerHTML = F.innerHTML.replace(/\n/g, "").replace(/<br[ /]*>/g, "\n"));
    }, "after:highlightElement":function(F) {
      F = F.result;
      ka.useBR && (F.value = F.value.replace(/\n/g, "<br>"));
    }}), q.addPlugin(I), q.addPlugin({"after:highlightElement":function(F) {
      F = F.result;
      ka.tabReplace && (F.value = F.value.replace(Ia, function(L) {
        return L.replace(/\t/g, ka.tabReplace);
      }));
    }}), q;
  }({});
}();
"object" == typeof exports && "undefined" != typeof module && (module.exports = hljs);
hljs.registerLanguage("nginx", function() {
  return function(c) {
    var d = {className:"variable", variants:[{begin:/\$\d+/}, {begin:/\$\{/, end:/\}/}, {begin:/[$@]/ + c.UNDERSCORE_IDENT_RE}]};
    return {name:"Nginx config", aliases:["nginxconf"], contains:[c.HASH_COMMENT_MODE, {begin:c.UNDERSCORE_IDENT_RE + "\\s+\\{", returnBegin:!0, end:/\{/, contains:[{className:"section", begin:c.UNDERSCORE_IDENT_RE}], relevance:0}, {begin:c.UNDERSCORE_IDENT_RE + "\\s", end:";|\\{", returnBegin:!0, contains:[{className:"attribute", begin:c.UNDERSCORE_IDENT_RE, starts:{endsWithParent:!0, keywords:{$pattern:"[a-z/_]+", literal:"on off yes no true false none blocked debug info notice warn error crit select break last permanent redirect kqueue rtsig epoll poll /dev/poll"}, 
    relevance:0, illegal:"=>", contains:[c.HASH_COMMENT_MODE, {className:"string", contains:[c.BACKSLASH_ESCAPE, d], variants:[{begin:/"/, end:/"/}, {begin:/'/, end:/'/}]}, {begin:"([a-z]+):/", end:"\\s", endsWithParent:!0, excludeEnd:!0, contains:[d]}, {className:"regexp", contains:[c.BACKSLASH_ESCAPE, d], variants:[{begin:"\\s\\^", end:"\\s|\\{|;", returnEnd:!0}, {begin:"~\\*?\\s+", end:"\\s|\\{|;", returnEnd:!0}, {begin:"\\*(\\.[a-z\\-]+)+"}, {begin:"([a-z\\-]+\\.)+\\*"}]}, {className:"number", 
    begin:"\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}(:\\d{1,5})?\\b"}, {className:"number", begin:"\\b\\d+[kKmMgGdshdwy]*\\b", relevance:0}, d]}}], relevance:0}], illegal:"[^\\s\\}]"};
  };
}());
hljs.registerLanguage("less", function() {
  var c = "a abbr address article aside audio b blockquote body button canvas caption cite code dd del details dfn div dl dt em fieldset figcaption figure footer form h1 h2 h3 h4 h5 h6 header hgroup html i iframe img input ins kbd label legend li main mark menu nav object ol p q quote samp section span strong summary sup table tbody td textarea tfoot th thead time tr ul var video".split(" "), d = "any-hover any-pointer aspect-ratio color color-gamut color-index device-aspect-ratio device-height device-width display-mode forced-colors grid height hover inverted-colors monochrome orientation overflow-block overflow-inline pointer prefers-color-scheme prefers-contrast prefers-reduced-motion prefers-reduced-transparency resolution scan scripting update width min-width max-width min-height max-height".split(" "), 
  f = "active any-link blank checked current default defined dir disabled drop empty enabled first first-child first-of-type fullscreen future focus focus-visible focus-within has host host-context hover indeterminate in-range invalid is lang last-child last-of-type left link local-link not nth-child nth-col nth-last-child nth-last-col nth-last-of-type nth-of-type only-child only-of-type optional out-of-range past placeholder-shown read-only read-write required right root scope target target-within user-invalid valid visited where".split(" "), 
  b = "after backdrop before cue cue-region first-letter first-line grammar-error marker part placeholder selection slotted spelling-error".split(" "), h = "align-content align-items align-self animation animation-delay animation-direction animation-duration animation-fill-mode animation-iteration-count animation-name animation-play-state animation-timing-function auto backface-visibility background background-attachment background-clip background-color background-image background-origin background-position background-repeat background-size border border-bottom border-bottom-color border-bottom-left-radius border-bottom-right-radius border-bottom-style border-bottom-width border-collapse border-color border-image border-image-outset border-image-repeat border-image-slice border-image-source border-image-width border-left border-left-color border-left-style border-left-width border-radius border-right border-right-color border-right-style border-right-width border-spacing border-style border-top border-top-color border-top-left-radius border-top-right-radius border-top-style border-top-width border-width bottom box-decoration-break box-shadow box-sizing break-after break-before break-inside caption-side clear clip clip-path color column-count column-fill column-gap column-rule column-rule-color column-rule-style column-rule-width column-span column-width columns content counter-increment counter-reset cursor direction display empty-cells filter flex flex-basis flex-direction flex-flow flex-grow flex-shrink flex-wrap float font font-display font-family font-feature-settings font-kerning font-language-override font-size font-size-adjust font-smoothing font-stretch font-style font-variant font-variant-ligatures font-variation-settings font-weight height hyphens icon image-orientation image-rendering image-resolution ime-mode inherit initial justify-content left letter-spacing line-height list-style list-style-image list-style-position list-style-type margin margin-bottom margin-left margin-right margin-top marks mask max-height max-width min-height min-width nav-down nav-index nav-left nav-right nav-up none normal object-fit object-position opacity order orphans outline outline-color outline-offset outline-style outline-width overflow overflow-wrap overflow-x overflow-y padding padding-bottom padding-left padding-right padding-top page-break-after page-break-before page-break-inside perspective perspective-origin pointer-events position quotes resize right src tab-size table-layout text-align text-align-last text-decoration text-decoration-color text-decoration-line text-decoration-style text-indent text-overflow text-rendering text-shadow text-transform text-underline-position top transform transform-origin transform-style transition transition-delay transition-duration transition-property transition-timing-function unicode-bidi vertical-align visibility white-space widows width word-break word-spacing word-wrap z-index".split(" ").reverse(), 
  k = f.concat(b);
  return function(u) {
    var x = {className:"selector-attr", begin:/\[/, end:/\]/, illegal:"$", contains:[u.APOS_STRING_MODE, u.QUOTE_STRING_MODE]}, t = [], r = [], w = function(Y) {
      return {className:"string", begin:"~?" + Y + ".*?" + Y};
    }, B = function(Y, X, a) {
      return {className:Y, begin:X, relevance:a};
    }, P = {$pattern:/[a-z-]+/, keyword:"and or not only", attribute:d.join(" ")}, ca = {begin:"\\(", end:"\\)", contains:r, keywords:P, relevance:0};
    r.push(u.C_LINE_COMMENT_MODE, u.C_BLOCK_COMMENT_MODE, w("'"), w('"'), u.CSS_NUMBER_MODE, {begin:"(url|data-uri)\\(", starts:{className:"string", end:"[\\)\\n]", excludeEnd:!0}}, {className:"number", begin:"#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})"}, ca, B("variable", "@@?[\\w-]+", 10), B("variable", "@\\{[\\w-]+\\}"), B("built_in", "~?`[^`]*?`"), {className:"attribute", begin:"[\\w-]+\\s*:", end:":", returnBegin:!0, excludeEnd:!0}, {className:"meta", begin:"!important"});
    ca = r.concat({begin:/\{/, end:/\}/, contains:t});
    var R = {beginKeywords:"when", endsWithParent:!0, contains:[{beginKeywords:"and not"}].concat(r)};
    w = {begin:"([\\w-]+|@\\{[\\w-]+\\})\\s*:", returnBegin:!0, end:/[;}]/, relevance:0, contains:[{begin:/-(webkit|moz|ms|o)-/}, {className:"attribute", begin:"\\b(" + h.join("|") + ")\\b", end:/(?=:)/, starts:{endsWithParent:!0, illegal:"[<=$]", relevance:0, contains:r}}]};
    r = {className:"keyword", begin:"@(import|media|charset|font-face|(-[a-z]+-)?keyframes|supports|document|namespace|page|viewport|host)\\b", starts:{end:"[;{}]", keywords:P, returnEnd:!0, contains:r, relevance:0}};
    P = {className:"variable", variants:[{begin:"@[\\w-]+\\s*:", relevance:15}, {begin:"@[\\w-]+"}], starts:{end:"[;}]", returnEnd:!0, contains:ca}};
    x = {variants:[{begin:"[\\.#:&\\[>]", end:"[;{}]"}, {begin:"([\\w-]+|@\\{[\\w-]+\\})", end:/\{/}], returnBegin:!0, returnEnd:!0, illegal:"[<='$\"]", relevance:0, contains:[u.C_LINE_COMMENT_MODE, u.C_BLOCK_COMMENT_MODE, R, B("keyword", "all\\b"), B("variable", "@\\{[\\w-]+\\}"), {begin:"\\b(" + c.join("|") + ")\\b", className:"selector-tag"}, B("selector-tag", "([\\w-]+|@\\{[\\w-]+\\})%?", 0), B("selector-id", "#([\\w-]+|@\\{[\\w-]+\\})"), B("selector-class", "\\.([\\w-]+|@\\{[\\w-]+\\})", 0), 
    B("selector-tag", "&", 0), x, {className:"selector-pseudo", begin:":(" + f.join("|") + ")"}, {className:"selector-pseudo", begin:"::(" + b.join("|") + ")"}, {begin:"\\(", end:"\\)", contains:ca}, {begin:"!important"}]};
    B = {begin:"[\\w-]+:(:)?(" + k.join("|") + ")", returnBegin:!0, contains:[x]};
    return t.push(u.C_LINE_COMMENT_MODE, u.C_BLOCK_COMMENT_MODE, r, P, B, w, x), {name:"Less", case_insensitive:!0, illegal:"[=>'/<($\"]", contains:t};
  };
}());
hljs.registerLanguage("python", function() {
  return function(c) {
    var d = {$pattern:/[A-Za-z]\w+|__\w+__/, keyword:"and as assert async await break class continue def del elif else except finally for from global if import in is lambda nonlocal|10 not or pass raise return try while with yield".split(" "), built_in:"__import__ abs all any ascii bin bool breakpoint bytearray bytes callable chr classmethod compile complex delattr dict dir divmod enumerate eval exec filter float format frozenset getattr globals hasattr hash help hex id input int isinstance issubclass iter len list locals map max memoryview min next object oct open ord pow print property range repr reversed round set setattr slice sorted staticmethod str sum super tuple type vars zip".split(" "), 
    literal:"__debug__ Ellipsis False None NotImplemented True".split(" "), type:"Any Callable Coroutine Dict List Literal Generic Optional Sequence Set Tuple Type Union".split(" ")}, f = {className:"meta", begin:/^(>>>|\.\.\.) /}, b = {className:"subst", begin:/\{/, end:/\}/, keywords:d, illegal:/#/}, h = {begin:/\{\{/, relevance:0};
    h = {className:"string", contains:[c.BACKSLASH_ESCAPE], variants:[{begin:/([uU]|[bB]|[rR]|[bB][rR]|[rR][bB])?'''/, end:/'''/, contains:[c.BACKSLASH_ESCAPE, f], relevance:10}, {begin:/([uU]|[bB]|[rR]|[bB][rR]|[rR][bB])?"""/, end:/"""/, contains:[c.BACKSLASH_ESCAPE, f], relevance:10}, {begin:/([fF][rR]|[rR][fF]|[fF])'''/, end:/'''/, contains:[c.BACKSLASH_ESCAPE, f, h, b]}, {begin:/([fF][rR]|[rR][fF]|[fF])"""/, end:/"""/, contains:[c.BACKSLASH_ESCAPE, f, h, b]}, {begin:/([uU]|[rR])'/, end:/'/, relevance:10}, 
    {begin:/([uU]|[rR])"/, end:/"/, relevance:10}, {begin:/([bB]|[bB][rR]|[rR][bB])'/, end:/'/}, {begin:/([bB]|[bB][rR]|[rR][bB])"/, end:/"/}, {begin:/([fF][rR]|[rR][fF]|[fF])'/, end:/'/, contains:[c.BACKSLASH_ESCAPE, h, b]}, {begin:/([fF][rR]|[rR][fF]|[fF])"/, end:/"/, contains:[c.BACKSLASH_ESCAPE, h, b]}, c.APOS_STRING_MODE, c.QUOTE_STRING_MODE]};
    var k = {className:"number", relevance:0, variants:[{begin:"(\\b([0-9](_?[0-9])*)|((\\b([0-9](_?[0-9])*))?\\.([0-9](_?[0-9])*)|\\b([0-9](_?[0-9])*)\\.))[eE][+-]?([0-9](_?[0-9])*)[jJ]?\\b"}, {begin:"((\\b([0-9](_?[0-9])*))?\\.([0-9](_?[0-9])*)|\\b([0-9](_?[0-9])*)\\.)[jJ]?"}, {begin:"\\b([1-9](_?[0-9])*|0+(_?0)*)[lLjJ]?\\b"}, {begin:"\\b0[bB](_?[01])+[lL]?\\b"}, {begin:"\\b0[oO](_?[0-7])+[lL]?\\b"}, {begin:"\\b0[xX](_?[0-9a-fA-F])+[lL]?\\b"}, {begin:"\\b([0-9](_?[0-9])*)[jJ]\\b"}]}, u = {className:"comment", 
    begin:(x = /# type:/, function(t) {
      for (var r = [], w = 0; w < arguments.length; ++w) {
        r[w - 0] = arguments[w];
      }
      return r.map(function(B) {
        return B ? "string" == typeof B ? B : B.source : null;
      }).join("");
    }("(?=", x, ")")), end:/$/, keywords:d, contains:[{begin:/# type:/}, {begin:/#/, end:/\b\B/, endsWithParent:!0}]};
    x = {className:"params", variants:[{className:"", begin:/\(\s*\)/, skip:!0}, {begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:d, contains:["self", f, k, h, c.HASH_COMMENT_MODE]}]};
    var x;
    return b.contains = [h, k, f], {name:"Python", aliases:["py", "gyp", "ipython"], keywords:d, illegal:/(<\/|->|\?)|=>/, contains:[f, k, {begin:/\bself\b/}, {beginKeywords:"if", relevance:0}, h, u, c.HASH_COMMENT_MODE, {variants:[{className:"function", beginKeywords:"def"}, {className:"class", beginKeywords:"class"}], end:/:/, illegal:/[${=;\n,]/, contains:[c.UNDERSCORE_TITLE_MODE, x, {begin:/->/, endsWithParent:!0, keywords:d}]}, {className:"meta", begin:/^[\t ]*@/, end:/(?=#)|$/, contains:[k, 
    x, h]}]};
  };
}());
hljs.registerLanguage("r", function() {
  function c(d) {
    for (var f = [], b = 0; b < arguments.length; ++b) {
      f[b - 0] = arguments[b];
    }
    return f.map(function(h) {
      return (k = h) ? "string" == typeof k ? k : k.source : null;
      var k;
    }).join("");
  }
  return function(d) {
    var f = /(?:(?:[a-zA-Z]|\.[._a-zA-Z])[._a-zA-Z0-9]*)|\.(?!\d)/;
    return {name:"R", illegal:/->/, keywords:{$pattern:f, keyword:"function if in break next repeat else for while", literal:"NULL NA TRUE FALSE Inf NaN NA_integer_|10 NA_real_|10 NA_character_|10 NA_complex_|10", built_in:"LETTERS letters month.abb month.name pi T F abs acos acosh all any anyNA Arg as.call as.character as.complex as.double as.environment as.integer as.logical as.null.default as.numeric as.raw asin asinh atan atanh attr attributes baseenv browser c call ceiling class Conj cos cosh cospi cummax cummin cumprod cumsum digamma dim dimnames emptyenv exp expression floor forceAndCall gamma gc.time globalenv Im interactive invisible is.array is.atomic is.call is.character is.complex is.double is.environment is.expression is.finite is.function is.infinite is.integer is.language is.list is.logical is.matrix is.na is.name is.nan is.null is.numeric is.object is.pairlist is.raw is.recursive is.single is.symbol lazyLoadDBfetch length lgamma list log max min missing Mod names nargs nzchar oldClass on.exit pos.to.env proc.time prod quote range Re rep retracemem return round seq_along seq_len seq.int sign signif sin sinh sinpi sqrt standardGeneric substitute sum switch tan tanh tanpi tracemem trigamma trunc unclass untracemem UseMethod xtfrm"}, 
    compilerExtensions:[function(b, h) {
      if (b.beforeMatch) {
        if (b.starts) {
          throw Error("beforeMatch cannot be used with starts");
        }
        h = Object.assign({}, b);
        Object.keys(b).forEach(function(k) {
          delete b[k];
        });
        b.begin = c(h.beforeMatch, c("(?=", h.begin, ")"));
        b.starts = {relevance:0, contains:[Object.assign(h, {endsParent:!0})]};
        b.relevance = 0;
        delete h.beforeMatch;
      }
    }], contains:[d.COMMENT(/#'/, /$/, {contains:[{className:"doctag", begin:"@examples", starts:{contains:[{begin:/\n/}, {begin:/#'\s*(?=@[a-zA-Z]+)/, endsParent:!0}, {begin:/#'/, end:/$/, excludeBegin:!0}]}}, {className:"doctag", begin:"@param", end:/$/, contains:[{className:"variable", variants:[{begin:f}, {begin:/`(?:\\.|[^`\\])+`/}], endsParent:!0}]}, {className:"doctag", begin:/@[a-zA-Z]+/}, {className:"meta-keyword", begin:/\\[a-zA-Z]+/}]}), d.HASH_COMMENT_MODE, {className:"string", contains:[d.BACKSLASH_ESCAPE], 
    variants:[d.END_SAME_AS_BEGIN({begin:/[rR]"(-*)\(/, end:/\)(-*)"/}), d.END_SAME_AS_BEGIN({begin:/[rR]"(-*)\{/, end:/\}(-*)"/}), d.END_SAME_AS_BEGIN({begin:/[rR]"(-*)\[/, end:/\](-*)"/}), d.END_SAME_AS_BEGIN({begin:/[rR]'(-*)\(/, end:/\)(-*)'/}), d.END_SAME_AS_BEGIN({begin:/[rR]'(-*)\{/, end:/\}(-*)'/}), d.END_SAME_AS_BEGIN({begin:/[rR]'(-*)\[/, end:/\](-*)'/}), {begin:'"', end:'"', relevance:0}, {begin:"'", end:"'", relevance:0}]}, {className:"number", relevance:0, beforeMatch:/([^a-zA-Z0-9._])/, 
    variants:[{match:/0[xX][0-9a-fA-F]+\.[0-9a-fA-F]*[pP][+-]?\d+i?/}, {match:/0[xX][0-9a-fA-F]+([pP][+-]?\d+)?[Li]?/}, {match:/(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?[Li]?/}]}, {begin:"%", end:"%"}, {begin:c(/[a-zA-Z][a-zA-Z_0-9]*/, "\\s+<-\\s+")}, {begin:"`", end:"`", contains:[{begin:/\\./}]}]};
  };
}());
hljs.registerLanguage("bash", function() {
  function c(d) {
    for (var f = [], b = 0; b < arguments.length; ++b) {
      f[b - 0] = arguments[b];
    }
    return f.map(function(h) {
      return (k = h) ? "string" == typeof k ? k : k.source : null;
      var k;
    }).join("");
  }
  return function(d) {
    var f = {}, b = {begin:/\$\{/, end:/\}/, contains:["self", {begin:/:-/, contains:[f]}]};
    Object.assign(f, {className:"variable", variants:[{begin:c(/\$[\w\d#@][\w\d_]*/, "(?![\\w\\d])(?![$])")}, b]});
    var h = {className:"subst", begin:/\$\(/, end:/\)/, contains:[d.BACKSLASH_ESCAPE]};
    b = {begin:/<<-?\s*(?=\w+)/, starts:{contains:[d.END_SAME_AS_BEGIN({begin:/(\w+)/, end:/(\w+)/, className:"string"})]}};
    var k = {className:"string", begin:/"/, end:/"/, contains:[d.BACKSLASH_ESCAPE, f, h]};
    h.contains.push(k);
    h = {begin:/\$\(\(/, end:/\)\)/, contains:[{begin:/\d+#[0-9a-f]+/, className:"number"}, d.NUMBER_MODE, f]};
    var u = d.SHEBANG({binary:"(fish|bash|zsh|sh|csh|ksh|tcsh|dash|scsh)", relevance:10}), x = {className:"function", begin:/\w[\w\d_]*\s*\(\s*\)\s*\{/, returnBegin:!0, contains:[d.inherit(d.TITLE_MODE, {begin:/\w[\w\d_]*/})], relevance:0};
    return {name:"Bash", aliases:["sh", "zsh"], keywords:{$pattern:/\b[a-z._-]+\b/, keyword:"if then else elif fi for while in do done case esac function", literal:"true false", built_in:"break cd continue eval exec exit export getopts hash pwd readonly return shift test times trap umask unset alias bind builtin caller command declare echo enable help let local logout mapfile printf read readarray source type typeset ulimit unalias set shopt autoload bg bindkey bye cap chdir clone comparguments compcall compctl compdescribe compfiles compgroups compquote comptags comptry compvalues dirs disable disown echotc echoti emulate fc fg float functions getcap getln history integer jobs kill limit log noglob popd print pushd pushln rehash sched setcap setopt stat suspend ttyctl unfunction unhash unlimit unsetopt vared wait whence where which zcompile zformat zftp zle zmodload zparseopts zprof zpty zregexparse zsocket zstyle ztcp"}, 
    contains:[u, d.SHEBANG(), x, h, d.HASH_COMMENT_MODE, b, k, {className:"", begin:/\\"/}, {className:"string", begin:/'/, end:/'/}, f]};
  };
}());
hljs.registerLanguage("shell", function() {
  return function(c) {
    return {name:"Shell Session", aliases:["console"], contains:[{className:"meta", begin:/^\s{0,3}[/~\w\d[\]()@-]*[>%$#]/, starts:{end:/[^\\](?=\s*$)/, subLanguage:"bash"}}]};
  };
}());
hljs.registerLanguage("properties", function() {
  return function(c) {
    var d = {end:"([ \\t\\f]*[:=][ \\t\\f]*|[ \\t\\f]+)", relevance:0, starts:{className:"string", end:/$/, relevance:0, contains:[{begin:"\\\\\\\\"}, {begin:"\\\\\\n"}]}};
    return {name:".properties", case_insensitive:!0, illegal:/\S/, contains:[c.COMMENT("^\\s*[!#]", "$"), {returnBegin:!0, variants:[{begin:"([^\\\\\\W:= \\t\\f\\n]|\\\\.)+[ \\t\\f]*[:=][ \\t\\f]*", relevance:1}, {begin:"([^\\\\\\W:= \\t\\f\\n]|\\\\.)+[ \\t\\f]+", relevance:0}], contains:[{className:"attr", begin:"([^\\\\\\W:= \\t\\f\\n]|\\\\.)+", endsParent:!0, relevance:0}], starts:d}, {begin:"([^\\\\:= \\t\\f\\n]|\\\\.)+([ \\t\\f]*[:=][ \\t\\f]*|[ \\t\\f]+)", returnBegin:!0, relevance:0, contains:[{className:"meta", 
    begin:"([^\\\\:= \\t\\f\\n]|\\\\.)+", endsParent:!0, relevance:0}], starts:d}, {className:"attr", relevance:0, begin:"([^\\\\:= \\t\\f\\n]|\\\\.)+[ \\t\\f]*$"}]};
  };
}());
hljs.registerLanguage("xml", function() {
  function c(f) {
    for (var b = [], h = 0; h < arguments.length; ++h) {
      b[h - 0] = arguments[h];
    }
    return b.map(function(k) {
      return k ? "string" == typeof k ? k : k.source : null;
    }).join("");
  }
  function d(f) {
    for (var b = [], h = 0; h < arguments.length; ++h) {
      b[h - 0] = arguments[h];
    }
    return "(" + b.map(function(k) {
      return k ? "string" == typeof k ? k : k.source : null;
    }).join("|") + ")";
  }
  return function(f) {
    var b = c(/[A-Z_]/, c("(", /[A-Z0-9_.-]*:/, ")?"), /[A-Z0-9_.-]*/), h = {className:"symbol", begin:/&[a-z]+;|&#[0-9]+;|&#x[a-f0-9]+;/}, k = {begin:/\s/, contains:[{className:"meta-keyword", begin:/#?[a-z_][a-z1-9_-]+/, illegal:/\n/}]}, u = f.inherit(k, {begin:/\(/, end:/\)/}), x = f.inherit(f.APOS_STRING_MODE, {className:"meta-string"}), t = f.inherit(f.QUOTE_STRING_MODE, {className:"meta-string"}), r = {endsWithParent:!0, illegal:/</, relevance:0, contains:[{className:"attr", begin:/[A-Za-z0-9._:-]+/, 
    relevance:0}, {begin:/=\s*/, relevance:0, contains:[{className:"string", endsParent:!0, variants:[{begin:/"/, end:/"/, contains:[h]}, {begin:/'/, end:/'/, contains:[h]}, {begin:/[^\s"'=<>`]+/}]}]}]};
    return {name:"HTML, XML", aliases:"html xhtml rss atom xjb xsd xsl plist wsf svg".split(" "), case_insensitive:!0, contains:[{className:"meta", begin:/<![a-z]/, end:/>/, relevance:10, contains:[k, t, x, u, {begin:/\[/, end:/\]/, contains:[{className:"meta", begin:/<![a-z]/, end:/>/, contains:[k, u, t, x]}]}]}, f.COMMENT(/\x3c!--/, /--\x3e/, {relevance:10}), {begin:/<!\[CDATA\[/, end:/\]\]>/, relevance:10}, h, {className:"meta", begin:/<\?xml/, end:/\?>/, relevance:10}, {className:"tag", begin:/<style(?=\s|>)/, 
    end:/>/, keywords:{name:"style"}, contains:[r], starts:{end:/<\/style>/, returnEnd:!0, subLanguage:["css", "xml"]}}, {className:"tag", begin:/<script(?=\s|>)/, end:/>/, keywords:{name:"script"}, contains:[r], starts:{end:/<\/script>/, returnEnd:!0, subLanguage:["javascript", "handlebars", "xml"]}}, {className:"tag", begin:/<>|<\/>/}, {className:"tag", begin:c(/</, c("(?=", c(b, d(/\/>/, />/, /\s/)), ")")), end:/\/?>/, contains:[{className:"name", begin:b, relevance:0, starts:r}]}, {className:"tag", 
    begin:c(/<\//, c("(?=", c(b, />/), ")")), contains:[{className:"name", begin:b, relevance:0}, {begin:/>/, relevance:0, endsParent:!0}]}]};
  };
}());
hljs.registerLanguage("basic", function() {
  return function(c) {
    return {name:"BASIC", case_insensitive:!0, illegal:"^.", keywords:{$pattern:"[a-zA-Z][a-zA-Z0-9_$%!#]*", keyword:"ABS ASC AND ATN AUTO|0 BEEP BLOAD|10 BSAVE|10 CALL CALLS CDBL CHAIN CHDIR CHR$|10 CINT CIRCLE CLEAR CLOSE CLS COLOR COM COMMON CONT COS CSNG CSRLIN CVD CVI CVS DATA DATE$ DEFDBL DEFINT DEFSNG DEFSTR DEF|0 SEG USR DELETE DIM DRAW EDIT END ENVIRON ENVIRON$ EOF EQV ERASE ERDEV ERDEV$ ERL ERR ERROR EXP FIELD FILES FIX FOR|0 FRE GET GOSUB|10 GOTO HEX$ IF THEN ELSE|0 INKEY$ INP INPUT INPUT# INPUT$ INSTR IMP INT IOCTL IOCTL$ KEY ON OFF LIST KILL LEFT$ LEN LET LINE LLIST LOAD LOC LOCATE LOF LOG LPRINT USING LSET MERGE MID$ MKDIR MKD$ MKI$ MKS$ MOD NAME NEW NEXT NOISE NOT OCT$ ON OR PEN PLAY STRIG OPEN OPTION BASE OUT PAINT PALETTE PCOPY PEEK PMAP POINT POKE POS PRINT PRINT] PSET PRESET PUT RANDOMIZE READ REM RENUM RESET|0 RESTORE RESUME RETURN|0 RIGHT$ RMDIR RND RSET RUN SAVE SCREEN SGN SHELL SIN SOUND SPACE$ SPC SQR STEP STICK STOP STR$ STRING$ SWAP SYSTEM TAB TAN TIME$ TIMER TROFF TRON TO USR VAL VARPTR VARPTR$ VIEW WAIT WHILE WEND WIDTH WINDOW WRITE XOR"}, 
    contains:[c.QUOTE_STRING_MODE, c.COMMENT("REM", "$", {relevance:10}), c.COMMENT("'", "$", {relevance:0}), {className:"symbol", begin:"^[0-9]+ ", relevance:10}, {className:"number", begin:"\\b\\d+(\\.\\d+)?([edED]\\d+)?[#!]?", relevance:0}, {className:"number", begin:"(&[hH][0-9a-fA-F]{1,4})"}, {className:"number", begin:"(&[oO][0-7]{1,6})"}]};
  };
}());
hljs.registerLanguage("c", function() {
  function c(d) {
    return function(f) {
      for (var b = [], h = 0; h < arguments.length; ++h) {
        b[h - 0] = arguments[h];
      }
      return b.map(function(k) {
        return k ? "string" == typeof k ? k : k.source : null;
      }).join("");
    }("(", d, ")?");
  }
  return function(d) {
    var f = d.COMMENT("//", "$", {contains:[{begin:/\\\n/}]}), b = "(decltype\\(auto\\)|" + c("[a-zA-Z_]\\w*::") + "[a-zA-Z_]\\w*" + c("<[^<>]+>") + ")", h = {className:"keyword", begin:"\\b[a-z\\d_]*_t\\b"}, k = {className:"string", variants:[{begin:'(u8?|U|L)?"', end:'"', illegal:"\\n", contains:[d.BACKSLASH_ESCAPE]}, {begin:"(u8?|U|L)?'(\\\\(x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4,8}|[0-7]{3}|\\S)|.)", end:"'", illegal:"."}, d.END_SAME_AS_BEGIN({begin:/(?:u8?|U|L)?R"([^()\\ ]{0,16})\(/, end:/\)([^()\\ ]{0,16})"/})]}, 
    u = {className:"number", variants:[{begin:"\\b(0b[01']+)"}, {begin:"(-?)\\b([\\d']+(\\.[\\d']*)?|\\.[\\d']+)((ll|LL|l|L)(u|U)?|(u|U)(ll|LL|l|L)?|f|F|b|B)"}, {begin:"(-?)(\\b0[xX][a-fA-F0-9']+|(\\b[\\d']+(\\.[\\d']*)?|\\.[\\d']+)([eE][-+]?[\\d']+)?)"}], relevance:0}, x = {className:"meta", begin:/#\s*[a-z]+\b/, end:/$/, keywords:{"meta-keyword":"if else elif endif define undef warning error line pragma _Pragma ifdef ifndef include"}, contains:[{begin:/\\\n/, relevance:0}, d.inherit(k, {className:"meta-string"}), 
    {className:"meta-string", begin:/<.*?>/}, f, d.C_BLOCK_COMMENT_MODE]}, t = {className:"title", begin:c("[a-zA-Z_]\\w*::") + d.IDENT_RE, relevance:0}, r = c("[a-zA-Z_]\\w*::") + d.IDENT_RE + "\\s*\\(", w = {keyword:"int float while private char char8_t char16_t char32_t catch import module export virtual operator sizeof dynamic_cast|10 typedef const_cast|10 const for static_cast|10 union namespace unsigned long volatile static protected bool template mutable if public friend do goto auto void enum else break extern using asm case typeid wchar_t short reinterpret_cast|10 default double register explicit signed typename try this switch continue inline delete alignas alignof constexpr consteval constinit decltype concept co_await co_return co_yield requires noexcept static_assert thread_local restrict final override atomic_bool atomic_char atomic_schar atomic_uchar atomic_short atomic_ushort atomic_int atomic_uint atomic_long atomic_ulong atomic_llong atomic_ullong new throw return and and_eq bitand bitor compl not not_eq or or_eq xor xor_eq", 
    built_in:"std string wstring cin cout cerr clog stdin stdout stderr stringstream istringstream ostringstream auto_ptr deque list queue stack vector map set pair bitset multiset multimap unordered_set unordered_map unordered_multiset unordered_multimap priority_queue make_pair array shared_ptr abort terminate abs acos asin atan2 atan calloc ceil cosh cos exit exp fabs floor fmod fprintf fputs free frexp fscanf future isalnum isalpha iscntrl isdigit isgraph islower isprint ispunct isspace isupper isxdigit tolower toupper labs ldexp log10 log malloc realloc memchr memcmp memcpy memset modf pow printf putchar puts scanf sinh sin snprintf sprintf sqrt sscanf strcat strchr strcmp strcpy strcspn strlen strncat strncmp strncpy strpbrk strrchr strspn strstr tanh tan vfprintf vprintf vsprintf endl initializer_list unique_ptr _Bool complex _Complex imaginary _Imaginary", 
    literal:"true false nullptr NULL"}, B = [x, h, f, d.C_BLOCK_COMMENT_MODE, u, k], P = {variants:[{begin:/=/, end:/;/}, {begin:/\(/, end:/\)/}, {beginKeywords:"new throw return else", end:/;/}], keywords:w, contains:B.concat([{begin:/\(/, end:/\)/, keywords:w, contains:B.concat(["self"]), relevance:0}]), relevance:0};
    return {name:"C", aliases:["h"], keywords:w, disableAutodetect:!0, illegal:"</", contains:[].concat(P, {className:"function", begin:"(" + b + "[\\*&\\s]+)+" + r, returnBegin:!0, end:/[{;=]/, excludeEnd:!0, keywords:w, illegal:/[^\w\s\*&:<>.]/, contains:[{begin:"decltype\\(auto\\)", keywords:w, relevance:0}, {begin:r, returnBegin:!0, contains:[t], relevance:0}, {className:"params", begin:/\(/, end:/\)/, keywords:w, relevance:0, contains:[f, d.C_BLOCK_COMMENT_MODE, k, u, h, {begin:/\(/, end:/\)/, 
    keywords:w, relevance:0, contains:["self", f, d.C_BLOCK_COMMENT_MODE, k, u, h]}]}, h, f, d.C_BLOCK_COMMENT_MODE, x]}, B, [x, {begin:"\\b(deque|list|queue|priority_queue|pair|stack|vector|map|set|bitset|multiset|multimap|unordered_map|unordered_set|unordered_multiset|unordered_multimap|array)\\s*<", end:">", keywords:w, contains:["self", h]}, {begin:d.IDENT_RE + "::", keywords:w}, {className:"class", beginKeywords:"enum class struct union", end:/[{;:<>=]/, contains:[{beginKeywords:"final class struct"}, 
    d.TITLE_MODE]}]), exports:{preprocessor:x, strings:k, keywords:w}};
  };
}());
hljs.registerLanguage("ruby", function() {
  function c(d) {
    for (var f = [], b = 0; b < arguments.length; ++b) {
      f[b - 0] = arguments[b];
    }
    return f.map(function(h) {
      return (k = h) ? "string" == typeof k ? k : k.source : null;
      var k;
    }).join("");
  }
  return function(d) {
    var f = {keyword:"and then defined module in return redo if BEGIN retry end for self when next until do begin unless END rescue else break undef not super class case require yield alias while ensure elsif or include attr_reader attr_writer attr_accessor __FILE__", built_in:"proc lambda", literal:"true false nil"}, b = {className:"doctag", begin:"@[A-Za-z]+"}, h = {begin:"#<", end:">"};
    b = [d.COMMENT("#", "$", {contains:[b]}), d.COMMENT("^=begin", "^=end", {contains:[b], relevance:10}), d.COMMENT("^__END__", "\\n$")];
    var k = {className:"subst", begin:/#\{/, end:/\}/, keywords:f}, u = {className:"string", contains:[d.BACKSLASH_ESCAPE, k], variants:[{begin:/'/, end:/'/}, {begin:/"/, end:/"/}, {begin:/`/, end:/`/}, {begin:/%[qQwWx]?\(/, end:/\)/}, {begin:/%[qQwWx]?\[/, end:/\]/}, {begin:/%[qQwWx]?\{/, end:/\}/}, {begin:/%[qQwWx]?</, end:/>/}, {begin:/%[qQwWx]?\//, end:/\//}, {begin:/%[qQwWx]?%/, end:/%/}, {begin:/%[qQwWx]?-/, end:/-/}, {begin:/%[qQwWx]?\|/, end:/\|/}, {begin:/\B\?(\\\d{1,3})/}, {begin:/\B\?(\\x[A-Fa-f0-9]{1,2})/}, 
    {begin:/\B\?(\\u\{?[A-Fa-f0-9]{1,6}\}?)/}, {begin:/\B\?(\\M-\\C-|\\M-\\c|\\c\\M-|\\M-|\\C-\\M-)[\x20-\x7e]/}, {begin:/\B\?\\(c|C-)[\x20-\x7e]/}, {begin:/\B\?\\?\S/}, {begin:/<<[-~]?'?(\w+)\n(?:[^\n]*\n)*?\s*\1\b/, returnBegin:!0, contains:[{begin:/<<[-~]?'?/}, d.END_SAME_AS_BEGIN({begin:/(\w+)/, end:/(\w+)/, contains:[d.BACKSLASH_ESCAPE, k]})]}]}, x = {className:"params", begin:"\\(", end:"\\)", endsParent:!0, keywords:f};
    u = [u, {className:"class", beginKeywords:"class module", end:"$|;", illegal:/=/, contains:[d.inherit(d.TITLE_MODE, {begin:"[A-Za-z_]\\w*(::\\w+)*(\\?|!)?"}), {begin:"<\\s*", contains:[{begin:"(" + d.IDENT_RE + "::)?" + d.IDENT_RE, relevance:0}]}].concat(b)}, {className:"function", begin:c(/def\s+/, (t = "([a-zA-Z_]\\w*[!?=]?|[-+~]@|<<|>>|=~|===?|<=>|[<>]=?|\\*\\*|[-/+%^&*~`|]|\\[\\]=?)\\s*(\\(|;|$)", c("(?=", t, ")"))), relevance:0, keywords:"def", end:"$|;", contains:[d.inherit(d.TITLE_MODE, 
    {begin:"([a-zA-Z_]\\w*[!?=]?|[-+~]@|<<|>>|=~|===?|<=>|[<>]=?|\\*\\*|[-/+%^&*~`|]|\\[\\]=?)"}), x].concat(b)}, {begin:d.IDENT_RE + "::"}, {className:"symbol", begin:d.UNDERSCORE_IDENT_RE + "(!|\\?)?:", relevance:0}, {className:"symbol", begin:":(?!\\s)", contains:[u, {begin:"([a-zA-Z_]\\w*[!?=]?|[-+~]@|<<|>>|=~|===?|<=>|[<>]=?|\\*\\*|[-/+%^&*~`|]|\\[\\]=?)"}], relevance:0}, {className:"number", relevance:0, variants:[{begin:"\\b([1-9](_?[0-9])*|0)(\\.([0-9](_?[0-9])*))?([eE][+-]?([0-9](_?[0-9])*)|r)?i?\\b"}, 
    {begin:"\\b0[dD][0-9](_?[0-9])*r?i?\\b"}, {begin:"\\b0[bB][0-1](_?[0-1])*r?i?\\b"}, {begin:"\\b0[oO][0-7](_?[0-7])*r?i?\\b"}, {begin:"\\b0[xX][0-9a-fA-F](_?[0-9a-fA-F])*r?i?\\b"}, {begin:"\\b0(_?[0-7])+r?i?\\b"}]}, {className:"variable", begin:"(\\$\\W)|((\\$|@@?)(\\w+))(?=[^@$?])(?![A-Za-z])(?![@$?'])"}, {className:"params", begin:/\|/, end:/\|/, relevance:0, keywords:f}, {begin:"(" + d.RE_STARTERS_RE + "|unless)\\s*", keywords:"unless", contains:[{className:"regexp", contains:[d.BACKSLASH_ESCAPE, 
    k], illegal:/\n/, variants:[{begin:"/", end:"/[a-z]*"}, {begin:/%r\{/, end:/\}[a-z]*/}, {begin:"%r\\(", end:"\\)[a-z]*"}, {begin:"%r!", end:"![a-z]*"}, {begin:"%r\\[", end:"\\][a-z]*"}]}].concat(h, b), relevance:0}].concat(h, b);
    var t;
    k.contains = u;
    x.contains = u;
    t = [{begin:/^\s*=>/, starts:{end:"$", contains:u}}, {className:"meta", begin:"^([>?]>|[\\w#]+\\(\\w+\\):\\d+:\\d+>|(\\w+-)?\\d+\\.\\d+\\.\\d+(p\\d+)?[^\\d][^>]+>)(?=[ ])", starts:{end:"$", contains:u}}];
    return b.unshift(h), {name:"Ruby", aliases:["rb", "gemspec", "podspec", "thor", "irb"], keywords:f, illegal:/\/\*/, contains:[d.SHEBANG({binary:"ruby"})].concat(t).concat(b).concat(u)};
  };
}());
hljs.registerLanguage("php", function() {
  return function(c) {
    var d = {className:"variable", begin:"\\$+[a-zA-Z_-ÿ][a-zA-Z0-9_-ÿ]*(?![A-Za-z0-9])(?![$])"}, f = {className:"meta", variants:[{begin:/<\?php/, relevance:10}, {begin:/<\?[=]?/}, {begin:/\?>/}]}, b = {className:"subst", variants:[{begin:/\$\w+/}, {begin:/\{\$/, end:/\}/}]}, h = c.inherit(c.APOS_STRING_MODE, {illegal:null}), k = c.inherit(c.QUOTE_STRING_MODE, {illegal:null, contains:c.QUOTE_STRING_MODE.contains.concat(b)});
    b = c.END_SAME_AS_BEGIN({begin:/<<<[ \t]*(\w+)\n/, end:/[ \t]*(\w+)\b/, contains:c.QUOTE_STRING_MODE.contains.concat(b)});
    h = {className:"string", contains:[c.BACKSLASH_ESCAPE, f], variants:[c.inherit(h, {begin:"b'", end:"'"}), c.inherit(k, {begin:'b"', end:'"'}), k, h, b]};
    k = {className:"number", variants:[{begin:"\\b0b[01]+(?:_[01]+)*\\b"}, {begin:"\\b0o[0-7]+(?:_[0-7]+)*\\b"}, {begin:"\\b0x[\\da-f]+(?:_[\\da-f]+)*\\b"}, {begin:"(?:\\b\\d+(?:_\\d+)*(\\.(?:\\d+(?:_\\d+)*))?|\\B\\.\\d+)(?:e[+-]?\\d+)?"}], relevance:0};
    b = {keyword:"__CLASS__ __DIR__ __FILE__ __FUNCTION__ __LINE__ __METHOD__ __NAMESPACE__ __TRAIT__ die echo exit include include_once print require require_once array abstract and as binary bool boolean break callable case catch class clone const continue declare default do double else elseif empty enddeclare endfor endforeach endif endswitch endwhile enum eval extends final finally float for foreach from global goto if implements instanceof insteadof int integer interface isset iterable list match|0 mixed new object or private protected public real return string switch throw trait try unset use var void while xor yield", 
    literal:"false null true", built_in:"Error|0 AppendIterator ArgumentCountError ArithmeticError ArrayIterator ArrayObject AssertionError BadFunctionCallException BadMethodCallException CachingIterator CallbackFilterIterator CompileError Countable DirectoryIterator DivisionByZeroError DomainException EmptyIterator ErrorException Exception FilesystemIterator FilterIterator GlobIterator InfiniteIterator InvalidArgumentException IteratorIterator LengthException LimitIterator LogicException MultipleIterator NoRewindIterator OutOfBoundsException OutOfRangeException OuterIterator OverflowException ParentIterator ParseError RangeException RecursiveArrayIterator RecursiveCachingIterator RecursiveCallbackFilterIterator RecursiveDirectoryIterator RecursiveFilterIterator RecursiveIterator RecursiveIteratorIterator RecursiveRegexIterator RecursiveTreeIterator RegexIterator RuntimeException SeekableIterator SplDoublyLinkedList SplFileInfo SplFileObject SplFixedArray SplHeap SplMaxHeap SplMinHeap SplObjectStorage SplObserver SplObserver SplPriorityQueue SplQueue SplStack SplSubject SplSubject SplTempFileObject TypeError UnderflowException UnexpectedValueException UnhandledMatchError ArrayAccess Closure Generator Iterator IteratorAggregate Serializable Stringable Throwable Traversable WeakReference WeakMap Directory __PHP_Incomplete_Class parent php_user_filter self static stdClass"};
    return {aliases:"php3 php4 php5 php6 php7 php8".split(" "), case_insensitive:!0, keywords:b, contains:[c.HASH_COMMENT_MODE, c.COMMENT("//", "$", {contains:[f]}), c.COMMENT("/\\*", "\\*/", {contains:[{className:"doctag", begin:"@[A-Za-z]+"}]}), c.COMMENT("__halt_compiler.+?;", !1, {endsWithParent:!0, keywords:"__halt_compiler"}), f, {className:"keyword", begin:/\$this\b/}, d, {begin:/(::|->)+[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/}, {className:"function", relevance:0, beginKeywords:"fn function", 
    end:/[;{]/, excludeEnd:!0, illegal:"[$%\\[]", contains:[{beginKeywords:"use"}, c.UNDERSCORE_TITLE_MODE, {begin:"=>", endsParent:!0}, {className:"params", begin:"\\(", end:"\\)", excludeBegin:!0, excludeEnd:!0, keywords:b, contains:["self", d, c.C_BLOCK_COMMENT_MODE, h, k]}]}, {className:"class", variants:[{beginKeywords:"enum", illegal:/[($"]/}, {beginKeywords:"class interface trait", illegal:/[:($"]/}], relevance:0, end:/\{/, excludeEnd:!0, contains:[{beginKeywords:"extends implements"}, c.UNDERSCORE_TITLE_MODE]}, 
    {beginKeywords:"namespace", relevance:0, end:";", illegal:/[.']/, contains:[c.UNDERSCORE_TITLE_MODE]}, {beginKeywords:"use", relevance:0, end:";", contains:[c.UNDERSCORE_TITLE_MODE]}, h, k]};
  };
}());
hljs.registerLanguage("typescript", function() {
  function c(h) {
    for (var k = [], u = 0; u < arguments.length; ++u) {
      k[u - 0] = arguments[u];
    }
    return k.map(function(x) {
      return (t = x) ? "string" == typeof t ? t : t.source : null;
      var t;
    }).join("");
  }
  var d = "as in of if for while finally var new function do return void else break catch instanceof with throw case default try switch continue typeof delete let yield const class debugger async await static import from export extends".split(" "), f = "true false null undefined NaN Infinity".split(" "), b = [].concat("setInterval setTimeout clearInterval clearTimeout require exports eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape".split(" "), 
  "arguments this super console window document localStorage module global".split(" "), "Intl DataView Number Math Date String RegExp Object Function Boolean Error Symbol Set Map WeakSet WeakMap Proxy Reflect JSON Promise Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array Float32Array Array Uint8Array Uint8ClampedArray ArrayBuffer BigInt64Array BigUint64Array BigInt".split(" "), "EvalError InternalError RangeError ReferenceError SyntaxError TypeError URIError".split(" "));
  return function(h) {
    var k = {$pattern:"[A-Za-z$_][0-9A-Za-z$_]*", keyword:d.concat("type namespace typedef interface public private protected implements declare abstract readonly".split(" ")), literal:f, built_in:b.concat("any void number boolean string object never enum".split(" "))}, u = {className:"meta", begin:"@[A-Za-z$_][0-9A-Za-z$_]*"}, x = function(r, w, B) {
      var P = r.contains.findIndex(function(ca) {
        return ca.label === w;
      });
      if (-1 === P) {
        throw Error("can not find mode to replace");
      }
      r.contains.splice(P, 1, B);
    }, t = function(r) {
      var w = /<[A-Za-z0-9\\._:-]+/, B = /\/[A-Za-z0-9\\._:-]+>|\/>/, P = {$pattern:"[A-Za-z$_][0-9A-Za-z$_]*", keyword:d, literal:f, built_in:b}, ca = {className:"number", variants:[{begin:"(\\b(0|[1-9](_?[0-9])*|0[0-7]*[89][0-9]*)((\\.([0-9](_?[0-9])*))|\\.)?|(\\.([0-9](_?[0-9])*)))[eE][+-]?([0-9](_?[0-9])*)\\b"}, {begin:"\\b(0|[1-9](_?[0-9])*|0[0-7]*[89][0-9]*)\\b((\\.([0-9](_?[0-9])*))\\b|\\.)?|(\\.([0-9](_?[0-9])*))\\b"}, {begin:"\\b(0|[1-9](_?[0-9])*)n\\b"}, {begin:"\\b0[xX][0-9a-fA-F](_?[0-9a-fA-F])*n?\\b"}, 
      {begin:"\\b0[bB][0-1](_?[0-1])*n?\\b"}, {begin:"\\b0[oO][0-7](_?[0-7])*n?\\b"}, {begin:"\\b0[0-7]+n?\\b"}], relevance:0}, R = {className:"subst", begin:"\\$\\{", end:"\\}", keywords:P, contains:[]}, Y = {begin:"html`", end:"", starts:{end:"`", returnEnd:!1, contains:[r.BACKSLASH_ESCAPE, R], subLanguage:"xml"}}, X = {begin:"css`", end:"", starts:{end:"`", returnEnd:!1, contains:[r.BACKSLASH_ESCAPE, R], subLanguage:"css"}}, a = {className:"string", begin:"`", end:"`", contains:[r.BACKSLASH_ESCAPE, 
      R]}, g = {className:"comment", variants:[r.COMMENT(/\/\*\*(?!\/)/, "\\*/", {relevance:0, contains:[{className:"doctag", begin:"@[A-Za-z]+", contains:[{className:"type", begin:"\\{", end:"\\}", relevance:0}, {className:"variable", begin:"[A-Za-z$_][0-9A-Za-z$_]*(?=\\s*(-)|$)", endsParent:!0, relevance:0}, {begin:/(?=[^\n])\s/, relevance:0}]}]}), r.C_BLOCK_COMMENT_MODE, r.C_LINE_COMMENT_MODE]}, e = [r.APOS_STRING_MODE, r.QUOTE_STRING_MODE, Y, X, a, ca, r.REGEXP_MODE];
      R.contains = e.concat({begin:/\{/, end:/\}/, keywords:P, contains:["self"].concat(e)});
      R = [].concat(g, R.contains);
      R = R.concat([{begin:/\(/, end:/\)/, keywords:P, contains:["self"].concat(R)}]);
      e = {className:"params", begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:P, contains:R};
      return {name:"Javascript", aliases:["js", "jsx", "mjs", "cjs"], keywords:P, exports:{PARAMS_CONTAINS:R}, illegal:/#(?![$_A-z])/, contains:[r.SHEBANG({label:"shebang", binary:"node", relevance:5}), {label:"use_strict", className:"meta", relevance:10, begin:/^\s*['"]use (strict|asm)['"]/}, r.APOS_STRING_MODE, r.QUOTE_STRING_MODE, Y, X, a, g, ca, {begin:c(/[{,\n]\s*/, c("(?=", c(/(((\/\/.*$)|(\/\*(\*[^/]|[^*])*\*\/))\s*)*/, "[A-Za-z$_][0-9A-Za-z$_]*\\s*:"), ")")), relevance:0, contains:[{className:"attr", 
      begin:"[A-Za-z$_][0-9A-Za-z$_]*" + c("(?=", "\\s*:", ")"), relevance:0}]}, {begin:"(" + r.RE_STARTERS_RE + "|\\b(case|return|throw)\\b)\\s*", keywords:"return throw case", contains:[g, r.REGEXP_MODE, {className:"function", begin:"(\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)|" + r.UNDERSCORE_IDENT_RE + ")\\s*=>", returnBegin:!0, end:"\\s*=>", contains:[{className:"params", variants:[{begin:r.UNDERSCORE_IDENT_RE, relevance:0}, {className:null, begin:/\(\s*\)/, skip:!0}, {begin:/\(/, 
      end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:P, contains:R}]}]}, {begin:/,/, relevance:0}, {className:"", begin:/\s/, end:/\s*/, skip:!0}, {variants:[{begin:"<>", end:"</>"}, {begin:w, "on:begin":function(l, m) {
        var v = l[0].length + l.index, n = l.input[v];
        "<" !== n ? ">" === n && (n = "</" + l[0].slice(1), -1 !== l.input.indexOf(n, v) || m.ignoreMatch()) : m.ignoreMatch();
      }, end:B}], subLanguage:"xml", contains:[{begin:w, end:B, skip:!0, contains:["self"]}]}], relevance:0}, {className:"function", beginKeywords:"function", end:/[{;]/, excludeEnd:!0, keywords:P, contains:["self", r.inherit(r.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*"}), e], illegal:/%/}, {beginKeywords:"while if switch catch for"}, {className:"function", begin:r.UNDERSCORE_IDENT_RE + "\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)\\s*\\{", returnBegin:!0, contains:[e, r.inherit(r.TITLE_MODE, 
      {begin:"[A-Za-z$_][0-9A-Za-z$_]*"})]}, {variants:[{begin:"\\.[A-Za-z$_][0-9A-Za-z$_]*"}, {begin:"\\$[A-Za-z$_][0-9A-Za-z$_]*"}], relevance:0}, {className:"class", beginKeywords:"class", end:/[{;=]/, excludeEnd:!0, illegal:/[:"[\]]/, contains:[{beginKeywords:"extends"}, r.UNDERSCORE_TITLE_MODE]}, {begin:/\b(?=constructor)/, end:/[{;]/, excludeEnd:!0, contains:[r.inherit(r.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*"}), "self", e]}, {begin:"(get|set)\\s+(?=[A-Za-z$_][0-9A-Za-z$_]*\\()", end:/\{/, 
      keywords:"get set", contains:[r.inherit(r.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*"}), {begin:/\(\)/}, e]}, {begin:/\$[(.]/}]};
    }(h);
    return Object.assign(t.keywords, k), t.exports.PARAMS_CONTAINS.push(u), t.contains = t.contains.concat([u, {beginKeywords:"namespace", end:/\{/, excludeEnd:!0}, {beginKeywords:"interface", end:/\{/, excludeEnd:!0, keywords:"interface extends"}]), x(t, "shebang", h.SHEBANG()), x(t, "use_strict", {className:"meta", relevance:10, begin:/^\s*['"]use strict['"]/}), t.contains.find(function(r) {
      return "function" === r.className;
    }).relevance = 0, Object.assign(t, {name:"TypeScript", aliases:["ts", "tsx"]}), t;
  };
}());
hljs.registerLanguage("vbnet", function() {
  function c(f) {
    for (var b = [], h = 0; h < arguments.length; ++h) {
      b[h - 0] = arguments[h];
    }
    return b.map(function(k) {
      return k ? "string" == typeof k ? k : k.source : null;
    }).join("");
  }
  function d(f) {
    for (var b = [], h = 0; h < arguments.length; ++h) {
      b[h - 0] = arguments[h];
    }
    return "(" + b.map(function(k) {
      return k ? "string" == typeof k ? k : k.source : null;
    }).join("|") + ")";
  }
  return function(f) {
    var b = /\d{1,2}\/\d{1,2}\/\d{4}/, h = /\d{4}-\d{1,2}-\d{1,2}/, k = /(\d|1[012])(:\d+){0,2} *(AM|PM)/, u = /\d{1,2}(:\d{1,2}){1,2}/;
    b = {className:"literal", variants:[{begin:c(/# */, d(h, b), / *#/)}, {begin:c(/# */, u, / *#/)}, {begin:c(/# */, k, / *#/)}, {begin:c(/# */, d(h, b), / +/, d(k, u), / *#/)}]};
    h = f.COMMENT(/'''/, /$/, {contains:[{className:"doctag", begin:/<\/?/, end:/>/}]});
    f = f.COMMENT(null, /$/, {variants:[{begin:/'/}, {begin:/([\t ]|^)REM(?=\s)/}]});
    return {name:"Visual Basic .NET", aliases:["vb"], case_insensitive:!0, classNameAliases:{label:"symbol"}, keywords:{keyword:"addhandler alias aggregate ansi as async assembly auto binary by byref byval call case catch class compare const continue custom declare default delegate dim distinct do each equals else elseif end enum erase error event exit explicit finally for friend from function get global goto group handles if implements imports in inherits interface into iterator join key let lib loop me mid module mustinherit mustoverride mybase myclass namespace narrowing new next notinheritable notoverridable of off on operator option optional order overloads overridable overrides paramarray partial preserve private property protected public raiseevent readonly redim removehandler resume return select set shadows shared skip static step stop structure strict sub synclock take text then throw to try unicode until using when where while widening with withevents writeonly yield", 
    built_in:"addressof and andalso await directcast gettype getxmlnamespace is isfalse isnot istrue like mod nameof new not or orelse trycast typeof xor cbool cbyte cchar cdate cdbl cdec cint clng cobj csbyte cshort csng cstr cuint culng cushort", type:"boolean byte char date decimal double integer long object sbyte short single string uinteger ulong ushort", literal:"true false nothing"}, illegal:"//|\\{|\\}|endif|gosub|variant|wend|^\\$ ", contains:[{className:"string", begin:/"(""|[^/n])"C\b/}, 
    {className:"string", begin:/"/, end:/"/, illegal:/\n/, contains:[{begin:/""/}]}, b, {className:"number", relevance:0, variants:[{begin:/\b\d[\d_]*((\.[\d_]+(E[+-]?[\d_]+)?)|(E[+-]?[\d_]+))[RFD@!#]?/}, {begin:/\b\d[\d_]*((U?[SIL])|[%&])?/}, {begin:/&H[\dA-F_]+((U?[SIL])|[%&])?/}, {begin:/&O[0-7_]+((U?[SIL])|[%&])?/}, {begin:/&B[01_]+((U?[SIL])|[%&])?/}]}, {className:"label", begin:/^\w+:/}, h, f, {className:"meta", begin:/[\t ]*#(const|disable|else|elseif|enable|end|externalsource|if|region)\b/, 
    end:/$/, keywords:{"meta-keyword":"const disable else elseif enable end externalsource if region then"}, contains:[f]}]};
  };
}());
hljs.registerLanguage("x86asm", function() {
  return function(c) {
    return {name:"Intel x86 Assembly", case_insensitive:!0, keywords:{$pattern:"[.%]?" + c.IDENT_RE, keyword:"lock rep repe repz repne repnz xaquire xrelease bnd nobnd aaa aad aam aas adc add and arpl bb0_reset bb1_reset bound bsf bsr bswap bt btc btr bts call cbw cdq cdqe clc cld cli clts cmc cmp cmpsb cmpsd cmpsq cmpsw cmpxchg cmpxchg486 cmpxchg8b cmpxchg16b cpuid cpu_read cpu_write cqo cwd cwde daa das dec div dmint emms enter equ f2xm1 fabs fadd faddp fbld fbstp fchs fclex fcmovb fcmovbe fcmove fcmovnb fcmovnbe fcmovne fcmovnu fcmovu fcom fcomi fcomip fcomp fcompp fcos fdecstp fdisi fdiv fdivp fdivr fdivrp femms feni ffree ffreep fiadd ficom ficomp fidiv fidivr fild fimul fincstp finit fist fistp fisttp fisub fisubr fld fld1 fldcw fldenv fldl2e fldl2t fldlg2 fldln2 fldpi fldz fmul fmulp fnclex fndisi fneni fninit fnop fnsave fnstcw fnstenv fnstsw fpatan fprem fprem1 fptan frndint frstor fsave fscale fsetpm fsin fsincos fsqrt fst fstcw fstenv fstp fstsw fsub fsubp fsubr fsubrp ftst fucom fucomi fucomip fucomp fucompp fxam fxch fxtract fyl2x fyl2xp1 hlt ibts icebp idiv imul in inc incbin insb insd insw int int01 int1 int03 int3 into invd invpcid invlpg invlpga iret iretd iretq iretw jcxz jecxz jrcxz jmp jmpe lahf lar lds lea leave les lfence lfs lgdt lgs lidt lldt lmsw loadall loadall286 lodsb lodsd lodsq lodsw loop loope loopne loopnz loopz lsl lss ltr mfence monitor mov movd movq movsb movsd movsq movsw movsx movsxd movzx mul mwait neg nop not or out outsb outsd outsw packssdw packsswb packuswb paddb paddd paddsb paddsiw paddsw paddusb paddusw paddw pand pandn pause paveb pavgusb pcmpeqb pcmpeqd pcmpeqw pcmpgtb pcmpgtd pcmpgtw pdistib pf2id pfacc pfadd pfcmpeq pfcmpge pfcmpgt pfmax pfmin pfmul pfrcp pfrcpit1 pfrcpit2 pfrsqit1 pfrsqrt pfsub pfsubr pi2fd pmachriw pmaddwd pmagw pmulhriw pmulhrwa pmulhrwc pmulhw pmullw pmvgezb pmvlzb pmvnzb pmvzb pop popa popad popaw popf popfd popfq popfw por prefetch prefetchw pslld psllq psllw psrad psraw psrld psrlq psrlw psubb psubd psubsb psubsiw psubsw psubusb psubusw psubw punpckhbw punpckhdq punpckhwd punpcklbw punpckldq punpcklwd push pusha pushad pushaw pushf pushfd pushfq pushfw pxor rcl rcr rdshr rdmsr rdpmc rdtsc rdtscp ret retf retn rol ror rdm rsdc rsldt rsm rsts sahf sal salc sar sbb scasb scasd scasq scasw sfence sgdt shl shld shr shrd sidt sldt skinit smi smint smintold smsw stc std sti stosb stosd stosq stosw str sub svdc svldt svts swapgs syscall sysenter sysexit sysret test ud0 ud1 ud2b ud2 ud2a umov verr verw fwait wbinvd wrshr wrmsr xadd xbts xchg xlatb xlat xor cmove cmovz cmovne cmovnz cmova cmovnbe cmovae cmovnb cmovb cmovnae cmovbe cmovna cmovg cmovnle cmovge cmovnl cmovl cmovnge cmovle cmovng cmovc cmovnc cmovo cmovno cmovs cmovns cmovp cmovpe cmovnp cmovpo je jz jne jnz ja jnbe jae jnb jb jnae jbe jna jg jnle jge jnl jl jnge jle jng jc jnc jo jno js jns jpo jnp jpe jp sete setz setne setnz seta setnbe setae setnb setnc setb setnae setcset setbe setna setg setnle setge setnl setl setnge setle setng sets setns seto setno setpe setp setpo setnp addps addss andnps andps cmpeqps cmpeqss cmpleps cmpless cmpltps cmpltss cmpneqps cmpneqss cmpnleps cmpnless cmpnltps cmpnltss cmpordps cmpordss cmpunordps cmpunordss cmpps cmpss comiss cvtpi2ps cvtps2pi cvtsi2ss cvtss2si cvttps2pi cvttss2si divps divss ldmxcsr maxps maxss minps minss movaps movhps movlhps movlps movhlps movmskps movntps movss movups mulps mulss orps rcpps rcpss rsqrtps rsqrtss shufps sqrtps sqrtss stmxcsr subps subss ucomiss unpckhps unpcklps xorps fxrstor fxrstor64 fxsave fxsave64 xgetbv xsetbv xsave xsave64 xsaveopt xsaveopt64 xrstor xrstor64 prefetchnta prefetcht0 prefetcht1 prefetcht2 maskmovq movntq pavgb pavgw pextrw pinsrw pmaxsw pmaxub pminsw pminub pmovmskb pmulhuw psadbw pshufw pf2iw pfnacc pfpnacc pi2fw pswapd maskmovdqu clflush movntdq movnti movntpd movdqa movdqu movdq2q movq2dq paddq pmuludq pshufd pshufhw pshuflw pslldq psrldq psubq punpckhqdq punpcklqdq addpd addsd andnpd andpd cmpeqpd cmpeqsd cmplepd cmplesd cmpltpd cmpltsd cmpneqpd cmpneqsd cmpnlepd cmpnlesd cmpnltpd cmpnltsd cmpordpd cmpordsd cmpunordpd cmpunordsd cmppd comisd cvtdq2pd cvtdq2ps cvtpd2dq cvtpd2pi cvtpd2ps cvtpi2pd cvtps2dq cvtps2pd cvtsd2si cvtsd2ss cvtsi2sd cvtss2sd cvttpd2pi cvttpd2dq cvttps2dq cvttsd2si divpd divsd maxpd maxsd minpd minsd movapd movhpd movlpd movmskpd movupd mulpd mulsd orpd shufpd sqrtpd sqrtsd subpd subsd ucomisd unpckhpd unpcklpd xorpd addsubpd addsubps haddpd haddps hsubpd hsubps lddqu movddup movshdup movsldup clgi stgi vmcall vmclear vmfunc vmlaunch vmload vmmcall vmptrld vmptrst vmread vmresume vmrun vmsave vmwrite vmxoff vmxon invept invvpid pabsb pabsw pabsd palignr phaddw phaddd phaddsw phsubw phsubd phsubsw pmaddubsw pmulhrsw pshufb psignb psignw psignd extrq insertq movntsd movntss lzcnt blendpd blendps blendvpd blendvps dppd dpps extractps insertps movntdqa mpsadbw packusdw pblendvb pblendw pcmpeqq pextrb pextrd pextrq phminposuw pinsrb pinsrd pinsrq pmaxsb pmaxsd pmaxud pmaxuw pminsb pminsd pminud pminuw pmovsxbw pmovsxbd pmovsxbq pmovsxwd pmovsxwq pmovsxdq pmovzxbw pmovzxbd pmovzxbq pmovzxwd pmovzxwq pmovzxdq pmuldq pmulld ptest roundpd roundps roundsd roundss crc32 pcmpestri pcmpestrm pcmpistri pcmpistrm pcmpgtq popcnt getsec pfrcpv pfrsqrtv movbe aesenc aesenclast aesdec aesdeclast aesimc aeskeygenassist vaesenc vaesenclast vaesdec vaesdeclast vaesimc vaeskeygenassist vaddpd vaddps vaddsd vaddss vaddsubpd vaddsubps vandpd vandps vandnpd vandnps vblendpd vblendps vblendvpd vblendvps vbroadcastss vbroadcastsd vbroadcastf128 vcmpeq_ospd vcmpeqpd vcmplt_ospd vcmpltpd vcmple_ospd vcmplepd vcmpunord_qpd vcmpunordpd vcmpneq_uqpd vcmpneqpd vcmpnlt_uspd vcmpnltpd vcmpnle_uspd vcmpnlepd vcmpord_qpd vcmpordpd vcmpeq_uqpd vcmpnge_uspd vcmpngepd vcmpngt_uspd vcmpngtpd vcmpfalse_oqpd vcmpfalsepd vcmpneq_oqpd vcmpge_ospd vcmpgepd vcmpgt_ospd vcmpgtpd vcmptrue_uqpd vcmptruepd vcmplt_oqpd vcmple_oqpd vcmpunord_spd vcmpneq_uspd vcmpnlt_uqpd vcmpnle_uqpd vcmpord_spd vcmpeq_uspd vcmpnge_uqpd vcmpngt_uqpd vcmpfalse_ospd vcmpneq_ospd vcmpge_oqpd vcmpgt_oqpd vcmptrue_uspd vcmppd vcmpeq_osps vcmpeqps vcmplt_osps vcmpltps vcmple_osps vcmpleps vcmpunord_qps vcmpunordps vcmpneq_uqps vcmpneqps vcmpnlt_usps vcmpnltps vcmpnle_usps vcmpnleps vcmpord_qps vcmpordps vcmpeq_uqps vcmpnge_usps vcmpngeps vcmpngt_usps vcmpngtps vcmpfalse_oqps vcmpfalseps vcmpneq_oqps vcmpge_osps vcmpgeps vcmpgt_osps vcmpgtps vcmptrue_uqps vcmptrueps vcmplt_oqps vcmple_oqps vcmpunord_sps vcmpneq_usps vcmpnlt_uqps vcmpnle_uqps vcmpord_sps vcmpeq_usps vcmpnge_uqps vcmpngt_uqps vcmpfalse_osps vcmpneq_osps vcmpge_oqps vcmpgt_oqps vcmptrue_usps vcmpps vcmpeq_ossd vcmpeqsd vcmplt_ossd vcmpltsd vcmple_ossd vcmplesd vcmpunord_qsd vcmpunordsd vcmpneq_uqsd vcmpneqsd vcmpnlt_ussd vcmpnltsd vcmpnle_ussd vcmpnlesd vcmpord_qsd vcmpordsd vcmpeq_uqsd vcmpnge_ussd vcmpngesd vcmpngt_ussd vcmpngtsd vcmpfalse_oqsd vcmpfalsesd vcmpneq_oqsd vcmpge_ossd vcmpgesd vcmpgt_ossd vcmpgtsd vcmptrue_uqsd vcmptruesd vcmplt_oqsd vcmple_oqsd vcmpunord_ssd vcmpneq_ussd vcmpnlt_uqsd vcmpnle_uqsd vcmpord_ssd vcmpeq_ussd vcmpnge_uqsd vcmpngt_uqsd vcmpfalse_ossd vcmpneq_ossd vcmpge_oqsd vcmpgt_oqsd vcmptrue_ussd vcmpsd vcmpeq_osss vcmpeqss vcmplt_osss vcmpltss vcmple_osss vcmpless vcmpunord_qss vcmpunordss vcmpneq_uqss vcmpneqss vcmpnlt_usss vcmpnltss vcmpnle_usss vcmpnless vcmpord_qss vcmpordss vcmpeq_uqss vcmpnge_usss vcmpngess vcmpngt_usss vcmpngtss vcmpfalse_oqss vcmpfalsess vcmpneq_oqss vcmpge_osss vcmpgess vcmpgt_osss vcmpgtss vcmptrue_uqss vcmptruess vcmplt_oqss vcmple_oqss vcmpunord_sss vcmpneq_usss vcmpnlt_uqss vcmpnle_uqss vcmpord_sss vcmpeq_usss vcmpnge_uqss vcmpngt_uqss vcmpfalse_osss vcmpneq_osss vcmpge_oqss vcmpgt_oqss vcmptrue_usss vcmpss vcomisd vcomiss vcvtdq2pd vcvtdq2ps vcvtpd2dq vcvtpd2ps vcvtps2dq vcvtps2pd vcvtsd2si vcvtsd2ss vcvtsi2sd vcvtsi2ss vcvtss2sd vcvtss2si vcvttpd2dq vcvttps2dq vcvttsd2si vcvttss2si vdivpd vdivps vdivsd vdivss vdppd vdpps vextractf128 vextractps vhaddpd vhaddps vhsubpd vhsubps vinsertf128 vinsertps vlddqu vldqqu vldmxcsr vmaskmovdqu vmaskmovps vmaskmovpd vmaxpd vmaxps vmaxsd vmaxss vminpd vminps vminsd vminss vmovapd vmovaps vmovd vmovq vmovddup vmovdqa vmovqqa vmovdqu vmovqqu vmovhlps vmovhpd vmovhps vmovlhps vmovlpd vmovlps vmovmskpd vmovmskps vmovntdq vmovntqq vmovntdqa vmovntpd vmovntps vmovsd vmovshdup vmovsldup vmovss vmovupd vmovups vmpsadbw vmulpd vmulps vmulsd vmulss vorpd vorps vpabsb vpabsw vpabsd vpacksswb vpackssdw vpackuswb vpackusdw vpaddb vpaddw vpaddd vpaddq vpaddsb vpaddsw vpaddusb vpaddusw vpalignr vpand vpandn vpavgb vpavgw vpblendvb vpblendw vpcmpestri vpcmpestrm vpcmpistri vpcmpistrm vpcmpeqb vpcmpeqw vpcmpeqd vpcmpeqq vpcmpgtb vpcmpgtw vpcmpgtd vpcmpgtq vpermilpd vpermilps vperm2f128 vpextrb vpextrw vpextrd vpextrq vphaddw vphaddd vphaddsw vphminposuw vphsubw vphsubd vphsubsw vpinsrb vpinsrw vpinsrd vpinsrq vpmaddwd vpmaddubsw vpmaxsb vpmaxsw vpmaxsd vpmaxub vpmaxuw vpmaxud vpminsb vpminsw vpminsd vpminub vpminuw vpminud vpmovmskb vpmovsxbw vpmovsxbd vpmovsxbq vpmovsxwd vpmovsxwq vpmovsxdq vpmovzxbw vpmovzxbd vpmovzxbq vpmovzxwd vpmovzxwq vpmovzxdq vpmulhuw vpmulhrsw vpmulhw vpmullw vpmulld vpmuludq vpmuldq vpor vpsadbw vpshufb vpshufd vpshufhw vpshuflw vpsignb vpsignw vpsignd vpslldq vpsrldq vpsllw vpslld vpsllq vpsraw vpsrad vpsrlw vpsrld vpsrlq vptest vpsubb vpsubw vpsubd vpsubq vpsubsb vpsubsw vpsubusb vpsubusw vpunpckhbw vpunpckhwd vpunpckhdq vpunpckhqdq vpunpcklbw vpunpcklwd vpunpckldq vpunpcklqdq vpxor vrcpps vrcpss vrsqrtps vrsqrtss vroundpd vroundps vroundsd vroundss vshufpd vshufps vsqrtpd vsqrtps vsqrtsd vsqrtss vstmxcsr vsubpd vsubps vsubsd vsubss vtestps vtestpd vucomisd vucomiss vunpckhpd vunpckhps vunpcklpd vunpcklps vxorpd vxorps vzeroall vzeroupper pclmullqlqdq pclmulhqlqdq pclmullqhqdq pclmulhqhqdq pclmulqdq vpclmullqlqdq vpclmulhqlqdq vpclmullqhqdq vpclmulhqhqdq vpclmulqdq vfmadd132ps vfmadd132pd vfmadd312ps vfmadd312pd vfmadd213ps vfmadd213pd vfmadd123ps vfmadd123pd vfmadd231ps vfmadd231pd vfmadd321ps vfmadd321pd vfmaddsub132ps vfmaddsub132pd vfmaddsub312ps vfmaddsub312pd vfmaddsub213ps vfmaddsub213pd vfmaddsub123ps vfmaddsub123pd vfmaddsub231ps vfmaddsub231pd vfmaddsub321ps vfmaddsub321pd vfmsub132ps vfmsub132pd vfmsub312ps vfmsub312pd vfmsub213ps vfmsub213pd vfmsub123ps vfmsub123pd vfmsub231ps vfmsub231pd vfmsub321ps vfmsub321pd vfmsubadd132ps vfmsubadd132pd vfmsubadd312ps vfmsubadd312pd vfmsubadd213ps vfmsubadd213pd vfmsubadd123ps vfmsubadd123pd vfmsubadd231ps vfmsubadd231pd vfmsubadd321ps vfmsubadd321pd vfnmadd132ps vfnmadd132pd vfnmadd312ps vfnmadd312pd vfnmadd213ps vfnmadd213pd vfnmadd123ps vfnmadd123pd vfnmadd231ps vfnmadd231pd vfnmadd321ps vfnmadd321pd vfnmsub132ps vfnmsub132pd vfnmsub312ps vfnmsub312pd vfnmsub213ps vfnmsub213pd vfnmsub123ps vfnmsub123pd vfnmsub231ps vfnmsub231pd vfnmsub321ps vfnmsub321pd vfmadd132ss vfmadd132sd vfmadd312ss vfmadd312sd vfmadd213ss vfmadd213sd vfmadd123ss vfmadd123sd vfmadd231ss vfmadd231sd vfmadd321ss vfmadd321sd vfmsub132ss vfmsub132sd vfmsub312ss vfmsub312sd vfmsub213ss vfmsub213sd vfmsub123ss vfmsub123sd vfmsub231ss vfmsub231sd vfmsub321ss vfmsub321sd vfnmadd132ss vfnmadd132sd vfnmadd312ss vfnmadd312sd vfnmadd213ss vfnmadd213sd vfnmadd123ss vfnmadd123sd vfnmadd231ss vfnmadd231sd vfnmadd321ss vfnmadd321sd vfnmsub132ss vfnmsub132sd vfnmsub312ss vfnmsub312sd vfnmsub213ss vfnmsub213sd vfnmsub123ss vfnmsub123sd vfnmsub231ss vfnmsub231sd vfnmsub321ss vfnmsub321sd rdfsbase rdgsbase rdrand wrfsbase wrgsbase vcvtph2ps vcvtps2ph adcx adox rdseed clac stac xstore xcryptecb xcryptcbc xcryptctr xcryptcfb xcryptofb montmul xsha1 xsha256 llwpcb slwpcb lwpval lwpins vfmaddpd vfmaddps vfmaddsd vfmaddss vfmaddsubpd vfmaddsubps vfmsubaddpd vfmsubaddps vfmsubpd vfmsubps vfmsubsd vfmsubss vfnmaddpd vfnmaddps vfnmaddsd vfnmaddss vfnmsubpd vfnmsubps vfnmsubsd vfnmsubss vfrczpd vfrczps vfrczsd vfrczss vpcmov vpcomb vpcomd vpcomq vpcomub vpcomud vpcomuq vpcomuw vpcomw vphaddbd vphaddbq vphaddbw vphadddq vphaddubd vphaddubq vphaddubw vphaddudq vphadduwd vphadduwq vphaddwd vphaddwq vphsubbw vphsubdq vphsubwd vpmacsdd vpmacsdqh vpmacsdql vpmacssdd vpmacssdqh vpmacssdql vpmacsswd vpmacssww vpmacswd vpmacsww vpmadcsswd vpmadcswd vpperm vprotb vprotd vprotq vprotw vpshab vpshad vpshaq vpshaw vpshlb vpshld vpshlq vpshlw vbroadcasti128 vpblendd vpbroadcastb vpbroadcastw vpbroadcastd vpbroadcastq vpermd vpermpd vpermps vpermq vperm2i128 vextracti128 vinserti128 vpmaskmovd vpmaskmovq vpsllvd vpsllvq vpsravd vpsrlvd vpsrlvq vgatherdpd vgatherqpd vgatherdps vgatherqps vpgatherdd vpgatherqd vpgatherdq vpgatherqq xabort xbegin xend xtest andn bextr blci blcic blsi blsic blcfill blsfill blcmsk blsmsk blsr blcs bzhi mulx pdep pext rorx sarx shlx shrx tzcnt tzmsk t1mskc valignd valignq vblendmpd vblendmps vbroadcastf32x4 vbroadcastf64x4 vbroadcasti32x4 vbroadcasti64x4 vcompresspd vcompressps vcvtpd2udq vcvtps2udq vcvtsd2usi vcvtss2usi vcvttpd2udq vcvttps2udq vcvttsd2usi vcvttss2usi vcvtudq2pd vcvtudq2ps vcvtusi2sd vcvtusi2ss vexpandpd vexpandps vextractf32x4 vextractf64x4 vextracti32x4 vextracti64x4 vfixupimmpd vfixupimmps vfixupimmsd vfixupimmss vgetexppd vgetexpps vgetexpsd vgetexpss vgetmantpd vgetmantps vgetmantsd vgetmantss vinsertf32x4 vinsertf64x4 vinserti32x4 vinserti64x4 vmovdqa32 vmovdqa64 vmovdqu32 vmovdqu64 vpabsq vpandd vpandnd vpandnq vpandq vpblendmd vpblendmq vpcmpltd vpcmpled vpcmpneqd vpcmpnltd vpcmpnled vpcmpd vpcmpltq vpcmpleq vpcmpneqq vpcmpnltq vpcmpnleq vpcmpq vpcmpequd vpcmpltud vpcmpleud vpcmpnequd vpcmpnltud vpcmpnleud vpcmpud vpcmpequq vpcmpltuq vpcmpleuq vpcmpnequq vpcmpnltuq vpcmpnleuq vpcmpuq vpcompressd vpcompressq vpermi2d vpermi2pd vpermi2ps vpermi2q vpermt2d vpermt2pd vpermt2ps vpermt2q vpexpandd vpexpandq vpmaxsq vpmaxuq vpminsq vpminuq vpmovdb vpmovdw vpmovqb vpmovqd vpmovqw vpmovsdb vpmovsdw vpmovsqb vpmovsqd vpmovsqw vpmovusdb vpmovusdw vpmovusqb vpmovusqd vpmovusqw vpord vporq vprold vprolq vprolvd vprolvq vprord vprorq vprorvd vprorvq vpscatterdd vpscatterdq vpscatterqd vpscatterqq vpsraq vpsravq vpternlogd vpternlogq vptestmd vptestmq vptestnmd vptestnmq vpxord vpxorq vrcp14pd vrcp14ps vrcp14sd vrcp14ss vrndscalepd vrndscaleps vrndscalesd vrndscaless vrsqrt14pd vrsqrt14ps vrsqrt14sd vrsqrt14ss vscalefpd vscalefps vscalefsd vscalefss vscatterdpd vscatterdps vscatterqpd vscatterqps vshuff32x4 vshuff64x2 vshufi32x4 vshufi64x2 kandnw kandw kmovw knotw kortestw korw kshiftlw kshiftrw kunpckbw kxnorw kxorw vpbroadcastmb2q vpbroadcastmw2d vpconflictd vpconflictq vplzcntd vplzcntq vexp2pd vexp2ps vrcp28pd vrcp28ps vrcp28sd vrcp28ss vrsqrt28pd vrsqrt28ps vrsqrt28sd vrsqrt28ss vgatherpf0dpd vgatherpf0dps vgatherpf0qpd vgatherpf0qps vgatherpf1dpd vgatherpf1dps vgatherpf1qpd vgatherpf1qps vscatterpf0dpd vscatterpf0dps vscatterpf0qpd vscatterpf0qps vscatterpf1dpd vscatterpf1dps vscatterpf1qpd vscatterpf1qps prefetchwt1 bndmk bndcl bndcu bndcn bndmov bndldx bndstx sha1rnds4 sha1nexte sha1msg1 sha1msg2 sha256rnds2 sha256msg1 sha256msg2 hint_nop0 hint_nop1 hint_nop2 hint_nop3 hint_nop4 hint_nop5 hint_nop6 hint_nop7 hint_nop8 hint_nop9 hint_nop10 hint_nop11 hint_nop12 hint_nop13 hint_nop14 hint_nop15 hint_nop16 hint_nop17 hint_nop18 hint_nop19 hint_nop20 hint_nop21 hint_nop22 hint_nop23 hint_nop24 hint_nop25 hint_nop26 hint_nop27 hint_nop28 hint_nop29 hint_nop30 hint_nop31 hint_nop32 hint_nop33 hint_nop34 hint_nop35 hint_nop36 hint_nop37 hint_nop38 hint_nop39 hint_nop40 hint_nop41 hint_nop42 hint_nop43 hint_nop44 hint_nop45 hint_nop46 hint_nop47 hint_nop48 hint_nop49 hint_nop50 hint_nop51 hint_nop52 hint_nop53 hint_nop54 hint_nop55 hint_nop56 hint_nop57 hint_nop58 hint_nop59 hint_nop60 hint_nop61 hint_nop62 hint_nop63", 
    built_in:"ip eip rip al ah bl bh cl ch dl dh sil dil bpl spl r8b r9b r10b r11b r12b r13b r14b r15b ax bx cx dx si di bp sp r8w r9w r10w r11w r12w r13w r14w r15w eax ebx ecx edx esi edi ebp esp eip r8d r9d r10d r11d r12d r13d r14d r15d rax rbx rcx rdx rsi rdi rbp rsp r8 r9 r10 r11 r12 r13 r14 r15 cs ds es fs gs ss st st0 st1 st2 st3 st4 st5 st6 st7 mm0 mm1 mm2 mm3 mm4 mm5 mm6 mm7 xmm0  xmm1  xmm2  xmm3  xmm4  xmm5  xmm6  xmm7  xmm8  xmm9 xmm10  xmm11 xmm12 xmm13 xmm14 xmm15 xmm16 xmm17 xmm18 xmm19 xmm20 xmm21 xmm22 xmm23 xmm24 xmm25 xmm26 xmm27 xmm28 xmm29 xmm30 xmm31 ymm0  ymm1  ymm2  ymm3  ymm4  ymm5  ymm6  ymm7  ymm8  ymm9 ymm10  ymm11 ymm12 ymm13 ymm14 ymm15 ymm16 ymm17 ymm18 ymm19 ymm20 ymm21 ymm22 ymm23 ymm24 ymm25 ymm26 ymm27 ymm28 ymm29 ymm30 ymm31 zmm0  zmm1  zmm2  zmm3  zmm4  zmm5  zmm6  zmm7  zmm8  zmm9 zmm10  zmm11 zmm12 zmm13 zmm14 zmm15 zmm16 zmm17 zmm18 zmm19 zmm20 zmm21 zmm22 zmm23 zmm24 zmm25 zmm26 zmm27 zmm28 zmm29 zmm30 zmm31 k0 k1 k2 k3 k4 k5 k6 k7 bnd0 bnd1 bnd2 bnd3 cr0 cr1 cr2 cr3 cr4 cr8 dr0 dr1 dr2 dr3 dr8 tr3 tr4 tr5 tr6 tr7 r0 r1 r2 r3 r4 r5 r6 r7 r0b r1b r2b r3b r4b r5b r6b r7b r0w r1w r2w r3w r4w r5w r6w r7w r0d r1d r2d r3d r4d r5d r6d r7d r0h r1h r2h r3h r0l r1l r2l r3l r4l r5l r6l r7l r8l r9l r10l r11l r12l r13l r14l r15l db dw dd dq dt ddq do dy dz resb resw resd resq rest resdq reso resy resz incbin equ times byte word dword qword nosplit rel abs seg wrt strict near far a32 ptr", 
    meta:"%define %xdefine %+ %undef %defstr %deftok %assign %strcat %strlen %substr %rotate %elif %else %endif %if %ifmacro %ifctx %ifidn %ifidni %ifid %ifnum %ifstr %iftoken %ifempty %ifenv %error %warning %fatal %rep %endrep %include %push %pop %repl %pathsearch %depend %use %arg %stacksize %local %line %comment %endcomment .nolist __FILE__ __LINE__ __SECT__  __BITS__ __OUTPUT_FORMAT__ __DATE__ __TIME__ __DATE_NUM__ __TIME_NUM__ __UTC_DATE__ __UTC_TIME__ __UTC_DATE_NUM__ __UTC_TIME_NUM__  __PASS__ struc endstruc istruc at iend align alignb sectalign daz nodaz up down zero default option assume public bits use16 use32 use64 default section segment absolute extern global common cpu float __utf16__ __utf16le__ __utf16be__ __utf32__ __utf32le__ __utf32be__ __float8__ __float16__ __float32__ __float64__ __float80m__ __float80e__ __float128l__ __float128h__ __Infinity__ __QNaN__ __SNaN__ Inf NaN QNaN SNaN float8 float16 float32 float64 float80m float80e float128l float128h __FLOAT_DAZ__ __FLOAT_ROUND__ __FLOAT__"}, 
    contains:[c.COMMENT(";", "$", {relevance:0}), {className:"number", variants:[{begin:"\\b(?:([0-9][0-9_]*)?\\.[0-9_]*(?:[eE][+-]?[0-9_]+)?|(0[Xx])?[0-9][0-9_]*(\\.[0-9_]*)?(?:[pP](?:[+-]?[0-9_]+)?)?)\\b", relevance:0}, {begin:"\\$[0-9][0-9A-Fa-f]*", relevance:0}, {begin:"\\b(?:[0-9A-Fa-f][0-9A-Fa-f_]*[Hh]|[0-9][0-9_]*[DdTt]?|[0-7][0-7_]*[QqOo]|[0-1][0-1_]*[BbYy])\\b"}, {begin:"\\b(?:0[Xx][0-9A-Fa-f_]+|0[DdTt][0-9_]+|0[QqOo][0-7_]+|0[BbYy][0-1_]+)\\b"}]}, c.QUOTE_STRING_MODE, {className:"string", 
    variants:[{begin:"'", end:"[^\\\\]'"}, {begin:"`", end:"[^\\\\]`"}], relevance:0}, {className:"symbol", variants:[{begin:"^\\s*[A-Za-z._?][A-Za-z0-9_$#@~.?]*(:|\\s+label)"}, {begin:"^\\s*%%[A-Za-z0-9_$#@~.?]*:"}], relevance:0}, {className:"subst", begin:"%[0-9]+", relevance:0}, {className:"subst", begin:"%!S+", relevance:0}, {className:"meta", begin:/^\s*\.[\w_-]+/}]};
  };
}());
hljs.registerLanguage("kotlin", function() {
  var c = {className:"number", variants:[{begin:"(\\b([0-9](_*[0-9])*)((\\.([0-9](_*[0-9])*))|\\.)?|(\\.([0-9](_*[0-9])*)))[eE][+-]?([0-9](_*[0-9])*)[fFdD]?\\b"}, {begin:"\\b([0-9](_*[0-9])*)((\\.([0-9](_*[0-9])*))[fFdD]?\\b|\\.([fFdD]\\b)?)"}, {begin:"(\\.([0-9](_*[0-9])*))[fFdD]?\\b"}, {begin:"\\b([0-9](_*[0-9])*)[fFdD]\\b"}, {begin:"\\b0[xX](([0-9a-fA-F](_*[0-9a-fA-F])*)\\.?|([0-9a-fA-F](_*[0-9a-fA-F])*)?\\.([0-9a-fA-F](_*[0-9a-fA-F])*))[pP][+-]?([0-9](_*[0-9])*)[fFdD]?\\b"}, {begin:"\\b(0|[1-9](_*[0-9])*)[lL]?\\b"}, 
  {begin:"\\b0[xX]([0-9a-fA-F](_*[0-9a-fA-F])*)[lL]?\\b"}, {begin:"\\b0(_*[0-7])*[lL]?\\b"}, {begin:"\\b0[bB][01](_*[01])*[lL]?\\b"}], relevance:0};
  return function(d) {
    var f = {keyword:"abstract as val var vararg get set class object open private protected public noinline crossinline dynamic final enum if else do while for when throw try catch finally import package is in fun override companion reified inline lateinit init interface annotation data sealed internal infix operator out by constructor super tailrec where const inner suspend typealias external expect actual", built_in:"Byte Short Char Int Long Boolean Float Double Void Unit Nothing", literal:"true false null"}, 
    b = {className:"symbol", begin:d.UNDERSCORE_IDENT_RE + "@"}, h = {className:"subst", begin:/\$\{/, end:/\}/, contains:[d.C_NUMBER_MODE]}, k = {className:"variable", begin:"\\$" + d.UNDERSCORE_IDENT_RE};
    k = {className:"string", variants:[{begin:'"""', end:'"""(?=[^"])', contains:[k, h]}, {begin:"'", end:"'", illegal:/\n/, contains:[d.BACKSLASH_ESCAPE]}, {begin:'"', end:'"', illegal:/\n/, contains:[d.BACKSLASH_ESCAPE, k, h]}]};
    h.contains.push(k);
    h = {className:"meta", begin:"@(?:file|property|field|get|set|receiver|param|setparam|delegate)\\s*:(?:\\s*" + d.UNDERSCORE_IDENT_RE + ")?"};
    var u = {className:"meta", begin:"@" + d.UNDERSCORE_IDENT_RE, contains:[{begin:/\(/, end:/\)/, contains:[d.inherit(k, {className:"meta-string"})]}]}, x = d.COMMENT("/\\*", "\\*/", {contains:[d.C_BLOCK_COMMENT_MODE]}), t = {variants:[{className:"type", begin:d.UNDERSCORE_IDENT_RE}, {begin:/\(/, end:/\)/, contains:[]}]};
    return t.variants[1].contains = [t], t.variants[1].contains = [t], {name:"Kotlin", aliases:["kt", "kts"], keywords:f, contains:[d.COMMENT("/\\*\\*", "\\*/", {relevance:0, contains:[{className:"doctag", begin:"@[A-Za-z]+"}]}), d.C_LINE_COMMENT_MODE, x, {className:"keyword", begin:/\b(break|continue|return|this)\b/, starts:{contains:[{className:"symbol", begin:/@\w+/}]}}, b, h, u, {className:"function", beginKeywords:"fun", end:"[(]|$", returnBegin:!0, excludeEnd:!0, keywords:f, relevance:5, contains:[{begin:d.UNDERSCORE_IDENT_RE + 
    "\\s*\\(", returnBegin:!0, relevance:0, contains:[d.UNDERSCORE_TITLE_MODE]}, {className:"type", begin:/</, end:/>/, keywords:"reified", relevance:0}, {className:"params", begin:/\(/, end:/\)/, endsParent:!0, keywords:f, relevance:0, contains:[{begin:/:/, end:/[=,\/]/, endsWithParent:!0, contains:[t, d.C_LINE_COMMENT_MODE, x], relevance:0}, d.C_LINE_COMMENT_MODE, x, h, u, k, d.C_NUMBER_MODE]}, x]}, {className:"class", beginKeywords:"class interface trait", end:/[:\{(]|$/, excludeEnd:!0, illegal:"extends implements", 
    contains:[{beginKeywords:"public protected internal private constructor"}, d.UNDERSCORE_TITLE_MODE, {className:"type", begin:/</, end:/>/, excludeBegin:!0, excludeEnd:!0, relevance:0}, {className:"type", begin:/[,:]\s*/, end:/[<\(,]|$/, excludeBegin:!0, returnEnd:!0}, h, u]}, k, {className:"meta", begin:"^#!/usr/bin/env", end:"$", illegal:"\n"}, c]};
  };
}());
hljs.registerLanguage("erlang", function() {
  return function(c) {
    var d = {keyword:"after and andalso|10 band begin bnot bor bsl bzr bxor case catch cond div end fun if let not of orelse|10 query receive rem try when xor", literal:"false true"}, f = c.COMMENT("%", "$"), b = {className:"number", begin:"\\b(\\d+(_\\d+)*#[a-fA-F0-9]+(_[a-fA-F0-9]+)*|\\d+(_\\d+)*(\\.\\d+(_\\d+)*)?([eE][-+]?\\d+)?)", relevance:0}, h = {begin:"fun\\s+[a-z'][a-zA-Z0-9_']*/\\d+"}, k = {begin:"([a-z'][a-zA-Z0-9_']*:[a-z'][a-zA-Z0-9_']*|[a-z'][a-zA-Z0-9_']*)\\(", end:"\\)", returnBegin:!0, 
    relevance:0, contains:[{begin:"([a-z'][a-zA-Z0-9_']*:[a-z'][a-zA-Z0-9_']*|[a-z'][a-zA-Z0-9_']*)", relevance:0}, {begin:"\\(", end:"\\)", endsWithParent:!0, returnEnd:!0, relevance:0}]}, u = {begin:/\{/, end:/\}/, relevance:0}, x = {begin:"\\b_([A-Z][A-Za-z0-9_]*)?", relevance:0}, t = {begin:"[A-Z][a-zA-Z0-9_]*", relevance:0}, r = {begin:"#" + c.UNDERSCORE_IDENT_RE, relevance:0, returnBegin:!0, contains:[{begin:"#" + c.UNDERSCORE_IDENT_RE, relevance:0}, {begin:/\{/, end:/\}/, relevance:0}]}, w = 
    {beginKeywords:"fun receive if try case", end:"end", keywords:d};
    w.contains = [f, h, c.inherit(c.APOS_STRING_MODE, {className:""}), w, k, c.QUOTE_STRING_MODE, b, u, x, t, r];
    h = [f, h, w, k, c.QUOTE_STRING_MODE, b, u, x, t, r];
    k.contains[1].contains = h;
    u.contains = h;
    r.contains[1].contains = h;
    k = {className:"params", begin:"\\(", end:"\\)", contains:h};
    return {name:"Erlang", aliases:["erl"], keywords:d, illegal:"(</|\\*=|\\+=|-=|/\\*|\\*/|\\(\\*|\\*\\))", contains:[{className:"function", begin:"^[a-z'][a-zA-Z0-9_']*\\s*\\(", end:"->", returnBegin:!0, illegal:"\\(|#|//|/\\*|\\\\|:|;", contains:[k, c.inherit(c.TITLE_MODE, {begin:"[a-z'][a-zA-Z0-9_']*"})], starts:{end:";|\\.", keywords:d, contains:h}}, f, {begin:"^-", end:"\\.", relevance:0, excludeEnd:!0, returnBegin:!0, keywords:{$pattern:"-" + c.IDENT_RE, keyword:"-module -record -undef -export -ifdef -ifndef -author -copyright -doc -vsn -import -include -include_lib -compile -define -else -endif -file -behaviour -behavior -spec".split(" ").map(function(B) {
      return B + "|1.5";
    }).join(" ")}, contains:[k]}, b, c.QUOTE_STRING_MODE, r, x, t, u, {begin:/\.$/}]};
  };
}());
hljs.registerLanguage("csharp", function() {
  return function(c) {
    var d = {keyword:"abstract as base break case class const continue do else event explicit extern finally fixed for foreach goto if implicit in interface internal is lock namespace new operator out override params private protected public readonly record ref return sealed sizeof stackalloc static struct switch this throw try typeof unchecked unsafe using virtual void volatile while".split(" ").concat("add alias and ascending async await by descending equals from get global group init into join let nameof not notnull on or orderby partial remove select set unmanaged value|0 var when where with yield".split(" ")), 
    built_in:"bool byte char decimal delegate double dynamic enum float int long nint nuint object sbyte short string ulong uint ushort".split(" "), literal:["default", "false", "null", "true"]}, f = c.inherit(c.TITLE_MODE, {begin:"[a-zA-Z](\\.?\\w)*"}), b = {className:"number", variants:[{begin:"\\b(0b[01']+)"}, {begin:"(-?)\\b([\\d']+(\\.[\\d']*)?|\\.[\\d']+)(u|U|l|L|ul|UL|f|F|b|B)"}, {begin:"(-?)(\\b0[xX][a-fA-F0-9']+|(\\b[\\d']+(\\.[\\d']*)?|\\.[\\d']+)([eE][-+]?[\\d']+)?)"}], relevance:0}, h = 
    {className:"string", begin:'@"', end:'"', contains:[{begin:'""'}]}, k = c.inherit(h, {illegal:/\n/}), u = {className:"subst", begin:/\{/, end:/\}/, keywords:d}, x = c.inherit(u, {illegal:/\n/}), t = {className:"string", begin:/\$"/, end:'"', illegal:/\n/, contains:[{begin:/\{\{/}, {begin:/\}\}/}, c.BACKSLASH_ESCAPE, x]}, r = {className:"string", begin:/\$@"/, end:'"', contains:[{begin:/\{\{/}, {begin:/\}\}/}, {begin:'""'}, u]}, w = c.inherit(r, {illegal:/\n/, contains:[{begin:/\{\{/}, {begin:/\}\}/}, 
    {begin:'""'}, x]});
    u.contains = [r, t, h, c.APOS_STRING_MODE, c.QUOTE_STRING_MODE, b, c.C_BLOCK_COMMENT_MODE];
    x.contains = [w, t, k, c.APOS_STRING_MODE, c.QUOTE_STRING_MODE, b, c.inherit(c.C_BLOCK_COMMENT_MODE, {illegal:/\n/})];
    h = {variants:[r, t, h, c.APOS_STRING_MODE, c.QUOTE_STRING_MODE]};
    k = {begin:"<", end:">", contains:[{beginKeywords:"in out"}, f]};
    u = c.IDENT_RE + "(<" + c.IDENT_RE + "(\\s*,\\s*" + c.IDENT_RE + ")*>)?(\\[\\])?";
    x = {begin:"@" + c.IDENT_RE, relevance:0};
    return {name:"C#", aliases:["cs", "c#"], keywords:d, illegal:/::/, contains:[c.COMMENT("///", "$", {returnBegin:!0, contains:[{className:"doctag", variants:[{begin:"///", relevance:0}, {begin:"\x3c!--|--\x3e"}, {begin:"</?", end:">"}]}]}), c.C_LINE_COMMENT_MODE, c.C_BLOCK_COMMENT_MODE, {className:"meta", begin:"#", end:"$", keywords:{"meta-keyword":"if else elif endif define undef warning error line region endregion pragma checksum"}}, h, b, {beginKeywords:"class interface", relevance:0, end:/[{;=]/, 
    illegal:/[^\s:,]/, contains:[{beginKeywords:"where class"}, f, k, c.C_LINE_COMMENT_MODE, c.C_BLOCK_COMMENT_MODE]}, {beginKeywords:"namespace", relevance:0, end:/[{;=]/, illegal:/[^\s:]/, contains:[f, c.C_LINE_COMMENT_MODE, c.C_BLOCK_COMMENT_MODE]}, {beginKeywords:"record", relevance:0, end:/[{;=]/, illegal:/[^\s:]/, contains:[f, k, c.C_LINE_COMMENT_MODE, c.C_BLOCK_COMMENT_MODE]}, {className:"meta", begin:"^\\s*\\[", excludeBegin:!0, end:"\\]", excludeEnd:!0, contains:[{className:"meta-string", 
    begin:/"/, end:/"/}]}, {beginKeywords:"new return throw await else", relevance:0}, {className:"function", begin:"(" + u + "\\s+)+" + c.IDENT_RE + "\\s*(<.+>\\s*)?\\(", returnBegin:!0, end:/\s*[{;=]/, excludeEnd:!0, keywords:d, contains:[{beginKeywords:"public private protected static internal protected abstract async extern override unsafe virtual new sealed partial", relevance:0}, {begin:c.IDENT_RE + "\\s*(<.+>\\s*)?\\(", returnBegin:!0, contains:[c.TITLE_MODE, k], relevance:0}, {className:"params", 
    begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:d, relevance:0, contains:[h, b, c.C_BLOCK_COMMENT_MODE]}, c.C_LINE_COMMENT_MODE, c.C_BLOCK_COMMENT_MODE]}, x]};
  };
}());
hljs.registerLanguage("lisp", function() {
  return function(c) {
    var d = {className:"literal", begin:"\\b(t{1}|nil)\\b"}, f = {className:"number", variants:[{begin:"(-|\\+)?\\d+(\\.\\d+|\\/\\d+)?((d|e|f|l|s|D|E|F|L|S)(\\+|-)?\\d+)?", relevance:0}, {begin:"#(b|B)[0-1]+(/[0-1]+)?"}, {begin:"#(o|O)[0-7]+(/[0-7]+)?"}, {begin:"#(x|X)[0-9a-fA-F]+(/[0-9a-fA-F]+)?"}, {begin:"#(c|C)\\((-|\\+)?\\d+(\\.\\d+|\\/\\d+)?((d|e|f|l|s|D|E|F|L|S)(\\+|-)?\\d+)? +(-|\\+)?\\d+(\\.\\d+|\\/\\d+)?((d|e|f|l|s|D|E|F|L|S)(\\+|-)?\\d+)?", end:"\\)"}]}, b = c.inherit(c.QUOTE_STRING_MODE, 
    {illegal:null}), h = c.COMMENT(";", "$", {relevance:0}), k = {begin:"\\*", end:"\\*"}, u = {className:"symbol", begin:"[:&][a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*"}, x = {begin:"[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*", relevance:0}, t = {contains:[f, b, k, u, {begin:"\\(", end:"\\)", contains:["self", d, b, f, x]}, x], variants:[{begin:"['`]\\(", end:"\\)"}, {begin:"\\(quote ", end:"\\)", keywords:{name:"quote"}}, {begin:"'\\|[^]*?\\|"}]}, r = {variants:[{begin:"'[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*"}, 
    {begin:"#'[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*(::[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*)*"}]}, w = {begin:"\\(\\s*", end:"\\)"}, B = {endsWithParent:!0, relevance:0};
    return w.contains = [{className:"name", variants:[{begin:"[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*", relevance:0}, {begin:"\\|[^]*?\\|"}]}, B], B.contains = [t, r, w, d, f, b, h, k, u, {begin:"\\|[^]*?\\|"}, x], {name:"Lisp", illegal:/\S/, contains:[f, c.SHEBANG(), d, b, h, t, r, w, x]};
  };
}());
hljs.registerLanguage("diff", function() {
  return function(c) {
    return {name:"Diff", aliases:["patch"], contains:[{className:"meta", relevance:10, variants:[{begin:/^@@ +-\d+,\d+ +\+\d+,\d+ +@@/}, {begin:/^\*\*\* +\d+,\d+ +\*\*\*\*$/}, {begin:/^--- +\d+,\d+ +----$/}]}, {className:"comment", variants:[{begin:/Index: /, end:/$/}, {begin:/^index/, end:/$/}, {begin:/={3,}/, end:/$/}, {begin:/^-{3}/, end:/$/}, {begin:/^\*{3} /, end:/$/}, {begin:/^\+{3}/, end:/$/}, {begin:/^\*{15}$/}, {begin:/^diff --git/, end:/$/}]}, {className:"addition", begin:/^\+/, end:/$/}, 
    {className:"deletion", begin:/^-/, end:/$/}, {className:"addition", begin:/^!/, end:/$/}]};
  };
}());
hljs.registerLanguage("rust", function() {
  return function(c) {
    return {name:"Rust", aliases:["rs"], keywords:{$pattern:c.IDENT_RE + "!?", keyword:"abstract as async await become box break const continue crate do dyn else enum extern false final fn for if impl in let loop macro match mod move mut override priv pub ref return self Self static struct super trait true try type typeof unsafe unsized use virtual where while yield", literal:"true false Some None Ok Err", built_in:"drop i8 i16 i32 i64 i128 isize u8 u16 u32 u64 u128 usize f32 f64 str char bool Box Option Result String Vec Copy Send Sized Sync Drop Fn FnMut FnOnce ToOwned Clone Debug PartialEq PartialOrd Eq Ord AsRef AsMut Into From Default Iterator Extend IntoIterator DoubleEndedIterator ExactSizeIterator SliceConcatExt ToString assert! assert_eq! bitflags! bytes! cfg! col! concat! concat_idents! debug_assert! debug_assert_eq! env! panic! file! format! format_args! include_bin! include_str! line! local_data_key! module_path! option_env! print! println! select! stringify! try! unimplemented! unreachable! vec! write! writeln! macro_rules! assert_ne! debug_assert_ne!"}, 
    illegal:"</", contains:[c.C_LINE_COMMENT_MODE, c.COMMENT("/\\*", "\\*/", {contains:["self"]}), c.inherit(c.QUOTE_STRING_MODE, {begin:/b?"/, illegal:null}), {className:"string", variants:[{begin:/r(#*)"(.|\n)*?"\1(?!#)/}, {begin:/b?'\\?(x\w{2}|u\w{4}|U\w{8}|.)'/}]}, {className:"symbol", begin:/'[a-zA-Z_][a-zA-Z0-9_]*/}, {className:"number", variants:[{begin:"\\b0b([01_]+)([ui](8|16|32|64|128|size)|f(32|64))?"}, {begin:"\\b0o([0-7_]+)([ui](8|16|32|64|128|size)|f(32|64))?"}, {begin:"\\b0x([A-Fa-f0-9_]+)([ui](8|16|32|64|128|size)|f(32|64))?"}, 
    {begin:"\\b(\\d[\\d_]*(\\.[0-9_]+)?([eE][+-]?[0-9_]+)?)([ui](8|16|32|64|128|size)|f(32|64))?"}], relevance:0}, {className:"function", beginKeywords:"fn", end:"(\\(|<)", excludeEnd:!0, contains:[c.UNDERSCORE_TITLE_MODE]}, {className:"meta", begin:"#!?\\[", end:"\\]", contains:[{className:"meta-string", begin:/"/, end:/"/}]}, {className:"class", beginKeywords:"type", end:";", contains:[c.inherit(c.UNDERSCORE_TITLE_MODE, {endsParent:!0})], illegal:"\\S"}, {className:"class", beginKeywords:"trait enum struct union", 
    end:/\{/, contains:[c.inherit(c.UNDERSCORE_TITLE_MODE, {endsParent:!0})], illegal:"[\\w\\d]"}, {begin:c.IDENT_RE + "::", keywords:{built_in:"drop i8 i16 i32 i64 i128 isize u8 u16 u32 u64 u128 usize f32 f64 str char bool Box Option Result String Vec Copy Send Sized Sync Drop Fn FnMut FnOnce ToOwned Clone Debug PartialEq PartialOrd Eq Ord AsRef AsMut Into From Default Iterator Extend IntoIterator DoubleEndedIterator ExactSizeIterator SliceConcatExt ToString assert! assert_eq! bitflags! bytes! cfg! col! concat! concat_idents! debug_assert! debug_assert_eq! env! panic! file! format! format_args! include_bin! include_str! line! local_data_key! module_path! option_env! print! println! select! stringify! try! unimplemented! unreachable! vec! write! writeln! macro_rules! assert_ne! debug_assert_ne!"}}, 
    {begin:"->"}]};
  };
}());
hljs.registerLanguage("yaml", function() {
  return function(c) {
    var d = {className:"string", relevance:0, variants:[{begin:/'/, end:/'/}, {begin:/"/, end:/"/}, {begin:/\S+/}], contains:[c.BACKSLASH_ESCAPE, {className:"template-variable", variants:[{begin:/\{\{/, end:/\}\}/}, {begin:/%\{/, end:/\}/}]}]}, f = c.inherit(d, {variants:[{begin:/'/, end:/'/}, {begin:/"/, end:/"/}, {begin:/[^\s,{}[\]]+/}]}), b = {end:",", endsWithParent:!0, excludeEnd:!0, keywords:"true false yes no null", relevance:0};
    c = [{className:"attr", variants:[{begin:"\\w[\\w :\\/.-]*:(?=[ \t]|$)"}, {begin:'"\\w[\\w :\\/.-]*":(?=[ \t]|$)'}, {begin:"'\\w[\\w :\\/.-]*':(?=[ \t]|$)"}]}, {className:"meta", begin:"^---\\s*$", relevance:10}, {className:"string", begin:"[\\|>]([1-9]?[+-])?[ ]*\\n( +)[^ ][^\\n]*\\n(\\2[^\\n]+\\n?)*"}, {begin:"<%[%=-]?", end:"[%-]?%>", subLanguage:"ruby", excludeBegin:!0, excludeEnd:!0, relevance:0}, {className:"type", begin:"!\\w+![\\w#;/?:@&=+$,.~*'()[\\]]+"}, {className:"type", begin:"!<[\\w#;/?:@&=+$,.~*'()[\\]]+>"}, 
    {className:"type", begin:"![\\w#;/?:@&=+$,.~*'()[\\]]+"}, {className:"type", begin:"!![\\w#;/?:@&=+$,.~*'()[\\]]+"}, {className:"meta", begin:"&" + c.UNDERSCORE_IDENT_RE + "$"}, {className:"meta", begin:"\\*" + c.UNDERSCORE_IDENT_RE + "$"}, {className:"bullet", begin:"-(?=[ ]|$)", relevance:0}, c.HASH_COMMENT_MODE, {beginKeywords:"true false yes no null", keywords:{literal:"true false yes no null"}}, {className:"number", begin:"\\b[0-9]{4}(-[0-9][0-9]){0,2}([Tt \\t][0-9][0-9]?(:[0-9][0-9]){2})?(\\.[0-9]*)?([ \\t])*(Z|[-+][0-9][0-9]?(:[0-9][0-9])?)?\\b"}, 
    {className:"number", begin:c.C_NUMBER_RE + "\\b", relevance:0}, {begin:/\{/, end:/\}/, contains:[b], illegal:"\\n", relevance:0}, {begin:"\\[", end:"\\]", contains:[b], illegal:"\\n", relevance:0}, d];
    d = [].concat($jscomp.arrayFromIterable(c));
    return d.pop(), d.push(f), b.contains = d, {name:"YAML", case_insensitive:!0, aliases:["yml"], contains:c};
  };
}());
hljs.registerLanguage("json", function() {
  return function(c) {
    var d = {literal:"true false null"}, f = [c.C_LINE_COMMENT_MODE, c.C_BLOCK_COMMENT_MODE], b = [c.QUOTE_STRING_MODE, c.C_NUMBER_MODE], h = {end:",", endsWithParent:!0, excludeEnd:!0, contains:b, keywords:d}, k = {begin:/\{/, end:/\}/, contains:[{className:"attr", begin:/"/, end:/"/, contains:[c.BACKSLASH_ESCAPE], illegal:"\\n"}, c.inherit(h, {begin:/:/})].concat(f), illegal:"\\S"};
    c = {begin:"\\[", end:"\\]", contains:[c.inherit(h)], illegal:"\\S"};
    return b.push(k, c), f.forEach(function(u) {
      b.push(u);
    }), {name:"JSON", contains:b, keywords:d, illegal:"\\S"};
  };
}());
hljs.registerLanguage("makefile", function() {
  return function(c) {
    var d = {className:"variable", variants:[{begin:"\\$\\(" + c.UNDERSCORE_IDENT_RE + "\\)", contains:[c.BACKSLASH_ESCAPE]}, {begin:/\$[@%<?\^\+\*]/}]};
    return {name:"Makefile", aliases:["mk", "mak", "make"], keywords:{$pattern:/[\w-]+/, keyword:"define endef undefine ifdef ifndef ifeq ifneq else endif include -include sinclude override export unexport private vpath"}, contains:[c.HASH_COMMENT_MODE, d, {className:"string", begin:/"/, end:/"/, contains:[c.BACKSLASH_ESCAPE, d]}, {className:"variable", begin:/\$\([\w-]+\s/, end:/\)/, keywords:{built_in:"subst patsubst strip findstring filter filter-out sort word wordlist firstword lastword dir notdir suffix basename addsuffix addprefix join wildcard realpath abspath error warning shell origin flavor foreach if or and call eval file value"}, 
    contains:[d]}, {begin:"^" + c.UNDERSCORE_IDENT_RE + "\\s*(?=[:+?]?=)"}, {className:"meta", begin:/^\.PHONY:/, end:/$/, keywords:{$pattern:/[\.\w]+/, "meta-keyword":".PHONY"}}, {className:"section", begin:/^[^\s]+:/, end:/$/, contains:[d]}]};
  };
}());
hljs.registerLanguage("css", function() {
  var c = "a abbr address article aside audio b blockquote body button canvas caption cite code dd del details dfn div dl dt em fieldset figcaption figure footer form h1 h2 h3 h4 h5 h6 header hgroup html i iframe img input ins kbd label legend li main mark menu nav object ol p q quote samp section span strong summary sup table tbody td textarea tfoot th thead time tr ul var video".split(" "), d = "any-hover any-pointer aspect-ratio color color-gamut color-index device-aspect-ratio device-height device-width display-mode forced-colors grid height hover inverted-colors monochrome orientation overflow-block overflow-inline pointer prefers-color-scheme prefers-contrast prefers-reduced-motion prefers-reduced-transparency resolution scan scripting update width min-width max-width min-height max-height".split(" "), 
  f = "active any-link blank checked current default defined dir disabled drop empty enabled first first-child first-of-type fullscreen future focus focus-visible focus-within has host host-context hover indeterminate in-range invalid is lang last-child last-of-type left link local-link not nth-child nth-col nth-last-child nth-last-col nth-last-of-type nth-of-type only-child only-of-type optional out-of-range past placeholder-shown read-only read-write required right root scope target target-within user-invalid valid visited where".split(" "), 
  b = "after backdrop before cue cue-region first-letter first-line grammar-error marker part placeholder selection slotted spelling-error".split(" "), h = "align-content align-items align-self animation animation-delay animation-direction animation-duration animation-fill-mode animation-iteration-count animation-name animation-play-state animation-timing-function auto backface-visibility background background-attachment background-clip background-color background-image background-origin background-position background-repeat background-size border border-bottom border-bottom-color border-bottom-left-radius border-bottom-right-radius border-bottom-style border-bottom-width border-collapse border-color border-image border-image-outset border-image-repeat border-image-slice border-image-source border-image-width border-left border-left-color border-left-style border-left-width border-radius border-right border-right-color border-right-style border-right-width border-spacing border-style border-top border-top-color border-top-left-radius border-top-right-radius border-top-style border-top-width border-width bottom box-decoration-break box-shadow box-sizing break-after break-before break-inside caption-side clear clip clip-path color column-count column-fill column-gap column-rule column-rule-color column-rule-style column-rule-width column-span column-width columns content counter-increment counter-reset cursor direction display empty-cells filter flex flex-basis flex-direction flex-flow flex-grow flex-shrink flex-wrap float font font-display font-family font-feature-settings font-kerning font-language-override font-size font-size-adjust font-smoothing font-stretch font-style font-variant font-variant-ligatures font-variation-settings font-weight height hyphens icon image-orientation image-rendering image-resolution ime-mode inherit initial justify-content left letter-spacing line-height list-style list-style-image list-style-position list-style-type margin margin-bottom margin-left margin-right margin-top marks mask max-height max-width min-height min-width nav-down nav-index nav-left nav-right nav-up none normal object-fit object-position opacity order orphans outline outline-color outline-offset outline-style outline-width overflow overflow-wrap overflow-x overflow-y padding padding-bottom padding-left padding-right padding-top page-break-after page-break-before page-break-inside perspective perspective-origin pointer-events position quotes resize right src tab-size table-layout text-align text-align-last text-decoration text-decoration-color text-decoration-line text-decoration-style text-indent text-overflow text-rendering text-shadow text-transform text-underline-position top transform transform-origin transform-style transition transition-delay transition-duration transition-property transition-timing-function unicode-bidi vertical-align visibility white-space widows width word-break word-spacing word-wrap z-index".split(" ").reverse();
  return function(k) {
    var u = [k.APOS_STRING_MODE, k.QUOTE_STRING_MODE];
    return {name:"CSS", case_insensitive:!0, illegal:/[=|'\$]/, keywords:{keyframePosition:"from to"}, classNameAliases:{keyframePosition:"selector-tag"}, contains:[k.C_BLOCK_COMMENT_MODE, {begin:/-(webkit|moz|ms|o)-(?=[a-z])/}, k.CSS_NUMBER_MODE, {className:"selector-id", begin:/#[A-Za-z0-9_-]+/, relevance:0}, {className:"selector-class", begin:"\\.[a-zA-Z-][a-zA-Z0-9_-]*", relevance:0}, {className:"selector-attr", begin:/\[/, end:/\]/, illegal:"$", contains:[k.APOS_STRING_MODE, k.QUOTE_STRING_MODE]}, 
    {className:"selector-pseudo", variants:[{begin:":(" + f.join("|") + ")"}, {begin:"::(" + b.join("|") + ")"}]}, {className:"attribute", begin:"\\b(" + h.join("|") + ")\\b"}, {begin:":", end:"[;}]", contains:[{className:"number", begin:"#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})"}, {className:"meta", begin:"!important"}, k.CSS_NUMBER_MODE].concat($jscomp.arrayFromIterable(u), [{begin:/(url|data-uri)\(/, end:/\)/, relevance:0, keywords:{built_in:"url data-uri"}, contains:[{className:"string", begin:/[^)]/, 
    endsWithParent:!0, excludeEnd:!0}]}, {className:"built_in", begin:/[\w-]+(?=\()/}])}, {begin:(x = /@/, function(t) {
      for (var r = [], w = 0; w < arguments.length; ++w) {
        r[w - 0] = arguments[w];
      }
      return r.map(function(B) {
        return B ? "string" == typeof B ? B : B.source : null;
      }).join("");
    }("(?=", x, ")")), end:"[{;]", relevance:0, illegal:/:/, contains:[{className:"keyword", begin:/@-?\w[\w]*(-\w+)*/}, {begin:/\s/, endsWithParent:!0, excludeEnd:!0, relevance:0, keywords:{$pattern:/[a-z-]+/, keyword:"and or not only", attribute:d.join(" ")}, contains:[{begin:/[a-z-]+(?=:)/, className:"attribute"}].concat($jscomp.arrayFromIterable(u), [k.CSS_NUMBER_MODE])}]}, {className:"selector-tag", begin:"\\b(" + c.join("|") + ")\\b"}]};
    var x;
  };
}());
hljs.registerLanguage("ini", function() {
  function c(d) {
    for (var f = [], b = 0; b < arguments.length; ++b) {
      f[b - 0] = arguments[b];
    }
    return f.map(function(h) {
      return h ? "string" == typeof h ? h : h.source : null;
    }).join("");
  }
  return function(d) {
    var f = {className:"number", relevance:0, variants:[{begin:/([+-]+)?[\d]+_[\d_]+/}, {begin:d.NUMBER_RE}]}, b = d.COMMENT();
    b.variants = [{begin:/;/, end:/$/}, {begin:/#/, end:/$/}];
    var h = {className:"variable", variants:[{begin:/\$[\w\d"][\w\d_]*/}, {begin:/\$\{(.*?)\}/}]}, k = {className:"literal", begin:/\bon|off|true|false|yes|no\b/};
    d = {className:"string", contains:[d.BACKSLASH_ESCAPE], variants:[{begin:"'''", end:"'''", relevance:10}, {begin:'"""', end:'"""', relevance:10}, {begin:'"', end:'"'}, {begin:"'", end:"'"}]};
    var u = {begin:/\[/, end:/\]/, contains:[b, k, h, d, f, "self"], relevance:0}, x = "(" + [/[A-Za-z0-9_-]+/, /"(\\"|[^"])*"/, /'[^']*'/].map(function(t) {
      return t ? "string" == typeof t ? t : t.source : null;
    }).join("|") + ")";
    return {name:"TOML, also INI", aliases:["toml"], case_insensitive:!0, illegal:/\S/, contains:[b, {className:"section", begin:/\[+/, end:/\]+/}, {begin:c(x, "(\\s*\\.\\s*", x, ")*", c("(?=", /\s*=\s*[^#\s]/, ")")), className:"attr", starts:{end:/$/, contains:[b, u, k, h, d, f]}}]};
  };
}());
hljs.registerLanguage("apache", function() {
  return function(c) {
    var d = {className:"number", begin:/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(:\d{1,5})?/};
    return {name:"Apache config", aliases:["apacheconf"], case_insensitive:!0, contains:[c.HASH_COMMENT_MODE, {className:"section", begin:/<\/?/, end:/>/, contains:[d, {className:"number", begin:/:\d{1,5}/}, c.inherit(c.QUOTE_STRING_MODE, {relevance:0})]}, {className:"attribute", begin:/\w+/, relevance:0, keywords:{nomarkup:"order deny allow setenv rewriterule rewriteengine rewritecond documentroot sethandler errordocument loadmodule options header listen serverroot servername"}, starts:{end:/$/, 
    relevance:0, keywords:{literal:"on off all deny allow"}, contains:[{className:"meta", begin:/\s\[/, end:/\]$/}, {className:"variable", begin:/[\$%]\{/, end:/\}/, contains:["self", {className:"number", begin:/[$%]\d+/}]}, d, {className:"number", begin:/\d+/}, c.QUOTE_STRING_MODE]}}], illegal:/\S/};
  };
}());
hljs.registerLanguage("go", function() {
  return function(c) {
    var d = {keyword:"break default func interface select case map struct chan else goto package switch const fallthrough if range type continue for import return var go defer bool byte complex64 complex128 float32 float64 int8 int16 int32 int64 string uint8 uint16 uint32 uint64 int uint uintptr rune", literal:"true false iota nil", built_in:"append cap close complex copy imag len make new panic print println real recover delete"};
    return {name:"Go", aliases:["golang"], keywords:d, illegal:"</", contains:[c.C_LINE_COMMENT_MODE, c.C_BLOCK_COMMENT_MODE, {className:"string", variants:[c.QUOTE_STRING_MODE, c.APOS_STRING_MODE, {begin:"`", end:"`"}]}, {className:"number", variants:[{begin:c.C_NUMBER_RE + "[i]", relevance:1}, c.C_NUMBER_MODE]}, {begin:/:=/}, {className:"function", beginKeywords:"func", end:"\\s*(\\{|$)", excludeEnd:!0, contains:[c.TITLE_MODE, {className:"params", begin:/\(/, end:/\)/, keywords:d, illegal:/["']/}]}]};
  };
}());
hljs.registerLanguage("perl", function() {
  function c(f) {
    for (var b = [], h = 0; h < arguments.length; ++h) {
      b[h - 0] = arguments[h];
    }
    return b.map(function(k) {
      return k ? "string" == typeof k ? k : k.source : null;
    }).join("");
  }
  function d(f) {
    for (var b = [], h = 0; h < arguments.length; ++h) {
      b[h - 0] = arguments[h];
    }
    return "(" + b.map(function(k) {
      return k ? "string" == typeof k ? k : k.source : null;
    }).join("|") + ")";
  }
  return function(f) {
    var b = /[dualxmsipngr]{0,12}/, h = {$pattern:/[\w.]+/, keyword:"abs accept alarm and atan2 bind binmode bless break caller chdir chmod chomp chop chown chr chroot close closedir connect continue cos crypt dbmclose dbmopen defined delete die do dump each else elsif endgrent endhostent endnetent endprotoent endpwent endservent eof eval exec exists exit exp fcntl fileno flock for foreach fork format formline getc getgrent getgrgid getgrnam gethostbyaddr gethostbyname gethostent getlogin getnetbyaddr getnetbyname getnetent getpeername getpgrp getpriority getprotobyname getprotobynumber getprotoent getpwent getpwnam getpwuid getservbyname getservbyport getservent getsockname getsockopt given glob gmtime goto grep gt hex if index int ioctl join keys kill last lc lcfirst length link listen local localtime log lstat lt ma map mkdir msgctl msgget msgrcv msgsnd my ne next no not oct open opendir or ord our pack package pipe pop pos print printf prototype push q|0 qq quotemeta qw qx rand read readdir readline readlink readpipe recv redo ref rename require reset return reverse rewinddir rindex rmdir say scalar seek seekdir select semctl semget semop send setgrent sethostent setnetent setpgrp setpriority setprotoent setpwent setservent setsockopt shift shmctl shmget shmread shmwrite shutdown sin sleep socket socketpair sort splice split sprintf sqrt srand stat state study sub substr symlink syscall sysopen sysread sysseek system syswrite tell telldir tie tied time times tr truncate uc ucfirst umask undef unless unlink unpack unshift untie until use utime values vec wait waitpid wantarray warn when while write x|0 xor y|0"}, 
    k = {className:"subst", begin:"[$@]\\{", end:"\\}", keywords:h}, u = {begin:/->\{/, end:/\}/}, x = {variants:[{begin:/\$\d/}, {begin:c(/[$%@](\^\w\b|#\w+(::\w+)*|\{\w+\}|\w+(::\w*)*)/, "(?![A-Za-z])(?![@$%])")}, {begin:/[$%@][^\s\w{]/, relevance:0}]}, t = [f.BACKSLASH_ESCAPE, k, x], r = [/!/, /\//, /\|/, /\?/, /'/, /"/, /#/], w = function(P, ca, R) {
      R = void 0 === R ? "\\1" : R;
      var Y = "\\1" === R ? R : c(R, ca);
      return c(c("(?:", P, ")"), ca, /(?:\\.|[^\\\/])*?/, Y, /(?:\\.|[^\\\/])*?/, R, b);
    }, B = function(P, ca, R) {
      return c(c("(?:", P, ")"), ca, /(?:\\.|[^\\\/])*?/, R, b);
    };
    f = [x, f.HASH_COMMENT_MODE, f.COMMENT(/^=\w/, /=cut/, {endsWithParent:!0}), u, {className:"string", contains:t, variants:[{begin:"q[qwxr]?\\s*\\(", end:"\\)", relevance:5}, {begin:"q[qwxr]?\\s*\\[", end:"\\]", relevance:5}, {begin:"q[qwxr]?\\s*\\{", end:"\\}", relevance:5}, {begin:"q[qwxr]?\\s*\\|", end:"\\|", relevance:5}, {begin:"q[qwxr]?\\s*<", end:">", relevance:5}, {begin:"qw\\s+q", end:"q", relevance:5}, {begin:"'", end:"'", contains:[f.BACKSLASH_ESCAPE]}, {begin:'"', end:'"'}, {begin:"`", 
    end:"`", contains:[f.BACKSLASH_ESCAPE]}, {begin:/\{\w+\}/, relevance:0}, {begin:"-?\\w+\\s*=>", relevance:0}]}, {className:"number", begin:"(\\b0[0-7_]+)|(\\b0x[0-9a-fA-F_]+)|(\\b[1-9][0-9_]*(\\.[0-9_]+)?)|[0_]\\b", relevance:0}, {begin:"(\\/\\/|" + f.RE_STARTERS_RE + "|\\b(split|return|print|reverse|grep)\\b)\\s*", keywords:"split return print reverse grep", relevance:0, contains:[f.HASH_COMMENT_MODE, {className:"regexp", variants:[{begin:w("s|tr|y", d.apply(null, $jscomp.arrayFromIterable(r)))}, 
    {begin:w("s|tr|y", "\\(", "\\)")}, {begin:w("s|tr|y", "\\[", "\\]")}, {begin:w("s|tr|y", "\\{", "\\}")}], relevance:2}, {className:"regexp", variants:[{begin:/(m|qr)\/\//, relevance:0}, {begin:B("(?:m|qr)?", /\//, /\//)}, {begin:B("m|qr", d.apply(null, $jscomp.arrayFromIterable(r)), /\1/)}, {begin:B("m|qr", /\(/, /\)/)}, {begin:B("m|qr", /\[/, /\]/)}, {begin:B("m|qr", /\{/, /\}/)}]}]}, {className:"function", beginKeywords:"sub", end:"(\\s*\\(.*?\\))?[;{]", excludeEnd:!0, relevance:5, contains:[f.TITLE_MODE]}, 
    {begin:"-\\w\\b", relevance:0}, {begin:"^__DATA__$", end:"^__END__$", subLanguage:"mojolicious", contains:[{begin:"^@@.*", end:"$", className:"comment"}]}];
    return k.contains = f, u.contains = f, {name:"Perl", aliases:["pl", "pm"], keywords:h, contains:f};
  };
}());
hljs.registerLanguage("lua", function() {
  return function(c) {
    var d = {begin:"\\[=*\\[", end:"\\]=*\\]", contains:["self"]}, f = [c.COMMENT("--(?!\\[=*\\[)", "$"), c.COMMENT("--\\[=*\\[", "\\]=*\\]", {contains:[d], relevance:10})];
    return {name:"Lua", keywords:{$pattern:c.UNDERSCORE_IDENT_RE, literal:"true false nil", keyword:"and break do else elseif end for goto if in local not or repeat return then until while", built_in:"_G _ENV _VERSION __index __newindex __mode __call __metatable __tostring __len __gc __add __sub __mul __div __mod __pow __concat __unm __eq __lt __le assert collectgarbage dofile error getfenv getmetatable ipairs load loadfile loadstring module next pairs pcall print rawequal rawget rawset require select setfenv setmetatable tonumber tostring type unpack xpcall arg self coroutine resume yield status wrap create running debug getupvalue debug sethook getmetatable gethook setmetatable setlocal traceback setfenv getinfo setupvalue getlocal getregistry getfenv io lines write close flush open output type read stderr stdin input stdout popen tmpfile math log max acos huge ldexp pi cos tanh pow deg tan cosh sinh random randomseed frexp ceil floor rad abs sqrt modf asin min mod fmod log10 atan2 exp sin atan os exit setlocale date getenv difftime remove time clock tmpname rename execute package preload loadlib loaded loaders cpath config path seeall string sub upper len gfind rep find match char dump gmatch reverse byte format gsub lower table setn insert getn foreachi maxn foreach concat sort remove"}, 
    contains:f.concat([{className:"function", beginKeywords:"function", end:"\\)", contains:[c.inherit(c.TITLE_MODE, {begin:"([_a-zA-Z]\\w*\\.)*([_a-zA-Z]\\w*:)?[_a-zA-Z]\\w*"}), {className:"params", begin:"\\(", endsWithParent:!0, contains:f}].concat(f)}, c.C_NUMBER_MODE, c.APOS_STRING_MODE, c.QUOTE_STRING_MODE, {className:"string", begin:"\\[=*\\[", end:"\\]=*\\]", contains:[d], relevance:5}])};
  };
}());
hljs.registerLanguage("cpp", function() {
  function c(d) {
    for (var f = [], b = 0; b < arguments.length; ++b) {
      f[b - 0] = arguments[b];
    }
    return f.map(function(h) {
      return (k = h) ? "string" == typeof k ? k : k.source : null;
      var k;
    }).join("");
  }
  return function(d) {
    var f = d.COMMENT("//", "$", {contains:[{begin:/\\\n/}]}), b = "(decltype\\(auto\\)|" + c("(", "[a-zA-Z_]\\w*::", ")?") + "[a-zA-Z_]\\w*" + c("(", "<[^<>]+>", ")?") + ")", h = {className:"keyword", begin:"\\b[a-z\\d_]*_t\\b"}, k = {className:"string", variants:[{begin:'(u8?|U|L)?"', end:'"', illegal:"\\n", contains:[d.BACKSLASH_ESCAPE]}, {begin:"(u8?|U|L)?'(\\\\(x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4,8}|[0-7]{3}|\\S)|.)", end:"'", illegal:"."}, d.END_SAME_AS_BEGIN({begin:/(?:u8?|U|L)?R"([^()\\ ]{0,16})\(/, 
    end:/\)([^()\\ ]{0,16})"/})]}, u = {className:"number", variants:[{begin:"\\b(0b[01']+)"}, {begin:"(-?)\\b([\\d']+(\\.[\\d']*)?|\\.[\\d']+)((ll|LL|l|L)(u|U)?|(u|U)(ll|LL|l|L)?|f|F|b|B)"}, {begin:"(-?)(\\b0[xX][a-fA-F0-9']+|(\\b[\\d']+(\\.[\\d']*)?|\\.[\\d']+)([eE][-+]?[\\d']+)?)"}], relevance:0}, x = {className:"meta", begin:/#\s*[a-z]+\b/, end:/$/, keywords:{"meta-keyword":"if else elif endif define undef warning error line pragma _Pragma ifdef ifndef include"}, contains:[{begin:/\\\n/, relevance:0}, 
    d.inherit(k, {className:"meta-string"}), {className:"meta-string", begin:/<.*?>/}, f, d.C_BLOCK_COMMENT_MODE]}, t = {className:"title", begin:c("(", "[a-zA-Z_]\\w*::", ")?") + d.IDENT_RE, relevance:0}, r = c("(", "[a-zA-Z_]\\w*::", ")?") + d.IDENT_RE + "\\s*\\(", w = {keyword:"int float while private char char8_t char16_t char32_t catch import module export virtual operator sizeof dynamic_cast|10 typedef const_cast|10 const for static_cast|10 union namespace unsigned long volatile static protected bool template mutable if public friend do goto auto void enum else break extern using asm case typeid wchar_t short reinterpret_cast|10 default double register explicit signed typename try this switch continue inline delete alignas alignof constexpr consteval constinit decltype concept co_await co_return co_yield requires noexcept static_assert thread_local restrict final override atomic_bool atomic_char atomic_schar atomic_uchar atomic_short atomic_ushort atomic_int atomic_uint atomic_long atomic_ulong atomic_llong atomic_ullong new throw return and and_eq bitand bitor compl not not_eq or or_eq xor xor_eq", 
    built_in:"_Bool _Complex _Imaginary", _relevance_hints:"asin atan2 atan calloc ceil cosh cos exit exp fabs floor fmod fprintf fputs free frexp auto_ptr deque list queue stack vector map set pair bitset multiset multimap unordered_set fscanf future isalnum isalpha iscntrl isdigit isgraph islower isprint ispunct isspace isupper isxdigit tolower toupper labs ldexp log10 log malloc realloc memchr memcmp memcpy memset modf pow printf putchar puts scanf sinh sin snprintf sprintf sqrt sscanf strcat strchr strcmp strcpy strcspn strlen strncat strncmp strncpy strpbrk strrchr strspn strstr tanh tan unordered_map unordered_multiset unordered_multimap priority_queue make_pair array shared_ptr abort terminate abs acos vfprintf vprintf vsprintf endl initializer_list unique_ptr complex imaginary std string wstring cin cout cerr clog stdin stdout stderr stringstream istringstream ostringstream".split(" "), 
    literal:"true false nullptr NULL"}, B = {className:"function.dispatch", relevance:0, keywords:w, begin:c(/\b/, /(?!decltype)/, /(?!if)/, /(?!for)/, /(?!while)/, d.IDENT_RE, (P = /\s*\(/, c("(?=", P, ")")))}, P;
    P = [B, x, h, f, d.C_BLOCK_COMMENT_MODE, u, k];
    var ca = {variants:[{begin:/=/, end:/;/}, {begin:/\(/, end:/\)/}, {beginKeywords:"new throw return else", end:/;/}], keywords:w, contains:P.concat([{begin:/\(/, end:/\)/, keywords:w, contains:P.concat(["self"]), relevance:0}]), relevance:0};
    return {name:"C++", aliases:"cc c++ h++ hpp hh hxx cxx".split(" "), keywords:w, illegal:"</", classNameAliases:{"function.dispatch":"built_in"}, contains:[].concat(ca, {className:"function", begin:"(" + b + "[\\*&\\s]+)+" + r, returnBegin:!0, end:/[{;=]/, excludeEnd:!0, keywords:w, illegal:/[^\w\s\*&:<>.]/, contains:[{begin:"decltype\\(auto\\)", keywords:w, relevance:0}, {begin:r, returnBegin:!0, contains:[t], relevance:0}, {begin:/::/, relevance:0}, {begin:/:/, endsWithParent:!0, contains:[k, 
    u]}, {className:"params", begin:/\(/, end:/\)/, keywords:w, relevance:0, contains:[f, d.C_BLOCK_COMMENT_MODE, k, u, h, {begin:/\(/, end:/\)/, keywords:w, relevance:0, contains:["self", f, d.C_BLOCK_COMMENT_MODE, k, u, h]}]}, h, f, d.C_BLOCK_COMMENT_MODE, x]}, B, P, [x, {begin:"\\b(deque|list|queue|priority_queue|pair|stack|vector|map|set|bitset|multiset|multimap|unordered_map|unordered_set|unordered_multiset|unordered_multimap|array)\\s*<", end:">", keywords:w, contains:["self", h]}, {begin:d.IDENT_RE + 
    "::", keywords:w}, {className:"class", beginKeywords:"enum class struct union", end:/[{;:<>=]/, contains:[{beginKeywords:"final class struct"}, d.TITLE_MODE]}]), exports:{preprocessor:x, strings:k, keywords:w}};
  };
}());
hljs.registerLanguage("markdown", function() {
  function c(d) {
    for (var f = [], b = 0; b < arguments.length; ++b) {
      f[b - 0] = arguments[b];
    }
    return f.map(function(h) {
      return (k = h) ? "string" == typeof k ? k : k.source : null;
      var k;
    }).join("");
  }
  return function(d) {
    d = {begin:/<\/?[A-Za-z_]/, end:">", subLanguage:"xml", relevance:0};
    var f = {variants:[{begin:/\[.+?\]\[.*?\]/, relevance:0}, {begin:/\[.+?\]\(((data|javascript|mailto):|(?:http|ftp)s?:\/\/).*?\)/, relevance:2}, {begin:c(/\[.+?\]\(/, /[A-Za-z][A-Za-z0-9+.-]*/, /:\/\/.*?\)/), relevance:2}, {begin:/\[.+?\]\([./?&#].*?\)/, relevance:1}, {begin:/\[.+?\]\(.*?\)/, relevance:0}], returnBegin:!0, contains:[{className:"string", relevance:0, begin:"\\[", end:"\\]", excludeBegin:!0, returnEnd:!0}, {className:"link", relevance:0, begin:"\\]\\(", end:"\\)", excludeBegin:!0, 
    excludeEnd:!0}, {className:"symbol", relevance:0, begin:"\\]\\[", end:"\\]", excludeBegin:!0, excludeEnd:!0}]}, b = {className:"strong", contains:[], variants:[{begin:/_{2}/, end:/_{2}/}, {begin:/\*{2}/, end:/\*{2}/}]}, h = {className:"emphasis", contains:[], variants:[{begin:/\*(?!\*)/, end:/\*/}, {begin:/_(?!_)/, end:/_/, relevance:0}]};
    b.contains.push(h);
    h.contains.push(b);
    var k = [d, f];
    return b.contains = b.contains.concat(k), h.contains = h.contains.concat(k), k = k.concat(b, h), {name:"Markdown", aliases:["md", "mkdown", "mkd"], contains:[{className:"section", variants:[{begin:"^#{1,6}", end:"$", contains:k}, {begin:"(?=^.+?\\n[=-]{2,}$)", contains:[{begin:"^[=-]*$"}, {begin:"^", end:"\\n", contains:k}]}]}, d, {className:"bullet", begin:"^[ \t]*([*+-]|(\\d+\\.))(?=\\s+)", end:"\\s+", excludeEnd:!0}, b, h, {className:"quote", begin:"^>\\s+", contains:k, end:"$"}, {className:"code", 
    variants:[{begin:"(`{3,})[^`](.|\\n)*?\\1`*[ ]*"}, {begin:"(~{3,})[^~](.|\\n)*?\\1~*[ ]*"}, {begin:"```", end:"```+[ ]*$"}, {begin:"~~~", end:"~~~+[ ]*$"}, {begin:"`.+?`"}, {begin:"(?=^( {4}|\\t))", contains:[{begin:"^( {4}|\\t)", end:"(\\n)$"}], relevance:0}]}, {begin:"^[-\\*]{3,}", end:"$"}, f, {begin:/^\[[^\n]+\]:/, returnBegin:!0, contains:[{className:"symbol", begin:/\[/, end:/\]/, excludeBegin:!0, excludeEnd:!0}, {className:"link", begin:/:\s*/, end:/$/, excludeBegin:!0}]}]};
  };
}());
hljs.registerLanguage("scss", function() {
  var c = "a abbr address article aside audio b blockquote body button canvas caption cite code dd del details dfn div dl dt em fieldset figcaption figure footer form h1 h2 h3 h4 h5 h6 header hgroup html i iframe img input ins kbd label legend li main mark menu nav object ol p q quote samp section span strong summary sup table tbody td textarea tfoot th thead time tr ul var video".split(" "), d = "any-hover any-pointer aspect-ratio color color-gamut color-index device-aspect-ratio device-height device-width display-mode forced-colors grid height hover inverted-colors monochrome orientation overflow-block overflow-inline pointer prefers-color-scheme prefers-contrast prefers-reduced-motion prefers-reduced-transparency resolution scan scripting update width min-width max-width min-height max-height".split(" "), 
  f = "active any-link blank checked current default defined dir disabled drop empty enabled first first-child first-of-type fullscreen future focus focus-visible focus-within has host host-context hover indeterminate in-range invalid is lang last-child last-of-type left link local-link not nth-child nth-col nth-last-child nth-last-col nth-last-of-type nth-of-type only-child only-of-type optional out-of-range past placeholder-shown read-only read-write required right root scope target target-within user-invalid valid visited where".split(" "), 
  b = "after backdrop before cue cue-region first-letter first-line grammar-error marker part placeholder selection slotted spelling-error".split(" "), h = "align-content align-items align-self animation animation-delay animation-direction animation-duration animation-fill-mode animation-iteration-count animation-name animation-play-state animation-timing-function auto backface-visibility background background-attachment background-clip background-color background-image background-origin background-position background-repeat background-size border border-bottom border-bottom-color border-bottom-left-radius border-bottom-right-radius border-bottom-style border-bottom-width border-collapse border-color border-image border-image-outset border-image-repeat border-image-slice border-image-source border-image-width border-left border-left-color border-left-style border-left-width border-radius border-right border-right-color border-right-style border-right-width border-spacing border-style border-top border-top-color border-top-left-radius border-top-right-radius border-top-style border-top-width border-width bottom box-decoration-break box-shadow box-sizing break-after break-before break-inside caption-side clear clip clip-path color column-count column-fill column-gap column-rule column-rule-color column-rule-style column-rule-width column-span column-width columns content counter-increment counter-reset cursor direction display empty-cells filter flex flex-basis flex-direction flex-flow flex-grow flex-shrink flex-wrap float font font-display font-family font-feature-settings font-kerning font-language-override font-size font-size-adjust font-smoothing font-stretch font-style font-variant font-variant-ligatures font-variation-settings font-weight height hyphens icon image-orientation image-rendering image-resolution ime-mode inherit initial justify-content left letter-spacing line-height list-style list-style-image list-style-position list-style-type margin margin-bottom margin-left margin-right margin-top marks mask max-height max-width min-height min-width nav-down nav-index nav-left nav-right nav-up none normal object-fit object-position opacity order orphans outline outline-color outline-offset outline-style outline-width overflow overflow-wrap overflow-x overflow-y padding padding-bottom padding-left padding-right padding-top page-break-after page-break-before page-break-inside perspective perspective-origin pointer-events position quotes resize right src tab-size table-layout text-align text-align-last text-decoration text-decoration-color text-decoration-line text-decoration-style text-indent text-overflow text-rendering text-shadow text-transform text-underline-position top transform transform-origin transform-style transition transition-delay transition-duration transition-property transition-timing-function unicode-bidi vertical-align visibility white-space widows width word-break word-spacing word-wrap z-index".split(" ").reverse();
  return function(k) {
    var u = {className:"number", begin:"#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})"}, x = {className:"variable", begin:"(\\$[a-zA-Z-][a-zA-Z0-9_-]*)\\b"};
    return {name:"SCSS", case_insensitive:!0, illegal:"[=/|']", contains:[k.C_LINE_COMMENT_MODE, k.C_BLOCK_COMMENT_MODE, {className:"selector-id", begin:"#[A-Za-z0-9_-]+", relevance:0}, {className:"selector-class", begin:"\\.[A-Za-z0-9_-]+", relevance:0}, {className:"selector-attr", begin:/\[/, end:/\]/, illegal:"$", contains:[k.APOS_STRING_MODE, k.QUOTE_STRING_MODE]}, {className:"selector-tag", begin:"\\b(" + c.join("|") + ")\\b", relevance:0}, {className:"selector-pseudo", begin:":(" + f.join("|") + 
    ")"}, {className:"selector-pseudo", begin:"::(" + b.join("|") + ")"}, x, {begin:/\(/, end:/\)/, contains:[k.CSS_NUMBER_MODE]}, {className:"attribute", begin:"\\b(" + h.join("|") + ")\\b"}, {begin:"\\b(whitespace|wait|w-resize|visible|vertical-text|vertical-ideographic|uppercase|upper-roman|upper-alpha|underline|transparent|top|thin|thick|text|text-top|text-bottom|tb-rl|table-header-group|table-footer-group|sw-resize|super|strict|static|square|solid|small-caps|separate|se-resize|scroll|s-resize|rtl|row-resize|ridge|right|repeat|repeat-y|repeat-x|relative|progress|pointer|overline|outside|outset|oblique|nowrap|not-allowed|normal|none|nw-resize|no-repeat|no-drop|newspaper|ne-resize|n-resize|move|middle|medium|ltr|lr-tb|lowercase|lower-roman|lower-alpha|loose|list-item|line|line-through|line-edge|lighter|left|keep-all|justify|italic|inter-word|inter-ideograph|inside|inset|inline|inline-block|inherit|inactive|ideograph-space|ideograph-parenthesis|ideograph-numeric|ideograph-alpha|horizontal|hidden|help|hand|groove|fixed|ellipsis|e-resize|double|dotted|distribute|distribute-space|distribute-letter|distribute-all-lines|disc|disabled|default|decimal|dashed|crosshair|collapse|col-resize|circle|char|center|capitalize|break-word|break-all|bottom|both|bolder|bold|block|bidi-override|below|baseline|auto|always|all-scroll|absolute|table|table-cell)\\b"}, 
    {begin:":", end:";", contains:[x, u, k.CSS_NUMBER_MODE, k.QUOTE_STRING_MODE, k.APOS_STRING_MODE, {className:"meta", begin:"!important"}]}, {begin:"@(page|font-face)", lexemes:"@[a-z-]+", keywords:"@page @font-face"}, {begin:"@", end:"[{;]", returnBegin:!0, keywords:{$pattern:/[a-z-]+/, keyword:"and or not only", attribute:d.join(" ")}, contains:[{begin:"@[a-z-]+", className:"keyword"}, {begin:/[a-z-]+(?=:)/, className:"attribute"}, x, k.QUOTE_STRING_MODE, k.APOS_STRING_MODE, u, k.CSS_NUMBER_MODE]}]};
  };
}());
hljs.registerLanguage("http", function() {
  function c(d) {
    for (var f = [], b = 0; b < arguments.length; ++b) {
      f[b - 0] = arguments[b];
    }
    return f.map(function(h) {
      return (k = h) ? "string" == typeof k ? k : k.source : null;
      var k;
    }).join("");
  }
  return function(d) {
    var f = {className:"attribute", begin:c("^", /[A-Za-z][A-Za-z0-9-]*/, "(?=\\:\\s)"), starts:{contains:[{className:"punctuation", begin:/: /, relevance:0, starts:{end:"$", relevance:0}}]}}, b = [f, {begin:"\\n\\n", starts:{subLanguage:[], endsWithParent:!0}}];
    return {name:"HTTP", aliases:["https"], illegal:/\S/, contains:[{begin:"^(?=HTTP/(2|1\\.[01]) \\d{3})", end:/$/, contains:[{className:"meta", begin:"HTTP/(2|1\\.[01])"}, {className:"number", begin:"\\b\\d{3}\\b"}], starts:{end:/\b\B/, illegal:/\S/, contains:b}}, {begin:"(?=^[A-Z]+ (.*?) HTTP/(2|1\\.[01])$)", end:/$/, contains:[{className:"string", begin:" ", end:" ", excludeBegin:!0, excludeEnd:!0}, {className:"meta", begin:"HTTP/(2|1\\.[01])"}, {className:"keyword", begin:"[A-Z]+"}], starts:{end:/\b\B/, 
    illegal:/\S/, contains:b}}, d.inherit(f, {relevance:0})]};
  };
}());
hljs.registerLanguage("plaintext", function() {
  return function(c) {
    return {name:"Plain text", aliases:["text", "txt"], disableAutodetect:!0};
  };
}());
hljs.registerLanguage("powershell", function() {
  return function(c) {
    var d = {$pattern:/-?[A-z\.\-]+\b/, keyword:"if else foreach return do while until elseif begin for trap data dynamicparam end break throw param continue finally in switch exit filter try process catch hidden static parameter", built_in:"ac asnp cat cd CFS chdir clc clear clhy cli clp cls clv cnsn compare copy cp cpi cpp curl cvpa dbp del diff dir dnsn ebp echo|0 epal epcsv epsn erase etsn exsn fc fhx fl ft fw gal gbp gc gcb gci gcm gcs gdr gerr ghy gi gin gjb gl gm gmo gp gps gpv group gsn gsnp gsv gtz gu gv gwmi h history icm iex ihy ii ipal ipcsv ipmo ipsn irm ise iwmi iwr kill lp ls man md measure mi mount move mp mv nal ndr ni nmo npssc nsn nv ogv oh popd ps pushd pwd r rbp rcjb rcsn rd rdr ren ri rjb rm rmdir rmo rni rnp rp rsn rsnp rujb rv rvpa rwmi sajb sal saps sasv sbp sc scb select set shcm si sl sleep sls sort sp spjb spps spsv start stz sujb sv swmi tee trcm type wget where wjb write"}, 
    f = {begin:"`[\\s\\S]", relevance:0}, b = {className:"variable", variants:[{begin:/\$\B/}, {className:"keyword", begin:/\$this/}, {begin:/\$[\w\d][\w\d_:]*/}]}, h = {className:"string", variants:[{begin:/"/, end:/"/}, {begin:/@"/, end:/^"@/}], contains:[f, b, {className:"variable", begin:/\$[A-z]/, end:/[^A-z]/}]}, k = {className:"string", variants:[{begin:/'/, end:/'/}, {begin:/@'/, end:/^'@/}]}, u = c.inherit(c.COMMENT(null, null), {variants:[{begin:/#/, end:/$/}, {begin:/<#/, end:/#>/}], contains:[{className:"doctag", 
    variants:[{begin:/\.(synopsis|description|example|inputs|outputs|notes|link|component|role|functionality)/}, {begin:/\.(parameter|forwardhelptargetname|forwardhelpcategory|remotehelprunspace|externalhelp)\s+\S+/}]}]}), x = {className:"class", beginKeywords:"class enum", end:/\s*[{]/, excludeEnd:!0, relevance:0, contains:[c.TITLE_MODE]}, t = {className:"function", begin:/function\s+/, end:/\s*\{|$/, excludeEnd:!0, returnBegin:!0, relevance:0, contains:[{begin:"function", relevance:0, className:"keyword"}, 
    {className:"title", begin:/\w[\w\d]*((-)[\w\d]+)*/, relevance:0}, {begin:/\(/, end:/\)/, className:"params", relevance:0, contains:[b]}]}, r = {begin:/using\s/, end:/$/, returnBegin:!0, contains:[h, k, {className:"keyword", begin:/(using|assembly|command|module|namespace|type)/}]}, w = {className:"function", begin:/\[.*\]\s*[\w]+[ ]??\(/, end:/$/, returnBegin:!0, relevance:0, contains:[{className:"keyword", begin:"(".concat(d.keyword.toString().replace(/\s/g, "|"), ")\\b"), endsParent:!0, relevance:0}, 
    c.inherit(c.TITLE_MODE, {endsParent:!0})]};
    c = [w, u, f, c.NUMBER_MODE, h, k, {className:"built_in", variants:[{begin:"(Add|Clear|Close|Copy|Enter|Exit|Find|Format|Get|Hide|Join|Lock|Move|New|Open|Optimize|Pop|Push|Redo|Remove|Rename|Reset|Resize|Search|Select|Set|Show|Skip|Split|Step|Switch|Undo|Unlock|Watch|Backup|Checkpoint|Compare|Compress|Convert|ConvertFrom|ConvertTo|Dismount|Edit|Expand|Export|Group|Import|Initialize|Limit|Merge|Mount|Out|Publish|Restore|Save|Sync|Unpublish|Update|Approve|Assert|Build|Complete|Confirm|Deny|Deploy|Disable|Enable|Install|Invoke|Register|Request|Restart|Resume|Start|Stop|Submit|Suspend|Uninstall|Unregister|Wait|Debug|Measure|Ping|Repair|Resolve|Test|Trace|Connect|Disconnect|Read|Receive|Send|Write|Block|Grant|Protect|Revoke|Unblock|Unprotect|Use|ForEach|Sort|Tee|Where)+(-)[\\w\\d]+"}]}, 
    b, {className:"literal", begin:/\$(null|true|false)\b/}, {className:"selector-tag", begin:/@\B/, relevance:0}];
    f = {begin:/\[/, end:/\]/, excludeBegin:!0, excludeEnd:!0, relevance:0, contains:[].concat("self", c, {begin:"(string|char|byte|int|long|bool|decimal|single|double|DateTime|xml|array|hashtable|void)", className:"built_in", relevance:0}, {className:"type", begin:/[\.\w\d]+/, relevance:0})};
    return w.contains.unshift(f), {name:"PowerShell", aliases:["ps", "ps1"], case_insensitive:!0, keywords:d, contains:c.concat(x, t, r, {variants:[{className:"operator", begin:"(-and|-as|-band|-bnot|-bor|-bxor|-casesensitive|-ccontains|-ceq|-cge|-cgt|-cle|-clike|-clt|-cmatch|-cne|-cnotcontains|-cnotlike|-cnotmatch|-contains|-creplace|-csplit|-eq|-exact|-f|-file|-ge|-gt|-icontains|-ieq|-ige|-igt|-ile|-ilike|-ilt|-imatch|-in|-ine|-inotcontains|-inotlike|-inotmatch|-ireplace|-is|-isnot|-isplit|-join|-le|-like|-lt|-match|-ne|-not|-notcontains|-notin|-notlike|-notmatch|-or|-regex|-replace|-shl|-shr|-split|-wildcard|-xor)\\b"}, 
    {className:"literal", begin:/(-)[\w\d]+/, relevance:0}]}, f)};
  };
}());
hljs.registerLanguage("sql", function() {
  function c(f) {
    for (var b = [], h = 0; h < arguments.length; ++h) {
      b[h - 0] = arguments[h];
    }
    return b.map(function(k) {
      return k ? "string" == typeof k ? k : k.source : null;
    }).join("");
  }
  function d(f) {
    for (var b = [], h = 0; h < arguments.length; ++h) {
      b[h - 0] = arguments[h];
    }
    return "(" + b.map(function(k) {
      return k ? "string" == typeof k ? k : k.source : null;
    }).join("|") + ")";
  }
  return function(f) {
    var b = f.COMMENT("--", "$"), h = ["true", "false", "unknown"], k = "bigint binary blob boolean char character clob date dec decfloat decimal float int integer interval nchar nclob national numeric real row smallint time timestamp varchar varying varbinary".split(" "), u = "abs acos array_agg asin atan avg cast ceil ceiling coalesce corr cos cosh count covar_pop covar_samp cume_dist dense_rank deref element exp extract first_value floor json_array json_arrayagg json_exists json_object json_objectagg json_query json_table json_table_primitive json_value lag last_value lead listagg ln log log10 lower max min mod nth_value ntile nullif percent_rank percentile_cont percentile_disc position position_regex power rank regr_avgx regr_avgy regr_count regr_intercept regr_r2 regr_slope regr_sxx regr_sxy regr_syy row_number sin sinh sqrt stddev_pop stddev_samp substring substring_regex sum tan tanh translate translate_regex treat trim trim_array unnest upper value_of var_pop var_samp width_bucket".split(" "), 
    x = "create table;insert into;primary key;foreign key;not null;alter table;add constraint;grouping sets;on overflow;character set;respect nulls;ignore nulls;nulls first;nulls last;depth first;breadth first".split(";"), t = "abs;acos;all;allocate;alter;and;any;are;array;array_agg;array_max_cardinality;as;asensitive;asin;asymmetric;at;atan;atomic;authorization;avg;begin;begin_frame;begin_partition;between;bigint;binary;blob;boolean;both;by;call;called;cardinality;cascaded;case;cast;ceil;ceiling;char;char_length;character;character_length;check;classifier;clob;close;coalesce;collate;collect;column;commit;condition;connect;constraint;contains;convert;copy;corr;corresponding;cos;cosh;count;covar_pop;covar_samp;create;cross;cube;cume_dist;current;current_catalog;current_date;current_default_transform_group;current_path;current_role;current_row;current_schema;current_time;current_timestamp;current_path;current_role;current_transform_group_for_type;current_user;cursor;cycle;date;day;deallocate;dec;decimal;decfloat;declare;default;define;delete;dense_rank;deref;describe;deterministic;disconnect;distinct;double;drop;dynamic;each;element;else;empty;end;end_frame;end_partition;end-exec;equals;escape;every;except;exec;execute;exists;exp;external;extract;false;fetch;filter;first_value;float;floor;for;foreign;frame_row;free;from;full;function;fusion;get;global;grant;group;grouping;groups;having;hold;hour;identity;in;indicator;initial;inner;inout;insensitive;insert;int;integer;intersect;intersection;interval;into;is;join;json_array;json_arrayagg;json_exists;json_object;json_objectagg;json_query;json_table;json_table_primitive;json_value;lag;language;large;last_value;lateral;lead;leading;left;like;like_regex;listagg;ln;local;localtime;localtimestamp;log;log10;lower;match;match_number;match_recognize;matches;max;member;merge;method;min;minute;mod;modifies;module;month;multiset;national;natural;nchar;nclob;new;no;none;normalize;not;nth_value;ntile;null;nullif;numeric;octet_length;occurrences_regex;of;offset;old;omit;on;one;only;open;or;order;out;outer;over;overlaps;overlay;parameter;partition;pattern;per;percent;percent_rank;percentile_cont;percentile_disc;period;portion;position;position_regex;power;precedes;precision;prepare;primary;procedure;ptf;range;rank;reads;real;recursive;ref;references;referencing;regr_avgx;regr_avgy;regr_count;regr_intercept;regr_r2;regr_slope;regr_sxx;regr_sxy;regr_syy;release;result;return;returns;revoke;right;rollback;rollup;row;row_number;rows;running;savepoint;scope;scroll;search;second;seek;select;sensitive;session_user;set;show;similar;sin;sinh;skip;smallint;some;specific;specifictype;sql;sqlexception;sqlstate;sqlwarning;sqrt;start;static;stddev_pop;stddev_samp;submultiset;subset;substring;substring_regex;succeeds;sum;symmetric;system;system_time;system_user;table;tablesample;tan;tanh;then;time;timestamp;timezone_hour;timezone_minute;to;trailing;translate;translate_regex;translation;treat;trigger;trim;trim_array;true;truncate;uescape;union;unique;unknown;unnest;update   ;upper;user;using;value;values;value_of;var_pop;var_samp;varbinary;varchar;varying;versioning;when;whenever;where;width_bucket;window;with;within;without;year;add;asc;collation;desc;final;first;last;view".split(";").filter(function(w) {
      return !u.includes(w);
    }), r = {begin:c(/\b/, d.apply(null, $jscomp.arrayFromIterable(u)), /\s*\(/), keywords:{built_in:u}};
    return {name:"SQL", case_insensitive:!0, illegal:/[{}]|<\//, keywords:{$pattern:/\b[\w\.]+/, keyword:function(w, B) {
      B = void 0 === B ? {} : B;
      var P = B.exceptions, ca = B.when;
      return P = P || [], w.map(function(R) {
        return R.match(/\|\d+$/) || P.includes(R) ? R : ca(R) ? R + "|0" : R;
      });
    }(t, {when:function(w) {
      return 3 > w.length;
    }}), literal:h, type:k, built_in:"current_catalog current_date current_default_transform_group current_path current_role current_schema current_transform_group_for_type current_user session_user system_time system_user current_time localtime current_timestamp localtimestamp".split(" ")}, contains:[{begin:d.apply(null, $jscomp.arrayFromIterable(x)), keywords:{$pattern:/[\w\.]+/, keyword:t.concat(x), literal:h, type:k}}, {className:"type", begin:d("double precision", "large object", "with timezone", 
    "without timezone")}, r, {className:"variable", begin:/@[a-z0-9]+/}, {className:"string", variants:[{begin:/'/, end:/'/, contains:[{begin:/''/}]}]}, {begin:/"/, end:/"/, contains:[{begin:/""/}]}, f.C_NUMBER_MODE, f.C_BLOCK_COMMENT_MODE, b, {className:"operator", begin:/[-+*/=%^~]|&&?|\|\|?|!=?|<(?:=>?|<|>)?|>[>=]?/, relevance:0}]};
  };
}());
hljs.registerLanguage("scheme", function() {
  return function(c) {
    var d = {className:"literal", begin:"(#t|#f|#\\\\[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+|#\\\\.)"}, f = {className:"number", variants:[{begin:"(-|\\+)?\\d+([./]\\d+)?", relevance:0}, {begin:"(-|\\+)?\\d+([./]\\d+)?[+\\-](-|\\+)?\\d+([./]\\d+)?i", relevance:0}, {begin:"#b[0-1]+(/[0-1]+)?"}, {begin:"#o[0-7]+(/[0-7]+)?"}, {begin:"#x[0-9a-f]+(/[0-9a-f]+)?"}]}, b = c.QUOTE_STRING_MODE, h = [c.COMMENT(";", "$", {relevance:0}), c.COMMENT("#\\|", "\\|#")], k = {begin:"[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+", 
    relevance:0}, u = {className:"symbol", begin:"'[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+"}, x = {endsWithParent:!0, relevance:0}, t = {variants:[{begin:/'/}, {begin:"`"}], contains:[{begin:"\\(", end:"\\)", contains:["self", d, b, f, k, u]}]}, r = {className:"name", relevance:0, begin:"[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+", keywords:{$pattern:"[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+", "builtin-name":"case-lambda call/cc class define-class exit-handler field import inherit init-field interface let*-values let-values let/ec mixin opt-lambda override protect provide public rename require require-for-syntax syntax syntax-case syntax-error unit/sig unless when with-syntax and begin call-with-current-continuation call-with-input-file call-with-output-file case cond define define-syntax delay do dynamic-wind else for-each if lambda let let* let-syntax letrec letrec-syntax map or syntax-rules ' * + , ,@ - ... / ; < <= = => > >= ` abs acos angle append apply asin assoc assq assv atan boolean? caar cadr call-with-input-file call-with-output-file call-with-values car cdddar cddddr cdr ceiling char->integer char-alphabetic? char-ci<=? char-ci<? char-ci=? char-ci>=? char-ci>? char-downcase char-lower-case? char-numeric? char-ready? char-upcase char-upper-case? char-whitespace? char<=? char<? char=? char>=? char>? char? close-input-port close-output-port complex? cons cos current-input-port current-output-port denominator display eof-object? eq? equal? eqv? eval even? exact->inexact exact? exp expt floor force gcd imag-part inexact->exact inexact? input-port? integer->char integer? interaction-environment lcm length list list->string list->vector list-ref list-tail list? load log magnitude make-polar make-rectangular make-string make-vector max member memq memv min modulo negative? newline not null-environment null? number->string number? numerator odd? open-input-file open-output-file output-port? pair? peek-char port? positive? procedure? quasiquote quote quotient rational? rationalize read read-char real-part real? remainder reverse round scheme-report-environment set! set-car! set-cdr! sin sqrt string string->list string->number string->symbol string-append string-ci<=? string-ci<? string-ci=? string-ci>=? string-ci>? string-copy string-fill! string-length string-ref string-set! string<=? string<? string=? string>=? string>? string? substring symbol->string symbol? tan transcript-off transcript-on truncate values vector vector->list vector-fill! vector-length vector-ref vector-set! with-input-from-file with-output-to-file write write-char zero?"}};
    r = {variants:[{begin:"\\(", end:"\\)"}, {begin:"\\[", end:"\\]"}], contains:[{begin:/lambda/, endsWithParent:!0, returnBegin:!0, contains:[r, {endsParent:!0, variants:[{begin:/\(/, end:/\)/}, {begin:/\[/, end:/\]/}], contains:[k]}]}, r, x]};
    return x.contains = [d, f, b, k, u, t, r].concat(h), {name:"Scheme", illegal:/\S/, contains:[c.SHEBANG(), f, b, u, t, r].concat(h)};
  };
}());
hljs.registerLanguage("javascript", function() {
  function c(h) {
    for (var k = [], u = 0; u < arguments.length; ++u) {
      k[u - 0] = arguments[u];
    }
    return k.map(function(x) {
      return (t = x) ? "string" == typeof t ? t : t.source : null;
      var t;
    }).join("");
  }
  var d = "as in of if for while finally var new function do return void else break catch instanceof with throw case default try switch continue typeof delete let yield const class debugger async await static import from export extends".split(" "), f = "true false null undefined NaN Infinity".split(" "), b = [].concat("setInterval setTimeout clearInterval clearTimeout require exports eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape".split(" "), 
  "arguments this super console window document localStorage module global".split(" "), "Intl DataView Number Math Date String RegExp Object Function Boolean Error Symbol Set Map WeakSet WeakMap Proxy Reflect JSON Promise Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array Float32Array Array Uint8Array Uint8ClampedArray ArrayBuffer BigInt64Array BigUint64Array BigInt".split(" "), "EvalError InternalError RangeError ReferenceError SyntaxError TypeError URIError".split(" "));
  return function(h) {
    var k = /<[A-Za-z0-9\\._:-]+/, u = /\/[A-Za-z0-9\\._:-]+>|\/>/, x = {$pattern:"[A-Za-z$_][0-9A-Za-z$_]*", keyword:d, literal:f, built_in:b}, t = {className:"number", variants:[{begin:"(\\b(0|[1-9](_?[0-9])*|0[0-7]*[89][0-9]*)((\\.([0-9](_?[0-9])*))|\\.)?|(\\.([0-9](_?[0-9])*)))[eE][+-]?([0-9](_?[0-9])*)\\b"}, {begin:"\\b(0|[1-9](_?[0-9])*|0[0-7]*[89][0-9]*)\\b((\\.([0-9](_?[0-9])*))\\b|\\.)?|(\\.([0-9](_?[0-9])*))\\b"}, {begin:"\\b(0|[1-9](_?[0-9])*)n\\b"}, {begin:"\\b0[xX][0-9a-fA-F](_?[0-9a-fA-F])*n?\\b"}, 
    {begin:"\\b0[bB][0-1](_?[0-1])*n?\\b"}, {begin:"\\b0[oO][0-7](_?[0-7])*n?\\b"}, {begin:"\\b0[0-7]+n?\\b"}], relevance:0}, r = {className:"subst", begin:"\\$\\{", end:"\\}", keywords:x, contains:[]}, w = {begin:"html`", end:"", starts:{end:"`", returnEnd:!1, contains:[h.BACKSLASH_ESCAPE, r], subLanguage:"xml"}}, B = {begin:"css`", end:"", starts:{end:"`", returnEnd:!1, contains:[h.BACKSLASH_ESCAPE, r], subLanguage:"css"}}, P = {className:"string", begin:"`", end:"`", contains:[h.BACKSLASH_ESCAPE, 
    r]}, ca = {className:"comment", variants:[h.COMMENT(/\/\*\*(?!\/)/, "\\*/", {relevance:0, contains:[{className:"doctag", begin:"@[A-Za-z]+", contains:[{className:"type", begin:"\\{", end:"\\}", relevance:0}, {className:"variable", begin:"[A-Za-z$_][0-9A-Za-z$_]*(?=\\s*(-)|$)", endsParent:!0, relevance:0}, {begin:/(?=[^\n])\s/, relevance:0}]}]}), h.C_BLOCK_COMMENT_MODE, h.C_LINE_COMMENT_MODE]}, R = [h.APOS_STRING_MODE, h.QUOTE_STRING_MODE, w, B, P, t, h.REGEXP_MODE];
    r.contains = R.concat({begin:/\{/, end:/\}/, keywords:x, contains:["self"].concat(R)});
    r = [].concat(ca, r.contains);
    r = r.concat([{begin:/\(/, end:/\)/, keywords:x, contains:["self"].concat(r)}]);
    R = {className:"params", begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:x, contains:r};
    return {name:"Javascript", aliases:["js", "jsx", "mjs", "cjs"], keywords:x, exports:{PARAMS_CONTAINS:r}, illegal:/#(?![$_A-z])/, contains:[h.SHEBANG({label:"shebang", binary:"node", relevance:5}), {label:"use_strict", className:"meta", relevance:10, begin:/^\s*['"]use (strict|asm)['"]/}, h.APOS_STRING_MODE, h.QUOTE_STRING_MODE, w, B, P, ca, t, {begin:c(/[{,\n]\s*/, c("(?=", c(/(((\/\/.*$)|(\/\*(\*[^/]|[^*])*\*\/))\s*)*/, "[A-Za-z$_][0-9A-Za-z$_]*\\s*:"), ")")), relevance:0, contains:[{className:"attr", 
    begin:"[A-Za-z$_][0-9A-Za-z$_]*" + c("(?=", "\\s*:", ")"), relevance:0}]}, {begin:"(" + h.RE_STARTERS_RE + "|\\b(case|return|throw)\\b)\\s*", keywords:"return throw case", contains:[ca, h.REGEXP_MODE, {className:"function", begin:"(\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)|" + h.UNDERSCORE_IDENT_RE + ")\\s*=>", returnBegin:!0, end:"\\s*=>", contains:[{className:"params", variants:[{begin:h.UNDERSCORE_IDENT_RE, relevance:0}, {className:null, begin:/\(\s*\)/, skip:!0}, {begin:/\(/, 
    end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:x, contains:r}]}]}, {begin:/,/, relevance:0}, {className:"", begin:/\s/, end:/\s*/, skip:!0}, {variants:[{begin:"<>", end:"</>"}, {begin:k, "on:begin":function(Y, X) {
      var a = Y[0].length + Y.index, g = Y.input[a];
      "<" !== g ? ">" === g && (g = "</" + Y[0].slice(1), -1 !== Y.input.indexOf(g, a) || X.ignoreMatch()) : X.ignoreMatch();
    }, end:u}], subLanguage:"xml", contains:[{begin:k, end:u, skip:!0, contains:["self"]}]}], relevance:0}, {className:"function", beginKeywords:"function", end:/[{;]/, excludeEnd:!0, keywords:x, contains:["self", h.inherit(h.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*"}), R], illegal:/%/}, {beginKeywords:"while if switch catch for"}, {className:"function", begin:h.UNDERSCORE_IDENT_RE + "\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)\\s*\\{", returnBegin:!0, contains:[R, h.inherit(h.TITLE_MODE, 
    {begin:"[A-Za-z$_][0-9A-Za-z$_]*"})]}, {variants:[{begin:"\\.[A-Za-z$_][0-9A-Za-z$_]*"}, {begin:"\\$[A-Za-z$_][0-9A-Za-z$_]*"}], relevance:0}, {className:"class", beginKeywords:"class", end:/[{;=]/, excludeEnd:!0, illegal:/[:"[\]]/, contains:[{beginKeywords:"extends"}, h.UNDERSCORE_TITLE_MODE]}, {begin:/\b(?=constructor)/, end:/[{;]/, excludeEnd:!0, contains:[h.inherit(h.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*"}), "self", R]}, {begin:"(get|set)\\s+(?=[A-Za-z$_][0-9A-Za-z$_]*\\()", end:/\{/, 
    keywords:"get set", contains:[h.inherit(h.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*"}), {begin:/\(\)/}, R]}, {begin:/\$[(.]/}]};
  };
}());
hljs.registerLanguage("java", function() {
  var c = {className:"number", variants:[{begin:"(\\b([0-9](_*[0-9])*)((\\.([0-9](_*[0-9])*))|\\.)?|(\\.([0-9](_*[0-9])*)))[eE][+-]?([0-9](_*[0-9])*)[fFdD]?\\b"}, {begin:"\\b([0-9](_*[0-9])*)((\\.([0-9](_*[0-9])*))[fFdD]?\\b|\\.([fFdD]\\b)?)"}, {begin:"(\\.([0-9](_*[0-9])*))[fFdD]?\\b"}, {begin:"\\b([0-9](_*[0-9])*)[fFdD]\\b"}, {begin:"\\b0[xX](([0-9a-fA-F](_*[0-9a-fA-F])*)\\.?|([0-9a-fA-F](_*[0-9a-fA-F])*)?\\.([0-9a-fA-F](_*[0-9a-fA-F])*))[pP][+-]?([0-9](_*[0-9])*)[fFdD]?\\b"}, {begin:"\\b(0|[1-9](_*[0-9])*)[lL]?\\b"}, 
  {begin:"\\b0[xX]([0-9a-fA-F](_*[0-9a-fA-F])*)[lL]?\\b"}, {begin:"\\b0(_*[0-7])*[lL]?\\b"}, {begin:"\\b0[bB][01](_*[01])*[lL]?\\b"}], relevance:0};
  return function(d) {
    var f = {className:"meta", begin:"@[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*", contains:[{begin:/\(/, end:/\)/, contains:["self"]}]};
    return {name:"Java", aliases:["jsp"], keywords:"false synchronized int abstract float private char boolean var static null if const for true while long strictfp finally protected import native final void enum else break transient catch instanceof byte super volatile case assert short package default double public try this switch continue throws protected public private module requires exports do", illegal:/<\/|#/, contains:[d.COMMENT("/\\*\\*", "\\*/", {relevance:0, contains:[{begin:/\w+@/, relevance:0}, 
    {className:"doctag", begin:"@[A-Za-z]+"}]}), {begin:/import java\.[a-z]+\./, keywords:"import", relevance:2}, d.C_LINE_COMMENT_MODE, d.C_BLOCK_COMMENT_MODE, d.APOS_STRING_MODE, d.QUOTE_STRING_MODE, {className:"class", beginKeywords:"class interface enum", end:/[{;=]/, excludeEnd:!0, relevance:1, keywords:"class interface enum", illegal:/[:"\[\]]/, contains:[{beginKeywords:"extends implements"}, d.UNDERSCORE_TITLE_MODE]}, {beginKeywords:"new throw return else", relevance:0}, {className:"class", 
    begin:"record\\s+" + d.UNDERSCORE_IDENT_RE + "\\s*\\(", returnBegin:!0, excludeEnd:!0, end:/[{;=]/, keywords:"false synchronized int abstract float private char boolean var static null if const for true while long strictfp finally protected import native final void enum else break transient catch instanceof byte super volatile case assert short package default double public try this switch continue throws protected public private module requires exports do", contains:[{beginKeywords:"record"}, 
    {begin:d.UNDERSCORE_IDENT_RE + "\\s*\\(", returnBegin:!0, relevance:0, contains:[d.UNDERSCORE_TITLE_MODE]}, {className:"params", begin:/\(/, end:/\)/, keywords:"false synchronized int abstract float private char boolean var static null if const for true while long strictfp finally protected import native final void enum else break transient catch instanceof byte super volatile case assert short package default double public try this switch continue throws protected public private module requires exports do", 
    relevance:0, contains:[d.C_BLOCK_COMMENT_MODE]}, d.C_LINE_COMMENT_MODE, d.C_BLOCK_COMMENT_MODE]}, {className:"function", begin:"([À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*(<[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*(\\s*,\\s*[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*)*>)?\\s+)+" + d.UNDERSCORE_IDENT_RE + "\\s*\\(", returnBegin:!0, end:/[{;=]/, excludeEnd:!0, keywords:"false synchronized int abstract float private char boolean var static null if const for true while long strictfp finally protected import native final void enum else break transient catch instanceof byte super volatile case assert short package default double public try this switch continue throws protected public private module requires exports do", 
    contains:[{begin:d.UNDERSCORE_IDENT_RE + "\\s*\\(", returnBegin:!0, relevance:0, contains:[d.UNDERSCORE_TITLE_MODE]}, {className:"params", begin:/\(/, end:/\)/, keywords:"false synchronized int abstract float private char boolean var static null if const for true while long strictfp finally protected import native final void enum else break transient catch instanceof byte super volatile case assert short package default double public try this switch continue throws protected public private module requires exports do", 
    relevance:0, contains:[f, d.APOS_STRING_MODE, d.QUOTE_STRING_MODE, c, d.C_BLOCK_COMMENT_MODE]}, d.C_LINE_COMMENT_MODE, d.C_BLOCK_COMMENT_MODE]}, c, f]};
  };
}());
hljs.registerLanguage("php-template", function() {
  return function(c) {
    return {name:"PHP template", subLanguage:"xml", contains:[{begin:/<\?(php|=)?/, end:/\?>/, subLanguage:"php", contains:[{begin:"/\\*", end:"\\*/", skip:!0}, {begin:'b"', end:'"', skip:!0}, {begin:"b'", end:"'", skip:!0}, c.inherit(c.APOS_STRING_MODE, {illegal:null, className:null, contains:null, skip:!0}), c.inherit(c.QUOTE_STRING_MODE, {illegal:null, className:null, contains:null, skip:!0})]}]};
  };
}());
hljs.registerLanguage("python-repl", function() {
  return function(c) {
    return {aliases:["pycon"], contains:[{className:"meta", starts:{end:/ |$/, starts:{end:"$", subLanguage:"python"}}, variants:[{begin:/^>>>(?=[ ]|$)/}, {begin:/^\.\.\.(?=[ ]|$)/}]}]};
  };
}());
hljs.registerLanguage("swift", function() {
  function c(l) {
    return d("(?=", l, ")");
  }
  function d(l) {
    for (var m = [], v = 0; v < arguments.length; ++v) {
      m[v - 0] = arguments[v];
    }
    return m.map(function(n) {
      return n ? "string" == typeof n ? n : n.source : null;
    }).join("");
  }
  function f(l) {
    for (var m = [], v = 0; v < arguments.length; ++v) {
      m[v - 0] = arguments[v];
    }
    return "(" + m.map(function(n) {
      return n ? "string" == typeof n ? n : n.source : null;
    }).join("|") + ")";
  }
  var b = function(l) {
    return d(/\b/, l, /\w$/.test(l) ? /\b/ : /\B/);
  }, h = ["Protocol", "Type"].map(b), k = ["init", "self"].map(b), u = ["Any", "Self"], x = ["associatedtype", "async", "await", /as\?/, /as!/, "as", "break", "case", "catch", "class", "continue", "convenience", "default", "defer", "deinit", "didSet", "do", "dynamic", "else", "enum", "extension", "fallthrough", /fileprivate\(set\)/, "fileprivate", "final", "for", "func", "get", "guard", "if", "import", "indirect", "infix", /init\?/, /init!/, "inout", /internal\(set\)/, "internal", "in", "is", "lazy", 
  "let", "mutating", "nonmutating", /open\(set\)/, "open", "operator", "optional", "override", "postfix", "precedencegroup", "prefix", /private\(set\)/, "private", "protocol", /public\(set\)/, "public", "repeat", "required", "rethrows", "return", "set", "some", "static", "struct", "subscript", "super", "switch", "throws", "throw", /try\?/, /try!/, "try", "typealias", /unowned\(safe\)/, /unowned\(unsafe\)/, "unowned", "var", "weak", "where", "while", "willSet"], t = ["false", "nil", "true"], r = "assignment associativity higherThan left lowerThan none right".split(" "), 
  w = "#colorLiteral #column #dsohandle #else #elseif #endif #error #file #fileID #fileLiteral #filePath #function #if #imageLiteral #keyPath #line #selector #sourceLocation #warn_unqualified_access #warning".split(" "), B = "abs all any assert assertionFailure debugPrint dump fatalError getVaList isKnownUniquelyReferenced max min numericCast pointwiseMax pointwiseMin precondition preconditionFailure print readLine repeatElement sequence stride swap swift_unboxFromSwiftValueWithType transcode type unsafeBitCast unsafeDowncast withExtendedLifetime withUnsafeMutablePointer withUnsafePointer withVaList withoutActuallyEscaping zip".split(" "), 
  P = f(/[/=\-+!*%<>&|^~?]/, /[\u00A1-\u00A7]/, /[\u00A9\u00AB]/, /[\u00AC\u00AE]/, /[\u00B0\u00B1]/, /[\u00B6\u00BB\u00BF\u00D7\u00F7]/, /[\u2016-\u2017]/, /[\u2020-\u2027]/, /[\u2030-\u203E]/, /[\u2041-\u2053]/, /[\u2055-\u205E]/, /[\u2190-\u23FF]/, /[\u2500-\u2775]/, /[\u2794-\u2BFF]/, /[\u2E00-\u2E7F]/, /[\u3001-\u3003]/, /[\u3008-\u3020]/, /[\u3030]/), ca = f(P, /[\u0300-\u036F]/, /[\u1DC0-\u1DFF]/, /[\u20D0-\u20FF]/, /[\uFE00-\uFE0F]/, /[\uFE20-\uFE2F]/), R = d(P, ca, "*");
  P = f(/[a-zA-Z_]/, /[\u00A8\u00AA\u00AD\u00AF\u00B2-\u00B5\u00B7-\u00BA]/, /[\u00BC-\u00BE\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF]/, /[\u0100-\u02FF\u0370-\u167F\u1681-\u180D\u180F-\u1DBF]/, /[\u1E00-\u1FFF]/, /[\u200B-\u200D\u202A-\u202E\u203F-\u2040\u2054\u2060-\u206F]/, /[\u2070-\u20CF\u2100-\u218F\u2460-\u24FF\u2776-\u2793]/, /[\u2C00-\u2DFF\u2E80-\u2FFF]/, /[\u3004-\u3007\u3021-\u302F\u3031-\u303F\u3040-\uD7FF]/, /[\uF900-\uFD3D\uFD40-\uFDCF\uFDF0-\uFE1F\uFE30-\uFE44]/, /[\uFE47-\uFEFE\uFF00-\uFFFD]/);
  var Y = f(P, /\d/, /[\u0300-\u036F\u1DC0-\u1DFF\u20D0-\u20FF\uFE20-\uFE2F]/), X = d(P, Y, "*"), a = d(/[A-Z]/, Y, "*"), g = ["autoclosure", d(/convention\(/, f("swift", "block", "c"), /\)/), "discardableResult", "dynamicCallable", "dynamicMemberLookup", "escaping", "frozen", "GKInspectable", "IBAction", "IBDesignable", "IBInspectable", "IBOutlet", "IBSegueAction", "inlinable", "main", "nonobjc", "NSApplicationMain", "NSCopying", "NSManaged", d(/objc\(/, X, /\)/), "objc", "objcMembers", "propertyWrapper", 
  "requires_stored_property_inits", "testable", "UIApplicationMain", "unknown", "usableFromInline"], e = "iOS iOSApplicationExtension macOS macOSApplicationExtension macCatalyst macCatalystApplicationExtension watchOS watchOSApplicationExtension tvOS tvOSApplicationExtension swift".split(" ");
  return function(l) {
    var m = {match:/\s+/, relevance:0}, v = l.COMMENT("/\\*", "\\*/", {contains:["self"]});
    v = [l.C_LINE_COMMENT_MODE, v];
    var n = {className:"keyword", begin:d(/\./, c(f.apply(null, [].concat($jscomp.arrayFromIterable(h), $jscomp.arrayFromIterable(k))))), end:f.apply(null, [].concat($jscomp.arrayFromIterable(h), $jscomp.arrayFromIterable(k))), excludeBegin:!0}, y = {match:d(/\./, f.apply(null, $jscomp.arrayFromIterable(x))), relevance:0}, D = x.filter(function(H) {
      return "string" == typeof H;
    }).concat(["_|0"]), C = {variants:[{className:"keyword", match:f.apply(null, [].concat($jscomp.arrayFromIterable(x.filter(function(H) {
      return "string" != typeof H;
    }).concat(u).map(b)), $jscomp.arrayFromIterable(k)))}]};
    D = {$pattern:f(/\b\w+/, /#\w+/), keyword:D.concat(w), literal:t};
    n = [n, y, C];
    y = [{match:d(/\./, f.apply(null, $jscomp.arrayFromIterable(B))), relevance:0}, {className:"built_in", match:d(/\b/, f.apply(null, $jscomp.arrayFromIterable(B)), /(?=\()/)}];
    var p = {match:/->/, relevance:0};
    C = [p, {className:"operator", relevance:0, variants:[{match:R}, {match:"\\.(\\.|" + ca + ")+"}]}];
    var z = {className:"number", relevance:0, variants:[{match:"\\b(([0-9]_*)+)(\\.(([0-9]_*)+))?([eE][+-]?(([0-9]_*)+))?\\b"}, {match:"\\b0x(([0-9a-fA-F]_*)+)(\\.(([0-9a-fA-F]_*)+))?([pP][+-]?(([0-9]_*)+))?\\b"}, {match:/\b0o([0-7]_*)+\b/}, {match:/\b0b([01]_*)+\b/}]}, I = function(H) {
      H = void 0 === H ? "" : H;
      return {className:"subst", variants:[{match:d(/\\/, H, /[0\\tnr"']/)}, {match:d(/\\/, H, /u\{[0-9a-fA-F]{1,8}\}/)}]};
    }, W = function(H) {
      return {className:"subst", label:"interpol", begin:d(/\\/, void 0 === H ? "" : H, /\(/), end:/\)/};
    }, ba = function(H) {
      H = void 0 === H ? "" : H;
      var fa = d(H, /"""/), Z = d(/"""/, H), aa = I(H);
      var T = H;
      T = {className:"subst", match:d(/\\/, void 0 === T ? "" : T, /[\t ]*(?:[\r\n]|\r\n)/)};
      return {begin:fa, end:Z, contains:[aa, T, W(H)]};
    }, M = function(H) {
      H = void 0 === H ? "" : H;
      return {begin:d(H, /"/), end:d(/"/, H), contains:[I(H), W(H)]};
    };
    ba = {className:"string", variants:[ba(), ba("#"), ba("##"), ba("###"), M(), M("#"), M("##"), M("###")]};
    var ea = {match:d(/`/, X, /`/)};
    M = [ea, {className:"variable", match:/\$\d+/}, {className:"variable", match:"\\$" + Y + "+"}];
    var q = [{match:/(@|#)available/, className:"keyword", starts:{contains:[{begin:/\(/, end:/\)/, keywords:e, contains:[].concat($jscomp.arrayFromIterable(C), [z, ba])}]}}, {className:"keyword", match:d(/@/, f.apply(null, $jscomp.arrayFromIterable(g)))}, {className:"meta", match:d(/@/, X)}], E = {match:c(/\b[A-Z]/), relevance:0, contains:[{className:"type", match:d(/(AV|CA|CF|CG|CI|CL|CM|CN|CT|MK|MP|MTK|MTL|NS|SCN|SK|UI|WK|XC)/, Y, "+")}, {className:"type", match:a, relevance:0}, {match:/[?!]+/, 
    relevance:0}, {match:/\.\.\./, relevance:0}, {match:d(/\s+&\s+/, c(a)), relevance:0}]};
    p = {begin:/</, end:/>/, keywords:D, contains:[].concat($jscomp.arrayFromIterable(v), $jscomp.arrayFromIterable(n), $jscomp.arrayFromIterable(q), [p, E])};
    E.contains.push(p);
    p = {begin:/\(/, end:/\)/, relevance:0, keywords:D, contains:["self", {match:d(X, /\s*:/), keywords:"_|0", relevance:0}].concat($jscomp.arrayFromIterable(v), $jscomp.arrayFromIterable(n), $jscomp.arrayFromIterable(y), $jscomp.arrayFromIterable(C), [z, ba], $jscomp.arrayFromIterable(M), $jscomp.arrayFromIterable(q), [E])};
    ea = {beginKeywords:"func", contains:[{className:"title", match:f(ea.match, X, R), endsParent:!0, relevance:0}, m]};
    var J = {begin:/</, end:/>/, contains:[].concat($jscomp.arrayFromIterable(v), [E])}, N = {begin:/\(/, end:/\)/, keywords:D, contains:[{begin:f(c(d(X, /\s*:/)), c(d(X, /\s+/, X, /\s*:/))), end:/:/, relevance:0, contains:[{className:"keyword", match:/\b_\b/}, {className:"params", match:X}]}].concat($jscomp.arrayFromIterable(v), $jscomp.arrayFromIterable(n), $jscomp.arrayFromIterable(C), [z, ba], $jscomp.arrayFromIterable(q), [E, p]), endsParent:!0, illegal:/["']/};
    ea = {className:"function", match:c(/\bfunc\b/), contains:[ea, J, N, m], illegal:[/\[/, /%/]};
    m = {className:"function", match:/\b(subscript|init[?!]?)\s*(?=[<(])/, keywords:{keyword:"subscript init init? init!", $pattern:/\w+[?!]?/}, contains:[J, N, m], illegal:/\[|%/};
    J = {beginKeywords:"operator", end:l.MATCH_NOTHING_RE, contains:[{className:"title", match:R, endsParent:!0, relevance:0}]};
    N = {beginKeywords:"precedencegroup", end:l.MATCH_NOTHING_RE, contains:[{className:"title", match:a, relevance:0}, {begin:/{/, end:/}/, relevance:0, endsParent:!0, keywords:[].concat($jscomp.arrayFromIterable(r), $jscomp.arrayFromIterable(t)), contains:[E]}]};
    for (var O = $jscomp.makeIterator(ba.variants), K = O.next(); !K.done; K = O.next()) {
      K = K.value.contains.find(function(H) {
        return "interpol" === H.label;
      });
      K.keywords = D;
      var A = [].concat($jscomp.arrayFromIterable(n), $jscomp.arrayFromIterable(y), $jscomp.arrayFromIterable(C), [z, ba], $jscomp.arrayFromIterable(M));
      K.contains = [].concat($jscomp.arrayFromIterable(A), [{begin:/\(/, end:/\)/, contains:["self"].concat($jscomp.arrayFromIterable(A))}]);
    }
    return {name:"Swift", keywords:D, contains:[].concat($jscomp.arrayFromIterable(v), [ea, m, {className:"class", beginKeywords:"struct protocol class extension enum", end:"\\{", excludeEnd:!0, keywords:D, contains:[l.inherit(l.TITLE_MODE, {begin:/[A-Za-z$_][\u00C0-\u02B80-9A-Za-z$_]*/})].concat($jscomp.arrayFromIterable(n))}, J, N, {beginKeywords:"import", end:/$/, contains:[].concat($jscomp.arrayFromIterable(v)), relevance:0}], $jscomp.arrayFromIterable(n), $jscomp.arrayFromIterable(y), $jscomp.arrayFromIterable(C), 
    [z, ba], $jscomp.arrayFromIterable(M), $jscomp.arrayFromIterable(q), [E, p])};
  };
}());
hljs.registerLanguage("dos", function() {
  return function(c) {
    var d = c.COMMENT(/^\s*@?rem\b/, /$/, {relevance:10});
    return {name:"Batch file (DOS)", aliases:["bat", "cmd"], case_insensitive:!0, illegal:/\/\*/, keywords:{keyword:"if else goto for in do call exit not exist errorlevel defined equ neq lss leq gtr geq", built_in:"prn nul lpt3 lpt2 lpt1 con com4 com3 com2 com1 aux shift cd dir echo setlocal endlocal set pause copy append assoc at attrib break cacls cd chcp chdir chkdsk chkntfs cls cmd color comp compact convert date dir diskcomp diskcopy doskey erase fs find findstr format ftype graftabl help keyb label md mkdir mode more move path pause print popd pushd promt rd recover rem rename replace restore rmdir shift sort start subst time title tree type ver verify vol ping net ipconfig taskkill xcopy ren del"}, 
    contains:[{className:"variable", begin:/%%[^ ]|%[^ ]+?%|![^ ]+?!/}, {className:"function", begin:"^\\s*[A-Za-z._?][A-Za-z0-9_$#@~.?]*(:|\\s+label)", end:"goto:eof", contains:[c.inherit(c.TITLE_MODE, {begin:"([_a-zA-Z]\\w*\\.)*([_a-zA-Z]\\w*:)?[_a-zA-Z]\\w*"}), d]}, {className:"number", begin:"\\b\\d+", relevance:0}, d]};
  };
}());
hljs.registerLanguage("objectivec", function() {
  return function(c) {
    var d = /[a-zA-Z@][a-zA-Z0-9_]*/, f = {$pattern:d, keyword:"@interface @class @protocol @implementation"};
    return {name:"Objective-C", aliases:["mm", "objc", "obj-c", "obj-c++", "objective-c++"], keywords:{$pattern:d, keyword:"int float while char export sizeof typedef const struct for union unsigned long volatile static bool mutable if do return goto void enum else break extern asm case short default double register explicit signed typename this switch continue wchar_t inline readonly assign readwrite self @synchronized id typeof nonatomic super unichar IBOutlet IBAction strong weak copy in out inout bycopy byref oneway __strong __weak __block __autoreleasing @private @protected @public @try @property @end @throw @catch @finally @autoreleasepool @synthesize @dynamic @selector @optional @required @encode @package @import @defs @compatibility_alias __bridge __bridge_transfer __bridge_retained __bridge_retain __covariant __contravariant __kindof _Nonnull _Nullable _Null_unspecified __FUNCTION__ __PRETTY_FUNCTION__ __attribute__ getter setter retain unsafe_unretained nonnull nullable null_unspecified null_resettable class instancetype NS_DESIGNATED_INITIALIZER NS_UNAVAILABLE NS_REQUIRES_SUPER NS_RETURNS_INNER_POINTER NS_INLINE NS_AVAILABLE NS_DEPRECATED NS_ENUM NS_OPTIONS NS_SWIFT_UNAVAILABLE NS_ASSUME_NONNULL_BEGIN NS_ASSUME_NONNULL_END NS_REFINED_FOR_SWIFT NS_SWIFT_NAME NS_SWIFT_NOTHROW NS_DURING NS_HANDLER NS_ENDHANDLER NS_VALUERETURN NS_VOIDRETURN", 
    literal:"false true FALSE TRUE nil YES NO NULL", built_in:"BOOL dispatch_once_t dispatch_queue_t dispatch_sync dispatch_async dispatch_once"}, illegal:"</", contains:[{className:"built_in", begin:"\\b(AV|CA|CF|CG|CI|CL|CM|CN|CT|MK|MP|MTK|MTL|NS|SCN|SK|UI|WK|XC)\\w+"}, c.C_LINE_COMMENT_MODE, c.C_BLOCK_COMMENT_MODE, c.C_NUMBER_MODE, c.QUOTE_STRING_MODE, c.APOS_STRING_MODE, {className:"string", variants:[{begin:'@"', end:'"', illegal:"\\n", contains:[c.BACKSLASH_ESCAPE]}]}, {className:"meta", begin:/#\s*[a-z]+\b/, 
    end:/$/, keywords:{"meta-keyword":"if else elif endif define undef warning error line pragma ifdef ifndef include"}, contains:[{begin:/\\\n/, relevance:0}, c.inherit(c.QUOTE_STRING_MODE, {className:"meta-string"}), {className:"meta-string", begin:/<.*?>/, end:/$/, illegal:"\\n"}, c.C_LINE_COMMENT_MODE, c.C_BLOCK_COMMENT_MODE]}, {className:"class", begin:"(" + f.keyword.split(" ").join("|") + ")\\b", end:/(\{|$)/, excludeEnd:!0, keywords:f, contains:[c.UNDERSCORE_TITLE_MODE]}, {begin:"\\." + c.UNDERSCORE_IDENT_RE, 
    relevance:0}]};
  };
}());
hljs.registerLanguage("coffeescript", function() {
  var c = "as in of if for while finally var new function do return void else break catch instanceof with throw case default try switch continue typeof delete let yield const class debugger async await static import from export extends".split(" "), d = "true false null undefined NaN Infinity".split(" "), f = [].concat("setInterval setTimeout clearInterval clearTimeout require exports eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape".split(" "), 
  "arguments this super console window document localStorage module global".split(" "), "Intl DataView Number Math Date String RegExp Object Function Boolean Error Symbol Set Map WeakSet WeakMap Proxy Reflect JSON Promise Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array Float32Array Array Uint8Array Uint8ClampedArray ArrayBuffer BigInt64Array BigUint64Array BigInt".split(" "), "EvalError InternalError RangeError ReferenceError SyntaxError TypeError URIError".split(" "));
  return function(b) {
    var h = {keyword:c.concat("then unless until loop by when and or is isnt not".split(" ")).filter((k = ["var", "const", "let", "function", "static"], function(r) {
      return !k.includes(r);
    })), literal:d.concat(["yes", "no", "on", "off"]), built_in:f.concat(["npm", "print"])}, k, u = {className:"subst", begin:/#\{/, end:/\}/, keywords:h}, x = [b.BINARY_NUMBER_MODE, b.inherit(b.C_NUMBER_MODE, {starts:{end:"(\\s*/)?", relevance:0}}), {className:"string", variants:[{begin:/'''/, end:/'''/, contains:[b.BACKSLASH_ESCAPE]}, {begin:/'/, end:/'/, contains:[b.BACKSLASH_ESCAPE]}, {begin:/"""/, end:/"""/, contains:[b.BACKSLASH_ESCAPE, u]}, {begin:/"/, end:/"/, contains:[b.BACKSLASH_ESCAPE, 
    u]}]}, {className:"regexp", variants:[{begin:"///", end:"///", contains:[u, b.HASH_COMMENT_MODE]}, {begin:"//[gim]{0,3}(?=\\W)", relevance:0}, {begin:/\/(?![ *]).*?(?![\\]).\/[gim]{0,3}(?=\W)/}]}, {begin:"@[A-Za-z$_][0-9A-Za-z$_]*"}, {subLanguage:"javascript", excludeBegin:!0, excludeEnd:!0, variants:[{begin:"```", end:"```"}, {begin:"`", end:"`"}]}];
    u.contains = x;
    u = b.inherit(b.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*"});
    var t = {className:"params", begin:"\\([^\\(]", returnBegin:!0, contains:[{begin:/\(/, end:/\)/, keywords:h, contains:["self"].concat(x)}]};
    return {name:"CoffeeScript", aliases:["coffee", "cson", "iced"], keywords:h, illegal:/\/\*/, contains:x.concat([b.COMMENT("###", "###"), b.HASH_COMMENT_MODE, {className:"function", begin:"^\\s*[A-Za-z$_][0-9A-Za-z$_]*\\s*=\\s*(\\(.*\\)\\s*)?\\B[-=]>", end:"[-=]>", returnBegin:!0, contains:[u, t]}, {begin:/[:\(,=]\s*/, relevance:0, contains:[{className:"function", begin:"(\\(.*\\)\\s*)?\\B[-=]>", end:"[-=]>", returnBegin:!0, contains:[t]}]}, {className:"class", beginKeywords:"class", end:"$", 
    illegal:/[:="\[\]]/, contains:[{beginKeywords:"extends", endsWithParent:!0, illegal:/[:="\[\]]/, contains:[u]}, u]}, {begin:"[A-Za-z$_][0-9A-Za-z$_]*:", end:":", returnBegin:!0, returnEnd:!0, relevance:0}])};
  };
}());

