/*!
 *  ModPopDialog — Sophisticated JavaScript modal dialog box
 *
 *  @file      20190502°0211 www.trekta.biz/svn/modpopdev/trunk/modpopdialog/modpopdialog.js
 *  @version   v0.1.6 — 20220307°1111
 *  @copyright © 2019 - 2023 Norbert C. Maier
 *  @authors   Norbert C. Maier
 *  @license   BSD 3-Clause
 */
/**
 *  @encoding  UTF-8-without-BOM
 *  @status    Starts working
 */

// Options for JSHint https://jshint.com/ [seq 20190504°0121]
/* jshint laxbreak : true, laxcomma : true */

// Options for JSLint https://jslint.com/ [seq 20190504°0131]
/*jslint
    browser, devel, long, single, this, white
*/
/*
    browser = Assume a browser (see https://jslint.com/help.html#browser)
    devel   = Assume in development (see https://jslint.com/help.html)
    long    = Tolerate long source lines (more than 80 characters)
    single  = Tolerate single quote strings
    this     = Tolerate this
    white   = Tolerate whitespace mess
*/

/**
 *  This function shall constitute class Mpd
 *
 *  @id 20190502°0221
 *  @callers • E.g. func 20190426°0121 Daf.Utlis.showModPop_continue
 *  @constructor —
 */
Mpd = function ()
{
    'use strict';

    /**
     *  This method closes the popup window and shall clean up ..
     *
     *  @id func 20190503°0311
     *  @callers • Many
     *  @param oCloser {object} — Shall hold the properties 'Action' and 'Actor'
     *  @returns {undefined} — Nothing
     */
    this.CloseDialogWindow = function (oCloser)
    {
        // () Set up the result object
        Mpd.Result.Closer = oCloser;
        Mpd.Result = this.CloseDialogWindox_CollectResult(Mpd.Result);

        // () Debug notification
        if (Mpd.Dbg.bDbgMsg_General) {
            Mpd.WriteDbgMsg('<b>CloseDialogWindow</b>' + Mpd.GetResultDebugString(Mpd.Result));
        }

        // () Original sequence [seq 20190503°0325]
        const elMpdFrameParentDiv = document.getElementById(Mpd.Ht.sEl_Modpop_Frame_Parent_Div);
        const divOverlay = document.getElementById(Mpd.Ht.Els[Mpd.Ht.iEl_Modpop_Container_Div][Mpd.Ht.iNdxId]);

        // (X) Cleanup [seq 20190503°0327]
        // (X.1) Stop observing [line 20190503°0331]
        if (oObserver !== null) {                                      // Feature detect
            oObserver.disconnect();                                    // IE9/IE10 complain 'unable to get property disconnect'
        }

        // (X.2) Remove handlers created in seq 20190503°0125 [seq 20190503°0335]
        document.removeEventListener("click", this.Handle_Clicks, false);
        document.removeEventListener("keydown", this.Handle_Keys, false);
        document.removeEventListener("focusin", this.Handle_Focusin, false);

        // (X.3) Remove the complete popup element [seq 20190503°0337]
        elMpdFrameParentDiv.parentNode.removeChild(elMpdFrameParentDiv);
        divOverlay.parentNode.removeChild(divOverlay);
        this.elMpdMasterDiv.parentNode.removeChild(this.elMpdMasterDiv);  // Empirical try [line 20220305°1231] Does not help nor hurt against issue 20220305°1151 'Pressing enter does not close dialog'
        this.elMpdMasterDiv = null;                                    // Proforma only?

        // (X.4) After all is done here, continue on caller´ side [seq 20190503°0343]
        if (Mpd.aInsts[Mpd.aInsts.length - 1].OnPopUpCloseCallBack) {
            Mpd.aInsts[Mpd.aInsts.length - 1].OnPopUpCloseCallBack(Mpd.Result);
        }
    };

    /**
     *  This method extracts a result object from an HTML page with input fields
     *
     *  @id 20220302°1511
     *  @ref dmitripavlutin.com/check-if-object-has-property-javascript/ [ref 20220302°1532]
     *  @callers Only above CloseDialogWindow
     *  @returns {object} — The wanted result object
     */
    this.CloseDialogWindox_CollectResult = function (oResult)
    {
        // Is the result area a dynamic creation or an iFrame? [seq 20220303°0921]
        //  Inside the 'Modpop_Window_Td' element exists either (1) a 'Modpop_Text_Div'
        //  if it is dynamic content or (2) a 'Modpop_Window_Frame' if it is an iFrame.
        let elTarget = document.getElementById('Modpop_Text_Div');
        if (elTarget === null) {
            let elIframe = document.getElementById('Modpop_Window_Frame');
            elTarget = elIframe.contentWindow.document.body;           // Access from carrier page to iFrame page (see issue 20220226°1021)
        }

        // Collect values [seq 20220302°1521] Compare seq 20220301°2151
        //let sDbgHtml = elTarget.innerHTML;                           // Debug
        let elsInput = elTarget.querySelectorAll("input");
        elsInput.forEach(item => {
            if (item.id.length > 0) {
                let elsLbl = elTarget.querySelectorAll("label[for=" + item.id + "]");
                let oFields = {};
                if (elsLbl[0] !== undefined) {
                    oFields['Name'] = elsLbl[0].innerHTML;
                    oFields['Value'] = item.value;
                    oFields['Id'] = item.id;
                }
                oResult['Fields'].push(oFields);
            }
        });

        // Optional debug notification [seq 20220302°1621]
        if (Mpd.Dbg.bMsg_CollectResult_oFields) {
            let sMsg = 'CollectResult — Fields :';
            let sIds = '<span style="font-style:italic; font-size:small; padding-left:1.1em; color:SlateGray;">Ids :';
            if ('Fields' in oResult) {
                oResult.Fields.forEach( field => {
                    sMsg += ' • ' + field.Name + ' = ' + field.Value;
                    sIds += ' • ' + field.Id;
                });
            }
            if (Mpd.Dbg.bMsg_CollectResult_Ids) {                      // [condition 20220302°1611]
                sMsg += sIds + '</span>';
            }
            Mpd.WriteDbgMsg(sMsg);
        }

        return oResult;
    }

    /**
     *  This public non-static function builds the dialog DIV and sets it up
     *
     *  @id 20190503°0111
     *  @callers • func 20190502°0231 ShowModalDialog
     *  @param sTitle {string} The title of the box
     *  @param sMessage {string} The message to be shown
     *  @param sIframSrc {string} Empty if simple dialog, URL if iframe
     *  @param {number} iHeight — The height of the box (so far in pixel)
     *  @param {number} iWidth — The width of the box (so far in pixel)
     *  @param {array} arButtons — Array with the button labels
     *  @returns {undefined} —
     */
    this.CreateDialogWindow = function(sTitle, sMessage, sIframSrc, iWidth, iHeight, arButtons)
    {
        // () Debug notification [seq 20190503°0121]
        if (Mpd.Dbg.bDbgMsg_General) {
            Mpd.WriteDbgMsg('CreateDialogWindow : title = ' + sTitle);  // Why says WebStorm 'Unresolved function ..'?! [issue 20220307°0951]
        }

        // Provide main div [seq 20190503°0123]
        this.elMpdMasterDiv = document.createElement('div');           // This is the useless(?) box in the left bottom page corner
        this.elMpdMasterDiv.id = 'Mpd_Master_Div';
        this.elMpdMasterDiv.style.border = '7px dashed MidnightBlue';
        this.elMpdMasterDiv.style.width = '1.1em';
        this.elMpdMasterDiv.style.height = '1.1em';

        this.elMpdMasterDiv.innerHTML = Mpd.sOverLayHTML;
        document.body.appendChild(this.elMpdMasterDiv);

        // Attach mouse and keyboard handlers [seq 20190503°0125]
        //  Remember — On object cleanup remove them all in seq 20190503°0335
        document.addEventListener("click", this.Handle_Clicks, false);
        document.addEventListener("keydown", this.Handle_Keys, false);
        document.addEventListener("focusin", this.Handle_Focusin, false);  // 'focusin' bubbles, as opposed to 'focus' [line 20220306°1041]

        // Provide content DIV, either message or iframe [seq 20190503°0127]
        let eDvContent = document.getElementById(Mpd.Ht.sEl_Modpop_Window_Td);
        let eDvInlay = document.createElement('div');

        // Determine what kind of inlay is given [seq 20220302°1311]
        //  Given is (1) either a dynamic fields object (2) or a custom form to be put into an iFrame
        let bIsDynamic = true;                                         // Predetermination for dynamic generation
        if (typeof sIframSrc === 'string') {
            bIsDynamic = false;                                        // It is the URL of a custom form for an iFrame
        }

        // Generate the inlay HTML
        let sHt = '';
        if (bIsDynamic) {                                              // [condition 20190503°0417]
            sHt = this.CreateDialogWindow_Dynamic(sIframSrc, arButtons);  // Here, sIframSrc is not a string but an object!
        }
        else {
            sHt = this.CreateDialogWindow_Custom();
        }

        // Process the above generated payload
        eDvInlay.innerHTML = sHt;
        eDvContent.appendChild(eDvInlay)

        // Process debug borders
        if (bIsDynamic) {                                              // For the iFrame, it is done via the onLoad event
            Mpd.RenderDebugBorders();                                  // Earliest sensible moment for painting [line 20220305°1011] — // Why says WebStorm 'Unresolved function ..'?! [issue 20220307°0951`02]
        }

        // Prologue [seq 20190503°0131]
        elDivBoxParent = document.getElementById(Mpd.Ht.sEl_Modpop_Frame_Parent_Div);
        let divOverlay = document.getElementById(Mpd.Ht.Els[Mpd.Ht.iEl_Modpop_Container_Div][Mpd.Ht.iNdxId]);

        // Calculate position [seq 20190503°0133 family 20190503°0821]
        //  Sequence family containing former window.screen.availWidth
        let left = (window.innerWidth - iWidth) / 2;                   // window.screen.availWidth is not what we need here
        let top = (window.innerHeight - iHeight) / 5;                  // 2
        left += window.pageXOffset;
        top += window.pageYOffset;

        // Calculate size [seq 20190503°0135]
        document.getElementById(Mpd.Ht.sEl_Modpop_Window_Buttons_Tr).style.display = "none";
        document.getElementById(Mpd.Ht.sEl_Modpop_Titlebar_Span).innerHTML = sTitle;
        divOverlay.style.top = "0px";
        divOverlay.style.left = "0px";

        // [seq 20190503°0137]
        // See note 20190503°0711 'on element sizes'
        let maxWidth = Math.max ( document.body.offsetWidth
                                 , document.body.scrollWidth
                                  , document.documentElement.clientWidth
                                   , document.documentElement.offsetWidth
                                    , document.documentElement.scrollWidth
                                     );
        let maxHeight = Math.max ( document.body.offsetHeight
                                  , document.body.scrollHeight
                                   , document.documentElement.clientHeight
                                    , document.documentElement.offsetHeight
                                     , document.documentElement.scrollHeight
                                      );
        divOverlay.style.height = maxHeight + "px";
        divOverlay.style.width = maxWidth - 2 + "px";
        divOverlay.style.display = "";

        // Calculate iframe position and size [seq 20190503°0141]
        elDivBoxParent.style.display = "";                             // WebStorm warning 'Unresolved variable style' because the object is of type 'Element' instead 'HTMLElement'. How can we cast it? [issue 20220226°0811 low priority]
        elDivBoxParent.style.left = left + "px";
        elDivBoxParent.style.top = top + "px";
        elDivBoxParent.style.width = iWidth + "px";
        elDivBoxParent.style.height = iHeight + "px";

        // Process buttons [seq 20190503°0143]
        let tdOverLay = document.getElementById(Mpd.Ht.sEl_Modpop_Window_Td);
        if (bIsDynamic) {                                              // [condition 20190503°0721]
            // Process built-in box [seq 20190503°0723]
            document.getElementById(Mpd.Ht.sEl_Modpop_Window_Buttons_Tr).style.display = "";
            document.getElementById(Mpd.Ht.sEl_Modpop_Text_Span).innerHTML = sMessage;
            document.getElementById(Mpd.Ht.sEl_Modpop_Text_Div).style.display = "";
            document.getElementById("Modpop_Loading_Span").style.display = "none";
            document.getElementById("Modpop_Window_Td").style.textAlign = "left";  // Added 20220305°1141
        }
        else {
            // Process iframe box [seq 20190503°0731]
            let elMpdFrameParentDiv = document.getElementById(Mpd.Ht.sEl_Modpop_Frame_Parent_Div);
            let elIframe = document.getElementById(Mpd.Ht.sEl_Modpop_Window_Frame);
            let elModpopWindowTd = document.getElementById(Mpd.Ht.sEl_Modpop_Window_Td);  // Empirical [var 20220226°0841]
            tdOverLay.style.height = (elMpdFrameParentDiv.offsetHeight - 20).toString() + "px";
            tdOverLay.style.width = (elMpdFrameParentDiv.offsetWidth - 2).toString() + "px";
            document.getElementById(Mpd.Ht.sEl_Modpop_Loading_Span).style.display = "";  // Before, it was 'none'
            elIframe.style.height = (elModpopWindowTd.offsetHeight - 60).toString() + "px";
            elIframe.style.width = (elModpopWindowTd.offsetWidth - 2).toString() + "px";

            // Load the form into the iframe element [line 20190503°0733]
            setTimeout ( function() { elIframe.src = sIframSrc; } , 7);  // Seven just for fun, was formerly 1
        }

        // (X) Setup resize observer [seq 20190503°0151]
        // Function MutationObserver exists in IE11 and higher
        //  Sequence after ref 20190428°0233 'mdn → MutationObserver'
        //  See issue 20190503°0547 'make iframe resizable'
        // (X.1) Feature detect [seq 20190503°0152]
        if (typeof MutationObserver === 'function') {

            // (X.2) Prologue [seq 20190503°0153]
            let targetNode = document.getElementById(Mpd.Ht.sEl_Modpop_Frame_Parent_Div);
            let config = { attributes: true };

            // (X.3) Create observer instance linked to callback function [line 20190503°0155]
            //  On dialog end, the observer will be destroyed, see line 20190503°0331
            oObserver = new MutationObserver(MutationObserved);

            // (X.4) Start observing target node for configured mutations [line 20190503°0157]
            oObserver.observe(targetNode, config);
        }

        // Maintain focus [seq 20220306°1221]
        // Note. For iFrame custom forms, this is done in OnIframeLoaded via timer
        if (bIsDynamic) {
            this.OnDialogOpen_SetFocus(true);
        }
    };

    /**
     *  This wannabe private non-static function builds the dynamic input area
     *
     *  @id 20220302°1321
     *  @callers • Only func 20190502°0231 CreateDialogWindow
     *  @param {object} oFields — The fields object
     *  @param {array} arButtons — The button definitions
     *  @returns {string} — The wanted HTML content
     */
    this.CreateDialogWindow_Dynamic = function(oFields, arButtons)
    {
        // Start the HTML fragment
        let sHt =  `\n    <div id="Modpop_Text_Div" style="margin-top:0.7em; padding:0.7em; display:none;">`
                 + `\n      <span id="Modpop_Text_Span"></span>`
                  ;

        // Process the optional input fields object [seq 20220302°1341]
        let sAutofocus = ' autofocus';                                 // Does not help [feature 20220306°0731 Use autofocus on field] Try to solve issue 20220305°1155 'Pressing Space doubles the buttons'
        if (oFields) {
            sHt += '\n    <table>';
            const arKeys = Object.keys(oFields);
            arKeys.forEach(sKey => {
                let sInputId = Mpd.Conv_ObjKey2InputId(sKey);
                sHt += `\n      <tr>`
                     + `\n        <td>`
                     + `\n               <label for="${sInputId}">${sKey}</label>`
                     + `\n        </td>`
                     + `\n        <td>`
                     //+ `\n               <input type="text" id="${sInputId}" value="${oFields[sKey]}"${sAutofocus}>`
                     + `\n               <input type="text" id="${sInputId}" value="${oFields[sKey]}">`
                     + `\n        </td>`
                     + `\n      </tr>`
                      ;
                sAutofocus = '';
            });
            sHt += `\n    </table>`
        }

        // Complete the HTML fragment
        sHt += `\n  </div>`;

        // Put buttons into the button container [seq 20220226°0831]
        let bAutofocus = true;                                         // Does not help [feature 20220306°0733 Use autofocus on button] Try to solve issue 20220305°1155 'Pressing Space doubles the buttons'
        let elConForBut = document.getElementById(Mpd.Ht.sEl_Container_For_Buttons);
        for (let iNdx = 0; iNdx < arButtons.length; iNdx++) {
            let elBut = document.createElement('input');
            elBut.type = 'button';
            elBut.id = 'modpop_window_button_' + iNdx.toString()
            elBut.onclick = Function ("Mpd.aInsts[Mpd.aInsts.length - 1].CloseDialogWindow({Action:'" + arButtons[iNdx] + "', Actor:'button'});");
            elBut.value = arButtons[iNdx];
            elBut.style.padding = '3px';
            elBut.style.border = '1 solid #dcdcdc';
            elBut.style.backgroundColor = 'background-color: #f5f5f5';
//          elBut.autofocus = bAutofocus;
            elConForBut.appendChild(elBut);
            bAutofocus = false;
        }

        return sHt;
    }

    /**
     *  This wannabe private non-static function builds the custom form area
     *
     *  @id 20220302°1331
     *  @callers • Only func 20190502°0231 CreateDialogWindow
     *  @returns {string} — The wanted HTML content
     */
    this.CreateDialogWindow_Custom = function()
    {
        // [line 20190503°0421]
        let sHt = '    <div id="Iframe_Wrapper_Div"'                   // '5x dashed LimeGreen'
                 + '          style="/* border:5px dashed LimeGreen; */'  // The LimeGreen does not appear!?
                 + '           width:94%;' // Behaviour-critical value, 93..95 % seems useful, 96..99%  goes crazy
                 + '            /* overflow:hidden; */ resize:both;'   // See issue 20190503°0547 'make iframe resizable'
                 + '         ">'
                 + '     <iframe onload="Mpd.aInsts[Mpd.aInsts.length - 1].OnIframeLoaded();"'
                 + '             style="/* border:5px solid Magenta; */'  // Debug
                 + '                 width:33%; height: 33%;"'         // This makes no difference // Display: block;' // Try to make it resizable [line 20190503°0543]
                 + '             id="Modpop_Window_Frame"'
                 + '             >'
                 + '     </iframe>'
                 + '    </div>'
                  ;
        return sHt;
    }

    /**
     *  This method .. serves the dragging logic
     *
     *  @id 20190503°0621
     *  @callers •
     *  @param evt {event} —
     *  @returns {undefined} —
     */
    this.DraggingDo = function(evt)
    {
        // [seq 20190503°0623]
        if (! dragging) {
            return;
        }

        // [seq 20190503°0625]
        let __div_frame_parent = document.getElementById(Mpd.Ht.sEl_Modpop_Frame_Parent_Div);
        let top = parseInt(__div_frame_parent.style.top.replace('px', ''));
        let left = parseInt(__div_frame_parent.style.left.replace('px', ''));
        let currentX = 0;
        let currentY = 0;

        // [seq 20190503°0631]
        if (typeof evt !== 'undefined' && typeof evt.pageX !== 'undefined') {
            currentX = evt.pageX;
            currentY = evt.pageY;
        }
        if (typeof evt !== 'undefined' && typeof evt.x !== 'undefined') {
            currentX = evt.x;
            currentY = evt.y;
        }

        // [seq 20190503°0633]
        if (currentY > dragTop) {
            top += currentY - dragTop;
        }
        else {
            top -= dragTop - currentY;
        }

        // [seq 20190503°0635]
        if (currentX > dragLeft) {
            left += currentX - dragLeft;
        }
        else {
            left -= dragLeft - currentX;
        }

        // [seq 20190503°0641]
        dragTop = currentY;
        dragLeft = currentX;
        try {
          __div_frame_parent.style.top = top + "px";
            __div_frame_parent.style.left = left + "px";
        }
        catch(ex) {
            window.alert('Error 20190503°0643 :\n\n' + ex.message);
        }
    };

    /**
     *  This method .. serves the dragging logic
     *
     *  @id 20190503°0611
     *  @callers •
     *  @param {event} evt —
     *  @param {object} oThis —
     *  @returns {undefined} — Nothing
     */
    this.DraggingStart = function(evt, oThis) {
        dragging = true;
        dragTop = evt.pageY;
        dragLeft = evt.pageX;
        oThis.style.cursor ='move';
    };

    /**
     *  This method .. serves the dragging logic
     *
     *  @id 20190503°0651
     *  @callers •
     *  @returns {undefined} —
     */
    this.DraggingStop = function() {
        dragging = false;
        document.getElementById(Mpd.Ht.sEl_Modpop_Titlebar_Tr).style.cursor ='default';
    };

    /**
     *  This function maintains the focus for a newly opened dialog,
     *   be it a dynamic form or an iFrame document
     *
     *  @id func 20220306°1211
     *  @callers • this.OnIframeLoaded • ..
     *  @param {boolean} bIsDynamic —
     *  @constructor
     */
    this.OnDialogOpen_SetFocus = function(bIsDynamic) {

        if (Mpd.Dbg.bDbgMsg_General) {
            Mpd.WriteDbgMsg_Focus('OnIframeLoaded 77/1');
        }

        let elIframe = document.getElementById('Modpop_Window_Frame');

        // Determine target differently in iFrames and in dynamic forms
        let elFramDoc = null;
        if (bIsDynamic) {
            elFramDoc = document.getElementById('Modpop_Frame_Parent_Div');
        }
        else {
            elFramDoc = elIframe.contentWindow.document;               // Access from carrier page to iFrame page (see issue 20220226°1021)
        }

        // The actual focussing work
        let elsFocus2 = elFramDoc.querySelectorAll('input[autofocus]');
        let elTarget = elsFocus2[0];
        if (! elTarget) {
            elsFocus2 = elFramDoc.querySelectorAll('input');
            elTarget = elsFocus2[0];
        }
        if (elTarget) {
            elTarget.focus();
            if (elTarget.select) {                                     // E.g. buttons do not support selection
                elTarget.select();                                     // Equivalent to "el.setSelectionRange(0, el.value.length)"
            }
        }

        // Debug notification
        if (Mpd.Dbg.bDbgMsg_General) {
            Mpd.WriteDbgMsg_Focus('OnIframeLoaded 77/2');
        }
    };

    /*
    issue 20220305°1131 'OnIframeLoaded is executed twice'
    matter : OnIframeLoaded is executed twice. Why is this so? How can I prevent it?
    location : Func 20190503°0551 this.OnIframeLoaded
    status : Open
    */

    /**
     *  This event handler is called after the iFrame has completely loaded
     *
     *  @id func 20190503°0551
     *  @see ref 20190503°0555 'stackoverflow → why are iframes inline elements by default?'
     *  @note Process issue 20220305°1131 'OnIframeLoaded is executed twice'
     *  @callers •  The iframe onload event
     *  @returns {undefined} —
     */
    this.OnIframeLoaded = function() {

        // [seq 20220305°1051]
        // Here might be the right moment to fix issue 20220226°0851 'Tiny misalignment'
        setTimeout ( function() {
            Mpd.RenderDebugBorders();

            // Bad workaround for issue 20220226°0851 'Tiny misalignment' [seq 20220305°1121]
            let el35 = document.getElementById('Modpop_Frame_Parent_Div');
            let sStyleWidth = el35.style.width;
            let iWidth = parseInt(sStyleWidth.replace('px', ''));
            el35.style.width = (iWidth + 16).toString() + 'px';        // 16 // This seems good enough

            // Debug notification
            if (Mpd.bDbgMsg_General) {
                let s36 = "OnIframeLoaded 7ms — Width = " + sStyleWidth + '..' + el35.style.width;
                Mpd.WriteDbgMsg(s36);
            }

        } , 7);                                                        // 777, 11

        // Spinner is ready
        document.getElementById(Mpd.Ht.sEl_Modpop_Loading_Span).style.display = "none";

        // Call the focus setting routine [seq 20220306°0921] This partially solves issue 20220306°0911 'Focus element in iFrame'
        setTimeout ( function() {
            Mpd.aInsts[Mpd.aInsts.length - 1].OnDialogOpen_SetFocus(false);
        } , 77);                                                       // 7 is too short, 111 works, 77 works
    };

    /**
     *  This public nonstatic method toggles the maximized state of the dialog box
     *
     *  @id 20190503°0731
     *  @callers • onclick event of div element Modpop_Window_Max_Span
     *  @returns {undefined} —
     */
    this.ToggleMaximized = function() {

        // Prologue [line 20190503°0733]
        let elBtnMax = document.getElementById(Mpd.Ht.sEl_Modpop_Window_Max_Span);

        // Toggle pos and size [condition 20190503°0735]
        if (! bMaximized) {

            // Maximize
            // Save current size [seq 20190503°0741]
            iCurrentPosX = elDivBoxParent.offsetLeft;
            iCurrentPosY = elDivBoxParent.offsetTop;
            iCurrentWidth = elDivBoxParent.offsetWidth;
            iCurrentHeight = elDivBoxParent.offsetHeight;

            // Action [seq 20190503°0743 family 20190503°0821]
            //  Sequence family containing former window.screen.availWidth
            ResizeDialogBox ( window.innerWidth - 66
                , window.innerHeight - 66
            );

            // Update status [seq 20190503°0745]
            elBtnMax.innerHTML = '🗗';                                  // 🗗 = &#128471 [chg 20220226°0911`03] Formerly '[Norm]'
            bMaximized = true;
        }
        else {

            // Reset to normal size
            // Action [seq 20190503°0751]
            ResizeDialogBox(iCurrentWidth, iCurrentHeight);
            elDivBoxParent.style.left = iCurrentPosX + 'px';
            elDivBoxParent.style.top = iCurrentPosY + 'px';

            // Update status [seq 20190503°0753]
            elBtnMax.innerHTML = '🗖';                                  // 🗖 = &#128470 [chg 20220226°0911] Formerly '[Maxi]'
            bMaximized = false;
        }
    };

    /**
     *  This (wannabe private) nonstatic field stores the main parent DIV, representing
     *   the outer border of the dialog box, containing all the other elements
     *
     *  @note Shall mainly be replaced by 'elDivBoxParent'
     *  @id localvar 20190503°0423
     *  @type {Element|null} —                                         // Remark: 'Element' is not a JavaScript type
     */
    this.elMpdMasterDiv = null;

    /**
     *  This (wannabe private) nonstatic field shall store the callback to
     *   continue with, after closing the dialog
     *
     *  @id 20190503°0453
     *  @type {function|null} — The callback function given as parameter by the caller
     */
    this.OnPopUpCloseCallBack = null;

    /**
     *  This public method constitutes the event handler for mouse clicks
     *
     *  @id 20190503°0511
     *  @note See ref 20190425°0312 'Chirp Internet → simple modal feedback form'
     *  @param {event} evt — The ignited event
     *  @returns {undefined} — Nothing
     */
    this.Handle_Clicks = function(evt)                                 // Made nonstatic property [chg 20220305°1211]
    {
        //debugger;

        // Do not act, if no dialog exists
        if (! this.elMpdMasterDiv) {
            return;
        }

        // IE might not know the target property [seq 20190503°0513]
        if (! evt.target) {
            evt.target = evt.srcElement;
        }

        // [line 20190503°0515]
        let eClicked = evt.target;

        // [seq 20190503°0517]
        let bInside = this.elMpdMasterDiv.contains(eClicked);          // Line does not work as expected
        let bClosingHit = (eClicked.id === Mpd.Ht.Els[Mpd.Ht.iEl_Modpop_Container_Div][Mpd.Ht.iNdxId]);  // Replacement for above e.contains() which seems not to work as expected

        // Debug [seq 20190503°0521]
        if (Mpd.Dbg.bMsg_MouseClick) {
            const s5 = 'Mouseclick :'
                      + ' • id = "' + eClicked.id + '"'                // E.g. 'Modpop_Container_Div', 'modpop_window_button_one'
                       + ' • name = "' + eClicked.nodeName + '"'       // E.g. 'SPAN'
                        + ((eClicked.nodeName === 'INPUT') ? ' • value = "' + eClicked.value + '"' : '')
                         + ' • closing = ' + bClosingHit
                          + ' • inside = ' + bInside
                           + ' (main.id = "' + this.elMpdMasterDiv.id + '")'  // 'Mpd_Master_Div'
                            ;
            Mpd.WriteDbgMsg(s5);
        }

        // Conditionally close the popup window [seq 20190503°0523]
        if (bClosingHit) {
            Mpd.aInsts[Mpd.aInsts.length - 1].CloseDialogWindow({Action:'Cancel', Actor:'mouse'});
        }
    };

    /**
     *  This public method constitutes the event handler for elements getting focus
     *
     *  @id 20220306°1111
     *  @status First quick implementation, does probably not yet all it should
     *  @note The focusin event is one of the no-cancelable events
     *  @see javascript.info/focus-blur [ref 20220305°1523]
     *  @param {event} evnt —
     *  @returns {undefined} — Nothing
     */
    this.Handle_Focusin = function(evnt)
    {
        if (Mpd.Dbg.bMsg_HandleFocus) {
            Mpd.WriteDbgMsg_Focus('Handle_Focusin');
        }

        // Is the focus in the non-dialog area? [seq 20220306°1131]
        let elActive = document.activeElement;
        let elDialog = document.getElementById('Mpd_Master_Div')       // Curiously, this works, but this.elMpdMasterDiv does not work [issue 20220306°1133]
        if (! elDialog.contains(elActive)) {
            elActive.blur();                                           // ?
        }
    }

    /**
     *  This public method constitutes the event handler for key presses
     *
     *  @id 20190503°0531
     *  @note See ref 20190425°0312 'Chirp Internet → simple modal feedback form'
     *  @param {event} evt —
     *  @returns {undefined} —
     */
    this.Handle_Keys = function(evt)                                   // [chg 20220305°1221] Switched from local var 'let HanleKeys' to method 'this.Handle_Keys'
    {
        let sKey = evt["key"];                                         // Indexer access avoids WebStorm warning 'Unresolved variable key', as opposed to evt.key

        // [seq 20190503°0533]
        if (Mpd.Dbg.bMsg_HandleKeys) {                                 // WebStorm warning "Potentially invalid usage of this" [issue 20220224°1731]
            Mpd.WriteDbgMsg("Handle_Keys — Key = '" + sKey + "'");
        }

        // Dispatch key press [seq 20190503°0535]
        switch (sKey) {                                                // Note — e.keyCode is deprecated
            case ' ':                                                  // Blank
                break;
            case 'Enter':
                if (Mpd.Dbg.bMsg_HandleKeys) {
                    Mpd.WriteDbgMsg("Handle_Keys — Enter was pressed.");
                }
                evt.preventDefault();                                  // [line 20220306°0711] Fix issue 20220305°1151 'Pressing Enter does not close dialog'
                Mpd.aInsts[Mpd.aInsts.length - 1].CloseDialogWindow({Action:'Enter', Actor:'key'});  // Remember issue 20220305°1151 'Pressing enter does not close dialog'
                break;
            case 'Escape' :                                            // Normal browsers // Fallthrough
            case 'Esc':                                                // IE11
                evt.preventDefault();                                  // [line 20220306°0711`02] Fix issue 20220305°1151 'Pressing Enter does not close dialog'
                Mpd.aInsts[Mpd.aInsts.length - 1].CloseDialogWindow({Action:'Cancel', Actor:'key'});
                break;
            default :
                break;                                                 // Other key, nothing to do
        }

        if (Mpd.Dbg.bMsg_HandleKeys) {
            Mpd.WriteDbgMsg_Focus('Handle_Keys');
        }
    };

    /**
     *  This private nonstatic method shall serve as the callback for the MutationObserver
     *
     *  Works in
     *     • Chrome : Fine
     *     • Edge : No
     *     • FF : Curious, e.g. if scrollbars are lost, they will not come back
     *     • IE10 : Does not know the MutationObserver
     *     • IE11 : iframe resizing works, but dialog box resizing does not work
     *     • Opera : Works, but if scrollbars are lost, they will not come back
     *  In general, this solution needs some fine-tuning, but seems practicable.
     *
     *  @id 20190503°0211
     *  @callers •
     *  @returns {undefined} —
     */
    let MutationObserved = function()
    {
        // Get elements handles [seq 20190503°0221]
        let elmSrc = document.getElementById(Mpd.Ht.sEl_Iframe_Wrapper_Div);
        let elmTgt = document.getElementById(Mpd.Ht.sEl_Modpop_Window_Frame);

        // Not an iframe dialog? [seq 20190503°0223]
        if (typeof elmSrc === 'undefined' || elmSrc === null) {
            return;
        }

        // Set new size [seq 20190503°0225]
        elmTgt.style.width = (elmSrc.offsetWidth - 7) + "px";
        elmTgt.style.height = (elmSrc.offsetHeight - 7) + "px";
    };

    /**
     *  This private function ..
     *
     *  Todo : Shift position calculation (or complete content) to the caller
     *
     *  @id 20190503°0811
     *  @callers Only • func 20190503°0731 ToggleMaximized
     *  @param {number} iHeight (Integer) — The width to set
     *  @param {number} iWidth (Integer) — The height to set
     *  @returns {undefined} —
     */
    let ResizeDialogBox = function(iWidth, iHeight)
    {
        // [seq 20190503°0813 family 20190503°0821]
        //  Sequence family containing former window.screen.availWidth
        let left = (window.innerWidth - iWidth) / 2;
        let top = (window.innerWidth - iHeight) / 5;

        // [condition 20190503°0815]
        if (bMaximized) {
            left = window.pageXOffset + 10;
            top = window.pageYOffset + 10;
        } else {
            left += window.pageXOffset;
            top += window.pageYOffset;
        }

        // Set position and size [seq 20190503°0817]
        elDivBoxParent.style.top = top + "px";
        elDivBoxParent.style.left = left + "px";
        elDivBoxParent.style.height = iHeight + "px";
        elDivBoxParent.style.width = iWidth + "px";
    };

    /**
     *  This static wannabe private field stores the instances of the dialog window
     *
     *  Normally only one instance is in use so far, thus a simple object would
     *   suffice. But we want sketch the idea, that possibly one dialog can raise
     *   another one, thus we provide an array of Mpd objects, so they can stack.
     *
     *  @id prop 20190502°0641
     *  @see note 20190502°0643 'on static members'
     *  @type {array} — Array of Mpd objects
     */
    Mpd.aInsts = [];

    /**
     *  This static property stores the result object if dialog closes
     *
     *  This did not work as the nonstatic property 'this.oResult', since it shall
     *   survive the dialog object, to be available for the continuation callback
     *
     *  @id prop 20220303°0911
     *  @type {object} —
     */
    Mpd.Result = { 'Closer' : { Action : '', Actor : '' }, 'Fields' : [] };

    /**
     *  This static (wannabe private const) field provides file 20190427o0133.loading2.gif
     *   converted to Base64 on www.base64encode.org
     *
     *  @id prop 20190503°0413
     *  @note : The conversion was done on www.base64encode.org (ref 20190315°0313)
     *  @type {string} —
     */
    Mpd.sImgLoading = "data:image/png;base64,"
        + "R0lGODlhQgBCAPMAAP///wAAAExMTHp6etzc3KCgoPj4+BwcHMLCwgAAAAAAAAAAAAAAAAAAAAAA"
        + "AAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJ"
        + "CgAAACwAAAAAQgBCAAAE/xDISau9VBzMu/8VcRTWsVXFYYBsS4knZZYH4d6gYdpyLMErnBAwGFg0"
        + "pF5lcBBYCMEhR3dAoJqVWWZUMRB4Uk5KEAUAlRMqGOCFhjsGjbFnnWgliLukXX5b8jUUTEkSWBNM"
        + "c3tffVIEA4xyFAgCdRiTlWxfFl6MH0xkITthfF1fayxxTaeDo5oUbW44qaBpCJ0tBrmvprc5GgKn"
        + "fqWLb7O9xQQIscUamMJpxC4pBYxezxi6w8ESKU3O1y5eyts/Gqrg4cnKx3jmj+gebevsaQXN8HDJ"
        + "yy3J9OCc+AKycCVQWLZfAwqQK5hPXR17v5oMWMhQEYKLFwmaQTDgl5OKHP8cQjlGQCHIKftOqlzJ"
        + "sqVLPwJiNokZ86UkjDg5emxyIJHNnDhtCh1KtGjFkt9WAgxZoGNMny0RFMC4DyJNASZtips6VZkE"
        + "p1P9qZQ3VZFROGLPfiiZ1mDKHBApwisZFtWkmNSUIlXITifWtv+kTl0IcUBSlgYEk2tqa9PhZ2/F"
        + "yd3UcfIQAwXy+jHQ8R0+zHVHdQZ8A7RmIZwFeN7TWMpS1plJsxmNwnAYqc4Sx8Zhb/WPyqMynwL9"
        + "eMrpQwlfTOxQco1gx7IvOPLNmEJmSbbrZf3c0VmRNUVeJZe0Gx9H35x9h6+HXjj35dgJfYXK8RTd"
        + "6B7K1vZO/3qFi2MV0cccemkkhJ8w01lA4ARNHegHUgpCBYBUDgbkHzwRAAAh+QQJCgAAACwAAAAA"
        + "QgBCAAAE/xDISau9VAjMu/8VIRTWcVjFYYBsSxFmeVYm4d6gYa5U/O64oGQwsAwOpN5skipWiEKP"
        + "QXBAVJq0pYTqnCB8UU5KwJPAVEqK7mCbrLvhyxRZobYlYMD5CYxzvmwUR0lbGxNHcGtWfnoDZYd0"
        + "EyKLGAgClABHhi8DmCxjj3o1YYB3Em84UxqmACmEQYghJmipVGRqCKE3BgWPa7RBqreMGGfAQnPD"
        + "xGomymGqnsuAuh4FI7oG0csAuRYGBgTUrQca2ts5BAQIrC8aBwPs5xzg6eEf1lzi8qf06foVvMrt"
        + "m7fO3g11/+R9SziwoZ54DoPx0CBgQAGIEefRWyehwACKGv/gZeywcV3BFwg+hhzJIV3Bbx0IXGSJ"
        + "ARxDmjhz6tzJs4NKkBV7SkJAtOi6nyDh8FRnlChGoVCjSp0aRqY5ljZjplSpNKdRfxQ8Jp3ZE1xT"
        + "jpkqFuhGteQicFQ1xmWEEGfWXWKfymPK9kO2jxZvLstW1GBLwI54EiaqzxoRvSPVrYWYsq8byFWx"
        + "qcOs5vFApoKlEEm8L9va0DVHo06F4HQUA6pxrQZoGIBpyy1gEwlVuepagK1xg/BIWpLn1wV6ASfr"
        + "gpcuj5hkPpVOIbi32lV3V+8U9pVVNck5ByPiyeMjiy+Sh3C9L6VyN9qZJEruq7X45seNe0Jfnfkp"
        + "+u1F4xEjKx6tF006NPFS3BCv2AZgTwTwF1ZX4QnFSzQSSvLeXOrtEwEAIfkECQoAAAAsAAAAAEIA"
        + "QgAABP8QyEmrvVQIzLv/FSEU1nFYhWCAbEsRx1aZ5UG4OGgI9ny+plVuCBiQKoORr1I4DCyDJ7Gz"
        + "EyCYziVlcDhOELRpJ6WiGGJCSVhy7k3aXvGlGgfwbpM1ACabNMtyHGCAEk1xSRRNUmwmV4F7BXhb"
        + "Aot7ApIXCJdbMRYGA44uZGkSIptTMG5vJpUsVQOYAIZiihVtpzhVhAAGCKQ5vaQiQVOfGr+PZiYH"
        + "yLlJu8mMaI/GodESg7EfKQXIBtrXvp61F2Sg10RgrBwEz7DoLcONH5oa3fBUXKzNc2TW+Fic8OtA"
        + "QBzAfv8OKgwBbmEOBHiSRIHo0AWBFMuwPdNgpGFFAJr/li3D1KuAu48YRBIgMHAPRZSeDLSESbOm"
        + "zZs4oVDaKTFnqZVAgUbhSamVzYJIIb70ybSp06eBkOb81rJklCg5k7IkheBq0UhTgSpdKeFqAYNO"
        + "Za58+Q0qBpluAwWDSRWYyXcoe0Gc+abrRL7XviGAyNLDxSj3bArey+EuWJ+LG3ZF+8YjNW9Ac5m0"
        + "LEYv4A8GTCaGp5fykNBGPhNZrHpcajOFi8VmM9i0K9G/EJwVI9VM7dYaR7Pp2Fn3L8GcLxREZtJa"
        + "aMvLXwz2NFvOReG6Mel+sbvvUtKbmQgvECf0v4K2k+kWHnp8eeO+v0f79PhLdz91sts6C5yFfJD3"
        + "FVIHHnoWkPVRe7+Qt196eSkongXw4fQcCnW41F9F0+ETAQAh+QQJCgAAACwAAAAAQgBCAAAE/xDI"
        + "Sau9dAjMu/8VISCWcFiFYIBsS4lbJcSUSbg4aMxrfb68nFBSKFg0xhpNgjgMUM9hZye4URCC6MRU"
        + "GRxI18NSesEOehIqGjCjUK1pU5KMMSBlVd9LXCmI13QWMGspcwADWgApiTtfgRIEBYCHAoYEA2AY"
        + "WHCHThZ2nCyLgG9kIgehp4ksdlmAKZlCfoYAjSpCrWduCJMuBrxAf1K5vY9xwmTExp8mt4GtoctN"
        + "zi0FmJMG0csAwBUGs5pZmNtDWAeeGJdZBdrk6SZisZoaA5LuU17n9jpm7feK53Th+FXs3zd//xJO"
        + "yKbQGAIriOp1a9giErwYCCJGZEexQ8ZzIP8PGPplDRGtjj7OVUJI4CHKeQhfypxJs6bNDyU11rs5"
        + "IaTPnBpP0oTncwzPo0iTKjXWMmbDjPK8IShikmfIlVeslSwwseZHn1G0sitY0yLINGSVEnC6lFVX"
        + "igbi5iDJ8WW2tWkXTpWYd9tdvGkjFXlrdy1eDlOLsG34t9hUwgwTyvV2d6Big4efDe6LqylnDt+K"
        + "fO6cGddmNwRGf5qcxrNp0SHqDmnqzbBqblxJwR7WklTvuYQf7yJL8IXL2rfT5c7KCUEs2gt/G5wa"
        + "auoa57vk/Ur9L1LXb12x6/0OnVxoQC3lcQ1xXC93d2stOK8ur3x0u9YriB+ffBl4+Sc5158LMdvJ"
        + "F1Vpbe1HTgQAIfkECQoAAAAsAAAAAEIAQgAABP8QyEmrvXQMzLv/lTEUliBYxWCAbEsRwlaZpUC4"
        + "OCgKK0W/pl5uWCBVCgLE7ERBxFDGYUc0UDYFUclvMkhWnExpB6ERAgwx8/Zsuk3Qh6z4srNybb4w"
        + "AKYHIHlzHjAqFEh2ABqFWBRoXoESBAVmEkhZBANuGJeHXTKMmDkphC8amUN8pmxPOAaik4ZzSJ4S"
        + "cIA5VKO0BJOsCGaNtkOtZY9TAgfBUri8xarJYsOpzQAIyMxjVbwG0tN72gVxGGSl3VJOB+GaogXc"
        + "5ZoD6I7YGpLuU/DI9Trj7fbUyLlaGPDlD0OrfgUTnkGosAUCNymKEGzYIhI+JghE0dNH8QKZY+j/"
        + "8jEikJFeRwwgD4xAOJChwowuT8qcSbOmzQ5FRugscnNCypD5IkYc0VML0JB9iipdyrQptIc9yRyy"
        + "sC1jETkzU2IxZfVqgYk2yRxNdxUB2KWRUtK65nSX02Lb2NoTETOE1brNwFljse2q25MiQnLUZPWs"
        + "TBghp76QiLegXpXi2GlrnANqCHCz9g3uVu0AZYMZDU8zEFKuZtHdSKP7/Cb0r7/KDPwCaRr010kk"
        + "Wb8hkEq15xyRDA/czIr3JNWZdcCeYNbUQLlxX/CmCgquWTO5XxzKvnt5ueGprjc5tC0Vb+/TSJ4d"
        + "eNbsyPXG54rXHn4qyeMPa5+Sxp351JZU6SbMGXz+2YWeTOxZ4F4F9/UE4BeKRffWHgJ6EAEAIfkE"
        + "CQoAAAAsAAAAAEIAQgAABP8QyEmrvXQMzLv/lTEglmYhgwGuLEWYlbBVg0C0OCim9DwZMlVuCECQ"
        + "KoVRzCdBCAqWApTY2d0oqOkENkkeJ04m9fIqCCW7M0BGEQnUbu34YvD2rhIugMDGBucdLzxgSltM"
        + "WW0CAl9zBAhqEnYTBAV4ZAOWBU8WdZYrWZBWY3w2IYpyK3VSkCiMOU6uboM4dQNmbQSQtI+Jf0Sq"
        + "t4Acsp45tcHCpr5zqsXJfLOfBbwhzsl7unWbFwhSlddUTqcclN664IE1iq5k3tTow5qn53Td3/Ac"
        + "CAdP9FXv+JwQWANIEFfBZAIjSRHY7yAGSuoESHDkbWFDhy8U7dsnxwBFbw7/O2iUgYxOrpDk7qFc"
        + "ybKly5cIK7qDSUHjgY37uumcNo3mBAE3gQaV6LOo0aNI4XkcGFJnFUc62bEUesCWJYpR/7nMeDPo"
        + "FCNGTiatBZSogYtHCTBN2sIjWnAi1po08vaavqpy0UBlyFJE15L1wNaF9yKo1ImCjTq5KWYS3xCD"
        + "h2gFUOcAqg8G6AK8G3lY2M4sgOzL+/QxQANBSQf+dxZ0m5KiD7jObBqx6gsDqlbgMzqHI7E/avu+"
        + "6Yp3Y8zAHVty20ETo7IWXtz2l1zt1Uz72ty8fM2jVrVq1GK5ieSmaxC/4TgKv/zmcqDHAXmHZH23"
        + "J6CoOONLPpG/eAoFZIdEHHz4LEWfJwSY55N30RVD3IL87VFMDdOh9B88EQAAIfkECQoAAAAsAAAA"
        + "AEIAQgAABP8QyEmrvbQUzLv/lVEg1jBYyGCAbEsRw1aZ5UC4OCiq80kZplVuCECQKprjhEZJyZpP"
        + "IkZUuL1iPeRAKSEIfFIOQiOUAAtlANMc/Jm4YQsVXuAtwQAYvtiOcwhkTVsZUU5uAlZ+BghpEkkv"
        + "aB2AiQB1UWZVOWORP3WNOAZflABAApc6m41jcDiGh3agqT8Eny4GtK+1LHO6fmxfvbsanL4hJrBh"
        + "i5nFFV7IIJOfBsF+uCEIphiAI6PMLikC2VObjN62A+E2H9sj1OYi6cQetxrd5hXYpu5y1vfj9v4C"
        + "XpgmkBkBK6sQ9CvYYke6LqtGGNknEEa4i+LMHBwxgqEHdOn/ynG4RTHgJI8oU6pcyXKlkZcwW5Y4"
        + "gPGiEY4JZc6gyVPAgT06gwodStQjSaFjAGokEDOoz3iUmMJUWNKfxZ7iXh6sarTOUzNcZS4sqmgs"
        + "QxFKRzI1WxDBgZ8Ub0llK7DUW3kD54YtBuOtAFYT9BLFdlfbVjl7W4jslHEX08Qf3AqAPItqwFA0"
        + "0+o4SLcYZkRSblmeMI2yiDSf98ode1hKgZ8hnmq+wLmRXMoE3o7CDPTD0WYHmxwAPAEblwE05ajz"
        + "dZsCcjzJJ7zGY+AtceaPK+im8Fb4ASQ0KXdoHvhtmu6kt5P22VvR6CXRJ6Cf4POS2wPip3yqr/17"
        + "hvjSnVKXGnry+VcefkjNV6AF1gmV2ykKOgIaWRT4FFAEACH5BAkKAAAALAAAAABCAEIAAAT/EMhJ"
        + "q720FMy7/5VREJZmIYUBriwlbpUZD2prf289FUM4pLeghIA4jWKwCWFQrCCaQo4BpRsWoBLZBDEg"
        + "UZa9aIdwreYoPxfPzMOKLdNjBrhLAgxpCpf+xpy3cll2S1giXX0SU1UST4UIXhhkVXtwgSxECIt/"
        + "Qng0IW03cZkVZJBBXG6dnqGNZgaLNgYEbD+wLKK2iIkDvLm3rbqVtYhxvm9gxhdEs3DJx7BTTJHA"
        + "wUJgeRdT1NUrZLyHHpiPztWGvKMgsk/kwVzDsczcHVOm8vY47PfdXo0E8fo2iBQQwGuIuCf/AHLw"
        + "RpAgtjvqGin0wItgmXkJJ1oopbGjx48g/0MCPNhPZIUBAlKqJLjskct6IlE2VBnGpM2bOHN6lJXP"
        + "HgqYLmQtA+pRJsFHX1r6ywgSzEoBMJbO6jmRiMwwr3SGo6p1Xtadlla88sdVDIKUq/BJLRsFj0o+"
        + "ftaaXKLSTVKyOc+mtONiaiWA6NRAjXXggF1detmSKnxAsQcDAg4IcHyHMeXHKhUTsKzGsQgzKok+"
        + "5ozmQM0gA0/fyXxjQOFFmw2LiV0P8gG+ILjAKnz67OEtArDIrCTaBoLCplyfTpnBtIvIv4kV5ouc"
        + "QuEvkmNIvoyhwGvsja0fcFF9AuTB8gwUduNd9fXSfI9PtvdQQmTq45urBqBlovoD9bxn3hd3NsVm"
        + "gYATRFZcVeiJV4IAC5rEnD0RAAAh+QQJCgAAACwAAAAAQgBCAAAE/xDISau9FCHMu/+VgRBWUVhE"
        + "YYBsS4lbhZyy6t6gaFNFPBmmFW4IIJAqhFEN2bNoiB6YcJL0SUy1IxUL7VSnAGmGJgHuyiZt9wJT"
        + "A2bg5k++Pa/ZGnBS/dxazW5QBgRgEnsvCIUhShMzVmWMLnuFYoJBISaPOV9IkUOOmJc4gyNgBqdd"
        + "g6YFA3Y3pIl3HWauo5OybCa1Q6SKuCm7s4mKqLgXhBY6moa3xkQpAwPLZVXIzi1A0QWByXvW1xwi"
        + "2rGbSb7gVNHkLqfn6GHf7/Lh7vM31kZGxfbYM9ED1EaM0MfPi4l/rf6cGsit4JV/PeqpcojhEMWL"
        + "GDNq3Agln0cjHP8nIBz50WPIhwIGpFRJ5qTLlzBjrkEgLaSGhoYKCDjA80DIaCl7qBnQs+cAnAWh"
        + "pVwZo6eAbTJ1qARYBCnMeDI7DqgHDohVNkQPtOSHICjXH2EPbL0IRIDbdRjK8hTw9V3blNMApM1L"
        + "kYDKpxiI1hIxDy6kVq948u1CIOVZEI0PCHjM6y/lcHMvV3bccSfdF8FYiDBlmVfmCoK76Bzrl/MN"
        + "op8pEOBZl0Pj2GgB31tbYSdVCWX5lh2aEgVUWQh4gkk9wS2P4j/eyjOwc+xONTszOH8++V0ByXrA"
        + "U+D5Yidp3dcMKK7w/beE7BRYynCruQWX+GIrSGYPncfYedQd4AYZeS+Ix9FsAliwX2+4adTYfwQ+"
        + "VxtG/V0TAQAh+QQJCgAAACwAAAAAQgBCAAAE/xDISau9FCHMu/+VgRCWZhGIAa4sJW6VGRdqa39v"
        + "PSFFWKS3oIRAqqCKO9gEpdwhhRgDSjccxZoAzRNAKPSgHRGBmqP8XDwybwsOHa9UmcRwpnSBbU55"
        + "aU3aC090gHlzYyd9c3hRillyEyJUK0SGLlNggpGCWCBSI5GWUF1bmpErUkRkBqUtUmpeq6ZHsIQA"
        + "gjRtp5S0Ll6MUJ2zuD/BF6ilqrvFxzybhZ7JQl29epO60DheXmwWudbX3Dy9xI+T48kEA8M3qua7"
        + "rd/wks3x0TUH9wKD9DYiXukSBe4JPCBg3j4+BdINSNekiwCBAg52SJgOUDAEAwxKBCWxo8ePIP9D"
        + "whtIUmQFigtTFnhIkqBJMyljfnlJs6bNm/Qwajz4hoNDiDRlMgpIMiPNLjEXwoCoD2e/lEO24VzS"
        + "buqHLlUJiVk34N5MiRjztaMjcEDWPHRS+irBUoBUnisXvu1KcOfGhQUxdL0Vwi6YtSL+tSDw0G8Q"
        + "wmYJESZ4loWBAQISg1ksoDEryJIPP6zMy/IjRo8jW6YcaS+YlV9rYW7clbMdgm9BEHYbAnJq2QPY"
        + "PBxgJy8HjE/icmvaBgFjCrYpCIg4Qfij5bFxPUz98Mny3sx3iIYX0PWQ4xMeulhOJvk1A9VPRq7g"
        + "Enk+I+S/ebFgWnl2CQjWz/CI/kCk9kvE9xIUAQCGd4AF0NGE3m3XnZSZVfpdEwEAIfkECQoAAAAs"
        + "AAAAAEIAQgAABP8QyEmrvZQQzLv/laFZCGIRiAGuLCVuFXqmbQ2KNFWGpWr/ANGJ4JvIMghYRgnE"
        + "vIoSQ7KyQzKD1Sbn6dJAj9Geq3TVhryxnCSLNSHV5gt3Iv0yUUwpXIsYlDV5RB0iX2xRgjUDBwJX"
        + "c0B6UFgFZR8GB5eRL1p4PAV7K5aXeQaRNaRQep8soQelcWOeri2ssnGptbMCB26vIbGJBwOlYL0h"
        + "pSKTGIqXBcVNKAXJGAiXi5TOWwjRqhUF1QK42EEE24gfBMu84hfkk+EX2u/OhOv1K8T2Zojf0vmz"
        + "0NEkFNBVLZg6f3K0RVt4Z+A3hB0WejLHbsBBiF3kYdzIsaPHjyz/CBZcBJKCxJMiCwooOSHagAIv"
        + "XzZjSbOmzZvitF3kyIkDuWUkS8JkCGVASgF+WEKL+dINwZcaMeoZegjnlqhWO5DDamuKqXQ8B1jU"
        + "aMDhgQJczUgRO9YDgqfXEJYV28+Ct0U7O/60iMHbJyn5KIbhm0tA3jjohL0yoAtcPQN008YQQFny"
        + "KraWgzRGxQ0UnLmKbRCg7JiC0ZlA+qCOgtmG0dJGKMcFgQ52FKo10JWiPCADYQzomMDs7Sszlcom"
        + "BawWm3w15KSPKa8GIJsCZRdIj4cWN9D2aNvX6RhFJfawFsaMtFcI39Lw5O3OAlYwepD9GuUkzGND"
        + "f8W+ZvgefWeBEn8AGDUbQuhcRGAfxtnD3DoRAAAh+QQJCgAAACwAAAAAQgBCAAAE/xDISau9lBDM"
        + "u/8VcRSWZhmEAa4shRxHuVVI2t6gAc+TSaE2nBAwGFgEoxBPApQNPbokpXAQKEMI1a/29FAPWokI"
        + "nFkCwwDgsnuCkSgwREY+QdF7NTTb8joskUY9SxpmBFl7EggDawCAGQd3FyhohoyTOANVen2MLXZ6"
        + "BghcNwZIZBSZgUOGoJV6KwSmaAYFr54Gs6KHQ6VVnYhMrmxRAraIoaLGpEiRwEx5N5m1J83OTK92"
        + "v1+Q1ry6vwAIpgLg3dS6yhPbA+nmdqJBHwaZ3OYchtA3BNP2GJf9AD0YCggMlwRTAwqUIygJXwE6"
        + "BUzBEDCgGsMtoh4+NFOAXpWLHP8y1oh3YZ9FkGlIolzJsqXLlzgkwpgIcwKCAjhzPhSApCcMVTBv"
        + "CtV4sqbRo0iTshFak1WHfQN6WgmaM5+EiFWqUFxIMJROnDN4UuSX1E5OMVyPGlSKaF+7bqHenogq"
        + "oKi9fQ/lponIk+zFUAkVthPHc9FLwGA58K17FO9DDBH9PguoMuXjFgSi2u2SWTKvwnpx0MIZ2h/o"
        + "gLQSlq5QauuW1axJpvac4/QUAW+GKGo2G3ZEwxl4ws5QZE3qzSU9R80NIHO5fUsUMX82/II4drcj"
        + "FXGR8EdxgPMYoyKHCmhmoM1V9/s9iyIait6x1+mIXEjrNeKmw59SMUSR6l5UE1EjM9txN1049RUU"
        + "lR771fFfUw1OEJUF38E0TzURJkLbUR31EwEAOwAAAAAAAAAAAA=="
         ;

    /**
     *  This static (wannabe private const) field provides the bulk HTML for
     *   the dialog box. It is made up like follows.
     *
     *  • div                     Modpop_Container_Div
     *  • div                      Modpop_Frame_Parent_Div
     *    • table                  .
     *      • tr                   Modpop_Titlebar_Tr
     *        • td                 .
     *          • span             Modpop_Titlebar_Span
     *        • td                 .
     *          • span             Modpop_Window_Max_Span
     *      • tr                   .
     *        • td                 Modpop_Window_Td
     *          • span             Modpop_Loading_Span
     *             • img           Modpop_Loading_Img
     *          • div              Modpop_Text_Div
     *             • span          Modpop_Text_Span
     *          • div              Iframe_Wrapper_Div
     *            • iframe         Modpop_Window_Frame
     *      • tr                   Modpop_Window_Buttons_Tr
     *        • td                 Container_For_Buttons — The buttons are inserted dynamically then
     *  @id 20190503°0415
     *  @type {string} —
     */
   Mpd.sOverLayHTML
          = '\n<div id="Modpop_Container_Div"'
          + '\n      onmousemove="Mpd.aInsts[Mpd.aInsts.length - 1].DraggingDo(event);"'
          + '\n       onmouseup="Mpd.aInsts[Mpd.aInsts.length - 1].DraggingStop();"'
          + '\n        style="position:absolute; z-index:10;'
          + '\n         background-color:White; filter:alpha(opacity = 70); opacity:0.7;'
          + '\n          ">'
          + '\n</div>'
          + '\n<div id="Modpop_Frame_Parent_Div"'
          + '\n  style="position:absolute; z-index:12;'
          + '\n          background-color:White; /* -moz-box-shadow: 0 0 20px 20px #bbbbbb; */'
          + '\n           box-shadow: 0 0 20px 20px #bbbbbb;'
          + '\n            overflow:hidden; resize:both;'              // Try to make it resizable [line 20190503°0541]
          + '\n              ">'
          + '\n  <table id="Mpd_Table" style="width:100%"'
          + '\n          onmouseup="Mpd.aInsts[Mpd.aInsts.length - 1].DraggingStop();"'
          + '\n           >'
          + '\n    <tr style="background-color:LightGray; line-height:1.5em;" id="Modpop_Titlebar_Tr"'
          + '\n          onmousedown="Mpd.aInsts[Mpd.aInsts.length - 1].DraggingStart(event, this);"'
          + '\n           onmousemove="Mpd.aInsts[Mpd.aInsts.length - 1].DraggingDo(event);"'
          + '\n            onmouseup="Mpd.aInsts[Mpd.aInsts.length - 1].DraggingStop();">'
          + '\n      <td id="Mpd_Titlebar_Cell1" style="padding:0.7em;">'
          + '\n        <span id="Modpop_Titlebar_Span" data-docs="The title bar text"'
          + '\n                style="color:White; font-size1.1em;'
          + '\n                 ">'
          + '\n        </span>'
          + '\n      </td>'
          + '\n      <td id="Mpd_Titlebar_Cell2" style="text-align:right; width:5.7em; height:0.1em;">'  // The final height will be such that the content fits in
          + '\n        <span id="Modpop_Window_Max_Span" data-docs="The title bar´s Maximize icon"'
          + '\n               style="font-size:x-large; cursor:pointer;"'
          + '\n                onclick="Mpd.aInsts[Mpd.aInsts.length - 1].ToggleMaximized();"'
          + '\n                 >🗖'                                    // 🗖 = &#128470 [chg 20220226°0911`02] Formerly '[Maxi]'
          + '\n        </span>'
          + '\n        <span id="Mpd_Titlebar_Cell2_Span2" data-docs="The title bar´s Close icon"'
          + '\n               onclick="Mpd.aInsts[Mpd.aInsts.length - 1].CloseDialogWindow({Action:\'Cancel\', Actor:\'icon\'});"'
          + '\n                style="font-size:x-large; cursor:pointer;"'
          + '\n                 >❌'                                    // ❌ = &#x274c;
          + '\n        </span>'
          + '\n      </td>'
          + '\n    </tr>'
          + '\n    <tr>'
          + '\n      <td colspan="2" style="text-align:center; width:100%;" id="Modpop_Window_Td">'  // center
          + '\n        <span id="Modpop_Loading_Span">'
          + '\n          <img id="Modpop_Loading_Img" src="' + Mpd.sImgLoading + '" alt="Loading ...">'
          + '\n        </span>'
          + '\n        <!-- Content area, either the message div or the iframe div -->'
          + '\n      </td>'
          + '\n    </tr>'
          + '\n    <tr id="Modpop_Window_Buttons_Tr" style="border:3px solid SeaGreen;">'
          + '\n      <td colspan="2" style="text-align:center; padding-top:20px;padding-bottom:10px;" id="Container_For_Buttons">' // [seq 20190503°0455]
          + '\n      </td>'
          + '\n    </tr>'
          + '\n  </table>'
          + '\n</div>'
           ;

    /**
     *  This (wannabe private) nonstatic field stores outer border of the dialog box
     *
     *  @id localvar 20190503°0425
     *  @todo This shall replace var 'elMpdFrameParentDiv'. Try 'this.elDivBoxParent'.
     *  @todo Remember and use ref 20190210°0245 'stackoverflow → make div height expand with content'
     *  @type {Element} —                                              // Remark: 'Element' is not a JavaScript type
     */
    let elDivBoxParent = null;

    /**
     *  This (wannabe private) nonstatic field ...
     *
     *  @id localvar 20190503°0427
     *  @type {boolean} —
     */
    let bMaximized = false;

    /**
     *  This (wannabe private) nonstatic field .. serves the dragging logic
     *
     *  @id localvar 20190503°0433
     *  @note Relates to 'dragging logic'
     *  @type {number} (integer?) —
     */
    let dragLeft = 0;

    /**
     *  This (wannabe private) nonstatic field .. serves the dragging logic
     *
     *  @id localvar 20190503°0435
     *  @note Relates to 'dragging logic'
     *  @type {number} (integer?) —
     */
    let dragTop = 0;

    /**
     *  This (wannabe private) nonstatic field .. serves the dragging logic
     *
     *  @id localvar 20190503°0437
     *  @type {boolean} —
     */
    let dragging = false;

    /**
     *  This (wannabe private) nonstatic field ..
     *
     *  @id localvar 20190503°0441
     *  @type {number|null} (integer) —
     */
    let iCurrentHeight = null;

    /**
     *  This (wannabe private) nonstatic field ..
     *
     *  @id localvar 20190503°0443
     *  @type {number|null} (integer) —
     */
    let iCurrentWidth = null;

    /**
     *  This (wannabe private) nonstatic field ..
     *
     *  @id localvar 20190503°0445
     *  @type {number|null} (integer) —
     */
    let iCurrentPosX = null;

    /**
     *  This (wannabe private) nonstatic field ..
     *
     *  @id localvar 20190503°0447
     *  @type {number|null} (integer) —
     */
    let iCurrentPosY = null;

    /**
     *  This (wannabe private) nonstatic field ..
     *
     *  @id localvar 20190503°0451
     *  @type {object} —
     */
    let oObserver = null;
};

/**
 *  This function replaces blanks by underlines. The purpose is,
 *   to use an object key as HTML element ID
 *
 *  @id func 20220302°1431
 *  @callers
 *  @param {string} sObjKey — The given object key
 *  @return {string} — The wanted HTML element ID
 */
Mpd.Conv_ObjKey2InputId = function (sObjKey) {
    'use strict';

    let sRet = 'inpt_' + sObjKey.replaceAll(' ', '_') + '_id';
    return sRet;
};

/**
 *  This function builds the result object debug string
 *
 *  @id func 20220305°1041
 *  @callers
 *  @param {object} oRslt — The given result object
 *  @return {string} — The wanted string
 */
Mpd.GetResultDebugString = function(oRslt) {
    'use strict';

    // () Debug notification [seq 20220305°1031] Partially similar below seq 20220302°1621
    let sMsg = ` — Closer : • Action = '${oRslt.Closer.Action}' • Actor = ${oRslt.Closer.Actor}`;
    sMsg += ' — Fields :';
    let sIds = '<span style="font-style:italic; font-size:small; padding-left:1.1em; color:SlateGray;">IDs :';
    if ('Fields' in oRslt) {
        oRslt.Fields.forEach( field => {
            sMsg += ` •  ${field.Name} = ${field.Value}`;
            sIds += ` •  ${field.Id}`;
        });
    }
    if (Mpd.Dbg.bMsg_CollectResult_Ids) {                              // [condition 20220302°1611]
        sMsg += sIds + '</span>';
    }

    return sMsg;
};

/**
 *  This function processes the debug borders checkbox
 *
 *  @id func 20220227°1411
 *  @ref attacomsian.com/blog/javascript-foreach [ref 20220227°1422]
 *  @ref iqcode.com/code/javascript/iframe-getelementbyid [ref 20220227°1432]
 */
Mpd.RenderDebugBorders = function() {
    'use strict';

    // Process checkbox from demos.html [seq 20220307°0941]
    let elCbx = document.getElementById('Cbx_DebugBorders_Id');
    let bChecked = false;
    if (elCbx) {
        bChecked = elCbx.checked;
    }

    // Iterate all bordered HTML elements [seq 20220227°1413]
    Mpd.Ht.Els.forEach(item => {
        if ( (item[Mpd.Ht.iNdxBorder]).length > 0) {

            // (.) Render one element [seq 20220305°0941] Reduced as opposed it was before
            // (.1) Get element
            let elTgt = null;
            if (item[Mpd.Ht.iNdxIsIframe] !== 'i') {
                elTgt = document.getElementById(item[Mpd.Ht.iNdxId]);
            }
            else {
                let elIfram = document.getElementById('Modpop_Window_Frame');  // Carrier page´s carrier element
                if (elIfram) {
                    elTgt = elIfram.contentWindow.document.getElementById(item[Mpd.Ht.iNdxId]);  // Access from carrier page to iFrame page (see issue 20220226°1021)
                }
            }
            // (.2) Render element
            if (elTgt) {
                elTgt.style.border = bChecked ? item[Mpd.Ht.iNdxBorder] : '';
            }
        }
    });
};

/**
 *  This public static method constitutes a primitive debug output facility
 *
 *  @id 20190503°0231
 *  @note Interestingly, this is a static method defined inside the class body,
 *      not outside like the other static methods so far. Seems to work, does it?
 *  @chg 20220302°0823 : Formerly this function was inside the Mpd.ShowModalDialog
 *      function. It worked fine there, except then for line, where it caused runtime
 *      error 'Not a function'. Outsourcing the function to here solved the issue.
 *  @param sMsg {string} — The message to output
 *  @returns {undefined} —
 */
Mpd.WriteDbgMsg = function (sMsg) {

    // () Prologue [seq 20190503°0233]
    let elDbgMsgArea = document.getElementById('Debug_Messages_Area');

    // () Possibly create the area first [seq 20190503°0235]
    if (elDbgMsgArea === null) {
        elDbgMsgArea = document.createElement('div');
        elDbgMsgArea.id = 'Debug_Messages_Area';
        elDbgMsgArea = document.body.insertBefore(elDbgMsgArea, document.body.firstChild);
    }
    if (Mpd.Ht.iDbgCounter < 1) {
        elDbgMsgArea.style.fontSize = 'small';
        elDbgMsgArea.style.border = '2px solid SlateGray';
        elDbgMsgArea.style.borderRadius = '0.7em';
        elDbgMsgArea.style.backgroundColor = 'Cornsilk';               // Cornsilk PeachPuff;
    }

    // () Build message [seq 20190503°0237]
    let el17 = document.createElement('p');
    el17.style.paddingLeft = '2.7em';
    el17.style.textIndent = '-2.1em';                                  // Hanging indent
    el17.innerHTML = ++Mpd.Ht.iDbgCounter + ' ' + sMsg;

    // () Print message [seq 20190503°0239]
    // Compare gomakethings.com/adding-an-element-to-the-end-of-a-set-of-elements-with-vanilla-javascript/ [ref 20220226°0822]
    if (Mpd.Dbg.bOutput_Order_TopDown) {
        elDbgMsgArea.appendChild(el17);                                // Insert at the end of the node list
    }
    else {
        // Feature detect [seq 20220302°1421] Not sure whether I will keep this distinction
        if (!! elDbgMsgArea.prepend) {                                 // Short for "= elDbgMsgArea.prepend ? true : false;" [line 20220302°1425] Compare ref 20220306°1012
            elDbgMsgArea.prepend(el17);                                // Insert at the beginning of the node list -- Does not work with Opera 36
        }
        else {
            // Fallback e.g. for Opera 36
            elDbgMsgArea.insertBefore(el17, elDbgMsgArea.firstChild);
        }
    }
};

/**
 *  Wrapper function to output the current focus status ..
 *
 *  @id func 20220306°0811
 *  @callers
 *  @param {string} sPrefix —
 *  @constructor
 */
Mpd.WriteDbgMsg_Focus = function (sPrefix) {

    let elActive = document.activeElement;
    let sActiveTag = elActive.tagName;
    let sActiveId = elActive.id;
    let sActiveName = elActive.name;                                   // nodeName
    //let sActiveIHtml = elActive.innerHTML;                           // Empty anyway
    let sActiveOHtml = elActive.outerHTML;
    sActiveOHtml = Mpd.sConfine(sActiveOHtml.escapeHtml(),111);        // See func 20220306°0751

    if (Mpg.bDbgMsg_General) {
        let sMsg = ' — Focus : • Tag = ' + sActiveTag + ' • Id = ' + sActiveId
                  + ' • Name = ' + sActiveName + ' • outerHTML = ' + sActiveOHtml
                   ;
        Mpd.WriteDbgMsg(sPrefix + sMsg);
    }
}

/**
 *  Subnamespace for debug flags
 *
 *  id 20220302°1541
 *  @type {object}
 */
Mpd.Dbg = {};

/**
 *  Properties set 'debug flags'
 *
 *  id 20220302°1551
 *  @type {boolean}
 */
Mpd.Dbg.bDbgMsg_General = false; // true                               // [var 20220307°1011]
Mpd.Dbg.bMsg_CollectResult_Ids = false; // true                        // [var 20220302°1553]
Mpd.Dbg.bMsg_CollectResult_oFields = false; // true                    // [var 20220302°1555]
Mpd.Dbg.bMsg_CustomForm_Extraction = false; // true                    // [var 20220303°0931]
Mpd.Dbg.bMsg_HandleKeys = false; // true                               // [var 20200104°0435]
Mpd.Dbg.bMsg_MouseClick = false; // true                               // [var 20220305°1021]
Mpd.Dbg.bMsg_HandleFocus = false; // true                              // Most important [var 20220306°1121]
Mpd.Dbg.bOutput_Order_TopDown = false;                                 // [var 20220306°0951]

/**
 *  Subnamespace for the HTMLElement variables
 *
 *  id 20220302°1131
 *  @type {object}
 */
Mpd.Ht = {};

/**
 * The Elements Array with element definitions
 *
 * id 20220227°1111
 * @type {array}
 */
Mpd.Ht.Els = [
      [ "Modpop_Container_Div"     , "1px dashed SlateGray"     , ""  , "" ]  // 0
    , [ "Modpop_Frame_Parent_Div"  , "4px dotted Magenta"       , ""  , "" ]  // 1
    , [ "Mpd_Table"                , "5px solid Yellow"         , ""  , "" ]  // 2
    , [ "Modpop_Titlebar_Tr"       , "1px dashed SlateGray"     , ""  , "" ]  // 3
    , [ "Mpd_Titlebar_Cell1"       , "1px dashed SlateGray"     , ""  , "" ]  // 4
    , [ "Modpop_Titlebar_Span"     , "3px dotted Chocolate"     , ""  , "" ]  // 5
    , [ "Mpd_Titlebar_Cell2"       , "3px dashed Chocolate"     , ""  , "" ]  // 6
    , [ "Modpop_Window_Max_Span"   , "3px dotted Chocolate"     , ""  , "" ]  // 7
    , [ "Mpd_Titlebar_Cell2_Span2" , "3px dotted Chocolate"     , ""  , "" ]  // 8
    , [ "Modpop_Window_Td"         , "1px dashed SlateGray"     , ""  , "Area where either the dynamic Dialog or the iFrame is faded-in" ]  // 9
    , [ "Modpop_Loading_Span"      , "1px dashed SlateGray"     , ""  , "The loading animation, first displayed, then switched off" ]  // 10
    , [ "Modpop_Loading_Img"       , "1px dashed SlateGray"     , ""  , "The animated image inside the loading span" ]  // 11
    , [ "Modpop_Text_Div"          , "1px dashed SlateGray"     , ""  , "~Dynamically inserted" ]  // 12
    , [ "Modpop_Text_Span"         , "1px dashed SlateGray"     , ""  , "~Dynamically inserted" ]  // 13
    , [ "Iframe_Wrapper_Div"       , "5px dashed LimeGreen"     , ""  , "A div around the iFrame. Dynamically inserted in the carrier page" ]  // 14
    , [ "Modpop_Window_Frame"      , "5px solid Magenta"        , ""  , "The iFrame itself. Dynamically inserted in the carrier page" ]  // 15
    , [ "Modpop_Window_Buttons_Tr" , "3px solid SeaGreen"       , ""  , "The buttons bar" ]  // 16
    , [ "Container_For_Buttons"    , "1px dashed SlateGray"     , ""  , "The cell where the buttons are inserted" ]  // 17
    , [ "Div_Show_Result"          , "3px dashed Black"         , ""  , "Some other container used occasionally" ]  // 18
    , [ "Mpd_Custom_Body"          , "3px dashed Orange"        , "i" , "The iFrame body" ]  // 19
    , [ "Mpd_Custom_Table"         , "3px dotted HotPink"       , "i" , "The iFrame content area" ]  // 20
    , [ "Page_Demo_Body"           , "3px dotted Gold"          , ""  , "The demo page`s body element" ]  // 21
    , [ "id20190502o0323"          , "3px dotted Tomato"        , ""  , "The demo page`s screen div" ]  // 22
];

/**
 *  This variable serves as the line counter for the debug message output function
 *
 *  @id 20220302°0811
 *  @type {number}
 */
Mpd.Ht.iDbgCounter = 0;

/**
 *  Variable set for handling all the involved elements
 *
 *  @note So far, only the first one is used, not yet the others. It is not yet clear,
 *      whether the overhead of having this structure pays the advantage of having it.
 *  @id 20220227°1003
 *  @type {number}
 */
Mpd.Ht.iEl_Modpop_Container_Div = 0;                                   // [var 20220227°1011`02] The only used one of the set — This style is too bulky.
Mpd.Ht.iEl_Modpop_Frame_Parent_Div = 1;                                // [var 20220227°1013`02]
Mpd.Ht.iEl_table42 = 2;                                                // [var 20220227°1015`02] // Unused dummy so far
Mpd.Ht.iEl_Modpop_Titlebar_Tr = 3;                                     // [var 20220227°1017`02]
Mpd.Ht.iEl_table42cells1 =4;                                           // [var 20220227°1019`02] // Unused dummy so far
Mpd.Ht.iEl_Modpop_Titlebar_Span = 5;                                   // [var 20220227°1021`02]
Mpd.Ht.iEl_table42cells2 =6;                                           // [var 20220227°1023`02] // Unused dummy so far
Mpd.Ht.iEl_Modpop_Window_Max_Span = 7;                                 // [var 20220227°1025`02]
Mpd.Ht.iEl_rows43 =8;                                                  // [var 20220227°1027`02] // Unused dummy so far
Mpd.Ht.iEl_Modpop_Window_Td = 9;                                       // [var 20220227°1029`02]
Mpd.Ht.iEl_Modpop_Loading_Span = 10;                                   // [var 20220227°1031`02]
Mpd.Ht.iEl_Modpop_Loading_Img = 11;                                    // [var 20220227°1033`02] // Not used
Mpd.Ht.iEl_Modpop_Text_Div = 12;                                       // [var 20220227°1035`02]
Mpd.Ht.iEl_Modpop_Text_Span = 13;                                      // [var 20220227°1037`02]
Mpd.Ht.iEl_Iframe_Wrapper_Div = 14;                                    // [var 20220227°1039`02]
Mpd.Ht.iEl_Modpop_Window_Frame = 15;                                   // [var 20220227°1041`02]
Mpd.Ht.iEl_Modpop_Window_Buttons_Tr = 16;                              // [var 20220227°1043`02]
Mpd.Ht.iEl_Container_For_Buttons = 17;                                 // [var 20220227°1045`02] // The buttons are inserted dynamically then
Mpd.Ht.iEl_Div_Show_Result = 18;                                       // [var 20220227°1047`02]
Mpd.Ht.iEl_Mpd_Custom_Body = 19;                                       // [var 20220227°1049`02]
Mpd.Ht.iEl_Mpd_Custom_Table = 20;                                      // [var 20220227°1051`02]
Mpd.Ht.iEl_Page_Demo_Body = 21;                                        // [var 20220227°1052`02]
Mpd.Ht.iEl_Page_Demo_Screen = 22;                                      // [var 20220227°1053`02]

/**
 *  This variable set holds the array indices for the border elements array
 *
 *   @id 20220227°1121 variable set
 *   @type {number}
 */
Mpd.Ht.iNdxId = 0;                                                     // [var 20220227°1123]
Mpd.Ht.iNdxBorder = 1;                                                 // [var 20220227°1125]
Mpd.Ht.iNdxIsIframe = 2;                                               // [var 20220305°0951]
Mpd.Ht.iNdxSummary = 3;                                                // [var 20220227°1127] Not yet used

/**
 *  Variable set for handling all the involved elements
 *
 *  id 20220227°1001
 *  @type {string}
 */
Mpd.Ht.sEl_Modpop_Container_Div = 'Modpop_Container_Div';              // [var 20220227°1011`01]
Mpd.Ht.sEl_Modpop_Frame_Parent_Div = 'Modpop_Frame_Parent_Div';        // [var 20220227°1013`01]
Mpd.Ht.sEl_table42 ='Mpd_Table';                                       // [var 20220227°1015`01]
Mpd.Ht.sEl_Modpop_Titlebar_Tr = 'Modpop_Titlebar_Tr';                  // [var 20220227°1017`01]
Mpd.Ht.sEl_table42cells1 ='Mpd_Titlebar_Cell1';                        // [var 20220227°1019`01] // Unused dummy so far
Mpd.Ht.sEl_Modpop_Titlebar_Span = 'Modpop_Titlebar_Span';              // [var 20220227°1021`01]
Mpd.Ht.sEl_table42cells2 ='Mpd_Titlebar_Cell2';                        // [var 20220227°1023`01] // Unused dummy so far
Mpd.Ht.sEl_Modpop_Window_Max_Span = 'Modpop_Window_Max_Span';          // [var 20220227°1025`01]
Mpd.Ht.sEl_rows43 ='Mpd_Titlebar_Cell2_Span2';                         // [var 20220227°1027`01] // Unused dummy so far
Mpd.Ht.sEl_Modpop_Window_Td = 'Modpop_Window_Td';                      // [var 20220227°1029`01]
Mpd.Ht.sEl_Modpop_Loading_Span = 'Modpop_Loading_Span';                // [var 20220227°1031`01]
Mpd.Ht.sEl_Modpop_Loading_Img = 'Modpop_Loading_Img';                  // [var 20220227°1033`01] // Not used
Mpd.Ht.sEl_Modpop_Text_Div = 'Modpop_Text_Div';                        // [var 20220227°1035`01]
Mpd.Ht.sEl_Modpop_Text_Span = 'Modpop_Text_Span';                      // [var 20220227°1037`01]
Mpd.Ht.sEl_Iframe_Wrapper_Div = 'Iframe_Wrapper_Div';                  // [var 20220227°1039`01]
Mpd.Ht.sEl_Modpop_Window_Frame = 'Modpop_Window_Frame';                // [var 20220227°1041`01]
Mpd.Ht.sEl_Modpop_Window_Buttons_Tr = 'Modpop_Window_Buttons_Tr';      // [var 20220227°1043`01]
Mpd.Ht.sEl_Container_For_Buttons = 'Container_For_Buttons';            // [var 20220227°1045`01] // The buttons are inserted dynamically then
Mpd.Ht.sEl_Div_Show_Result = 'Div_Show_Result';                        // [var 20220227°1047`01]

/**
 *  This is the most simple API function, it resembles the JavaScript alert() function
 *
 *  @id func 20220304°0911
 *  @param sMessage {string} —
 *  @param fnCallback {function} —
 *  @constructor
 */
Mpd.ShowAlert = function (sMessage, fnCallback) {

    Mpd.ShowModalDialog ( 'Note'                                       // Title text
                         , sMessage                                    // Message
                          , null                                       // Fields object or iFrame URL string
                           , 400                                       // Width in pixel
                            , 300                                      // Height in pixel
                             , ['OK']                                  // Array of buttons
                              , fnCallback                             // Callback function
                               );                                      // Returns nothing
}

/**
 *  This is the most simple user input function, it resembles the JavaScript prompt()
 *   function. Except that it does not block, but instead uses a callback to continue.
 *
 *  @id func 20220304°1011
 *  @param sMessage {string} — Optional message
 *  @param fnCallback {function} —
 *  @constructor
 */
Mpd.ShowPrompt = function (sMessage, fnCallback) {

    Mpd.ShowModalDialog ( 'Input'                                      // Title text
                         , sMessage                                    // Message
                          , { '' : 'Tiptap..' }                              // Fields object or iFrame URL string
                           , 400                                       // Width in pixel
                            , 300                                      // Height in pixel
                             , ['OK']                                  // Array of buttons
                              , function(oResult) { fnCallback(oResult.Fields[0].Value); } //  Callback function
                               );                                      // Returns nothing
}

/**
 *  This static method shall be a replacement for the obsolete
 *   window.ShowModalDialog function, with simplified parameterization
 *
 *  The original function was synchronous, which seems to be the reason
 *   why it is obsolete. This here is equipped with a callback parameter.
 *
 *  @id 20190502°0231
 *  @see ref 20190429°0722 'mdn → window.ShowModalDialog'
 *  //@see ref 20190502°0632 'mdn → is type array'
 *  @callers •
 *  @param {string} sTitle — Text for the title bar
 *  @param {string} sMsg — The message text
 *  @param {object|string} sUrl — Either null or an object for built-in dialog, or URL for the external page to use
 *  @param {number} iWidth (Integer) — Width in pixel
 *  @param {number} iHeight (Integer) — Height in pixel
 *  @param {string|array} arButtons — Array with the button definitions ~~~for built-in dialog or to pass it to the external page
 *  @param {function} fnCbk — The function to be called after dialog close
 *  @returns {object|null} — The newly created Mpd instance
 */
Mpd.ShowModalDialog = function (sTitle, sMsg, sUrl, iWidth, iHeight, arButtons, fnCbk) {

    'use strict';

    // Construct object
    let oModPop = new Mpd();

    // Save the callback [line 20220302°1041] Is it really necessary? Could it be simplified?
    oModPop.OnPopUpCloseCallBack = fnCbk;

    // Craft the dialog box
    Mpd.aInsts.push(oModPop);
    oModPop.CreateDialogWindow ( sTitle                                // The title of the box
                                , sMsg                                 // The message to be shown
                                 , sUrl                                // Empty if simple dialog, object if generated form, URL if custom form in iframe
                                  , iWidth                             // The height of the box (so far in pixel)
                                   , iHeight                           // The width of the box (so far in pixel)
                                    , arButtons                        // The buttons array
                                     );
};

// [func 20220306°0751]
// See coderwall.com/p/ostduq/escape-html-with-javascript [ref ]
// todo 20220306°1021 : See trektautils.js func 20140926°1431 Trekta.Utils.htmlEscape and merge that
/**
 *  Mixin to extend the String type with a method to escape unsafe characters
 *   for use in HTML. Uses OWASP guidelines for safe strings in HTML.
 *
 *  Credit: benv.ca/2012/10/4/you-are-probably-misusing-DOM-text-methods/
 *     github.com/janl/mustache.js/blob/16ffa430a111dc293cd9ed899ecf9da3729f58bd/mustache.js#L62
 *
 *  Maintained by stevejansen_github@icloud.com
 *
 *  @ license opensource.org/licenses/MIT
 *  @ version 1.0
 *  @ mixin
 */
(function(){
    "use strict";

    function escapeHtml() {
        return this.replace(/[&<>"'\/]/g, function (s) {
            let entityMap = {
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;",
                '"': '&quot;',
                "'": '&#39;',
                "/": '&#x2F;'
            };
            return entityMap[s];
        });
    }
    if (typeof(String.prototype.escapeHtml) !== 'function') {
        String.prototype.escapeHtml = escapeHtml;
    }
})();

// Added 20220306°1031 from file 20190105°1717 daftari/jsi/trektautils.js
/**
 *  This function shortens a long string by placing ellipsis in the middle
 *
 * @id 20201203°1445
 * @callers •
 * @param {string} sOrig — The string to be shortened
 * @param {number} iMaxLen (integer) — The maximum lenght of the output string
 * @return {string} — The wanted shortened string
 */
// // Trekta.Utils.sConfine = function(sOrig, iMaxLen)
Mpd.sConfine = function(sOrig, iMaxLen)
{
    // // var sRet = '';
    let sRet = '';
    if (sOrig.length > iMaxLen) {
        sRet = sOrig.substring(0, iMaxLen / 2)
            + ' … '
            + sOrig.substring(sOrig.length - iMaxLen / 2)
        ;
    }
    sRet = sRet.split('\n').join('☼');
    sRet = sRet.split('<').join('&lt;');
    sRet = sRet.split('>').join('&gt;');
    return sRet;
};
