<?php

/**
 * This file stores the Configo class
 *
 * file      : 20110824°2121
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 * callers   : • The beginning of all response files • ~~eps015b/eps-*.php
 */

namespace Trekta\Daftari;
use Trekta\Daftari\Globals as Glb;

// [seq 20110824°2128]
Glb::outDbg_Fileinfo(__FILE__);

// [seq 20110824°2127]
if (! Configo::guaranteeConfiguration())
{
   $s = '<p>Error 20110826°2211.</p>';
   echo ($s);
   exit;
}

/**
 * This class provides the configuration read from an INI file
 *
 * @id 20110828°1921
 * @ref See howto 20190312°0711 'php singleton class'
 */
final class Configo
{
   /**
    * This constructor is private, so the class can only be instantiated by itself
    *
    * @id 20110828°1831
    */
   private final function __construct() {}

   /**
    * This method prohibits cloning by clone() from the outside
    *
    * @id 20110828°1832
    */
   private function __clone() {}

   /**
    * Private unserialize method to prevent unserializing of the Singleton instance
    *
    * @id 20190312°0751
    * @note Inserted after ref 20190312°0641 'stackoverflow: what is singleton'
    * @return void
    */
   public function __wakeup() {}                               // Set public after error "The magic method __wakeup() must have public visibility" [fix 20220606°1531 adjust for PHP8]

    /**
    * This static method returns the wanted instance
    *
    * @id 20110828°1833
    */
   public static function getInstance()
   {
      if (! isset(self::$oInstance))
      {
         $o = new self();                                      // Superfluous intermediat variable ? [fix 20190312°0632]
         self::$oInstance = $o;

         //-----------------------
         /*
         $b = self::$oInstance->readIni();                     //
         if ($b === FALSE) {
            return NULL;                                       // Provisory — todo : Implement exception
         }
         */
         //-----------------------
      }
      return self::$oInstance;
   }

   /**
    * This method .. the target page calls an editrange-edit-action, remember ..
    *
    * @id 20110830°1652
    * @param $sVarNam {String} The name of the wanted variable
    * @param $sSection {String} The optional inifile section to read from
    * @return {String} The wanted value
    */
   public function getValue($sVarNam, $sSection = 'Options')
   {
      $s = self::y_readIniVar ( $sVarNam
                               , $sSection
                                , Glb::$Cfg_sInifileFullname
                                 );
      return $s;
   }

   /**
    * This function ...
    *
    * @id 20110826°2221
    * @callers Only this file's script level
    * @return bool Success flag
    */
   public static function guaranteeConfiguration()
   {
      // ref : See issue 20190201°0805 'guarantee directory'
      if (! is_dir (Glb::$Ses_sDataDirFullname))
      {
         if (! mkdir(Glb::$Ses_sDataDirFullname))
         {
            $s = '<p class="phpError">Thing 20110826°2201 - No data directory.</p>';
            echo ($s);
            exit;
         }
      }

      return TRUE;
   }

   /**
    * This function writes settings to inifile outside the program folder
    *  — currently not used
    *
    * @id 20110821°1921
    * @note This method is not used now, but it may be a nice idea,
    *         just to write the POST array in bulk to an INI file
    * @callers • None anymore for now
    * @param Array $aPOST Associative array with the key/value pairs to write
    */
   function optionsWriteToSeparateInifile($aPOST)
   {

      // Provisory test [seq 20110824°2123]
      $cfg = self::getInstance();

      // Write back target file
      self::y_guaranteeInifile(Glb::$Cfg_sInifileFullname);
      foreach( $aPOST as $sKey => $sVal )
      {
         self::y_writeIniVar ( $sKey
                              , $sVal
                               , 'Options'
                                , Glb::$Cfg_sInifileFullname
                                 );
      }
   }

   /**
    * This method .. the target page calls an editrange-edit-action, remember ..
    *
    * @id 20110828°1922
    * @callers Only getInstance()
    * @note If not static, then caller gets warning : Non-static
    *    method Configura::readIni() should not be called statically.
    * @return bool Success flag
    */
   private static function readIni()
   {
      if ( ! is_file(Glb::$Cfg_sInifileFullname) )
      {
         self::$oInstance->writeCompleteConfiguration();
      }

      // [line 20120925°1113]
      self::$oInstance->cfg_canary_variable
               = self::y_readIniVar ( 'cfg_canary_variable'
                                     , 'Options'
                                      , Glb::$Cfg_sInifileFullname
                                       );

      return TRUE;
   }

   /**
    * This method shall write back one setting key/value pair
    *
    * @id 20110830°1651
    * @note See todo 20190127°0551 'provide variable write/read method'
    * @callers Only • SettingsPage.php::evaluate_post()
    * @param String $sVar The variable name to store
    * @param String $sVal The variable value to store
    * @param String $sSection The inifile's section
    * @return Boolean Success flag — it is a quick'n'dirty dummy, always returns TRUE
    */
   public static function setValue($sVar, $sVal, $sSection = 'Options')
   {

      // convert bool to string [seq 20120925°1911]
      // note : See todo 20190127°0551 provide variable write/read method
      // note : The ternary operator "$sVal = ($sVal) ? 'true' : 'false';"
      //    were no good idea. Mind the threefold-assignment-sign. The
      //    condition is built so possibly none of the two branches executes.
      if ($sVal === true)
      {
         $sVal = 'true';
      }
      else if ($sVal === false)
      {
         $sVal = 'false';
      }

      $s = self::y_writeIniVar ( $sVar
                                , $sVal
                                 , $sSection
                                  , Glb::$Cfg_sInifileFullname
                                   );

      return TRUE;
   }

   /**
    * This method shall bulk write back the complete configuration
    *  (but is a dummy method so far)
    *
    * @id 20110830°1241
    * @callers So far only readIni()
    * @return bool Success flag
    */
   private static function writeCompleteConfiguration()
   {
      $s = '';

      // [line 20120925°1111]
      $s .= self::y_writeIniVar ( 'cfg_canary_variable'
                                 , self::$oInstance->cfg_canary_variable
                                  , 'Options'
                                   , Glb::$Cfg_sInifileFullname
                                    );

      // Self test for existence [seq 20120510°1301]
      if (! isset(self::$oInstance->cfg_canary_variable)) {
         // This is not what is written when creating a new inifile! Is this superfluous?!
         self::$oInstance->cfg_canary_variable = 'false';
      }
      $s .= self::y_writeIniVar ( 'cfg_canary_variable'
                                 , self::$oInstance->cfg_canary_variable
                                  , 'Options'
                                   , Glb::$Cfg_sInifileFullname
                                    );

      // Paranoia [seq 20120510°1301]
      // todo: Implement some reaction on possible error
      if ($s !== '')
      {
         // error
      }

      return TRUE;
   }


   //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   //Bulk shifted here the complete content of (former) class Inivariables [log 20190312°0655]


   /**
    * This function guarantees an existing and valid inifile
    *
    * @id 20101101°1621
    * @note Do not use function utf8_encode() in subsequent fwrite(),
    *        otherwise the string will blow up in chinese.
    * @param $sFile {string} ...
    */
   public static function y_guaranteeInifile($sFile)
   {
      if (! file_exists($sFile))
      {
         $s = '[' . Glb::INI_sSECTION_General . ']';
         $s .= Glb::$sTkNL . Glb::INI_sINIKEY_Creationtime . '=' . 'jjjjmmtt.hhmmss';
         // notes :
         // - Umlauts given directly are written wrong here, but correctly when written via writeIniVar()
         // - The quotes are necessary with the non-ascii chars
         // - The hex sequence means '=Hello umlauts ÄäÖöÜüßéêè, how are you?'
         $s .= Glb::$sTkNL . Glb::INI_sINIKEY_TestUtf8 . "=\"Hello umlauts \xc3\x84\xc3\xa4\xc3\x96\xc3\xb6\xc3\x9c\xc3\xbc\xc3\x9f, how are you? \xd1\x88\xd1\x82\xd1\x80\xd0\xb0\xd1\x84!\"";
         $s .= Glb::$sTkNL;
         $f = fopen($sFile, 'w');
         $iWritten = fwrite($f, $s);
         fclose($f);
      }
   }

   /**
    * This function is the counterpart for above function strToHex().
    *
    * @id 20101114°0452
    * @param $hex {String} The number to convert into hexadecimal format
    * @return {String} The wanted decimal representation of the given hexadecimal number
    */
   private static function y_hexToStr($hex)
   {
      $string = '';
      for ($i = 0; $i < strlen($hex)-1; $i += 2)
      {
          $string .= chr(hexdec($hex[$i].$hex[$i+1]));
      }
      return $string;
   }

   /**
    * This function ...
    *
    * @id 20101101°1641
    * @param $sVarname {string} ...
    * @param $sSection {string} ...
    * @param $sFile {string} ...
    * @return string The read value
    */
   public static function y_readIniVar($sVarname, $sSection, $sFile)
   {
      if ( $sFile == '' )
      {
         $sFile = Glb::$Cfg_sInifileFullname;
      }
      if ( $sSection == '' )
      {
         $sSection = Glb::INI_sSECTION_General;                        // 'General';
      }
      if ( $sVarname == '' )
      {
         $sValue = '';
         return $sValue;
      }

      self::y_guaranteeInifile($sFile);

      // TRUE means to work with sections
      $a = parse_ini_file($sFile, TRUE);

      // Paranoia, otherwise error 'undefined index' below
      if ( ! isset($a[$sSection][$sVarname]) )
      {
         $a[$sSection][$sVarname] = '';
         // Check — Now possibly write this variabel back to file?
      }

      $sValue = $a[$sSection][$sVarname];

      // Workaround 20101114°0453
      if ( $sVarname == Glb::INI_sINIKEY_sSqlCommand )
      {
         $sValue = self::y_hexToStr($sValue);
      }

      return $sValue;
   }

   /**
    * This function converts a .. string to a hex string
    *
    * @id 20101114°0451
    * @note Functions after ref 20101114°0443 'ditio.net → php string-to-hex and hex-to-string'
    * @see ref 20101114°0442 'pgregg → hexstr to ascii string to hex string'
    * @note Why such function? With parse_ini_file(), any non-alphanumerical
    *    characters disturb. Not even encode_base64() can be used because that
    *    uses '==' at end of lines .. so we had to retreat to such heavy means.
    * @param $string {string} The number to convert
    * @return string The wanted hexadecimal representation of the given number
    */
   private static function y_strToHex($string)
   {
      $hex = '';
      for ($i = 0; $i < strlen($string); $i++)
      {
         $iAsci = ord($string[$i]);
         if ($iAsci < 16)
         {
            $hex .= '0';
         }
         $hex .= dechex($iAsci);
      }
      return $hex;
   }

   /**
    * This function writes one inivariable
    *
    * @id 20101101°1642
    * @note Linebreaks must not be '\r\n' but '\n', otherwise PHP parse_ini_file() will fail
    * @note CHECK - provide more lines with paranoia
    * @param $sVarname {String} ...
    * @param $sValue {String} ...
    * @param $sSection {String} ...
    * @param $sFile {String} ...
    * @return String Blank on success, error message on error
    */
   public static function y_writeIniVar($sVarname, $sValue, $sSection, $sFile)
   {
      $bWRITE_VALUES_WITH_QUOTES = TRUE;                               // [line 20110830°1222]

      // Complete input parameters
      if ( $sFile == '' )
      {
         $sFile = Glb::$Cfg_sInifileFullname;                          // '*.ini'
      }
      if ( $sSection == '' )
      {
         $sSection = Glb::INI_sSECTION_General;                        // 'General';
      }
      if ( $sVarname == '' )
      {
         $sErr = 'ERROR - missing inivariable name';
         return $sErr;
      }

      // Paranoia
      self::y_guaranteeInifile($sFile);

      // [workaround 20101114°0454]
      global $sFWD_INIVAR_SQLCOMMAND;
      if ($sVarname == $sFWD_INIVAR_SQLCOMMAND)
      {
         $sValue = self::y_strToHex($sValue);
      }

      // () Get original ini array
      // See ref 20110830°1221 'php → function parse ini file' with explanation
      // note : Let's see what happens, if we write-back the file using
      //         quotes around the values [note 20110830°1222]
      $aInifile = parse_ini_file($sFile, TRUE);                        // TRUE = work with sections

      // () Set or add wanted value to ini array
      $aInifile[$sSection][$sVarname] = $sValue;

      // Translate array to string to be written back to file
      $sInifile = '';
      $aSectionkeys = array_keys($aInifile);
      foreach ($aSectionkeys as $sSection)
      {
         if ($sInifile !== '')
         {
            $sInifile .= Glb::$sTkNL . Glb::$sTkNL;
         }
         $sInifile .= '[' . $sSection . ']';
         $aVarikeys = array_keys($aInifile[$sSection]);

         // note: For sorting case-insensitive see posting 07-May-2011 12:37
         // by Anon at www.php.net/manual/en/function.asort.php [ref 20110825°0121]
         $b = asort($aVarikeys);

         foreach ($aVarikeys as $sKey)
         {
            if ($bWRITE_VALUES_WITH_QUOTES)
            {
               $sInifile .= Glb::$sTkNL . $sKey . '="' . $aInifile[$sSection][$sKey] . '"';
            }
            else
            {
               $sInifile .= Glb::$sTkNL . $sKey . '=' . $aInifile[$sSection][$sKey];
            }
         }
      }
      $sInifile .= Glb::$sTkNL;

      // Write updated array back to file
      $f = fopen($sFile, 'w');
      if (! $f)
      {
         $sErr = 'ERROR - File open failed, file = ' . $sFile;
         return $sErr;
      }
      $iWritten = fwrite($f, $sInifile);
      if (! $iWritten)
      {
         $sErr = 'ERROR - File write failed, file = ' . $sFile;
         return $sErr;
      }
      $b = fclose($f);
      if (! $b)
      {
         $sErr = 'ERROR - File close failed, file = ' . $sFile;
         return $sErr;
      }

      return '';
   }

   //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


   // The configuration variables with default values.
   // This is step one in the setting chain.
   // The class properties delclaration, no variables allowed.

   /**
    * @id 20110828°1834
    * @var {String} Dummy value is written when creating the pristine inifile
    */
   public $cfg_canary_variable = 'true'; // 'false'; // 'off';

   /**
    * @id 20190312°0651
    * @var {String} The current spider item
    */
   public $cfg_spider_current = ''; // 'false'; // 'off';

   /**
    * This field ..
    *
    * @id 20110828°1835
    * @var Object The one created instance
    */
   private static $oInstance;

}

/* eof */
