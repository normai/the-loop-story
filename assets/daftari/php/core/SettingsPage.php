<?php

/**
 * This module builds the text body for page settings.html
 *
 * file      : 20120826°1551
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 * callers   :
 */

namespace Trekta\Daftari; // [namespace 20190128°0421]

use Trekta\Daftari\Globals as Glb;

include_once(__DIR__ . '/../core/Session.php');                        // Must be first line of code [line 20121028°181115]
include_once(__DIR__ . '/../sitemop/DbTxt.php');                       // Is this really wanted? Probably not. [line 20140620°1331]

// Call module code [line 20190202°0731]
SettingsPage::makeHtml();

/**
 * This class generates the payload HTML for the Settings Page
 *
 * @id 20190202°0711
 */
class SettingsPage
{

   /**
    * This function evaluates the page post event
    *
    * @id 20110507°2111 [[20110507°2111]]
    * @callers ..
    */
   private static function evaluate_post()
   {
      $sCR = Glb::$sTkNL;

      // () Read checkboxes [seq 20110507°2113]
      if (! isset($_POST[Glb::POSTKEY_SET_settings_checkboxes]))       // 'settings_checkboxes'
      {
         // Otherwise errors below if nothing is checked
         $_POST[Glb::POSTKEY_SET_settings_checkboxes] = array();       // 'settings_checkboxes'
      }
      $_SESSION[Glb::SESKEY_bDebug] = ( in_array
                                     ( 'debug' // 'debug'
                                      , $_POST[Glb::POSTKEY_SET_settings_checkboxes] // 'settings_checkboxes'
                                       )) ? true : false
                                        ;
      $_SESSION[Glb::SESKEY_bMore010_sessionInfo] = ( in_array
                                                   ( 'more' // 'more'
                                                    , $_POST[Glb::POSTKEY_SET_settings_checkboxes] // 'settings_checkboxes'
                                                     )) ? true : false
                                                      ;
      $_SESSION[Glb::SESKEY_bMore060inivarCanary] = ( in_array
                                                   ( 'more060inivarCanary' // 'more060inivarCanary'
                                                    , $_POST[Glb::POSTKEY_SET_settings_checkboxes] // 'settings_checkboxes'
                                                     )) ? true : false
                                                      ;
      $_SESSION[Glb::SESKEY_bReadlocaldrive] = ( in_array
                                              ( 'accesslocaldrive' // 'accesslocaldrive'
                                               , $_POST[Glb::POSTKEY_SET_settings_checkboxes] // 'settings_checkboxes'
                                                )) ? true : false
                                                 ;
      $_SESSION[Glb::SESKEY_MaintenanceMode] = ( in_array
                                              ( Glb::SESKEY_MaintenanceMode
                                               , $_POST[Glb::POSTKEY_SET_settings_checkboxes] // 'settings_checkboxes'
                                                )) ? true : false
                                                 ;

      // () Read other settings [seq 20110507°2115]
      $_SESSION[Glb::SESKEY_iAlbumColumns] = $_POST[Glb::POSTKEY_GLR_settings_album_columns]; // 'settings_album_columns'
      $_SESSION[Glb::SESKEY_iAlbumMaxRows] = $_POST[Glb::POSTKEY_GLR_seetings_album_maxrows]; // 'settings_album_maxrows'
      $_SESSION[Glb::SESKEY_sAlbumThumbsize] = $_POST[Glb::SESKEY_sAlbumThumbsize];
      $_SESSION[Glb::SESKEY_iObjectImagesize] = $_POST[Glb::SESKEY_iObjectImagesize]; // experimental re-use of global constant [line 20110514°1331]

      // () Experiment [seq 20120925°1121]
      include_once(__DIR__ . '/../core/Configo.php');
      $cfg = Configo::getInstance();
      $cfg->cfg_canary_variable = $_SESSION[Glb::SESKEY_bMore060inivarCanary]; // todo : Make this be calculated automatically?
      $cfg->setValue( 'cfg_canary_variable'
                     , $_SESSION[Glb::SESKEY_bMore060inivarCanary]
                      );
      return;
   }

   /**
    * This function generates the payload HTML for the Settings Page
    *
    * @id 20190202°0721
    */
   public static function makeHtml()
   {

      $sCR = Glb::$sTkNL;

      Glb::$Ses_bIsLoggedin = FALSE;                                   // Is initialized as FALSE anyway
      if (isset($_SESSION[Glb::SESKEY_login]))
      {
         Glb::$Ses_bIsLoggedin = TRUE;
      }

      include_once(__DIR__ . '/../core/Configo.php');
      $cfg = Configo::getInstance();

      // React on form postings
      if (isset($_POST[Glb::POSTKEY_SET_settings_form_was_sent]))      // 'settings_form_was_sent'
      {
         self::evaluate_post();
      }

      // () Start html generation [seq 20120826°1553]
      $sOut = '';

      // Supplement [seq 20120925°1131]
      // note : The form ID is wanted in settings.html
      $sOut .= $sCR . '<form action="" method="post" style="text-align:left;" id="id20201228o1111">';
      $sOut .= $sCR . '<input type="hidden" name="settings_form_was_sent" value="yes">';

      $sOut .= $sCR . ' <table border="0" width="98%">';

      // () Settings section
      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td colspan="3">';
      $sOut .= $sCR . '                <b>Server</b>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td colspan="2">';
      $sOut .= $sCR . '               <input type="checkbox" name="settings_checkboxes[]" value="more010"'
                                       . ($_SESSION[Glb::SESKEY_bMore010_sessionInfo] ? ' checked ' : ' ')
                                        . '> Show session infos'
                                         ;
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td colspan="2">';
      $sOut .= $sCR . '               <input type="checkbox" name="settings_checkboxes[]" value="more020"'
                                       . ($_SESSION[Glb::SESKEY_bMore020] ? ' checked ' : ' ')
                                        . '> Show debug sScriptInfo <span style="color:Moccasin;">(Moccasin pane)</span>'
                                         ;
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td colspan="2">';
      $sOut .= $sCR . '               <input type="checkbox" name="settings_checkboxes[]" value="more030"'
                                       . ($_SESSION[Glb::SESKEY_bMore030] ? ' checked ' : ' ')
                                        . '> Show debug Glb::$Ses_sLocalhostInfo <span style="color:LightCoral;">(LightCoral pane)</span>'
                                         ;
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td colspan="2">';
      $sOut .= $sCR . '               <input type="checkbox" name="settings_checkboxes[]" value="more040"'
                                       . ($_SESSION[Glb::SESKEY_bMore040] ? ' checked ' : ' ')
                                        . '> Show debug sJs e.g. javascript info()'
                                         ;
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      //() Settings section
      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td style="color:silver;">';
      $sOut .= $sCR . '                <b>Albums&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td colspan="2">';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      // () Determine album dimensions display (pretty tortuous)
      $sTextbrickAlbumColumns = '';
      $sTextbrickAlbumRows = '';
      $aAlbumColumns = array(0 => 2, 1 => 3, 3 => 4, 4 => 5, 5 => 6, 6 => 7, 7 => 8 );
      $aAlbumRows = array(0 => 2, 1 => 3, 2 => 4, 3 => 5, 4 => 6, 5 => 7, 6 => 8, 7 => 9, 8 => 10, 9 => 11, 10 => 12, 11 => 13, 12 => 14, 13 => 15, 14 => 16 );
      // ()
      foreach ($aAlbumColumns as $i)
      {
         if (! ($i == $_SESSION[Glb::SESKEY_iAlbumColumns]))
         {
            $sTextbrickAlbumColumns .= '<option>' . $i . '</option>';
         }
         else
         {
            $sTextbrickAlbumColumns .= '<option selected>' . $i . '</option>';
         }
      }
      // ()
      foreach ($aAlbumRows as $i)
      {
         if (! ($i == $_SESSION[Glb::SESKEY_iAlbumMaxRows]))
         {
            $sTextbrickAlbumRows .= '<option>' . $i . '</option>';
         }
         else
         {
            $sTextbrickAlbumRows .= '<option selected>' . $i . '</option>';
         }
      }

      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td align="right">';
      $sOut .= $sCR . '               Spalten je Blatt :';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '               <select name="settings_album_columns">';
      $sOut .= $sCR . '                   ' . $sTextbrickAlbumColumns;
      $sOut .= $sCR . '               </select>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td align="right">';
      $sOut .= $sCR . '               Zeilen je Blatt :';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '               <select name="settings_album_maxrows">';
      $sOut .= $sCR . '                  ' . $sTextbrickAlbumRows;
      $sOut .= $sCR . '               </select>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      // Determine thumbsize display (pretty tortuous)
      $sTextbrickThumbsizeOptions = '';
      foreach (Glb::GLR_ENUM_THUMBSIZES as $s)                         // array('Small', 'Medium', 'Large')
      {
         if (! ($s == $_SESSION[Glb::SESKEY_sAlbumThumbsize]))
         {                                                             // 'sAlbumThumbsize'
            $sTextbrickThumbsizeOptions .= '<option>' . $s . '</option>';
         }
         else
         {
            $sTextbrickThumbsizeOptions .= '<option selected>' . $s . '</option>';
         }
      }

      $sTextbrickSingleimageSizeOptions = '';
      $aSinglesizes = array(0 => '360', 1 => '540', 2 => '630', 3 => '720', 4 => '900', 5 => '1080');
      foreach ($aSinglesizes as $s)
      {
         if (! ($s == $_SESSION[Glb::SESKEY_iObjectImagesize]))        // 'iSingleimagesize'
         {
            $sTextbrickSingleimageSizeOptions .= '<option>' . $s . '</option>';
         }
         else
         {
            $sTextbrickSingleimageSizeOptions .= '<option selected>' . $s . '</option>';
         }
      }

      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td align="right">';
      $sOut .= $sCR . '               Album Bildgröße :';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '               <select name="' . Glb::SESKEY_sAlbumThumbsize . '">';
      $sOut .= $sCR . '                   ' . $sTextbrickThumbsizeOptions;
      $sOut .= $sCR . '               </select>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td align="right">';
      $sOut .= $sCR . '               Einzelbild Größe :';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '               <select name="' . Glb::SESKEY_iObjectImagesize . '">'; // experimental re-use of global constant [line 20110514°1331]
      $sOut .= $sCR . '                   ' . $sTextbrickSingleimageSizeOptions;
      $sOut .= $sCR . '               </select>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      // Canary
      $bCanary = ($cfg->cfg_canary_variable == 'true') ? true : false ;
      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td>';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td colspan="2">';
      $sOut .= $sCR . '               <input type="checkbox" name="settings_checkboxes[]" value="more060inivarCanary"' . ($bCanary ? ' checked ' : ' ') . '> Inivar Canary';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      if ( Glb::$Ses_bIsLoggedin )
      {
         $sOut .= $sCR . '  <tr>';
         $sOut .= $sCR . '   <td colspan="2"    >';
         $sOut .= $sCR . '                <hr style="border:solid #ffc0ff 1px;" />';
         $sOut .= $sCR . '                Erweitert';
         $sOut .= $sCR . '   </td>';
         $sOut .= $sCR . '  </tr>';

         if ( Glb::$Lgn_bHostIsLocalhost )
         {
            $sOut .= $sCR . '  <tr>';
            $sOut .= $sCR . '   <td>';
            $sOut .= $sCR . '   </td>';
            $sOut .= $sCR . '   <td>';
            $sOut .= $sCR . '               <input type="checkbox" name="settings_checkboxes[]" value="' . 'accesslocaldrive' . '"' . ($_SESSION[Glb::SESKEY_bReadlocaldrive] ? ' checked ' : ' ') . '> Read from local drive';
            $sOut .= $sCR . '   </td>';
            $sOut .= $sCR . '  </tr>';

            $sOut .= $sCR . '  <tr>';
            $sOut .= $sCR . '   <td>';
            $sOut .= $sCR . '   </td>';
            $sOut .= $sCR . '   <td>';
            $sOut .= $sCR . '               <input type="checkbox" name="settings_checkboxes[]" value="' . Glb::SESKEY_MaintenanceMode . '"' . ($_SESSION[Glb::SESKEY_MaintenanceMode] ? ' checked ' : ' ') . '> Maintenance mode';
            $sOut .= $sCR . '   </td>';
            $sOut .= $sCR . '  </tr>';
         }
         else
         {
            // EXPERIMENTAL - works but is tedious (if field were not printed at all, the value would be reset to false on each submit)
            $sOut .= $sCR . '  <tr>';
            $sOut .= $sCR . '   <td>';
            $sOut .= $sCR . '   </td>';
            $sOut .= $sCR . '   <td>';
            $sOut .= $sCR . '    <input type="hidden" name="settings_checkboxes[]" value="' . 'accesslocaldrive' . '"' . ($_SESSION[Glb::SESKEY_bReadlocaldrive] ? ' checked ' : ' ') . '>';
            $sOut .= $sCR . '    <input type="hidden" name="settings_checkboxes[]" value="' . Glb::SESKEY_bReadlocaldrive . '"' . ($_SESSION[Glb::SESKEY_MaintenanceMode] ? ' checked ' : ' ') . '>';
            $sOut .= $sCR . '   </td>';
            $sOut .= $sCR . '  </tr>';
         }
      }
      else
      {
         // Experimental — works but is tedious (if field were not printed at all, the value would be reset to false on each submit)
         $sOut .= $sCR . '  <tr>';
         $sOut .= $sCR . '   <td>';
         $sOut .= $sCR . ' <input type="hidden" name="settings_checkboxes[]" value="' . 'accesslocaldrive' . '"' . ($_SESSION[Glb::SESKEY_bReadlocaldrive] ? ' checked ' : ' ') . '>';
         $sOut .= $sCR . ' <input type="hidden" name="settings_checkboxes[]" value="' . Glb::SESKEY_bReadlocaldrive . '"' . ($_SESSION[Glb::SESKEY_MaintenanceMode] ? ' checked ' : ' ') . '>';
         $sOut .= $sCR . '   </td>';
         $sOut .= $sCR . '  </tr>';
      }

      $sOut .= $sCR . '  <tr>';
      $sOut .= $sCR . '   <td colspan="2">';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '   <td style="text-align:right;">';
      $sOut .= $sCR . '               <input type="submit" name="login_button_enter" value=" &Auml;nderungen Best&auml;tigen ">';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr>';

      if (($_SESSION[Glb::SESKEY_bMore010_sessionInfo] === true) or ($_SESSION[Glb::SESKEY_bDebug] === true))
      {
         $sOut .= $sCR . '  <tr>';
         $sOut .= $sCR . '   <td colspan="3">';
         $sOut .= $sCR . '           <hr style="border:solid #ffc0ff 1px;" />';
         $sOut .= $sCR . '   </td>';
         $sOut .= $sCR . '  </tr>';
      }

      if ($_SESSION[Glb::SESKEY_bMore010_sessionInfo] === true)
      {
         $sOut .= $sCR . '  <tr>';
         $sOut .= $sCR . '   <td>';
         $sOut .= $sCR . '                 Session-Infos';
         $sOut .= $sCR . '   </td>';
         $sOut .= $sCR . '   <td colspan="2">';
         $sOut .= $sCR . '            <br> Page requests = ' . $_SESSION[Glb::SESKEY_requestcount];
         $sOut .= $sCR . '            <br> Privileges = ' . $_SESSION[Glb::SESKEY_privileges];
         $sOut .= $sCR . '   </td>';
         $sOut .= $sCR . '  </tr>';
      }
      if ($_SESSION[Glb::SESKEY_bDebug] === true)
      {
         $sOut .= $sCR . '  <tr>';
         $sOut .= $sCR . '   <td>';
         $sOut .= $sCR . '                 Debug-Infos';
         $sOut .= $sCR . '   </td>';
         $sOut .= $sCR . '   <td colspan="2">';
         $sOut .= $sCR . '            <br> Session ID = ' . session_id();
         $sOut .= $sCR . '            <br> Loginhash = ' . ((isset($_SESSION[Glb::SESKEY_login])) ? ($_SESSION[Glb::SESKEY_login]) : 'n/a');
         $sOut .= $sCR . '            <br> Privileges = ' . ($_SESSION[Glb::SESKEY_privileges] ?? ''); // Using '??' avoids PhanTypeArraySuspiciousNullable [line 20190312°0111`04]
         $sOut .= $sCR . '   </td>';
         $sOut .= $sCR . '  </tr>';
      }

      $sOut .= $sCR . '  <!-- tr>';
      $sOut .= $sCR . '   <td colspan="3">';
      $sOut .= $sCR . '              <hr style="border:solid #ffc0ff 1px;" />';
      $sOut .= $sCR . '   </td>';
      $sOut .= $sCR . '  </tr -->';

      $sOut .= $sCR . ' </table>';
      $sOut .= $sCR . '</form>';

      self::$sOutput = $sOut;
   }

   /**
    * This field tells the wanted output of func makeHtml
    *
    * @id field 20190202°0741
    * @var String
    */
   public static $sOutput = '';
}

/* eof */
