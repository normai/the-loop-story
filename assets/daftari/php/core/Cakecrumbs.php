<?php

/**
 * This module builds and inserts a breadcrumb paragraph
 *
 * The cakecrumbs are made from a CSV file breadcrumbsdef.txt residing
 *  in the sitemap folder, means the folder where sitmapdaf.js resides.
 *
 * Usage. This feature is obsolet by the JavaScript Cakecrumbs, but it shall
 *  continue to run on page 20111231°0411 daftari/docs/cakecrumbs3.html.
 *
 * file        : 20120917°1711
 * license     : GNU AGPL v3
 * copyright   : © 2012 - 2023 Norbert C. Maier
 * authors     : ncm
 * status      : Archival, reserve
 * encoding    : UTF-8-without-BOM
 * note        : Remember issue 20120816°1011 'php with files larger 2 gb'
 * callers     : HTML pages from their cakecrumbs insertion point
 */

namespace Trekta\Daftari; // [namespace 20190128°0421]

use Trekta\Daftari\Globals as Glb;

/**
 * This class defines the properties of one webpage.
 *
 * @id 20120814°1011
 * @callers
 */
class Cakecrumb
{
   /**
    * The ID of the crumb
    *
    * @id 20120825°0203
    * @var String
    */
   public $sId = '';

   /**
    * @id 20120825°0204
    * @var String The parent crumb of this crumb
    */
   public $sParent = '';

   /**
    * @id 20120825°0205
    * @var String The path where the Crumb points to
    */
   public $sPath = '';

   /**
    * @id 20120825°0206
    * @var String The Crumb itself
    */
   public $sCrumb = '';

   /**
    * @id 20120825°0207
    * @var String The title of the Crumb
    */
   public $sTitle = '';

   /**
    * @id 20120825°0213
    * @var String Optional e.g. '<i>'
    */
   public $sStyleOpen = '';

   /**
    * @id 20120825°0214
    * @var String Optional e.g. '</i>'
    */
   public $sStyleClose = '';

   /**
    * @id 20120825°0215
    * @var String Optional either '' or 'extern'
    */
   public $sLinkExternal = '';

   /**
    * @id 20120825°0216
    * @var String Optional e.g. 'http://www.any-other-page.lum'
    */
   public $sOverwriteLink = '';

   /**
    * @id 20120825°1452
    * @var String Optional e.g. '../index' as alternative link, if crumb is thispage
    */
   public $sOverwriteSelf = '';

   /**
    * @id 20120920°1411 proposal
    * @var String Optional property e.g. 'all', 'login', 'member', 'admin'
    */
   public $sRights = '';
}

/**
 * This class defines the properties of one website's breadcrumb set
 *
 * @id 20120814°1021
 * @note The breadcrumb set is seen from one specific page.
 * @callers
 */
class CakecrumbSet
{
   /**
    * @id 20120814°1022
    * @var Array Array with the Cakecrumbs the set consists of
    */
   public $pages = array();

   /**
    * @id 20120814°1023
    * @var String Set when reading the CSV file, used when building the crumbs block
    */
   public $sResidence = '';

   /**
    * @id 20120814°1024
    * @var String The page for navigating to 'previous'
    */
   public $sPageBefore = '';

   /**
    * @id 20120814°1025
    * @var String The page for navigating to 'next'
    */
   public $sPageNext = '';

   /**
    * @id 20120814°1026
    * @var String The page for navigating 'up'
    */
   public $sPageParent = '';

   /**
    * @id 20120814°1027
    * @var String The current pages page number
    */
   public $sPageNumber = '';
}

/**
 * This class shall provide the PHP Cakecrumbs
 *
 * @id 20190129°0811
 * @callers
 */
class Cakecrumbs
{

   /**
    * This function builds the breadcrumb for/from a website object.
    *
    * @id 20120814°1401
    * @callers getBreadcrumbs()
    * @param $bcs {CakecrumbSet} The breadcrumb set to build a HTML fragment
    * @return string The wanted HTML fragment
    */
   static private function buildBreadcrumbs($bcs)
   {
      // Determine this pages script path [seq 20120814°1402]
      $sScriptfile = Glb::$Env_sRequestedScriptName;

      // () Extract the page path
      // note: $sScriptfile is e.g.: '/index.html' or '/subdom/index.html',
      //        $bcs->sResidence is e.g. '' or '/subdom'
      $sThispage = substr($sScriptfile, strlen($bcs->sResidence) + 1);

      // Search this page in the cakecrumbs definition array
      $i = 0;
      foreach($bcs->pages as $p)
      {
         // E.g. $sThispage = "cakecrumbs3.html"
         // E.g. $p->sPath = "../../index.html"
         if ($p->sPath == $sThispage)
         {
            $thispage = $p;

            if ($i > 0)
            {
               $bcs->sPageBefore = $bcs->pages[$i - 1]->sPath;
            }
            if ($i < count($bcs->pages) - 1)
            {
               $bcs->sPageNext = $bcs->pages[$i + 1]->sPath;
            }
            $bcs->sPageNumber = '' . ($i + 1);

            $bcs->sPageParent = $bcs->pages[$i]->sParent;
         }
         $i++;
      }

      // Possibly the breadcrumb definitions do not even exist
      if (! isset($thispage))
      {
         $s = '<p>(No cakecrumbs.)</p><hr />';
         return $s;
      }

      // Calculate folder-delfing prefix from actual page to cakecrumbs folder
      // todo : Make more intelligent
      // note : The same value is retrieved in retrieveBreadcrumbDefinitions()
      $iCount = substr_count($thispage->sPath, '/');
      $sPrefix = '';
      for ($i = 1; $i <= $iCount; $i++)
      {
         $sPrefix = $sPrefix . '../';
      }
      //echo('<p>[Debug 20140602°1213] $sPrefix = ' . $sPrefix . '</p>');

      // Quick fix
      $bcs->sPageBefore = $sPrefix . $bcs->sPageBefore;
      $bcs->sPageNext = $sPrefix . $bcs->sPageNext;
      $bcs->sPageParent = $sPrefix . $bcs->sPageParent;

      // Build siblings/parents/etc. lines
      // [note 20120825°1551] Below conditions are somewhat obscure. I think this
      //    has to do with the exception, that the 1 home element is not on a line for
      //    itself, but as opposed to the others, on the same line with it's childs.
      $sParentId = $thispage->sParent;
      $sFormerParent = '';
      $aLines = array();
      while (Glb::bToggle_TRUE)
      {
         // Before looping, determine grandparent
         $sGrandParentId = '';
         foreach($bcs->pages as $p)
         {
            if ($p->sId == $sParentId)
            {
               $sGrandParentId = $p->sParent;
            }
         }

         // Build parent line (only 'home')
         // First loop over siblings, then parents, grandparents, so on
         // [todo 20120825°0221] Why translate original properties into array aCrumb?! Possibly eliminate $aCrumb.
         $aCrumb = array();
         $aLine = array();
         foreach($bcs->pages as $p)
         {
            if ($p->sParent == $sParentId)
            {
               // Sequence redundand with below
               //----------------------------------------------
               if ($p->sOverwriteLink == '')
               {
                  $aCrumb['target'] = $sPrefix . $p->sPath;
               }
               else
               {
                  $aCrumb['target'] = $p->sOverwriteLink;
               }
               $aCrumb['text'] = $p->sCrumb;
               $aCrumb['stylop'] = $p->sStyleOpen;                     // Additional style opening e.g. '<i>'
               $aCrumb['stylcl'] = $p->sStyleClose;                    // Additional style closing e.g. '</i>'
               $aCrumb['addatt'] = $p->sLinkExternal;                  // Additional attribute e.g. 'target="_blank"'
               //----------------------------------------------

               // This is the difference between sequence below
               if ($p->sId == $thispage->sId)
               {
                  $aCrumb['style'] = 'self';
               }
               else if ($p->sId == $sFormerParent)
               {
                  $aCrumb['style'] = 'bold';
               }
               else
               {
                  $aCrumb['style'] = 'plain';
               }

               $aLine[] = $aCrumb;
            }
         }
         array_unshift($aLines, $aLine);                               // Insert at the beginning

         // Assemble found sibling/parent lines
         if ($sParentId == '')
         {
            break;
         }

         // Prepare next level
         $sFormerParent = $sParentId;
         $sParentId = $sGrandParentId;
      }

      // Build possible childs line
      // [todo 20120825°0211] Possibly eliminate $aCrumb or at least convert it into a class?!
      $aLine = array();
      foreach($bcs->pages as $p)
      {
         // note 20140602°1211 : When using '==' operator, NetBeans hints: "comparison
         //     with equal op ('==') should be avoided, using identical op ('===') instead"
         if ($p->sParent === $thispage->sId)  // [condi 20140602°1212] empirical try, seems fine
         {

            // Sequence redundand with above!
            //-------------------------------------------------
            if ($p->sOverwriteLink == '')
            {
               $aCrumb['target'] = $sPrefix . $p->sPath;
            }
            else
            {
               $aCrumb['target'] = $p->sOverwriteLink;
            }
            $aCrumb['text'] = $p->sCrumb;
            $aCrumb['stylop'] = $p->sStyleOpen;                        // Additional style opening e.g. '<i>'
            $aCrumb['stylcl'] = $p->sStyleClose;                       // Additional style closing e.g. '</i>'
            $aCrumb['addatt'] = $p->sLinkExternal;                     // Additional attribute e.g. 'target="_blank"'
            //-------------------------------------------------

            // This line is the difference between sequence above
            $aCrumb['style'] = 'plain';

            $aLine[] = $aCrumb;
         }
      }
      $aLines[] = $aLine;

      // Assemble the crumb HTML block from lines/line/crumbs arrays
      // todo : Possibly put arrays building and html assembling in separate functions
      $sBlock = '';
      $iCount = 0;
      foreach($aLines as $aLine)
      {
         $sLine = '';
         foreach($aLine as $aCrumb)
         {
            // Set crumb appearance
            $sLinkOpenOpen = '   <a href=';
            $sLinkOpenClose = '>';
            $sLinkClose = '</a>';
            $sBoldOpen = '';
            $sBoldClose = '';
            if ($aCrumb['style'] == 'self')
            {
               // [feature 20120825°1451]
               // Exception for the situation that on the home, we don't
               //  want a non-link crumb, but link to another 'home' one
               //  level deeper outside the actual domain/alias/site.
               if ($thispage->sOverwriteSelf == '')
               {
                  $sLinkOpenOpen  = '   <!-- a href=';
                  $sLinkOpenClose = ' -->';
                  $sLinkClose     = '<!-- /a -->';
                  $sBoldOpen = '<b>';
                  $sBoldClose = '</b>';

               }
               else
               {
                  $sLinkOpenOpen  = '   <a href=';
                  $sLinkOpenClose = '>';
                  $sLinkClose     = '</a>';
                  $sBoldOpen = '<i>';
                  $sBoldClose = '</i>';
                  $aCrumb['target'] = $thispage->sOverwriteSelf;
               }
            }
            else if ($aCrumb['style'] == 'bold')
            {
               $sBoldOpen = '<b>';
               $sBoldClose = '</b>';
            }
            else if ($aCrumb['style'] == 'plain')
            {
            }
            else
            {
               $sLine = $sLine . '[Error]';
               break;
            }

            // Assemble the line
            $sLine = $sLine . Glb::$sTkNL
                    . $sLinkOpenOpen . '"' . $aCrumb['target'] . '"'
                     . $aCrumb['addatt'] . $sLinkOpenClose
                      . $sBoldOpen . $aCrumb['stylop']
                       . $aCrumb['text'] . $aCrumb['stylcl']
                        . $sBoldClose . $sLinkClose . ' &nbsp;'
                         ;
         }

         if ($iCount == 0)
         {
            $sBlock = '<p class="topnavi1" data-wafailiskip="yes">' . $sLine . ' - &nbsp;';
            if (count($aLines) < 2)
            {
               // Guarantee closing tag if only root page exists (todo : test this line also)
               $sBlock = $sBlock . Glb::$sTkNL . '</p>';
            }
         }
         else if ($iCount == 1)
         {
            $sBlock = $sBlock . $sLine . Glb::$sTkNL . '</p>';
         }
         else
         {
            $i = $iCount;
            if ($iCount > 4)
            {
               $i = 4;
            }
            $sBlock = $sBlock . Glb::$sTkNL . '<p class="topnavi' . $i
                     . '" data-wafailiskip="yes">' . $sLine
                      . Glb::$sTkNL . '</p>'
                       ;
         }
         $iCount++;
      }
      return $sBlock;
   }

   /**
    * This function ...
    *
    * @id 20120917°1721
    * @callers Only func 20120917°1712 Go::wafailiBrick_Breadcrumbs() in daftari.php
    * @see issue 20190312°0411 'navigation arrows broken for satelite sites'
    */
   static public function getBreadcrumbs()
   {

      // Find and read the definition file
      $bcs = self::retrieveBreadcrumbDefinitions();

      $sTbl = '<table border="0" width="98%">' . Glb::$sTkNL . ' <tr>' . Glb::$sTkNL . '<td data-wafailiskip="yes" style="border:none;">';
      $s1 = Cakecrumbs::buildBreadcrumbs($bcs);
      $sTbl .= $s1;
      $sTbl .= Glb::$sTkNL . '</td><td data-wafailiskip="yes" style="border:none;">';

      // note : The plain width of the arrows is 88px, but e.g. IE makes
      //  borders about 2px wide, so we need some more: 2*2*3 = 12px more.
      $sTbl .= Glb::$sTkNL . '</td><td data-wafailiskip="yes" style="border:none; text-align:right; width:100px;">';
      $sTbl .= Glb::$sTkNL . '<span style="color:silver;">';
      $sTbl .= Glb::$sTkNL . '<a href="' . $bcs->sPageBefore . '">'
             . '<img src="' . Glb::$Glr_sLocation_Daftari_Url          // fix 20190421°0123
              . '/jsi/imgs23/20081114o1721.navi-arrow-left.png.gif"'
               . ' width="32" height="24" alt="Goto previous page">'
                . '</a>'
                 ;
      $sTbl .=        '<img src="' . Glb::$Glr_sLocation_Daftari_Url   // fix 20190421°0123
             . '/jsi/imgs23/20081114o1727.navi-arrow-up-wide.gif"'
              . ' width="24" height="24" alt="Goto parent page">'
               ;
      $sTbl .=        '<a href="' . $bcs->sPageNext . '">'
             . '<img src="' . Glb::$Glr_sLocation_Daftari_Url          // fix 20190421°0123
              . '/jsi/imgs23/20081114o1722.navi-arrow-right.png.gif"'
               . ' width="32" height="24" alt="Goto next page">'
                . '</a>'
                 ;
      $sTbl .= Glb::$sTkNL . '<br />'
             . '<small>Page ' . $bcs->sPageNumber
              . ' of ' . count($bcs->pages) . '</small>'
               ;
      $sTbl .= Glb::$sTkNL . '</span>';
      if (Glb::$Ses_sUserShortName !== '')
      {
         $sTbl .= '<br /> ' . Go::wafailiBrick_UserInfo() . Glb::$sTkNL;
      }
      $sTbl .= Glb::$sTkNL . '</td></tr>' . Glb::$sTkNL;
      $sTbl .= Glb::$sTkNL . '</table>' . Glb::$sTkNL;

      return $sTbl;
   }

   /**
    * This function retrieves the cakecrumbs definition file 'breadcrumbsdef.txt'
    *
    * @id 20120816°1001
    * @todo The precise UTF-8 handling of fgetcsv() is unknown! It works for
    *     now, but may yield surprises on other machines. Test and fix this.
    *      - $x = localeconv();                                        // That's math and money only
    *      - $x2 = setlocale(LC_ALL, 0);                               // Tells nothing about utf-8
    * @return CakecrumbSet The wanted breadcrumb set
    */
   static private function retrieveBreadcrumbDefinitions()
   {
      global $s_BreadcrumbsDefinitionCsv;                              // Is null
      global $b_WRITE_JSON_TO_FILE;                                    // Is null

      // Doubled here from high above [seqeuence 20120917°1731]
      // todo : Cleanup double definitions
      $s_BreadcrumbsDefinitionCsv = 'breadcrumbsdef.txt';
      $b_WRITE_JSON_TO_FILE = FALSE;

      $bcs = new CakecrumbSet();

      // Traverse down from the target page to find the cakecrumbs definition file
      $sScriptfile = Glb::$Env_sRequestedScriptName;
      $iStepdown = substr_count($sScriptfile, '/') - 1;                // Tricky to know the exact subtractor -1 or -2
      $sPrefix = '';
      $sCsvFile = '';
      $aResidence = explode('/', $sScriptfile);

      // Search four levels down to find breadcrumbs.txt file [algo 20120824°1911]
      for ($i = $iStepdown; $i >= 0; $i--)
      {
         $s = $sPrefix . $s_BreadcrumbsDefinitionCsv;
         $sDbg = realpath($s);
         if (file_exists($s))
         {
            $sCsvFile = $s;
            unset($aResidence[count($aResidence) -1]);                 // [line 20120824°1913]
            break;
         }
         $sPrefix = $sPrefix . '../';
         unset($aResidence[count($aResidence) -1]);
      }

      // Assemble path
      $bcs->sResidence = implode('/', $aResidence);                    // e.g. • '/daftari/demo' • ''

      // Paranoia
      if ($sCsvFile == '')
      {
         print('<p>No cakecrumbs definition file found : ' . $s_BreadcrumbsDefinitionCsv . '</p>');
         return $bcs;
      }

      //
      // See ref 20120816°1121 'php → function.fgetcsv.php'
      // note : Curiously, if the field looks like this (; 'History'      ;),
      //   it reads it like this ('History     '), so we apply trim.
      if (($h = fopen($sCsvFile, 'r')) !== FALSE)
      {
         while (($aRec = fgetcsv($h, 1000, ';')) !== FALSE)
         {
            if ( empty($aRec[0]) )
            {
               continue;
            }

            // Possibly supplement trailing non-mandatory fields
            $iMaximumFields = 11;
            for ($i = 0; $i < $iMaximumFields; $i++)
            {
               if (! isset($aRec[$i]))
               {
                  $aRec[$i] = '';
               }
            }

            $bc = new Cakecrumb();

            $iNum = count($aRec);
            for ($i = 0; $i < $iNum; $i++)
            {
               $bc->sId            = trim($aRec[0]);
               $bc->sParent        = trim($aRec[1]);
               $bc->sPath          = trim($aRec[2]);
               $bc->sCrumb         = trim($aRec[3]);
               $bc->sTitle         = trim($aRec[4]);

               // [seq 20120825°0201]
               $bc->sStyleOpen     = trim($aRec[5]);
               $bc->sStyleClose    = trim($aRec[6]);
               $bc->sLinkExternal  = trim($aRec[7]);
               $bc->sOverwriteLink = trim($aRec[8]);
               $bc->sOverwriteSelf = trim($aRec[9]);
               $bc->sRights        = trim($aRec[10]);                  // [line 20120920°1412]
            }

            $bcs->pages[] = $bc;
         }
         fclose($h);
      }

      // Write site to file
      if ($b_WRITE_JSON_TO_FILE)
      {
         $s = $sCsvFile . '.json';
         $json = json_encode($bcs);
         $iDummy = file_put_contents($s, $json);
      }

      return $bcs;
   }

   /**
    * This function reads the site structure from an existing json file.
    *
    * @note Function never used, code body is shutdown.
    * @return CakecrumbSet The wanted website object
    */
   private static function sitestructureJsonReadFile_NOTUSEDNOW()
   {
      $bcs = new CakecrumbSet();

      /*
      global $s_BreadcrumbsDefinitionCsv;
      $json = file_get_contents($s_BreadcrumbsDefinitionCsv . 'json');
      $bcs = json_decode($json);
      */

      return $bcs;
   }
}

/* eof */
