<?php

/**
 * This module ..
 *
 * file        : 20190414°0211
 * summary     : 
 * license     : GNU AGPL v3
 * copyright   : © 2019 - 2023 Norbert C. Maier
 * encoding    : UTF-8-without-BOM
 * status      : under construction
 */

// Methods copied here from JsiEdit.php [asof 20190414°0153]

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

/**
 * This class ..
 *
 * @id 20190414°0121
 */
class JsiPhysical
{

   /**
    * This function finds the triangle point of two paths.
    *
    * examples :
    *  (1)
    *  (1.1) pathOne = ""
    *  (1.2) pathTwo = ""
    *  (1.3) o.commonPart =
    *  (1.4) o.tailOne =
    *  (1.5) o.tailTwo =
    *
    * @id 20170925°0411
    * @note This func is former seq 20110820°1921 'build physical page path'.
    * @callers
    * @param {String} $sPathOne
    * @param {String} $sPathTwo
    * @return Array An array with the three wanted values
    */
   private static function calculateTriangle ($sPathOne, $sPathTwo)
   {
      // (.) Buid pysical page path [seq 20110820°1921 'buid pysical page path']
      // summary : Remove leading path elements common to the both of them.
      // note : Old algo, shutdown but not thrown away
      // note : Compare func 20180307°0631 Daf.Utlis.get_ThreeFromTwoByDir

      // (.1) Prepare
      $aTarget = explode('/', $sPathOne);                              // E.g. "/somesite/daftari/docs/notes.html"
      $aScript = explode('/', $sPathTwo);                              // E.g. "/somesite/daftari/php/core/JsiEdit.php"

      // (.2) Disassemble
      // From left to right find first not-matching caracter, triangle point
      // wanted e.g. "X:/workspaces/daftaridev/trunk/daftari/docs/notes.html"
      while ($aTarget[0] == $aScript[0])
      {
         unset($aTarget[0]);
         unset($aScript[0]);
         $aTarget = array_values($aTarget);                            // Re-index
         $aScript = array_values($aScript);                            // Re-index
      }

      // (.3) Re-assemble target
      $aRet = Array();
      $aRet[0] = '';
      $aRet[2] = '';
      $aRet[3] = '';

      return $aRet;
   }

   /**
    * This function shall retrieve the physical path of the current page.
    *
    * Here some test cases :
    *  (1)
    *  (1.1) __FILE__ = 'X:\workspaces\daftaridev\trunk\daftari\php\core\JsiEdit.php'
    *  (1.2) $sTargetPageUrl = "http://localhost/daftaridev/daftari/docs/treeview.html"
    *  (1.3) $sRetPathOnDrive = "X:/workspaces/daftaridev/trunk/daftari/docs/notes.html"
    *  (2)
    *  (2.1) __FILE__ = 'X:\workspaces\demomatrix\trunk\htdocs\daftari\php\core\JsiEdit.php'
    *  (2.2) $sTargetPageUrl = "http://localhost/demomatrix/demomatrix/trunk/htdocs/arrays.html"
    *  (2.2b) $sTargetPageUrl = "http://localhost/demomatrice/htdocs/arrays.html"
    *  (2.3) $sRetPathOnDrive = "X:/workspaces/demomatrix/trunk/htdocs/arrays.html"
    *
    * @id 20170914°1911
    * @note Remember issue 20170914°1711 'find physical path of page'
    * @see note 20120103°2131 'on path calculations'
    * @param {string} $sSuperGetFile The self page URL, e.g.
    *    - "http://localhost/daftari/index.html"
    *    - "http://localhost/daftaridev/trunk/daftari/docs/notes.html"
    * @return string The wanted physical path e.g. "X:/daftaridev/trunk/daftari/docs/notes.html"
    */
   public static function getPagePhysical ($sSuperGetFile)
   {
      global $sDbEc; // workaround

      include_once(__DIR__ . '/../sitemop/DbTxt.php');

      $sPagePhysical = '';                                             // Shall store the finally wanted value

      /**
       * This variable holds the full path of this script.
       *
       * @id 201709011°0123
       * @note Value e.g. "X:\daftaridev\trunk\daftari\php\core\JsiEdit.php"
       * @vartype (A.3) 'Values we have at hand'
       */
      $sSuperPhp_File = __FILE__;

      /**
       * This PHP function is called just for fun so far
       *
       * @id 201709011°0131
       * @vartype (B.1) 'Some primitive tries' (1)
       */
      $aParseSuperGetFile_0 = parse_url($sSuperGetFile);               // 'scheme' 'host' 'path'

      /**
       * This value ... is used to build the target path
       *
       * @id 201709011°0132
       * @note Value e.g. "/daftaridev/trunk/daftari/docs/notes.html"
       * @vartype (B.2) Some primitive tries
       */
      $sPageUrlPath = $aParseSuperGetFile_0['path'];

      /**
       * This variable seems not so useful, it does not handle server aliases.
       *  This is one basic information about the drive system. With using
       *  server side aliases, the URL may point to a totally different location.
       *
       * @id 20170925°0131
       * @note Value e.g. "X:/workspaces/app.apache1/trunk/htdocs"
       * @vartype (A.3) 'Values we have at hand'
       */
      $sSuperDocumentRoot = $_SERVER[Glb::SVR_KEY_DOCUMENT_ROOT];      // 'DOCUMENT_ROOT'

      /**
       * This variable looks like path to self page, relative from the folder where this script resides
       *
       * @id 20170925°0141
       * @note Value e.g. "../../../../../workspaces/daftaridev/trunk/daftari"
       * @vartype (A.3) 'Values we have at hand'
       */
      $sSuperContextDocRoot = $_SERVER[Glb::SVR_KEY_CONTEXT_DOCUMENT_ROOT]; // 'CONTEXT_DOCUMENT_ROOT'

      //-----------------------------------------------
      // (.) Algo 20170925°0121 'build physical self page path'
      // (.1) Retrieve number of steps down
      // From $sSuperContextDocRoot, count the number of '../'
      $iLevelsDown = 0;
      $a1 = explode('/', $sSuperContextDocRoot);
      foreach ($a1 as $s) {
         if ($s === '..') {
            $iLevelsDown++;
         }
      }
      // (.2) Retrieve triangle folder
      // From $sProbe, subtract the number of trailing folders.
      $sTriangleFolder = '';
      $s = __FILE__;
      $aProbe = explode("\\", $s);                                     // todo : Normalize '\' to '/' before!
      for ($i = 0 ; $i < (sizeof($aProbe) - $iLevelsDown - 1) ; $i++) { // -1 is empirical summand
         $sTriangleFolder .= $aProbe[$i] . '/';
      }
      // Now the triangle folder is known.
      //-----------------------------------------------

      // () Retrieve physical target fullfilename
      $aPhysPath = self::mapTargetRoot ( $sSuperDocumentRoot
                                        , $aParseSuperGetFile_0['path']
                                         );

      // Workaround [seq 20190417°0151]
      // todo : Since this is probably always the case, the issue should
      //  be solved inside self::mapTargetRoot
      // Example e.g.
      //  before : G:/work//daftaridev/trunk/daftari/docs/moonwalk.html
      //  after  : G:/work/daftaridev/trunk/daftari/docs/moonwalk.html
      if ((substr($aPhysPath[0], strlen($aPhysPath[0]) - 1) === '/')
          && (substr($aPhysPath[1], 0, 1) === '/')
           ) {
         $sPathFixed = substr($aPhysPath[0], 0, strlen($aPhysPath[0]) - 1);
      } else {
         $sPathFixed = $aPhysPath[0];                                  // 20201203°1441 bugfix — Add this line
      }

      // (.3) Assemble fullfilename
      $sWantedPhysicalSelf = $sPathFixed . $aPhysPath[1];

      // () Just for fun
      $aTri = self::calculateTriangle ( $aParseSuperGetFile_0['path']
                                       , Glb::$Env_sRequestedScriptName
                                        );

      if (Glb::bToggle_TRUE) {
         $s = "\n";
         $s .= "\n" . "Debug 201709011°0141 'self page physical path calculation'";
         $s .= "\n" . ' (A) values at hand';
         $s .= "\n" . ' (A.1) $sSuperDocumentRoot = "' . $sSuperDocumentRoot . '"';
         $s .= "\n" . ' (A.2) $sSuperGetFile = "' . $sSuperGetFile . '"';
         $s .= "\n" . ' (A.2.1) $aParseSuperGetFile_0[\'scheme\'] = "' . $aParseSuperGetFile_0['scheme'] . '"';
         $s .= "\n" . ' (A.2.2) $aParseSuperGetFile_0[\'host\'] = "' . $aParseSuperGetFile_0['host'] . '"';
         $s .= "\n" . ' (A.2.3) $aParseSuperGetFile_0[\'path\'] = "' . $aParseSuperGetFile_0['path'] . '"';
         $s .= "\n" . ' (A.3) Glb::$Env_sRequestedScriptName = "' . Glb::$Env_sRequestedScriptName . '"';
         $s .= "\n" . ' (A.4) $sSuperPhp_File  = "' . $sSuperPhp_File . '"';
         $s .= "\n" . ' (A.5) $sSuperContextDocRoot = "' . $sSuperContextDocRoot . '"';
         $s .= "\n";
         $s .= "\n" . ' (B) finally wanted values';
         $s .= "\n" . ' (B.1) $aPhysPath[0] = "' . $aPhysPath[0] . '"';
         $s .= "\n" . ' (B.2) $aPhysPath[1] = "' . $aPhysPath[1] . '"';
         $s .= "\n" . ' (B.3) $sWantedPhysicalSelf = "' . $sWantedPhysicalSelf . '"';
         $s .= "\n";
         $s .= "\n" . ' (C) other values';
         $s .= "\n" . ' (C.1) $sTriangleFolder = "' . $sTriangleFolder . '"';
         if (Glb::bToggle_TRUE) {
            $s .= "\n";
            $s .= "\n" . ' (D) test triangle calculation';
            $s .= "\n" . ' (D.1) $sPathOne = "' . $aParseSuperGetFile_0['path'] . '"';
            $s .= "\n" . ' (D.2) $sPathTwo = "' . Glb::$Env_sRequestedScriptName . '"';
            $s .= "\n" . ' (D.3) o.common = "' . $aTri[0] . '"';
            if (isset($aTri[1])) {
               $s .= "\n" . ' (D.4) o.tailOne = "' . $aTri[1] . '"';   // possibly undefined
            }
            if (isset($aTri[2])) {
               $s .= "\n" . ' (D.5) o.tailTwo = "' . $aTri[2] . '"';
            }
         }
         $s .= "\n\n";
         $sDbEc = $s;
      }

      return $sWantedPhysicalSelf;
   }

   /**
    * This function finds the triangle point of two paths
    *
    * examples :
    *  (1)
    *  (1.1) in  $sTargetOne = "X:/workspacetwo/app.apache1/trunk/htdocs"
    *  (1.2) in  $sTrigger = "daftari"?
    *  (1.3) out $sTargetTwo = "X:/workspaceone/daftaridev/trunk/daftari"
    *
    * @id 20170925°0551
    * @param $sSuperDocumentRoot {String} The path to be mapped
    * @param $sDocPathFromRoot {String} PageUrlPath with trigger condition
    * @return Array The wanted mapped target root
    */
   private static function mapTargetRoot ($sSuperDocumentRoot, $sDocPathFromRoot)
   {
      // Provide return value [seq 20170925°0552]
      $aPhysPath = array();
      $aPhysPath[0] = $sSuperDocumentRoot;
      $aPhysPath[1] = $sDocPathFromRoot;

      // () Initialize configuration
      // () Provide table [seq 20170925°0553]
      $tb = new DbTxt('targetmap', Glb::$Ses_sDataDirFullname);
      $aaTable = $tb->readAll();

      // Loop [seq 20170925°0554]
      foreach ($aaTable as $aRec)
      {
         // Alias adjusting [seq 20170925°0211]
         // e.g. $sSuperDocumentRoot = "X:/workspacetwo/app.apache1/trunk/htdocs"
         if ($sSuperDocumentRoot === $aRec[0])
         {
            // Process some use cases [seq 20170925°0555]
            // e.g. $sDocPathFromRoot = "/daftaridev/trunk/daftari/docs/index.html"
            $a33 = explode('/', $sDocPathFromRoot);

            // () Extract leftmost folder as trigger folder [seq 20170925°0556]
            // note : Index 1 because index 0 is empty element for leading slash
            $sFirstFolder = $a33[1];

            // () Delete trigger folder [seq 20170925°0557]
            unset($a33[1]);

            // () Trigger fires [seq 20170925°0558]
            if ($sFirstFolder == $aRec[1])
            {
               // () Do the mapping
               $aPhysPath[0] = $aRec[2];
               $aPhysPath[1] = implode('/', $a33);
            }
         }
      }

      return $aPhysPath;
   }
}
