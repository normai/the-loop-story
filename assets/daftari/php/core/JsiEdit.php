<?php

/**
 * This module processes one AJAX request and writes changes back to the target page
 *
 * id          : file 20110818°1321
 * summary     : The facts are
 *    • The input for this module comes from JS func 20120830°0451 EditFinishTransmit.
 *    • The input consists of two components (1) get and (2) post.
 *    • The echo of this module goes to JS function 20110816°1624 HandleResponse.
 *    • The tag seems created by JS function 20110811°1221 execPageSwitchEditmodeOn.
 * license     : GNU AGPL v3
 * copyright   : © 2011 - 2023 Norbert C. Maier
 * authors     : ncm
 * encoding    : UTF-8-without-BOM
 * status      : working, waiting to be refactored
 */

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

/**
 * This class wraps the edit feature code
 *
 * @id 20190205°0411
 * @note Process or discard todo 20120824°1241 'tighten edit request'
 */
class JsiEdit
{
   /**
    * This method is the modules entry point
    *
    * @id 20190205°0421
    * @callers Only • func 20190209°0341 Go::dispatchCommand
    */
   public static function execute()
   {
      include_once(__DIR__ . '/JsiPhysical.php');

      // (.) Retrieve parameters from request
      // (.1) Ajax message
      //  This is the HTML content to be written over the element indicated by 'tag'.
      // note : Remember issue 20190205°0531 'http_raw_post_data'
      // () read post-data [seq 20190205°0533] replacement for HTTP_RAW_POST_DATA
      // note : Compare • JsiEdit.php seq 20190205°0533 and • jsi-demo-pshdp.php seq 20190205°0534
      $sAjaxmessage = file_get_contents("php://input");

      // (.2) TagId
      $sTagid = $_GET[Glb::GET_KEY_AJX_tagid];                         // 'tagid', value e.g. 'mimg.P.12'
      $aTagid = explode('.', $sTagid);                                 // provide the three elements of the tag

      // () Comfort
      $sTag = '';
      if (isset($aTagid[1])) {
         $sTag = $aTagid[1];
      }
      $iIndex = '';
      if (isset($aTagid[2])) {
         $iIndex = $aTagid[2];                                         // The elementnumber // the '-1' is WRONG FOR LIs [line 20120104°1041]
      }

      // (.3) Targetfile as told by JS [var 20170903°0311]
      // Examples :
      // - $_GET['file'] = "http://localhost/daftari/index.html"
      // - $_GET['file'] = "http://localhost/daftaridev/daftari/docs/notes.html"
      $sSuperGetFile = $_GET[Glb::GET_KEY_AJX_file];                   // 'file'

      $aTarget = explode('/', $sSuperGetFile);                         // [line 20170903°0312]

      $sSuperServerScriptname = Glb::$Env_sRequestedScriptName;        // [line 20170903°0313]

      $aScript = explode('/', substr($sSuperServerScriptname,1));      // [line 20170903°0314] // Without the leading slash

      // Collect output inside getPagePhysical() [var 20180512°0831]
      $sDbEc = '';

      // Retrieve file path
      $sPagePhysical = JsiPhysical::getPagePhysical($sSuperGetFile);   // This uses $sDbEc
      // [debug 20180303°1941] Now $sPagePhysical is wrong e.g.
      //  - X:/workspaces/app.apache1/trunk/htdocs/work/trekta/demomatrix/trunk/manual/index.html

      // (.4) Mode
      // Modes so far : a = insert after, b = insert before, o = open page in text editor
      $sMode = $_GET[Glb::GET_KEY_AJX_mode];                           // 'mode'

      echo($sDbEc);                                                    // Catch up what was delayed

      // Debug output
      if (Glb::bToggle_TRUE) { // TRUE FALSE
         $sDbEc  = "\n" . ' --------------------------------';
         $sDbEc .= "\n" . ' [Debug 20170903°0121 JsiEdit.php]';
         $sDbEc .= "\n" . ' __FILE__ = ' . __FILE__;                   // e.g. .../Configo.php
         $sDbEc .= "\n" . ' $sSuperGetFile = "' . $sSuperGetFile . '"'; // e.g. 'mimg.P.5'
         $sDbEc .= "\n" . ' $sSuperServerScriptname = "' . $sSuperServerScriptname . '"'; // e.g. 'mimg.P.5'
         $sDbEc .= "\n" . ' $sTagid = "' . $sTagid . '"';              // e.g. 'mimg.P.5'
         $sDbEc .= "  " . ' $sTag = "' . $sTag . '"';                  // e.g. 'P'
         $sDbEc .= "  " . ' $iIndex = ' . $iIndex . '';                // e.g. '5'
         $sDbEc .= "\n" . ' * $aTarget = ';
         foreach ($aTarget as $e) {
            $sDbEc .= ' ' . $e . ' ';
         }
         $sDbEc .= "\n" . ' * $aScript = ';
         foreach ($aScript as $sEle) {
            $sDbEc .= ' ' . $sEle . ' ';
         }
         $sDbEc .= "\n" . ' --------------------------------' . "\n";
         echo ($sDbEc);
      }

      // () Buid pysical page path [seq 20110820°1921]
      // summary : Remove leading path elements common to the both of them.
      // note : Compare func 20180307°0631 Daf.Utlis.get_ThreeFromTwoByDir
      while ($aTarget[0] === $aScript[0])
      {
         unset($aTarget[0]);
         unset($aScript[0]);
         $aTarget = array_values($aTarget);                            // Re-index
         $aScript = array_values($aScript);                            // Re-index
      }

      // Debug
      if (Glb::bToggle_TRUE) {
         $s = ''; // '   --------------------------------';
         $s .= "\n" . '   [Debug 20170903°0123] JsiEdit';
         $s .= "\n" . '   New page path = ';                           // E.g. "http:    localhost  daftaridev  daftari  moonwalk  notes.html"
         foreach ($aTarget as $e) {
            $s .= ' ' . $e . ' ';
         }
         $s .= "\n" . '   New script path = ';                         // E.g. "daftaridev  trunk  daftari  php  core  JsiEdit.php"
         foreach ($aScript as $e) {
            $s .= ' ' . $e . ' ';
         }
         $s .= "\n" . ' $sFilename = ' . $sPagePhysical;               // E.g. "http://localhost/daftaridev/daftari/docs/notes.html"
         $s .= "\n" . '   --------------------------------';
         echo ($s);
      }

      // Fetch original file
      $sHtml = file_get_contents($sPagePhysical);                      // E.g. "X:/workspaces/daftaridev/trunk/daftari/index.html"

      // Do the job
      $bSuccess = self::htmlElementEdit ( $sHtml                       // ref in/out - original file and changed file
                                         , $sTag                       // in - tag for wanted element
                                          , $iIndex                    // in - number of wanted element
                                           , $sAjaxmessage             // in - the replacement text
                                            , $sMode                   // in - '', 'insertbefore', 'insertafter'
                                             , $sErr                   // ref out - possible error message
                                              );                       // return - success flag

      // Write back target file (possibly switch on/off)
      if ($bSuccess)
      {
         // (.) Write back edited file
         // debug 20170903°0142 : Should this better be a local path name instead an url?
         // para : e.g. $sFilename = "http://localhost/daftaridev/trunk/daftari/docs/notes.html"
         if (file_put_contents($sPagePhysical, $sHtml))
         {
            // todo : Provide some positive notification, e.g. into a logfile.
            //         Juste as it is done now, this is a bit too heavy.
            $sEcho = $sHtml;
         }
         else
         {
            $sEcho = "<p>ERROR 20170903°0141 - STATIC WRITE FAILED";
            if (Glb::bToggle_TRUE) {
               $sEcho .= ' on file "' . $sPagePhysical . '"';
               // E.g. "http://localhost/daftaridev/trunk/daftari/docs/notes.html"
            }
            $sEcho .= "</p>";
         }
      }
      else
      {
         $sEcho = $sErr;
      }

      // Finish with notification
      $sEcho = '[' . $sTagid . "]" . Glb::$sTkNL . $sEcho;
      echo $sEcho;
   }

   /**
    * This function exchanges the 'innerHTML' of the target tag.
    *
    * @id 20110818°1322
    * @note This is the essential editor function.
    * @note We would like to utilize a DOM parser for writing changes to the
    *     page. But nor SimpleHTMLDOMParser (dld 20110816°2121) nor DOMDOCUMENT
    *     write the manipulated DOM nicely back (see files demo-*.php). So we
    *     roll our own write-back method based on plaintext file manipulation.
    * @param &$sHtml {string} ..
    * @param $sTag {string} ..
    * @param $iIndex {string} ..
    * @param $sReplacement {string} ..
    * @param $sMode {string} ..
    * @param &$sErr {string} ..
    * @return bool Success flag
    */
   private static function htmlElementEdit ( &$sHtml                   // ref in/out - incoming file, changed file
                                            , $sTag                    // in - the tag to edit e.g. 'P', 'TD'
                                             , $iIndex                 // in - tag number (starting with zero)
                                              , $sReplacement          // in - the new text to write
                                               , $sMode                // in - '', 'insertbefore', 'insertafter'
                                                , &$sErr               // ref out - possible error message
                                                 )                     // return - success flag
   {
      $sErr = "";

      // () Debug 20170903°0211
      if (Glb::bToggle_TRUE) {
         $s = "\n";
         $s .= '(debug 20170903°0211) function htmlElementEdit starts';
         echo ($s);
      }

      // () Iterate over tags to determine the edit range [seq 20120819°1831]
      $iStartTagStart = 0;
      for ($i = 0; $i <= $iIndex; $i++)
      {
         /*
         issue 20120104°1121 'searching li finds also link'
         solution : Counteract after reading one more character.
         */

         // () Locate next tag in target html file
         //  Here hits issue issue 20120104°1121 'searching li finds also link'
         $iStartTagStart = stripos($sHtml, '<' . $sTag, $iStartTagStart);

         // () Tag not found
         if ($iStartTagStart === false)
         {
            $sErr = '<p>Error 20121111°1721 — Wanted tag not found.</p>';
            return false;
         }

         // () Counteract issue 20120104°1121 [seq 20120104°1125]
         $s = substr($sHtml, $iStartTagStart + 1 + (strlen($sTag)), 7); // debug
         if ( (substr($sHtml, $iStartTagStart + 1 + (strlen($sTag)), 1) !== '>')
               && (substr($sHtml, $iStartTagStart + 1 + (strlen($sTag)), 1) !== ' ')
              )
         {
            // It's not the exact tag, e.g. it is '<link' tag instead of a '<li'
            //  brute force, should be much more elegant
            $i--;
         }

         // [debug 20170903°0212]
         if (Glb::bToggle_TRUE) {
            $s = "\n" . ' - $iStartTagStart = ' . $iStartTagStart;
            $s .= ' Snippet = ' . substr($sHtml, $iStartTagStart + 1 , 24);
            echo ($s);
         }

         $iStartTagStart++;
      }

      if ($sMode == 'insertbefore')
      {
         $sBlockBefore = substr($sHtml, 0, $iStartTagStart - 1);
         $sBlockAfter = substr($sHtml, $iStartTagStart - 1);
      }
      else if ($sMode == 'insertafter')
      {
         $iStartTagEnd = stripos($sHtml, '>', $iStartTagStart);
         $iTextblockStart = $iStartTagEnd + 1;
         $iEndTagStart = stripos($sHtml, '</' . $sTag . '>', $iTextblockStart);
         $sBlockBefore = substr($sHtml, 0, $iEndTagStart + 3 + strlen($sTag));
         $sBlockAfter = substr($sHtml, $iEndTagStart + 3 + strlen($sTag));
      }
      else
      {
         $iStartTagEnd = stripos($sHtml, '>', $iStartTagStart);
         $iTextblockStart = $iStartTagEnd + 1;
         $iEndTagStart = stripos($sHtml, '</' . $sTag . '>', $iTextblockStart); // false if tag not found e.g. missing closing tag [line 20121111°1711]
         if ($iEndTagStart === false)
         {
            $sErr = '<p>Error 20121111°1722 — Closing tag no found.</p>';
            return false;
         }
         $iLen = $iEndTagStart - $iTextblockStart;                     // Minus if end tag not found
         $sTextblock = substr($sHtml, $iTextblockStart, $iLen);        // False if end tag not found
         // Now we got the textblock to be exchanged, but we need the part before and after
   
         // Disassemble file
         $sBlockBefore = substr($sHtml, 0, $iTextblockStart);
         $sBlockAfter = substr($sHtml, $iEndTagStart);
      }

      // [workaround 20140307°0511]
      // note : It seems extremely difficult to preserve the indent from the
      //    original HTML file. The JavaScript cannot know it. And here we
      //    could find it out, but only with expensive text search operations.
      //    So we suffice with *fixed indent*. Can be made more intelligent.
      $sIndent = '';
      if ($sTag == 'LI')
      {
         $sIndent = ' ';
      }
      else if ($sTag == 'TD')
      {
         $sIndent = '  ';
      }

      // Re-assemble file
      $sHtml = $sBlockBefore . $sReplacement . $sIndent . $sBlockAfter;

      return true;
   }
}
/* eof */
