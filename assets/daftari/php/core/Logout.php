<?php

/**
 * This module processes the logout request
 *
 * file      : 20190129°0411
 * license   : GNU AGPL v3
 * copyright : © 2019 - 2023 Norbert C. Maier
 * authors   : ncm
 * todo      : Eliminate this module
 * status    :
 * encoding  : UTF-8-without-BOM
 * callers   :
 */

namespace Trekta\Daftari;

include(__DIR__ . './../core/Login.php');

Login::runLogout();

/* eof */
