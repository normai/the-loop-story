<?php

/**
 * This processes one AJAX request and writes changes back to the target page
 *
 * file        : 20190414°0111 (after 20110818°1321)
 * summary     : 
 * license     : GNU AGPL v3
 * copyright   : © 2019 - 2023 Norbert C. Maier
 * encoding    : UTF-8-without-BOM
 * status      : under construction
 */

/*
   note 20190414°0112 : This file is copied from JsiEdit.php and shall
     resemble that, just with some enhancements e.g.
      • Use element IDs
      • Get better plaintext element locating
      • Be generic for CakeCrumbs, FadeInFile, Blog, etc
*/

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

/**
 * This class ..
 *
 * @id 20190414°0121
 */
class JsiCrumbs
{
   /**
    * This method is the modules entry point
    *
    * @id 20190205°0441
    * @callers Only • func 20190209°0341 Go::dispatchCommand from
    *     • func 20190413°0741 Daf.Dspat.process_cake2build seq 20190413°0753
    */
   public static function crmbsExecute($aCmds)
   {
      // () Prologue
      include(__DIR__ . '/JsiPhysical.php');
      include(__DIR__ . '/JsiReplace.php');

      // () Retrieve HTML fragment from request [line 20190205°0431]
      $sPayload = file_get_contents("php://input");                    // E.g. the innerHTML of the cakecrumbs bag

      // () Read request parameters [seq 20190205°0433]
      $sTargetId = $aCmds['targetid'];                                 // E.g. "DafFurnitureCakecrumbs_Bag"
      $sTargetFile = $aCmds['targetfile'];                             // E.g. "http://localhost/daftaridev/trunk/daftari/docs/moonwalk.html"

      // () Targetfile as told by JS [var 20170903°0311]
      $sPagePhysical = JsiPhysical::getPagePhysical($sTargetFile);

      // () Perform wanted job [line 20190205°0434]
      $x = JsiReplace::replaceDomElement($sTargetId, $sPagePhysical, $sPayload);

      // () Debug message [line 20190205°0435]
      $sEco = Glb::$sTkNL . '☼ Finished crmbsExecute'
             //. Glb::$sTkNL . '    — target   = ' . $sTargetFile
             //. Glb::$sTkNL . '    — physical = ' . $sPagePhysical
             //. Glb::$sTkNL . '    — result   = "' . $x . '"'
             . Glb::$sTkNL . '۞' . Glb::$sTkNL
              ;

      // () Finish [line 20190205°0437]
      echo $sEco;
   }
}
/* eof */
