<?php
/**
 * This module ...
 *
 * file      : 20110515°1721
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 * callers   : many
 */

namespace Trekta\Daftari;

//use Trekta\Daftari as TD;
//use Trekta\Daftari\Globals as Glb;

/**
 * This class provides an error handler
 *
 * @id 20190202°0431
 * @todo 20190202°0433 : This class is artificially created just to formally
 *    streamline the project. Since PHP 5.1 try/catch is built-in. Probably
 *    now are better solutions available to use try/catch in PHP. Check and
 *    implement exception usage state-of-the-art.
 */
class ErrorHandler
{
   /**
    * This function .. is just a helper function to suffice class syntax ..
    *
    * @id func 20190202°0441
    * @callers Only script level
    */
   public static function runErrHand()
   {

      // Define below function only once [line 20120816°1401]
      if (! function_exists("Trekta\\Daftari\\my_exceptons_error_handler"))
      {

         /**
          * This function installs an error-handler which turns
          *  errors into exceptions to make try/catch available.
          *
          * @id 20110829°1321
          * @status works
          * @ref http://php.net/manual/de/language.exceptions.php [ref 20110829°1322]
          *         and http://www.php.net/manual/de/class.errorexception.php [ref 20110829°1323]
          * @ref 20190202°0423 'nested php functions'
          * @note Standard PHP functions do not throw exceptions, so they cannot be
          *     wrapped in a try/catch block, but one can extend the error handling.
          * @callers ..
          * @param $severity {Integer} ? ..
          * @param $message {String} ? ..
          * @param $filename {String} ? ..
          * @param $lineno {Integer} ? ..
          */
         function my_exceptons_error_handler($severity, $message, $filename, $lineno)
         {
            if (error_reporting() == 0)
            {
               return;
            }
            if ( error_reporting() && $severity )
            {
               throw new \ErrorException($message, 0, $severity, $filename, $lineno);
            }
         }
      }
   }
}

// [line 20190202°0451]
ErrorHandler::runErrHand();

/* eof */
