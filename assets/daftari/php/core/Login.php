<?php

/**
 * This file hosts the Login class
 *
 * file      : 20190129°0311
 * version   :
 * license   : GNU AGPL v3
 * copyright : © 2019 - 2023 Norbert C. Maier
 * authors   : ncm
 * reference : This class was inspired from
 *    • PHP Login Without MySql by Aarmale https://sourceforge.net/projects/php-login
 *       [download 20110506°1921 and download 20161010°1311]
 * encoding  : UTF-8-without-BOM
 * status    :
 * callers   :
 */

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

$sDbg = __DIR__;
$sDir = "G:/work/downtown/daftaridev/trunk/daftari/php/core";          // Bad workaround — See issue 20220222°0751
$sDir = __DIR__;                                                       // issue 20220222°0751 '__DIR__ tells "xdebug:" instead directory'
include_once $sDir . '/../sitemop/DbTxt.php';                          // todo : Eliminate if go.php works
include_once $sDir . '/../core/Globals.php';                           // todo : Eliminate if go.php works

/**
 * This class provides the login mechanism
 *
 * The logic goes like this:
 * (B) We have three states
 * (B.1) There exists a session
 * (B.1.1)
 * (B.2) The user requested a login
 * (B.2.1)
 * (B.3) No session wanted
 * (B.3.1) No code is executed
 *
 * @id 20190129°0321
 * @callers
 */
class Login
{

   /**
    * This function fetches the credentials for the user given by the post request
    *
    * @id 20110507°1911
    * @callers Only from this script level
    * @return {array}
    */
   private static function retrieve_credentials()
   {
      $db = new DbTxt ( 'accounts'                                     // 'accounts'
                        , Glb::$Ses_sDataDirFullname
                         );
      $aRec = $db->lookupRecord
             ( Glb::TBL_ACCTS_iFDX_Shortname                           // 1
              , $_POST[Glb::POSTKEY_LGN_user]                          // 'user'
               );

      return $aRec;
   }

   /**
    * This static method runs the login procedure
    *
    * @id 20190129°0331
    * @callers This script level only
    */
   public static function runLogin()
   {

      // (B) Process login status
      // (B.1) A session is already ongoing
      if ( isset( $_SESSION[Glb::SESKEY_login] ) )                     // 'login'
      {
         // (.) Calculate login hash from stored plain text password
         // todo : Clear issue 20110508°1933 'is storing plaintext pw useful'
         $hash = md5( Glb::LGN_sMD5SALTONE . $_SESSION[Glb::SESKEY_pass] . Glb::LGN_sMD5SALTTWO );

         // (.) Do the login credentials match the stored session credentials?
         if ( $_SESSION[Glb::SESKEY_login] !== $hash )
         {
            Glb::$Lgn_sMsg_Status_Text = "Bad session. Clear session and try again.";
         }
      }
      // (B.2) The user requested a login by pressing the submit button
      else if ( isset( $_POST[Glb::POSTKEY_LGN_submit] ) )             // 'submit'
      {
         // (B.2.1) Retrieve the credentials from the user database
         $aRec = self::retrieve_credentials();

         // (B.2.2) Is user registered?
         if (sizeof($aRec) < 1)
         {
            // (B.2.2.1) User does not exist
            Glb::$Lgn_sMsg_Status_Text = "Benutzer '"
                                        . $_POST[Glb::POSTKEY_LGN_user] // 'user'
                                         . "' ist nicht registriert."
                                          ;
         }
         else
         {
            // (B.2.2.2) Yes, user is registered
            // (B.2.2.2.1) Calculate hash value from the currently given password
            // note : See todo 20190127°0111 'replace md5'
            $hash = md5(Glb::LGN_sMD5SALTONE . $_POST[Glb::SESKEY_pass] . Glb::LGN_sMD5SALTTWO);

            // (B.2.2.2.2) Match current hash with the stored hash
            if ( $hash == $aRec[Glb::TBL_ACCTS_iFDX_Pwhash] )
            {
               // (B.2.2.2.2.1) The hashes match, we start a new session
               // (x.1) Fill the basic session array elements
               $_SESSION[Glb::SESKEY_login] = $hash;
               $_SESSION[Glb::SESKEY_userfullname] = $aRec[Glb::TBL_ACCTS_iFDX_Fullname];
               $_SESSION[Glb::SESKEY_username] = $aRec[Glb::TBL_ACCTS_iFDX_Shortname];

               // Note : Not sure, whether we shall store the pw in plaintext
               //   in the session array (see todo 20110508°1933)
               $_SESSION[Glb::SESKEY_pass] = $_POST[Glb::SESKEY_pass];

               //-----------------
               // Note : This looks like a sequence continued in LoginPage.php and Session.php ..
               // Todo : Merge the two sequences, either here or there [todo 20110508°2121]
               switch ($_SESSION[Glb::SESKEY_username])
               {
                  case (Glb::SESVAL_sUSERNAME_admin) : $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_full  ; break;
                  case (Glb::SESVAL_sUSERNAME_guest) : $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_guest ; break;
                  case (Glb::SESVAL_sUSERNAME_jenny) : $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_full  ; break;
                  default                          : $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_user  ; break;
               }

               // Login behaviour by privileges
               switch ($_SESSION[Glb::SESKEY_privileges])
               {
                  case Glb::SESVAL_PRIVIL_full :
                     $_SESSION[Glb::SESKEY_bDebug] = true;
                     $_SESSION[Glb::SESKEY_bMore010_sessionInfo] = true;
                     break;
                  case Glb::SESVAL_PRIVIL_guest :
                     // //$_SESSION[Glb::SESKEY_bMore010_sessionInfo] = true;
                     // //break;
                  case Glb::SESVAL_PRIVIL_user :
                     $_SESSION[Glb::SESKEY_bMore010_sessionInfo] = true;
                     break;
                  default :
                     $_SESSION[Glb::SESKEY_bDebug] = false;
                     $_SESSION[Glb::SESKEY_bMore010_sessionInfo] = false;
                     break;
               }
               //-----------------

               // (.) After header redirection immediately end execution [seq 20150126°0911]
               // note : Remember ref 20150125°2112 'Why call exit after redirection'
               header("Location: " . $_SERVER['PHP_SELF']);
               exit();
            }
            else
            {
               Glb::$Lgn_sMsg_Status_Text = "Kennwort falsch.";        // [line 20110507°1901] added
               Glb::$Lgn_sMsg_Status_Text .= Glb::$sTkNL . '&nbsp; <input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
            }
         }
      }
      // (B.3) No session exists nor is one wanted
      else
      {
      }
   }

   /**
    * This static method runs the logout procedure
    *
    * @id 20190129°0341
    * @callers Only from Logout.php script level
    */
   public static function runLogout()
   {
      // Check — This is called inside Session.php, so isn't it already done?!
      session_start();

      // If about cookie named login exists...
      if (isset($_SESSION[Glb::SESKEY_login]))                         // Try
      {
         // Delete cookie by setting expiration time backwards
         setcookie("login", "" ,time() - 3600);                        // Seems not useful
         unset($_SESSION[Glb::SESKEY_login]);                          // Try - it works

         //-----------------------
         // [seq 20110508°2123`01]                                     //// [Marker 20210526°1011 Merge sequences]
         // todo : This sequence exists three times — Merge them.
         $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_none;
         $_SESSION[Glb::SESKEY_bDebug] = false;
         $_SESSION[Glb::SESKEY_bMore010_sessionInfo] = false;          // Todo : Check — Is this the right field?
         $_SESSION[Glb::SESKEY_login] = '';
         $_SESSION[Glb::SESKEY_pass] = '';
         $_SESSION[Glb::SESKEY_username] = '';
         $_SESSION[Glb::SESKEY_userfullname] = '';
         //-----------------------
      }

      // Redirect back to the calling page [seq 20150126°0912]
      // note : After header redirection, immediately end PHP execution
      // note : Remember ref 20150125°2112 'why call exit after redirection'
      $s= $_SERVER[Glb::SVR_KEY_HTTP_REFERER];                         // 'HTTP_REFERER', value e.g. '../../login.html'
      header('Location: ' . $s);
      exit();
   }
}

Login::runLogin();

/* Ω */
