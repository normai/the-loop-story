<?php

/**
 * This file shall host the Session class ..
 *
 * file      : 20110506°1721
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    : applied
 * encoding  : UTF-8-without-BOM
 * callers   :
 */

namespace Trekta\Daftari;

////session_start();  # PROVISORY TEST

use Trekta\Daftari\Globals as Glb;

/*
         // SHIFTED HERE 20231008°1321 -- Does not help against quirk
         // ref : https://stackoverflow.com/questions/49024013/php-warning-session-start-cannot-start-session-when-headers-already-sent [ref 20231008°1336]
         // ref : https://stackoverflow.com/questions/8028957/how-to-fix-headers-already-sent-error-in-php [ref 20231008°1338]
         if (session_start() !== true) {
            // Fatal error, using css class 'error' [seq 20110508°1841]
            echo('<p class="PhpError">Quirk 20110508°1852 was seen.</p>');
            exit();                                                    // [line 20120916°1701]
         }
*/

include(__DIR__ . '/../core/Globals.php');
include_once(__DIR__ . '/../galari/FileFamilies.php');                 // For getBcTime()

// Run former script-level code
Session::runSession();

/**
 * This class maintains the session ingredients
 *
 * @id 20190129°0211
 * @see ref 20140822°0019 'Matthew James Taylor → CSRF Prevention Methods'
 * @callers
 */
class Session
{
   /**
    * This static method runs the session procedure
    *
    * @id 20190129°0221
    * @see : ref 20110503°1031 'tedd sperling simple session example'
    * @see : note 20121028°1803 'session.save_path must exist'
    * @see : note 20131118°1632 'session_start and output_buffering'
    * @callers This script level only
    */
   public static function runSession()
   {
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // Re-introduce sequence from tag 20201203o1521.daftari.v068m [seq 20220222°0821]
      // Yes, this fixes the no-more working login. Only why and when was the sequence lost?

      //// SEQUENCE EXPERIMENTALLY SHIFTED ABOVE 20231008°1321`02
      // note 20180508°2023 : Fixed issue 20180508°2021 'session had already started'
      if (! isset($_SESSION)) {
         // Remember ref 20110503°1031 'tedd sperling → simple session example' — Must be first line of code
         if (session_start() !== true) {
            // Fatal error, using css class 'error' [seq 20110508°1841]
            echo('<p class="error">Quirk 20110508°1852 was seen.</p>');
            exit();                                                    // [line 20120916°1701]
         }
      }


      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      // () Store session start time [line 20110506°1726]
      //  Now using getBcTime in favour of formerly used getMicrotime
      Glb::$Bct_sExecutionStartTime = FileFamilies::getBcTime();

      if (! isset($_SESSION[Glb::SESKEY_requestcount]))
      {
         $_SESSION[Glb::SESKEY_requestcount] = 1;
      }
      else
      {
         $_SESSION[Glb::SESKEY_requestcount]++;
      }

      // (.) Handle login settings per request [seq 20110507°1904]
      // note : This shall be the central place to handle login settings per request
      // todo : Consider not only the username but crosscheck other variables.
      // note : $_SESSION[Glb::SESKEY_username] is set and changed in Login.php
      Glb::$Ses_bIsLoggedin = false;                                   // Is initialized as false anyway
      if (isset($_SESSION[Glb::SESKEY_login]))
      {
         Glb::$Ses_bIsLoggedin = TRUE;
         Glb::$Ses_sUserShortName = $_SESSION[Globals::SESKEY_username];
      }

      // Guarantee all necessary session variables
      if (! isset($_SESSION[Glb::SESKEY_bDebug]))
      {
         $_SESSION[Glb::SESKEY_bDebug] = false;
      }
      if (! isset($_SESSION[Glb::SESKEY_bMore010_sessionInfo]))
      {
         $_SESSION[Glb::SESKEY_bMore010_sessionInfo] = false;
      }
      if (! isset($_SESSION[Glb::SESKEY_bMore020]))
      {
         $_SESSION[Glb::SESKEY_bMore020] = false;
      }
      if (! isset($_SESSION[Glb::SESKEY_bMore030]))
      {
         $_SESSION[Glb::SESKEY_bMore030] = false;
      }
      if (! isset($_SESSION[Glb::SESKEY_bMore040]))
      {
         $_SESSION[Glb::SESKEY_bMore040] = false;
      }
      if (! isset($_SESSION[Glb::SESKEY_bMore060inivarCanary]))
      {
         $_SESSION[Glb::SESKEY_bMore060inivarCanary] = false;
      }
      if (! isset($_SESSION[Glb::SESKEY_iAlbumColumns]))
      {
         $_SESSION[Glb::SESKEY_iAlbumColumns] = Glb::DEFVAL_GLR_iALBUM_COLUMNS;  // 4
      }
      if (! isset($_SESSION[Glb::SESKEY_iAlbumMaxRows]))
      {
         $_SESSION[Glb::SESKEY_iAlbumMaxRows] = Glb::DEFVAL_GLR_iALBUM_ROWS;  // 3 or 4
      }
      if (! isset($_SESSION[Glb::SESKEY_sAlbumThumbsize]))
      {
         $_SESSION[Glb::SESKEY_sAlbumThumbsize] = Glb::GLR_ENUM_THUMBSIZES[Glb::DEFVAL_GLR_iALBUM_THUMBSIZE];  // 'medium'
      }
      if (! isset($_SESSION[Glb::SESKEY_iObjectImagesize]))
      {
         $_SESSION[Glb::SESKEY_iObjectImagesize] = Glb::DEFVAL_GLR_iOBJECT_IMAGESIZE; // '560'
      }
      if (! isset($_SESSION[Glb::SESKEY_username]))
      {
         $_SESSION[Glb::SESKEY_username] = '';
      }
      if (! isset($_SESSION[Glb::SESKEY_userfullname]))
      {
         $_SESSION[Glb::SESKEY_userfullname] = '';
      }
      if (! isset($_SESSION[Glb::SESKEY_bReadlocaldrive]))
      {
         if ( Glb::$Lgn_bHostIsLocalhost )
         {
            $_SESSION[Glb::SESKEY_bReadlocaldrive] = true;
         }
         else
         {
            $_SESSION[Glb::SESKEY_bReadlocaldrive] = false;
         }
      }

      if (! isset($_SESSION[Glb::SESKEY_MaintenanceMode]))
      {
         if ( Glb::$Lgn_bHostIsLocalhost )
         {
            $_SESSION[Glb::SESKEY_MaintenanceMode] = false;
         }
         else
         {
            $_SESSION[Glb::SESKEY_MaintenanceMode] = true;
         }
      }

      if (! isset($_SESSION[Glb::SESKEY_privileges]))
      {
         $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_none;
      }
      // ['sPrivileges'] = 'none'

      $sCR = Glb::$sTkNL;

      // Introduce the Null Coalescing operator [line 20220222°0811]
      // The Null Coalescing operator exists since PHP 7. The Null Coalescing Assignment operator '??=' sincePHP 7.4
      // See e.g. www.phptutorial.net/php-tutorial/php-null-coalescing-operator/ [ref 20220222°0812]
      $sSessionLogin = $_SESSION[Glb::SESKEY_login] ?? '&lt;n/a&gt;';

      // () Build the the LightCoral localhost debug pane [seq 20110506°1727]
      // This pane is included in some pages at the bottom
      // todo 20181228°1012 Use some switch to set it on/off (best an already existing one)
      Glb::$Ses_sLocalhostInfo = $sCR . '<pre style="background-color:LightCoral; font-size:small;">'; // Formerly red 20181228°1021
      if ( Glb::$Lgn_bHostIsLocalhost === TRUE )
      {
         Glb::$Ses_sLocalhostInfo .= $sCR . '************* LightCoral LocalhostInfo *************';
         Glb::$Ses_sLocalhostInfo .= $sCR . ' Glb::$Ses_bIsLoggedin                   = ' . (Glb::$Ses_bIsLoggedin ? 'TRUE' : 'FALSE');
         Glb::$Ses_sLocalhostInfo .= $sCR . ' $sSessionLogin                          = ' . $sSessionLogin;
         Glb::$Ses_sLocalhostInfo .= $sCR . ' $_SESSION[Glb::SESKEY_bReadlocaldrive]  = ' . /* (string) */ $_SESSION[Glb::SESKEY_bReadlocaldrive];
         Glb::$Ses_sLocalhostInfo .= $sCR . ' $_SESSION[Glb::SESKEY_userfullname]     = ' . $_SESSION[Glb::SESKEY_userfullname];
         Glb::$Ses_sLocalhostInfo .= $sCR . ' $_SESSION[Glb::SESKEY_username]         = ' . $_SESSION[Glb::SESKEY_username];
         Glb::$Ses_sLocalhostInfo .= $sCR . '* note : On the Login page, press page refresh (F5)';
         Glb::$Ses_sLocalhostInfo .= $sCR . '****************************************************';
      } else {
         Glb::$Ses_sLocalhostInfo .= $sCR . 'Here were the LightCoral box';
      }
      Glb::$Ses_sLocalhostInfo .= $sCR . '</pre>';

   }
}

/* eof */
