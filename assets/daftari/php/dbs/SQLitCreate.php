<?php

/**
 * file        : 20180331°0301
 * encoding    : UTF-8-without-BOM
 * ref         : http://www.sqlitetutorial.net/sqlite-php/create-tables/ [ref 20180330°0116]
 */

namespace Trekta\Databese;

//use Trekta\Daftari as TD;
//use Trekta\Daftari\Globals as Glb;

/**
 * This class provides the means to create SQLite tables
 *
 * @author ncm
 * @id 20180331°0311
 */
class SQLitCreate {

   /**
    * This field stores the PDO object ..
    *
    * @id 20180331°0321
    * @var \PDO
    */
   private $pdo;

   /**
    * This constructor connects to the SQLite database
    *
    * @id 20180331°0322
    * @param Object $pdo The PDO object which shall connect to the SQLite database
    */
   public function __construct($pdo) {
      $this->pdo = $pdo;
   }

   /**
    * Create tables
    *
    * @id 20180331°0323
    */
   public function createTables() {

      $commands = [ 'CREATE TABLE IF NOT EXISTS projects
                    ( project_id   INTEGER PRIMARY KEY
                     , project_name TEXT NOT NULL
                      )'
                   , 'CREATE TABLE IF NOT EXISTS tasks
                    ( task_id INTEGER PRIMARY KEY
                     , task_name  VARCHAR (255) NOT NULL
                      , completed  INTEGER NOT NULL
                       , start_date TEXT
                        , completed_date TEXT
                         , project_id VARCHAR (255)
                          , FOREIGN KEY (project_id)
                            REFERENCES projects(project_id)
                             ON UPDATE CASCADE
                              ON DELETE CASCADE
                             )'];

      // execute the sql commands to create new tables
      foreach ($commands as $command) {
         $this->pdo->exec($command);
      }
   }

   /**
    * This method gets the list of tables in the database
    *
    * @id 20180331°0324
    */
   public function getTableList() {

      $stmt = $this->pdo->query( "SELECT name
                                  FROM sqlite_master
                                   WHERE type = 'table'
                                    ORDER BY name"
                                  );
      $tables = [];
      while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
         $tables[] = $row['name'];
      }

      return $tables;
   }
}

/* eof */
