<?php

/**
 * This module provides miscellaneous functions
 *
 * file      : 20101231°1921
 * license   : GNU AGPL v3
 * copyright : © 2010 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 * callers   :
 */

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

/**
 * This class provides various methods to handle file and folder families ..
 *
 * id        : class 20190202°0511
 * callers   :
 */
class FileFamilies
{

   /**
    * This function inserts an element into a numerically indexed array at a specific position
    *
    * @id 20110524°1951
    * @note After ref 20110524°1941 'Justin Cook → insert into array at specific position'
    * @note The original function had the array given as reference '&$array', that
    *        caused php exclamation 'Call-time pass-by-reference has been deprecated'.
    * @callers PageAlbum.php
    * @param Array $array {} ...
    * @param String $insert {} ...
    * @param Integer $position {} ...
    * @return {Array} On success the wanted array, on fail it dies
    */
   public static function array_insert($array, $insert, $position = -1) : Array
   {
      // ()
      $position = ($position < 0) ? (count($array)) : $position ;      // fixed line - handles position other negative values
      $position = ($position > count($array)) ? (count($array)) : $position ;  // added line - handles position greater than existing array

      if ( $position !== (count($array)) )
      {
         $ta = $array;
         $tmp = array();                                               // fight error "Unsupported operand types" below (if position greater than existing array)
         for ( $i = $position; $i < (count($array)); $i++ )
         {
            if ( ! isset($array[$i]) )
            {
               die ( print_r($array, TRUE)
                    . "\r\nInvalid array: All keys must be numerical and in sequence." )
                     ;
            }
            $tmp[$i+1] = $array[$i];
            unset($ta[$i]);
         }
         $ta[$position] = $insert;
         $array = $ta + $tmp;                                          // php error "Unsupported operand types" if $tmp is not defined above (when testing with position greater than the existing array)
         //print_r($array);
      }
      else
      {
         $array[$position] = $insert;
      }

      ksort($array);

      // Won't pass the array as reference, so we need return array value
      return $array;
   }

   /**
    * This function gets the accurate time as a 'float' in a string.
    *
    * @id 20110526°2251
    * @ref http://php.net/manual/de/function.microtime.php
    * @callers ..
    * @return string The wanted string with the accurate time
    */
   public static function getBcTime()
   {
      list($u, $s) = explode(' ',microtime());
      return bcadd($u, $s, Glb::GLR_iPRECISION_BC_TIMEGAUGING);
   }


   /**
    * This function gets the printable duration string for two bctimes
    *
    * @id 20110526°2252
    * @callers ..
    * @param $sStart {String} Start time
    * @param $sStop {String} Stop time
    * @return String The wanted printable string
    */
   public static function getBcTimespan($sStart, $sStop)
   {
      $sResult = bcsub($sStop, $sStart, Glb::GLR_iPRECISION_BC_TIMEGAUGING);
      return $sResult;
   }

   /**
    * This function returns an array with folderfamilynames.
    *
    * @id 20110522°1341
    * @see todo 20110522°1343 'optimize folder-familynames generation'
    * @callers Only • method 20190202°1121 PageAlbum::doPageAlbum
    * @param $sBasepath {string} The folder in which to search for folders
    * @return Array The array with the foldersfamilynames
    */
   public static function getDirFamnams($sBasepath)
   {
      $a = array();
      $aa = self::getDirFams2($sBasepath);                             // ? [mark 20190203°0711 refactor]

      // Reduce (for simplicity, we assume, the filefamilyname is ALWAYS index 0)
      $aFofamnam = array();
      foreach ($aa as $a)
      {
         $aFofamnam[] = $a[0];
      }

      return $aFofamnam;
   }

   /**
    * This function makes an array of arrays with target-twin-folders
    *  where to search for buddyfiles
    *
    * @id 20110427°2221
    * @callers • file 20110427°2142 PageRaw.php • func getDirFamnams
    * @param {String} $sTargetfolder The folder in which to search for folders
    * @return Array|Boolean False = error, array = the found foldersfamilies (typically a '*' plus one '*.s' folder)
    */
   public static function getDirFams1($sTargetfolder)
   {
      // Paranoia [seq 20110930°0122] force valid folder
      //  if the given path is invalid, it shall fallback to the Daftari built-in one
      if (! is_dir($sTargetfolder))
      {
         // PROVISORY — Todo: softcode this value
         $sTargetfolder = "X:\\workspaces\\daftaridev\\trunk\\daftari\\docs\\imgs26"; // marker 20190201°0813 'gallery'
      }

      $aDirs = array();
      if ( substr($sTargetfolder,-1) !== '\\' )
      {
         $sTargetfolder .= '\\';
      }

      if (! is_dir($sTargetfolder))
      {
         $s = '<p>Error sTargetfolder = ' . $sTargetfolder . '</p>';
         echo($s);
         return FALSE;
      }

      $handle = opendir($sTargetfolder);
      if ($handle)
      {
         while (FALSE !== ($file = readdir($handle)))
         {
            if ($file == '.')
            {
               continue;
            }
            if ($file == '..')
            {
               continue;
            }
            if ($file == Glb::GLR_sPATHPART_INTERCALARY)               // 'img'
            {
               if (Glb::bToggle_FALSE)                                 // Shutdown 20180325°0331
               {
                  continue;
               }
            }
            if (substr($file,0,3) !== Glb::GLR_sPATHPART_INTERCALARY)  // skip 'img'
            {
               if (Glb::bToggle_FALSE) // shutdown 20180325°0332
               {
                  continue;
               }
            }
            $aDirs[] = $file;
         }
         closedir($handle);

         // Paranoia [seq 20110522°1531]
         if (sizeof($aDirs) < 1)
         {
            return FALSE;
         }

         // Not sure wether this is necessary, but we rely on a sorted array
         $b = sort($aDirs);
         if (! $b)
         {
            return FALSE;
         }

         $aaFoFams = array();
         $aaFoFams[0] = array(0 => $aDirs[0]);
         for ($i = 1; $i < sizeof($aDirs); $i++)
         {
            $s1 = $aDirs[$i-1];
            $s2 = $aDirs[$i];
            if (Glb::bToggle_FALSE) {
               // original algo
               $iMatch = strncmp ($s2, $s1, strlen($s1));
            } else {
               // new experimental algo
               $iMatch = self::isSameFileFamily ($s1, $s2);
            }

            if ($iMatch == 0)
            {
               $iX = sizeof($aaFoFams)-1;
               $aaFoFams[$iX][] = $aDirs[$i];
            }
            else
            {
               $aaFoFams[] = array();
               $aaFoFams[sizeof($aaFoFams)-1][] = $aDirs[$i];
            }
         }
      }
      return $aaFoFams;
   }

   /**
    * This function makes an array of arrays with target folders with images
    *
    * @id 20190203°0421
    * @note This function shall replace func 20110427°2221 getDirFams1
    * @callers • file 20110427°2142 PageRaw.php
    * @param $sTargetfolder {string} The folder in which to search for folders
    * @return Array of arrays with folders with images
    */
   public static function getDirFams2($sTargetfolder)
   {
      // Validate given target folder [seq 20110930°0122]
      //  If the given path is invalid, it shall fallback to just below Daftari
      if (! is_dir($sTargetfolder) || TRUE ) // [mark 20190203°0711 refactor] scanning the gallery lasts too long
      {
         // Fallback
         $sTargetfolder = __DIR__ . '\..\..\..';
      }
      $sTargetfolder = realpath($sTargetfolder);

      // Scan [seq 20190203°0431]
      self::$aFoundDirs = array($sTargetfolder);
      self::getDirFams2_scan();

      // Provisory dummy [seq 20190203°0433]
      $aaFoFams = array(); // dummy
      foreach (self::$aFoundDirs as $s)
      {
         $sNeedle = '↯';
         $sStartchar = mb_substr($s, 0, 1);
         if ( $sStartchar !== $sNeedle )
         {
            array_push($aaFoFams, array($s));
         }
      }

      return $aaFoFams;
   }

   /**
    * This function performs a recursive directory search
    *
    * @id 20190203°0511
    * @note See ref 20110102°0121 'recursive directory listing'
    * @todo 20190203°0513 : Not using a class field but only an input
    *        parameter for the current path were much more elegant, but
    *        also a bit more complicated. Possibly refactor this.
    * @callers • func 20190203°0421 getDirFams2 • this func itself
    */
   public static function getDirFams2_scan() // getDirFams2_scan($sTargetfolder)
   {
      // Prologue [seq 20190203°0515]
      $iNdxThis = sizeof(self::$aFoundDirs) - 1;
      $bHasImages = FALSE;
      $sPathGiven = self::$aFoundDirs[$iNdxThis];

      // Guarantee folder mark [seq 20190203°0521]
      if ( (substr($sPathGiven, -1) !== '/') && ( substr($sPathGiven, -1) !== '\\' ) )
      {
         $sPathGiven .= '/';
         self::$aFoundDirs[$iNdxThis] = $sPathGiven;
      }

      // [seq 20190203°0522]
      if ( $handle = opendir($sPathGiven) )
      {
         // [seq 20190203°0523]
         while ( FALSE !== ( $sFile = readdir($handle) ) )
         {
            $sPathNext = $sPathGiven . $sFile;

            // [seq 20190203°0524]
            // note : Possible values of filetype are fifo, char, dir,
            //         block, link, file, socket, unknown
            if ( filetype($sPathNext) === 'dir')
            {
               // Skip specific folders [seq 20190203°0525]
               if ( ($sFile !== '.') && ($sFile !== '..')
                   && ($sFile !== 'daftari.DATA')
                    && ($sFile !== 'jslibs')
                     && ($sFile !== 'phplibs')
                      )
               {
                  clearstatcache();                                    // 'clear file status cache'
                  array_push( self::$aFoundDirs, $sPathNext );
                  self::getDirFams2_scan();
               }
            }
            else
            {
               // Contains this folder images [seq 20190203°0526]
               $sExt = pathinfo($sPathNext, PATHINFO_EXTENSION);
               switch ($sExt)
               {
                  case 'bmp'  : $bHasImages = TRUE; break;
                  case 'gif'  : $bHasImages = TRUE; break;
                  case 'jpeg' : $bHasImages = TRUE; break;
                  case 'jpg'  : $bHasImages = TRUE; break;
                  case 'png'  : $bHasImages = TRUE; break;
                  case 'svg'  : $bHasImages = TRUE; break;
                  case 'tif'  : $bHasImages = TRUE; break;
                  case 'txt'  : $bHasImages = TRUE; break; // ?
                  case 'webp' : $bHasImages = TRUE; break;
                  default :
               }
            }
         }
         closedir($handle);
      }

      if (! $bHasImages) {
         self::$aFoundDirs[$iNdxThis] = '↯' . self::$aFoundDirs[$iNdxThis];
      }

      // // Since self::$dirs is a field of this class, it needs not be returned
      //return self::$dirs;
   }

   /**
   }
   }
    * This function makes an array with target folders to search for
    *  buddyfiles. This is not a plain directory listing, but it is
    *  adjusted for a special folderstructure.
    *
    * @id 20110427°2225 [[20110427°2221]]
    * @note For recursively searching directories compare ref 20110102°0121
    * @note See note 20110427°2235 'redundant dir listings'
    * @callers PageAlbum.php, PageRaw.php
    * @param $sPath {string} The folder in which to search for folders
    * @return Array The array with the found folders
    */
   public static function getDirs1($sPath)
   {
      $aDirs = array();
      if ( substr($sPath,-1) !== '\\' )
      {
         $sPath .= '\\';
      }
      $handle = opendir($sPath);
      if ($handle)
      {
         while (FALSE !== ($file = readdir($handle)))
         {
            if ($file == '.')
            {
               continue;
            }
            if ($file == '..')
            {
               continue;
            }
            if ($file === Glb::GLR_sPATHPART_INTERCALARY)              // 'img'
            {
               continue;
            }
            if (substr($file,0,3) !== Glb::GLR_sPATHPART_INTERCALARY)  // skip 'img'
            {
               continue;
            }
            if (substr($file,-6) == '.EMPTY')
            {
               continue;
            }
            $aDirs[] = $file;
         }
         closedir($handle);
      }
      return $aDirs;
   }

   /**
    * This is the entry point function from the raw folders page -- NO!
    *
    * @id 20110522°1541
    * @note What about the array with target-twin-folders to search for buddyfiles.
    * @callers Only func 20110428°2121 get_filefamily 
    * @param $sParentfolder {string} ..
    * @return Array|Boolean False = error, array = folderfamily
    */
   public static function getFolderfamilyFromParent($sParentfolder)
   {

      // Provisory dummy [seq 20190203°0721] // [mark 20190203°0711 chg]
      return array($sParentfolder);


      $aDirs = array();
      if ( substr($sParentfolder, -1) !== '\\' ) {
         $sParentfolder .= '\\';
      }

      $sGrandparent = dirname($sParentfolder);

      $handle = opendir($sGrandparent);
      if ($handle)
      {
         while (FALSE !== ($file = readdir($handle)))
         {
            if ($file == '.')
            {
               continue;
            }
            if ($file == '..')
            {
               continue;
            }
            $iMatch = strncmp ($sGrandparent . '/' . $file, $sParentfolder, strlen($sParentfolder) - 1);
            if ($iMatch !== 0)
            {
               continue;
            }
            $aDirs[] = $file;
         }
         closedir($handle);

         // Paranoia
         if (sizeof($aDirs) < 1)
         {
            return false;
         }
      }

      return $aDirs;
   }

   /**
    * This function tells, whether two files belong to the same family.
    *
    * @id 20180328°0111
    * @callers Only func 20110427°2221 getDirFams1
    * @param $sFileOne {string} Filename of first file
    * @param $sFileTwo {string} Filename of second file
    * @return integer Flag 1 = no, 0 = yes
    */
   private static function isSameFileFamily($sFileOne , $sFileTwo)
   {

      // First very primitive algo
      $a1 = explode('.',$sFileOne);
      $a2 = explode('.',$sFileTwo);

      if ((count($a1) < 2) || (count($a2) < 2))
      {
         return 1;
      }

      if (($a1[0] === $a2[0]) && ($a1[1] === $a2[1]))
      {
         return 0;
      }
      else
      {
         return 1;
      }
   }

   /**
    * This function guarantees the file to valid for ...
    *
    * @id 20110516°2101
    * @note Check what exatly about urlencode() and rawurlencode()
    * @callers class.obyekt.php readbuddyfile(), readShadowbuddyfile() - the both use a fullfilename
    * @param $sFilename {string} The filename to be mangled. Only  'http://' are mangled.
    * @param $bForce {bool} Optional. If set true, the function does not skip strings with 'http://'
    *           prefix (because for plain filenames, it cannot discern url and localdrive).
    * @return string The mangled filename
    */
   public static function mangleUrlFilename($sFilename, $bForce = false)
   {
      /*
      issue 20110517°0121 ''
      matter : The filename must only be mangled for urls (remote paths),
         not for localdrive pathnames. So far, the callers obey to use the
         mangler only for remote paths. But it were nice, if the mangler itself
         were smart enough to detect the condition and react respectively.
      ◦
      */

      // note : Only URLs must be mangled, localdrive names not.
      if (! $bForce)
      {
         $i = strspn ( $sFilename , 'http://' );
         if ( $i < 7 )
         {
            return $sFilename;
         }
      }

      // note : See todo 20110524°1933 'centralize url lists'
      if (Glb::bToggle_TRUE)
      {
         $i = strpos($sFilename," ");
         $i2 = strpos($sFilename,"'");
         $i3 = strpos($sFilename,"&");
         $i4 = strpos($sFilename,"#");
         if (($i !== false) or ($i2 !== false) or ($i3 !== false) or ($i4 !== false))
         {
            $sFilename = str_replace ( " " , '%20' , $sFilename );
            $sFilename = str_replace ( '#' , '%23' , $sFilename );
            $sFilename = str_replace ( "&" , '%26' , $sFilename );
            $sFilename = str_replace ( "'" , '%27' , $sFilename );
         }
      }
      else
      {
         $sFilename = rawurlencode($sFilename);
      }

      return $sFilename;
   }

   /**
    * This function guarantees the file to be valid for ...
    *
    * @id 20110523°1442
    * @note Check what about a php function like urlencode() and rawurlencode() or the like?
    * @see e.g. ref 20110523°1451 'wikipedia → list ofxml and html character entity_references'
    * @callers class.obyekt.php buildShadowbuddyfile()
    * @param $sText The string to be mangled.
    * @return string The mangled string
    */
   public static function mangleXmlContent($sText)
   {
      /*
      issue 20110523°1443 'nasty effect'
      matter : Object 'imgp20051016/img/_20061223-0234_biz&divers213_' runs fine
         inside NetBeans debugging, but leaves a blank page in standalone browser.
      status : ?
      */

      $s = str_replace('&', '&amp;', $sText);

      return $s;
   }

   /**
    * This function guarantees a string to be useful as url after
    *   being passed through a form/submit cycle. (???)
    *
    * @id func 20110524°1931
    * @explanation We pass filenames via form submit mechanism. In this process,
    *     periods are replaced underlines. PHP's rawurlencode does not cover the
    *     period (and not underline and dash). So we just make our own extension.
    *     Interestingly, urldecode() then decodes this correctly without any ado.
    * @callers Only • PageAlbum.php
    * @param $sIn {string} The string to be encoded
    * @return string The encoded string
    */
   public static function mkRawUrlEncode($sIn)
   {
      $sOut = rawurlencode($sIn);
      $sOut = str_replace(".", '%2e',$sOut);                           // The extension
      return $sOut;
   }

   /**
    * This function guarantees a string to be useful as url.
    *  This function may possibly be replaced by rawurlencode() or urlencode().
    *
    * @id 20110502°1821
    * @see ref 20110502°1625 'php → filter.filters.sanitize'
    * @see ref 20110502°1624 'php → function.filter-var'
    * @see ref 20110502°1623 'blooberry → characters be encoded and why'
    * @see ref 20110502°1621 'decodeunicode → basic latin number sign'
    * @note Only $s = filter_var(sFilename, FILTER_SANITIZE_STRING) did not
    *     work as expected. So we retreat provisory to the brute force way.
    * @todo Make it based on PHP's respective function(s)
    * @callers • Obyekt.php • PageAlbum.php • PageObject.php
    * @param $sFile {string} The filename to be mangled
    * @return string The mangled filename
    */
   public static function mkUrlEncode($sFile)
   {
      // [seq 20110502°1823]
      // note : See todo 20110524°1933 'centralize url lists'
      if (Glb::bToggle_TRUE)
      {
         $s = str_replace(' ', '%20',$sFile);
         $s = str_replace('#', '%23',$s);
         $s = str_replace('&', '%26',$s);
         $s = str_replace("'", '%27',$s);
      }
      else
      {
         $s = rawurlencode($sFile);
      }
      return $s;
   }

   /**
    * This variable only serves the recursive collection of folders
    *
    * @id
    * @var String
    */
   private static $aFoundDirs = [];
}
