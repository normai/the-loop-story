<?php

/**
 * This module generates the payload HTML of one Raw Folder page
 *
 * file      : 20110427°2142
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 * callers   :
 */

namespace Trekta\Daftari;

use Trekta\Daftari as TD;
use Trekta\Daftari\Globals as Glb;

/**
 * This class shall generate the Gallery Raw page content
 *
 * @id 20190202°0311
 * @callers
 */
class PageRaw
{

   /**
    * This method performs the former script level code ...
    *
    * @see Issue 20190203°0411 'folder families creation'
    * @caller
    * @id method 20190202°0321
    */
   public static function doPageRaw()
   {
      // () Prologue [seq 20120104°0911]
      $sCR = Glb::$sTkNL;
      $bSHOW_FOLDERNUMBERS = false;                                    // [var 20110423°0732 user setting]
      $Glr_bRawView_OrderReverse = FALSE; // TRUE                      // [var 20110423°0731 user setting]
      $iICONS_MAXDIM = 50;                                             // original size is 60, small is 50 // [var 20110423°0733 user setting]
      $iMAXCOLUMNS = 5; // 7;                                          // 8 // [var 20110423°0734 user setting]

      // () Scan for the raw folders [line 20120104°0915]
      if (Glb::bToggle_FALSE) {
         $aaFofams = TD\FileFamilies::getDirFams1(Glb::$Glr_sBasePathImgFolders); // marker 20190201°0813 'gallery refactor'
      }
      else {
         $aaFofams = TD\FileFamilies::getDirFams2('');
      }
      // Now ~ e.g. aa[0] = 'img12670201', aa[1] = 'img12670201.s'

      // Build URL pointing to folder scan base [seq 20190203°0641]
      // Glb::$Glr_sUrlImageRepoBase
      // The building blocks :
      //  - REQUEST_SCHEME e.g. 'http'
      //  - SERVER_NAME 'localhost'
      //  - SCRIPT_NAME e.g. "/workspaces/daftaridev/trunk/daftari/docs/gallery56raw.html"
      $sScheme = $_SERVER['REQUEST_SCHEME'];
      $sServer = $_SERVER['SERVER_NAME'];
      $s = $_SERVER['SCRIPT_NAME'];
      $iCut = strlen('/daftari/docs/gallery56raw.html');
      $iCut = strlen($s) - $iCut;
      $s = substr($s, 0, $iCut);
      $s = $sScheme . '://' . $sServer . $s;
      Glb::$Glr_sUrlImageRepoBase = $s;                                // e.g. "http//localhost/daftaridev/trunk"

      // Guarantee valid aaFofams [seq 20120104°0921]
      if (! isset($aaFofams))
      {
         $aaFoFams = array();
      }
      if (! $aaFofams)
      {
         // Artificial [line 20120104°1221]
         $aaFofams = array( 0 => array(0 => '*', 1 => '**'), 1 => array(0 => '***', 1 => '****') );
      }

      // Process sort order setting [seq 20120104°0925]
      if ($Glr_bRawView_OrderReverse)
      {
         $aaFofams = array_reverse($aaFofams);
      }

      // Build table [seq 20120104°0931]
      $iCol = 0;
      $sOut = '';
      if (Glb::bToggle_FALSE) // TRUE FALSE debug switch
      {
         //$sOut .= $sCR . "DUMP aaFofams = " . var_dump($aaFofams);
         $s = self::varDump($aaFofams);                                // PhanTypeMismatchArgument 20190312°0221 not-yet-solved
      }
      $sOut .= $sCR . '<table border="1">';
      $sOut .= $sCR . ' <tr>';

      // () Loop over the folderfamilies array [seq 20120104°0935]
      // Processing one folderfamily per interation, it shall be represented by one
      //   folderfamily icon, which shall link to the (not yet existing) folderfamily page.
      foreach ($aaFofams as $aFofam)
      {
         // () Preparation [seq 20120104°0941]
         $sParentfolder = $aFofam[0]; // e.g. "X:\workspaces\daftaridev\trunk/"
         $sParentfolder = substr($sParentfolder, strlen(Glb::$Glr_sLocation_ScanBase_Fs)); // "/", "/daftari/", ..

         // () Possibly insert row break [seq 20120104°0942]
         if ($iCol % $iMAXCOLUMNS == 0)
         {
            $sOut .= $sCR . ' </tr>';
            $sOut .= $sCR . ' <tr>';
         }

         // [seq 20120104°0943]
         // For the current folderfamily we want determine an icon. The icon
         //  can be found inside the folderfamily or we use the fallback icon
         $sDir = $aFofam[0];

         // Provisory patch [seq 20190204°0641]
         // Value e.g. $sDir = "X:\workspaces\daftaridev\trunk/"
         $s = substr($sDir, strlen(Glb::$Glr_sLocation_ScanBase_Fs)); // "/", "/daftari/", ..

         $sReposicon = Glb::$Glr_sLocation_ScanBase_Url . $s . 'foldericon.jpg'; // 'reposicon.jpg'
         // Value e.g. "http://localhost/workspaces/daftaridev/trunk/daftari/docs/imgs26/foldericon.jpg"

         // note : To see the existence of the file, 'is_file()' does not suffice,
         //  since that works only for the filesystem, but here we have an URL here.
         $file = @fopen($sReposicon, 'r');

         if (! $file) {
            // Fallback icon
            $sReposicon = Glb::$Glr_sLocation_Daftari_Url . '/' . 'docs/imgs26/20090504o2215.dummy0180.v0.p99q33.jpg';
         }

         // (X) Assemble values for below [seq 20120104°0944]
         // (X.1) Link to raw folder album
         // E.g. "gallery56album.html?album=rawfolder.img12670204kp"
         $sLink = './gallery56album.html?album='
                 . Glb::GLR_sLINKPART_RAWFOLDER                        // 'rawfolder.'
                  . $sParentfolder                                     //
                   ;
         // (X.2) Caption
         $sCaption = substr($aFofam[0], strlen(Glb::$Glr_sLocation_ScanBase_Fs)); // "/", "/daftari/", ..
         $sCaption = str_replace('/', '/&#8203;', $sCaption);

         // () [seq 20120104°0945]
         $sOut .= $sCR . '  <td class="album">';
         $sOut .= $sCR . '        <a href="' . $sLink . '">';
         $sOut .= $sCR . '         <img src="' . $sReposicon
                 . '" width="' . $iICONS_MAXDIM . '" height="' . $iICONS_MAXDIM
                  . '"></a>'
                   ;
         $sOut .= $sCR . '        <br><small>&nbsp;'. $sCaption . '</small>';
         if ($bSHOW_FOLDERNUMBERS)
         {
            $sOut .= $sCR . '        <br> ' . ($iCol + 1);
         }
         $sOut .= $sCR . '  </td>';
          $iCol++;
      }
      $sOut .= $sCR . ' </tr>';
      $sOut .= $sCR . '</table>';
      
      $sOut .= $sCR . '<p>';
      $sOut .= $sCR . '              This are ' . sizeof($aaFofams) . ' archives';
      $sOut .= $sCR . '</p>';

      // [line 20190202°0323]
      self::$sOutMain = $sOut;
   }

   /**
    * This method delivers var_dump as string
    *
    * @id method 20190203°0621
    * @ref Written after [ref 20190203°0613]
    *    https://stackoverflow.com/questions/139474/how-can-i-capture-the-result-of-var-dump-to-a-string
    * @status Does not work as expected, with all flavours the
    *    debugger just shows 'Evaluating ..' instead the value.
    * @todo Shift this method to some general utility class or attic
    * @param Object $oIn The variable to be returned as string
    * @return String The wanted string
    */
   public static function varDump($oIn)
   {
      if (Glb::bToggle_FALSE) {
         // Version 1 [seq 20190203°0631] does not work as expected
         ob_start();
         var_dump($oIn);
         $sRet = ob_get_clean();
         return $sRet;
      }

      if (Glb::bToggle_FALSE) {
         // Version 2 [seq 20190203°0632] does not work as expected
         ob_start();
         var_dump($oIn);
         $sRet = ob_get_contents();
         ob_get_clean();
         //error_log($sRet);
         return $sRet;
      }

      if (Glb::bToggle_TRUE) {
         // Version 3 [seq 20190203°0633] how does this work?
         $sRet = var_export($oIn, true);
         return $sRet;
      }

   }

   /**
    * This static field presents the page HTML main payload fragment
    *
    * @id field 20190202°0341
    * @var String
    */
   public static $sOutMain = '';

}

/* eof */
