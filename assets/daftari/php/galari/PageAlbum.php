<?php

/**
 * This module generates the payload HTML of one Album page
 *
 * file      : 20110428°0222
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 * note      :
 */

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

// Script level [seq 20110428°0301]
include_once(__DIR__ . '/../galari/Obyekt.php');
include_once(__DIR__ . '/../core/ErrorHandler.php');
set_error_handler("Trekta\\Daftari\\my_exceptons_error_handler");

/**
 * This class generates the Album page HTML payload ..
 *
 * @id class 20190202°1111
 */
class PageAlbum
{

   /**
    * This method .. performs the formerly script level code ...
    *
    * @caller
    * @id method 20190202°1121
    */
   public static function doPageAlbum()
   {

      // Prologue [seq 20110428°0302]
      $bRAWVIEW_ORDER_REVERSE = false;                                 // [var 20110428°0231]
      $iMAXOBJECTS = 24; // 0, 8, 24 // 0 means no limit               // [var 20110428°0232]
      $sCR = Glb::$sTkNL;                                              // [var 20110428°0233]

      // () React on form postings [seq 20110513°1622]
      if (isset($_POST[Glb::POSTKEY_GLR_album_form_was_sent]))
      {
         self::evaluate_post();
      }

      // Provide default album
      // e.g. 'all', 'drawings', 'rawfolder.img12670204kp', 'rawfolder.img20080301',
      //    'rawfolder.img20090101', 'rawfolder.img20090408'
      $sAlbum = 'drawings';                                            // [var 20110428°0303]

      // Retrieve album title parameter [seq 20110428°0304]
      if (array_key_exists(Glb::GET_KEY_GLR_album, $_GET))
      {
         // Request is coming with valid get parameters from the browser
         $sAlbum = $_GET[Glb::GET_KEY_GLR_album];
      }
      else
      {
         // Request coming from the debug environment, without a specified album
         // but we need a filled in _GET[] array e.g. in class.obyekt.php
         $_GET[Glb::GET_KEY_GLR_album] = $sAlbum;
      }

      // Retrieve page parameter [seq 20110428°0241]
      $sPage = 1;
      $sPage = (array_key_exists(Glb::GET_KEY_GLR_page, $_GET)) ? $_GET[Glb::GET_KEY_GLR_page] : $sPage;
      $iPage = (int) $sPage;

      // Is it a raw folder album? [seq 20110428°0242]
      $bIsRaw = false;
      $i = strspn($sAlbum, Glb::GLR_sLINKPART_RAWFOLDER);              // Find length of matching characters from the left
      if ($i == strlen(Glb::GLR_sLINKPART_RAWFOLDER))
      {
         $bIsRaw = true;
         $sParentFolder = substr($sAlbum, $i);
      }

      // Generate raw folder album different from normal album [seq 20110428°0243]
      if ($bIsRaw === true)
      {
         $s = Glb::$Glr_sLocation_ScanBase_Fs . $sParentFolder;
         $aObjects = self::get_buddyfiles2($s);
         if ($bRAWVIEW_ORDER_REVERSE)
         {
            $aObjects = array_reverse($aObjects);
         }

         //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         /*
         feature 20110516°0411 'rawfolders auto-walk-through'
         function : Do not rotate through only one album, but continue to the next.
         note : So far only forward, backward rotates through the one and only album.
         location : file 20110428°0222 seq 20110516°0412 'rawfolders auto-walk'
         */

         // () Raw-folders auto-walk [seq 20110516°0412]
         // (.1) Retrieve them all [seq 20110428°0305]
         $aFofamnams = FileFamilies::getDirFamnams(Glb::$Glr_sBasePathImgFolders);
         $sThisFolder = '';
         $sNextFolder = '';
         // (.2) Build the forward link [seq 20110428°0306]
         $i = strspn($sAlbum, 'rawfolder.');
         if ( $i > 0 )
         {
            $sThisFolder = substr($sAlbum,$i);
         }
         // [seq 20110428°0307]
         $iKi = array_search($sThisFolder, $aFofamnams);
         if ($iKi < ( sizeof($aFofamnams) - 1) )
         {
            $sNextFolder = $aFofamnams[$iKi + 1];                      // Normal case
         }
         else
         {
            $sNextFolder = $aFofamnams[0];                             // Last page
         }
         $sAlbumNext = 'rawfolder.' . $sNextFolder;
      
         sleep(0);
         //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      }
      else
      {
         // Predefined albums [seq 20110428°0308]
         $aObjects = self::read_albumobjects($sAlbum);
      }
      // Now we have e.g. $aObjects[0] = "X:\workspaces\daftaridev\trunk/20180924o1114.coucang.~.txt"

      // Process chunks [seq 20110509°1441]
      $iColumns = $_SESSION[Glb::SESKEY_iAlbumColumns];
      $iMaxRows = $_SESSION[Glb::SESKEY_iAlbumMaxRows];
      $iImagesPerPage = $iColumns * $iMaxRows;
      $aChunks = array_chunk($aObjects, $iImagesPerPage);

      // Build-prologue [seq 20110428°0244]
      //----------------------------------------------------------
      $sNavi = '';
      $sPrelude = '';
      $sCrumbs = '';
      $sFlicks = '';
      $sTips = '';
      $sLinkPrefix = Glb::$Glr_sLocation_Daftari_Url . '/docs/gallery56album.html?album='; // Must be softcoded! [marker 20190201°0813 'refactor gallery']
      $sPageOfPages = ( sizeof($aChunks) > 0 ) ? sizeof($aChunks) : 1; // Even if chunk has zero object, we have a page no 1
      $sPrelude = $sCR . '<p style="text-align:center; margin-top:0.1em;">';
      $sPrelude .= $sCR . '   Page <b>' . $iPage . '</b>'
                         . ' of <b>' . $sPageOfPages . '</b>'
                          . ' with ' . sizeof($aObjects) . ' images total'
                           ;

      // Build crumbs [seq 20110428°0245]
      for ($i = 0; $i < sizeof($aChunks); $i++)
      {
         $s = $sLinkPrefix . $sAlbum . '&' . Glb::GET_KEY_GLR_page . '=' . ($i + 1);
         // e.g. "http://localhost/workspaces/daftaridev/trunk/daftari/daftari/docs/gallery56album.html?album=rawfolder./daftari/docs/imgs26/&page=2"
         if ($i == ($iPage - 1))
         {
            $sCrumbs .= $sCR . '   <!-- a href="' . $s . '" --><b>' . ($i + 1) . '</b><!-- /a -->' . '&nbsp;';
         }
         else
         {
            $sCrumbs .= $sCR . '   <a href="' . $s . '">' . ($i + 1) . '</a>' . '&nbsp;';
         }
      }

      // Build flicks [seq 20110428°0246]
      if ($iPage > 1)
      {
         $sPgPrev = ($iPage - 1);
      }
      else
      {
         $sPgPrev = $sPageOfPages;
      }
      $sUrlPrev = $sLinkPrefix . $sAlbum . '&' . Glb::GET_KEY_GLR_page . '=' . $sPgPrev;

      // [seq 20110428°0311]
      if ($iPage < sizeof($aChunks))
      {
         $sPgNext = ($iPage + 1);
      }
      else
      {
         $sPgNext = '1';
      }

      // [seq 20110428°0312]
      if (($bIsRaw === true) and ($iPage == $sPageOfPages))
      {
         $sUrlNext = $sLinkPrefix . $sAlbumNext . '&' . Glb::GET_KEY_GLR_page . '=' . 1;
      }
      else
      {
         $sUrlNext = $sLinkPrefix . $sAlbum . '&' . Glb::GET_KEY_GLR_page . '=' . $sPgNext;
      }

      // Build prev/next HTML fragment [seq 20110428°0247]
      $sUrlThis = $sLinkPrefix . $sAlbum . '&' . Glb::GET_KEY_GLR_page . '=' . $iPage;
      $sFlick = '';
      $sFlick .= $sCR . '   <a href="' . $sUrlPrev . '">Previous page</a>' . '&nbsp;';
      $sFlick .= '&nbsp;&nbsp;';
      $sFlick .= $sCR . '   <a href="' . $sUrlNext . '" id="clickthis">Next page</a>' . '&nbsp;';

      // Build autoflick fragment [seq 20110428°0248]
      $sAuto =  $sCR . '      &nbsp;&nbsp;&nbsp;Autoflick:&nbsp;&nbsp;';
      $sAuto .= $sCR . '      <a href="' . $sUrlNext . '" onclick=flickerRun("SLOW") onmouseover="Daftari.XBT2(this, {id:\'ttFlickSlow\'}, \'SLOW\')" >slow</a>&nbsp;&nbsp;';
      $sAuto .= $sCR . '      <a href="' . $sUrlNext . '" onclick=flickerRun("FAST") onmouseover="Daftari.XBT2(this, {id:\'ttFlickFast\'}, \'FAST\')" >fast</a>&nbsp;&nbsp;';
      $sAuto .= $sCR . '      <a href="' . $sUrlThis . '" onclick=flickerStop()      onmouseover="Daftari.XBT2(this, {id:\'ttFlickStop\'}, \'STOP\')" >stop</a>&nbsp;&nbsp;';
      $sAuto .= $sCR . '      =&nbsp;&nbsp;';
      $sAuto .= $sCR . '      <span id="flickstatus" onmouseover="Daftari.XBT(this, {id:\'ttFlickStatus\'})">(loading)</span>';

      // Build tips fragment [seq 20110428°0251]
      $sTips .= $sCR . '<div id="ttFlickFast" class="xbtooltip"> Automatisches Bl&auml;ttern <b>schnell</b> </div>';
      $sTips .= $sCR . '<div id="ttFlickSlow" class="xbtooltip"> Automatisches Bl&auml;ttern <b>langsam</b> </div>';
      $sTips .= $sCR . '<div id="ttFlickStop" class="xbtooltip"> Automatisches Bl&auml;ttern Ende. Dieser Link <b>wirkt bereits bei Ber&uuml;hrung</b>, denn ein Klick gelingt oft nicht.</div>';
      $sTips .= $sCR . '<div id="ttFlickStatus" class="xbtooltip"> Zeigt den aktuellen Zustand von Autoflick (automatisches Bl&auml;ttern) an. </div>';

      // Assemble navigation fragment [seq 20110428°0252]
      $sNavi = '<p>';
      $sNavi .= $sPrelude . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $sNavi .= $sCrumbs . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $sNavi .= $sFlick . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $sNavi .= $sAuto . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $sNavi .= $sTips;
      $sNavi .= $sCR . '</p>';
      self::$sOutNavi = $sNavi;
      //----------------------------------------------------------

      // () Build main page
      // (.) Render form header [seq 20110428°0253]
      $sOut = '';
      if ( $_SESSION[Glb::SESKEY_MaintenanceMode] || TRUE)             // [mark 20190203°0711 refactor]
      {
         $sOut .= $sCR . '   <form action="" method="post">';
         $sOut .= $sCR . '   <input type="hidden" name="'
                . Glb::POSTKEY_GLR_album_form_was_sent
                 . '" value="yes">'
                  ;
      }
      $sOut .= $sCR . '<table>';

      // (.) Render Add/Remove button [seq 20110428°0254]
      if ( $_SESSION[Glb::SESKEY_MaintenanceMode] || TRUE)             // [mark 20190203°0711 refactor]
      {
         $sOut .= $sCR . ' <tr>';
         $sOut .= $sCR . '  <td colspan="' . $iColumns . '" style="text-align:right;">';
         $sOut .= $sCR . '        Albums : ';
         $sOut .= $sCR . '        <input type="submit" name="submit_albums_add" value="    Add    "> &nbsp;';
         $sOut .= $sCR . '        <input type="submit" name="submit_albums_remove" value="Remove">';
         $sOut .= $sCR . '  </td>';
         $sOut .= $sCR . ' </tr>';
      }
      $sOut .= $sCR . ' <tr>';

      // () Finally build the payload HTML [seq 20110428°0255]
      $iCol = 0;
      $aTimegaugeBcTotal = array();
      $aTimegaugeBcPart1 = array();
      $aTimegaugeBcPart2 = array();
      if ( sizeof($aChunks) > 0 )
      {
         $iChunks = sizeof($aChunks);
         if ($iPage > $iChunks)
         {
            $iPage = sizeof($aChunks);
         }

         // Process each single object of this page [seq 20110428°0256]
         foreach ($aChunks[$iPage - 1] as $sBuddy)
         {
            // [seq 20110428°0313]
            $sRelativepath = dirname($sBuddy);
            // E.g. $sBuddy = ""X:\workspaces\daftaridev\trunk/daftari/css//20180921o0243.arrowchar1.~.txt""
            // E.g. $sRelativepath = "X:\workspaces\daftaridev\trunk/daftari/css"

            // [seq 20110428°0314]
            // E.g. $sBuddy = "X:\workspaces\daftaridev\trunk/notes.txt"
            // E.g. $sBuddy = "X:\workspaces\daftaridev\trunk/daftari/docs/imgs26/19730815o1234.schwan.~.txt"
            $buddy = new Obyekt($sBuddy);
            if ($buddy === FALSE)
            {
               $aTimegaugeBcTotal[] = 0; // No more necessary after switching to bctime? // Prevent 'Fatal error: Unsupported operand types' (quirk 20110523°1421)
               $aTimegaugeBcPart1[] = 0; // No more necessary after switching to bctime? // Prevent 'Fatal error: Unsupported operand types' (quirk 20110523°1421)
               $aTimegaugeBcPart2[] = 0; // No more necessary after switching to bctime? // Prevent 'Fatal error: Unsupported operand types' (quirk 20110523°1421)
               continue; // Hm ..
               // todo: Care in advance, that no invalid objects are in the album database.
               // note: The labeling lines appear anyway.
            }
            $aTimegaugeBcTotal[] = $buddy->tTimegaugeBcTotal;
            $aTimegaugeBcPart1[] = $buddy->tTimegaugeBcPart1;
            $aTimegaugeBcPart2[] = $buddy->tTimegaugeBcPart2;

            // Next row? [seq 20110428°0315]
            if (($iCol > 0) and ($iCol % $iColumns == 0))
            {
               $sOut .= $sCR . ' </tr>';
               $sOut .= $sCR . ' <tr>';
            }

            //----------------------------------------------------
            // Sequence nearly identical in PageAlbum.php and PageObject.php [seq 20110428°0316]
            $iSizeWanted = 180;                                        // Finally use something like $_SESSION[Glb::SESKEY_iObjectImagesize];
            $aThumb = $buddy->getThumb($iSizeWanted);                  // ['file'] is with relative path
            if ( $aThumb[Glb::GLR_SHDW_KEY_maxdim] < 1 )
            {
               // [seq 20110428°0317]
               sleep(0);                                               // Error e.g. with '19810315o2321.helikopter'
               $sThumbFile = $buddy->sBasepath2use . $aThumb[Glb::GLR_SHDW_KEY_file];
               $iThumbWidth = $iSizeWanted;
               $iThumbHeight = $iSizeWanted;
            }
            else
            {
               // [seq 20110428°0318]
               $iFactor = $iSizeWanted / $aThumb[Glb::GLR_SHDW_KEY_maxdim];
               $sThumbFile = $buddy->sBasepath2use . $aThumb[Glb::GLR_SHDW_KEY_file];
               $iThumbWidth = (int) ($iFactor * $aThumb[Glb::GLR_SHDW_KEY_width]);
               $iThumbHeight = (int) ($iFactor * $aThumb[Glb::GLR_SHDW_KEY_height]);
            }

            // Boldest debug workaround 20190202°0132
            $sThumbFile = Glb::$Glr_sLocation_Daftari_Url . "/docs/imgs26/19810705o1703.prismlogo.v1.p25q66.jpg";
            //----------------------------------------------------

            $sThumbUrl = FileFamilies::mkUrlEncode($sThumbFile);

            // Seek invalid entries [seq 20110527°1221]
            if (Glb::GLR_bWRITE_ERRORLOG)
            {
               // [seq 20110428°0322]
               $dt = new \DateTime();
               $sTim = $dt->format("Ymd°His");                         // e.g. '20110527°134621' // formerly "Ymj'Gis"

               // [seq 20110428°0323]
               $sErrorlog = Glb::$Ses_sDataDirFullname . '/' . 'dbtxt/errorlog.txt';
               if (! is_file($sErrorlog))
               {
                  $sMsg = '   ' . $sTim . ' Logfile created: ' . $sErrorlog;
                  $i = file_put_contents($sErrorlog, $sMsg, FILE_APPEND);
               }

               // Look for quirks in title [seq 20110428°0324]
               $s = $buddy->aShadowbuddy[Glb::GLR_BUDY_KEY_title];
               $i = stripos($s, 'quirk');                              // stripos with 'i' like case insensitive, as opposed to strpos
               if ($i !== false)
               {
                  $sMsg = Glb::$sTkNL . '   #' . $sTim . ' Quirk        : "' . $sThumbFile . '"';
                  $i = file_put_contents($sErrorlog, $sMsg, FILE_APPEND);
               }

               // Validate filenameextension [seq 20110428°0325]
               $pathInfo = pathinfo($sThumbFile);
               //~~~~~~~~~~~~~~~~~~~~
               // Curious exception 'Undefined index: extension' [issue 20190202°0131]
               if (! array_key_exists ( 'extension' , $pathInfo ) ) {
                  // Debug
                  echo ('<p>Erro 20190202°0132 with "' . $sThumbFile .'"</p>'); // "http://localhost/gallery/"
                  exit();
               }
               //~~~~~~~~~~~~~~~~~~~~
               // [seq 20110428°0326]
               $s = $pathInfo['extension'];
               if (($s !== 'gif') and ($s !== 'jpg') and ($s !== 'png'))
               {
                  $sMsg = Glb::$sTkNL . '   #' . $sTim . ' Filetype wrong : "' . $sThumbFile . '"';
                  $i = file_put_contents($sErrorlog, $sMsg, FILE_APPEND);
               }

               // Validate existence [seq 20110428°0327]
               $f = fopen($sThumbUrl, 'r');                             // e.g. 'http://localhost/img12345678/img/20090124o1841.dir.txt' is wrong but opened
               if (! $f)
               {
                  $sMsg = Glb::$sTkNL . '   #' . $sTim . ' Not found      : "' . $sThumbUrl . '"';
                  $i = file_put_contents($sErrorlog, $sMsg, FILE_APPEND);
               }
               else
               {
                  fclose($f);
               }
            }

            // Debug [seq 20110428°0328]
            $s1 = $buddy->sRelativepath; // e.g. "X:\workspaces\daftaridev\trunk"
            $s2 = $buddy->sFilefamilyname; // e.g. "20180924o1114.coucang"
            $s3 = FileFamilies::mkUrlEncode($s2); // e.g. "20180924o1114.coucang"

            $sLinkToObject = FileFamilies::mkUrlEncode($buddy->sFilefamilyname); // e.g. "19730815o1234.schwan"

            // Render one image [seq 20110428°0332]
            $sOut .= $sCR . '  <td class="album">';
            $sOut .= $sCR . '     <a href="./gallery56object.html?object'
                           . '=' . $sLinkToObject
                            . '">'
                             ;
            $sOut .= $sCR . '      <img src="' . $sThumbUrl . '"';
            $sOut .=              ' width="' . $iThumbWidth . '"';
            $sOut .=              ' height="' . $iThumbHeight . '"';
            $sOut .=              ' alt=' . utf8_encode($buddy->sFilefamilyname) . '>'; // UTF-8 [line 20110526°1731]
            $sOut .= $sCR . '     </a>';
            $sOut .= $sCR . '     <br><small>';

            //----------------------------------------------------
            // [seq 20110428°0333]
            // check 20110515°1822 : The familylinks are no more with percent and maxdim
            //   flavour since we changed the algorithms .. to recover that flavours, a new
            //   mechanism has to be found .. possibly reanimating the Filenameparser class.
            $bTxtWanted = TRUE; // NOT REALLY USED, PERHAPS AS PREPARATION FOR LATER FEATURE
            $b99Wanted = TRUE; // NOT REALLY USED, PERHAPS AS PREPARATION FOR LATER FEATURE
            $aaDbg = $buddy->familylinks();
            foreach($aaDbg as $aLink)
            {
               // Values e.g.
               // $aLink[text] = 132
               // $aLink[url] = "http://localhost/downtown/daftaridev/trunk/G:\work\downtown\daftaridev\trunk/G:\work\downtown\daftaridev\trunk/20180924o1114.coucang.v1.p06q66.jpg"

               // [seq 20110428°0334]
               // note : From the many filefamily members, we want link only some
               //    relevant. Here is a primitive provisory familylink filter, only
               //    for txt files is a link provided so far.
               // todo: Make the familylink filter more useful, notably add the
               //    original version files v0, v1, etc.
               $bMake = FALSE;
               if ($bTxtWanted)
               {
                  // [seq 20110428°0335]
                  if (strpos(($aLink['url']), 'txt'))
                  {
                     $bMake = TRUE;
                     $bTxtWanted = FALSE;
                  }
               }
               if ($b99Wanted)
               {
                  // [seq 20110428°0336]
                  if (strpos(($aLink['url']), '99')) {
                     $bMake = TRUE;
                     $b99Wanted = FALSE;
                  }
               }
               if ($bMake)
               {
                  // Provisory treatment [seq 20190204°0651]
                  $s = $aLink['url']; // e.g. G:\work\downtown\daftaridev\trunk/20180924o1114.coucang.~.txt
                  $s = substr($s, strlen(Glb::$Glr_sLocation_ScanBase_Fs) );
                  $s = Glb::$Glr_sLocation_ScanBase_Url . $s;

                  // [seq 20110428°0337]
                  $sOut .= $sCR . '     <a href="'
                                 . FileFamilies::mkUrlEncode($s)
                                  . '">' . $aLink['text'] . '</a> &nbsp;'
                                   ;
               }
            }
            //----------------------------------------------------
            $sOut .= $sCR . '         </small>';

            // Build .. [seq 20110428°0338]
            try
            {
               $s = utf8_encode($buddy->aShadowbuddy[Glb::GLR_BUDY_KEY_title]); // UTF-8 [line 20110526°1731`02]
            }
            catch ( \ErrorException $e )
            {
               $s = 'Quirk_20110518o2224';
               // E.g. aShadowbuddy[xmlerror] = "File load failed: 'http://localhost/gallery/dbtxt/shadow/img20080301/_20080315-1931_gaudi_.xml'" [example 20110526°1621]
            }

            // Build .. message [seq 20110428°0342]
            try
            {
               $s2 = utf8_encode($buddy->aShadowbuddy[Glb::GLR_BUDY_KEY_timestamp]); // UTF-8 [line 20110526°1731`03]
            }
            catch ( \ErrorException $e )
            {
               $s2 = 'Quirk_20110518o2225';
            }

            // [seq 20110428°0343]
            $sOut .= $sCR . '     <br>' . $s;                          // filenamecore ?
            $sOut .= $sCR . '     <br><small>&#35; ' . $s2 . '</small>'; // albums list ?

            // Build .. fragment [seq 20110503°1123]
            //  See issue 20110503°1122 'redundant sequences'
            if ( $_SESSION[Glb::SESKEY_bDebug] === TRUE )
            {
               $sOut .= $sCR . '<p style="background-color:LightCoral;"><small><small>Debug Obyekt:';
               $sOut .= $sCR . '      <br><i>sFilefamilyname</i> = ' . utf8_encode($buddy->sFilefamilyname);
               $sOut .= $sCR . '      <br><i>sBuddyfullfilename</i> = ' . utf8_encode($buddy->sBuddyfullfilename);
               $sOut .= $sCR . '      <br><i>sBuddyfilename</i> = ' . utf8_encode($buddy->sBuddyfilename);
               $sOut .= $sCR . '      <br><i>sAbsolutepath</i> = ' . utf8_encode($buddy->sAbsolutepath);
               $sOut .= $sCR . '      <br><i>sBasepath2use</i> = ' . utf8_encode($buddy->sBasepath2use);
               $sOut .= $sCR . '      <br><i>sRelativepath</i> = ' . utf8_encode($buddy->sRelativepath);
               $sOut .= $sCR . '    </small></small></p>';
            }

            // Input albums [seq 20110428°0344]
            if ($_SESSION[Glb::SESKEY_MaintenanceMode])
            {
               // Show albums [seq 20110428°0345]
               $sOut .= $sCR . '   <br><span style="color:#e68a2e;"><small>'; // #ff9933
               $s = '';
               foreach ($buddy->aShadowbuddy[Glb::GLR_BUDY_KEY_albums] as $sAlb) // 'albums'
               {
                  // [seq 20110428°0346]
                  if ($sAlb == 'all')
                  {
                     continue;                                         // Skip 'all' album
                  }
                  if ($s !== '')
                  {
                     $s .= ', ';
                  }
                  $s .= utf8_encode($sAlb);                            // UTF-8 [line 20110526°1731´04]
               }
               $sOut .= $s;
               $sOut .= '</small></span>';

               /*
               issue 20110523°0242 'problem with dots'
               title : About the problem with dots.
               matter : Here were the place if we want replace dots in folder
                  or/and filenames in advance already in the HTML source. But first
                  we try to repair it afterwards when evaluating the post, where
                  the dots are replaced by underlines then.
               location : file 20110428°0222 PageAlbum.php seq ..
               status : open
               */

               // [seq 20110428°0347]
               // ref : See issue 20110523°0242 'problem with dots'
               // note : The input mechanism seems to replace '.' by '_', so
               //   the real filename is not allowed .. also we have to expect
               //   problems with filenames containing a '.' in the middle!!!
               $sPostfix = $buddy->sRelativepath . '/' . $buddy->sFilefamilyname;
               $sPostfix = FileFamilies::mkRawUrlEncode($sPostfix);
               $sOut .= $sCR . '     <br>   <input type="text" name="input_'
                      . $sPostfix . '" value="" size="12" maxlength="64">'
                       ; // maxlength?
            }

            $sOut .= $sCR . '  </td>';

            // Counter [seq 20110428°0348]
            $iCol++;
            if ( ($iMAXOBJECTS > 0) and ($iCol >= $iMAXOBJECTS))       // Debug
            {
               break;
            }
         }
      }

      // [seq 20110428°0352]
      $sOut .= $sCR . ' </tr>';
      $sOut .= $sCR . '</table>';
      if ($_SESSION[Glb::SESKEY_MaintenanceMode])
      {
         $sOut .= $sCR . '   </form>';
      }

      $sOut .= $sCR . '<p style="background-color:Moccasin;">Number of objects on this page: ' . ($iCol) . '</p>';

      //----------------------------------------------------------
      // [seq 20110428°0353]
      if ( ($_SESSION[Glb::SESKEY_bDebug] === TRUE ) || TRUE)
      {
         $sOut .= $sCR . '<table style="background-color:LightGreen;" align="left"><tr><td colspan="2">Debug 20110428°0353 :</td></tr>';
         $sOut .= $sCR . ' <tr><td>$Glr_sBasePathImgFolders&nbsp;</td><td>: ' . Glb::$Glr_sBasePathImgFolders . '</td></tr>';
         $sOut .= $sCR . ' <tr><td>$Glr_sBasePathToUse</td><td>: ' . Glb::$Glr_sBasePathToUse . '</td></tr>';
         $sOut .= $sCR . ' <tr><td>$Glr_sUrlImageRepoBase</td><td>: ' . Glb::$Glr_sUrlImageRepoBase . '</td></tr>';
         $sOut .= $sCR . '</table>';
      }
      //----------------------------------------------------------

      // [seq 20110428°0354]
      $nTimegaugeBcPage = FileFamilies::getBcTimespan
                         ( Glb::$Bct_sExecutionStartTime
                          , FileFamilies::getBcTime()
                           );

      // [seq 20110428°0355]
      $sOut .= $sCR . '<p style="font-size:small; background-color:Chartreuse; clear:both;">';
      $sOut .= $sCR . '    Execution time total &nbsp;= <b>' . $nTimegaugeBcPage . '</b> sec';
      $nTimegaugeBcTotalSum = '0';
      $nTimegaugeBcPart1Sum = '0';
      $nTimegaugeBcPart2Sum = '0';
      for ( $i = 0; $i < sizeof($aTimegaugeBcTotal); $i++ )            // Error 'undefined variable'
      {
         // [seq 20110428°0356]
         $nProportionParts = bcdiv( $aTimegaugeBcPart1[$i] , $aTimegaugeBcPart2[$i], 8 );
         $sOut .= $sCR . '    <br>Construct time ' . ($i + 1) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
         $sOut .=        ' t = ' . $aTimegaugeBcTotal[$i] . '&nbsp;&nbsp;&nbsp;';
         $sOut .=        ' p1 = ' . $aTimegaugeBcPart1[$i] . '&nbsp;&nbsp;&nbsp;';
         $sOut .=        ' p2 = ' . $aTimegaugeBcPart2[$i] . '&nbsp; sec &nbsp;&nbsp;&nbsp;';
         $sOut .=        ' - prop 1/2 &nbsp; = &nbsp;';
         $sOut .=        $nProportionParts . '%';
         $nTimegaugeBcTotalSum = bcadd($nTimegaugeBcTotalSum, $aTimegaugeBcTotal[$i], Glb::GLR_iPRECISION_BC_TIMEGAUGING);
         $nTimegaugeBcPart1Sum = bcadd($nTimegaugeBcPart1Sum, $aTimegaugeBcPart1[$i], Glb::GLR_iPRECISION_BC_TIMEGAUGING);
         $nTimegaugeBcPart2Sum = bcadd($nTimegaugeBcPart2Sum, $aTimegaugeBcPart2[$i], Glb::GLR_iPRECISION_BC_TIMEGAUGING);
      }

      // Add paranoia 20160609o0421 prevent Fatal error 'Uncaught exception Division by zero' below
      $nProportion = 0;
      if ($nTimegaugeBcTotalSum > 0)
      {
         $nProportion = bcdiv( $nTimegaugeBcTotalSum, $nTimegaugeBcPage );
      }

      // Continuation [seq 20110428°0357]
      $sOut .= $sCR . '    <br>Construct time sum &nbsp; = ' . $nTimegaugeBcTotalSum . '&nbsp;&nbsp;&nbsp;';
      $sOut .= $sCR . '    - ' . $nTimegaugeBcPart1Sum . '&nbsp;&nbsp;&nbsp;';
      $sOut .= $sCR . '    - ' . $nTimegaugeBcPart2Sum . ' sec';
      $sOut .= $sCR . '    <br>Construct time prop &nbsp; = ' . $nProportion . ' %';
      $sOut .= $sCR . '</p>';

      self::$sOutMain = $sOut;
   }

   /**
    * This function evaluates the pages post information
    *
    * @id method 20110513°1621
    * @callers Only this file script level
    */
   private static function evaluate_post()
   {
      // Evaluate add-album button [seq 20110428°0403]
      if ( (isset($_POST[Glb::POSTKEY_GLR_submit_albums_add]))         // 'submit_albums_add'
          or (isset($_POST[Glb::POSTKEY_GLR_submit_albums_remove]))    // 'submit_albums_remove'
           )
      {
         // [seq 20110428°0405]
         $aTargets = array();
         $aAlbums = array();
         $aKies = array_keys($_POST);
         foreach ($aKies as $sKi)
         {
            $i = strspn( $sKi, 'input_');
            if ($i > 0)
            {
               if ($_POST[$sKi] !== '')
               {
                  $s = substr($sKi, $i - 1);                           // [quirk 20110524°1922]
                  $s = rawurldecode($s);
                  $aTargets[] = $s;
                  $aAlbums[] = $_POST[$sKi];
               }
            }
         }
         // Now we have the objects plus the respective albums to add e.g.:
         // - aTargets[0] = 'img12670205_s/img/_19800107-0123_drumm_';       aAlbums[0] = 'colors';
         // - aTargets[1] = 'img12670205_s/img/_19800107-012302_drummcut_';  aAlbums[1] = 'colors';
         // Note the dot/underline problem in the archive foldername.

         // Loop over the edited objects [seq 20110428°0407]
         for ($i = 0; $i < sizeof($aTargets); $i++ )
         {
            // [seq 20110428°0413]
            $sTarget = $aTargets[$i];
            $sAlbum = $aAlbums[$i];

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Problem with the foldernames with dots (issue 20110523°1441)
            // The HTML source says correctly: 'name="input_img12670205.s\img/_19800107-0123_drumm_"',
            // But the post mechanism delivers: 'name="input_img12670205_s\img/_19800107-0123_drumm_"'
            // The dot is exchanged with an underline.
            //
            // Out of $sTarget = 'img12670205_s\img/_19800107-0123_drumm_'
            // we have to recover $sTarget = 'img12670205.s\img/_19800107-0123_drumm_'
            //
            // I see no definitve way to do so. Best were an replacement character put into
            // the HTML source. That character should be which is allowed in HTML, but is
            // not allowed in filenames. Such character will perhaps not exist.
            // The problem will also be with filenames with dots. We have such files!
            //
            // For now, let's split the leading part of the path, and process this alone.

            // Workaround 20110523°0251 for issue 20110523°1441
            // - The archive folder must not contain underlines
            // - HELPS NOT YET FOR FILENAMES WITH DOTS!
            $a = preg_split('#[\\\\/]#', $sTarget );
            $s = str_replace ( '_' , '.' , $a[0] );
            $a[0] = $s;
            $sTarget = implode('/',$a);
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // [seq 20110428°0415]
            // Value e.g. sBud = 'img20110102/img/_20110104-1031_n1293_.txt'
            $bud = new Obyekt($sTarget . '.txt');
            if ($bud === false)
            {
               echo ('<p>Quirk 20110524°1922</p>');
            }
            else
            {
               // Get existing albums directly from the shadowbuddy [seq 20110428°0427]
               $aExistingAlbums = $bud->aShadowbuddy[Glb::GLR_BUDY_KEY_albums]; // 'albums'
               if (! $aExistingAlbums)
               {
                  //
               }

               // Remove the mandatory 'all' album [seq 20110428°0423]
               $iKi = array_search('all',$aExistingAlbums);
               if ($iKi !== false)
               {
                  // note: This removes the element and leaves a gap in the indices [seq 20110428°0425]
                  unset($aExistingAlbums[$iKi]);
               }
               // Re-index, means close the numeric gap caused by unset() above
               $aExistingAlbums = array_values($aExistingAlbums);
   
               // [seq 20110428°0427]
               if (isset($_POST[Glb::POSTKEY_GLR_submit_albums_add]))  // 'submit_albums_add'
               {
                  // Add the new album to the existing list [seq 20110428°0433]
                  $aExistingAlbums[] = $sAlbum;
                  $b = sort($aExistingAlbums);
               }
               else
               {
                  // Remove the album from the existing list [seq 20110428°0435]
                  $iKi = array_search($sAlbum, $aExistingAlbums);
                  if ($iKi !== false) {
                     unset($aExistingAlbums[$iKi]);                    // Remove album
                  }
                  $aExistingAlbums = array_values($aExistingAlbums);   // Re-index
               }

               // Write back the new list to the live buddyfile [seq 20110428°0437]
               $iKi = array_search(Glb::GLR_BUDY_KEY__album, $bud->aaBuddyfile[0]); // 'album'
               $sAlbLine = '';
               if ($iKi !== false)
               {
                  // [seq 20110428°0443]
                  for ( $i2 = 0; $i2 < sizeof($aExistingAlbums); $i2++ )
                  {
                     if ( $i2 > 0 )
                     {
                        $sAlbLine .= ', ';
                     }
                     $sAlbLine .= $aExistingAlbums[$i2];
                  }
                  $bud->aaBuddyfile[1][$iKi] = $sAlbLine;
                }
               else
               {
                  // [seq 20110428°0445]
                  // That buddyfile has no albums field yet, so let's create one
                  //  place it behind owner or author or as first
                  $iIns = array_search( Glb::GLR_BUDY_KEY_owner        // 'owner'
                                        , $bud->aaBuddyfile[0]
                                         );

                  // [seq 20110428°0447]
                  if ($iIns === false)
                  {
                     // [seq 20110428°0453]
                     $iIns = array_search( Glb::GLR_BUDY_KEY_author    // 'author'
                                          , $bud->aaBuddyfile[0]
                                           );
                     if ($iIns === false)
                     {
                        $iIns = 0;
                     }
                  }

                  // [seq 20110428°0455]
                  $a = $bud->aaBuddyfile[0];                           // Intermediate array for developing, can be eliminated if code is fine
                  $a2 = $bud->aaBuddyfile[1];                          // Intermediate array for developing, can be eliminated if code is fine
                  $a = FileFamilies::array_insert($a, Glb::GLR_BUDY_KEY__album, $iIns + 1);  // 'album'
                  $a2 = FileFamilies::array_insert($a2, $sAlbum, $iIns + 1);
                  $bud->aaBuddyfile[0] = $a;
                  $bud->aaBuddyfile[1] = $a2;
               }

               // Save the changes [seq 20110428°0457]
               $b = $bud->writebackBuddyfile();

               sleep(0);
            }

            sleep(0);
         }
      }
   }

   /**
    * This function provides an array with target folders to search for buddyfiles
    *
    * @id func 20110522°1321
    * @note See note 20110427°2235 'redundant dir listings'
    * @callers ..
    * @param $sParentFolder {string} The folder in which to search for buddyfiles
    * @return Array The wanted list of buddyfiles (now including their path relative to the archive base)
    */
   private static function get_buddyfiles1($sParentFolder)   // e.g. "X:\workspaces\daftaridev\trunk/daftari/docs/imgs26/"
   {

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // Find folderfamily from folderfamilyname [seq 20110522°1331]
      // todo : Outsource this sequence to a dedicated function.
      $aDirs = array();
      $s = Glb::$Glr_sBasePathImgFolders;
      $handle = opendir($s);
      if ($handle)
      {
         // [seq 20110428°0501]
         while (FALSE !== ($sFile = readdir($handle)))
         {
            if ($sFile == '.') { continue; }
            if ($sFile == '..') { continue; }
            $s = substr($sFile, 0, strlen($sParentFolder));
            if ($sParentFolder !== $s)
            {
               continue;
            }
            $aDirs[] = $sFile;
         }
         closedir($handle);
      }
      // Typical result :
      //  - $aDirs[0] = 'img12670203'
      //  - $aDirs[1] = 'img12670203.s'
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      // [seq 20110428°0503]
      $aBuddies = array();
      foreach ($aDirs as $sFoFamMember)
      {
         // [seq 20110428°0505]
         $sTargetfolder = Glb::$Glr_sBasePathImgFolders . "\\" . $sFoFamMember
                         . "\\" . Glb::GLR_sPATHPART_INTERCALARY . "\\"
                          ;                                            // e.g. $sTargetfolder = 'X:\workspaces\img12345678\img\'
   
         // [seq 20110428°0507]
         if (substr($sTargetfolder,-1) !== '\\')
         {
            $sTargetfolder .= '\\';
         }

         //----------------------------------------------------
         // about error 20110516°0752
         // It seems, that if on localhost, e.g. the virus scanner takes all
         // resources, PHP does is not sufficient with it's 30-seconds limit.
         // What to do in such situation?
         //----------------------------------------------------

         // [seq 20110428°0513]
         if (! is_dir($sTargetfolder) )
         {
            sleep(0);                                                  // e.g. 'X:\workspaces\arc\img\'
            continue;
         }

         // [seq 20110428°0515]
         $handle = opendir($sTargetfolder);
         if ($handle)
         {
            // [seq 20110428°0517]
            while (FALSE !== ($sFile = readdir($handle)))
            {
               // [seq 20110428°0523]
               if (substr($sFile,-4) !== '.txt')
               {
                  continue;
               }
               $s = $sFoFamMember . '/' . Glb::GLR_sPATHPART_INTERCALARY . '/' . $sFile;
               $aBuddies[] = $s;
            }
            closedir($handle);
         }
      }

      return $aBuddies;
   }

   /**
    * This function provides an array with target folders to search for buddyfiles
    *
    * @id func 20190203°0651 [[replacing 20110522°1321]]
    * @note See note 20110427°2235 'redundant dir listings'
    * @callers Only • method 20190202°1121 doPageAlbum
    * @param $sParentFolder {string} The folder in which to search for
    *     buddyfiles, e.g. "X:\workspaces\daftaridev\trunk/daftari/docs/img/img2/"
    * @return Array The wanted list of buddyfiles (now including their path relative to the archive base)
    */
   private static function get_buddyfiles2($sParentFolder)
   {
      // [seq 20110428°0531]
      $aBuddies = array();

      // paranoia [seq 20110428°0532]
      if (! is_dir($sParentFolder))
      {
         return $aBuddies;
      }

      // [seq 20110428°0533]
      $handle = opendir($sParentFolder);
      if ($handle)
      {
         // [seq 20110428°0535]
         while (FALSE !== ($sFile = readdir($handle)))
         {
            // [seq 20110428°0537]
            if ( substr($sFile, -4) !== '.txt' )
            {
               continue;
            }
            array_push($aBuddies, $sParentFolder . $sFile);
         }
         closedir($handle);
      }

      return $aBuddies;
   }

   /**
    * This function creates the array with the album objects.
    *
    * @id 20110502°2131
    * @note Functional similar to 20110427°2221 get_buddyfiles1() in PageRaw.php
    * @callers ..
    * @param $sAlbum {string} ..
    * @return Array The wanted album object array
    */
   private static function read_albumobjects($sAlbum)
   {
      // [seq 20110428°0543]
      $sAlbfile = Glb::GLR_sALBUMSFOLDER . $sAlbum . '.txt';           // 'dbtxt/albums/'
      $aObjs = @file($sAlbfile, FILE_IGNORE_NEW_LINES);                // Flag FILE_IGNORE_NEW_LINES not used
      if (! $aObjs)
      {
         // [seq 20110428°0545]
         $aObjs = array();                                             // Prevent later eventual type errors
      }

      // [seq 20110502°2133]
      // Fight issue 20110523°1441 'frankie style filenames'
      for ($i = 0; $i < count($aObjs); $i++)
      {
         // algo 20110527°2141 'read filenames with blanks'
         // note : This works only, because the filename is the last field to be read
         // from that record (which has 2 fields). Of course even more state of the art
         // were, to have the filename wrapped in quotes, so it need not be the last field.
         $a = explode(' ', $aObjs[$i]);
         if (count($a) > 2)
         {
            sleep(0); // breakpoint at filename with blank
         }
         if (count($a) < 2)
         {
            // Error - something is wrong
            echo ('<p>Quirk 20110527°2142<p>');
         }

         // [seq 20110428°0547]
         //$sTim = $a[0];                                              // Timestamp, not used here
         unset($a[0]);
         $s = implode(' ', $a);
         $aObjs[$i] = $s;
      }

      return $aObjs;
   }


   /**
    * This static field tells the album name. It is provided with a default value
    *
    * @id field 20110428°0234
    * @note Values are e.g. 'all', 'drawings', 'rawfolder.img12670204kp',
    *     'rawfolder.img20080301', 'rawfolder.img20090101', 'rawfolder.img20090408'
    * @var String
    */
   public static $sAlbum = 'drawings';

   /**
    * This static field collects the page navigation HTML fragment
    *
    * @id field 20110428°0235
    * @var String
    */
   public static $sOutNavi = '';

   /**
    * This static field collects the page HTML main payload fragment
    *
    * @id field 20110428°0236
    * @var String
    */
   public static $sOutMain = '';

}

/* eof */
