<?php

/**
 * This module ...
 *
 * file      : 20110502°1321
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 * note      : Class Filenameparse works fine, but is not used anymore
 *              since we read the image dimensions via getimagesize($s).
 * todo      : The thumbnail company links with the percent and maxdim
 *              flavours were nice. Possibly restore them by putting them
 *              in the shadowbuddyfiles filefamily members attributes.
 * ref       : E.g. http://www.php.net/manual/en/regexp.reference.meta.php [ref 20110516°0121]
 */

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

include_once (__DIR__ . '/../core/ErrorHandler.php');
set_error_handler("Trekta\\Daftari\\my_exceptons_error_handler");


/**
 * This class provides a filename's tokens.
 *
 * @id 20110502°1322
 * @description : filename styles e.g.
 *      (1) _20110428-1234_hollariaho_p99q66_.jpg
 *      (2) _20080628-0105_n004_.x0180y0135p85.jpg NOT YET PROCESSED
 * @note        : initial functionality was copied here from 20110428°2222
 * @callers buildShadowbuddyfile()
 */
class Filenameparse
{

   /**
    * This constructor instantiates an obyekt by it's buddyfile.
    *
    * @id 20110502°1323
    * @callers Only func 20110514°1921 buildShadowbuddyfile
    * @param $sFile {String} The plainfilename
    * @param $sAbsolutePath {String} This may be a remote http path
    * @param $sLocaldrivePath {String} Optional (if given, filesize is calculated additionally)
    */
   function __construct($sFile, $sAbsolutePath, $sLocaldrivePath = '') // $sAbsolutePath is a remote path, possibly provide a localdrive path as well
   {
      // Basic property
      $this->sFparFilename = $sFile;

      // Extract tokens from filename
      $this->parse_filename();

      // Extract values from flavour token
      $this->parse_flavour();                                 // BROKEN?!

      //Read data from live file
      $this->retrieve_dimensions($sAbsolutePath, $sLocaldrivePath);
   }

   /**
   * This function parses the filename into it's tokens (by a pretty simple regex algorithm).
   *
   * @id : 20110502°1324 (after 20110515°2121)
   * @note From $this->sFilename calculate
   *        - $this->sTimestamp
   *        - $this->sFnamCorename
   *        - $this->sFnamFlavour
   *        - $this->sFnamExtension
   *        - $this->sAdditional
   * @callers
   * @return {Boolean} Success flag
   */
   private function parse_filename()
   {
      // Styles e.g.
      // - Underline delimiter style
      // - Underline/period delimiter style
      // - Octothorp style

      // Variables to be retrieved
      $sTimestamp = '';
      $sFnamCorename = '';
      $sFnamFlavour = '';
      $sFnamExtension = '';
      $sAdditional = '';

      //-----------------------
      // Detect fankie-style filenames [sequence 20110528°0021]
      // e.g.:
      // - '#20090424.1111 track01.p99.png'
      // - '#20090424.1111 track01 .p99.png'
      $sFilnam = $this->sFparFilename;                                 // comfort
      $i = preg_match('/#\d+\.\d+ .+/', $sFilnam);                     // frankie-style with blank behind timestamp token
      if ($i > 0)
      {
         // Process frankie-style filename
         $a = explode(' ',$sFilnam);
         $sTimestamp = substr($a[0],1);                                // extract timestamp token without the octothorp
         unset($a[0]);
         $s = implode(' ',$a);                                         // re-assemble the rest
         $a = explode('.',$s);                                         // disassemlbe at dots
         $sFnamExtension = $a[count($a)-1];                            // extract filenameextension
         unset($a[count($a)-1]);
         if (sizeof($a) > 1)
         {
            $sFnamFlavour = $a[count($a)-1];                           // extract flavour
            unset($a[count($a)-1]);
         }
         $sFnamCorename = implode('.',$a);
         $sFnamCorename = trim($sFnamCorename);                        // there might be that additional blank at the end
      }
      else
      {
         //-----------------------
         // Process common filename styles

         // Split
         $a = preg_split('/[#_.]/', $this->sFparFilename );
         $iSize = sizeof($a);
         for ($i = 0; $i < $iSize ; $i++)
         {
            if ( $a[$i] === '' ) { unset($a[$i]); }
         }
         $a = array_values($a);

         // Categorize after array size
         switch ( sizeof($a) )
         {
            case 0 :
               break;
            case 1 :
               $sFnamCorename = $a[0];
               break;
            case 2 :
               $sFnamCorename = $a[0];
               $sFnamExtension = $a[1];
               break;
            case 3 :
               $sTimestamp = $a[0];
               $sFnamCorename = $a[1];
               $sFnamExtension = $a[2];
               break;
            case 4 :
               $sTimestamp = $a[0];
               $sFnamCorename = $a[1];
               $sFnamFlavour = $a[2];
               $sFnamExtension = $a[3];
               break;
            default : // size > 4
               $sTimestamp = $a[0];
               $sFnamCorename = $a[1];
               $sFnamFlavour = $a[2];
               $sFnamExtension = $a[(sizeof($a) - 1)];                 // Just the last token
               // Collect the rest into one string (from fourth to last but one token)
               // CHECK - supplement a delimiter ..
               for ($i = 3; $i < (sizeof($a) - 1); $i++)
               {
                  $sAdditional .= $a[$i];
               }
               break;
         }
      }

      // Validate timestamp [seq ..]
      // todo: Implement it.
      // ...

      // Mangle timestamp
      $a = preg_split('/[-\'\.]/', $sTimestamp );                      // Split at '-', "'" or '.'
      if (sizeof($a) == 2)
      {
         $sTimestamp = $a[0] . '.' . $a[1];
      }
      else
      {
         // Invalid timestamp
         // .. leave it as is .. so it will produce error elsewhere ..
         // If validation above were implemented, the problem here will be gone.
      }

      // Assign the found tokens
      $this->sFnamTimestamp = $sTimestamp;
      $this->sFnamCorename = $sFnamCorename;
      $this->sFnamFlavour = $sFnamFlavour;
      $this->sFnamExtension = $sFnamExtension;
      $this->sAdditional = $sAdditional;                               // Check — Not yet propagated to the end users ...?

      $sFnamFlavour = $this->sFnamFlavour;                             // Hm ..

      return TRUE;
   }


   /**
   * This function extracts details from the filename flavour token and
   *  assigns them to the respective properties (hence no return value)
   *
   * @id 20110502°1325
   * @note From $this->sFnamFlavour calculate
   *        - $this->iPercent
   *        - $this->iWidth
   *        - $this->iHeight
   *        - $this->iQuality
   *        - $this->iMaxdim
   *        - $this->iVersion
   * @callers
   * @note Flavours are e.g. 'p33q40', 'x0180y0120q33', 'v0'
   */
   private function parse_flavour()
   {
      // Input parameter
      $sFnamFlavour = $this->sFnamFlavour;

      // Output values
      $this->iFlavWidth = 0;
      $this->iFlavHeight = 0;
      $this->iFlavPercent = 0;
      $this->iFlavMaxdim = 0;
      $this->iFlavQuality = 0;
      $this->iVersion = (int) null; // experimental

      // Work values
      $sPercent = "";
      $sWidth = "";
      $sHeight = "";
      $sQuality = "";
      $sVersion = "";

      // Process flavour
      $sReading = "";
      for ($i=0; $i < strlen($sFnamFlavour); $i++)
      {
         $s = substr($sFnamFlavour,$i,1);
         switch ($s)
         {
            case "p" : $sReading = "p"; break;
            case "q" : $sReading = "q"; break;
            case "x" : $sReading = "x"; break;
            case "y" : $sReading = "y"; break;
            case "v" : $sReading = "v"; break;
            default:
               if (is_numeric($s))
               {
                  switch ($sReading)
                  {
                     case "p" : $sPercent .= $s; break;
                     case "q" : $sQuality .= $s; break;
                     case "x" : $sWidth .= $s; break;
                     case "y" : $sHeight .= $s; break;
                     case "v" : $sVersion .= $s; break;
                     default :
                  }
               }
         }
      }

      // Write findings to output
      $this->iFlavPercent = (int) $sPercent;
      $this->iFlavWidth = (int) $sWidth;
      $this->iFlavHeight = (int) $sHeight;
      $this->iFlavQuality = (int) $sQuality;
      $this->iFlavMaxdim = ($this->iFlavWidth > $this->iFlavHeight) ? $this->iFlavWidth : $this->iFlavHeight;
      $this->iVersion = (int) $sVersion;

      return;
   }

   /**
    * This function retrieves dimensions from the live image file
    *
    * @id 20110514°0321
    * @note This method needs not the filename as a string, but needs
    *        accessing the file in read mode. So this is the place for
    *        possibly adding more file analytics (as done with filesize).
    * @note There exists another parameter for getimagesize() to retrieve even more information
    * @todo Returning nothing — The result is written directly into to
    *     class properties. — BETTER RETURN bool false = error, true = success
    * @callers Only this class constructor
    * @param {String} $sArchivefolder e.g. "X:\workspaces\daftaridev\trunk/daftari/docs/imgs26/19730815o1234.schwan.v1.p12q33.jpg"
    * @param {String} $sLocaldrivePath
    * @return {Boolean} Just proforma
    */
   function retrieve_dimensions ( $sArchivefolder
                                 , $sLocaldrivePath = ''
                                  )
   {

      /*
      $sFile = $sLocaldrivePath . '/' . $sArchivefolder
              . '/' . Glb::GLR_sPATHPART_INTERCALARY
               . '/' . $this->sFilename
                ;
      */
      $sFile = $sArchivefolder; // [mark 20190203°0711 chg]

      $sErrorstring = '';

      // () ..  [seq 20110514°0331]
      // note : Remember issue 20110518°1941 'getimagesize locally and remote'
      $this->iFparFilesize = -1;                                       // -1 = marker for 'n/a'
      if ($sLocaldrivePath !== '')
      {
         try
         {
            $this->iFparFilesize = filesize($sFile);
         }
         catch ( \ErrorException $e )
         {
            echo ('<p>Issue 20110518°1941</p>');
            return false;                                              // added 20110526°1102
         }
      }

      // () Restrict use of getimagesize for files with minimum size [seq 20110514°0333]
      if ( $this->iFparFilesize > Glb::GLR_iGETIMAGESIZE_MAX_SIZE )
      {
            $a = getimagesizeDummyResult();
      }
      else
      {

         // () .. [seq 20110514°0335]
         // note : Remember issue 20110516°2121 'get imagesize zero bytes'
         try
         {
            $a = getimagesize($sFile);

            // Append the workaround field [line 20190204°0758]
            $a[Glb::GLR_SHDW_KEY_url] = '';
         }
         catch ( \ErrorException $e )
         {
            // getimagesize() failed ungracefully e.g. if it hit a zero-byte-file
            // Provide dummy array the caller can continue to work with

            $s1 = $e->getMessage();
            $i2 = $e->getCode();
            $s3 = $e->getFile();
            $i4 = $e->getLine();
            $i5 = $e->getSeverity();
            $sErrorstring = 'Error ' . $i2 . ' "' . $s1 . '" in ' . $s3 . ' at line ' . $i4 . ' severity ' . $i5;

            $a = getimagesizeDummyResult();
         }

         // () [seq 20110514°0337]
         if ($a === FALSE)
         {
            // getimagesize() failed gracefully
            $a = getimagesizeDummyResult();

            // Error message building not yet activated
            if (Glb::bToggle_FALSE)
            {
               // note 20110514°0341 :
               //    Do not do this without further ado. Otherwise any innocent
               //    txt files get's an error as well. If we want this message
               //    have active, we must before care about only having files
               //    here which ARE imagefiles, and should have sizes.
               $sErrorstring = 'Error "getimagesize(' . $sFile . ')  failed.';
            }

         }
         // Now $a is either FALSE or an array like
         //   • $a[0] = (integer) 911; $a[1] = (integer) 682; $a[2] = (integer) 2;
         //   • $a[3] = (string) 'width="911" height="682"'; $a['bits'] = (integer) 8;
         //   • $a['channels'] = (integer) 3; $a['mime'] = (string) 'image/jpg';

      }
      // Now $a is either a dummy result or a real result

      // Bad workaround [seq 20190204°0759]
      //  Prevent below exception 'array key does not exist'
      if (! array_key_exists(0, $a)) { $a[0] = 0; }
      if (! array_key_exists(1, $a)) { $a[1] = 0; }
      if (! array_key_exists(2, $a)) { $a[2] = 0; }
      if (! array_key_exists(3, $a)) { $a[3] = 0; }
      if (! array_key_exists(Glb::GLR_SHDW_KEY_mime, $a)) { $a[Glb::GLR_SHDW_KEY_mime] = '?'; }
      if (! array_key_exists(Glb::GLR_SHDW_KEY_url, $a)) { $a[Glb::GLR_SHDW_KEY_url] = '?'; }
      
      // todo : Eliminate this 3 variables [seq 20110526°1111]
      $this->iLiveWidthReal = $a[0];
      $this->iLiveHeightReal = $a[1];
      $this->iLiveMaxdimReal = ($this->iLiveWidthReal > $this->iLiveHeightReal)
                              ? $this->iLiveWidthReal
                               : $this->iLiveHeightReal
                                ;

      // This shall replace above 'real' values [seq 20110526°1121]
      if (($this->iFlavWidth < 1) and ($a[0] > 0))
      {
         $this->iFlavWidth = $a[0];
      }
      if (($this->iFlavHeight < 1) and ($a[1] > 0))
      {
         $this->iFlavHeight = $a[1];
      }
      $this->iFlavMaxdim = ($this->iFlavWidth > $this->iFlavHeight) ? $this->iFlavWidth : $this->iFlavHeight;

      $this->sFparError = $sErrorstring;

      $this->iGrphGraphicstype = $a[2];
      $this->sGrphHeightWidthHtml = $a[3]; // e.g. ' width="40" height="40"'
      $this->iGrphBits = (isset($a[Glb::GLR_SHDW_KEY_bits])) ? isset($a[Glb::GLR_SHDW_KEY_bits]) : -1 ; // Issue 'Undefined index: bits' [issue 20110518°2042] CHECK  which kind of image file leaves 'bits' undefined
      $this->iGrphChannels = (isset($a[Glb::GLR_SHDW_KEY_channels])) ? isset($a[Glb::GLR_SHDW_KEY_channels]) : -1 ; // may be 'Undefined index' .. image with transparency e.g. 'img20110102/img/_20110323-123202_handytwitter1_v0_.png'
      $this->sGrphMimetype = $a[Glb::GLR_SHDW_KEY_mime];

      $this->sTargetUrl = $a[Glb::GLR_SHDW_KEY_url]; // [line 20190204°0756]

      // [added 20110526°1102]
      return TRUE;
   }

   /**
    * This field holds the files height deduced from the flavour token
    *
    * @id 20110502°1425
    * @var String
    */
   public $iFlavHeight;

   /**
    * This field holds the files max-dimension deduced from the flavour token
    *
    * @id 20110502°1431
    * @var String
    */
   public $iFlavMaxdim;

   /**
    * This field holds the files percent deduced from the flavour token
    *
    * @id 20110502°1427
    * @var String
    */
   public $iFlavPercent;

   /**
    * This field holds the files quality deduced from the flavour token
    *
    * @id 20110502°1433
    * @var String
    */
   public $iFlavQuality;

   /**
    * This field holds the files width deduced from the flavour token
    *
    * @id 20110502°1423
    * @var String
    */
   public $iFlavWidth;

   /**
    * This field tells the file size for the file
    *
    * @id 20110502°1455
    * @note Only retrievable with localdrive path
    * @var String
    */
   public $iFparFilesize;

   /**
    * This field tells the bit depth of the respective image
    *
    * @id 20110502°1447
    * @var String
    */
   public $iGrphBits;

   /**
    * This field tells the number of channels for the respective image
    *
    * @id 20110502°1451
    * @var String
    */
   public $iGrphChannels;

   /**
    * This field tells the graphics type of the respective file
    *
    * @id 20110516°2211
    * @var String
    */
   public $iGrphGraphicstype;

   /**
    * This field holds the real height of the image, obtained from the live file
    *
    * @todo ELIMINATE [todo 20110526°1111]
    * @id 20110502°1437
    * @var String
    */
   public $iLiveHeightReal;

   /**
    * This field holds the real max-dimension of the image, obtained from the live file
    *
    * @id 20110502°1441
    * @todo ELIMINATE [todo 20110526°1112]
    * @var String
    */
   public $iLiveMaxdimReal;

   /**
    * This field holds the real width of the image, obtained from the live file
    *
    * @id 20110502°1435
    * @todo ELIMINATE [todo 20110526°1113]
    * @var String
    */
   public $iLiveWidthReal;

   /**
    * This field ...
    *
    * @id 20190312°0133
    * @note Created after complain PhanUndeclaredProperty
    * @var String
    */
   public $iVersion;

   /**
    * This field ...
    *
    * @id 20190312°0131
    * @note Created after complain PhanUndeclaredProperty
    * @var String
    */
   public $sAdditional;

   /**
    * This field holds the core filename-part from basic filename tokenizing
    *
    * @id 20110502°1415
    * @var String
    */
   public $sFnamCorename;

   /**
    * This field holds the extension filename-part from basic filename tokenizing
    *
    * @id 20110502°1421
    * @var String
    */
   public $sFnamExtension;

   /**
    * This field holds the flavour filename-part from basic filename tokenizing
    *
    * @id 20110502°1417
    * @var String
    */
   public $sFnamFlavour;

   /**
    * This field holds the timestamp filename-part from basic filename tokenizing
    *
    * @id 20110502°1413
    * @var String
    */
   public $sFnamTimestamp;

   /**
    * This field tells any error message for the parsed filename
    *
    * @id 20110516°2211
    * @var String
    */
   public $sFparError;

   /**
    * This field stores the filename to be parsed
    *
    * @id 20110502°1411
    * @var String
    */
   public $sFparFilename;

   /**
    * This field tells holds the HTML fragment for the width/height attributes
    *
    * @id 20110502°1443
    * @var String
    */
   public $sGrphHeightWidthHtml;

   /**
    * This field tells the mime-type for the file
    *
    * @id 20110502°1453
    * @var String
    */
   public $sGrphMimetype;

   /**
    * This field holds the URL of the target file in the filefamilies files list
    *
    * @id 20190204°0755
    * @var String
    */
   public $sTargetUrl;

}

/**
 * This is a helper function to build a dummy array like getimagesize would deliver it
 *
 * @id : 20110518°2011
 * @callers retrieve_dimensions() three times
 * @return Array The wanted getimagesize() dummy array
 */
function getimagesizeDummyResult()
{
   $a = array();

   $a[0]        = 0;                                                   // (integer) width
   $a[1]        = 0;                                                   // (integer) height
   $a[2]        = 0;                                                   // (integer) graphics type, one of the IMAGETYPE_XXX constants
   $a[3]        = '';                                                  // (string) 'height="yyy" width="xxx"'
   $a[Glb::GLR_SHDW_KEY_bits] = 0;                                     // 'bits' (integer)
   $a[Glb::GLR_SHDW_KEY_channels] = 0;                                 // 'channels' (integer)
   $a[Glb::GLR_SHDW_KEY_mime] = '';                                    // (string)

   $a[Glb::GLR_SHDW_KEY_url] = '';                                     // (string)

   return $a;
}

/* eof */
