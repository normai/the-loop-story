<?php

/**
 * This module tries to provide file access via WebDAV (with no success so far)
 *
 * file      : 20110503°0121
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    : Abandoned in favor of phpsvnclient.r63, or just sleeping.
 * note 20110503°1021 : Could not get it run. Perhaps it is worth a second try.
 * encoding  : UTF-8-without-BOM
 * note      :
 */

namespace Trekta\Daftari;

/**
 * This class provide file access via WebDAV, namely directory listing.
 *
 * @id 20110503°0122
 * @note Based on sample code 20110428°2135 by Michael Stöckel
 *        from http://www.html-world.de/program/phpex_13.php
 * @note See note 20110428°2151 'get a directory listing from url'
 * @callers so far only from class.obyekt.php
 */
class Webdav
{

   /**
    * This constructor provides file access via WebDAV, namely for
    *    directory listings (internal connect function of the class)
    *
    * @id 20110503°0123
    * @todo 20161008°0842 : Refactor returns away, use exceptions to indicate errors.
    * @callers • None
    * @param Array|Null $param Associative array with params like 'content'
    */
   function __connect( $param = array() )
   {
      $fp = fsockopen($this->server, 80, $errno, $errstr, 5);
      if (! $fp)
      {
         // note : Constructor must not have a return, see todo 20161008°0842
         return "$errno -> $errstr<br>";
      }
      else
      {
         fwrite($fp, $param['content']);
         $output_array = array();
         while(! feof($fp))
         {
            array_push($output_array,fgets($fp));
         }
         fclose($fp);

         // note : Constructor must not have a return, see todo 20161008°0842
         return $output_array;
      }
   }

   /**
    * This method tests a WebDAV connection
    *
    * @id 20110503°0124
    * @callers • None?
    * @param Array $param Parameter proforma, not used, just to look like the other methods
    * @return Boolean|String Success flag proforma
    */
   public function check_webdav( $param = array() )
   {
      $content = "HEAD / HTTP/1.1 \r\n";
      $content .= "Host: $this->server \r\n";
      $content .= "Connection: Close\r\n";
      $content .= "\r\n";
      $output = $this->__connect( array('content'=>$content) );

      foreach($output as $line)
      {
         if ( preg_match("/Server:/",$line) )
         {
            if ( !preg_match("/DAV*/",$line) )
            {
               return "1";
            }
         }
      }
      return TRUE;
   }

   /**
    * This method creates a new WebDAV folder.
    *
    * @id 20110503°0128
    * @callers ..
    * @param Array $param Some parameters like 'user', 'folder'
    * @return string Some message HTML fragment
    */
   public function create_new_folder( $param = array() )
   {
      $user = $param['user'];
      $folder = $param['folder'];

      $content = "MKCOL /files/$user/$folder HTTP/1.1 \r\n";
      $content .= "Host: $this->server \r\n";
      $content .= "Connection: Close\r\n";
      $content .= "\r\n";

      $output = $this->__connect( array('content'=>$content) );
      if (preg_match("/201/",$output[0]))
      {
         $content = "<script type=text/javascript>
         alert('Ordner $folder wurde angelegt');
         document.location.href=\"index.php\";
         </script>";
      }
      else
      {
         $content = "<script type=text/javascript>
         alert('Der Ordner $folder konnte nicht angelegt werden.');
         document.location.href=\"index.php\";
         </script>";
      }
      return $content;
   }

   /**
    * This method deletes a WebDAV file
    *
    * @id 20110503°0127
    * @note [form action=\"?method=delete&file=$folder\" method=post]
    *         [input type=submit value=\"Datei löschen\"]
    *          [/form]
    * @callers • None
    * @param Array $param some parameters like 'query_string'
    * @return string Some message HTML fragment
    */
   public function delete_file( $param = array() )
   {
      $query_string = $param['query_string'];
      $query_param = split("&", $query_string);

      $filename = split("=",$query_param[1]);
      $file_tmp = str_replace("%3CD:href%3E", "", $filename[1]);
      $file = str_replace("%3C/D:href%3E", "", $file_tmp);

      $file_param = pathinfo($file);
      $path = $file_param['dirname'] . "/";
      $file = $file_param['basename'];

      $content = "DELETE $path/$file HTTP/1.1 \r\n";
      $content .= "Host: $this->server \r\n";
      $content .= "Connection: Close\r\n";
      $content .= "Content-length:0\r\n";
      $content .= "Destroy:NoUndelete\r\n";
      $content .= "\r\n";

      $output = $this->__connect( array('content'=>$content) );

      if (preg_match("/204/",$output[0]))
      {
         $content = "<script type=text/javascript>
         alert('Datei $file wurde gelöscht');
         document.location.href=\"index.php\";
         </script>";
      }
      else
      {
         $content = "<script type=text/javascript>
         alert('Datei $file konnte nicht gelöscht werden');
         document.location.href=\"index.php\";
      </script>";
      }

      return $content;
   }

   /**
    * This method uploads a file into the WebDAV folder.
    *
    * @id 20110503°0126
    * @callers • None
    * @param Array $param Key/value pairs with some arguments e.g. 'user'
    * @return String Some message HTML fragment
    */
   public function file_upload( $param = array() )
   {
      $user = $param['user'];
      $tmp_filename = $param['tmpfile'];
      $filename = $param['file'];

      if ( copy($tmp_filename, "D:/Webdav/" . $user ."/" . $filename) )
      {
         $content = "<script type=text/javascript>
         alert(\"Datei $filename wurde hochgeladen\");
         document.location.href=\"index.php\";
         </script>";
      }
      else
      {
         $content = "<script type=text/javascript>
         alert(\"Datei $filename konnte nicht hochgeladen werden\");
         document.location.href=\"index.php\";
         </script>";
      }

      return $content;
   }

   /**
    * This method shows the content of the WebDAV drive or folder
    *
    * @id 20110503°0125
    * @callers • None so far
    * @param Array $param Optional parameters e.g. 'user'
    */
   public function show_content( $param = array() )
   {
      $user = $param['user'];

      $sTarget = "/workspaces/img12345678/";                           // todo : Eliminate hardcoded target

      // //$content = "PROPFIND /files/$user/ HTTP/1.1 \r\n";          // Original line
      $content = "PROPFIND " . $sTarget . " HTTP/1.1 \r\n";            // [line 20110503°0213 new]
      $content .= "Host: $this->server \r\n";
      $content .= "Depth: 1\r\n";
      $content .= "Content-Type: text/xml\r\n";
      $content .= "Connection:close\r\n";
      $content .= "Content-Length: 0\r\n";
      $content .= "\r\n";
      $content .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";
      $content .= "<D:propfind xmlns:D=\"DAV:\">\r\n";
      $content .= "<D:allprop/>\r\n";
      $content .= "</D:propfind>\r\n";                                 // Is the slash placed right? [line 20110503°0212]

      $output = $this->__connect( array('content'=>$content) );
      array_pop($output);
      $key = array_search("\r\n",$output);
      $output = array_slice($output,($key+1));
   }

   //**********************************************************
   // Zugriff auf die PHP-Klasse in einem Skript
   //**********************************************************
   // id : 20110503°0129
   // note :

   //$conn_check = $webdav->check_webdav(array("server"=>"localhost"));
   // //if ( $conn_check == "1" )
   // //{
   // //   print("Der angegebene Server ist nicht webdav-kompatibel");
   // //}
   // //else
   // //{
   // //   //... Anzeige des WebDav-Formulares ...
   // //}
   // //$content = $webdav->show_content( array("user"=>"test1") );


   //**********************************************************
   // Aufruf der Funktionen in der PHP-Klasse
   //**********************************************************
   // version : 20110503°0130
   // note :

   // // //----------------------------------------------------------
   // //print("
   // //    <h1>Anlegen eines neuen Ordners</h1>
   // //    <form action=\"index.php?method=folder\" method=\"post\">
   // //    <input type=\"text\" name=\"ordner\">
   // //    <input type=\"submit\" value=\"Ordner anlegen\">
   // //    </form>
   // //");
   // // //----------------------------------------------------------

   // // //----------------------------------------------------------
   // //if (preg_match("/folder/",getenv('QUERY_STRING')))
   // //{
   // //   $folder = $_POST['ordner'];
   // //   $folder_content = $webdav->create_new_folder(
   // //                array('user' => $user, 'folder' => $folder));
   // //   print_r($folder_content);
   // //}
   // // //----------------------------------------------------------

   // // //----------------------------------------------------------
   // //if (preg_match("/delete/",getenv('QUERY_STRING')))
   // //{
   // //   $delete_content = $webdav->delete_file(
   // //                   array('query_string' => getenv('QUERY_STRING')));
   // //   print("$delete_content");
   // //}
   // // //----------------------------------------------------------

   // // //----------------------------------------------------------
   // //if (preg_match("/upload/",getenv('QUERY_STRING')))
   // //{
   // //   $upload = $webdav->file_upload(
   // //           array("user"=>$user, "tmpfile"=>$_FILES['file']['tmp_name'],
   // //                 "file" => $_FILES['file']['name']));
   // //   print("$upload");
   // //}
   // // //----------------------------------------------------------

   /**
    * This field stores .. e.g.
    *
    * @id 20110503°0211
    * @note Compare http://de2.php.net/manual/de/function.fsockopen.php [ref 20110503°0212]
    * @note Values e.g.:
    *     • 'localhost'
    *     • 'localhost/svn' — fsockopen() fails with "no such host is known"
    *     • 'user:pw@www.gallery.lum/svn/' — fsockopen() fails with "php_network_getaddresses: getaddrinfo failed: No such host is known."
    * @var String
    */
   public $server = 'localhost/svn';

}

/* eof */
