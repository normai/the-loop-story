<?php

/**
 * This module generates the payload HTML of one Object page
 *
 * file      : 20110502°2121
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 * note      :
 * callers   :
 */

namespace Trekta\Daftari;

use Trekta\Daftari as TD;
use Trekta\Daftari\Globals as Glb;

include_once(__DIR__ . '/../galari/FileFamilies.php');
include_once(__DIR__ . '/../galari/Obyekt.php');

/**
 * This class generates the Object page HTML payload ..
 *
 * @id class 20190202°1211
 */
class PageObject
{

   /**
    * This method works off the former script level code ...
    *
    * @id method 20190202°1221
    */
   public static function doPageObject()
   {

      //Prologue [seq 20110502°2142]
      $sCR = Glb::$sTkNL;

      // () Get the target object [seq 20110502°2122]
      $sObject = ( array_key_exists(Glb::GET_KEY_GLR_object, $_GET) )
                ? $_GET[Glb::GET_KEY_GLR_object]
                 : 'N/A';
                  ;
      self::$sPageTitleObjectPart = $sObject;

      // Does a shadowfile exist [seq 20190204°0721]
      $buddy = self::seekShadowFile($sObject);

      // Paranoia [seq 20190204°0723]
      if ($buddy === FALSE )
      {
         $sOut = '<p>Object not found : "<b>'
                . $sObject
                 . '</b>"</p>'
                  ;
         self::$sOutMain = $sOut;
         return;
      }

      // Convenience [line 20110502°2144]
      $aShadow = $buddy->aShadowbuddy;

      // Paranoia [line 20110502°2145]
      //+ + + + + + + + + + + + + + +
      // issue 20110516°0219 solved with sequence 20110516°0221 in class.obyekt.php
      // if the object page is called e.g. with psychobox when it's shadowbuddyfile
      // does not yet exist, then we get an empty $buddy->aShadowbuddy here
      // on the first request, and on the second request, it will be filled.
      // Let's try a wicked workaround:
      if ( sizeof($aShadow) < 1 )
      {
         // The shadowbuddyfile already exists in this moment, only it is not read yet.
         sleep(0);
      }
      //+ + + + + + + + + + + + + + +

      // () Start building the HTML fragment [seq 20110502°2146]
      $sOut = '';
      $sOut .= $sCR . '<table border="0">';
      $sOut .= $sCR . ' <tr>';
      $sOut .= $sCR . '  <td colspan="3" style="text-align:center;">';

      //- - - - - - - - - - - - - - -
      // [seq 20110502°2123]
      // Sequences nearly identical in PageAlbum.php and PageObject.php
      $iSizeWanted = $_SESSION[Glb::SESKEY_iObjectImagesize];
      $a = $buddy->getThumb($iSizeWanted);
      $iSizeGiven = $a[Glb::GLR_SHDW_KEY_maxdim];                      // 'maxdim'
      $nFactor = 0.9; // just for fun
      if ($iSizeGiven > 0)
      {
         $nFactor = $iSizeWanted / $iSizeGiven;
      }
      $sImagefile = $a[Glb::GLR_SHDW_KEY_file];                        // "G:.G/trunk/img/G:\work\downtown\daftaridev\trunk/daftari/docs/imgs26/19730815o1234.schwan.v1.p12q33.jpg"

      // Determine shown image size [seq 20110502°2124]
      $iWidth = (int) ($nFactor * $a[Glb::GLR_SHDW_KEY_width]);
      $iHeight = (int) ($nFactor * $a[Glb::GLR_SHDW_KEY_height]);
      //- - - - - - - - - - - - - - -

      // Determine main file to link [seq 20110502°2125]
      // note 20190205°0055 : Does not work as expected? Can also be skipped?
      $a = $buddy->getThumb(0); // zero means biggest percent-flavour

      // Build HTML fragment [seq 20110502°2126]
      if ($a[Glb::GLR_SHDW_KEY_file] !== '' )
      {
         $sOut .= $sCR . '   <a href="' . $a[Glb::GLR_SHDW_KEY_file] . '">';
      }
      $sOut .= $sCR;
      $sOut .= '    <img src="' . TD\FileFamilies::mkUrlEncode($sImagefile) . '"';
      $sOut .= ' width="' . $iWidth . '"';
      $sOut .= ' height="' . $iHeight . '"';
      $sOut .= ' alt=' . utf8_encode($buddy->sFilefamilyname) . '>';
      if ($a[Glb::GLR_SHDW_KEY_file] !== '' )
      {
         $sOut .= $sCR . '   </a>';
      }

      // Debug [seq 20110930°0121] just make module run without exception
      if (! isset($buddy->aShadowbuddy[Glb::GLR_BUDY_KEY_title]))
      {
         $buddy->aShadowbuddy[Glb::GLR_BUDY_KEY_title] = '';
      }
      if (! isset($buddy->aShadowbuddy[Glb::GLR_BUDY_KEY_timestamp]))
      {
         $buddy->aShadowbuddy[Glb::GLR_BUDY_KEY_timestamp] = '';
      }

      // .. [line 20110502°2147]
      $sOut .= $sCR . '   <br>' . utf8_encode($buddy->aShadowbuddy[Glb::GLR_BUDY_KEY_title]);
      $sOut .= $sCR . '   <br><small>&#35; '
                     . utf8_encode($buddy->aShadowbuddy[glb::GLR_BUDY_KEY_timestamp])
                      . '</small>'
                       ;
      $sOut .= $sCR . '  </td>';
      $sOut .= $sCR . ' </tr>';

      // () Shift filefamily to end of array [seq 20110502°2127]
      $a = $aShadow[Glb::GLR_BUDY_KEY_filefamily];
      unset( $aShadow[Glb::GLR_BUDY_KEY_filefamily]);
      $aShadow[Glb::GLR_BUDY_KEY_filefamily] = $a;

      // () [seq 20110502°2148]
      foreach ($aShadow as $sKi => $val)
      {
         $sVal = '';
         if ($sKi == Glb::GLR_BUDY_KEY_albums)
         {
            // () [seq 20110502°2151]
            $sVal = '';
            foreach ($aShadow[Glb::GLR_BUDY_KEY_albums] as $s)
            {
               if ($sVal !== '')
               {
                  $sVal .= ', ';
               }
               $sVal .= $s;
            }
         }
         else if ($sKi == Glb::GLR_BUDY_KEY_filefamily)
         {
            // () [seq 20110502°2152]
            // See note 20110516°0433 'draft color set'
            // note : style="table-layout: fixed" is a try to make max-width work in td
            $sVal = '<table align="left" border="0" style="table-layout:fixed;">';
            $sVal .= $sCR . ' <tr>' . $sCR;
            //$sVal .= $sCR . '<td style="font-size:77%;color:black;">'   . Glb::GLR_SHDW_KEY_pext   . '</td>';
            $sVal .= $sCR . '<td style="font-size:77%; max-width:4.7em;">' . Glb::GLR_SHDW_KEY_files  . '</td>';
            $sVal .= $sCR . '<td style="font-size:77%;color:blue;">'    . Glb::GLR_SHDW_KEY_width  . '</td>';
            $sVal .= $sCR . '<td style="font-size:77%;color:green;">'   . Glb::GLR_SHDW_KEY_height . '</td>';
            $sVal .= $sCR . '<td style="font-size:77%;color:red;">'     . Glb::GLR_SHDW_KEY_maxdim . '</td>';
            $sVal .= $sCR . '<td style="font-size:77%;color:lime;">'    . Glb::GLR_SHDW_KEY_ver    . '</td>';  // Version
            $sVal .= $sCR . '<td style="font-size:77%;color:gray;">'    . Glb::GLR_SHDW_KEY_fpc    . '</td>';  // Percent
            $sVal .= $sCR . '<td style="font-size:77%;color:fuchsia;">' . Glb::GLR_SHDW_KEY_fpq    . '</td>';  // Quality
            $sVal .= $sCR . '<td style="font-size:66%;color:green;">'   . Glb::GLR_SHDW_KEY_flav   . '</td>';  // FlavourString
            $sVal .= $sCR . '<td style="font-size:77%;color:blue;">'    . Glb::GLR_SHDW_KEY_addi   . '</td>';  // AdditionalFilenameComponent
            $sVal .= $sCR . '<td style="font-size:77%;color:teal;">'    . Glb::GLR_SHDW_KEY_ext    . '</td>';  // FilenameExtension
            $sVal .= $sCR . '<td style="font-size:77%;color:blue;">'    . Glb::GLR_SHDW_KEY_size   . '</td>';  // Filesize
            $sVal .= $sCR . '<td style="font-size:77%;color:green;">'   . Glb::GLR_SHDW_KEY_gtyp   . '</td>';  // Graphicstype
            $sVal .= $sCR . '<td style="font-size:77%;color:blue;">'    . Glb::GLR_SHDW_KEY_bits   . '</td>';  // Bits
            $sVal .= $sCR . '<td style="font-size:77%;color:green;">'   . Glb::GLR_SHDW_KEY_chan   . '</td>';  // Channels
            $sVal .= $sCR . '<td style="font-size:77%;color:lime;">'    . Glb::GLR_SHDW_KEY_mime   . '</td>';  // Mimetype
            $sVal .= $sCR . ' </tr>';

            // [seq 20110502°2128]
            $a = $aShadow[Glb::GLR_BUDY_KEY_filefamily];
            $iArSiz = ($a) ? sizeof($a[Glb::GLR_SHDW_KEY_files]) : 0;  // Paranoia 20190202°0853
            for ($i = 0; $i < $iArSiz; $i++)                           // 'files'
            {
               // Retrieve current file pathes [line 20190204°0743]
               $sUrl = $a[Glb::GLR_SHDW_KEY_url][$i];
               $sFil = $a[Glb::GLR_SHDW_KEY_files][$i];
               $sFil = substr($sFil, strlen(Glb::$Glr_sLocation_ScanBase_Fs)); // workaround

               $sVal .= $sCR . ' <tr>' . $sCR;
               //$sVal .= $sCR . '<td style="font-size:77%;color:black;">'   . $a[Glb::GLR_SHDW_KEY_pext][$i]   . '</td>';
               $sVal .= $sCR . '<td style="font-size:77%; max-width:4.7em; overflow:hidden; text-align:right;text-overflow: ellipsis;"><a href="' . $sUrl . '">' . $sFil . '</a></td>';
               $sVal .= $sCR . '<td style="font-size:77%;color:blue;">'    . $a[Glb::GLR_SHDW_KEY_width][$i]  . '</td>';
               $sVal .= $sCR . '<td style="font-size:77%;color:green;">'   . $a[Glb::GLR_SHDW_KEY_height][$i] . '</td>';
               $sVal .= $sCR . '<td style="font-size:77%;color:red;">'     . $a[Glb::GLR_SHDW_KEY_maxdim][$i] . '</td>';
               $sVal .= $sCR . '<td style="font-size:77%;color:lime;">'    . $a[Glb::GLR_SHDW_KEY_ver][$i]    . '</td>'; // Version
               $sVal .= $sCR . '<td style="font-size:77%;color:gray;">'    . $a[Glb::GLR_SHDW_KEY_fpc][$i]    . '</td>'; // Percent
               $sVal .= $sCR . '<td style="font-size:77%;color:fuchsia;">' . $a[Glb::GLR_SHDW_KEY_fpq][$i]    . '</td>'; // Quality
               $sVal .= $sCR . '<td style="font-size:66%;color:green;">'   . $a[Glb::GLR_SHDW_KEY_flav][$i]   . '</td>'; // FlavourString
               $sVal .= $sCR . '<td style="font-size:77%;color:blue;">'    . $a[Glb::GLR_SHDW_KEY_addi][$i]   . '</td>'; // AdditionalFilenameComponent
               $sVal .= $sCR . '<td style="font-size:77%;color:teal;">'    . $a[Glb::GLR_SHDW_KEY_ext][$i]    . '</td>'; // FilenameExtension
               $sVal .= $sCR . '<td style="font-size:77%;color:blue;">'    . $a[Glb::GLR_SHDW_KEY_size][$i]   . '</td>'; // Filesize
               $sVal .= $sCR . '<td style="font-size:77%;color:green;">'   . $a[Glb::GLR_SHDW_KEY_gtyp][$i]   . '</td>'; // Graphicstype
               $sVal .= $sCR . '<td style="font-size:77%;color:blue;">'    . $a[Glb::GLR_SHDW_KEY_bits][$i]   . '</td>'; // Bits
               $sVal .= $sCR . '<td style="font-size:77%;color:green;">'   . $a[Glb::GLR_SHDW_KEY_chan][$i]   . '</td>'; // Channels
               $sVal .= $sCR . '<td style="font-size:77%;color:lime;">'    . $a[Glb::GLR_SHDW_KEY_mime][$i]   . '</td>'; // Mimetype
               $sVal .= $sCR . ' </tr>';

               // () [seq 20110502°2153]
               if ($a[Glb::GLR_SHDW_KEY_err][$i] !== '')
               {
                  $sVal .= $sCR . ' <tr>' . $sCR;
                  $sVal .= $sCR . '<td></td>';
                  if ($_SESSION[Glb::SESKEY_MaintenanceMode])
                  {
                     $sVal .= $sCR . '<td style="font-size:77%;color:red;" colspan="15">' . $a[Glb::GLR_SHDW_KEY_err][$i] . '</td>';
                  }
                  else
                  {
                     $sVal .= $sCR . '<td style="font-size:77%;color:red;" colspan="15">' . 'Something is wrong.' . '</td>';
                  }
                  $sVal .= $sCR . ' </tr>';
               }
            }
            $sVal .= $sCR . '</table>' . $sCR;
         }
         else
         {
            // () [line 20110502°2154]
            $sVal = $val;
         }

         // () [seq 20110502°2155]
         if ($sVal !== '')
         {
            // The buddyfile part
            $sOut .= $sCR . ' <tr>';
            $sOut .= $sCR . '  <td>';
            $sOut .= $sCR . '                 ' . $sKi;
            $sOut .= $sCR . '  </td>';
            $sOut .= $sCR . '  <td>';

            // () Care for umlauts [line 20110526°1721]
            // ref : See issue 20110526°1721 'umlauts and htmlspecialchars'
            $sOut .= $sCR . '                 ' . utf8_encode($sVal);  // This works

            $sOut .= $sCR . '  </td>';
            $sOut .= $sCR . '  <td>';
            $sOut .= $sCR . '                 <small>[ctrl]</small>'; // '&lt;ctrl&gt;'
            $sOut .= $sCR . '  </td>';
            $sOut .= $sCR . ' </tr>';
         }
      }

      $sOut .= $sCR . '</table>';
      $sOut .= $sCR . '<p>&nbsp;</p>';

      // Build .. fragment [seq 20110503°1124]
      //  See issue 20110503°1122 'redundant sequences'
      if ($_SESSION[Glb::SESKEY_bDebug] === true)
      {
         $sOut .= $sCR . '<p>';
         $sOut .= $sCR . ' <small>Debug Obyekt:';
         $sOut .= $sCR . '  <br><i>sFilefamilyname</i> = ' . utf8_encode($buddy->sFilefamilyname);
         $sOut .= $sCR . '  <br><i>sBuddyfullfilename</i> = ' . utf8_encode($buddy->sBuddyfullfilename);
         $sOut .= $sCR . '  <br><i>sBuddyfilename</i> = ' . utf8_encode($buddy->sBuddyfilename);
         $sOut .= $sCR . '  <br><i>sAbsolutepath</i> = ' . utf8_encode($buddy->sAbsolutepath);
         $sOut .= $sCR . '  <br><i>sBasepath2use</i> = ' . utf8_encode($buddy->sBasepath2use);
         $sOut .= $sCR . '  <br><i>sRelativepath</i> = ' . utf8_encode($buddy->sRelativepath);
         $sOut .= $sCR . ' </small>';
         $sOut .= $sCR . '</p>';
      }
      //--------------------------

      self::$sOutMain = $sOut;
   }

   /**
    * This method searches the shadow file for a given file family
    *
    * @callers • self
    * @id method 20190204°0731
    * @param string $sFileFam The file-family-name to search for
    * @return Object|Boolean Either the found buddy object or FALSE
    */
   private static function seekShadowFile($sFileFam)
   {
      // Build shadowfilename [seq 20190204°0732]
      $sXmlFile = Glb::$Ses_sDataDirFullname;
      $sXmlFile .= '/' . 'DbTxt/shadow';
      $sXmlFile .= '/' . $sFileFam . '.xml';

      // Load XML [seq 20190204°0733]
      if (!is_file($sXmlFile))
      {
         return FALSE;
      }
      $xml = simplexml_load_file($sXmlFile);

      // Paranoia [seq 20190204°0734]
      if ($xml === FALSE)
      {
         // attention
         return FALSE;
      }

      // Get buddyfile to create an Obyekt from it [seq 20190204°0735]
      $sBdyFil = '';
      $eFiFam = $xml->filefamily;
      foreach ($eFiFam->children() as $fil)
      {
         foreach($fil->attributes() as $key => $val)
         {
            if (($key === 'ext') &&  (("" . $val) === 'txt' ))
            {
               $sBdyFil = "" . $fil;
               break;
            }
         }
      }

      // Instantiate wanted buddy object [seq 20190204°0736]
      $buddy = FALSE;
      if ($sBdyFil !== '')
      {
         $buddy = new Obyekt($sBdyFil);
      }

      // ready
      return $buddy; // $ret;
   }

   /**
    * This static field presents the page HTML main payload fragment
    *
    * @id field 20190202°1232
    * @callers The HTML page
    * @var String
    */
   public static $sPageTitleObjectPart = 'N/A';
   //public static $buddy = null;

   /**
    * This static field presents the page HTML main payload fragment
    *
    * @id field 20190202°1233
    * @var String
    */
   public static $sOutMain = '';

}

/* eof */
