<?php

/**
 * This module provides constants for the file family record.
 *
 * file      : 20110428°2321
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2023 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 */

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

include_once(__DIR__ . '/../core/ErrorHandler.php');
set_error_handler("Trekta\\Daftari\\my_exceptons_error_handler");

/**
 * This class represents a buddyfile plus it's file family.
 *
 * @id 20110501°0121
 * @note This is to handle an image filefamily, but we call it 'object',
 *        since it shall be ready to handle other filefamilies as well.
 * @callers PageAlbum.php, PageRaw.php, ...
 */
class Obyekt
{

   /**
    * This constructor instantiates one object of class obyekt by it's buddyfile.
    *
    * @id 20110501°0122
    * @todo Refactor the returns away. Use exceptions to indicate errors! [todo 20161008°0841]
    * @callers • File 20110428°0222 PageAlbum.php 2x
    *           • File 20110428°2321 PageObject.php 1x
    * @param $sFile {String} The buddyfile
    */
   function __construct($sFile)                                        // e.g. $sFile = "X:\workspaces\img12345678\img\20120123o1234.dingsda.txt"
   {
      // Helper for eliminating the forbidden returns [line 20190312°0141]
      $bReturnShallBeFalse = FALSE;

      // Runtime gauging [feature 20110515°1621]
      $this->tTimegaugeBcTotal = FileFamilies::getBcTime();            // e.g. '1306449157.86924400'
      $this->tTimegaugeBcPart1 = FileFamilies::getBcTime();

      // Detect problem with '~' in buddyfilename [seq 20190201°0441]
      $bBuddyTilde = FALSE;
      $posOne = strpos($sFile, '~');                                   // returns either • integer strpos starting with zero or • FALSE
      $posTwo = strpos($sFile, '_');
      if ( ! ( ($posOne === FALSE) && ($posTwo === FALSE) ) )
      {
         $bBuddyTilde = TRUE;
         //echo ('<p>[Dbg 20190201°0442] ' . $sFile . '<p>');
      }

      // () Set fields [seq 20110501°0123]
      $sFullfilename = $sFile;                                         // e.g. "X:\workspaces\daftaridev\trunk/daftari/license.txt"
      $this->sRelativepath = dirname($sFile);                          // e.g. 'img20110103.s/img'
      $a = preg_split( '#[\./\\\\]#', $this->sRelativepath );          // explode but for '.' or '/' or '\'
      $this->sArchivePlain = $a[0];                                    // e.g. 'img20110103' // provisory use just the first element
      $this->sArchivesBasepathLocaldrive = Glb::$Glr_sBasePathImgFolders;
      $this->sBuddyfullfilename = $sFullfilename;
      $this->sBuddyfilename = basename($sFullfilename);
      $this->sAbsolutepath = dirname($sFullfilename);
      $this->sBasepath2use = Glb::$Glr_sBasePathToUse;
      $this->sFilefamilyname = substr($this->sBuddyfilename,0,-4);     // CHECK - better use a filesplit function

      // () Process tilde names [seq 20190201°0443]
      if ($bBuddyTilde) {
         // subtract '~', '_', '+' and the like
         $this->sFilefamilyname = substr($this->sFilefamilyname,  0, -2);
      }

      // () Make the shadowbuddyfile's HTTP path [seq 20110501°0124]
      //  So far for reading only
      // ref : Remember issue 20190215°1323 'superglobal script_uri does not exist'
      $s = Glb::$Glr_sLocation_WebRoot_Url . $_SERVER['SCRIPT_NAME']; // BAD QUICK FIX 20190215°1325 // [marker 20190201°0813 'refactor']
      $a = explode('/', $s);
      unset($a[sizeof($a) - 1]);                                       // remove trailing element
      $s = implode('/', $a);                                           // e.g. 'http://localhost/gallery'
      $s = Glb::$Glr_sLocation_Daftari_Url . '.DATA'; // BAD QUICK FIX 20190202°0151 // [marker 20190201°0813 'refactor']
      $s .= '/dbtxt/shadow';                                           //
      $s .= '/' . $this->sFilefamilyname . '.xml';                     //
      $this->sShadowbuddyHttpFullname = $s;                            //

      // () Make the shadowbuddyfile's local path [seq 20110501°0125]
      //  So far to write or delete the file
      $s = $_SERVER[Glb::SVR_KEY_SCRIPT_FILENAME];                     // 'SCRIPT_FILENAME' // e.g. $s = "X:/workspaces/daftaridev/trunk/daftari/docs/gallery56album.html"
      $a = explode('/', $s);
      unset($a[sizeof($a) - 1]);                                       // remove trailing element
      $s = implode('/', $a);                                           // e.g. ~~ 'X:/workspaces/trunk/gallery'
      $s .= '/dbtxt/shadow' . '/' . $this->sArchivePlain;              // e.g. ~~ 'X:/workspaces/trunk/gallery/dbtxt/shadow/img1003'
      $s = Glb::$Ses_sDataDirFullname . '/dbtxt/shadow' . '/' . $this->sArchivePlain; // fix 20190201°0816
      $s .= '/' . $this->sFilefamilyname . '.xml';                     //
      $this->sShadowbuddyLocalFullname = $s;                           //
      $sBuddyFileFullname = Glb::$Ses_sDataDirFullname . '/dbtxt/shadow' . '/' . $this->sFilefamilyname . '.xml';                           // [mark 20190203°0711 refactor]

      // todo : Shift this below or above for adjusting the proportional gauging
      $this->tTimegaugeBcPart1 = FileFamilies::getBcTimespan($this->tTimegaugeBcPart1, FileFamilies::getBcTime());
      $this->tTimegaugeBcPart2 = FileFamilies::getBcTime();

      // () Read or build shadowbuddyfile [seq 20110501°0126]
      if ( is_file($sBuddyFileFullname) )                              //
      {
         $this->aShadowbuddy = self::readShadowbuddyfile
                              ( $sBuddyFileFullname
                               , $sFile
                                );                                     // [marker 20190201°0813 'gallery xml file']
         if ($this->aShadowbuddy === false)
         {
            // todo : A CONSTRUCTOR MUST NOT CONTAIN return (see todo 20161008°0841)
            //return FALSE;
            $bReturnShallBeFalse = TRUE;
         }
      }
      else
      {
         $b = $this->buildShadowbuddyfile();
         if ($b !== true)
         {
            //return FALSE;  // A CONSTRUCTOR MUST NOT CONTAIN return! // error, sometimes not easy to debug, so the caller should handle this gracefully (see todo 20161008°0841)
            $bReturnShallBeFalse = TRUE;
         }
      }

      if (! $bReturnShallBeFalse) {
         // () parse buddyfile for its fields [seq 20110501°0127]
         // note : The design philosophy is like follows. First look for the
         //    shadowbuddyfile, and only if that does not exist, or does not
         //    contain the wanted information, then go for the live buddyfile
         //    or other sources.
         $this->aaBuddyfile = $this->readbuddyfile($sFullfilename);
         if (gettype($this->aaBuddyfile) !== 'array')
         {
            // todo : Fix wrong syntax here — A constructor must not contain 'return'
            // Error — File does probably not adhere to the buddyfile format (see todo 20161008°0841)
            //return FALSE;
            $bReturnShallBeFalse = TRUE;
         }
      }

      if (! $bReturnShallBeFalse) {
         // () Measure time [seq 20110501°0128]
         $this->tTimegaugeBcPart2 = FileFamilies::getBcTimespan($this->tTimegaugeBcPart2, FileFamilies::getBcTime());
         $this->tTimegaugeBcTotal = FileFamilies::getBcTimespan($this->tTimegaugeBcTotal, FileFamilies::getBcTime());
      }
   }

   /**
    * This method builds this->shadowbuddy from a xml shadowbuddyfile.
    *
    * @id 20110514°1921
    * @note For now, we do this only on the local drive, because reading the
    *        folder listing via WebDAV or SVN client is not yet settled. As long
    *        as the gallery maintenance is restricted to localhost, this is sufficient
    * @callers Only • __construct
    * @return {Boolean} Success flag
    */
   private function buildShadowbuddyfile()
   {
      // Paranoia [seq 20110514°1931]
      //  We can use only localdrive, as long as we have no WebDAV or SVN access.
      if (! Glb::GLR_ACCESSSTYLE_localdrive)                           // 'localdrive'
      {
         // todo 20110514°1927 : Implement some feedback.
         // sorry ...
      }

      // Provide parent xml element [line 20110514°1932]
      $eBuddy = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><buddy></buddy>');

      // Guarantee the buddyfile be already read [seq 20110515°0631]
      // Quick'n'dirty fix, if the shadowbuddyfile does not yet exist
      // CHECK - Were the buddyfile-reading possibly better done somewhere before? Refactor?
      if (! isset($this->aaBuddyfile))
      {
         $s = $this->sRelativepath . '/' . $this->sBuddyfilename;      // [mark 20190203°0711 chg]
         $this->aaBuddyfile = $this->readbuddyfile($s);                // "X:\workspaces/X:\workspaces\daftaridev\trunk/daftari/docs/imgs26/19730815o1234.schwan.~.txt"
      }

      // Process the onedimensional bulk elements [seq 20110514°1933]
      foreach(self::$aShadowElementsSimple as $sField)
      {
         // Loop over the fieldnames of buddyfile [seq 20110514°1934]
         if (gettype($this->aaBuddyfile[0]) !== 'array')
         {
            echo ("<p><small>(Error 20110523°1445 issue 20110523°1441 occurred.)</small></p>");
            return false;
         }
         try
         {
            // event 20110522°2231 "Fatal error: Uncaught exception 'ErrorException'
            //  with message 'array_search() expects parameter 2 to be array, null given'"
            $iKi = array_search($sField, $this->aaBuddyfile[0]); 
         }
         catch ( \ErrorException $e )
         {
            echo ("<p><small>(Error 20110523°1446 issue 20110523°1441 occurred.)</small></p>");
            return false;
         }

         // [seq 20110514°1935]
         if ($iKi > 0)
         {
            $s = $this->aaBuddyfile[1][$iKi];
            // Otherwise e.g. umlauts would later prevent opening the xml file
            $s = utf8_encode($s);
            //-----------------
            // Line addChild() had error 20110516°0611 '.. unterminated entity
            // reference Wilhelm Heer', from string 'Eva & Wilhelm' with ampersand.
            // Check — How to officially encode the ampersand?! Use something like htmlspecialchars()?
            // Workaround so far:
            if (strpos($s, '&') !== false)                             // PRELIMINARY WORKAROUND?
            {
               $s = str_replace ('&' , '+' , $s );                     // PRELIMINARY WORKAROUND?
            }                                                          // PRELIMINARY WORKAROUND?

            $e = $eBuddy->addChild($sField,$s);                        // we'd like to set the value of an existing element but don't know how - CHECK: how to set the value of an existing element? EVENTUAL SOLUTION: simplexml_load_string()
            //-----------------
         }
         else
         {
            $e = $eBuddy->addChild($sField,'.');
         }
      }

      // Process special onedimensional elements [seq 20110514°1936]
      $s = utf8_encode($this->sArchivePlain);                          // Prophylactic [line 20110526°1641]
      $eArchive = $eBuddy->addChild(Glb::GLR_BUDY_KEY_archive, $s);    // 'archive'

      // Process filefamily [seq 20110514°1937]
      $s = $this->sArchivesBasepathLocaldrive . '/' . $this->sRelativepath;
      $aFam = self::get_filefamily ( $this->sArchivesBasepathLocaldrive
                                    , $this->sArchivePlain
                                     , $this->sFilefamilyname
                                      , $this->sRelativepath)
                                       ; // works only with the localdrive path so far
      $eFifa = $eBuddy->addChild(Glb::GLR_BUDY_KEY_filefamily);        // 'filefamily'

      // [seq 20110514°1938]
      foreach($aFam as $aFile)                                         // E.g. '_20110414-1315_psychobox_.v0m0040.png'
      {
         $sFextOBSOLETE = $aFile[0];                                   // foldernameextension
         $sFile = $aFile[1];
         $s = $aFile;                                                  // [mark 20190203°0711 chg]
         // Now e.g. $s = "X:\workspaces\daftaridev\trunk/daftari/docs/imgs26/:"

         //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         // [seq 20110515°1921]
         // Re-introducing image dimension-flavours 'percent' and 'maxdim' retrieved
         //  by filenameparsing plus casually obtaining timestamp and the corename

         include_once __DIR__ . '/../galari/FilenameParser.php';

         $fp = new Filenameparse($aFile, $s , $this->sArchivesBasepathLocaldrive);
         if (! $fp)
         {
            echo ('<p>Issue 20110518°2145</p>');
            return false;
         }

         // Build the node [seq 20110514°1939]
         try
         {
            $sFile = FileFamilies::mangleXmlContent($aFile);           // introduced with issue 20110523°1422 'filename with ampersand'
            $s = utf8_encode($sFile);                                  // prophylactic [line 20110526°1641`02]
            $eFile = $eFifa->addChild ('file', $sFile);                // 'file' e.g. "X:\workspaces\daftaridev\trunk/daftari/docs/imgs26/19730815o1234.schwan.~.txt"
         }
         catch ( \ErrorException $e )
         {
            // Fatal — Subsequent error 20110518°2131 'Fatal error: Call to a member function addAttribute() on a non-object in X:\workspaces\trunk\gallery\class.obyekt.php on line 0'
            echo ('<p>Issue 20110518°2144</p>');
            return false;
         }

         // Fill in attributes [seq 20110514°1941]
         $s = utf8_encode($sFextOBSOLETE);                             // prophylactic [line 20110526°1641`03]
         $s = ($s == '') ? '' : '.' . $s;                              // supplement dot for blank
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_pext, $s);             // 'pext' // pathnameextension, blank for the parent folder or '.s' for the scales folder

         // [seq 20110514°1942]
         if (Glb::bToggle_FALSE)                                       // EXPERIMENTAL [seq 20110526°1651] - COMPARE BELOW seq 20110526°1652
         {
            $eFile->addAttribute(Glb::GLR_SHDW_KEY_width, $fp->iLiveWidthReal); // 'width' // fp->iWidthReal shall be ELIMINATED
            $eFile->addAttribute(Glb::GLR_SHDW_KEY_height, $fp->iLiveHeightReal); // 'height' // fp->iHeightReal shall be ELIMINATED
            $eFile->addAttribute(Glb::GLR_SHDW_KEY_maxdim, $fp->iLiveMaxdimReal); // 'maxdim' // fp->iMaxdimReal shall be ELIMINATED
         }
         else
         {
            $eFile->addAttribute(Glb::GLR_SHDW_KEY_width, $fp->iFlavWidth); // 'width' // fp->iWidthReal shall be ELIMINATED
            $eFile->addAttribute(Glb::GLR_SHDW_KEY_height, $fp->iFlavHeight); // 'height' // fp->iHeightReal shall be ELIMINATED
            $eFile->addAttribute(Glb::GLR_SHDW_KEY_maxdim, $fp->iFlavMaxdim); // 'maxdim' // fp->iMaxdimReal shall be ELIMINATED
         }

         // [seq 20110514°1943]
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_ver, $fp->iVersion);   // 'ver' // 'version'
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_fpc, $fp->iFlavPercent); // 'fpc' // 'fp-percent'
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_fpq, $fp->iFlavQuality); // 'fpq' // 'fp-quality'
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_size, $fp->iFparFilesize); // 'size' //
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_ext, utf8_encode($fp->sFnamExtension)); // 'ext' // 'extension' (utf-8 20110526°164104)
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_flav, utf8_encode($fp->sFnamFlavour)); // 'flav' // 'flavour' - the flavour string e.g. 'p99q77' or 'v2x0720y560q66' (utf-8 20110526°164105)
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_addi, utf8_encode($fp->sAdditional)); // 'addi' // (utf-8 20110526°164106)

         // For fun or possible comparison purposes [seq 20110514°1944]
         if (self::$bHandleFilefamilymemberExtraAttributes === true)
         {
            if (Glb::bToggle_FALSE) // experiment [seq 20110526°1652]
            {
               // Since above seq 20110526°1651 we now write 'fp->iWidth' into 'width',
               // 'nwidth' is free to store 'fp->iWidthReal' ... Just for not throwing it
               // away for the moment ... But definitely throw it away on the long-term!
               $eFile->addAttribute(Glb::GLR_SHDX_KEY_nwidth, $fp->iLiveWidthReal); // 'nwidth' fp->iWidthReal shall be ELIMINATED
               $eFile->addAttribute(Glb::GLR_SHDX_KEY_nheight, $fp->iLiveHeightReal); // 'nheight' fp->iHeightReal shall be ELIMINATED
               $eFile->addAttribute(Glb::GLR_SHDX_KEY_nmaxdim, $fp->iLiveMaxdimReal); // 'nmaxdim' fp->iMaxdimReal shall be ELIMINATED
            }
            else
            {
               $eFile->addAttribute(Glb::GLR_SHDX_KEY_nwidth, $fp->iFlavWidth); // 'nwidth' 'nominal-width' - width obtained by filenameparsing
               $eFile->addAttribute(Glb::GLR_SHDX_KEY_nheight, $fp->iFlavHeight); // 'nheight' 'nominal-height' - height obtained by filenameparsing
               $eFile->addAttribute(Glb::GLR_SHDX_KEY_nmaxdim, $fp->iFlavMaxdim); // 'nmaxdim' 'nominal-maxdim' - maxdim obtained by filenameparsing
            }
         }

         // More attributes from getimagesize() (new 20110516°2211)
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_gtyp, $fp->iGrphGraphicstype); // 'gtyp'
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_bits, $fp->iGrphBits);  // 'bits'
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_chan, $fp->iGrphChannels); // 'chan'
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_mime, utf8_encode($fp->sGrphMimetype)); // 'mime' // (utf-8 20110526°164107)
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_err, utf8_encode($fp->sFparError)); // 'err' // (utf-8 20110526°164108)
         if (self::$bHandleFilefamilymemberExtraAttributes === true)
         {
            $eFile->addAttribute(Glb::GLR_SHDX_KEY_html, utf8_encode($fp->sGrphHeightWidthHtml)); // (utf-8 20110526°164109)
         }

         // Bold workaround to supplement URL [seq 20190204°0752]
         $sFileUrl = substr($sFile, strlen(Glb::$Glr_sLocation_ScanBase_Fs));
         $sFileUrl = Glb::$Glr_sLocation_ScanBase_Url . $sFileUrl;
         $eFile->addAttribute(Glb::GLR_SHDW_KEY_url, $sFileUrl);

         // Then some other values [seq 20110514°1945]
         $sFParsedTimestamp = utf8_encode($fp->sFnamTimestamp);        // (utf-8 20110526°164110)
         $sFParsedCorename = utf8_encode($fp->sFnamCorename);          // (utf-8 20110526°164111)
         //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      }

      // Just use the values from last filefamily member [seq 20110514°1946]
      $eTimestamp = $eBuddy->addChild(Glb::GLR_BUDY_KEY_timestamp, utf8_encode($sFParsedTimestamp)); // 'timestamp' // (utf-8 20110526°164112)

      $s = FileFamilies::mangleXmlContent($sFParsedCorename);
      $s = utf8_encode($s);                                            // [line 20110526°1632] added
      $eCorename = $eBuddy->addChild(Glb::GLR_BUDY_KEY_corename, $s);  // 'corename'

      // Process albums [seq 20110514°1947]
      $eAlbums = $eBuddy->addChild(Glb::GLR_BUDY_KEY_albums); // 'albums'
      $iKi = array_search(Glb::GLR_BUDY_KEY__album, $this->aaBuddyfile[0]); // 'album'
      if ($iKi)
      {
         // [seq 20110514°1948]
         $sLine = $this->aaBuddyfile[1][$iKi];
         $aAlbs = explode(',', $sLine);                                // Make albums array - TODO: outsource as function and make more generic
         for ($i = 0; $i < sizeof($aAlbs); $i++)
         {
            $aAlbs[$i] = trim($aAlbs[$i]);
         }

         // Eliminate empty array entries [seq 20110514°1949]
         for ($i = 0; $i < sizeof($aAlbs); $i++)
         {
            if ($aAlbs[$i] === '') { unset($aAlbs[$i]); }
         }
         $aAlbs = array_values($aAlbs);                                //Re-index

         $aAlbs[] = 'all';                                             // Mandatory

         // Enroll albums [seq 20110514°1951]
         for ($i = 0; $i < sizeof($aAlbs); $i++)
         {
            try
            {
               $s = utf8_encode($aAlbs[$i]);  // Did this also with other fields // [line 20110526°1631] fix quirk 20110518°2224 (xml will not open if album contains scharfes 's')
               $eAlbum = $eAlbums->addChild (Glb::GLR_BUDY_KEY__album, $s); // 'album'
            }
            catch ( \ErrorException $e )
            {
               $eAlbum = $eAlbums->addChild (Glb::GLR_BUDY_KEY__album, 'Error_20110518°2223'); // 'album'
            }
         }
      }

      // Build provisory new-style shadow folder path [seq 20190203°0731]
      $sPathId = $this->sRelativepath; // e.g. "X:\workspaces\daftaridev\trunk/daftari/docs/imgs26"
      $sPathId = substr($sPathId, 34, strlen($sPathId)); // [mark 20190203°0711 chg]
      $sPathId = str_replace("\\", '^', $sPathId);
      $sPathId = str_replace("/", '^', $sPathId);

      // Guarantee folder existing [seq 20110515°0632]
      // todo : See issue 20190201°0805 'guarantee directory'
      $sShadowpath = Glb::$Ses_sDataDirFullname .  '/dbtxt/shadow';  // . '/' . $sPathId; // [mark 20190203°0711 chg]
      if (! is_dir( Glb::$Ses_sDataDirFullname .  '/dbtxt' ))
      {
         $b = mkdir( Glb::$Ses_sDataDirFullname .  '/dbtxt' );
         if ($b !== TRUE)
         {
            $s = '<p class="phpError">Issue 20190201°0817 - Folder missing.</p>';
            return FALSE;
         }
      }
      if (! is_dir( Glb::$Ses_sDataDirFullname .  '/dbtxt/shadow' ))
      {
         $b = mkdir( Glb::$Ses_sDataDirFullname .  '/dbtxt/shadow' );
         if ($b !== TRUE)
         {
            $s = '<p class="phpError">Issue 20190201°0818 - Folder missing.</p>';
            return FALSE;
         }
      }

      // [seq 20110514°1952] // Superfluous since we have no more subfolders [note 20190204°0422]
      if (! is_dir($sShadowpath))
      {
         $b = mkdir($sShadowpath); // "X:\workspaces\daftaridev\trunk\daftari.DATA/dbtxt/shadow/X:"
         if ($b !== TRUE)
         {
            $s = '<p class="phpError">Issue 20110518°2143 - Folder missing.</p>';
            return FALSE;
         }
      }

      // Write XML shadowbuddyfile [seq 20110514°1953]
      $sTgt = $sShadowpath . '\\' . $this->sFilefamilyname . '.xml';
      self::xmlWriteToFile($eBuddy, $sTgt); // PhanTypeMismatchArgument [phan 20190312°0126`12]

      // Immediately create the shadowbuddy property [seq 20110516°0221]
      $this->aShadowbuddy = self::readShadowbuddyfile($sTgt, ''); // [marker 20190201°0813 'gallery xml file']

      // Maintain the albums of the now read object [seq 20110514°1954]
      foreach ($this->aShadowbuddy[Glb::GLR_BUDY_KEY_albums] as $s)    // 'albums'
      {
         $b = $this->maintain_album($s);
         if (! $b)
         {
            echo ('<p>Issue 20110518°2138</p>');
            return FALSE;
         }
      }

      return TRUE;
   }

   /**
    * Provide array of arrays with the small direct family-file links
    *
    * @id 20110501°2121
    * @callers Only • PageRaw.php
    * @return Array $aaFlinks — The wanted family-links
    */
   public function familylinks()
   {
      // Provisory — Avoid error if folder does not exist [line 20110503°0141]
      $aaFlinks = array();

      $sUrlBase = Glb::$Glr_sUrlImageRepoBase . '/' . $this->sRelativepath . '/';

      // [seq 20110518°2221]
      // todo : Possibly merge nearly identical sequens as above for error
      try
      {
         $iNumberOfFiles = sizeof($this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_files]);
      }
      catch ( \ErrorException $e )
      {
         sleep(0);                                                     // Breakpoint
         return $aaFlinks;
      }

      // () [seq 20110501°2123]
      for ($i = 0; $i < $iNumberOfFiles; $i++)
      {
         // () [seq 20110501°2125]
         $s = $this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_files][$i];
         $i2 = strpos( $s , '#');
         if ($i2 !== FALSE)
         {
            sleep(0);                                                  // Breakpoint
         }

         // () Other flavours [seq 20110501°2126]
         $s = $this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_maxdim][$i];
         if ($s == '')
         {
            $s = $this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_files][$i];
            $s = (substr($s,-4) == '.txt') ? 'txt' : 'fjl';            // What is 'fjl'? [quest 20190130°0135]
         }

         // () Append link for one file [seq 20110501°2127]
         $aaFlinks[$i]['text'] = $s;                                   // quick'n'dirty
         $aaFlinks[$i][Glb::GLR_SHDW_KEY_url] =  $this->aShadowbuddy
                                        [Glb::GLR_BUDY_KEY_filefamily]
                                         [Glb::GLR_SHDW_KEY_files]
                                          [$i]
                                           ;
      }
      return $aaFlinks;
   }

   /**
    * This function possibly returns foldernameextension.
    *
    * @id 20110522°1621
    * @todo This method were probably better placed e.g. into some Utilities class
    * @callers ..
    * @param $sFoldername {string} ...
    * @return string The wanted foldername extension so far '' or '.s'
    */
   private static function getFoldernameExtension($sFoldername)
   {
      $sFext = '';
      $a = explode('.',$sFoldername);
      if (sizeof($a) > 1) {
         $sFext = $a[sizeof($a)-1];
      }

      return $sFext;
   }

   /**
    * This method retrieves a thumbnail of specified dimension.
    *
    *  CHECK - Possibly store this information in the shadowbuddyfile
    *  to save later runtime?
    *
    * @id 20110514°1311
    * @callers PageObject.php
    * @param $iDimWanted {} If 0, then retrieve the maximum percented' file (usually
    *      99 percent), otherwise the wanted dimension (maximum pixel width/height).
    * @return Array An array with elements ['file'] ['width'] ['height'] ['maxdim']
    *      where ['file'] is the image with it's relative path e.g. and ['maxdim']
    *      serves as a control how precise the given image matches the wanted
    */
   public function getThumb($iDimWanted)
   {
      // Prologue [seq 20110514°1321]
      $aThumb = array();
      $aThumb[Glb::GLR_SHDW_KEY_file] = '';
      $aThumb[Glb::GLR_SHDW_KEY_width] = 0;
      $aThumb[Glb::GLR_SHDW_KEY_height] = 0;
      $aThumb[Glb::GLR_SHDW_KEY_maxdim] = 0;
      $aThumb[Glb::GLR_SHDW_KEY_fpc] = 0;

      $aThumb[Glb::GLR_SHDW_KEY_url] = '';

      // Guarantee shadowbuddy exist [seq 20110514°1322]
      // E.g. 'Undefined index: filefamily'
      try
      {
         $iNumberOfFiles = sizeof( $this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_files] );
      }
      catch ( \ErrorException $e )
      {
         return $aThumb;
      }

      // Paranoia [seq 20110514°1323]
      if (sizeof($this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_files]) < 1)
      {
         $this->aShadowbuddy = self::readShadowbuddyfile($this->sShadowbuddyHttpFullname, ''); // [marker 20190201°0813 'gallery xml file']
      }

      // [seq 20110514°1324]
      // todo : Possibly merge nearly identical sequens as below for error [todo 20110518°2222]
      try
      {
         $iNumberOfFiles = sizeof($this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_files]);
      }
      catch ( \ErrorException $e )
      {
         return $aThumb;
      }

      // Flavour selection algorithm [seq 20110514°1325]
      // Selecting either a thumb after dimension or a percentage
      for ($iKi = 0; $iKi < $iNumberOfFiles; $iKi++)
      {
         $bTake = false;
         if ($iDimWanted == 0)
         {
            // Percentage selection algorithm [seq 20110514°1326]
            // Retrieve a 100 percent file
            $iPercentCandidate = $this->aShadowbuddy
                                        [Glb::GLR_BUDY_KEY_filefamily]
                                         [Glb::GLR_SHDW_KEY_fpc]
                                          [$iKi]
                                           ;                           // Selection after percent
            $iPercentGiven = $aThumb[Glb::GLR_SHDW_KEY_fpc];           // Comfort
            if ($iPercentGiven < 1)
            {
               $bTake = true;                                          // We have none yet at all, so take any
            }
            else if ($iPercentCandidate > $iPercentGiven)
            {
               $bTake = true;
            }
         }
         else
         {
            // (.) Thumbnail selection algorithm [seq 20110526°1141]
            // We are looking for the dimension closest to the wanted one.
            // If exact match is not available, we choose the next bigger.

            // (.1) Retrieve a specified maxdim file
            $iDimGiven = $aThumb[Glb::GLR_SHDW_KEY_maxdim];            // comfort
            $iDimCandidate = $this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_maxdim][$iKi];

            // (.2) We have no thumb yet, so take any
            if ($iDimGiven < 1)
            {
               $bTake = true;
            }
            // (.3) If (candidate smaller than given) and (greater than wanted)
            else if (($iDimCandidate <= $iDimGiven) and ($iDimCandidate >= $iDimWanted))
            {
               $bTake = true;
            // (.4) Candidate (greater than wanted) and (given smaller than wanted)
            } else if (($iDimCandidate >= $iDimWanted) and ($iDimGiven < $iDimWanted))
            {
               $bTake = true;
            }
         }

         // [seq 20110514°1327]
         if ($bTake === true)
         {
            // Bad quick workaround [seq 20190204°0741]
            $s = $this->sAbsolutepath; // e.g.  "X:\workspaces\daftaridev\trunk/daftari/docs/imgs26"
            $s = substr($s, strlen(Glb::$Glr_sLocation_ScanBase_Fs ));
            $s = Glb::$Glr_sLocation_ScanBase_Url . $s;
            $s2 = $this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_files][$iKi];
            $aHlp = explode('/', $s2);
            $s2 = $aHlp[sizeof($aHlp) - 1];
            $s = $s . '/' . $s2;
            $aThumb[Glb::GLR_SHDW_KEY_file] = $s;

            $aThumb[Glb::GLR_SHDW_KEY_width] = $this->aShadowbuddy
                                                       [Glb::GLR_BUDY_KEY_filefamily]
                                                        [Glb::GLR_SHDW_KEY_width] // 'width'
                                                         [$iKi]
                                                          ;
            $aThumb[Glb::GLR_SHDW_KEY_height] = $this->aShadowbuddy
                                                        [Glb::GLR_BUDY_KEY_filefamily]
                                                         [Glb::GLR_SHDW_KEY_height]
                                                          [$iKi]
                                                           ;
            $aThumb[Glb::GLR_SHDW_KEY_maxdim] = $this->aShadowbuddy
                                                        [Glb::GLR_BUDY_KEY_filefamily]
                                                         [Glb::GLR_SHDW_KEY_maxdim]
                                                          [$iKi]
                                                           ;
            $aThumb[Glb::GLR_SHDW_KEY_fpc] = $this->aShadowbuddy
                                                     [Glb::GLR_BUDY_KEY_filefamily]
                                                      [Glb::GLR_SHDW_KEY_fpc] // 'fpc'
                                                       [$iKi]
                                                        ;

            // [line 20190204°0753]
            $aThumb[Glb::GLR_SHDW_KEY_url]
               = $this->aShadowbuddy[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_url][$iKi];
         }
      }

      return $aThumb;
   }

   /**
    * This function retrieves the file-family for a given buddy-file.
    *
    * @id 20110428°2121
    * @note 20110503°1321 : The file-family has the DISADVANTAGE so far,
    *        that it does not contain the folder, only the plain-filenames!
    *        Implement entries with relative paths like in the albums files.
    * @callers only __construct()
    * @param $sBasepath {String} The ...
    * @param $sParentFolder {String} Folder to get files, either local drive or URL (so far only on local drive)
    * @param $sFilefamilyname {String} The file-family-name of the files to be listed, e.g. '19730815o1234'
    * @param $sFullpath {String} The full file path
    * @return {Array} An array of arrays with the folder-name-extension and the files-names of the file-family
    */
   private static function get_filefamily ( $sBasepath
                                           , $sParentFolder
                                            , $sFilefamilyname
                                             , $sFullpath              // Additional [mark 20190203°0711 chg]
                                              )
   {

      //~~~~~~~~~~~~~~~~~~~~~~~
      // [seq 20190203°0721
      $aRet = array();
      $handle = opendir($sFullpath);                                   // Only localdrive folders
      if ($handle)
      {
         while ( $sFile = readdir($handle) )
         {
            if ( strlen($sFile) < strlen($sFilefamilyname) )
            {
               continue;
            }
            if (substr($sFile, 0, strlen($sFilefamilyname)) === $sFilefamilyname)
            {
               array_push($aRet, $sFullpath . '/' . $sFile);
            }
         }
      }
      return $aRet;
      //~~~~~~~~~~~~~~~~~~~~~~~

// outcommented 20190312°0153
/*
      // For now, we have only this capability [seq 20110428°2122]
      $sAccessStyle = Glb::GLR_ACCESSSTYLE_localdrive;                 // 'localdrive'

      // Retrieve directory listing, either from local drive or from WebDAV [seq 20110428°2123]
      if ($sAccessStyle == Glb::GLR_ACCESSSTYLE_localdrive)            // 'localdrive'
      {
         // Use local drive [seq 20110428°2124]
         $aFam = array();                                              // Return value

         //-------------------------------------------------------
         // [seq 20110522°1521]
         // todo : Outsource sequence to function getFolderfamFiles.
         $sArchivePlain = $sParentFolder;                              // Synonym for folderfamily
         $a = FileFamilies::getFolderfamilyFromParent($sBasepath . '/' . $sArchivePlain);
         $a = array($sNewPath); // So what!? [phan 20190312°0151'02] PhanUndeclaredVariable $sNewPath
         foreach ($a as $sFoFamMemb)                                   // [mark 20190203°0711 chg]
         {
            $sFoldernameextension = self::getFoldernameExtension($sFoFamMemb); // either '' or '.s/'
            $sFullpath =  $sBasepath . '/' . $sFoFamMemb . '/' . Glb::GLR_sPATHPART_INTERCALARY;
            if (! is_dir($sFullpath))
            {
               sleep(0); // ERROR, e.g. archive contains no 'img' folder
               continue;
            }
            $handle = opendir($sFullpath);                             // Only localdrive folders
            if (! $handle)
            {
               return FALSE; // error
            }
            while (FALSE !== ($file = readdir($handle)))
            {
               if (strlen($file) < strlen($sFilefamilyname))
               {
                  continue;
               }
               if (substr($file, 0, strlen($sFilefamilyname)) === $sFilefamilyname)
               {
                  $aHlp[0] = $sFoldernameextension;
                  $aHlp[1] = $file;
                  $aFam[] =  $aHlp ; // append
               }
            }
            sleep(0);                                                  // Just a breakpoint
         }
         sleep(0);                                                     // Just a breakpoint
         //-------------------------------------------------------

      }
      else if ($sAccessStyle === Glb::GLR_ACCESSSTYLE_svnclient)       // 'svn-client'
      {
         //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         // This is an already little-bit tested seqence saved from the abandoned
         // former function getFilefamily() for later re-implementation [note 20110515°1531]
         // IT IS CERTAINLY MESSED UP BY THE ADDITIONAL FOLDERFAMILY FEATURE [note 20110522°1622]
         //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

         // Use WebDAV [seq 20110428°2125]
         // note : phpsvnclient->getDirectoryFiles() does not work with
         // a repository forest, but only with a concrete repository!
         // note : See there, warnings msg 20110510°0121, HOW TO REPAIR THAT WARNING?!
         include_once(__DIR__ . '/../phplibs/phpsvnclient.r63/phpsvnclient.php');

         $psc = new phpsvnclient($sReposUrl, $sReposUser, $sReposPw);

         $aa = $psc->getDirectoryFiles(Glb::GLR_sPATHPART_INTERCALARY); // 'img'
         if ($aa === false)
         {
            sleep(0);                                                  // Fatal error
         }
         // now we have e.g.
         // aa[0][type]     = directory
         // aa[0][last-mod] = Thu, 28 Apr 2011 00:04:34 GMT
         // aa[0][path]     = img
         // aa[0][status]   = HTTP/1.1 200 OK
         // aa[1][type]     = file
         // aa[1][last-mod] = Wed, 24 Jun 2009 16:35:44 GMT
         // aa[1][path]     = img/_19470708-1123_klassenfoto_p25q85_.jpg
         // aa[1][status]   = HTTP/1.1 200 OK

         // Read filename out [seq 20110428°2126]
         foreach ($aa as $a)
         {
            if ($a['type'] == 'file')                                  // 'type' 'file'
            {
               $aFam[] = $sPlainreposname . '/' . $a['path'];          // 'path'
            }
         }
         sleep(0); // breakpoint
      }

      return $aFam;  // files with relative/related paths
*/
   }

   /**
    * This method guarantees the existence of a folder, possibly creating it
    *
    * .. oh, I better first care for the place where the caller wants
    *   to write, which is wrong ..
    *
    * @id method 20190202°0111
    * @ref See issue 20190201°0805 'guarantee directory'
    * @callers :
    * @param {String} The wanted path, either a file or a folder
    * @return Boolean Success flag
    */
   private static function guaranteeFolderExistence($sWantedPath)      // [marker 20190201°0813 'restore gallery']
   {
      return TRUE;
   }

   /**
    * This method reads albums from buddyfile and possibly enroll the obyekt
    *   into album database.
    *
    * @id 20110429°1501
    *
    * @note Similar file access code is e.g. in
    *        - class.obyekt.php::readbuddyfile()
    *        - class.obyekt.php::maintain_albums()
    *        - class.dbtxt.php::load()
    * @callers : only Obyekt->__construct() only in case of rawfolder view
    * @param $sAlbum [string} The album to be maintained
    * @return bool Success flag
    */
   private function maintain_album($sAlbum)
   {
      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      // check 20110502°2221 — Mysterious entries in the album database
      // e.g. ' /img/_20110414-1315_psychobox_.txt' without the repos folder.
      // Those are created during the album view running.
      // AHA - $sFolder indeed is ONLY set during rawview!!!
      // So run maintain_albums() ONLY when called from the raw view!!!
      // This also makes functional sense, since for now we want maintain
      // only in the private rawview, not during the public album view.
      // In the Superglobals, we find e.g. the following elements available:
      //  — $_SERVER['SCRIPT_URL'] = /gallery/rawfolder.html
      //  — $_SERVER['SCRIPT_URI'] = http://localhost/gallery/rawfolder.html
      //  — $_SERVER['SCRIPT_FILENAME'] = X:/workspaces/trunk/gallery/rawfolder.html
      //  — $_SERVER['REQUEST_URI'] = /gallery/rawfolder.html?XDEBUG_SESSION_START=netbeans-xdebug
      //  — $_SERVER['SCRIPT_NAME'] = /gallery/rawfolder.html
      //  — $_SERVER['PHP_SELF'] = /gallery/rawfolder.html
      //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      // [seq 20110429°1511]
      try
      {
         $sEntry = $this->aShadowbuddy[Glb::GLR_BUDY_KEY_timestamp]
                  . ' ' . $this->sRelativepath . '/' . $this->sBuddyfilename
                   ;
      }
      catch ( \ErrorException $e )
      {
         $sEntry = 'quirk-20110518-2226' . ' ' . $this->sRelativepath
                  . '/' . $this->sBuddyfilename
                   ;
      }
      $sEntry .=  Glb::$sTkNL;

      // Loop over the albums known from buddyfile [seq 20110429°1512]
      $sAlb = $sAlbum;
      $sAlbfile = Glb::GLR_sALBUMSFOLDER . $sAlb . '.txt';             // 'dbtxt/albums/'

      // Fix 20190201°0814
      // albfile goes to the wrong place [marker 20190201°0813 'folder wrong']
      //  it needs a supplement
      $sAlbfile = Glb::$Ses_sDataDirFullname . '/' . $sAlbfile;

      // [seq 20110429°1513]
      $aLines = @file($sAlbfile);                                      // Flag FILE_IGNORE_NEW_LINES not used
      if (! $aLines)
      {
         $aLines = array();                                            // Otherwise error in array_search() below
      }

      // Look if object is already enrolled in the album database [seq 20110429°1514]
      $k = array_search($sEntry, $aLines);
      if ($k === false)
      {
         // Enroll object into albums database [seq 20110429°1515]
         $aLines[] = $sEntry;
         if (! sort($aLines))
         {
            return false;                                              // todo: Provide error message
         }

         // () Guarantee folder existing [seq 20190205°0641] quick'n'dirty workaround
         // todo : Merge and normalize the folder-exist-paranoia of many places.
         // E.g. "G:\work\downtown\daftaridev\trunk\daftari.DATA/dbtxt/albums/all.txt"
         $sWantedDir = Glb::$Ses_sDataDirFullname . '/' . 'dbtxt/albums';
         if (! is_dir($sWantedDir))
         {
            $b = mkdir($sWantedDir);
            if ($b === FALSE)
            {
               return FALSE;
            }
         }

         $i = file_put_contents($sAlbfile, $aLines);
      }

      return TRUE;
   }

   /**
    * This function reads a shadowbuddyfile into an array.
    *
    * This shall be an independent function to be used from everywhere.
    *
    * @id 20110514°1821
    * @note See issue 20110510°1821 'simplexml access quirks'
    * @callers So far •_construc() • buildShadowbuddyfile • getThumb • PageObject::seekShadowFile
    * @param $sFullfilename {String} This may be a localdrive path or an http path
    * @param $sCallingBuddyFile {String} Optional. If given, it is a supplemented familifile [param 20190204°0435 added]
    * @return {Array|Boolean} An array or false or empty array — The wanted shadowbuddy
    *               or error — CHECK the return details may not be settled yet.
    */
   public static function readShadowbuddyfile ( $sFullfilename        // url
                                                , $sCallingBuddyFile   // filesystempath
                                                 )
   {
      // Initialize array [seq 20110514°1832]
      $aShadow = array();

      // Mangle path [seq 20110514°1833]
      //  Mangle it only if it is a HTTP one, do not mangle if it is a localdrive
      //   path because e.g '&' or "'" would cause simplexml_load_file fail
      $sFullfilename = FileFamilies::mangleUrlFilename($sFullfilename);

      // [seq 20110514°1834]
      // todo 20110526°1623 : Replace the '@' by thorough error processing.
      // note 20110526°1622 : XML file fails to open if its content
      //    contains an umlaut, e.g. 'ß' in 20090917o1214.n523.txt
      $xml = @simplexml_load_file($sFullfilename); // WRONG?!

      // Paraonoia [seq 20110514°1847] should theoretically never happen
      if ($xml === FALSE)
      {

         //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         // [seq 20110515°1631]
         // function : Provide dummy shadowbuddy with error information
         // check : Not much tested how far a caller will come with this dummy
         $aShadow = array();
         $aShadow[Glb::GLR_BUDY_KEY_albums] = array('none');           // 'albums'
         $aShadow[Glb::GLR_SHDW_KEY_xmlerror][] = 'File load failed: \'' // 'xmlerror'
                                                 . $sFullfilename
                                                  . '\'.'
                                                   ;

         // [seq 20110514°1835]
         // It seems always empty, I have never seen a filled error object.
         // note : Remember issue 20150209°1241 'libxml errorexception'
         try {

            // [line 20190312°0211] provide array param separately to
            //   avoid unjustified Phan complain PhanParamSpecial next line
            //   .. does not help either
            $a = array();
            $a = $xml.libxml_get_errors();
            ///$x = implode(",", $a );
            $x = implode( $a );

         }
         catch ( \ErrorException $err ) {
            $x = '[Holla 20150209°1251]' .  $err->getMessage();
         }

         // Outcommented 20190312°0215
         /*
         // the foreach just seems not allowed with this error object [line 20110516°1442]
         if (FALSE) {
            $a = Array();
            foreach($x as $err) {                                      // may throw 'Invalid argument supplied for foreach'
               $a[Glb::PHP_XCP_KEY_message] = $err->message;           // 'message'
               $a[Glb::PHP_XCP_KEY_file] = $err->file;                 // 'file'
               $a[Glb::PHP_XCP_KEY_line] = $err->line;                 // 'line'
               $a[Glb::PHP_XCP_KEY_column] = $err->column;             // 'column'
               $a[Glb::PHP_XCP_KEY_code] = $err->code;                 // 'code'
               $a[Glb::PHP_XCP_KEY_level] = $err->level;               // 'level'
               $aShadow[Glb::GLR_SHDW_KEY_xmlerror][] = $a;            // 'xmlerror'
            }
         }
         */
         //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

         return $aShadow;
      }

      // Process dedicated simple values [seq 20110514°1836]
      //  Process as opposed to the bulk values below
      $s = @sprintf('%s', $xml->timestamp);                            // utf-8 20110526°165302
      $aShadow[Glb::GLR_BUDY_KEY_timestamp] = utf8_decode($s);         // 'timestamp'
      $s = @sprintf('%s', $xml->corename);                             // utf-8 20110526°165303
      $aShadow[Glb::GLR_BUDY_KEY_corename] = utf8_decode($s);          // 'corename'

      // Process the onedimensional elements [seq 20110514°1837]
      foreach (self::$aShadowElementsSimple as $sKi)
      {
         $aShadow[$sKi] = '';
         $eVal = $xml->$sKi;
         $s = @sprintf('%s', $eVal);                                   // utf-8 20110526°165304
         $aShadow[$sKi] = utf8_decode($s);
      }
      if ($aShadow[Glb::GLR_BUDY_KEY_title] == '')                     // 'title' // [line 20110526°1851]
      {
         $aShadow[Glb::GLR_BUDY_KEY_title] = $aShadow[Glb::GLR_BUDY_KEY_corename]; // 'title' 'corename'
      }

      // Process the albums [seq 20110514°1842]
      $aShadow[Glb::GLR_BUDY_KEY_albums] = array();                    // 'albums'
      $iCount = count($xml->albums->album);
      for ($i = 0; $i < $iCount; $i++)
      {
         $fi = $xml->albums->album[$i];
         $s = @sprintf('%s', $fi[$i]);
         $aShadow[Glb::GLR_BUDY_KEY_albums][] = utf8_decode($s);       // 'albums' utf-8 20110526°1653
      }

      // Process the filefamily [seq 20110514°1843]
      $aShadow[Glb::GLR_BUDY_KEY_filefamily] = array();                // 'filefamily'
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_files] = array(); // 'filefamily' 'files'
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_width] = array(); // 'filefamily' 'width'
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_height] = array(); // 'filefamily' 'height'
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_maxdim] = array(); // 'filefamily' 'maxdim'

      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_ver] = array(); // 'filefamily' 'ver'
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_fpc] = array(); // 'filefamily' 'fpc'
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_fpq] = array(); // 'filefamily' 'fpq'
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_size] = array(); // 'filefamily' 'size'
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_ext] = array(); // 'filefamily' 'ext'
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_pext] = array(); // 'filefamily' 'pext'  // path extension (means the archive scales folders, e.g '.s')

      // [seq 20110514°1844]
      if (Glb::bToggle_FALSE)
      {
         $aShadow[Glb::GLR_BUDY_KEY_filefamily]['flavour'] = array();  // 'filefamily' 'flavour'
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDX_KEY_nwidth] = array(); // 'filefamily' 'nwidth' // 'nominal' SHALL BE ELIMINATED
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDX_KEY_nheight] = array();  // 'filefamily' 'nheight'
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDX_KEY_nmaxdim] = array();  // 'filefamily' 'nmaxdim'
      }

      // Added [line 20190204°0754]
      $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_url] = array();

      // [seq 20110514°1845]
      $bIsCurrentFileInFileFamily = FALSE;                             // [line 20190204°0433]
      $iCount = count($xml->filefamily->file);
      for ($i = 0; $i < $iCount; $i++)
      {

         // [seq 20110514°1846]
         $fi = $xml->filefamily->file[$i];

         // [seq 20190204°0434]
         $sFilnam = $fi->__toString();                                 // e.g. "X:\workspaces\daftaridev\trunk/daftari/license.txt"
         if ($sFilnam === $sCallingBuddyFile)
         {
            $bIsCurrentFileInFileFamily = TRUE;
         }

         $iWidth = (int) sprintf('%s', $fi->attributes()->width);
         $iHeight = (int) sprintf('%s', $fi->attributes()->height);

         $s = @sprintf('%s', $fi[$i]);                                 // UTF-8 20110526°165305
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_files][] = $s; // 'filefamily' 'files'

         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_width][] = $iWidth; // 'filefamily' 'width'
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_height][] = $iHeight; // 'filefamily' 'height'
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_maxdim][] = ($iHeight > $iWidth) ? $iHeight : $iWidth; // 'filefamily' 'maxdim'
         sleep(0);

         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_ver][] = (int) sprintf('%s', $fi->attributes()->ver);  // 'filefamily' 'ver'
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_fpc][] = (int) sprintf('%s', $fi->attributes()->fpc);  // 'filefamily' 'fpc'
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_fpq][] = (int) sprintf('%s', $fi->attributes()->fpq);  // 'filefamily' 'fpq'
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_size][] = (int) sprintf('%s', $fi->attributes()->size);  // 'filefamily' 'size'

         $s = sprintf('%s', $fi->attributes()->ext);                   // UTF-8 20110526°165306
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_ext][] = $s; // 'filefamily' 'ext'
         $s = sprintf('%s', $fi->attributes()->flav);                  // UTF-8 20110526°165307
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_flav][] = $s; // 'filefamily' 'flav'
         $s = sprintf('%s', $fi->attributes()->addi);                  // UTF-8 20110526°165308
         $aShadow[Glb::GLR_BUDY_KEY_filefamily]['addi'][] = $s;        // 'filefamily' 'addi'

         $s = sprintf('%s', $fi->attributes()->pext);                  // UTF-8 20110526°165309
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_pext][] = $s; // [line 20110522°1641]  // 'filefamily' 'pext'

         if (self::$bHandleFilefamilymemberExtraAttributes === true)
         {
            $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDX_KEY_nwidth][] = (int) sprintf('%s', $fi->attributes()->nwidth); // not utf-8 decoding because it's an integer  // 'filefamily' 'nwidth'
            $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDX_KEY_nheight][] = (int) sprintf('%s', $fi->attributes()->nheight); // not utf-8 decoding because it's an integer  // 'filefamily' 'nheight'
            $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDX_KEY_nmaxdim][] = (int) sprintf('%s', $fi->attributes()->nmaxdim); // not utf-8 decoding because it's an integer  // 'filefamily' 'nmaxdim'
         }

         // more attributes from getimagesize() [seq 20110516°2211 new]
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_gtyp][] = (int) sprintf('%s', $fi->attributes()->gtype); // not utf-8 decoding because it's an integer  // 'filefamily' 'gtyp'
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_bits][] = (int) sprintf('%s', $fi->attributes()->bits); // not utf-8 decoding because it's an integer  // 'filefamily' 'bits'
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_chan][] = (int) sprintf('%s', $fi->attributes()->chan); // not utf-8 decoding because it's an integer  // 'filefamily' 'chan'

         $s = sprintf('%s', $fi->attributes()->mime);                  // UTF-8 20110526°165310
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_mime][] = $s; // 'filefamily' 'mime'

         $s = sprintf('%s', $fi->attributes()->err);                   // UTF-8 20110526°165311
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_err][] =  $s; // 'filefamily' 'err'
         if (self::$bHandleFilefamilymemberExtraAttributes === true)
         {
            $s = sprintf('%s', $fi->attributes()->html);               // UTF-8 20110526°165312
            $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDX_KEY_html][] = $s; // 'filefamily' 'html'
         }

         // [line 20190204°0757]
         $s = sprintf('%s', $fi->attributes()->url);
         $aShadow[Glb::GLR_BUDY_KEY_filefamily][Glb::GLR_SHDW_KEY_url][] = $s;

      }

      // Possibly supplement file [seq 20190204°0441]
      if ( $bIsCurrentFileInFileFamily !== TRUE )
      {
         $n = $xml->filefamily->addChild ('file', $sCallingBuddyFile);
         $bSuccess = self::xmlWriteToFile($xml, $sFullfilename);

      }

      return $aShadow;
   }

   /**
    * This method reads a buddyfile into an array.
    *
    * @id method 20110501°1111
    * @note Similar file access code is e.g. in
    *         • class.obyekt.php::readbuddyfile()
    *         • class.obyekt.php::maintain_albums()
    *         • class.dbtxt.php::load()
    * callers ..
    * @param $sFilename {string} The buddyfile
    * @return {Array|BOOLEAN} The wanted array with
    *           • [0] the hashtag if available, otherwise blank
    *           • [1] the 'hashvalue'
    *           • [2] the original line
    *           or FALSE
    */
   private function readbuddyfile($sFilename)
   {
      //global DEFVAL_GLR_sTHUMB180LINE; // todo : somehow use this

      // Prologue [seq 20110501°1121]
      $aReturn = array();
      // check 20110426°0043 : Filename seems sometimes coming as URL, sometimes as localdrive.
      //    As localdrive, it must not be mangled
      $sFilename = FileFamilies::mangleUrlFilename($sFilename);

      // [seq 20110501°1123]
      // note 20110426°0041 : Function file_exists() does not work with URL.
      $fDatei = @fopen($sFilename, 'r');
      if (! $fDatei)
      {
         // todo 20110426°0044 : Somehow provide feedback about the situation.
         return FALSE;
      }
      else
      {
         $sFilecontents = file_get_contents($sFilename);
      }
      if ($sFilecontents == FALSE)
      {
         // todo 20110426°0045 : Somehow provide feedback about the situation.
         return FALSE;
      }

      // [line 20110501°1125]
      // note : wanted to do it with explode(), but that worked not with newlines
      $aLines = preg_split("/\n/", $sFilecontents);                    // works

      // () Loop over the lines of the controltextfile [seq 20110501°1127]
      for ($i = 0 ; $i < sizeof($aLines) ; $i++)
      {
         // (.1) Comfort - get single line
         $sLine = $aLines[$i];

         // (.2) Make key/value pair from that line [seq 20110501°1133]
         // todo 20110426°0045 : Settle one of the two algorithms
         //unset($aSplitcolon);

         $aSplitcolon = Array();
         if (Glb::bToggle_FALSE)
         {
            $aSplitcolon = preg_split("/:/", $sLine);
            for ($i2 = 2 ; $i2 < sizeof($aSplitcolon) ; $i2++)
            {
                $aSplitcolon[1] .= ':' . $aSplitcolon[$i2];
            }
         }
         else
         {
            $aSplitcolon[0] = '';
            $aSplitcolon[1] = '';
            $i2 = strpos($sLine, ':');
            if ($i2 < 0)                                               // No colon found on line
            {
               $aSplitcolon[1] = substr($sLine, $i2);
            }
            else if ($i2 == 0)                                         // Colon is first character of the line
            {
               $aSplitcolon[1] = substr($sLine, 1);
            }
            else if ($i2 > 0)                                          // Colon found, treat line as key/value pair
            {
               $aSplitcolon[0] = substr($sLine,0, $i2);
               $aSplitcolon[1] = substr($sLine, $i2 + 1);
            }
         }

         // (.3) Guarantee valid key/value pair [seq 20110501°1135]
         // (.3.1) Guarantee key value
         if (! isset($aSplitcolon[0]))
         {
            $aSplitcolon[0] = '';
         }
         $aSplitcolon[0] = trim($aSplitcolon[0]);

         // (.3.2) Guarantee value value
         if (! isset($aSplitcolon[1]))
         {
            $aSplitcolon[1] = $aSplitcolon[0];
            $aSplitcolon[0] = '';
         }
         $aSplitcolon[1] = trim($aSplitcolon[1]);

         $aReturn[0][$i] = $aSplitcolon[0];
         $aReturn[1][$i] = $aSplitcolon[1];
      }

      // cosmetics — Remove leading and trailing empty lines, then re-index [seq 20110501°1137]
      $i = sizeof($aReturn[0]) - 1;
      while (($aReturn[0][$i] == '') and ($aReturn[1][$i] == ''))
      {
          unset($aReturn[0][$i]);
          unset($aReturn[1][$i]);
          $i--;
      }
      // [seq 20110501°1143]
      $i = 0;
      while (($aReturn[0][$i] == '') and ($aReturn[1][$i] == ''))
      {
          unset($aReturn[0][$i]);
          unset($aReturn[1][$i]);
          $i++;
      }
      $aReturn[0] = array_values($aReturn[0]);
      $aReturn[1] = array_values($aReturn[1]);

      return $aReturn;
   }

   /**
    * This method writes back a modified buddyfile
    *
    * @id 20110513°1821
    * @callers : So far only PageAlbum.php
    * @note No parameter. Wanted is the modified buddyfile array, e.g. after an
    *     album was added, but that is not returned, it is part of the object.
    * @return bool Success flag
    */
   public function writebackBuddyfile()
   {
      // Prologue [seq 20110513°1831]
      $sFile = Glb::$Glr_sBasePathImgFolders . '/' . $this->sRelativepath . '/' . $this->sBuddyfilename;

      // [seq 20110513°1832]
      $aWrite = array();
      $aWrite[] = Glb::$sTkNL; // when reading the file, we trimmed all empty leading lines, now supply exactly one
      for ($i = 0; $i < sizeof($this->aaBuddyfile[0]); $i++)
      {
         // Write key part [seq 20110513°1833]
         $sLin = '';
         if ($this->aaBuddyfile[0][$i] !== '')
         {
            $sLin .= '   ' . str_pad($this->aaBuddyfile[0][$i],11) . ' :';
         }

         // Write value part [seq 20110513°1834]
         if ($this->aaBuddyfile[1][$i] !== '')
         {
            if ($this->aaBuddyfile[0][$i] !== '')
            {
               $sLin .= ' ';
            }
            else if ($i < 2)
            {
               $sLin .= '   ';
            }
            else
            {
               $sLin .= '                  ';
            }
            $sLin .= $this->aaBuddyfile[1][$i];
         }

         // Linebreak [seq 20110513°1835]
         //  But not with the last element (that seems to be done by file_put_contents)
         if ($i < sizeof($this->aaBuddyfile[0]) - 1)
         {
            $sLin .= Glb::$sTkNL;
         }

         // Finaly store this line [seq 20110513°1836]
         $aWrite[] = $sLin;
      }
      $aWrite[] = Glb::$sTkNL; // When reading the file, we trimmed all empty trailing lines, now supply exactly one

      // Delete the shadowbuddyfile, it's values will be obsolet [seq 20110513°1837]
      $b = unlink($this->sShadowbuddyLocalFullname);

      // Finaly write the file [seq 20110513°1838]
      $i = file_put_contents($sFile, $aWrite);

      return TRUE;
   }


   /**
    * This method writes one SimpleXMLElement to file
    *  The native method asXML(filename) does write all content in
    *  one line, here we have linebreaks as formatting at least.
    *
    * @id 20190204°0451
    * @param $xml {\SimpleXMLElement} The content to write
    * @param $sFullfilename {String} The file to write
    * @return {Boolean} Success flag
    */
   private static function xmlWriteToFile($xml, $sFullfilename)
   {

      // Write XML buddy back to shadowbuddyfile [[[seq 20110514°1953]]]
      // note : The normal line were just '$b = $eBuddy->asXML($this->aaBuddyfile);',
      //    but that makes no linebreaks (as opposed to the documentation).


      $s = $xml->asXML();

      // Step 1 - ok all tags on separate line (regex 20110526°1841)
      $s2 = preg_replace('#><#', ('>' . Glb::$sTkNL . '<'), $s);

      // Step 2 - recombine tags of same element (regex 20110526°1842)
      $s3 = preg_replace('#>' . Glb::$sTkNL . '</#', ('></'), $s2);

      $iBytes = file_put_contents($sFullfilename, $s3);                // [mark 20190203°0711 chg]

      // Evaluate return value
      $bSuccess = FALSE;
      if (is_int($iBytes) )
      {
         $bSuccess = TRUE;
      }

      return $bSuccess;
   }


   /**
    * This field tells the onedimensional shadowbuddy bulk elements
    *
    * @id 20110501°0151
    * @var Array of Strings
    */
   private static $aShadowElementsSimple = array
                    ( Glb::GLR_BUDY_KEY_title                          // 'title'
                     , Glb::GLR_BUDY_KEY_policy                        // 'policy'
                      , Glb::GLR_BUDY_KEY_license                      // 'license'
                       , Glb::GLR_BUDY_KEY_copyright                   // 'copyright'
                        , Glb::GLR_BUDY_KEY_author                     // 'author'
                         , Glb::GLR_BUDY_KEY_inscription               // 'inscription'
                          , Glb::GLR_BUDY_KEY_description              // 'description'
                           , Glb::GLR_BUDY_KEY_location                // 'location'
                            , Glb::GLR_BUDY_KEY_owner                  // 'owner'
                             , Glb::GLR_BUDY_KEY_objecttype            // 'objecttype'
                              );

   /**
    * This field provides the buddy files key/value array
    *
    * @id 20110501°0141
    * @var String
    */
   public $aShadowbuddy = array();

   /**
    * This field stores ..
    *
    * @id 20110501°0144
    * @todo Should finally be made private
    * @var Array
    */
   public $aaBuddyfile = Array(Array());

   /**
    * This field tells ..
    *
    * @id 20110501°0152
    * @var String
    */
   private static $bHandleFilefamilymemberExtraAttributes = FALSE;

   /**
    * This field stores ..
    *
    * @id 20110501°0133
    * @todo Eliminate this if possible
    * @note Value e.g. 'X:\workspaces\img23456789\img'
    * @var String
    */
   public $sAbsolutepath;

   /**
    * This field stores ..
    *
    * @id 20110501°0137
    * @note Value e.g. 'img20110102'
    * @var String
    */
   public $sArchivePlain;

   /**
    * This field stores ..
    *
    * @id 20110501°0135
    * @note Value e.g. 'X:\workspaces' or '' (depending on environment)
    * @var String
    */
   public $sArchivesBasepathLocaldrive;

   /**
    * This field stores ..
    *
    * @id 20110501°0134
    * @note Value depending on environment e.g. 'http://localhost/workspaces/'
    *        or e.g. 'http://www.gallery.lum/svn/'
    * @var String
    */
   public $sBasepath2use;

   /**
    * This field stores ..
    *
    * @id 20110501°0132
    * @note Value e.g. e.g. '_20110123-1234_mytitle_.txt'
    * @var String
    */
   public $sBuddyfilename;

   /**
    * This field stores ..
    *
    * @id 20110501°0131
    * @note Value e.g. 'X:\workspaces\img23456789\img\20110414o1315.psychobox.txt'
    * @var String
    */
   public $sBuddyfullfilename;

   /**
    * This field stores the part which is common to all fileFamilyMembers
    *
    * @id 20110501°0138
    * @note Value e.g. '20110123o1234.mytitle'
    * @var String
    */
   public $sFilefamilyname;

   /**
    * This field stores ..
    *
    * @id 20110501°0136
    * @note Value e.g. 'img20110102\img'
    * @var String
    */
   public $sRelativepath;

   /**
    * This field stores the URL of the Shadow XML file. This is perhaps
    *  not useful, since it is in an area not accessible via HTTP.
    *
    * @id 20110501°0142
    * @note Value e.g. ..
    * @var String
    */
   private $sShadowbuddyHttpFullname;

   /**
    * This field stores the file system path of the Shadow XML file
    *
    * @id 20110501°0143
    * @note Value e.g. ..
    * @var String
    */
   private $sShadowbuddyLocalFullname;

   /**
    * This field stores ..
    *
    * @id 20110501°0146
    * @var String
    */
   public $tTimegaugeBcPart1;

   /**
    * This field stores ..
    *
    * @id 20110501°0147
    * @var String
    */
   public $tTimegaugeBcPart2;

   /**
    * This field stores ..
    *
    * @id 20110501°0145
    * @var String
    */
   public $tTimegaugeBcTotal;

}

/* eof */
