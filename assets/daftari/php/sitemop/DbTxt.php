<?php
/**
 * This module provides a super simple text file based database.
 *
 * @id file 20110507°1221
 * @license AGPL v3
 * @copyright © 2011 - 2023 Norbert C. Maier
 * @authors ncm
 * @status It works for simple tasks
 * @encoding UTF-8-without-BOM
 * @callers
 *     • file 20110818°1321 JsiEdit.php
 *     • file 20110424°1623 LoginPage.php
 *     • file 20120826°1551 SettingsPage.php — not really
 *     • class 20190129°0321 Login
 */

namespace Trekta\Daftari;

//use Trekta\Daftari as TD;
use Trekta\Daftari\Globals as Glb;

$iTbl_Fdx_Primary = 0;                                                 // Generic for primary key field

/**
 * This class provides database like access on TXT files.
 *
 * id 20110507°1222
 */
class DbTxt
{
   /**
    * @id 20110508°1423
    * @var Array This field stores the table definitions
    */
   public $aTabdef;

   /**
    * @id 20110508°1425
    * @var String This field stores the text file name
    */
   public $sTextfilename;

   /**
    * @id 20110508°1421
    * @var Array This field defines the database tables
    */
   private $aaTables = array
                    ( Glb::TBL_ACCTS_sTABLENAME => array               // 'accounts'
                                 ( 1 => 'email'                        // 1 'email'
                                 , 2 => 'shortname'                    // 2 'shortname'
                                 , 3 => 'pwhash'                       // 3 'pwhash'
                                 , 4 => 'fullname'                     // 4 'fullname'
                                 )
                     , Glb::TBL_ALBMS_sTBLNAME => array                // 'albums'
                                 ( 1 => Glb::TBL_ALBMS_sFNA_album      // 1 'album'
                                 , 2 => Glb::TBL_ALBMS_sFNA_owner      // 2 'owner'
                                 )
                      , Glb::TBL_OWNRS_sTBLNAME => array               // 'owners'
                                 ( 1 => Glb::TBL_OWNRS_sFNA_owner      // 1 'owner'
                                 , 2 => Glb::TBL_OWNRS_sFNA_permission // 2 'permission'
                                 )
                       , Glb::TBL_TGTMAP_sTBLNAME => array             // 'targetmap'
                                 ( 1 => Glb::TBL_TGTMAP_sFNA_rootOne   // 1 'rootOne'
                                 , 2 => Glb::TBL_TGTMAP_sFNA_trigger   // 2 'trigger'
                                 , 3 => Glb::TBL_TGTMAP_sFNA_rootTwo   // 3 'rootTwo'
                                 )
                        );

   /**
    * This constructor instantiates one DbTxt object
    *
    * @id : 20110507°1223
    * @callers : • Login.php • LoginPage.php
    * @param $sTable The table to be instantiated
    * @param String $Ses_sDataDirFullname The data directory full path
    */
   function __construct($sTable, $Ses_sDataDirFullname)
   {
      // () set basic properties
      $this->sTextfilename = $Ses_sDataDirFullname
                            . '/' . 'dbtxt'
                             . '/' . $sTable . '.txt'                  // E.g. = "users/accounts.txt"
                              ;

      // () Guarantee datadir [seq 20111002°2021]
      // ✱ This may serve as reference sequence for issue 20190201°0805 'guarantee directory'
      // todo : Work off issue 20111002°1922  'check filepermissions'
      // todo : This sequence exists multiple times — Merge all occurrences
      //         e.g. into a method of class Globals [todo 20190127°0541]
      // todo : See issue 20190201°0805 'guarantee directory'
      // note : Compare method SQLitConf::init seq 20180331°0132
      $s = $Ses_sDataDirFullname . '/' . 'dbtxt'; // provisory hardcoded
      if (! is_dir ($s))
      {
         if (! mkdir($s))
         {
            $s = '<p class="phpError">Quirk 20111002°2022 - Folder missing.</p>';
            echo ($s);
            //exit();
         }
      }

      // Set table definition [seq 20110507°1225]
      // attention : array_key_exists() might return FALSE
      $x = array_key_exists($sTable, $this->aaTables);
      if ($x === false)
      {
      }
      $this->aTabdef = $this->aaTables[$sTable];
   }

   /**
    * This method adds a record to a DbTxt table.
    *
    * @id 20110507°1224
    * @note Similar file access code is e.g. in
    *        - class.obyekt.php::readbuddyfile()
    *        - class.obyekt.php::maintain_albums()
    *        - class.dbtxt.php::load()
    * @callers ..
    * @param $aNewRec {array} The new record to be added
    * @return bool Success flag
    */
   public function addrec($aNewRec)
   {
      global $iTbl_Fdx_Primary;

      $aLines = array();

      if (file_exists($this->sTextfilename))
      {
         $aLines = @file($this->sTextfilename);                        // Flag FILE_IGNORE_NEW_LINES not used
      }

      $iCols = count($aNewRec);
      $aaTable = array();

      //-------------------------------------------------------
      // Make 'table' from lines
      // todo: Replace seqence by calling the respective function (which was created in the meanwhile) readColumnsArrayFromTextfile()
      $iLin = 0;
      foreach ($aLines as $sLine)
      {
         $aToks = explode(' ', $sLine);
         for ($iTok = 0; $iTok < sizeof($aToks); $iTok++)
         {
            // Process all but the last field
            if ($iTok < ($iCols - 1))
            {
               $aaTable[$iLin][$iTok] = $aToks[$iTok];
               continue;
            }
            // Process the last field
            if ($iTok < ($iCols))
            {
               $aaTable[$iLin][($iCols - 1)] = $aToks[$iTok];
               continue;
            }
            $aaTable[$iLin][($iCols - 1)] .= ' ' . $aToks[$iTok];
         }
         $iLin++;
      }
      //-------------------------------------------------------

      // Does this record already exist?
      $sKeyvalue = $aNewRec[$iTbl_Fdx_Primary];
      $iIndex = -1;
      for ($i = 0; $i < sizeof($aaTable) ; $i++)
      {
         if ($aaTable[$i][$iTbl_Fdx_Primary] == $sKeyvalue)
         {
            $iIndex = $i;
            break;
         }
      }

      // Insert new record
      if ($iIndex < 0)
      {
         $s = implode(' ',$aNewRec) . Glb::$sTkNL;
         $aLines[] = $s;                                               // Add the new record
         if (! sort($aLines))
         {
            echo('<p class="error">Event 20110508°1846 passed.</p>');  // Fatal — Note css class 'error' [line 20110508°1841`11]
            return false;
         }
         $i = file_put_contents($this->sTextfilename,$aLines);
         if ($i === false)
         {
            echo('<p class="phpError">Event 20110508°1845 happened</p>'); // Fatal — Nte css class 'error' [line 20110508°1841`12]
            return false;
         }
      }
      else
      {
         return $aaTable[$iIndex];                                     // Feedback about any already existing record
      }

      return true;                                                     // Hm..
   }


   /**
    * This method deletes a record by it's primary key value.
    *
    * @id 20110508°1821
    * @callers ..
    * @param $sPrimaryKeyValue {string} The id of the record to be deleted
    * @return bool Success flag
    */
   public function deleteRecord($sPrimaryKeyValue)
   {
      global $iTbl_Fdx_Primary;

      $aaTabRev = $this->readColumnsArrayFromTextfile();
      $iKey = (array_search($sPrimaryKeyValue,$aaTabRev[$iTbl_Fdx_Primary]));
      if ($iKey === false)
      {
         echo('<p class="error">Event 20110508°1844 passed.</p>');     // Fatal — Note css class 'error' [line 20110508°1841`13]
         return false;
      }

      // The actual job
      for ($i = 0; $i < sizeof($aaTabRev); $i++)
      {
         unset($aaTabRev[$i][$iKey]);
      }
      // Indexes have a gap now, so re-index the arrays, otherwise error below
      for ($i = 0; $i < sizeof($aaTabRev); $i++)
      {
         $x = $aaTabRev[$i];
         $x = array_values($x);
         // If indices before were 0 1 2 3 4 6 7, they may now be 0 1 2 3 4 6 5.
         // That's not yet useful for below, so ksort()!
         // Hm .. sometimes indices are discontiuing, sometimes not .. WHY?
         // If we knew the precise working, we might save the time consuming sort.
         ksort($x);
         $aaTabRev[$i] = $x;
      }

      //-------------------------------------------------------
      // Mount lines array from table array and write it back to file [seq 20110508°1831]
      // todo: Outsource sequence to separate method
      $aLines = array();
      $i2 = 0;
      foreach ($aaTabRev as $aField)
      {
         $i = 0;
         foreach ($aField as $sVal)
         {
            if ($i2 < 1)
            {
               $aLines[$i] = $sVal;
            }
            else
            {
               $aLines[$i] .= ' ' . $sVal;
            }
            $i++;
         }
         $i2++;
      }
      for ($i = 0; $i < sizeof($aLines) ; $i++)
      {
         $aLines[$i] .= Glb::$sTkNL;
      }
      $i = file_put_contents($this->sTextfilename,$aLines);
      if ($i === false)
      {
         echo('<p class="error">Event 20110508°1843 passed.</p>');     // Fatal — Note css class 'error' [line 20110508°1841`14]
         return false;
      }
      //-------------------------------------------------------

      return true;
   }

   /**
    * This method retrieves a record by searching a value in a specified fieldnumber.
    *
    * @id 20110507°1921
    * @callers - Login.php, ..
    * @param $iFieldIndex  {} ..
    * @param $sSearchValue {} ..
    * @return Array If found then (sizeof($aRet) > 0); if not found then (sizeof($aRet) = 0);
    */
   public function lookupRecord($iFieldIndex, $sSearchValue)
   {
      $aRec = array();                                                 // Returning this array blank means 'not found'
      $aaTabRev = $this->readColumnsArrayFromTextfile();
      $iKey = (array_search($sSearchValue, $aaTabRev[$iFieldIndex]));
      if ($iKey !== false)
      {
         for ($i = 0; $i < sizeof($this->aTabdef); $i++)
         {
            $aRec[$i] = $aaTabRev[$i][$iKey];
         }
      }
      return $aRec;
   }

   /**
    * This method reads the table into an array of arrays.
    *
    * @id 20170925°0531
    * @return Array The result array of arrays
    */
   public function readAll()
   {
      $aaTable = $this->readRecordsArrayFromTextfile();

      return $aaTable;
   }

   /**
    * This method reads a textfile into a reverse-table-like array of arrays.
    *
    * @id 20110507°1922 (after 20110508°1421)
    * @note Why is the array reverse organized like $a[fieldno][recno]? This
    *    way it's more easy to search field values. But harder to pass one record.
    * @callers
    * @return Array The wanted array-of-arrays with the buddyfile's fields
    */
   private function readColumnsArrayFromTextfile()
   {
      $aLines = array();
      if (file_exists($this->sTextfilename))
      {
         $aLines = @file($this->sTextfilename);                        // Flag FILE_IGNORE_NEW_LINES not used
      }

      $iColNo = sizeof($this->aTabdef);

      // Prepare empty table (mind the reverse dimensions)
      $aaTable = array();
      for ($i = 0; $i < $iColNo; $i++)
      {
         $aaTable[$i] = array();
      }

      // Make 'table' from lines
      // todo : Replace seqence by calling the respective function (which was created in the meanwhile)
      $iLin = 0;
      foreach ($aLines as $sLine)
      {
         $sLine = trim($sLine);                                        // Remove trailing CRLF
         $aToks = explode(' ', $sLine);
         for ($iTok = 0; $iTok < sizeof($aToks); $iTok++)
         {
            // Process all but the last field
            if ($iTok < ($iColNo - 1))
            {
               $aaTable[$iTok][$iLin] = $aToks[$iTok];
               continue;
            }
            // Process the last field
            if ($iTok < $iColNo)
            {
               $aaTable[($iColNo - 1)][$iLin] = $aToks[$iTok];
               continue;
            }
            $aaTable[($iColNo - 1)][$iLin] .= ' ' . $aToks[$iTok];
         }
         $iLin++;
      }

      return $aaTable;

   } // End readColumnsArrayFromTextfile

   /**
    * This method reads a textfile into a table-like array of arrays.
    *
    * @id 20170925°0541
    * @callers
    * @return Array The wanted array of records
    */
   private function readRecordsArrayFromTextfile()
   {
      // () Standard intro
      $aLines = array();
      if (file_exists($this->sTextfilename))
      {
         $aLines = @file($this->sTextfilename);                        // Flag FILE_IGNORE_NEW_LINES not used
      }

      // Make table from lines
      // todo : Replace seqence by calling the respective function (which was created in the meanwhile)
      $aaTable = array();
      $iIndex = 0;
      foreach ($aLines as $sLine)
      {
         $sLine = trim($sLine);                                        // Remove trailing CRLF
         $aToks = explode(' ', $sLine);
         $aaTable[$iIndex] = $aToks;
         $iIndex++;
      }

      return $aaTable;

   } // End readRecordsArrayFromTextfile

   /**
    * This method writes a given record, identified by primary key field.
    *
    * @id 20110509°0221
    * @callers LoginPage.php change password sequence
    * @param $aNewRec {array} The record to be written
    * @return bool A success flag
    */
   public function writeRecord($aNewRec)
   {
      // Haha .. this is a too primitive and inefficient solution

      $b = $this->deleteRecord($aNewRec[0]);
      if ($b !== true)
      {
         echo('<p class="error">Event 20110509°0241 passed.</p>');     // Fatal — Note css class 'error' [line 20110508°1841`15]
         return false;
      }

      $b = $this->addrec($aNewRec);
      if ($b !== true)
      {
         echo('<p class="error">Event 20110509°0242 passed.</p>');     // Fatal — Note css class 'error' [line 20110508°1841`16]
         return false;
      }
      return true;
   }

}

/* eof */
