﻿/*!
 * file        : 20160426°2251
 * license     : GNU AGPL v3
 * copyright   : © 2014 - 2023 Norbert C. Maier
 * authors     : Norbert C. Maier
 * encoding    : UTF-8-with-BOM
 */
/**
 * issue 20161109°1821 : This sitemap does not handle multiple root chapters.
 * note        : Obey rule 20190419°0151 'sitmapdaf.js must always tell the page file name'
 * note note 20180307°0121 : Discern two places
 *    (1) Sitmap folder — the one, where this sitmapdaf.js file resides
 *    (2) Base folder — the one where the lowest level file resides
 * note 20180307°0122 : Developers remember problem tackled e.g. with debug 20180307°0111
 */

var DAF_PAGENODES =
[
   [ 'Daftari'                             ,  './../index.html'                // 'DaftariBoard#20161109°2114'
    , [ 'UserManual#20190205°0551'         ,  './../docs/help.html'            // 'Help#20161109°2122', 'HelpFiles#20190205°0552', 'UserManual#20190205°0551'
       , [ 'Installation#20161109°2123'    ,  './../docs/install.html'   ]     // Introduction#20161109°2124
       , [ 'Autoflick#20201114°0223'       ,  './../docs/autoflick.html' ]
       , [ 'Blogs#20180619°0323'           ,  './../docs/blogs.html'
          , [ 'Articles#20181217°0233'     ,  './../docs/blogitems/20180619o0511.articles.html' ]
         ]
       , [ 'Breadcrumbs#20161109°2111'     ,  './../docs/breadcrumbs.html' ]
       , [ 'Cakecrumbs#20181217°0234'      ,  './../docs/cakecrumbs.html' ]
       , [ 'CanvasGear#20161109°2112'      ,  './../docs/canvasgear.html' ]
       , [ 'Editpoints#20161109°2115'      ,  './../docs/editpoints.html' ]
       , [ 'FadeInFiles#20161109°2116'     ,  './../docs/fadeinfiles.html' ]
       , [ 'Gallery#20161109°2121'         ,  './../docs/gallery.html'   ]
       , [ 'Lingos#20161109°2127'          ,  './../docs/lingos.html'    ]
       , [ 'Login#20161109°2131'           ,  './../docs/login2.html'    ]
       , [ 'OutputPane#20161109°2132'      ,  './../docs/outputpane.html' ]
       , [ 'Sitemap#20181217°0235'         ,  './../docs/sitemap.html'   ]
       , [ 'Slideshow#20161109°2134'       ,  './../docs/slideshow.html' ]
       , [ 'Starting'                      ,  './../docs/start.html'     ]
       , [ 'Styles#20161109°2135'          ,  './../docs/styles.html'
          , [ 'Fonts#20230414°1011'        ,  './../docs/styles_fonts.html' ]
         ]
       , [ 'Issues#20161109°2125'          ,  './../docs/issues.html'    ]
       , [ 'Credits#20161109°2113'         ,  './../docs/credits.html'   ]
       , [ 'License#20161109°2126'         ,  './../docs/license.html'   ]
      ]
    , [ 'Tools#20161109°2136'              ,  './../panels/tools.html'   ]
    , [ 'Settings#20161109°2133'           ,  './../panels/settings.html' ]
    , [ 'Login#20161109°2131'              ,  './../panels/login.html'   ]
   ]
];
var DAF_SETTINGS = {
   'default_lang' : 'eng'
};
