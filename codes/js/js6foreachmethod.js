/*!
 * This script demonstrates the ForEach method in three flavours
 *
 * @id file 20231104°0911
 * @license BSD 3-Clause | © 2023 - 2024 Norbert C. Maier
 * @ref : https://www.w3schools.com/jsref/jsref_foreach.asp [ref …]
 * @ref : https://www.w3schools.com/Js/js_arrow_function.asp [ref …]
 * @status ...
 */

var NS = NS || {};                                             // Namespace

////const NS.ar = [4, 3, 2, 1, 0];
NS.ar = [4, 3, 2, 1, 0];

NS.myFunc = function(x) {
   NS.outLine(' ' + x);
}

/**
 * This function starts the demo
 * @id
 */
NS.runDemo = function()
{
   NS.outLine('*** Aloha js6foreachmethod.js ***');
   NS.outLine('\nHere is the array : ' + NS.ar);

   NS.outLine('\nForEach with named function:');
   NS.ar.forEach(NS.myFunc);

   NS.outLine('\nForEach with anonymous function:');
   NS.ar.forEach(function(x){ NS.outLine(' ' + x); });

   NS.outLine('\nForEach with lambda expression:');
   NS.ar.forEach( (x) => { NS.outLine(' ' + x) } );

   NS.outLine('\nBye.');
};


/**
 * This function constitutes the output facility
 * @id 20190430°0251
 * @param sMsg {String} The message to output
 */
NS.outLine = function (sMsg)
{
   var elm = document.getElementById('id20190430o0132');
   var sInner = elm.innerHTML;
   ////if (sInner !== '') { sInner += '\n'; }
   sInner += sMsg;
   elm.innerHTML = sInner;
};

/* eof */
