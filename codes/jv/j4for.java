/**
 *  file       : id 20231103°1921 [chain 20220826°1715 jv240for.java]
 *  version    :
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstratea For loop
 */

class j4for
{
   public static void main(String[] args)
   {
      System.out.println("*** Aloha j4for.java ***");

      System.out.print("Countdown:");
      for (int i = 4; i >= 0; i--)
      {
         System.out.print(" " + i);
      }

      System.out.println("");
      System.out.println("Bye.");
   }
}
