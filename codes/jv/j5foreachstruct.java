/**
 *  file       : id 20231103°1931 [chain 20220826°1715 jv240for.java]
 *  version    :
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the For-Each loop
 */

class j5foreachstruct {

   public static void main(String[] args)
   {
      System.out.println("*** Aloha j5foreachstruct.java ***");

      int ar[] = {4, 3, 2, 1, 0};

      System.out.print("Countdown:");
      for (int x : ar) {
         System.out.print(" " + x);
      }

      System.out.println();
      System.out.println("Bye.");
   }
}
