/**
 *  file       : id 20231221°1611 Jv8timer.java
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate looping with timer
 *  reference  : https://www.baeldung.com/java-timer-and-timertask [ref 20231221°1512]
 *  reference  : https://www.digitalocean.com/community/tutorials/java-timer-timertask-example [ref 20231221°1514]
 *  reference  : https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Timer.html [ref 20231221°1516]
 *  reference  : https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/TimerTask.html [ref 20231221°1517]
 */

////package com.baeldung.timer;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Random;
import java.util.Timer; ////
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

////public class NewsletterTask extends TimerTask {
class NewsletterTask extends TimerTask {

    @Override
    public void run() {
        System.out.println("Email sent at: " + LocalDateTime.ofInstant(Instant.ofEpochMilli(scheduledExecutionTime()), ZoneId.systemDefault()));
        Random random = new Random();
        int value = random.ints(1, 7).findFirst().getAsInt();
        System.out.println("The duration of sending the mail will took: " + value);
        try {
            TimeUnit.SECONDS.sleep(value);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class Jv8timer {

    public static void main(String[] fritz)
    {
        System.out.println("*** Tere tulemast Jv8timer.java ***");

        new Timer().schedule(new NewsletterTask(), 0, 1000);

        System.out.println();
        System.out.println("Bye.");
    }
}
