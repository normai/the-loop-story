/**
 *  file       : id 20231103°1911 [chain 20220826°1615] j3dowhile.java
 *  version    :
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Foot-controlled Do/While loop
 *  chain      : file 20220826°1615 cheeseburger/…/jv235dowhile.java
 *  note       : Counting down is not a typical Do-While case. For a
 *                typical case see e.g. 'roll dice until you get s six'
 */

class j3dowhile
{
   public static void main(String[] args)
   {
      System.out.println("*** This is j3dowhile.java ***");

      int iCount = 4;                                          // Initialize loop variable

      System.out.print("Count down:");
      do                                                       // Loop head unconditional
      {
         System.out.print(" " + iCount);
         iCount--;                                             // Change condition variable
      } while (iCount >= 0);                                   // Loop foot with the loop condition

      System.out.println("\nBye.");
   }
}
