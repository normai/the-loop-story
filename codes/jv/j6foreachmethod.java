/**
 *  file       : id 20231103°1941
 *  version    :
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the For-Each method
 *  ref        : https://www.baeldung.com/foreach-java [ref …]
 *  ref        : https://howtodoinjava.com/java8/foreach-method-example/ [ref …]
 *  note       : The function to be passed to the For-Each method can be
 *               1. A real function by it's name (not in Java)
 *               2. An anonymous function inside the parentheses (not in Java)
 *               3. A lambda expression (since Java 8)
 *  ref        : https://www.delftstack.com/howto/java/how-to-pass-a-function-as-a-parameter-in-java/ [ref …]
 *  ref        : https://stackoverflow.com/questions/2755445/how-can-i-write-an-anonymous-function-in-java [ref …]
 */

import java.util.ArrayList;
import java.util.Arrays;

class j6foreachmethod {

   private static void outputz() {
      System.out.print(" x");
   }

   public static void main(String[] args)
   {
      System.out.println("*** Aloha j6foreachmethod.java ***");

      // int ar = {4, 3, 2, 1, 0};                     // Has no forEach method
      ArrayList<Integer> ar = new ArrayList<>(Arrays.asList(4, 3, 2, 1, 0));

      System.out.print("Countdown:");
      ar.forEach (x -> {                               // Lambda expresssion. Since it is
         System.out.print(" " + x);                    // .. a one-liner, the curly braces
      });                                              // .. could be left out

      System.out.println();
      System.out.println("Bye.");
   }
}
