/**
 *  file       : id 20231103°1951
 *  version    :
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate recursion
 */

class j6foreachmethod {

   private static void rekuhrse(int i) {

      System.out.print(" " + i);
      if (i > 0) {
         rekuhrse(--i);
      }

   }

   public static void main(String[] args)
   {
      System.out.println("*** Aloha j7recursion.java ***");

      rekuhrse(4);

      System.out.println();
      System.out.println("Bye.");
   }
}
