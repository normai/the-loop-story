/**
 *  file       : id 20231103°1851 [chain 20220826°1515 jv230while.java]
 *  version    : •
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the While loop
 */

class j2while
{
   public static void main(String[] args)
   {
      System.out.println("*** This is j2while.java ***");

      // (1) Initialize loop
      int iCount = 4;

      // (2) Set loop head
      System.out.print("Count down:");
      while (iCount >= 0)
      {
         System.out.print(" " + iCount);
         iCount--;
      }
      System.out.println();

      System.out.println("Bye.");
   }
}
