@echo off
rem file 20231103`1943 [root 20231103`1851]

color A0
title This is %~n0%~x0 by user %USERNAME%
echo ***************************************************************
echo *** This is %~n0%~x0 by user %USERNAME%
echo *** Thisdir = %~dp0
echo *** CWD     = %cd%
rem echo *** options  = %1 %2 %3
echo ***************************************************************

%~d0
cd "%~dp0"
@echo on

javac.exe j6foreachmethod.java

pause

java.exe j6foreachmethod

pause
