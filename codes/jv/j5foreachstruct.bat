@echo off
rem file 20231103`1933 [root 20231103`1851]

color A0
title This is %~n0%~x0 by user %USERNAME%
echo ***************************************************************
echo *** This is %~n0%~x0 by user %USERNAME%
echo *** Thisdir = %~dp0
echo *** CWD     = %cd%
rem echo *** options  = %1 %2 %3
echo ***************************************************************

%~d0
cd "%~dp0"
@echo on

javac.exe j5foreachstruct.java

pause

java.exe j5foreachstruct

pause
