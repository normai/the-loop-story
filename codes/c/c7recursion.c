﻿/**
 *  file       : 20231103°1841 [chain 20231021°0931] c7recursion.c
 *  version    :
 *  license    : BSD-3-Clause license
 *  authors    : Norbert C. Maier
 *  summary    : Demonstrate looping by recursion
 */

#include <stdio.h>

void rekuhrs(int j) {                                                   /* [func 20231021°0935] */

   printf(" %i", j);
   j--;

   if (j >= 0) {
      rekuhrs(j);
   }
}

int main(int argc, char* argv[])                                        /* [func 20231021°0933] */
{
   printf("*** Demonstrate Looping By Recursion ***\n");

   int i = 4;

   printf("Countdown:");
   rekuhrs(i);

   printf("\nFinished.\n");
   printf("Press Enter to exit ...\n");
   getchar();

   return 0;
}
