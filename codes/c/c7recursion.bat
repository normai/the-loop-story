@echo off
rem 20231103`1843 [chain 20231021°0941, root 20211012`0717]
rem Remember iissue 20211012`1811:
rem    Multiple calls to vcvarsall.bat cause the
rem    path to grow each time. Pretty soon, it will be too long,
rem    thus causing the curious error "The input line is too long".
rem    Remedy : Wrap the vcvarsall.bat call in "IF NOT DEFINED DevEnvDir", as done below
rem    See e.g. https://stackoverflow.com/questions/16821784/input-line-is-too-long-error-in-bat-file

color A0
title This is file %~n0%~x0 by user %USERNAME%
echo ***************************************************************
echo *** This is file %~n0%~x0 by user %USERNAME%
echo *** This dir = %~dp0
echo *** CWD      = %cd%
rem echo *** options  = %1 %2 %3
echo ***************************************************************

rem set VCVARSALLPATH=C:\Program Files\Microsoft Visual Studio 8\VC
rem set VCVARSALLPATH=C:\Program Files\Microsoft Visual Studio 9.0\VC
rem set VCVARSALLPATH=C:\Program Files\Microsoft Visual Studio 10.0\VC
set VCVARSALLPATH=C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build

%~d0
cd "%~dp0"
@echo on

IF NOT DEFINED DevEnvDir (
   call "%VCVARSALLPATH%\vcvarsall.bat" x86
)

cl.exe c7recursion.c

pause

c7recursion.exe

pause
