﻿/**
 *  file       : 20231103°1831 [chain 20231021°0911]
 *  version    :
 *  license    : BSD-3-Clause license
 *  authors    : Norbert C. Maier
 *  summary    : Demonstrate looping by goto
 */

#include <stdio.h>

int main(int argc, char* argv[])                                    /* [func 20231021°0913] */
{
   printf("*** Demonstrate Looping By Goto ***\n");

   int i = 4;

   printf("Countdown:");

  loop_head:                           /* Label, jump mark */

   printf(" %i", i);
   i--;

   if (i >= 0) {
      goto loop_head;                  /* Jump */
   }

   printf("\nFinished");
   printf("*** Press Enter to exit. ***\n");
   getchar();

   return 0;
}
